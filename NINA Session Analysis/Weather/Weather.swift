//
//  Weather.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 02.06.24.
//

import Foundation
import SwiftUI
import WeatherKit
import CoreLocation

@Observable
public class Weather {
	var hourWeather: [HourWeather] = []
	private var lastRequest: Date?
	private var period: TimeInterval = TimeInterval(3600)
	
	func updateHourlyWeather(for loc: CLLocation) async {
		
		if let last = self.lastRequest {
			let current = Date()
			if abs(current.distance(to: last)) < period {
				return
			}
		}
		
		do {
			
			let service = WeatherService()
			let hourlyForecast = try await service.weather(for: loc, including: .hourly)
			
			if hourlyForecast.forecast.count > 0 {
				await MainActor.run {
					self.hourWeather = hourlyForecast.forecast
					self.lastRequest = Date()
				}
			}
			
		}
		catch(let error) {
			print(error.localizedDescription)
		}
		
	}
	
}
