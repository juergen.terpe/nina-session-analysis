//
//  CFAPattern.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 08.07.24.
//

public enum CFAPattern {
	
	/**
	 RG
	 GB
	 */
	case RGGB
	
	/**
	 BG
	 GR
	 */
	case BGGR
	
	/**
	 GR
	 BG
	 */
	case GRBG
	
	/**
	 GB
	 RG
	 */
	case GBRG
}
