//
//  ImageFormat.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 08.07.24.
//

import Accelerate


public protocol PixelFormat {
	
	static var channels : Int { get }
	static var alpha: CGImageAlphaInfo {get}
	static var colorSpace : CGColorSpace {get}
	
}

public struct Mono : PixelFormat {
	public static let channels: Int = 1
	public static let alpha: CGImageAlphaInfo = .none
	public static let colorSpace: CGColorSpace = CGColorSpaceCreateDeviceGray()
}

public struct RGB : PixelFormat {
	public static let channels: Int = 3
	public static let alpha: CGImageAlphaInfo = .none
	public static let colorSpace: CGColorSpace = CGColorSpaceCreateDeviceRGB()
}

public struct ARGB : PixelFormat {
	public static let channels: Int = 4
	public static let alpha: CGImageAlphaInfo = .noneSkipFirst
	public static let colorSpace: CGColorSpace = CGColorSpaceCreateDeviceRGB()
}

public struct RGBA : PixelFormat {
	public static let channels: Int = 4
	public static let alpha: CGImageAlphaInfo = .noneSkipLast
	public static let colorSpace: CGColorSpace = CGColorSpaceCreateDeviceRGB()
}
