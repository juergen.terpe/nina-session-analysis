//
//  FITS.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 02.07.24.
//

import Foundation

public class FITS {
	public let header: FitsHeader
	public let primary: PrimaryDataUnit

	public init(header: FitsHeader, primary: PrimaryDataUnit) {
		self.header = header
		self.primary = primary
	}
}

public extension FITS {
	
	static func readHeader(from url: URL) -> FitsHeader? {
		do {
			let fileHandle = try FileHandle(forReadingFrom: url)
			defer { try? fileHandle.close() }
			
			var headerData = Data()
			let blockSize = 2880
			
			while true {
				// Lese einen Block von 2880 Bytes
				guard let chunk = try fileHandle.read(upToCount: blockSize), !chunk.isEmpty else {
					break
				}
				headerData.append(chunk)
				
				// Prüfe, ob der Header bereits den "END"-Marker enthält.
				if hasEndMarker(in: headerData) {
					// Falls der gelesene Header noch nicht exakt ein Vielfaches von 2880 Bytes umfasst, auffüllen.
					let remainder = headerData.count % blockSize
					if remainder != 0 {
						headerData.append(Data(count: blockSize - remainder))
					}
					break
				}
			}
			
			return FitsHeader.read(from: headerData)
		} catch {
			return nil
		}
	}

	/// Prüft, ob in den übergebenen Daten ein 80-Byte-Card mit "END" als Start enthalten ist.
	private static func hasEndMarker(in data: Data) -> Bool {
		let cardSize = 80
		let endMarker = "END".data(using: .ascii)!
		
		// Gehe Block für Block (80 Byte) durch die Daten.
		for offset in stride(from: 0, to: data.count, by: cardSize) {
			if offset + cardSize > data.count { break }
			let card = data.subdata(in: offset..<offset+cardSize)
			if card.starts(with: endMarker) {
				return true
			}
		}
		return false
	}
	
	
	static func read(from url: URL) async -> FITS? {
		if let data = try? Data(contentsOf: url, options: .uncached) {
			let header = FitsHeader.read(from: data)
			if let pdu = await PrimaryDataUnit.read(from: data, for: header) {
				return FITS(header: header, primary: pdu)
			}
		}
		return nil
	}
	
}
