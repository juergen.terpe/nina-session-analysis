//
//  PrimaryDataUnit.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 02.07.24.
//
import Foundation

public class PrimaryDataUnit {
	let data: [UInt16]
	public let width: Int
	public let height: Int
	
	public init(data: [UInt16], width: Int, height: Int) {
		self.data = data
		self.width 	= width
		self.height = height
	}
}

extension Data {
	func load<T: FixedWidthInteger>(fromByteOffset offset: Int) -> T {
		return self.withUnsafeBytes { $0.load(fromByteOffset: offset, as: T.self) }
	}
	
	func loadBigEndian<T: FixedWidthInteger>(fromByteOffset offset: Int) -> T {
		return T(bigEndian: load(fromByteOffset: offset))
	}
	
	func loadBigEndian(fromByteOffset offset: Int) -> Float {
		let value: UInt32 = loadBigEndian(fromByteOffset: offset)
		return Float(bitPattern: value)
	}
	
	func loadBigEndian(fromByteOffset offset: Int) -> Double {
		let value: UInt64 = loadBigEndian(fromByteOffset: offset)
		return Double(bitPattern: value)
	}
}


extension PrimaryDataUnit {
	
	static func read(from data: Data, for header: FitsHeader) async -> PrimaryDataUnit? {
		guard let axisSizes = PrimaryDataUnit.getAxisSizes(from: header),
			  let contentType = header.getDataContentType() else {
			return nil
		}

		let totalNumberOfValues = axisSizes.reduce(1, *)
		let numberOfBytesPerValue = Int(abs(contentType.rawValue / 8))
		let offset = header.Offset
		let bzero = Double(header["BZERO"]?.Value ?? "0") ?? 0.0
		let bscale = Double(header["BSCALE"]?.Value ?? "1") ?? 1.0

		var pduData = [UInt16](repeating: 0, count: totalNumberOfValues)

		data.withUnsafeBytes { rawBuffer in
			let buffer = rawBuffer.baseAddress!.advanced(by: offset)
			
			switch contentType {
				case .BYTE:
					let values = buffer.assumingMemoryBound(to: UInt8.self)
					for i in 0..<totalNumberOfValues {
						pduData[i] = UInt16(clamping: Int((Double(values[i]) * bscale + bzero).rounded()))
					}
					
				case .INT16:
					let values = buffer.assumingMemoryBound(to: Int16.self)
					for i in 0..<totalNumberOfValues {
						pduData[i] = UInt16(clamping: Int((Double(values[i].bigEndian) * bscale + bzero).rounded()))
					}
					
				case .INT32:
					let values = buffer.assumingMemoryBound(to: Int32.self)
					for i in 0..<totalNumberOfValues {
						pduData[i] = UInt16(clamping: Int((Double(values[i].bigEndian) * bscale + bzero).rounded()))
					}
					
				case .INT64:
					let values = buffer.assumingMemoryBound(to: Int64.self)
					for i in 0..<totalNumberOfValues {
						pduData[i] = UInt16(clamping: Int((Double(values[i].bigEndian) * bscale + bzero).rounded()))
					}
					
				case .FLOAT:
					let values = buffer.assumingMemoryBound(to: Float.self)
					for i in 0..<totalNumberOfValues {
						pduData[i] = UInt16(clamping: Int((Double(values[i].bitPattern.bigEndian) * bscale + bzero).rounded()))
					}
					
				case .DOUBLE:
					let values = buffer.assumingMemoryBound(to: UInt64.self)
					for i in 0..<totalNumberOfValues {
						let doubleValue = Double(bitPattern: values[i].bigEndian)
						pduData[i] = UInt16(clamping: Int((doubleValue * bscale + bzero).rounded()))
					}
			}
		}

		return PrimaryDataUnit(data: pduData, width: axisSizes[0], height: axisSizes[1])
	}
	
	/*
	static func read(from data: Data, for header: FitsHeader) async -> PrimaryDataUnit? {
		guard let axisSizes = PrimaryDataUnit.getAxisSizes(from: header) else {
			return nil
		}
		guard let contentType = header.getDataContentType() else {
			return nil
		}
		
		let totalNumberOfValues = axisSizes.reduce(1, *)
		let numberOfBytesPerValue = Int(abs(contentType.rawValue / 8))
		let offset = header.Offset
		let bzero = Double(header["BZERO"]?.Value ?? "0") ?? 0.0
		let bscale = Double(header["BSCALE"]?.Value ?? "1") ?? 1.0
		
		var pduData = [UInt16](repeating: 0, count: totalNumberOfValues)
		
		for i in 0..<totalNumberOfValues {
			pduData[i] = PrimaryDataUnit.readValue(
				from: data, at: i, size: numberOfBytesPerValue, offset: offset,
				type: contentType, bzero: bzero, bscale: bscale
			)
		}
		
		return PrimaryDataUnit(data: pduData, width: axisSizes[0], height: axisSizes[1])
	}
	
	private static func readValue(from data: Data, at index: Int, size: Int, offset: Int,
								  type: DataContentType, bzero: Double, bscale: Double) -> UInt16 {
		let byteOffset = offset + index * size
		
		switch type {
			case .BYTE:
				let value: UInt8 = data.load(fromByteOffset: byteOffset)
				return UInt16(clamping: Int((Double(value) * bscale + bzero).rounded()))
			
			case .INT16:
				let value: Int16 = data.loadBigEndian(fromByteOffset: byteOffset)
				return UInt16(clamping: Int((Double(value) * bscale + bzero).rounded()))
				
			case .INT32:
				let value: Int32 = data.loadBigEndian(fromByteOffset: byteOffset)
				return UInt16(clamping: Int((Double(value) * bscale + bzero).rounded()))
			
			case .INT64:
				let value: Int64 = data.loadBigEndian(fromByteOffset: byteOffset)
				return UInt16(clamping: Int((Double(value) * bscale + bzero).rounded()))
			
			case .FLOAT:
				let value: Float = data.loadBigEndian(fromByteOffset: byteOffset)
				return UInt16(clamping: Int((Double(value) * bscale + bzero).rounded()))
			
			case .DOUBLE:
				let value: Double = data.loadBigEndian(fromByteOffset: byteOffset)
				return UInt16(clamping: Int((value * bscale + bzero).rounded()))
		}
	}*/
	
	private static func getAxisSizes(from header: FitsHeader) -> [Int]? {
		var sizes = [Int]()
		if let numberOfAxis = header.getnAxis() {
			
			for i in stride(from: 0, to: numberOfAxis, by: 1) {
				if let val = header["NAXIS\(i+1)"]?.Value, let sz = Int(val) {
					sizes.append(sz)
				}
			}
			return sizes
		}
		return nil
	}
}

