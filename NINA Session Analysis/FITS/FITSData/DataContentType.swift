
//
//  DataContentType.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 02.07.24.
//

public enum DataContentType: Int32 {
	case DOUBLE = -64
	case FLOAT = -32
	case BYTE = 8
	case INT16 = 16
	case INT32 = 32
	case INT64 = 64
}
