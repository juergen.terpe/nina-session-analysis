//
//  ImageDataContent.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 02.07.24.
//
import Foundation

public class ImageDataContent<T: Numeric>: DataContent {
	private var axisIndexFactors: [Int]
	private var axisSizes: [Int]
	private var preCalcedAxisBounds: [Int]
	
	private var rawData: Data
	
	public init(rawData: Data, axisSizes: [Int]) {
		self.axisSizes = axisSizes
		self.rawData = rawData
	
		self.axisIndexFactors = Array(repeating: 0, count: self.axisSizes.count)
		self.preCalcedAxisBounds = Array(repeating: 0, count: self.axisSizes.count)
		self.axisIndexFactors[0] = 1
		self.preCalcedAxisBounds[0] = self.axisSizes[0]

		
		for i in stride(from: 1, to: self.axisIndexFactors.count, by: 1) {
			self.axisIndexFactors[i] = self.axisIndexFactors[i - 1] * self.axisSizes[i - 1]
			self.preCalcedAxisBounds[i] = self.preCalcedAxisBounds[i - 1] * self.axisSizes[i]
		}
	}
	
	
}
