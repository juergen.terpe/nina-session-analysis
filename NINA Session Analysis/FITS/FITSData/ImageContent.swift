//
//  ImageContent.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 08.07.24.
//

import CoreGraphics
import Accelerate

/*
public class ImageContent {
	private let width: Int
	private let height: Int
	public let cgImage: CGImage
	
	public init(width: Int, height: Int, cgImage: CGImage) {
		self.width = width
		self.height = height
		self.cgImage = cgImage
	}
	
	private func extractFloatPixels(from image: CGImage) -> [Float]? {
		let width = image.width
		let height = image.height
		let bytesPerPixel = image.bitsPerPixel / 8
		let bitsPerComponent = image.bitsPerComponent
		let bitmapInfo = image.bitmapInfo

		guard let colorSpace = image.colorSpace else { return nil }
		let bytesPerRow = bytesPerPixel * width

		// Erstelle einen Kontext, um die Pixeldaten zu extrahieren
		guard let context = CGContext(data: nil,
									  width: width,
									  height: height,
									  bitsPerComponent: bitsPerComponent,
									  bytesPerRow: bytesPerRow,
									  space: colorSpace,
									  bitmapInfo: bitmapInfo.rawValue) else {
			return nil
		}

		context.draw(image, in: CGRect(x: 0, y: 0, width: width, height: height))

		// Extrahiere die Pixeldaten
		guard let data = context.data else { return nil }

		// Konvertiere Pixeldaten zu float-Array
		let pixelCount = width * height
		var floatPixels = [Float](repeating: 0.0, count: pixelCount * bytesPerPixel / (bitsPerComponent / 8))

		if bitsPerComponent == 8 {
			let pixelBuffer = data.bindMemory(to: UInt8.self, capacity: pixelCount * bytesPerPixel)
			for i in 0..<floatPixels.count {
				floatPixels[i] = Float(pixelBuffer[i]) / 255.0
			}
		} else if bitsPerComponent == 16 {
			let pixelBuffer = data.bindMemory(to: UInt16.self, capacity: pixelCount * bytesPerPixel / 2)
			for i in 0..<floatPixels.count {
				floatPixels[i] = Float(pixelBuffer[i]) / 65535.0
			}
		} else if bitsPerComponent == 32 {
			let pixelBuffer = data.bindMemory(to: UInt32.self, capacity: pixelCount * bytesPerPixel / 4)
			for i in 0..<floatPixels.count {
				floatPixels[i] = Float(pixelBuffer[i]) / 4294967295.0
			}
		} else {
			return nil
		}

		return floatPixels
	}

}
*/


