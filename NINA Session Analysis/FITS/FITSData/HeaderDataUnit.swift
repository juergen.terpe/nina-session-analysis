//
//  HeaderDataUnit.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 02.07.24.
//

public enum HeaderDataUnitType: Int {
	/// <summary>
	/// Marks the primary HDU (the first one of the fits file)
	/// </summary>
	case PRIMARY = 0
	/// <summary>
	/// Marks the HDU to contain image or n-dimensional data
	/// </summary>
	case IMAGE = 1
	/// <summary>
	/// Marks the HDU to contain table data
	/// </summary>
	case TABLE = 2
}



public protocol DataContent
{
}
	

public class HeaderDataUnit<T> {
	public var Data: T
	public var Header: FitsHeader
	public var UnitType: HeaderDataUnitType

	public init(type: HeaderDataUnitType, header: FitsHeader, data: T) {
		self.Data = data
		self.UnitType = type
		self.Header = header
	}
}
 
