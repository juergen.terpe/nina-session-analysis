//
//  Header.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 02.07.24.
//
import Foundation

public class HeaderEntry
{
	public var Key: String
	public var Value: String?
	public var Comment: String?

	public init(key: String, value: String?, comment: String? = nil) {
		self.Key = key
		self.Value = value
		self.Comment = comment
	}
}



public class FitsHeader {
	
	private var entries: [HeaderEntry]
	private var offset: Int
	

	/// <summary>
	/// Initializes a header with the given entries
	/// </summary>
	/// <param name="entries">A list of entries used to initialize the
	/// header</param>
	public init(entries: [HeaderEntry], offset: Int) {
		self.entries = entries
		self.offset = offset
	}

	
	/// <summary>
	/// A list of entries contained within the header
	/// </summary>
	public var Entries: [HeaderEntry] {
		self.entries
	}

	public var Offset: Int {
		self.offset
	}
	
	/// <summary>
	/// Gets the value for the given header entry.
	/// Returns only first found entry if there are multiple with the same key
	/// </summary>
	/// <param name="key">The key for which to search in the header</param>
	public subscript(key: String) -> HeaderEntry? {
		self.entries.first(where: { $0.Key == key })
	}
	
	
	public func getDataContentType() -> DataContentType? {
		if let bitPix = entries.first(where: { $0.Key == "BITPIX" }), let val = bitPix.Value, let pixData = Int32(val) {
			return DataContentType(rawValue: pixData)
		}
		return nil
	}
	
	public func getBitDepth() -> Int {
		if let bitPix = entries.first(where: { $0.Key == "BITPIX" }), let val = bitPix.Value, let pixData = Int(val) {
			return pixData
		}
		return 16
	}
	
	public func getnAxis() -> Int32? {
		if let nAxisEntry = entries.first(where: { $0.Key == "NAXIS" }), let val = nAxisEntry.Value, let naxis = Int32(val) {
			return naxis
		}
		return nil
	}
	
}




public extension FitsHeader {
	
	internal func getBayerMatrix() -> BayerMatrix? {
		if let bayerPattern = self["BAYERPAT"]?.Value {
			var characterSet = CharacterSet.whitespacesAndNewlines
			characterSet.insert(charactersIn: "\"\'")
			
			let bayer = bayerPattern.trimmingCharacters(in: characterSet)
		
			switch bayer {
				case "RGGB": return BayerMatrix.rggb
				case "RGBG": return BayerMatrix.rgbg
				case "GRGB": return BayerMatrix.grgb
				case "GBRG": return BayerMatrix.gbrg
				case "GBGR": return BayerMatrix.gbgr
				case "BGRG": return BayerMatrix.bgrg
				case "GRBG": return BayerMatrix.grbg
				case "BGGR": return BayerMatrix.bggr
				default: return nil 
			}
		}
		return nil
	}
	
	
	static func read(from data: Data) -> FitsHeader {
		
		let HeaderEntryChunkSize = 80
		let paddingSize = 2880
		let endMarker = "END".data(using: .ascii)!
		var entries = [HeaderEntry]()
		
		var offset = data.startIndex
		while offset < data.count {
			
			let remainingBytes = data.count - offset
			if remainingBytes < HeaderEntryChunkSize {
				break
			}
			let chunkRange = offset..<offset + HeaderEntryChunkSize
			let chunk = data.subdata(in: chunkRange)
			
			if chunk.starts(with: endMarker) {
				offset += HeaderEntryChunkSize
				break
			}
			if let key = String(data: chunk[0..<8], encoding: .ascii)?.trimmingCharacters(in: .whitespaces) {
				let commentOrHistoryEntry = isCommentOrHistoryEntry(key)
				if headerEntryChunkHasValueMarker(chunk: chunk) || headerEntryEntryChunkHasContinueMarker(key) || commentOrHistoryEntry {
					if let value = String(data: chunk[10..<80], encoding: .ascii) {
						if let slashIndex = value.firstIndex(of: "/") {
							let index1 = value.index(slashIndex, offsetBy: 1)
							let comment = value[index1...].trimmingCharacters(in: .whitespaces)
							let val = value[value.startIndex..<slashIndex].trimmingCharacters(in: .whitespaces)
							entries.append(HeaderEntry(key: key, value: val, comment: comment))
						} else {
							let val = value.trimmingCharacters(in: .whitespaces)
							entries.append(HeaderEntry(key: key, value: val))
						}
					}
				}
			}
			
			offset += HeaderEntryChunkSize
		}
		
		if offset % paddingSize != 0 {
			offset += (paddingSize - (offset % paddingSize))
		}
		
		return FitsHeader(entries: entries, offset: offset)
	}
	
	private static func isCommentOrHistoryEntry(_ key: String) -> Bool {
		return key == "HISTORY" || key == "COMMENT"
	}

	private static func headerEntryEntryChunkHasContinueMarker(_ key: String) -> Bool {
		return key == "CONTINUE"
	}
	
	private static func headerEntryChunkHasValueMarker(chunk: Data) -> Bool	{
		return chunk[8] == 0x3D && chunk[9] == 0x20
	}
}
