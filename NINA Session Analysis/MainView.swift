//
//  MainView.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 29.03.24.
//

import SwiftUI
import PureSwiftUI

struct MainView: View {
	@State var menuDisabled: Bool = false
	@State private var menuItem = MenuItem.data
	
	@State var showCheckList: Bool = false
	@State var showSettings: Bool = false
	@State var toolsActive: Bool = true
	@Environment(SessionData.self) private var session
	
	@ViewBuilder
	func getContent1() -> some View {
		if menuItem == .data {
			InputDataView(session: session, showSettings: $showSettings, menuDisabled: $menuDisabled)
				.background(Color.controlBackgroundColor)
		} else if menuItem == .hints {
			HintsView(session: session, showSettings: $showSettings)
				.background(Color.controlBackgroundTransparent)
		} else if menuItem == .overview {
			VStack(spacing: 0) {
				ToolsPanel(menuItem: .overview, showCheckList: $showCheckList, showSettings: $showSettings,
						   toolsActive: $toolsActive, session: session)
				OverviewView(session: session, showCheckList: $showCheckList, showSettings: $showSettings,
							 toolsActive: $toolsActive)
					.background(Color.controlBackgroundColor)
			}
		} else if menuItem == .timingStatistics {
			TimeStatisticsView(session: session)
				.background(Color.controlBackgroundColor)
			
		} else if menuItem == .table {
			VStack(spacing: 0) {
				ToolsPanel(menuItem: .table, showCheckList: $showCheckList, showSettings: $showSettings,
						   toolsActive: $toolsActive, session: session)
				TableView(session: session, showCheckList: $showCheckList, showSettings: $showSettings)
					.background(Color.controlBackgroundColor)
			}
		}
	}
	
	@ViewBuilder
	func getContent2() -> some View {
		if menuItem == .dithering {
			VStack(spacing: 0) {
				ToolsPanel(menuItem: .dithering, showCheckList: $showCheckList, showSettings: $showSettings,
						   toolsActive: $toolsActive, session: session)
				DitheringView(session: session)
					.background(Color.controlBackgroundColor)
			}
		} else if menuItem == .guiding {
			if let guiding = session.selectedGuideSession {
				GuidingView(session: session, selectedGuidingSessionId: guiding)
					.background(Color.controlBackgroundTransparent)
			} else {
				EmptyView()
			}
		} else if menuItem == .ninaLog {
			VStack(spacing: 0) {
				NINALogViewer(session: session)
					.background(Color.controlBackgroundColor)
			}
		} else if menuItem == .sky {
			VStack(spacing: 0) {
				ToolsPanel(menuItem: .sky, showCheckList: $showCheckList, showSettings: $showSettings,
						   toolsActive: $toolsActive, session: session)
				SkyChartView(session: session)
					.background(Color.controlBackgroundColor)
			}
		} else if menuItem == .imageAnalysis {
			ImageAnalysisView(session: session)
				.background(Color.controlBackgroundColor)
		} else if menuItem == .stacking {
			ImageStackingView(session: session)
				.background(Color.controlBackgroundColor)
		}
	}
	
	
    var body: some View {
		
		HStack(spacing: 0) {
			MainMenu(menuItem: $menuItem, session: session, menuDisabled: $menuDisabled)
			
			Group {
				if menuItem == .data
					|| menuItem == .hints
					|| menuItem == .overview
					|| menuItem == .timingStatistics
					|| menuItem == .table {
					getContent1()
				}
				else {
					getContent2()
				}
			}
			
		}
		.frame(minWidth: 1280, minHeight: 680)
		
    }
}

#Preview {
    MainView()
}
