//
//  SessionData.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 29.03.24.
//

import Foundation
import SwiftUI
import SwiftCSV
import SwiftyJSON
import CoreLocation
import ImageIO
import CoreImage


struct FoundLights {
	var images: [SessionImage]
	var targets: [String]
	var filters: [String]
	var pierSides: [String]
	var exposures: [String]
	var sessionDates: [DateOfSession]
}

enum LogEntryType: String  {
	case info = "Info"
	case note = "Note"
	case warn = "Warn"
	case error = "Error"
}

struct LogEntry: Equatable, Identifiable {
	let id: UUID = UUID()
	var text: LocalizedStringKey
	var type: LogEntryType = .info
}

extension Date {
	
	var midnight: Date {
		let cal = Calendar(identifier: .gregorian)
		let midnight = cal.startOfDay(for: self)
		let secondsFromGMT = TimeZone.current.secondsFromGMT()
		return midnight.addingTimeInterval(Double(secondsFromGMT))
	}
	
	var midDay: Date {
		return self.midnight.addingTimeInterval(Double(43200))
	}
}

public struct DateOfSession : Hashable {
	let text: String
	let date: Date
	
	public func hash(into hasher: inout Hasher) {
		hasher.combine(self.text)
	}
}

public struct AutoFocuserPoint {
	let position: Int
	let HFR: Float
	let mad: Float
	let stars: Int 
}

public struct AutoFocuserImage: Identifiable {
	public let id: UUID
	let filter: String
	let exposure: Float
	let hfr: Float
	let mean: Float
}

public struct AutoFocus: Identifiable, Equatable {
	
	
	public let id: UUID = UUID()
	let start: Date
	let end: Date
	let initialPosition: Int
	let initialHFR: Float
	let finalPosition: Int 
	let finalHFR: Float
	let points: [AutoFocuserPoint]
	let previousImages: [AutoFocuserImage]
	let nextImages: [AutoFocuserImage]
	
	func isSession(_ sessionDate: DateOfSession) -> Bool {
		if sessionDate.text == "Any" {
			return true
		} else {
			let sessionMidDay = sessionDate.date.midDay
			let nextMidDay = sessionMidDay.addingTimeInterval(86400)
			
			return self.start > sessionMidDay && self.start < nextMidDay 
		}
	}
	
	var duration: Double {
		abs(self.start.distance(to: self.end))
	}
	
	func inRange(beginTime: Date, endTime: Date, timeZone: TimeZone) -> Bool {
		let time = self.start.addingTimeInterval(TimeInterval(timeZone.secondsFromGMT(for: self.start)))
		return beginTime.timeIntervalSinceReferenceDate < time.timeIntervalSinceReferenceDate
			&& time.timeIntervalSinceReferenceDate < endTime.timeIntervalSinceReferenceDate
	}
	
	public static func == (lhs: AutoFocus, rhs: AutoFocus) -> Bool {
		return lhs.id == rhs.id
	}
}


@Observable class SessionData: Identifiable {
	var id : UUID = UUID() 
	var selectedGuideSession: UUID? 

	var missingDataImages: [SessionImage] = []
	var sessionImages: [SessionImage] = []
	var sessionDates: [DateOfSession] = [] {
		didSet {
			self.cachedFilteredImages = nil
			DispatchQueue.main.async {
				self.id = UUID()
			}
		}
		
	}
	var selectedSessionDate: String = "Any" {
		didSet {
			self.cachedFilteredImages = nil
			DispatchQueue.main.async {
				self.id = UUID()
			}
		}
	}
	
	var selectedSession: DateOfSession? {
		let sessionDate = self.selectedSessionDate
		if sessionDate != "Any" {
			return sessionDates.first(where: { $0.text == sessionDate })
		}
		return nil
	}
	
	var filters: [String] = ["Any"]
	var selectedFilter: String = "" {
		didSet {
			self.cachedFilteredImages = nil
			DispatchQueue.main.async {
				self.id = UUID()
			}
		}
	}
	var targets: [String] = ["Any"]
	var selectedTarget: String = "" {
		didSet {
			self.cachedFilteredImages = nil
			DispatchQueue.main.async {
				self.id = UUID()
			}
		}
	}
	var pierSides: [String] = ["Any"]
	var selectedPierSide: String = "Any" {
		didSet {
			self.cachedFilteredImages = nil
			DispatchQueue.main.async {
				self.id = UUID()
			}
		}
	}
	var exposures: [String] = ["Any"]
	var selectedExposure: String = "Any" {
		didSet {
			self.cachedFilteredImages = nil
			DispatchQueue.main.async {
				self.id = UUID()
			}
		}
	}
	
	
	var guiding: GuidingSessionData = GuidingSessionData()
	var metaDataAvailable: Bool = false {
		didSet {
			DispatchQueue.main.async {
				self.id = UUID()
			}
		}
	}
		
	var guideLogAvailable: Bool = false {
		didSet {
			DispatchQueue.main.async {
				self.id = UUID()
			}
		}
	}
	
	var NINALogAvailable: Bool = false {
		didSet {
			DispatchQueue.main.async {
				self.id = UUID()
			}
		}
	}
	
	var analyzeAvailable: Bool = false {
		didSet {
			DispatchQueue.main.async {
				self.id = UUID()
			}
		}
	}
	
	var selectedTimeZone: String = TimeZone.current.identifier {
		didSet {
			self.guiding.timeZone = currentTimeZone
		}
	}
	
	var currentTimeZone: TimeZone {
		return TimeZone(identifier: selectedTimeZone)!
	}
	
	var filterColors: [FilterData] = []
	
	var autoFocuserRuns: [AutoFocus] = []
	
	var ninaLogs: NINALogs = NINALogs()
	
	var tableColumns: [TableImageColumn] = []
	
	var logging: [LogEntry] = []
	
	var analyzationSettings: AnalyzeImageSettings = AnalyzeImageSettings()
	
	var quickFitsAvailable: Bool = false
	var pixInsightAvailable: Bool = false
	var astapAvailable: Bool = false
	var fitsPreviewAvailable: Bool = false 
	
	var readingData: Bool = false 
	var cachedFilteredImages: [SessionImage]? = nil

	var sortingOrder: [KeyPathComparator<SessionImage>] = [KeyPathComparator(\SessionImage.index, order: .forward)]
	
	var filteredImages: [SessionImage] {
			
		if let cached = self.cachedFilteredImages {
			return cached
		}
		
		var images = self.sessionImages.filter({ $0.startExposureUTC != nil })
		
		if selectedSessionDate != "Any" {
			images = images.filter({ $0.SessionDate?.text == selectedSessionDate })
		}
		
		if selectedFilter != "Any"  {
			images = images.filter({ $0.filter == selectedFilter })
		}
		
		if selectedTarget != "Any" {
			images = images.filter({ $0.target == selectedTarget })
		}
		
		if selectedPierSide != "Any" {
			images = images.filter({ $0.pierSide == selectedPierSide })
		}
		
		if selectedExposure != "Any" {
			images = images.filter({  $0.Exposure == selectedExposure })
		}
		
		images.sort(using: sortingOrder)
		
		self.cachedFilteredImages = images
		
		return images
			
	}
	
	public func clearCachedImages() {
		self.cachedFilteredImages = nil
	}
	
	public func setup() {
		for columnId in TableImageColumnId.allCases {
			self.tableColumns.append(TableImageColumn(item: columnId, element: columnId.element, enabled: true))
		}
		
		if let _ = NSWorkspace.shared.urlForApplication(withBundleIdentifier: "CYo.QuickFits") {
			self.quickFitsAvailable = true
		}
		
		if let _ = NSWorkspace.shared.urlForApplication(withBundleIdentifier: "software.pleiades.PixInsight") {
			self.pixInsightAvailable = true
		}
		
		if let _ = NSWorkspace.shared.urlForApplication(withBundleIdentifier: "com.company.astap") {
			self.astapAvailable = true
		}
		
		if let _ = NSWorkspace.shared.urlForApplication(withBundleIdentifier: "eu.cloudmakers.FITS-Preview") {
			self.fitsPreviewAvailable = true
		}
		
		self.analyzationSettings.readSettings()
	}
	
	
	public func openInQuickFits(_ image: SessionImage) {
		
		if let appURL = NSWorkspace.shared.urlForApplication(withBundleIdentifier: "CYo.QuickFits") {
			
			let config = NSWorkspace.OpenConfiguration()
			config.activates = true
			config.createsNewApplicationInstance = true
			config.promptsUserIfNeeded = true
			
			NSWorkspace.shared.open([image.url], withApplicationAt: appURL, configuration: config, completionHandler: nil)
		}
	}
	
	public func openInFitsPreview(_ image: SessionImage) {
		
		if let appURL = NSWorkspace.shared.urlForApplication(withBundleIdentifier: "eu.cloudmakers.FITS-Preview") {
			
			let config = NSWorkspace.OpenConfiguration()
			config.activates = true
			config.createsNewApplicationInstance = true
			config.promptsUserIfNeeded = true
			
			NSWorkspace.shared.open([image.url], withApplicationAt: appURL, configuration: config, completionHandler: nil)
		}
	}
	
	public func openInPixInsight(_ image: SessionImage) {
		
		if let appURL = NSWorkspace.shared.urlForApplication(withBundleIdentifier: "software.pleiades.PixInsight") {
			
			let config = NSWorkspace.OpenConfiguration()
			config.activates = true
			config.createsNewApplicationInstance = true
			config.promptsUserIfNeeded = true
			
			NSWorkspace.shared.open([image.url], withApplicationAt: appURL, configuration: config, completionHandler: nil)
		}
	}
	
	public func openInAstap(_ image: SessionImage) {
		
		if let appURL = NSWorkspace.shared.urlForApplication(withBundleIdentifier: "com.company.astap") {
			
			let config = NSWorkspace.OpenConfiguration()
			config.activates = true
			config.createsNewApplicationInstance = true
			config.promptsUserIfNeeded = true
			
			NSWorkspace.shared.open([image.url], withApplicationAt: appURL, configuration: config, completionHandler: nil)
		}
	}
	
	public func getFilterColor(_ filter: String) -> Color? {
		if let fc = self.filterColors.first(where: { $0.filterName == filter}) {
			return fc.color
		}
		return nil 
	}
	
	
	public func onContentUpdated() {
		self.clearCachedImages()
		let images = self.sessionImages.sorted(using: self.sortingOrder)
		self.sessionImages.removeAll()
		self.sessionImages = images
		self.id = UUID()
	}
	
	public func getImages(for sessionId: GuidingData.ID) -> [SessionImage] {
		if let guiding = self.guiding.guidingSessions.first(where: { $0.id == sessionId}) {
			let start = guiding.guidingStart
			let end = guiding.guidingEnd
			
			var result = [SessionImage]()
			for image in self.sessionImages {
				if let imageBegin = image.begin, let imageEnd = image.end {
					if (imageEnd.jd >= start.jd  && imageBegin.jd <= end.jd) || (imageBegin.jd < end.jd && imageEnd.jd >= start.jd) {
						result.append(image)
					}
				}
			}
			return result 
		}
		return []
	}
	
	public func getImages(for session: String) -> [SessionImage] {
		return self.sessionImages.filter({ $0.SessionDate?.text == session })
	}
	
	public func getImages(for session: String, target: String) -> [SessionImage] {
		if session == "Any" && target == "Any" {
			return self.sessionImages.filter({ $0.SessionDate != nil && $0.exposure != nil })
		} else if session == "Any" {
			return self.sessionImages.filter({ $0.SessionDate != nil && $0.exposure != nil && $0.target == target})
		} else if target == "Any" {
			return self.sessionImages.filter({ $0.SessionDate != nil && $0.exposure != nil
											&& $0.SessionDate?.text == session })
		}
		
		return self.sessionImages.filter({ $0.SessionDate != nil && $0.exposure != nil
											&& $0.SessionDate?.text == session && $0.target == target})
	}
	
	public func getTimeRange(for session: String) -> (begin: Date, end: Date)? {
		let images = getImages(for: session)
		var minDate: Date? = nil
		var maxDate: Date? = nil
		
		for image in images {
			if let start = image.startExposureUTC, let exp = image.exposure {
				let end = start.addingTimeInterval(TimeInterval(exp))
				if minDate == nil {
					minDate = start
				} else if minDate!.timeIntervalSinceReferenceDate > start.timeIntervalSinceReferenceDate {
					minDate = start
				}
				if maxDate == nil {
					maxDate = end
				} else if maxDate!.timeIntervalSinceReferenceDate < end.timeIntervalSinceReferenceDate {
					maxDate = end
				}
			}
			
		}
		if minDate != nil && maxDate != nil {
			return (begin: minDate!, end: maxDate!)
		}
		return nil
	}
	
	
	
	public func getImages(for target: String, exposure: String, filter: String) -> [SessionImage] {
		self.sessionImages.filter({ $0.target == target && $0.Exposure == exposure && $0.filter == filter})
	}
	
	
	private func getDateFormatter() -> DateFormatter {
		let dateFormatter = DateFormatter()
		dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
		dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
		dateFormatter.timeZone = self.currentTimeZone //TimeZone(abbreviation: "UTC")
		return dateFormatter
	}
	
	public func resetAll() {
		self.guiding.clear()
		
		self.filterColors = []
		self.metaDataAvailable = false
		self.guideLogAvailable = false
		self.analyzeAvailable = false
		self.NINALogAvailable = false
		self.ninaLogs.clear()
		
		self.filters.removeAll()
		self.targets.removeAll()
		
		self.pierSides.removeAll()
		self.exposures.removeAll()
		self.sessionDates.removeAll()
		self.sessionImages.removeAll()
		self.logging.removeAll()
		self.id = UUID()
		self.selectedSessionDate = "Any"
		self.selectedFilter = "Any"
		self.selectedTarget = "Any"
		self.selectedPierSide = "Any"
		self.selectedExposure = "Any"
	}
	
	
	public func getSurveyRegion() -> SurveyViewRegion? {
		if let first = sessionImages.first(where: { $0.sizeX != nil && $0.sizeY != nil && $0.pixSize != nil
													&& $0.focalLength != nil && $0.ra != nil && $0.dec != nil}) {
			
			if let sx = first.sizeX, let sy = first.sizeY, let px = first.pixSize, let focalLength = first.focalLength,
			   let ra = first.ra, let dec = first.dec {
				let imageRes = Double(px)/Double(focalLength) * 206.265 // in arcsecs
				let resDeg = imageRes/3600.0
				
				let xx = Double(sx) * resDeg
				let yy = Double(sy) * resDeg
				
				let size = 2.5 * max(xx, yy) * 3600.0
				let fov = Double(size)/3600.0
				
				return SurveyViewRegion(ra: ra, dec: dec, size: size, target: first.target, fov: fov)
			}
		}
		return nil
	}
	
	public func getImageByIndex(_ index: Int) -> SessionImage? {
		let images = self.filteredImages
		if images.count == 0 {
			return nil
		}
		
		let idx = index % images.count
		return images[idx]
	}
	
	public func getImageById(_ id: UUID?) -> SessionImage? {
		if id == nil || sessionImages.count == 0 {
			return nil
		}
		
		return self.sessionImages.first(where: { $0.id == id })
	}
	
	public func getSessionDates() -> [JulianDate] {
		if sessionImages.count == 0 {
			return []
		}
		
		var dates = [JulianDate]()
		for image in sessionImages.filter({ $0.startExposureUTC != nil && $0.startExposure != nil}) {
			if let start = image.startExposure {
				if dates.count == 0 {
					let jdd = JulianDate.fromDate(date: start)
					let begin = jdd.getBeginOfDay()
					let midDay = begin.addHours(12.0)
					dates.append(midDay)
				}
				else {
					let jdd = JulianDate.fromDate(date: start)
					if abs(jdd.jd - dates.last!.jd) >= 1.0 {
						let begin = jdd.getBeginOfDay()
						let midDay = begin.addHours(12.0)
						dates.append(midDay)
					}
				}
			}
			
		}
		
		return dates.sorted { jd1, jd2 in
			jd1.jd < jd2.jd
		}
	}
	
	public func getPixelSize() -> Float? {
		if let first = sessionImages.first {
			return first.pixSize
		}
		return nil
	}
	
	public func isColorCamera() -> Bool {
		if let first = sessionImages.first, !first.bayerPattern.isEmpty {
			return true
		}
		return false 
	}
	
	public func hasDriftData() -> Bool {
		if self.sessionImages.count == 0 {
			return false
		}
		return self.sessionImages.contains(where: { $0.dither != nil && $0.ra != nil && $0.dec != nil
												 && $0.sizeX != nil && $0.sizeY != nil })
	}
	
	public func getPixelScale() -> Float? {
		if let first = sessionImages.first, let px = first.pixSize, let focalLength = first.focalLength {
			return px/focalLength * 206.265 // in arcsecs
		}
		
		return nil
	}
	
	public func getImageSize() -> CGSize? {
		if let first = sessionImages.first, let sx = first.sizeX, let sy = first.sizeY {
			return CGSize(width: sx, height: sy)
		}
		return nil
	}
	
	
	public func getCenter() -> Vector3? {
		if let first = sessionImages.first(where: { $0.ra != nil && $0.dec != nil}), let ra = first.ra, let dec = first.dec {
			return Vector3.from(ra: ra*Constants.Rad, dec: dec*Constants.Rad)
		}
		return nil
	}
	
	public func beginJulianDate() -> JulianDate? {
		if let first = sessionImages.first, let start = first.startExposure {
			return JulianDate.fromDate(date: start)
		}
		return nil
	}
	
	public func endJulianDate() -> JulianDate? {
		if let last = sessionImages.last, let start = last.startExposure {
			return JulianDate.fromDate(date: start)
		}
		return nil
	}
	
	public func readGuideLog(_ guidingFile: URL) async {
		await self.guiding.read(guidingFile, timeZone: self.currentTimeZone)
		
		self.selectedGuideSession = self.guiding.getGuidingSessions(onlyImages: true, minDuration: 0).first?.id
		self.guideLogAvailable = self.guiding.count > 0
	}
	
	public func findNINALogs(_ logFileFolder: URL, update: Bool) async {
		let dates = self.sessionImages.filter({ $0.startExposure != nil }).compactMap { $0.SessionDate }.distinct()
		
		await self.ninaLogs.scan(logFileFolder, dates: dates, timeZone: self.currentTimeZone) { fname in
			// Hier wird das onLoaded Closure aufgerufen, das synchron sein muss
			self.logging.append(LogEntry(text: LocalizedStringKey("reading file \(fname)")))
		}
		
		let autoFocusRuns = await self.ninaLogs.analyzeAutoFocusRuns(with: self)
		
		await MainActor.run {
			self.autoFocuserRuns = autoFocusRuns
			if update {
				self.NINALogAvailable = self.ninaLogs.count > 0
			}
		}
	}

	public func getPreviousImages(for jd: JulianDate, limit n: Int = 1) -> [SessionImage] {
		return Array(self.sessionImages.filter({ $0.end != nil && $0.end! < jd }).suffix(n))
	}

	public func getNextImages(for jd: JulianDate, limit n: Int = 1) -> [SessionImage] {
		return Array(self.sessionImages.filter({ $0.begin != nil && $0.begin! > jd }).prefix(n))
	}
	
	public func getSelectedSessionJulianDate() -> JulianDate? {
		if let image = self.sessionImages.filter({ $0.startExposureUTC != nil && $0.SessionDate?.text == selectedSessionDate }).first {
			return image.begin
		}
		return nil 
	}
	
	public func getSelectedSessionEndJulianDate() -> JulianDate? {
		if let image = self.sessionImages.filter({ $0.startExposureUTC != nil && $0.SessionDate?.text == selectedSessionDate }).last {
			return image.end
		}
		return nil
	}
	
	
	public func getLocation(for jd: JulianDate) -> CLLocation? {
		if self.sessionImages.count == 0  {
			return nil
		}
		
		let filtered = self.sessionImages.filter({ $0.begin != nil })
		if filtered.count == 0 {
			return nil
		}
		
		if let first = filtered.sorted(by: { abs($0.begin!.jd - jd.jd) < abs($1.begin!.jd - jd.jd) }).first,
		   let long = first.obsLong, let lat = first.obsLat {
			return CLLocation(latitude: lat, longitude: long)
		}
		return nil
	}
	
	public func getFrames(between jd1: JulianDate, and jd2: JulianDate) -> [GuidingFrame] {
		return self.guiding.getFrames(between: jd1, and: jd2)
	}
	
	public func getGuideGraphData(for image: SessionImage) async -> [GuideGraphData] {
		var data = [GuideGraphData]()
		
		if let jd1 = image.begin, let jd2 = image.end {
			let frames = getFrames(between: jd1, and: jd2)
			let (raRate, decRate) = self.guiding.getGuidingRates(between: jd1, and: jd2)
			
			if frames.count > 0 {
				for frame in frames {
					let t = (frame.time - jd1.jd) * 86400.0
					
					if frame.drop {
						data.append(GuideGraphData(id: frame.id, time: Float(t), ra: 0, dec: 0,
												   pulseRA: 0, pulseDEC: 0, drop: true,
												   error: frame.errorCode,
												   exposure: frame.exposure/1000.0))
					} else {
						let ra = frame.raRawDistance
						let dec = frame.decRawDistance
						
						var raDir: Float = 1.0
						if frame.raDirection == "W" {
							raDir = -1.0
						}
						let pulseRA = frame.raDuration * raDir * raRate/1000.0
						
						var decDir: Float = 1.0
						if frame.decDirection == "S" {
							decDir = -1.0
						}
						let pulseDEC = frame.decDuration * decDir * decRate/1000.0
						
						data.append(GuideGraphData(id: frame.id, time: Float(t), ra: ra, dec: dec,
												   pulseRA: pulseRA, pulseDEC: pulseDEC, snr: frame.snr,
												   error: frame.errorCode,
												   exposure: frame.exposure/1000.0))
					}
				}
			}
		}
		return data
	}
	
	private func parseRA(_ text: String?) -> Double? {
		guard text != nil && text!.count > 0 else { return nil }
		let regEx = /(?<h>.+?)h (?<m>.+?)m (?<s>.+?)s/
		
		if let matches = try? regEx.wholeMatch(in: text!) {
			if let hh = Double(matches.h),
			   let mm = Double(matches.m),
			   let sec = Double(matches.s) {
				
				return 15.0 * (hh + mm/60.0 + sec/3600.0)
			}
		}
		return nil
	}
	
	private func parseDEC(_ text: String?) -> Double? {
		guard text != nil && text!.count > 0 else { return nil }
		let regEx = /(-?\d+)° (\d+)' (\d+)"/
		
		if let match = text!.firstMatch(of: regEx) {
			let degrees = match.1
			let minutes = match.2
			let seconds = match.3
				
			if let deg = Double(degrees),
			   let min = Double(minutes),
			   let secs = Double(seconds) {
					
				var dec = (abs(deg) + min/60.0 + secs/3600.0)
				if deg < 0.0 {
					dec = -dec
				}
				return dec
				
			}
		}
		return nil
	}
	
	
	public func updateAcquisitionData(_ acquisitionData: URL, update: Bool) {
		guard self.sessionImages.count > 0 else { return }
		
		do {
			let csvFile: CSV = try CSV<Named>(url: acquisitionData, delimiter: CSVDelimiter.comma)
			// TargetName,RACoordinates,DECCoordinates,TelescopeName,FocalLength,FocalRatio,CameraName,PixelSize,BitDepth,ObserverLatitude,ObserverLongitude,ObserverElevation

			for row in csvFile.rows {
				
				if let target = row["TargetName"] {
					for image in self.sessionImages {
						image.target = target
					}
				}
				
				if let ra = parseRA(row["RACoordinates"]), let dec = parseDEC(row["DECCoordinates"]) {
					for image in self.sessionImages {
						image.targetRA = ra
						image.targetDEC = dec
					}
				}
				
				if let telescope = row["TelescopeName"] {
					for image in self.sessionImages {
						image.telescope = telescope
					}
				}
				
				if let focalLength = parseFloat(row["FocalLength"]) {
					for image in self.sessionImages {
						image.focalLength = focalLength
					}
				}
				
				if let camera = row["CameraName"] {
					for image in self.sessionImages {
						image.camera = camera
					}
				}
				
				if let pxSize = parseFloat(row["PixelSize"]) {
					for image in self.sessionImages {
						image.pixSize = pxSize
					}
				}
				
				if let obsLat = parseFloat(row["ObserverLatitude"]) {
					for image in self.sessionImages {
						image.obsLat = Double(obsLat)
					}
				}
				
				if let obsLong = parseFloat(row["ObserverLongitude"]) {
					for image in self.sessionImages {
						image.obsLong = Double(obsLong)
					}
				}
			}
			
		}
		catch {
			print("Error: \(error.localizedDescription)")
		}
	}
	
	public func updateImageMetadata(_ imageMetadata: URL) async -> Bool {
		if self.sessionImages.count > 0 {
			
			do {
				let csvFile: CSV = try CSV<Named>(url: imageMetadata, delimiter: CSVDelimiter.comma)
				var hasMetaData = false
				
				if self.sessionImages.allSatisfy({ $0.isFits }) {
					
					for row in csvFile.rows {
						let index = row["ExposureNumber"] ?? ""
						let filepath = row["FilePath"] ?? ""
						let mean = row["ADUMean"] ?? ""
						let median = row["ADUMedian"] ?? ""
						let aduMin = row["ADUMin"] ?? ""
						let aduMax = row["ADUMax"] ?? ""
						let numStars = row["DetectedStars"] ?? ""
						let hfr = row["HFR"] ?? ""
						let hfrStDev = row["HFRStDev"] ?? ""
						let fwhm = row["FWHM"] ?? ""
						let ecc = row["Eccentricity"] ?? ""
						let rms = row["GuidingRMSArcSec"] ?? ""
						let rmsRA = row["GuidingRMSRAArcSec"] ?? ""
						let rmsDEC = row["GuidingRMSDECArcSec"] ?? ""
						
						let metadata = ImageMetadata(index: index, filePath: filepath, mean: mean, median: median, aduMin: aduMin, aduMax: aduMax,
													 numStars: numStars, hfr: hfr, hfrStdDev: hfrStDev, fwhm: fwhm, eccentricity: ecc,
													 rms: rms, rmsRA: rmsRA, rmsDEC: rmsDEC)
						if assign(metadata) {
							hasMetaData = true
						}
					}
					
				} else {
					// non-fits images
					for row in csvFile.rows {
						//ExposureNumber,FilePath,FilterName,ExposureStart,Duration,Binning,CameraTemp,CameraTargetTemp,
						//Gain,Offset,ADUStDev,ADUMean,ADUMedian,ADUMin,ADUMax,DetectedStars,HFR,HFRStDev,FWHM,Eccentricity,
						//GuidingRMS,GuidingRMSArcSec,GuidingRMSRA,GuidingRMSRAArcSec,GuidingRMSDEC,GuidingRMSDECArcSec,
						//FocuserPosition,FocuserTemp,RotatorPosition,PierSide,Airmass, ExposureStartUTC, MountRA, MountDec
						
						if let path = row["FilePath"] {
							let filename = URL(fileURLWithPath: path).lastPathComponent
							if let image = self.sessionImages.first(where: { $0.FileName.contains(filename)}) {
								
								image.index = Int(row["ExposureNumber"] ?? "0") ?? 0
								image.filter = row["FilterName"] ?? ""
								
								if let exposureStartUTC = parseDateUTC(row["ExposureStartUTC"] ?? "") {
									image.startExposureUTC = exposureStartUTC
									let utcOffset = TimeInterval(self.currentTimeZone.secondsFromGMT(for: exposureStartUTC))
									
									image.startExposure = exposureStartUTC.addingTimeInterval(utcOffset)
									
									if let date = image.startExposureUTC, let long = image.obsLong, let lat = image.obsLat {
										let jd = JulianDate.fromDate(date: date)
										let (azSun, altSun) = Sun.getAltAz(jd, long: long, lat: lat)
										let (azMoon, altMoon) = Moon.getAltAz(jd, long: long, lat: lat)
										
										image.sunAlt = Float(altSun*Constants.Deg)
										image.moonAlt = Float(altMoon*Constants.Deg)
										image.sunAz = Float(azSun*Constants.Deg)
										image.moonAz = Float(azMoon*Constants.Deg)
										
										if let targetRA = image.targetRA, let targetDEC = image.targetDEC {
											let targetPos = Vector3.from(ra: targetRA*Constants.Rad, dec: targetDEC*Constants.Rad)
											let moonPos = Moon.getPosition(jd.t)
											image.moonAngle = Float(angle(targetPos, moonPos).toDegrees())
											
											let sunPos = Sun.getPosition(jd.t)
											image.sunAngle = Float(angle(targetPos, sunPos).toDegrees())
											
											let (azimuth, altitude) = getAltAz(pos: targetPos, jd: jd, long: long, lat: lat)
											image.altitude = Float(altitude * Constants.Deg)
											image.azimuth = Float(azimuth * Constants.Deg)
										}
										
										let jdSession = frac(jd.jd) < 0.5 ? jd.getBeginOfDay().addHours(-12.0) : jd.getBeginOfDay().addHours(12.0)
										image.sessionDate = jdSession.toDate()
									}
									else if let date = image.startExposureUTC {
										image.sessionDate = JulianDate.fromDate(date: date).highNoon().toDate()
									}
									
								}
								
								if let exposure = Float(row["Duration"] ?? "") {
									image.exposure = exposure
								}
								
								image.cameraTemp = parseFloat(row["CameraTemp"])
								image.gain = parseInt(row["Gain"])
								image.offset = parseInt(row["Offset"])
								if image.offset == nil || image.offset! < 0 {
									image.offset = 0
								}
								
								image.mean = parseFloat(row["ADUMean"])
								image.median = parseInt(row["ADUMedian"])
								image.aduMin = parseInt(row["ADUMin"])
								image.aduMax = parseInt(row["ADUMax"])
								image.numStars = parseInt(row["DetectedStars"])
								if image.numStars == 0 {
									image.bad = true
									image.addBadMarking()
								}
								
								if let hfrValue =  parseFloat(row["HFR"]), hfrValue > 0.0 {
									image.hfr = hfrValue
									image.hfrStdev = parseFloat(row["HFRStDev"])
								}
								if let fwhmValue =  parseFloat(row["FWHM"]), fwhmValue > 0.0 {
									image.fwhm = fwhmValue
								}
								
								if image.hfr != nil || image.fwhm != nil {
									image.eccentricity = parseFloat(row["Eccentricity"])
								}
								
								image.rms = parseFloat(row["GuidingRMSArcSec"])
								image.rmsRA = parseFloat(row["GuidingRMSRAArcSec"])
								image.rmsDEC = parseFloat(row["GuidingRMSDECArcSec"])
								image.focusPosition = parseFloat(row["FocuserPosition"])
								image.focusTemp = parseFloat(row["FocuserTemp"])
								image.rotatorAngle = parseFloat(row["RotatorPosition"])
								image.pierSide = row["PierSide"] ?? ""
								image.airmass = parseFloat(row["Airmass"])
								image.ra = parseDouble(row["MountRA"])
								image.dec = parseDouble(row["MountDec"])
								image.metaData = .partial
								
								hasMetaData = true
							}
						}
						
					}
					
				}
				
				await checkImageData()
				return hasMetaData
			}
			catch {
				print("Error: \(error.localizedDescription)")
				return false
			}
		}
		else {
			
			var hasMetaData = false
			
			do {
				
				var images = [SessionImage]()
				
				let csvFile: CSV = try CSV<Named>(url: imageMetadata, delimiter: CSVDelimiter.comma)
				
				for row in csvFile.rows {
					//ExposureNumber,FilePath,FilterName,ExposureStart,Duration,Binning,CameraTemp,CameraTargetTemp,
					//Gain,Offset,ADUStDev,ADUMean,ADUMedian,ADUMin,ADUMax,DetectedStars,HFR,HFRStDev,FWHM,Eccentricity,
					//GuidingRMS,GuidingRMSArcSec,GuidingRMSRA,GuidingRMSRAArcSec,GuidingRMSDEC,GuidingRMSDECArcSec,
					//FocuserPosition,FocuserTemp,RotatorPosition,PierSide,Airmass, ExposureStartUTC, MountRA, MountDec
					
					if let path = row["FilePath"] {
						let filename = URL(fileURLWithPath: path)
						let image = SessionImage(url: filename)
						
						image.index = Int(row["ExposureNumber"] ?? "0") ?? 0
						image.filter = row["FilterName"] ?? ""
						
						if let exposureStartUTC = parseDateUTC(row["ExposureStartUTC"] ?? "") {
							image.startExposureUTC = exposureStartUTC
							let utcOffset = TimeInterval(self.currentTimeZone.secondsFromGMT(for: exposureStartUTC))
							
							image.startExposure = exposureStartUTC.addingTimeInterval(utcOffset)
							
							if let date = image.startExposureUTC, let long = image.obsLong, let lat = image.obsLat {
								let jd = JulianDate.fromDate(date: date)
								let (azSun, altSun) = Sun.getAltAz(jd, long: long, lat: lat)
								let (azMoon, altMoon) = Moon.getAltAz(jd, long: long, lat: lat)
								
								image.sunAlt = Float(altSun*Constants.Deg)
								image.moonAlt = Float(altMoon*Constants.Deg)
								image.sunAz = Float(azSun*Constants.Deg)
								image.moonAz = Float(azMoon*Constants.Deg)
								
								if let targetRA = image.targetRA, let targetDEC = image.targetDEC {
									let targetPos = Vector3.from(ra: targetRA*Constants.Rad, dec: targetDEC*Constants.Rad)
									let moonPos = Moon.getPosition(jd.t)
									image.moonAngle = Float(angle(targetPos, moonPos).toDegrees())
									
									let sunPos = Sun.getPosition(jd.t)
									image.sunAngle = Float(angle(targetPos, sunPos).toDegrees())
									
									let (azimuth, altitude) = getAltAz(pos: targetPos, jd: jd, long: long, lat: lat)
									image.altitude = Float(altitude * Constants.Deg)
									image.azimuth = Float(azimuth * Constants.Deg)
								}
								
								let jdSession = frac(jd.jd) < 0.5 ? jd.getBeginOfDay().addHours(-12.0) : jd.getBeginOfDay().addHours(12.0)
								image.sessionDate = jdSession.toDate()
							}
							else if let date = image.startExposureUTC {
								image.sessionDate = JulianDate.fromDate(date: date).highNoon().toDate()
							}
							
						}
						
						if let exposure = Float(row["Duration"] ?? "") {
							image.exposure = exposure
						}
						
						image.cameraTemp = parseFloat(row["CameraTemp"])
						image.gain = parseInt(row["Gain"])
						image.offset = parseInt(row["Offset"])
						if image.offset == nil || image.offset! < 0 {
							image.offset = 0
						}
						
						image.mean = parseFloat(row["ADUMean"])
						image.median = parseInt(row["ADUMedian"])
						image.aduMin = parseInt(row["ADUMin"])
						image.aduMax = parseInt(row["ADUMax"])
						image.numStars = parseInt(row["DetectedStars"])
						if image.numStars == 0 {
							image.bad = true
							image.addBadMarking()
						}
						
						if let hfrValue =  parseFloat(row["HFR"]), hfrValue > 0.0 {
							image.hfr = hfrValue
							image.hfrStdev = parseFloat(row["HFRStDev"])
						}
						if let fwhmValue =  parseFloat(row["FWHM"]), fwhmValue > 0.0 {
							image.fwhm = fwhmValue
						}
						
						if image.hfr != nil || image.fwhm != nil {
							image.eccentricity = parseFloat(row["Eccentricity"])
						}
						
						image.rms = parseFloat(row["GuidingRMSArcSec"])
						image.rmsRA = parseFloat(row["GuidingRMSRAArcSec"])
						image.rmsDEC = parseFloat(row["GuidingRMSDECArcSec"])
						image.focusPosition = parseFloat(row["FocuserPosition"])
						image.focusTemp = parseFloat(row["FocuserTemp"])
						image.rotatorAngle = parseFloat(row["RotatorPosition"])
						image.pierSide = row["PierSide"] ?? ""
						image.airmass = parseFloat(row["Airmass"])
						image.ra = parseDouble(row["MountRA"])
						image.dec = parseDouble(row["MountDec"])
						image.metaData = .partial
						
						images.append(image)
						
						hasMetaData = true
					}
				}
				
				let allImages = images
				let metaDataAvailable = hasMetaData
				
				await MainActor.run {
					self.sessionImages = allImages
					
					
					self.targets = allImages.compactMap{ $0.target }.distinct().sorted()
					self.filters = allImages.compactMap{ $0.filter }.distinct().sorted()
					self.pierSides = allImages.compactMap({ $0.pierSide}).distinct().sorted()
					self.exposures = allImages.compactMap({ $0.Exposure}).distinct().sorted()
					self.sessionDates = allImages.compactMap({ $0.SessionDate }).distinct().sorted(by: { d1, d2 in d1.date < d2.date })
					
					self.filterColors = createFilterColors(self.filters)
					
					self.readingData = false
					self.metaDataAvailable = metaDataAvailable
					
					self.selectedGuideSession = self.guiding.getGuidingSessions(onlyImages: true, minDuration: 0).first?.id
					self.guideLogAvailable = self.guiding.count > 0
					self.NINALogAvailable = self.ninaLogs.count > 0
					
					self.selectedTarget = self.targets.first ?? "Any"
					self.selectedFilter = self.filters.first ?? "Any"
					self.selectedPierSide = self.pierSides.count > 1 ? "Any" : (self.pierSides.first ?? "Any")
					self.selectedExposure = self.exposures.count > 1 ? "Any" : (self.exposures.first ?? "Any")
					self.selectedSessionDate = self.sessionDates.first?.text ?? "Any"
					
					self.id = UUID()
				}
				
				return hasMetaData
			}
			catch {
				print("Error: \(error.localizedDescription)")
				return false
			}
		}
	}
	
	private func checkImageData() async {
		
		var missing: [SessionImage] = []
		
		for image in self.sessionImages {
			if image.startExposureUTC == nil || image.exposure == nil {
				missing.append(image)
				continue
			}
			if image.ra == nil || image.dec == nil {
				missing.append(image)
				continue
			}
			image.metaData = .complete
		}
		
		let filtered = self.sessionImages.filter { $0.metaData == .complete }
		let missingDataImages = missing
		
		await MainActor.run {
			self.sessionImages = filtered
			self.missingDataImages = missingDataImages
		}
	}
	
	private func parseDouble(_ txt: String?) -> Double? {
		guard txt != nil && txt!.count > 0 && txt! != "NaN" && txt! != "nan" else { return nil }
		
		return Double(txt!)
	}
	
	private func parseFloat(_ txt: String?) -> Float? {
		guard txt != nil && txt!.count > 0 && txt! != "NaN" && txt! != "nan" else { return nil }
		
		return Float(txt!)
	}
	
	private func parseInt(_ txt: String?) -> Int? {
		guard txt != nil && txt!.count > 0 && txt! != "NaN" && txt! != "nan" else { return nil }
		
		return Int(txt!)
	}
	
	
	private func assign(_ metadata: ImageMetadata) -> Bool {
		for image in self.sessionImages {
			if metadata.filePath.contains(image.FileName) || image.FileName.contains(metadata.filePath) {
				
				//image.index = metadata.index
				image.mean = metadata.mean
				image.aduMin = metadata.aduMin
				image.aduMax = metadata.aduMax
				image.median = metadata.median
				image.numStars = metadata.numStars
				if image.numStars == 0 {
					image.bad = true
					image.addBadMarking()
				}
				image.hfr = metadata.hfr
				image.hfrStdev = metadata.hfrStdDev
				image.fwhm = metadata.fwhm
				image.eccentricity = metadata.eccentricity
				image.rms = metadata.rms
				image.rmsRA = metadata.rmsRA
				image.rmsDEC = metadata.rmsDEC
				image.metaData = .partial
				return true
			}
		}
		
		return false
	}
	
	public func updateSessionLights(_ lightsFolder: URL)  {
		
		let files = scanLightsFolder(lightsFolder)
		
		Task(priority: TaskPriority.medium) {

			await MainActor.run {
				self.readingData = true
				self.analyzeAvailable = false
			}
			
			var hasMetaData: Bool = false
			
			if files.count > 0 {
				
				if let found = await scanImages(files) {
					
					await MainActor.run {
						self.sessionImages = found.images
						
						self.targets = found.targets.sorted()
						self.filters = found.filters.sorted()
						self.pierSides = found.pierSides.sorted()
						self.exposures = found.exposures.sorted()
						self.sessionDates = found.sessionDates.sorted(by: { d1, d2 in d1.date < d2.date })
						
						self.filterColors = createFilterColors(self.filters)
					}
					
					if let metaDataUrls = findImageMetaDataCSVFiles(lightsFolder) {
						hasMetaData = await readImageMetaData(metaDataUrls, false)
					}
					
					if let weatherDataUrls = findWeatherDataCSVFiles(lightsFolder) {
						await readAllWeatherData(weatherDataUrls)
					}
					
					await readJSONFiles(lightsFolder)
					
					let guideLogUrls = findGuideLogFiles(lightsFolder)
					await readGuideLogs(guideLogUrls)
					
					await findNINALogs(lightsFolder, update: false)
					
					self.analyzeImages()
					let metaDataAvailable = hasMetaData
					
					await MainActor.run {
						self.readingData = false
						self.metaDataAvailable = metaDataAvailable
						
						self.selectedGuideSession = self.guiding.getGuidingSessions(onlyImages: true, minDuration: 0).first?.id
						self.guideLogAvailable = self.guiding.count > 0
						self.NINALogAvailable = self.ninaLogs.count > 0
						
						self.selectedTarget = self.targets.first ?? "Any"
						self.selectedFilter = self.filters.first ?? "Any"
						self.selectedPierSide = self.pierSides.count > 1 ? "Any" : (self.pierSides.first ?? "Any")
						self.selectedExposure = self.exposures.count > 1 ? "Any" : (self.exposures.first ?? "Any")
						self.selectedSessionDate = self.sessionDates.first?.text ?? "Any"
						
						self.id = UUID()
					}
				} else {
					await MainActor.run {
						self.readingData = false
						self.analyzeAvailable = false
						
						self.logging.append(LogEntry(text: LocalizedStringKey("Could not read all lights!"), type: .error))
						
						self.pierSides = []
						self.filters = []
						self.sessionDates = []
						self.exposures = []
						
						self.readingData = false
						self.metaDataAvailable = false
						
						self.filterColors = []
						
						self.selectedGuideSession = nil
						self.guideLogAvailable = false
						self.NINALogAvailable = false
						
						self.selectedTarget = "Any"
						self.selectedFilter = "Any"
						self.selectedPierSide = "Any"
						self.selectedExposure = "Any"
						self.selectedSessionDate = "Any"
						
						self.id = UUID()
						
					}
					
				}
			} else {
				// try to find CR2/CR3 (Canon)/NEF (Nikon), ARW (Sony) files
				let rawFiles = scanRawFileFolder(lightsFolder)
				if rawFiles.count > 0 {
					
					self.sessionImages = await scanRawImages(rawFiles)
					var hasMetaData: Bool = false
					let imageCount = self.sessionImages.count
					
					await MainActor.run {
						self.logging.append(LogEntry(text: LocalizedStringKey("\(imageCount) raw images found..."), type: .note))
					}
					
					if let acquisitionDetailFiles = findAcquisitionCSVFiles(lightsFolder) {
						await readAcquisitionData(acquisitionDetailFiles)
					}
					
					if let metaDataUrls = findImageMetaDataCSVFiles(lightsFolder) {
						hasMetaData = await readImageMetaData(metaDataUrls, true)
					}
					
					if let weatherDataUrls = findWeatherDataCSVFiles(lightsFolder) {
						await readAllWeatherData(weatherDataUrls)
					}
					
					await readJSONFiles(lightsFolder)
					
					self.sessionImages = self.sessionImages.filter({ $0.startExposureUTC != nil })
					if self.sessionImages.count > 0 {
						
						let guideLogUrls = findGuideLogFiles(lightsFolder)
						await readGuideLogs(guideLogUrls)
						
						await findNINALogs(lightsFolder, update: false)
						
						let piersides = self.sessionImages.map { $0.pierSide }.distinct()
						let filters = self.sessionImages.map { $0.filter }.distinct()
						let targets = self.sessionImages.map { $0.target }.distinct()
						let sessionDates = self.sessionImages.filter({ $0.startExposure != nil }).compactMap { $0.SessionDate }.distinct()
						let exposures = self.sessionImages.map { $0.Exposure }.distinct()
						
						self.analyzeImages()
						let metaDataAvailable = hasMetaData
						await MainActor.run {
							self.pierSides = piersides
							self.filters = filters
							self.targets = targets  
							self.sessionDates = sessionDates
							self.exposures = exposures
							
							self.readingData = false
							self.metaDataAvailable = metaDataAvailable
							
							self.filterColors = createFilterColors(self.filters)
							
							self.selectedGuideSession = self.guiding.getGuidingSessions(onlyImages: true, minDuration: 0).first?.id
							self.guideLogAvailable = self.guiding.count > 0
							self.NINALogAvailable = self.ninaLogs.count > 0
							
							self.selectedTarget = self.targets.first ?? "Any"
							self.selectedFilter = self.filters.first ?? "Any"
							self.selectedPierSide = self.pierSides.count > 1 ? "Any" : (self.pierSides.first ?? "Any")
							self.selectedExposure = self.exposures.count > 1 ? "Any" : (self.exposures.first ?? "Any")
							self.selectedSessionDate = self.sessionDates.first?.text ?? "Any"
							
							self.id = UUID()
						}
					}
					else {
						await MainActor.run {
							self.logging.append(LogEntry(text: LocalizedStringKey("Your metadata does not contain all required data. Please use the latest version of the Session Metadata plugin"), type: .error))
							
							self.pierSides = []
							self.filters = []
							self.sessionDates = []
							self.exposures = []
							
							self.readingData = false
							self.metaDataAvailable = false
							
							self.filterColors = []
							
							self.selectedGuideSession = nil
							self.guideLogAvailable = false
							self.NINALogAvailable = false
							
							self.selectedTarget = "Any"
							self.selectedFilter = "Any"
							self.selectedPierSide = "Any"
							self.selectedExposure = "Any"
							self.selectedSessionDate = "Any"
							
							self.id = UUID()
						}
					}
				}
				else if let metaDataUrls = findImageMetaDataCSVFiles(lightsFolder) {
					let metaDataAvailable = await readImageMetaData(metaDataUrls, false)
					await findNINALogs(lightsFolder, update: false)
					
					if self.sessionImages.count > 0 {
						
						let guideLogUrls = findGuideLogFiles(lightsFolder)
						await readGuideLogs(guideLogUrls)
					}
					
					await MainActor.run {
						self.readingData = false
						self.analyzeAvailable = false
						self.metaDataAvailable = metaDataAvailable
						self.selectedGuideSession = self.guiding.getGuidingSessions(onlyImages: true, minDuration: 0).first?.id
						self.guideLogAvailable = self.guiding.count > 0
						
						self.id = UUID()
					}
				}
			}
			
		}
	}
	
	private func createFilterColors(_ filters: [String]) -> [FilterData] {
		var filterdata = [FilterData]()
		
		var index = 0
		let defaultColors = [Color.filter1, Color.filter2, Color.filter3, Color.filter4, Color.filter5, Color.filter6, Color.filter7]
		
		let settings = UserDefaults.standard
		for filter in filters.distinct().filter({ $0 != "Any" }) {
			let overwrite: Bool = UserDefaults.standard.bool(forKey: "FilterOverwrite_\(filter)")
			let hfr: Float = self.analyzationSettings.getHFRTolerance(filter) * 100.0
			let mean: Float = self.analyzationSettings.getMeanTolerance(filter) * 100.0
			let starCount: Float = self.analyzationSettings.getStarCountTolerance(filter) * 100.0
						
			if let filterColorHex = settings.string(forKey: "FilterColor_\(filter)"), filterColorHex.isNotEmpty {
				let color = Color(hex: filterColorHex)
				filterdata.append(FilterData(filterName: filter, color: color, overwrite: overwrite, hfr: hfr, mean: mean, starCount: starCount))
			} else {
				
				switch filter {
					case "L", "NoFilter":
						filterdata.append(FilterData(filterName: filter, color: Color.filter1, overwrite: overwrite, hfr: hfr, mean: mean, starCount: starCount))
						break
					case "R":
						filterdata.append(FilterData(filterName: filter, color: Color.filter2, overwrite: overwrite, hfr: hfr, mean: mean, starCount: starCount))
						break
					case "G":
						filterdata.append(FilterData(filterName: filter, color: Color.filter3, overwrite: overwrite, hfr: hfr, mean: mean, starCount: starCount))
						break
					case "B":
						filterdata.append(FilterData(filterName: filter, color: Color.filter4, overwrite: overwrite, hfr: hfr, mean: mean, starCount: starCount))
						break
					case "Ha", "H", "Halpha":
						filterdata.append(FilterData(filterName: filter, color: Color.filter5, overwrite: overwrite, hfr: hfr, mean: mean, starCount: starCount))
						break
					case "O3", "OIII", "Oxygene":
						filterdata.append(FilterData(filterName: filter, color: Color.filter6, overwrite: overwrite, hfr: hfr, mean: mean, starCount: starCount))
						break
					case "Sulfor", "SII":
						filterdata.append(FilterData(filterName: filter, color: Color.filter7, overwrite: overwrite, hfr: hfr, mean: mean, starCount: starCount))
						break
					default:
						filterdata.append(FilterData(filterName: filter, color: defaultColors[index], overwrite: overwrite, hfr: hfr, mean: mean, starCount: starCount))
						index += 1
						if index >= defaultColors.count {
							index -= defaultColors.count
						}
						break
				}
				
			}
		}
		return filterdata
	}
	
	
	private func check(_ col: String?) -> String {
		if col == nil || col == "NaN" {
			return ""
		}
		return col!
	}
	
	private func parseWeatherDateJSON(_ date: String) -> Date? {
		let regEx = /(?<yyyy>.+?)-(?<MM>.+?)-(?<dd>.+?)T(?<hh>.+?):(?<mm>.+?):(?<ss>.+?)Z/
		if let matches = try? regEx.wholeMatch(in: date), let secs = Float(matches.ss) {
			let components = DateComponents(year: Int(matches.yyyy),
											month: Int(matches.MM),
											day: Int(matches.dd),
											hour: Int(matches.hh),
											minute: Int(matches.mm),
											second: Int(secs + 0.5))
											
			var calendar = Calendar(identifier: .gregorian)
			calendar.timeZone = self.currentTimeZone
			
			return calendar.date(from: components)
		}
		return nil
	}
	
	private func parseDateUTC(_ date: String) -> Date? {
		let regEx = /(?<yyyy>.+?)-(?<MM>.+?)-(?<dd>.+?)T(?<hh>.+?):(?<mm>.+?):(?<ss>.+?)Z/
		if let matches = try? regEx.wholeMatch(in: date) {
			let components = DateComponents(timeZone: self.currentTimeZone,
											year: Int(matches.yyyy),
											month: Int(matches.MM),
											day: Int(matches.dd),
											hour: Int(matches.hh),
											minute: Int(matches.mm),
											second: Int(matches.ss))
											
			var calendar = Calendar(identifier: .gregorian)
			calendar.timeZone = self.currentTimeZone
			
			return calendar.date(from: components)
		}
		
		let regEx1 = /(?<MM>.+?)\/(?<dd>.+?)\/(?<yyyy>.+?) (?<hh>.+?):(?<mm>.+?):(?<ss>.+?)/
		if let matches = try? regEx1.wholeMatch(in: date) {
			let components = DateComponents(timeZone: self.currentTimeZone,
											year: Int(matches.yyyy),
											month: Int(matches.MM),
											day: Int(matches.dd),
											hour: Int(matches.hh),
											minute: Int(matches.mm),
											second: Int(matches.ss))
											
			var calendar = Calendar(identifier: .gregorian)
			calendar.timeZone = self.currentTimeZone
			
			return calendar.date(from: components)
		}
		
		let regEx2 = /(?<yyyy>.+?)-(?<MM>.+?)-(?<dd>.+?) (?<hh>.+?):(?<mm>.+?)/
		if let matches = try? regEx2.wholeMatch(in: date) {
			let components = DateComponents(timeZone: self.currentTimeZone,
											year: Int(matches.yyyy),
											month: Int(matches.MM),
											day: Int(matches.dd),
											hour: Int(matches.hh),
											minute: Int(matches.mm),
											second: Int(0))
											
			var calendar = Calendar(identifier: .gregorian)
			calendar.timeZone = self.currentTimeZone
			
			return calendar.date(from: components)
		}
		
		return nil
	}
	
	private func readWeatherData(_ weatherURL: URL) {
		
		//ExposureNumber,ExposureStart,Temperature,DewPoint,Humidity,Pressure,WindSpeed,WindDirection,WindGust,CloudCover,SkyTemperature,SkyBrightness,SkyQuality
		do {
			let csvFile: CSV = try CSV<Named>(url: weatherURL, delimiter: CSVDelimiter.comma)
			for row in csvFile.rows {
				
				if let expNumber = parseInt(row["ExposureNumber"]) {
					
					if let image = self.sessionImages.first(where: { $0.index == expNumber }) {
						if let temp = Float(check(row["Temperature"])) {
							image.ambientTemp = temp
						}
						if let dewPoint = Float(check(row["DewPoint"])) {
							image.dewPoint = dewPoint
						}
						if let humidity = Float(check(row["Humidity"])) {
							image.humidity = humidity
						}
						if let pressure = Float(check(row["Pressure"])) {
							image.pressure = pressure
						}
						if let windSpeed = Float(check(row["WindSpeed"])) {
							image.windSpeed = windSpeed
						}
						if let windDir = Float(check(row["WindDirection"])) {
							image.windDirection = windDir
						}
						if let windGust = Float(check(row["WindGust"])) {
							image.windGust = windGust
						}
						if let cloudCover = Float(check(row["CloudCover"])) {
							image.cloudCover = cloudCover
						}
						if let skyTemperature = Float(check(row["SkyTemperature"])) {
							image.skyTemperature = skyTemperature
						}
						if let skyBrightness = Float(check(row["SkyBrightness"])) {
							image.skyBrightness = skyBrightness
						}
						if let skyQuality = Float(check(row["SkyQuality"])) {
							image.skyQuality = skyQuality
						}
					}
				}
			}
			
		}
		catch {
			print("Error: \(error.localizedDescription)")
		}
		
	}
	
	
	private func readGuideLogs(_ guideLogUrls: [URL]) async {
		if guideLogUrls.isEmpty { return }
		
		await withTaskGroup(of: Void.self) { taskGroup in
			// Iterate over each file and add task to TaskGroup
			for file in guideLogUrls {
				taskGroup.addTask {
					await MainActor.run {
						self.logging.append(LogEntry(text: LocalizedStringKey("reading \(file.absoluteString)")))
					}
					
					await self.guiding.read(file, timeZone: self.currentTimeZone)
				}
			}
			
			// Wait for all tasks to complete
			for await _ in taskGroup {}
		}
		
		
		await MainActor.run {
			self.selectedGuideSession = self.guiding.getGuidingSessions(onlyImages: true, minDuration: 0).first?.id
			self.guideLogAvailable = self.guiding.count > 0
			
			assignImagesToGuideLogs()
		}
	}

	
	public func assignImagesToGuideLogs() {
		
		for session in self.guiding.guidingSessions {
			let start = session.guidingStart
			let end = session.guidingEnd
			
			session.images.removeAll()
			
			for image in self.sessionImages.filter({ $0.startExposureUTC != nil && $0.begin != nil && $0.end != nil }) {
				if let imageBegin = image.begin, let imageEnd = image.end {
					if (start.jd <= imageBegin.jd && imageBegin.jd <= end.jd)
						|| (start.jd <= imageEnd.jd && end.jd >= imageEnd.jd) {
						session.images.append(image.id)
					}
				}
			}
		}
		
		if self.selectedGuideSession == nil {
			self.guideLogAvailable = self.guiding.count > 0
			self.selectedGuideSession = self.guiding.getGuidingSessions(onlyImages: true, minDuration: 0).first?.id
		}
	}
	
	private func readJSONFiles(_ baseURL: URL) async {
		let urls = findJSONFiles(baseURL)
		if urls.count > 0 {
			for url in urls {
				
				if let (data, _) = try? await URLSession.shared.data(from: url) {
					if let json = try? JSON(data: data), json.count > 0 {
						
						for i in stride(from: 0, to: json.count, by: 1) {
							if let expStart = parseWeatherDateJSON(json[i]["ExposureStart"].string ?? "") {
								if let image = self.sessionImages.first(where: { $0.startExposureUTC != nil
																		&& abs($0.startExposureUTC!.distance(to: expStart)) < 10.0 }) {
									
									if let temp = json[i]["Temperature"].float {
										image.ambientTemp = temp
									}
									if let dewPoint = json[i]["DewPoint"].float {
										image.dewPoint = dewPoint
									}
									if let humidity = json[i]["Humidity"].float {
										image.humidity = humidity
									}
									if let pressure = json[i]["Pressure"].float {
										image.pressure = pressure
									}
									if let windSpeed = json[i]["WindSpeed"].float {
										image.windSpeed = windSpeed
									}
									if let windDir = json[i]["WindDirection"].float {
										image.windDirection = windDir
									}
									if let windGust = json[i]["WindGust"].float {
										image.windGust = windGust
									}
									if let cloudCover = json[i]["CloudCover"].float {
										image.cloudCover = cloudCover
									}
									if let skyTemperature = json[i]["SkyTemperature"].float {
										image.skyTemperature = skyTemperature
									}
									if let skyBrightness = json[i]["SkyBrightness"].float {
										image.skyBrightness = skyBrightness
									}
									if let skyQuality = json[i]["SkyQuality"].float {
										image.skyQuality = skyQuality
									}
								}
							} else if let filePath = json[i]["FilePath"].string {
								
								let fileName = URL(fileURLWithPath: filePath).lastPathComponent
								
								if let image = self.sessionImages.first(where: { $0.FileName.contains(fileName) || fileName.contains($0.FileName) }) {
									
									
									if let mean = json[i]["ADUMean"].float {
										image.mean = mean
									}
									if let median = json[i]["ADUMedian"].float {
										image.median = Int(median)
									}
									if let aduMin = json[i]["ADUMin"].int {
										image.aduMin = aduMin
									}
									if let aduMax = json[i]["ADUMax"].int {
										image.aduMax = aduMax
									}
									if let numStars = json[i]["DetectedStars"].int {
										image.numStars = numStars
									}
									if let hfr = json[i]["HFR"].float {
										image.hfr = hfr
									}
									if let hfrStDev = json[i]["HFRStDev"].float {
										image.hfrStdev = hfrStDev
									}
									if let fwhm = json[i]["FWHM"].float {
										image.fwhm = fwhm
									}
									if let ecc = json[i]["Eccentricity"].float {
										image.eccentricity = ecc
									}
									if let rms = json[i]["GuidingRMSArcSec"].float {
										image.rms = rms
									}
									if let rmsRA = json[i]["GuidingRMSRAArcSec"].float {
										image.rmsRA = rmsRA
									}
									if let rmsDEC = json[i]["GuidingRMSDECArcSec"].float {
										image.rmsDEC = rmsDEC
									}
									
								}
							}
						}
					}
				}
				
				
			}
			
		}
		
	}
	
	private func readAcquisitionData(_ acquisitionDataUrls: [URL]) async {
		if acquisitionDataUrls.count > 0 {
			for url in acquisitionDataUrls {
				await MainActor.run {
					self.logging.append(LogEntry(text: LocalizedStringKey("reading \(url.absoluteString)")))
				}
				updateAcquisitionData(url, update: false)
			}
		}
	}
	
	
	private func readImageMetaData(_ metaDataUrls: [URL], _ removeMissing: Bool) async -> Bool {
		if metaDataUrls.isEmpty { return false }
		
		var hasMetaData: Bool = false
		
		for url in metaDataUrls {
			await MainActor.run {
				self.logging.append(LogEntry(text: LocalizedStringKey("reading \(url.absoluteString)")))
			}
			hasMetaData = await self.updateImageMetadata(url)
		}
		
		if removeMissing {
			
			// for DSLR remove all images without metadata here
			if self.sessionImages.allSatisfy({ !$0.isFits }) {
				
				self.sessionImages.removeAll(where: { $0.startExposureUTC == nil })
				self.sessionImages.sort(by: { $0.startExposureUTC! < $1.startExposureUTC! })
				
				if self.sessionImages.isEmpty {
					return false
				}
			}
		}
		return hasMetaData
	}

	private func readAllWeatherData(_ weatherDataUrls: [URL]) async {
		if weatherDataUrls.isEmpty { return }
		
		for url in weatherDataUrls {
			
			await MainActor.run {
				self.logging.append(LogEntry(text: LocalizedStringKey("reading \(url.absoluteString)")))
			}
			self.readWeatherData(url)
		}
		
	}

	
	private func scanRawImages(_ images: [URL]) async -> [SessionImage] {
		
		var sessionImages = [SessionImage]()
		for url in images {
			
			let image = SessionImage(url: url)
			
			if url.lastPathComponent.starts(with: "BAD_") {
				await MainActor.run {
					self.logging.append(LogEntry(text: LocalizedStringKey("\(url.absoluteString) image is marked as 'BAD'"), type: .warn))
				}
				image.bad = true
			}
			
			
			if let (width, height) = readImageSize(from: url) {
				image.sizeX = width
				image.sizeY = height
			}
			
			sessionImages.append(image)
		}
		return sessionImages
	}
	
	private func readImageSize(from url: URL) -> (width: Int, height: Int)? {
		if let sourceRef = CGImageSourceCreateWithURL(url as CFURL, nil) {
			if let cgImage = CGImageSourceCreateImageAtIndex(sourceRef, 0, nil) {
				return (width: cgImage.width, height: cgImage.height)
			}
		}
		return nil
	}
	
	
	
	private func split(_ images: [URL]) -> [[URL]] {
		// Split images into equal or nearly equal parts based on available cores
		let count = images.count
		let cores = ProcessInfo.processInfo.activeProcessorCount
		let batchSize = (count + cores - 1) / cores // Ceiling division

		return images.chunked(into: batchSize)
	}

	

	
	private func scanImages(_ images: [URL]) async -> FoundLights?  {
		var sessionImages = [SessionImage]()
		var targetHash = Set<String>()
		var filterHash = Set<String>()
		var pierSideHash = Set<String>()
		var exposureHash = Set<String>()
		var sessionDates = Set<DateOfSession>()
		
		guard let results = try? await readAllHeaders(images)
		else {
			return nil
		}
			
		sessionImages.append(contentsOf: results)
		
		for image in results {
			
			targetHash.insert(image.target)
			filterHash.insert(image.filter)
			pierSideHash.insert(image.pierSide)
			exposureHash.insert(image.Exposure)
			if let sessionDate = image.SessionDate {
				sessionDates.insert(sessionDate)
			}
		}
	
		if targetHash.count > 1 {
			targetHash.insert("Any")
		}
	
		if filterHash.count > 1 {
			filterHash.insert("Any")
		}
		
		if pierSideHash.count > 1 {
			pierSideHash.insert("Any")
		}
		
		if exposureHash.count > 1 {
			exposureHash.insert("Any")
		}
		
		if sessionDates.count > 1 {
			sessionDates.insert(DateOfSession(text: "Any", date: Date.distantPast))
		}
		
		var images = sessionImages.filter({ $0.startExposure != nil })
		
		images.sort { si1, si2 in si1.startExposure! < si2.startExposure! }
		var index = 1
		for image in images {
			image.index = index
			index += 1
		}
				
		return FoundLights(images: images, targets: Array(targetHash), filters: Array(filterHash),
						   pierSides: Array(pierSideHash), exposures: Array(exposureHash),
						   sessionDates: Array(sessionDates))
	}
	
	private func readAllHeaders(_ images: [URL]) async throws -> [SessionImage] {

		return try await withThrowingTaskGroup(of: SessionImage?.self) { group -> [SessionImage] in
			var results: [SessionImage?] = []
			
			for url in images {
				group.addTask {
					if let header = FITS.readHeader(from: url) {
						return await self.readHeaderData(header, url)
					}
					return nil
				}
			}
			
			for try await header in group {
				results.append(header)
			}
			
			return results.compactMap({ $0 })
		}
	}

	private func readHeaderData(_ header: FitsHeader, _ url: URL) async -> SessionImage? {
		
		let image = SessionImage(url: url)
		
		if let imgType=header[FitsHeaderData.IMAGETYP.rawValue]?.Value {
			if !imgType.contains("LIGHT") {
				await MainActor.run {
					self.logging.append(LogEntry(text: LocalizedStringKey("\(url.absoluteString) is not a light frame."), type: .warn))
				}
				return nil
			}
		} else {
			await MainActor.run {
				self.logging.append(LogEntry(text: LocalizedStringKey("\(url.absoluteString): image type not set."), type: .error))
			}
			return nil
		}
		
		if url.lastPathComponent.starts(with: "BAD_") {
			await MainActor.run {
				self.logging.append(LogEntry(text: LocalizedStringKey("\(url.absoluteString) image is marked as 'BAD'"), type: .warn))
			}
			image.bad = true
		}
				
		await MainActor.run {
			self.logging.append(LogEntry(text: LocalizedStringKey("reading light frame \(url.formatted()).")))
		}
		
		if let ra = header[FitsHeaderData.RA.rawValue]?.Value {
			image.ra = Double(ra)
		}
		if let dec = header[FitsHeaderData.DEC.rawValue]?.Value {
			image.dec = Double(dec)
		}
		if let exposure = header[FitsHeaderData.EXPOSURE.rawValue]?.Value {
			image.exposure = Float(exposure)
		} else {
			image.exposure = 0.0
		}
		if let startTime = header[FitsHeaderData.STARTTIME.rawValue]?.Value?.replacingOccurrences(of: "'", with: "") {
			let df = getDateFormatter()
			
			image.startExposure = df.date(from: startTime)
			if image.startExposure == nil {
				return nil
			}
		}
		if let startTime = header[FitsHeaderData.STARTTIMEUTC.rawValue]?.Value?.replacingOccurrences(of: "'", with: "") {
			let df = getDateFormatter()
			
			image.startExposureUTC = df.date(from: startTime)
			if image.startExposureUTC == nil {
				return nil
			}
		}
		
		if let obsLat = header[FitsHeaderData.OBS_LAT.rawValue]?.Value {
			image.obsLat = Double(obsLat)
		}
	
		if let obsLong = header[FitsHeaderData.OBS_LONG.rawValue]?.Value {
			image.obsLong = Double(obsLong)
		}
		
		if let gain = header[FitsHeaderData.GAIN.rawValue]?.Value {
			image.gain = Int(gain)
		}
		if let offset = header[FitsHeaderData.OFFSET.rawValue]?.Value {
			image.offset = Int(offset)
		}
		if let camera = header[FitsHeaderData.CAMERA.rawValue]?.Value?.replacingOccurrences(of: "'", with: "")  {
			image.camera = camera
		}
		if let cameraTemp = header[FitsHeaderData.CCDTEMP.rawValue]?.Value {
			image.cameraTemp = Float(cameraTemp)
		}
		
		if let bayerPattern = header[FitsHeaderData.BAYERPATTERN.rawValue]?.Value?.replacingOccurrences(of: "'", with: "") {
			image.bayerPattern = bayerPattern.trimmingCharacters(in: .whitespacesAndNewlines)
		}
		
		if let telescope = header[FitsHeaderData.TELESCOPE.rawValue]?.Value?.replacingOccurrences(of: "'", with: "")  {
			image.telescope = telescope
		}
		if let focalLength = header[FitsHeaderData.FOCALLENGTH.rawValue]?.Value {
			image.focalLength = Float(focalLength)
		}
		if let altitude = header[FitsHeaderData.ALTITUDE.rawValue]?.Value {
			image.altitude = Float(altitude)
		}
		if let azimuth = header[FitsHeaderData.AZIMUTH.rawValue]?.Value {
			image.azimuth = Float(azimuth)
		}
		if let airmass = header[FitsHeaderData.AIRMASS.rawValue]?.Value {
			image.airmass = Float(airmass)
		}
		
		if let windSpeed = header[FitsHeaderData.WINDSPD.rawValue]?.Value {
			image.windSpeed = Float(windSpeed)
		}
		
		if let pierside = header[FitsHeaderData.PIERSIDE.rawValue]?.Value?.replacingOccurrences(of: "'", with: "")  {
			image.pierSide = pierside.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
		} else {
			image.pierSide = "Unknown"
		}
		if let filter = header[FitsHeaderData.FILTER.rawValue]?.Value?.replacingOccurrences(of: "'", with: "")  {
			image.filter = filter.replacingOccurrences(of: "'", with: "").trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
		} else {
			image.filter = "NoFilter"
		}
		if let target = header[FitsHeaderData.TARGET.rawValue]?.Value {
			image.target = target.replacingOccurrences(of: "'", with: "").trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
		} else {
			image.target = "Unknown"
		}
		if let objRA = header[FitsHeaderData.OBJCTRA.rawValue]?.Value {
			// '11 20 39'
			let raParts = objRA.replacingOccurrences(of: "\'", with: "").components(separatedBy: " ")
			if raParts.count == 3 {
				let d = Double(raParts[0])!
				let m = Double(raParts[1])!/60.0
				let s =  Double(raParts[2])!/3600.0
				let ra = 15.0 * (d + m + s)
				image.targetRA = ra
			}
		}
		if let objDEC = header[FitsHeaderData.OBJCTDEC.rawValue]?.Value {
			// '+13 16 17'
			let decParts = objDEC.replacingOccurrences(of: "\'", with: "").components(separatedBy: " ")
			if decParts.count == 3 {
				let deg = decParts[0].replacingOccurrences(of: "+", with: "").replacingOccurrences(of: "-", with: "")
				let m = Double(decParts[1])!/60.0
				let s = Double(decParts[2])!/3600.0
				
				var dec = Double(deg)! + m + s
				if objDEC.starts(with: "-") {
					dec = -dec
				}
				image.targetDEC = dec
			}
		}
		
		if let targetRotation = header[FitsHeaderData.TARGETROTATION.rawValue]?.Value {
			image.targetRotation = Float(targetRotation)
		}
		if let focusPos = header[FitsHeaderData.FOCPOS.rawValue]?.Value {
			image.focusPosition = Float(focusPos)
		}
		if let focusTemp = header[FitsHeaderData.FOCTEMP.rawValue]?.Value {
			image.focusTemp = Float(focusTemp)
		}
		if let rotatorAngle = header[FitsHeaderData.ROTATANG.rawValue]?.Value {
			image.rotatorAngle = Float(rotatorAngle)
		}
		if let cloudCover = header[FitsHeaderData.CLOUDCVR.rawValue]?.Value {
			image.cloudCover = Float(cloudCover)
		}
		if let humidity = header[FitsHeaderData.HUMIDITY.rawValue]?.Value {
			image.humidity = Float(humidity)
		}
		if let dewPoint = header[FitsHeaderData.DEWPOINT.rawValue]?.Value {
			image.dewPoint = Float(dewPoint)
		}
		if let pressure = header[FitsHeaderData.PRESSURE.rawValue]?.Value {
			image.pressure = Float(pressure)
		}
		if let ambientTemp = header[FitsHeaderData.AMBTEMP.rawValue]?.Value {
			image.ambientTemp = Float(ambientTemp)
		}
		if let windDir = header[FitsHeaderData.WINDDIR.rawValue]?.Value {
			image.windDirection = Float(windDir)
		}
		if let windSpeed = header[FitsHeaderData.WINDSPD.rawValue]?.Value {
			image.windSpeed = Float(windSpeed)
		}
		if let pxSize = header[FitsHeaderData.XPIXSZ.rawValue]?.Value {
			image.pixSize = Float(pxSize)
		}
		if let sx = header[FitsHeaderData.SIZEX.rawValue]?.Value {
			image.sizeX = Int(sx)
		}
		if let sy = header[FitsHeaderData.SIZEY.rawValue]?.Value {
			image.sizeY = Int(sy)
		}
		
		if let date = image.startExposure, let long = image.obsLong, let lat = image.obsLat {
			let jd = JulianDate.fromDate(date: date)
			let (azSun, altSun) = Sun.getAltAz(jd, long: long, lat: lat)
			let (azMoon, altMoon) = Moon.getAltAz(jd, long: long, lat: lat)
			
			image.sunAlt = Float(altSun*Constants.Deg)
			image.moonAlt = Float(altMoon*Constants.Deg)
			image.sunAz = Float(azSun*Constants.Deg)
			image.moonAz = Float(azMoon*Constants.Deg)
			
			if let targetRA = image.targetRA, let targetDEC = image.targetDEC {
				let targetPos = Vector3.from(ra: targetRA*Constants.Rad, dec: targetDEC*Constants.Rad)
				let moonPos = Moon.getPosition(jd.t)
				image.moonAngle = Float(angle(targetPos, moonPos).toDegrees())
				
				let sunPos = Sun.getPosition(jd.t)
				image.sunAngle = Float(angle(targetPos, sunPos).toDegrees())
			}
			
			let jdSession = frac(jd.jd) < 0.5 ? jd.getBeginOfDay().addHours(-12.0) : jd.getBeginOfDay().addHours(12.0)
			image.sessionDate = jdSession.toDate()
		}
		else if let date = image.startExposure {
			image.sessionDate = JulianDate.fromDate(date: date).highNoon().toDate()
		}
		
		return image
	}
	
	private func findJSONFiles(_ url: URL) -> [URL] {
		var files = Set<URL>()
		var fileURLS = Stack<URL>()
		
		let validExtensions = ["json"]
		
		if let enumerator = FileManager.default.enumerator(at: url.resolvingSymlinksInPath(),
														   includingPropertiesForKeys: [.isRegularFileKey, .isSymbolicLinkKey],
														   options: [.skipsHiddenFiles, .skipsPackageDescendants]) {
			for case let fileURL as URL in enumerator {
				fileURLS.push(fileURL)
			}
			
			while fileURLS.count > 0 {
				if let fileURL = fileURLS.pop() {
					do {
						let fileAttributes = try fileURL.resourceValues(forKeys:[.isRegularFileKey, .isAliasFileKey])
						if fileAttributes.isAliasFile! {
							let destURL = try URL(resolvingAliasFileAt: fileURL)
							let destAttributes = try destURL.resourceValues(forKeys:[.isRegularFileKey, .isDirectoryKey])
							
							if destAttributes.isRegularFile! {
								
								if destURL.absoluteString.lowercased().contains("phd2_guidelog_"){
									files.insert(destURL)
									continue
								}
							} else if destAttributes.isDirectory! {
								if let subEnum = FileManager.default.enumerator(at: destURL.resolvingSymlinksInPath(),
																				includingPropertiesForKeys: [.isRegularFileKey, .isSymbolicLinkKey],
																				options: [.skipsHiddenFiles, .skipsPackageDescendants]) {
									for case let fileURL as URL in subEnum {
										fileURLS.push(fileURL)
									}
								}
							}
							
						} else if fileAttributes.isRegularFile!
							&& validExtensions.contains(fileURL.pathExtension.lowercased()) {
							files.insert(fileURL)
						}
					}
					catch {
						print(error, fileURL)
					}
				}
			}
		}
		
		return Array(files)
	}
	
	private func scanLightsFolder(_ url: URL) -> [URL] {
	
		var files = [URL]()
		let validExtensions = ["fit", "fits"]
		
		if let enumerator = FileManager.default.enumerator(at: url.resolvingSymlinksInPath(),
														   includingPropertiesForKeys: [.isRegularFileKey, .isSymbolicLinkKey],
														   options: [.skipsHiddenFiles, .skipsPackageDescendants]) {
			for case let fileURL as URL in enumerator {
			   do {
				   let fileAttributes = try fileURL.resourceValues(forKeys:[.isRegularFileKey, .isSymbolicLinkKey])
				   if fileAttributes.isRegularFile!
						&& validExtensions.contains(fileURL.pathExtension.lowercased()) {
					   
					   let parts = fileURL.pathComponents
					   if parts.map({$0.lowercased()})
						.allSatisfy({ !$0.contains("dark") && !$0.contains("flat") && !$0.contains("bias")}) {
						   files.append(fileURL)
					   }
				   } else  {
					   var destFile = fileURL
					   destFile.resolveSymlinksInPath()
					   if validExtensions.contains(destFile.pathExtension.lowercased()) {
						   files.append(destFile)
					   }
				   }
			   }
				catch {
					print(error, fileURL)
				}
			}
		}
		
		return files
	}
	
	private func scanRawFileFolder(_ url: URL) -> [URL] {
		var files = [URL]()
		let validExtensions = ["cr2", "cr3", "nef", "arw"]
		
		if let enumerator = FileManager.default.enumerator(at: url.resolvingSymlinksInPath(),
														   includingPropertiesForKeys: [.isRegularFileKey, .isSymbolicLinkKey],
														   options: [.skipsHiddenFiles, .skipsPackageDescendants]) {
			for case let fileURL as URL in enumerator {
			   do {
				   let fileAttributes = try fileURL.resourceValues(forKeys:[.isRegularFileKey, .isSymbolicLinkKey])
				   if fileAttributes.isRegularFile!
						&& validExtensions.contains(fileURL.pathExtension.lowercased()) {
					   
					   let parts = fileURL.pathComponents
					   if parts.map({$0.lowercased()})
						.allSatisfy({ !$0.contains("dark") && !$0.contains("flat") && !$0.contains("bias")}) {
						   files.append(fileURL)
					   }
				   } else  {
					   var destFile = fileURL
					   destFile.resolveSymlinksInPath()
					   if validExtensions.contains(destFile.pathExtension.lowercased()) {
						   files.append(destFile)
					   }
				   }
			   }
				catch {
					print(error, fileURL)
				}
			}
		}
		
		return files
		
	}
	
	
	private func scanWeatherDataCSVHeader(_ fileURL: URL) -> Bool {
		do {
			let csvFile: CSV = try CSV<Named>(url: fileURL, delimiter: CSVDelimiter.comma)
			let headerData = Set<String>(csvFile.header)
			
			return headerData.contains("ExposureStart")
				&& headerData.contains("Temperature")
				&& headerData.contains("DewPoint")
				&& headerData.contains("Humidity")
				&& headerData.contains("Pressure")
				&& headerData.contains("WindSpeed")
				&& headerData.contains("WindDirection")
				&& headerData.contains("WindGust")
				&& headerData.contains("CloudCover")
				&& headerData.contains("SkyTemperature")
				&& headerData.contains("SkyBrightness")
				&& headerData.contains("SkyQuality")
		}
		catch {
			print(error, fileURL)
			return false
		}
	}
	
	
	private func findWeatherDataCSVFiles(_ url: URL) -> [URL]? {
		
		var files = Set<URL>()
		var fileURLS = Stack<URL>()
		
		if let enumerator = FileManager.default.enumerator(at: url.resolvingSymlinksInPath(),
														   includingPropertiesForKeys: [.isRegularFileKey, .isAliasFileKey],
														   options: [.skipsHiddenFiles, .skipsPackageDescendants]) {
			for case let fileURL as URL in enumerator {
				fileURLS.push(fileURL)
			}
			
			while fileURLS.count > 0 {
				if let fileURL = fileURLS.pop() {
					do {
						let fileAttributes = try fileURL.resourceValues(forKeys:[.isRegularFileKey, .isAliasFileKey])
						if fileAttributes.isAliasFile! {
							let destURL = try URL(resolvingAliasFileAt: fileURL)
							let destAttributes = try destURL.resourceValues(forKeys:[.isRegularFileKey, .isDirectoryKey])
							
							if destAttributes.isRegularFile! {
								
								if destURL.absoluteString.lowercased().contains("phd2_guidelog_"){
									files.insert(destURL)
									continue
								}
							} else if destAttributes.isDirectory! {
								if let subEnum = FileManager.default.enumerator(at: destURL.resolvingSymlinksInPath(),
																				includingPropertiesForKeys: [.isRegularFileKey, .isSymbolicLinkKey],
																				options: [.skipsHiddenFiles, .skipsPackageDescendants]) {
									for case let fileURL as URL in subEnum {
										fileURLS.push(fileURL)
									}
								}
							}
							
						} else  if fileURL.lastPathComponent.hasSuffix("csv") {
							
							if fileAttributes.isRegularFile! {
								if fileURL.absoluteString.lowercased().contains("weatherdata.csv") {
									files.insert(fileURL)
								}
								else if scanWeatherDataCSVHeader(fileURL) {
									files.insert(fileURL)
								}
							}
						}
					}
					catch {
						print(error, fileURL)
					}
				}
			}
		}
		
		if files.count > 0 {
			return Array(files)
		}
		return nil
	}
	
	private func scanImageMetaDataCSVHeader(_ fileURL: URL) -> Bool {
		do {
			let csvFile: CSV = try CSV<Named>(url: fileURL, delimiter: CSVDelimiter.comma)
			let headerData = Set<String>(csvFile.header)
			
			return headerData.contains("ExposureNumber")
				&& headerData.contains("FilePath")
				&& headerData.contains("ADUMean")
				&& headerData.contains("ADUMedian")
				&& headerData.contains("ADUMin")
				&& headerData.contains("ADUMax")
				&& headerData.contains("DetectedStars")
				&& headerData.contains("HFR")
				&& headerData.contains("GuidingRMSArcSec")
				&& headerData.contains("GuidingRMSRAArcSec")
				&& headerData.contains("GuidingRMSDECArcSec")
		}
		catch {
			print(error, fileURL)
			return false
		}
	}
	
	private func findAcquisitionCSVFiles(_ url: URL) -> [URL]? {
		var files = Set<URL>()
		var fileURLS = Stack<URL>()
		
		if let enumerator = FileManager.default.enumerator(at: url.resolvingSymlinksInPath(), includingPropertiesForKeys: [.isRegularFileKey, .isSymbolicLinkKey],
														   options: [.skipsHiddenFiles, .skipsPackageDescendants]) {
			for case let fileURL as URL in enumerator {
				fileURLS.push(fileURL)
			}
				
			while fileURLS.count > 0 {
				if let fileURL = fileURLS.pop() {
					do {
						let fileAttributes = try fileURL.resourceValues(forKeys:[.isRegularFileKey, .isAliasFileKey])
						if fileAttributes.isAliasFile! {
							let destURL = try URL(resolvingAliasFileAt: fileURL)
							let destAttributes = try destURL.resourceValues(forKeys:[.isRegularFileKey, .isDirectoryKey])
							
							if destAttributes.isDirectory! {
								if let subEnum = FileManager.default.enumerator(at: destURL.resolvingSymlinksInPath(),
																				includingPropertiesForKeys: [.isRegularFileKey, .isSymbolicLinkKey],
																				options: [.skipsHiddenFiles, .skipsPackageDescendants]) {
									for case let fileURL as URL in subEnum {
										fileURLS.push(fileURL)
									}
								}
							}
							
						} else if fileURL.lastPathComponent.hasSuffix("csv") {
							
							if fileAttributes.isRegularFile! && fileURL.absoluteString.lowercased().contains("acquisitiondetails.csv") {
								files.insert(fileURL)
								continue
							} 
							/*
							else {
								if scanImageMetaDataCSVHeader(fileURL) {
									files.insert(fileURL)
									continue
								}
							}*/
						}
					}
					catch {
						print(error, fileURL)
					}
				}
			}
		}
		
		if files.count > 0 {
			return Array(files)
		}
		return nil
	}
	
	
	private func findImageMetaDataCSVFiles(_ url: URL) -> [URL]? {
		
		var files = Set<URL>()
		var fileURLS = Stack<URL>()
		
		if let enumerator = FileManager.default.enumerator(at: url.resolvingSymlinksInPath(), includingPropertiesForKeys: [.isRegularFileKey, .isSymbolicLinkKey], 
														   options: [.skipsHiddenFiles, .skipsPackageDescendants]) {
			for case let fileURL as URL in enumerator {
				fileURLS.push(fileURL)
			}
				
			while fileURLS.count > 0 {
				if let fileURL = fileURLS.pop() {
					do {
						let fileAttributes = try fileURL.resourceValues(forKeys:[.isRegularFileKey, .isAliasFileKey])
						if fileAttributes.isAliasFile! {
							let destURL = try URL(resolvingAliasFileAt: fileURL)
							let destAttributes = try destURL.resourceValues(forKeys:[.isRegularFileKey, .isDirectoryKey])
							
							if destAttributes.isRegularFile! {
								
								if destURL.absoluteString.lowercased().contains("phd2_guidelog_"){
									files.insert(destURL)
									continue
								}
							} else if destAttributes.isDirectory! {
								if let subEnum = FileManager.default.enumerator(at: destURL.resolvingSymlinksInPath(),
																				includingPropertiesForKeys: [.isRegularFileKey, .isSymbolicLinkKey],
																				options: [.skipsHiddenFiles, .skipsPackageDescendants]) {
									for case let fileURL as URL in subEnum {
										fileURLS.push(fileURL)
									}
								}
							}
							
						} else if fileURL.lastPathComponent.hasSuffix("csv") {
							
							if fileAttributes.isRegularFile! && fileURL.absoluteString.lowercased().contains("imagemetadata.csv") {
								files.insert(fileURL)
								continue
							} else {
								if scanImageMetaDataCSVHeader(fileURL) {
									files.insert(fileURL)
									continue
								}
							}
						}
					}
					catch {
						print(error, fileURL)
					}
				}
			}
		}
		
		if files.count > 0 {
			return Array(files)
		}
		return nil
	}
	
	
	private func findGuideLogFiles(_ url: URL) -> [URL] {
		
		var files = Set<URL>()
		var fnameHash = Set<String>()
		var fileURLS = Stack<URL>()
		
		if let enumerator = FileManager.default.enumerator(at: url.resolvingSymlinksInPath(),
														   includingPropertiesForKeys: [.isRegularFileKey, .isSymbolicLinkKey],
														   options: [.skipsHiddenFiles, .skipsPackageDescendants]) {
			for case let fileURL as URL in enumerator {
				fileURLS.push(fileURL)
			}
			
			while fileURLS.count > 0 {
				
				if let fileURL = fileURLS.pop() {
					do {
					
						let fileAttributes = try fileURL.resourceValues(forKeys:[.isRegularFileKey, .isAliasFileKey, .isDirectoryKey])
						if fileAttributes.isAliasFile! {
							let destURL = try URL(resolvingAliasFileAt: fileURL)
							let destAttributes = try destURL.resourceValues(forKeys:[.isRegularFileKey, .isDirectoryKey])
							
							if destAttributes.isRegularFile! {
								
								if destURL.absoluteString.lowercased().contains("phd2_guidelog_"){
									if !fnameHash.contains(destURL.lastPathComponent) {
										files.insert(destURL)
										fnameHash.insert(destURL.lastPathComponent)
									}
									continue
								}
							} else if destAttributes.isDirectory! {
								if let subEnum = FileManager.default.enumerator(at: destURL.resolvingSymlinksInPath(),
																				includingPropertiesForKeys: [.isRegularFileKey, .isSymbolicLinkKey],
																				options: [.skipsHiddenFiles, .skipsPackageDescendants]) {
									for case let fileURL as URL in subEnum {
										fileURLS.push(fileURL)
									}
								}
							}
							
						}
						else if fileAttributes.isRegularFile! {
							
							if fileURL.absoluteString.lowercased().contains("phd2_guidelog_") {
								if !fnameHash.contains(fileURL.lastPathComponent) {
									fnameHash.insert(fileURL.lastPathComponent)
									files.insert(fileURL)
								}
							}
						}
						
					}
					catch {
						print(error, fileURL)
					}
				}
			}
		}
	
		if files.count > 1, self.sessionImages.count > 0 {
			// filter by date
			let dates = self.sessionImages.compactMap { $0.SessionDate }.distinct()
			let regEx = /PHD2_GuideLog_(?<yyyy>.+?)-(?<MM>.+?)-(?<dd>.+?)_.+/
			var calendar = Calendar(identifier: .gregorian)
			calendar.timeZone = self.currentTimeZone
			
			var filtered = [URL]()
			
			for file in files {
				// PHD2_GuideLog_2024-05-15_223814
				if let matches = try? regEx.wholeMatch(in: file.lastPathComponent) {
					let year = Int(matches.yyyy)
					let month = Int(matches.MM)
					let day = Int(matches.dd)
					
					let components = DateComponents(year: year,
													month: month,
													day: day,
													hour: 0,
													minute: 0,
													second: 0)
					
					if let fileDate = calendar.date(from: components) {
						if dates.contains(where: { abs($0.date.distance(to: fileDate)) < 2.0 * 86400.0 }) {
							filtered.append(file)
						}
					}
				}
			}
			
			if filtered.count > 0 {
				return filtered
			}
		}
		
		
		return Array(files)
	}
	
	
}

