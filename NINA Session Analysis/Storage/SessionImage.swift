//
//  SessionImage.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 29.03.24.
//

import Foundation
import SwiftUI

public enum SessionItem: String, CaseIterable  {
	case INDEX = "Index"
	case EXPOSURE = "Exposure"
	case STARTTIME = "Time"
	case GAIN = "Gain"
	case OFFSET = "Offset"
	case CAMERA = "Camera"
	case CCDTEMP = "Temperature (Camera)"
	case TELESCOPE = "Telescope"
	case FOCALLENGTH = "Focallenght"
	case RA = "α"
	case DEC = "δ"
	case ALTITUDE = "Altitude"
	case AZIMUTH = "Azimuth"
	case AIRMASS = "Airmass"
	case PIERSIDE = "Pierside"
	case OBS_LAT = "Latitude"
	case OBS_LONG = "Longitude"
	case FILTER = "Filter"
	case TARGET = "Target"
	case TARGETROTATION = "Target Rotation"
	case FOCPOS = "Focuser Position"
	case FOCTEMP = "Focuser Temperature"
	case ROTATANG = "Rotationangle"
	case CLOUDCVR = "Cloud Coverage"
	case DEWPOINT = "Dew Point"
	case HUMIDITY = "Humidity"
	case PRESSURE = "Pressure"
	case AMBTEMP = "Ambient Temperature"
	case WINDDIR = "Wind Direction"
	case WINDSPD = "Wind Speed"
	case MOONALT = "Moon Altitude"
	case SUNALT = "Sun Altitude"
	case MOONAZ = "Moon Azimuth"
	case SUNAZ = "Sun Azimuth"
	case SUNANGLE = "Sun Angle"
	case MOONANGL = "Moon Angle"
	case HFR = "HFR"
	case FWHM = "FWHM"
	case ECCENTRICITY = "Eccentricity"
	case RMS = "RMS"
	case MEAN = "Mean"
	case MEDIAN = "Median"
	case RMSRA = "RMS(α)"
	case RMSDEC = "RMS(δ)"
	case RELRMSRADEC = "RMS(α)/RMS(δ)"
	case ADUMIN = "min(ADU)"
	case ADUMAX = "max(ADU)"
	case NUMSTARS = "#Stars"
	case DITHERRAPX = "Drift α(px)"
	case DITHERDECPX = "Drift δ(px)"
	case DITHERPX = "Drift (px)"
	case GuidingRAMaxPeak = "max(dα)"
	case GuidingRAMinPeak = "min(dα)"
	case GuidingDECMaxPeak = "max(dδ)"
	case GuidingDECMinPeak = "min(dδ)"
	case GuidingAvgRA = "avg(dα)"
	case GuidingAvgDEC = "avg(dδ)"
	case GuidingEastPulseCount = "EastPulses"
	case GuidingWestPulseCount = "WestPulses"
	case GuidingNorthPulseCount = "NorthPulses"
	case GuidingSouthPulseCount = "SouthPulses"
	case DeltaTemperature = "DeltaTemperature"
}

public struct GuidingMovementIssues {
	let insufficentMovements: [[GuidingFrame]]?
	let overshootMovements: [[GuidingFrame]]?
}


public struct GuidingIssues {
	let dec: GuidingMovementIssues?
	let ra: GuidingMovementIssues?
}

public enum MetaDataState: Int {
	case missing = 0
	case partial = 1
	case complete = 2
	
	public var icon: String {
		switch self {
			case .missing: return "xmark"
			case .partial: return "minus"
			case .complete: return "checkmark"
		}
	}
	
	public var color: Color {
		switch self {
			case .missing: return Color.red
			case .partial: return Color.yellow
			case .complete: return Color.green
		}
	}
}


public class SessionImage: Identifiable, Hashable, @unchecked Sendable {
	
	public let id: UUID = UUID()
	
	var index: Int
	var url: URL!
	var ra: Double?
	var dec: Double?
	var exposure: Float?
	var startExposure: Date?
	var startExposureUTC: Date?
	var gain: Int?
	var offset: Int?
	var camera: String? 
	var cameraTemp: Float?
	var bayerPattern: String = ""
	var filter: String = ""
	var telescope: String = ""
	var focalLength: Float?
	var pierSide: String = ""
	var altitude: Float?
	var azimuth: Float?
	var airmass: Float?
	var obsLat: Double?
	var obsLong: Double?
	var target: String = ""
	var targetRA: Double?
	var targetDEC: Double?
	var targetRotation: Float?
	var focusPosition: Float?
	var focusTemp: Float?
	var rotatorAngle: Float?
	var cloudCover: Float?
	var dewPoint: Float?
	var humidity: Float?
	var pressure: Float?
	var ambientTemp: Float?
	var windDirection: Float?
	var windSpeed: Float?
	var sunAngle: Float?
	var moonAngle: Float?
	var hfr: Float?
	var fwhm: Float?
	var eccentricity: Float?
	var rms: Float?
	var mean: Float?
	var median: Int?
	var aduMin: Int?
	var aduMax: Int?
	var numStars: Int?
	var hfrStdev: Float?
	var rmsRA: Float?
	var rmsDEC: Float?
	var moonAlt: Float?
	var moonAz: Float?
	var sunAlt: Float?
	var sunAz: Float?
	var pixSize: Float?
	var sizeX: Int?
	var sizeY: Int?
	var windGust: Float?
	var skyTemperature: Float?
	var skyBrightness: Float?
	var skyQuality: Float?
	var sessionDate: Date?
	var bad: Bool = false 
	var minPeakRA: Float?
	var maxPeakRA: Float?
	var minPeakDEC: Float?
	var maxPeakDEC: Float?
	var peaksRA: Int?
	var peaksDEC: Int?
	var avgGuideRA: Float?
	var avgGuideDEC: Float?
	var eastPulses: Int?
	var westPulses: Int?
	var northPulses: Int?
	var southPulses: Int?
	var eastPulseDurations: Float?
	var westPulseDurations: Float?
	var northPulseDurations: Float?
	var southPulseDurations: Float?
	var metaData: MetaDataState = .missing
	
	var guidingIssues: GuidingIssues? = nil
	
	var issues: Set<SessionIssues> = []
	
	public init(url: URL) {
		self.index = 0
		self.url = url
	}
	
	public static func == (lhs: SessionImage, rhs: SessionImage) -> Bool {
		lhs.id == rhs.id
	}
	
	public func hash(into hasher: inout Hasher) {
		hasher.combine(self.id)
	}
	
	var isFits: Bool {
		return self.url.pathExtension.contains("fit")
	}
	
	
	var ditherRA: Double? {
		if let tra = self.targetRA, let ra = self.ra {
			return (tra - ra)
		}
		return nil
	}
	
	var ditherRAPixel: Float? {
		if let px = self.pixSize, let dra = self.ditherRA, let focalLength = self.focalLength {
			let imageRes = Double(px)/Double(focalLength) * 206.265 // in arcsecs
			return Float(dra*3600.0/imageRes)
		}
		return nil
	}
	
	var ditherDEC: Double? {
		if let tdec = self.targetDEC, let dec = self.dec {
			return (tdec - dec)
		}
		return nil
	}
	
	var ditherDECPixel: Float? {
		if let px = self.pixSize, let dra = self.ditherDEC, let focalLength = self.focalLength {
			let imageRes = Double(px)/Double(focalLength) * 206.265 // in arcsecs
			return Float(dra*3600.0/imageRes)
		}
		return nil
	}
	
	var dither: Double? {
		if let tra = self.targetRA, let tdec = self.targetDEC, let ra = self.ra, let dec = self.dec {
			let targetPos = Vector3.from(ra: tra * Constants.Rad, dec: tdec * Constants.Rad)
			let pos = Vector3.from(ra: ra * Constants.Rad, dec: dec * Constants.Rad)
			
			return angle(targetPos, pos).toDegrees()
		}
		return nil
	}
	
	var ditherPixel: Float? {
		if let px = self.pixSize, let angle = self.dither, let focalLength = self.focalLength {
			let imageRes = Double(px)/Double(focalLength) * 206.265 // in arcsecs
			return Float(angle*3600.0/imageRes)
		}
		return nil
	}
	
	var PixelResolution: Float? {
		if let px = self.pixSize, let focalLength = self.focalLength {
			return Float(Double(px) / Double(focalLength) * 206.265) // in arcsecs
		}
		return nil
	}
	
	var deltaTemperature: Float? {
		if let aTemp = self.ambientTemp, let skyTemp = self.skyTemperature {
			return abs(skyTemp - aTemp)
		}
		return nil
	}
}

public struct ImageRectangle {
	var center: Polar
	var sx: Double
	var sy: Double
	var rotation: Double
}


public extension SessionImage {
	
	var Index: String {
		return "\(index)"
	}
	
	var begin: JulianDate? {
		if let utc = self.startExposureUTC {
			return JulianDate.fromDate(date: utc)
		}
		return nil
	}
	
	var end: JulianDate? {
		if let utc = self.startExposureUTC, let exp = self.exposure {
			return JulianDate.fromDate(date: utc).addSeconds(Double(exp))
		}
		return nil
	}
	
	var beginLocal: JulianDate? {
		if let start = self.startExposure {
			return JulianDate.fromDate(date: start)
		}
		return nil
	}
	
	var endLocal: JulianDate? {
		if let start = self.startExposure, let exp = self.exposure {
			return JulianDate.fromDate(date: start).addSeconds(Double(exp))
		}
		return nil
	}
	
	var relationRMSRADEC: Float? {
		if let ra = self.rmsRA, let dec = self.rmsDEC, let pxRes = self.PixelResolution {
			let a = max(ra, dec) / pxRes
			let b = min(ra, dec) / pxRes
			return sqrt(a*a - b*b)
		}
		return nil
	}
	
	var ImageSizePixel: String {
		if let x = self.sizeX, let y = self.sizeY {
			return "\(x) x \(y)"
		}
		return ""
	}
	
	var ImageResolution: (x: Float, y: Float)? {
		if let res = self.PixelResolution, let x = self.sizeX, let y = self.sizeY {
			let xx = (Float(x) * res) / 60.0
			let yy = (Float(y) * res) / 60.0
			
			return (x: xx, y: yy)
		}
		return nil
	}
	
	var BAD: String {
		return self.bad ? "❌" : "✅"
	}
	
	var FileName: String {
		return "\(url.lastPathComponent)"
	}
	
	var SessionDate: DateOfSession? {
		if let date = self.sessionDate {
			return DateOfSession(text: date.formatted(date: .abbreviated, time: .omitted), date: date)
		}
		if let start = self.startExposure {
			return DateOfSession(text: start.formatted(date: .abbreviated, time: .omitted), date: start)
		}
		return nil
	}
	
	var ExposureTime: String {
		if let start = self.startExposure, let exp = self.exposure {
			let begin = start.formatted(date: .omitted, time: .standard)
			let end = start.addingTimeInterval(TimeInterval(exp)).formatted(date: .omitted, time: .standard)
			return "\(begin)-\(end)"
		} else if let exp = self.exposure {
			return String(format: "%.0fs", exp)
		}
		return "???"
	}
	
	var ExposureBeginTime: String {
		if let start = self.startExposure {
			return start.formatted(date: .omitted, time: .standard)
		}
		return "???"
	}

	var ExposureEndTime: String {
		if let start = self.startExposure, let exp = self.exposure {
			return start.addingTimeInterval(TimeInterval(exp)).formatted(date: .omitted, time: .standard)
		}
		return "???"
	}
		
	var Exposure: String {
		if let exp = self.exposure {
			return String(format: "%.0fs", exp)
		}
		return "???"
	}
	
	var RA: String {
		if let ra = self.ra {
			return ValueFormatter.formatRectascension(Angle.degrees(ra))
		}
		return "???"
	}
	
	var DEC: String {
		if let dec = self.dec {
			return ValueFormatter.formatDeclination(Angle.degrees(dec))
		}
		return "???"
	}
	
	var Gain: String {
		if gain != nil {
			return "\(gain!)"
		}
		return ""
	}
	
	var Offset: String {
		if offset != nil {
			return "\(offset!)"
		}
		return ""
	}
	
	var Camera: String {
		return camera ?? ""
	}
	
	var CameraTemperature: String {
		if cameraTemp != nil {
			return String(format: "%2.1f", cameraTemp!)
		}
		return ""
	}
	
	var FocuserPosition: String {
		if focusPosition != nil {
			return String(format: "%d", Int(focusPosition!))
		}
		return ""
	}
	
	var Airmass: String {
		if airmass != nil {
			return String(format: "%4.3f", airmass!)
		}
		return ""
	}
	
	var Altitude: String {
		if altitude != nil {
			return ValueFormatter.formatAltitude(Angle.degrees(Double(altitude!)))
		}
		return ""
	}
	
	var Azimuth: String {
		if azimuth != nil {
			return ValueFormatter.formatAzimuth(Angle.degrees(Double(azimuth!)))
		}
		return ""
	}
	
	var HFR: String {
		if hfr != nil {
			return String(format: "%3.2f", hfr!)
		}
		return ""
	}
	
	var HFRStdDev: String {
		if hfrStdev != nil {
			return String(format: "%.3f", hfrStdev!)
		}
		return ""
	}
	
	var FWHM: String {
		if fwhm != nil {
			return String(format: "%3.1f", fwhm!)
		}
		return ""
	}
	
	var Eccentricity: String {
		if eccentricity != nil {
			return String(format: "%3.2f", eccentricity!)
		}
		return ""
	}

	var RMS: String {
		if rms != nil {
			return String(format: "%3.2f", rms!)
		}
		return ""
	}
	
	var RMSRA: String {
		if rmsRA != nil {
			return String(format: "%3.2f", rmsRA!)
		}
		return ""
	}
	
	var RMSDEC: String {
		if rmsDEC != nil {
			return String(format: "%3.2f", rmsDEC!)
		}
		return ""
	}
	
	var RelationRMSRADEC: String {
		if let rel = self.relationRMSRADEC {
			return String(format: "%.3f", rel)
		}
		return ""
	}

	var Mean: String {
		if mean != nil {
			return String(format: "%5.1f", mean!)
		}
		return ""
	}
	
	var Median: String {
		if median != nil {
			return "\(median!)"
		}
		return ""
	}
	
	var ADUMin: String {
		if aduMin != nil {
			return "\(aduMin!)"
		}
		return ""
	}
	
	var ADUMax: String {
		if aduMax != nil {
			return "\(aduMax!)"
		}
		return ""
	}
	
	var NumStars: String {
		if numStars != nil {
			return "\(numStars!)"
		}
		return ""
	}
	
	var RotatorAngle: String {
		if let ra = self.rotatorAngle {
			return String(format: "%.1f°", ra)
		}
		return ""
	}
	
	var FocalLength: String {
		if let fl = self.focalLength {
			return String(format: "%.1fmm", fl)
		}
		return ""
	}
	
	var PixelSize: String {
		if let pxS = self.pixSize {
			return String(format: "%.2fμm", pxS)
		}
		return ""
	}
	
	var MoonAltitude: String {
		if moonAlt != nil {
			return ValueFormatter.formatAltitude(Angle.degrees(Double(moonAlt!)))
		}
		return ""
	}
	
	var MoonAzimuth: String {
		if moonAz != nil {
			return ValueFormatter.formatAzimuth(Angle.degrees(Double(moonAz!)))
		}
		return ""
	}
	
	var SunAltitude: String {
		if sunAlt != nil {
			return ValueFormatter.formatAltitude(Angle.degrees(Double(sunAlt!)))
		}
		return ""
	}
	
	var SunAzimuth: String {
		if sunAz != nil {
			return ValueFormatter.formatAzimuth(Angle.degrees(Double(sunAz!)))
		}
		return ""
	}
	
	var MoonAngle: String {
		if moonAngle != nil {
			return ValueFormatter.formatAzimuth(Angle.degrees(Double(moonAngle!)))
		}
		return ""
	}
	
	var SunAngle: String {
		if sunAngle != nil {
			return ValueFormatter.formatAzimuth(Angle.degrees(Double(sunAngle!)))
		}
		return ""
	}
	
	var CloudCover: String {
		if cloudCover != nil {
			return String(format: "%.1f", cloudCover!)
		}
		return ""
	}
	
	var Pressure: String {
		if pressure != nil {
			return String(format: "%.1f hPa", pressure!)
		}
		return ""
	}
	
	var Humidity: String {
		if humidity != nil {
			return String(format: "%.1f%", humidity!)
		}
		return ""
	}
	
	var WindSpeed: String {
		if windSpeed != nil {
			return String(format: "%.1f km/h", windSpeed!)
		}
		return ""
	}
	
	var AmbientTemperature: String {
		if ambientTemp != nil {
			return String(format: "%.1f°C", ambientTemp!)
		}
		return ""
	}
	
	var SkyTemperature: String {
		if skyTemperature != nil {
			return String(format: "%.1f°C", skyTemperature!)
		}
		return ""
	}
	
	var SkyQuality: String {
		if skyQuality != nil {
			return String(format: "%.1f mag", skyQuality!)
		}
		return ""
	}
	
	var WindDirection: String {
		if windDirection != nil {
			return String(format: "%.1f°", windDirection!)
		}
		return ""
	}
	
	var WindGust: String {
		if windGust != nil {
			return String(format: "%.1f", windGust!)
		}
		return ""
	}
	
	var SkyBrightness: String {
		if skyBrightness != nil {
			return String(format: "%.1f mag", skyBrightness!)
		}
		return ""
	}
	
	var DriftRAPixel: String {
		if let rapx = self.ditherRAPixel {
			return String(format: "%.1f", rapx)
		}
		return ""
	}
	
	var DriftDECPixel: String {
		if let decpx = self.ditherDECPixel {
			return String(format: "%.1f", decpx)
		}
		return ""
	}
	
	var DriftPixel: String {
		if let px = self.ditherPixel {
			return String(format: "%.1f", px)
		}
		return ""
	}
	
	var DewPoint: String {
		if let dp = self.dewPoint {
			return String(format: "%.1f°C", dp)
		}
		return ""
	}
	
	var FocuserTemperature: String {
		if let ct = self.focusTemp {
			return String(format: "%.1f°C", ct)
		}
		return ""
	}
	
	var GuidingRAPeakToPeak: String {
		if let minra = self.minPeakRA, let maxra = self.maxPeakRA {
			return String(format: "%.1f\" - %.1f\"", minra, maxra)
		}
		return ""
	}

	var GuidingDECPeakToPeak: String {
		if let mindec = self.minPeakDEC, let maxdec = self.maxPeakDEC {
			return String(format: "%.1f\" - %.1f\"", mindec, maxdec)
		}
		return ""
	}

	var AvgGuidingRA: String {
		if let avg = self.avgGuideRA {
			return String(format: "%.3f\"", avg)
		}
		return ""
	}
	
	var AvgGuidingDEC: String {
		if let avg = self.avgGuideDEC {
			return String(format: "%.3f\"", avg)
		}
		return ""
	}
	
	var NorthPulseCount: String {
		if let np = self.northPulses {
			return String(format: "%d", np)
		}
		return ""
	}
	
	var SouthPulseCount: String {
		if let np = self.southPulses {
			return String(format: "%d", np)
		}
		return ""
	}
	
	var EastPulseCount: String {
		if let np = self.eastPulses {
			return String(format: "%d", np)
		}
		return ""
	}
	
	var WestPulseCount: String {
		if let np = self.westPulses {
			return String(format: "%d", np)
		}
		return ""
	}
	
	var DeltaTemperature: String {
		if let np = self.deltaTemperature {
			return String(format: "%.1f°C", np)
		}
		return ""
	}
	
	func getImageRectangle(useTargetRotation: Bool) -> ImageRectangle? {
		let rotation = useTargetRotation ? self.targetRotation ?? 0.0 : self.rotatorAngle ?? 0.0
		if let sx = self.sizeX, let sy = self.sizeY, let px = self.pixSize, let focalLength = self.focalLength, let ra = self.ra, let dec = self.dec {
			let imageRes = Double(px)/Double(focalLength) * 206.265 // in arcsecs
			let resDeg = imageRes/3600.0
			let center = Vector3.from(ra: ra * Constants.Rad, dec: dec * Constants.Rad)
			let dx = Double(sx) * resDeg
			let dy = Double(sy) * resDeg
			
			return ImageRectangle(center: center.toPolar(), sx: dx, sy: dy, rotation: Double(rotation))
		}
		return nil
	}
	
	func Mark() {
		if !self.bad {
			self.bad = !self.bad
			addBadMarking()
		}
	}
	
	func UnMark() {
		if self.bad {
			self.bad = !self.bad
			removeBadMarking()
		}
	}
	
	func toggleMarked() {
		
		if self.bad {
			self.bad = !self.bad
			removeBadMarking()
		} else {
			self.bad = !self.bad
			addBadMarking()
		}
	}
	
	func removeBadMarking() {
		do {
			let fileManager = FileManager.default
			var pathComponents = self.url.pathComponents
			let originalPath = NSString.path(withComponents: pathComponents)
			pathComponents[pathComponents.count - 1] = pathComponents[pathComponents.count - 1].replacingOccurrences(of: "BAD_", with: "")
			let newFilePath = NSString.path(withComponents: pathComponents)
			let newURL = URL(fileURLWithPath: newFilePath)
			try fileManager.moveItem(atPath: originalPath, toPath: newFilePath)
			self.url = newURL
			
		} catch let error as NSError {
			print("Ooops! Something went wrong: \(error)")
		}
	}
	
	func addBadMarking() {
		let fileManager = FileManager.default
		var pathComponents = self.url.pathComponents
		
		let originalPath = NSString.path(withComponents: pathComponents)
		if pathComponents.last!.contains("BAD_") {
			return
		}
		
		pathComponents[pathComponents.count - 1] = "BAD_" + pathComponents.last!
		let newFilePath = NSString.path(withComponents: pathComponents)
		
		do {
			
			let newURL = URL(fileURLWithPath: newFilePath)
			try fileManager.moveItem(atPath: originalPath, toPath: newFilePath)
			self.url = newURL
		} catch let error as NSError {
			print("Ooops! Something went wrong: \(error)")
		}
	}
}
