//
//  Session+Guiding.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 13.04.24.
//

import Foundation

extension SessionData {
	
	func analyzeGuiding(for images: [SessionImage], pixelScaleThreshold: Float = 0.6) {
		
		if let pixelScale = self.getPixelScale() {
			let threshold = pixelScaleThreshold * pixelScale
			let minThreshold = threshold * 0.5
			
			for image in images.filter({ $0.startExposureUTC != nil && $0.begin != nil && $0.end != nil }) {
				let frames = self.guiding.getFrames(between: image.begin!, and: image.end!)
				if frames.isEmpty { continue }
								
				let (raRate, decRate) = self.guiding.getGuidingRates(between: image.begin!, and: image.end!)
				
				var minPeakRA: Float = 0.0
				var maxPeakRA: Float = 0.0
				var minPeakDEC: Float = 0.0
				var maxPeakDEC: Float = 0.0
				var sumRaDistance: Float = 0.0
				var sumDECDistance: Float = 0.0
				var peakRACount: Int = 0
				var peakDECCount: Int = 0
				var eastPulseCount: Int = 0
				var westPulseCount: Int = 0
				var northPulseCount: Int = 0
				var southPulseCount: Int = 0
				var sumEastPulses: Float = 0.0
				var sumWestPulses: Float = 0.0
				var sumNorthPulses: Float = 0.0
				var sumSouthPulses: Float = 0.0
				
				var sumPosRADistance: Float = 0.0
				var sumNegRADistance: Float = 0.0
				var sumPosDECDistance: Float = 0.0
				var sumNegDECDistance: Float = 0.0
				
				var previousFrame: GuidingFrame? = nil
				
				var notEnoughMovementFramesRA: [[GuidingFrame]] = []
				var notEnoughMovementFramesDEC: [[GuidingFrame]] = []
				var currentNotEnoughMovementFramesRA: [GuidingFrame] = []
				var currentNotEnoughMovementFramesDEC: [GuidingFrame] = []
				
				var overshootFramesRA: [[GuidingFrame]] = []
				var overshootFramesDEC: [[GuidingFrame]] = []
				var currentOvershootFramesRA: [GuidingFrame] = []
				var currentOvershootFramesDEC: [GuidingFrame] = []
				
				for frame in frames {
					
					minPeakRA = min(minPeakRA, frame.raRawDistance)
					maxPeakRA = max(maxPeakRA, frame.raRawDistance)
					sumRaDistance += frame.raRawDistance
					if abs(frame.raRawDistance) > threshold {
						peakRACount += 1
					}
					
					if frame.raRawDistance >= 0.0 {
						sumPosRADistance += frame.raRawDistance
					} else {
						sumNegRADistance += frame.raRawDistance
					}
					
					minPeakDEC = min(minPeakDEC, frame.decRawDistance)
					maxPeakDEC = max(maxPeakDEC, frame.decRawDistance)
					sumDECDistance += frame.decRawDistance
					
					if abs(frame.decRawDistance) > threshold {
						peakDECCount += 1
					}
					
					if frame.decRawDistance >= 0.0 {
						sumPosDECDistance += frame.decRawDistance
					} else {
						sumNegDECDistance += frame.decRawDistance
					}
					
					if frame.raDirection == "W" {
						let pulseRA = frame.raDuration * -1.0 * raRate/1000.0
						westPulseCount += 1
						sumWestPulses += pulseRA
					} else {
						let pulseRA = frame.raDuration * raRate/1000.0
						eastPulseCount += 1
						sumEastPulses += pulseRA
					}
					
					if frame.decDirection == "S" {
						let pulseDEC = frame.decDuration * -1.0 * decRate/1000.0
						southPulseCount += 1
						sumSouthPulses += pulseDEC
					} else {
						let pulseDEC = frame.decDuration * decRate/1000.0
						northPulseCount += 1
						sumNorthPulses += pulseDEC
					}
					
					if let prev = previousFrame {
						// check for overshooting/not enough moving
						
						if prev.raRawDistance * frame.raRawDistance < 0.0 {
							// changed sign
							let diff = abs(prev.raRawDistance) + abs(frame.raRawDistance)
							if diff >= pixelScale {
								if currentOvershootFramesRA.isEmpty {
									currentOvershootFramesRA.append(prev)
								}
								currentOvershootFramesRA.append(frame)
							} else if currentOvershootFramesRA.isNotEmpty {
								overshootFramesRA.append(currentOvershootFramesRA)
								currentOvershootFramesRA = []
							}
							
							if currentNotEnoughMovementFramesRA.isNotEmpty {
								notEnoughMovementFramesRA.append(currentNotEnoughMovementFramesRA)
								currentNotEnoughMovementFramesRA = []
							}
							
						} else if abs(frame.raRawDistance) >= threshold
									|| (abs(frame.raRawDistance) > minThreshold && currentNotEnoughMovementFramesRA.isNotEmpty) {
							// same sign...
							if currentNotEnoughMovementFramesRA.isEmpty {
								currentNotEnoughMovementFramesRA.append(prev)
							}
							
							currentNotEnoughMovementFramesRA.append(frame)
							
							if currentOvershootFramesRA.isNotEmpty {
								overshootFramesRA.append(currentOvershootFramesRA)
								currentOvershootFramesRA = []
							}
							
						} else {
							// normal guiding behavior
							
							if currentNotEnoughMovementFramesRA.isNotEmpty {
								notEnoughMovementFramesRA.append(currentNotEnoughMovementFramesRA)
								currentNotEnoughMovementFramesRA = []
							}
							
							if currentOvershootFramesRA.isNotEmpty {
								overshootFramesRA.append(currentOvershootFramesRA)
								currentOvershootFramesRA = []
							}
						}
						
						if prev.decRawDistance * frame.decRawDistance < 0.0 {
							// changed sign
							let diff = abs(prev.decRawDistance) + abs(frame.decRawDistance)
							if diff >= threshold {
								if currentOvershootFramesDEC.isEmpty {
									currentOvershootFramesDEC.append(prev)
								}
								currentOvershootFramesDEC.append(frame)
							} else if currentOvershootFramesDEC.isNotEmpty {
								overshootFramesDEC.append(currentOvershootFramesDEC)
								currentOvershootFramesDEC = []
							}
							
							if currentNotEnoughMovementFramesDEC.isNotEmpty {
								notEnoughMovementFramesDEC.append(currentNotEnoughMovementFramesDEC)
								currentNotEnoughMovementFramesDEC = []
							}
							
						} else if abs(frame.decRawDistance) > threshold || (abs(frame.decRawDistance) > minThreshold && currentNotEnoughMovementFramesDEC.isNotEmpty) {
							// same sign...
							if currentNotEnoughMovementFramesDEC.isEmpty {
								currentNotEnoughMovementFramesDEC.append(prev)
							}
							
							currentNotEnoughMovementFramesDEC.append(frame)
							
							if currentOvershootFramesDEC.isNotEmpty {
								overshootFramesDEC.append(currentOvershootFramesDEC)
								currentOvershootFramesDEC = []
							}
							
						} else {
							// normal guiding behavior
							
							if currentNotEnoughMovementFramesDEC.isNotEmpty {
								notEnoughMovementFramesDEC.append(currentNotEnoughMovementFramesDEC)
								currentNotEnoughMovementFramesDEC = []
							}
							
							if currentOvershootFramesDEC.isNotEmpty {
								overshootFramesDEC.append(currentOvershootFramesDEC)
								currentOvershootFramesDEC = []
							}
						}
						
					}
					
					previousFrame = frame
				}
				
				image.minPeakRA = minPeakRA
				image.maxPeakRA = maxPeakRA
				image.minPeakDEC = minPeakDEC
				image.maxPeakDEC = maxPeakDEC
				image.peaksRA = peakRACount
				image.peaksDEC = peakDECCount
				image.avgGuideRA = sumRaDistance/Float(frames.count)
				image.avgGuideDEC = sumDECDistance/Float(frames.count)
				image.eastPulses = eastPulseCount
				image.westPulses = westPulseCount
				image.northPulses = northPulseCount
				image.southPulses = southPulseCount
				image.eastPulseDurations = sumEastPulses
				image.westPulseDurations = sumWestPulses
				image.northPulseDurations = sumNorthPulses
				image.southPulseDurations = sumSouthPulses
				
				
				if overshootFramesRA.isNotEmpty || overshootFramesDEC.isNotEmpty
					|| notEnoughMovementFramesRA.isNotEmpty || notEnoughMovementFramesDEC.isNotEmpty {
					
					let raIssues = overshootFramesRA.isNotEmpty || notEnoughMovementFramesRA.isNotEmpty
									? GuidingMovementIssues(insufficentMovements: notEnoughMovementFramesRA, overshootMovements: overshootFramesRA)
									: nil
					
					let decIssues = overshootFramesDEC.isNotEmpty || notEnoughMovementFramesDEC.isNotEmpty
									? GuidingMovementIssues(insufficentMovements: notEnoughMovementFramesDEC, overshootMovements: overshootFramesDEC)
									: nil
					
					let guidingIssues = GuidingIssues(dec: decIssues, ra: raIssues)
					image.guidingIssues = guidingIssues
				}
				
			}
		}
	}
	
}
