//
//  SignalToNoiseImageAnalyzer.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 19.06.24.
//

import Foundation

struct SignalToNoise {
	let signal: [Float]
	let noise: [Float]
	
	var avgSignal: Float {
		let sum = signal.reduce(0, +)
		return sum / Float(signal.count)
	}

	var avgNoise: Float {
		let sum = noise.reduce(0, +)
		return sum / Float(noise.count)
	}
}

/*
 
 class SignalToNoiseImageAnalyzer {
 private let apertureRadius: Int
 private let innerRadius: Int
 private let outerRadius: Int
 
 init(apertureRadius: Int = 10, innerRadius: Int = 15, outerRadius: Int = 20) {
 self.apertureRadius = apertureRadius
 self.innerRadius = innerRadius
 self.outerRadius = outerRadius
 }
 
 func getSignalAndNoise(image: AstroImage) async -> SignalToNoise? {
 let starDetector = StarDetector()
 let stars = await starDetector.detectStars(in: image)
 if stars.count > 0 {
 return getSignalAndNoise(image: image, stars: stars)
 }
 return nil
 }
 
 
 func getSignalAndNoise(image: AstroImage, stars: [DetectedStar]) -> SignalToNoise {
 let snrs = calculateSNR(image: image, stars: stars, apertureRadius: self.apertureRadius,
 innerRingRadius: self.innerRadius, outerRingRadius: self.outerRadius)
 
 return SignalToNoise(signal: snrs.map({ $0.signal }),
 noise: snrs.map({ $0.noise }))
 }
 
 
 private func calculateSNR(image: AstroImage, stars: [DetectedStar],
 apertureRadius: Int, innerRingRadius: Int, outerRingRadius: Int) -> [(signal: Float, noise: Float)] {
 var snrValues: [(signal: Float, noise: Float)] = []
 
 for star in stars {
 let (signal, _, noise) = calculateAperturePhotometry(image: image, centerX: star.x, centerY: star.y,
 apertureRadius: apertureRadius,
 innerRingRadius: innerRingRadius, outerRingRadius: outerRingRadius)
 
 snrValues.append((signal: signal, noise: noise))
 }
 
 return snrValues
 }
 
 func calculateAperturePhotometry(image: AstroImage, centerX: Int, centerY: Int,
 apertureRadius: Int, innerRingRadius: Int, outerRingRadius: Int) -> (signal: Float, background: Float, noise: Float) {
 var signal: Float = 0
 var background: Float = 0
 var backgroundPixelCount: Int = 0
 
 let width = image.width
 let height = image.height
 let apertureArea = Float.pi * pow(Float(apertureRadius), 2)
 
 for y in max(centerY - outerRingRadius, 0)..<min(centerY + outerRingRadius, height) {
 for x in max(centerX - outerRingRadius, 0)..<min(centerX + outerRingRadius, width) {
 let distance = sqrt(pow(Float(x - centerX), 2) + pow(Float(y - centerY), 2))
 let index = y * width + x
 let pixelValue = image.imageData[index].float
 
 if distance <= Float(apertureRadius) {
 signal += pixelValue
 } else if distance > Float(innerRingRadius) && distance <= Float(outerRingRadius) {
 background += pixelValue
 backgroundPixelCount += 1
 }
 }
 }
 
 let backgroundMean = background / Float(backgroundPixelCount)
 let signalWithoutBackground = signal - (backgroundMean * apertureArea)
 
 let photonNoise = sqrt(signal)
 let readoutNoise: Float = 10 // Typischer Wert, kann angepasst werden
 let backgroundNoise = sqrt(backgroundMean * apertureArea)
 
 let totalNoise = sqrt(photonNoise * photonNoise + readoutNoise * readoutNoise + backgroundNoise * backgroundNoise)
 
 return (signalWithoutBackground, backgroundMean, totalNoise)
 }
 }
 */
