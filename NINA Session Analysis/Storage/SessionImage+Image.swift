//
//  SessionImage+Image.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 19.06.24.
//

import Foundation
import CoreImage
import CoreGraphics
import Accelerate
import AppKit



extension SessionImage {
	
	
	func computeMedian(pixels: [UInt8], width: Int, height: Int) -> [Float] {
		let pixelCount = width * height
		let sortedPixels = pixels.sorted()
		
		var median: Float = 0.0
		if pixelCount % 2 == 0 {
			median = (Float(sortedPixels[pixelCount / 2 - 1]) + Float(sortedPixels[pixelCount / 2])) / 2.0
		} else {
			median = Float(sortedPixels[pixelCount / 2])
		}
		
		return [median / 255.0]
	}

	func computeMAD(pixels: [UInt8], median: [Float], width: Int, height: Int) -> [Float] {
		let pixelCount = width * height
		let med = median[0] * 255.0
		
		var deviations = [Float](repeating: 0.0, count: pixelCount)
		for i in 0..<pixelCount {
			deviations[i] = abs(Float(pixels[i]) - med)
		}
		
		let sortedDeviations = deviations.sorted()
		var mad: Float = 0.0
		if pixelCount % 2 == 0 {
			mad = (sortedDeviations[pixelCount / 2 - 1] + sortedDeviations[pixelCount / 2]) / 2.0
		} else {
			mad = sortedDeviations[pixelCount / 2]
		}
		
		return [mad / 255.0]
	}


	func mtfStretch(image: CGImage, m: Float = 0.25, sigma: Float = 0.5) -> CGImage? {
		let width = image.width
		let height = image.height

		let colorSpace = image.colorSpace ?? CGColorSpace(name: CGColorSpace.sRGB)!
		let bytesPerPixel = image.bitsPerPixel / 8
		let bitmapInfo = image.alphaInfo == .none ? CGImageAlphaInfo.none.rawValue : CGImageAlphaInfo.premultipliedLast.rawValue

		guard let context = CGContext(data: nil,
									  width: width,
									  height: height,
									  bitsPerComponent: image.bitsPerComponent,
									  bytesPerRow: width * bytesPerPixel,
									  space: colorSpace,
									  bitmapInfo: bitmapInfo) else {
			return nil
		}

		context.draw(image, in: CGRect(x: 0, y: 0, width: width, height: height))

		guard let data = context.data else {
			return nil
		}

		let pixelCount = width * height
		let rawPointer = data.bindMemory(to: UInt8.self, capacity: width * height * bytesPerPixel)
		let pixelBuffer = UnsafeMutableRawBufferPointer(start: rawPointer, count: width * height * bytesPerPixel)

		let channelCount = bytesPerPixel / (image.bitsPerComponent / 8)
		var floatPixels = [[Float]](repeating: [Float](repeating: 0.0, count: pixelCount), count: channelCount)

		for i in 0..<pixelCount {
			for channel in 0..<channelCount {
				let offset = i * bytesPerPixel + channel * (image.bitsPerComponent / 8)
				let value: Float

				switch image.bitsPerComponent {
				case 8:
					value = Float(pixelBuffer[offset]) / 255.0
				case 16:
					value = Float(pixelBuffer.load(fromByteOffset: offset, as: UInt16.self)) / 65535.0
				case 32:
					value = Float(pixelBuffer.load(fromByteOffset: offset, as: UInt32.self)) / 4294967295.0
				default:
					return nil
				}

				floatPixels[channel][i] = value
			}
		}

		for channel in 0..<channelCount {
			var meanValue: Float = 0.0
			vDSP_meanv(floatPixels[channel], 1, &meanValue, vDSP_Length(pixelCount))

			for i in 0..<pixelCount {
				let normalizedValue = (floatPixels[channel][i] - meanValue) / sigma
				let stretchedValue = meanValue + sigma * (pow(normalizedValue, 1.0 / m) - 1.0)
				floatPixels[channel][i] = max(0, min(1, stretchedValue))
			}
		}

		for i in 0..<pixelCount {
			for channel in 0..<channelCount {
				let offset = i * bytesPerPixel + channel * (image.bitsPerComponent / 8)
				let value = UInt8(floatPixels[channel][i] * 255.0)
				pixelBuffer[offset] = value
			}
		}

		guard let stretchedImage = context.makeImage() else {
			return nil
		}

		return stretchedImage
	}


	
	
	func getImageData() async -> AstroImage? {
		if let imageURL = self.url {
			
			if self.isFits {
				
				
				if let fits = await FITS.read(from: imageURL), let width = self.sizeX, let height = self.sizeY {
					let bm = fits.header.getBayerMatrix() ?? .none
					let bitDepth = fits.header.getBitDepth()
					
					return AstroImage(imageData: fits.primary.data,
									  width: width, height: height, bayerMatrix: bm,
									  bitDepth: bitDepth)
				}
				
			} else {
				
				
				if let nsImage = NSImage(contentsOf: imageURL) {
					
					var bitDepth: Int = 16
					
					if let cgImage = nsImage.toCGImage() {
						if let exifData = getExifData() {
							if let depth = exifData["Depth"] as? Int {
								bitDepth = depth
							}
						}
						
						return AstroImage(cgImage: cgImage, bitDepth: bitDepth, bayerMatrix: BayerMatrix.none)
					}
				}
			}
		}
		
		return nil
	}
	
	
	private func getExifData() -> [NSString: Any]? {
		if let data = NSData(contentsOf: self.url),
		   let source = CGImageSourceCreateWithData(data as CFData, nil) {
			if let metadata = CGImageSourceCopyPropertiesAtIndex(source, 0, nil) as? [NSString: Any] {
				return metadata
			}
		}
		return nil
	}

}
