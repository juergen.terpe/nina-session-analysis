//
//  PeriodicGuidingAnalyzer.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 01.01.25.
//
import Foundation
import Accelerate
import CubicSpline


struct PeriodicGuidingCurve {
	let times: [Double]   // x-Werte (Zeit)
	let values: [Float]   // y-Werte (Amplitude)
	let period: Double    // Dominante Periode in Sekunden
}

class PeriodicGuidingAnalyzer {
	private var threshold: Float
	private var periodicityTolerance: Double
	private var amplitudeTolerance: Float
	
	init(threshold: Float = 0.15, periodicityTolerance: Double = 0.5, amplitudeTolerance: Float = 0.05) {
		self.threshold = threshold
		self.periodicityTolerance = periodicityTolerance
		self.amplitudeTolerance = amplitudeTolerance
	}
	
	func analyze(data: [GuidingFrame], start jd: Double) -> PeriodicGuidingCurve? {
		// Filter gültige Frames
		let validData = data.filter { !$0.drop && !$0.settlingAfterDither && $0.errorCode == 0 }
		guard validData.count > 1 else { return nil }
		
		// Extrahiere Zeit- und Magnitudendaten
		let times = validData.map { ($0.time - jd) * 86400.0 }
		let magnitudes = validData.map { $0.raRawDistance }
		
		// Spline-Glättung
		let smoothedMagnitudes = smoothWithSpline(x: times, y: magnitudes)
		
		// FFT-Analyse und Kurvenrekonstruktion
		return performFFTAnalysis(times: times, values: smoothedMagnitudes)
	}
	
	private func smoothWithSpline(x: [Double], y: [Float]) -> [Float] {
		// Erstelle Punkte als SIMD2<Double> (x, y)
		let points = zip(x, y.map { Double($0) }).map { SIMD2($0.0, $0.1) }
		
		// Initialisiere den Spline (nicht geschlossen)
		let spline = CubicSpline(points: points, closed: false)
		
		// Interpoliere die Werte an den ursprünglichen x-Positionen
		let smoothedY = x.map { Float(spline.callAsFunction(t: $0).y) }
		return smoothedY
	}
	
	private func performFFTAnalysis(times: [Double], values: [Float]) -> PeriodicGuidingCurve? {
		let n = values.count
		let log2n = vDSP_Length(log2(Double(n)))
		
		// Speicher für Real- und Imaginärteile
		var real = values
		var imaginary = [Float](repeating: 0.0, count: n)
		
		// Initialisiere UnsafeMutablePointer
		let realPointer = UnsafeMutablePointer<Float>.allocate(capacity: n)
		let imaginaryPointer = UnsafeMutablePointer<Float>.allocate(capacity: n)
		defer {
			realPointer.deallocate()
			imaginaryPointer.deallocate()
		}
		
		// Kopiere Werte in die Pointer
		realPointer.initialize(from: &real, count: n)
		imaginaryPointer.initialize(from: &imaginary, count: n)
		
		var splitComplex = DSPSplitComplex(realp: realPointer, imagp: imaginaryPointer)
		let fftSetup = vDSP_create_fftsetup(log2n, FFTRadix(kFFTRadix2))!
		
		// FFT ausführen
		vDSP_fft_zip(fftSetup, &splitComplex, 1, log2n, FFTDirection(FFT_FORWARD))
		
		// Magnituden berechnen
		var magnitudes = [Float](repeating: 0.0, count: n / 2)
		vDSP_zvmags(&splitComplex, 1, &magnitudes, 1, vDSP_Length(n / 2))
		
		// Dominante Frequenz finden
		guard let maxIndex = magnitudes.enumerated().max(by: { $0.element < $1.element })?.offset else {
			return nil
		}
		
		let frequency = Double(maxIndex) / (times.last! - times.first!)
		let period = 1.0 / frequency
		
		// Rekonstruiere die periodische Kurve
		var reconstructedValues = [Float]()
		let amplitude = sqrt(real[maxIndex] * real[maxIndex] + imaginary[maxIndex] * imaginary[maxIndex]) / Float(n)
		
		for time in times {
			let phase = 2.0 * Double.pi * frequency * time
			let value = amplitude * Float(cos(phase))
			reconstructedValues.append(value)
		}
		
		vDSP_destroy_fftsetup(fftSetup)
		return PeriodicGuidingCurve(times: times, values: reconstructedValues, period: period)
	}
}
