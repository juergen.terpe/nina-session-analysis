//
//  LinearFit.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 08.05.24.
//

import Foundation

class LinearFit {
	
	var avx, avy, varx, covxy, vary, n : Double
	
	init() {
		self.avx = 0.0
		self.avy = 0.0
		self.varx = 0.0
		self.vary = 0.0
		self.covxy = 0.0
		self.n = 0.0
	}
	
	func add(x: Double, y: Double) {
		var k = n
		self.n += 1.0
		k /= self.n
		let dx = x - self.avx
		let dy = y - self.avy
		self.varx += (k * dx * dx - self.varx) / self.n
		self.covxy += (k * dx * dy - self.covxy) / self.n
		self.vary += (k * dy * dy - self.vary) / self.n
		self.avx += dx / self.n
		self.avy += dy / self.n
	}
	
	func reset() {
		self.avx = 0.0
		self.avy = 0.0
		self.varx = 0.0
		self.vary = 0.0
		self.covxy = 0.0
		self.n = 0.0
	}
	
	func getB() -> Double {
		return self.n >= 2.0 ? self.covxy / self.varx : 0.0
	}
	
	func getA() -> Double {
		return self.avy - getB() * self.avx
	}
	
	func result(a: inout Double, b: inout Double) {
		b = getB()
		a = self.avy - b * self.avx
	}
	
	func theta() -> Double {
		return n >= 2.0 ? atan2(covxy, varx) : 0.0
	}
}


struct Line {
	private let a: Double
	private let b: Double
	
	init(lfit: LinearFit) {
		var a = 0.0
		var b = 0.0
		lfit.result(a: &a, b: &b)
		self.a = a
		self.b = b
	}
	
	func interpolate(_ t: Double) -> Double {
		return a + b * t
	}
}




