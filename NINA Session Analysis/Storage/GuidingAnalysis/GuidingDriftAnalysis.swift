//
//  GuidingDriftAnalysis.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 08.05.24.
//

import Foundation
import Accelerate


struct DriftAnalysis: Identifiable {
	let id = UUID()
	let ra: Double
	let dec: Double
	let rms_ra: Double
	let rms_dec: Double
	let avg_ra: Double
	let avg_dec: Double
	let peak_ra: Double
	let peak_dec: Double
	let elongation: Double
	let errorPA: Double
	let pixelScale: Double
	
	var rmsRAPixel: String {
		return String(format: "%.2fpx", rms_ra)
	}
	
	func rmsRAPixelCamera(pixelScale: Float) -> String {
		return String(format: "%.2fpx", rms_ra * self.pixelScale / Double(pixelScale))
	}
	
	var rmsDECPixel: String {
		return String(format: "%.2fpx", rms_dec)
	}

	func rmsDECPixelCamera(pixelScale: Float) -> String {
		return String(format: "%.2fpx", rms_dec * self.pixelScale / Double(pixelScale))
	}
	
	var rmsRAArcSeconds: String {
		return String(format: "%.2f\"", rms_ra * pixelScale)
	}
	
	var rmsDECArcSeconds: String {
		return String(format: "%.2f\"", rms_dec * pixelScale)
	}
	
	var peakRAPixel: String {
		return String(format: "%.2fpx", peak_ra)
	}
	
	var peakDECPixel: String {
		return String(format: "%.2fpx", peak_dec)
	}
	
	func peakRAPixelCamera(pixelScale: Float) -> String {
		return String(format: "%.2fpx", peak_ra * self.pixelScale / Double(pixelScale))
	}
	
	func peakDECPixelCamera(pixelScale: Float) -> String {
		return String(format: "%.2fpx", peak_dec * self.pixelScale / Double(pixelScale))
	}
	
	var peakRAArcSeconds: String {
		return String(format: "%.2f\"", peak_ra * pixelScale)
	}
	
	var peakDECArcSeconds: String {
		return String(format: "%.2f\"", peak_dec * pixelScale)
	}
	
	var elongationPercent: String {
		return String(format: "%.1f%%", elongation * 100.0)
	}
	
	var raDriftPixel: String {
		return String(format: "%.2f px", ra)
	}

	var decDriftPixel: String {
		return String(format: "%.2f px", dec)
	}
	
	func raDriftPixelCamera(pixelScale: Float) -> String {
		return String(format: "%.2f px", ra * self.pixelScale / Double(pixelScale))
	}
	
	func decDriftPixelCamera(pixelScale: Float) -> String {
		return String(format: "%.2f px", dec * self.pixelScale / Double(pixelScale))
	}
	
	var raDriftArcSeconds: String {
		return String(format: "%.2f \"", ra * pixelScale)
	}

	var decDriftArcSeconds: String {
		return String(format: "%.2f \"", dec * pixelScale)
	}

	var errorPolarAlignment: String {
		return String(format: "%.2f'", errorPA)
	}
}


class GuidingDriftAnalysis {
	
	private func decDrift(frames: [GuidingFrame]) -> Double {
		if frames.count < 2 {
			return 0.0
		}
		
		if let _ = frames.first {
			
			var y_accum = 0.0
			var prev_y = 0.0
			var prev_guided = false
			
			let fit = LinearFit()
			
			for (index, frame) in frames.filter({ !$0.drop && !$0.settlingAfterDither && ($0.errorCode == 0 || $0.errorCode == 1) })
										.enumerated() {
				if index == 0 {
					prev_y = Double(frame.decRawDistance)
					prev_guided = abs(frame.decDuration) > 0.0
					fit.add(x: Double(frame.timeOffset), y: y_accum)
					
				} else {
					let y = Double(frame.decRawDistance)
					if !prev_guided {
						let dy = y - prev_y
						y_accum += dy
						fit.add(x: Double(frame.timeOffset), y: y_accum)
					}
					prev_y = Double(y)
					prev_guided = abs(frame.decDuration) > 0.0
				}
			}
			
			return fit.getB()
		}
		
		return 0.0
	}
	
	private func raDrift(frames: [GuidingFrame]) -> Double {
		
		var ra0: Double = 0.0
		var t0: Double = 0.0
		var sum: Double = 0.0
		
		var ra1: Double = 0.0
		var t1: Double = 0.0
		
		for (index, frame) in frames.filter({ !$0.drop && !$0.settlingAfterDither && ($0.errorCode == 0 || $0.errorCode == 1) })
									.enumerated() {
			if index == 0 {
				ra0 = Double(frame.raRawDistance)
				t0 = Double(frame.timeOffset)
				
				sum += abs(frame.raDuration) > 0.0 ? Double(frame.raGuideDistance) : 0.0
				
			} else {
				sum += abs(frame.raDuration) > 0.0 ? Double(frame.raGuideDistance) : 0.0
			}
		}
		
		if let lastFrame = frames.last(where: { !$0.drop && !$0.settlingAfterDither && ($0.errorCode == 0 || $0.errorCode == 1) }) {
			ra1 = Double(lastFrame.raRawDistance)
			t1 = Double(lastFrame.timeOffset)
			
			return t1 > t0 ? (ra1 - ra0 - sum) / (t1 - t0) : 0.0
		}
		
		return 0.0
	}
	
	func calculateStatistics(frames: [GuidingFrame], pixelScale: Double, dec: Double) -> DriftAnalysis {
		let fit = LinearFit()
		var peak_r = 0.0
		var peak_d = 0.0
		
		for frame in frames.filter({ !$0.drop && !$0.settlingAfterDither && ($0.errorCode == 0 || $0.errorCode == 1) }) {
			fit.add(x: Double(frame.raRawDistance), y: Double(frame.decRawDistance))
			
			if abs(Double(frame.raRawDistance)) > abs(peak_r) {
				peak_r = Double(frame.raRawDistance)
			}
			if abs(Double(frame.decRawDistance)) > abs(peak_d) {
				peak_d = Double(frame.decRawDistance)
			}
		}
		
		let rms_ra = sqrt(fit.varx)
		let rms_dec = sqrt(fit.vary)
		let avg_ra = fit.avx
		let avg_dec = fit.avy
		
		// angle of elongation
		let theta = fit.theta()
		// now get variances of the transformed coordinates offset by the
		// mean and rotated by theta
		let cost = cos(theta)
		let sint = sin(theta)
		
		let fitxy = LinearFit()
		for frame in frames.filter({ !$0.drop && !$0.settlingAfterDither && ($0.errorCode == 0 || $0.errorCode == 1) }) {
			let dr = Double(frame.raRawDistance) - avg_ra
			let dd = Double(frame.decRawDistance) - avg_dec
			let x = dr * cost + dd * sint
			let y = dd * cost - dr * sint

			fitxy.add(x: x, y: y)
		}
		
		let lx = sqrt(fitxy.varx)
		let ly = sqrt(fitxy.vary)
		let a = max(lx, ly)
		let b = min(lx, ly)
		let elongation = (a + b) > 1e-6 ? (a - b) / (a + b) : 1.0
		
		// drift in pixels per minute
		let drift_ra = raDrift(frames: frames) * 60.0
		let drift_dec = decDrift(frames: frames) * 60.0
		let paerror = 3.8197 * abs(drift_dec) * pixelScale / cos(dec * Constants.Rad)
		
		return DriftAnalysis(ra: drift_ra,
							 dec: drift_dec, 
							 rms_ra: rms_ra,
							 rms_dec: rms_dec,
							 avg_ra: avg_ra,
							 avg_dec: avg_dec,
							 peak_ra: peak_r,
							 peak_dec: peak_d,
							 elongation: elongation, 
							 errorPA: paerror,
							 pixelScale: pixelScale)
	}
	
	
	private func segmentData(frames: [GuidingFrame]) -> [[GuidingFrame]] {
		var segments = [[GuidingFrame]]()
		var currentSegment = [GuidingFrame]()
		var currentExposure: Float? = nil
		
		for frame in frames.filter({ !$0.drop && !$0.settlingAfterDither && $0.errorCode == 0 }) {
			if let exposure = currentExposure, exposure != frame.exposure {
				segments.append(currentSegment)
				currentSegment = [GuidingFrame]()
			}
			currentSegment.append(frame)
			currentExposure = frame.exposure
		}
		
		if !currentSegment.isEmpty {
			segments.append(currentSegment)
		}
		
		return segments
	}

	private func prepareFFT(frames: [GuidingFrame]) -> ([Float], Double) {
		
		let fitra = LinearFit()
		var rapos = 0.0
		var prev_raguide = 0.0
		var prev_raraw = 0.0
		
		var t = [Double]()
		var ra = [Double]()
		var rac = [Double]()
		
		let tOffset = frames.first!.time
		
		for frame in frames {
			let raraw = Double(frame.raRawDistance)
			let raguide = Double(frame.raGuideDistance)
			let move = raraw - prev_raraw - prev_raguide
			rapos += move
			prev_raraw = raraw
			prev_raguide = raguide
			
			let dt = 86400.0 * (frame.time - tOffset)
			t.append(dt)
			ra.append(rapos)
			
			fitra.add(x: dt, y: rapos)
		}
		
		// drift correction
		let line = Line(lfit: fitra)
		
		for i in stride(from: 0, to: t.count, by: 1) {
			rac.append(ra[i] - line.interpolate(t[i]))
		}
		
		let n = rac.count
		//let k = Double.pi * 2.0 / Double(rac.count - 1)
		
		var data = Array<Float>(repeating: 0.0, count: n * 2)
		let dt = 2.5 // (t[n - 1] - t[0]) / Double(n - 1)
		
		for i in 0..<n {
			data[i * 2] *= Float(rac[i])
			data[i * 2 + 1] = 0.0 // complex
		}
		
		return (data, dt)
		
		/*
		let spline = Spline(arguments: t, values: rac, boundaryCondition: .smooth)
		let k = Double.pi * 2.0 / Double(rac.count - 1)
		let n = rac.count
		var data = Array<Float>(repeating: 0.0, count: n * 2)

		let dt = (t[n - 1] - t[0]) / Double(n - 1)
		var x = t[0]
		for i in stride(from: 0, to: n, by: 1) {
			x += dt
			if x > t[n - 1] {
				x = t[n - 1]
			}
			
			let hw = 0.54 - 0.46 * cos(Double(i) * k)
			data[i * 2] = Float(hw * spline.f(t: x)) // real
			data[i * 2 + 1] = 0.0 // complex
		}
		
		return (data, dt)
		*/
	}
	
	
	func fastFourierTransform(on frames: [GuidingFrame],
							  pixelScale: Double) async -> [(period: Float, magnitude: Float)] {
		
		let usedFrames = frames.filter({ !$0.drop && !$0.settlingAfterDither && ($0.errorCode == 0 || $0.errorCode == 1) })
		
		let segmentedFrames = segmentData(frames: usedFrames)
		guard let longestFrames = segmentedFrames.sorted(by: { $0.count > $1.count }).first, longestFrames.count >= 2
		else { return [] }
		
		//let timeDifferences = zip(longestFrames.dropFirst(), longestFrames).map { ($0.time - $1.time)*86400.0 }
		//let sampleRate = 1.0 / (timeDifferences.reduce(0, +) / Double(timeDifferences.count))
		
		let (data, dt) = prepareFFT(frames: longestFrames)
		
		let length = data.count/2
		let log2n = vDSP_Length(log2(Float(length)))
		
		var realp = [Float](repeating: 0.0, count: length / 2)
		var imagp = [Float](repeating: 0.0, count: length / 2)
		
		var magnitudes = [Float](repeating: 0.0, count: length / 2)
		
		realp.withUnsafeMutableBufferPointer { realpPointer in
			imagp.withUnsafeMutableBufferPointer { imagpPointer in
				var splitComplex = DSPSplitComplex(realp: realpPointer.baseAddress!, imagp: imagpPointer.baseAddress!)
				data.withUnsafeBufferPointer { dataPointer in
					dataPointer.baseAddress!.withMemoryRebound(to: DSPComplex.self, capacity: length) { typeConvertedBaseAddress in
						vDSP_ctoz(typeConvertedBaseAddress, 2, &splitComplex, 1, vDSP_Length(length / 2))
					}
					
					let fftSetup = vDSP_create_fftsetup(log2n, FFTRadix(kFFTRadix2))
					vDSP_fft_zrip(fftSetup!, &splitComplex, 1, log2n, FFTDirection(FFT_FORWARD))
					
					vDSP_zvmags(&splitComplex, 1, &magnitudes, 1, vDSP_Length(length / 2))
					
					vDSP_destroy_fftsetup(fftSetup)
				}
			}
		}
		
		var magnitudesSqrt = [Float](repeating: 0.0, count: length / 2)
		vvsqrtf(&magnitudesSqrt, magnitudes, [Int32(length / 2)])
		
		// Keine explizite Normierung
		let result = (1..<length / 2).map { index -> (period: Float, magnitude: Float) in
			let frequency = Double(index) / (Double(length) * dt)
			let p = Float(1.0 / frequency)
			return (p, magnitudesSqrt[index] * Float(pixelScale))
		}
		
		return result.reversed()
		
		/*
		let length = longestFrames.count
		let log2n = vDSP_Length(log2(Float(length)))
		let normLength = length/2
		
		// Erstellen und Initialisieren der komplexen Eingabedaten
		var realp = [Float](repeating: 0.0, count: length / 2)
		var imagp = [Float](repeating: 0.0, count: length / 2)
		
		var magnitudes = [Float](repeating: 0.0, count: length / 2)
		
		realp.withUnsafeMutableBufferPointer { realpPointer in
			imagp.withUnsafeMutableBufferPointer { imagpPointer in
				var splitComplex = DSPSplitComplex(realp: realpPointer.baseAddress!, imagp: imagpPointer.baseAddress!)
				data.withUnsafeBufferPointer { dataPointer in
					dataPointer.baseAddress!.withMemoryRebound(to: DSPComplex.self, capacity: length) { typeConvertedBaseAddress in
						vDSP_ctoz(typeConvertedBaseAddress, 2, &splitComplex, 1, vDSP_Length(normLength))
					}
					
					// Erstellen des FFT-Setups
					let fftSetup = vDSP_create_fftsetup(log2n, FFTRadix(kFFTRadix2))
					
					// FFT durchführen
					vDSP_fft_zrip(fftSetup!, &splitComplex, 1, log2n, FFTDirection(FFT_FORWARD))
					
					// Berechnung der Magnitude
					vDSP_zvmags(&splitComplex, 1, &magnitudes, 1, vDSP_Length(normLength))
					
					// Freigeben des FFT-Setups
					vDSP_destroy_fftsetup(fftSetup)
				}
			}
		}
		
		// Ziehen der Quadratwurzel, um die Magnitude zu erhalten
		var magnitudesSqrt = [Float](repeating: 0.0, count: normLength)
		var magLength = Int32(normLength)
		magnitudes.withUnsafeBufferPointer { magnitudesPointer in
			magnitudesSqrt.withUnsafeMutableBufferPointer { magnitudesSqrtPointer in
				vvsqrtf(magnitudesSqrtPointer.baseAddress!, magnitudesPointer.baseAddress!, &magLength)
			}
		}
		
		// Normalisieren der Magnitude
		var normalizedMagnitudes = [Float](repeating: 0.0, count: normLength)
		let scale = Float(10.0 / Float(length))
		vDSP_vsmul(&magnitudesSqrt, 1, [scale], &normalizedMagnitudes, 1, vDSP_Length(normLength))
		
		// Berechnen der Frequenzen
		let nyquistFrequency = 2.0 * sampleRate
		let frequencyStep = nyquistFrequency / Double(normLength)
		
		let result = (1..<normLength).map { index -> (period: Float, magnitude: Float) in
			//let f = Double(index + 1) / (Double(normLength) * dt)
			//let p = 1.0 / f
			let frequency = Double(index) * frequencyStep
			let p = Float(1.0 / frequency)
			let a = normalizedMagnitudes[index] * Float(pixelScale)
			return (p, a)
		}.filter({ $0.period > 10.0 })
		
		return result.reversed()*/
	}
}
