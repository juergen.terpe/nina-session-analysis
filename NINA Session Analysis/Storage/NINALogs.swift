//
//  NINALogs.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 19.04.24.
//

import Foundation

struct NINALog: Identifiable {
	let id = UUID()
	
	let begin: Date
	let end: Date
	let url: URL
	
	let lines: [NINALogLine]
}

enum NINALogType: String, CaseIterable {
	case empty = ""
	case debug = "DEBUG"
	case info = "INFO"
	case warn = "WARN"
	case error = "ERROR"
}

enum NINAAutoFocusState {
	case none
	case initial
	case final
	case moving
}


/// "DATE|LEVEL|SOURCE|MEMBER|LINE|MESSAGE"
struct NINALogLine: Identifiable, Hashable {
	let id: UUID
	let time: Date
	let level: NINALogType
	let source: String
	let member: String
	let line: Int
	let text: String
	let timeText: String
	let attachedLines: [String]
	
	func hash(into hasher: inout Hasher) {
		hasher.combine(id)
	}
	
	var lineText: String {
		if attachedLines.isEmpty {
			return text
		} else {
			return text + attachedLines.joined(separator: "\n")
		}
	}
	
}

struct NINACategory: Hashable, Identifiable {
	var id: String {
		return "\(category): \(item)"
	}
	
	let category: String
	let item: String
	
	func hash(into hasher: inout Hasher) {
		hasher.combine(category)
		hasher.combine(item)
	}
}


class NINALogs {
	var logs: [NINALog] = []
	var categories: Set<NINACategory> = []
 
	func clear() {
		self.logs = []
		self.categories = []
	}
	
	var count: Int {
		return logs.count
	}
	
	var dates: [Date] {
		self.logs.map({ $0.begin.midDay }).distinct()
	}
	
	
	func scan(_ url: URL, dates: [DateOfSession], timeZone: TimeZone, onLoaded: @escaping (String) -> Void) async {
		// Get list of files to process
		let files = scanLogFolder(url, dates: dates, timeZone: timeZone)
		
		// Use TaskGroup to manage parallel tasks
		await withTaskGroup(of: Void.self) { taskGroup in
			// Iterate over each file and add task to TaskGroup
			for file in files {
				taskGroup.addTask {
					// Perform async operation to read NINA log
					await self.readNINALog(file)
					
					// Execute onLoaded delegate on the main actor
					await MainActor.run {
						onLoaded(file.path())
					}
				}
			}
			
			// Wait for all tasks to complete
			for await _ in taskGroup {}
		}
	}

	private func getAutoFocusImage(_ image: SessionImage) -> AutoFocuserImage? {
		if let exp = image.exposure, let mean = image.mean, let hfr = image.hfr  {
			return AutoFocuserImage(id: image.id,
									filter: image.filter,
									exposure: exp, hfr: hfr, mean: mean)
		}
		return nil
	}
	
	
	func analyzeAutoFocusRuns(with session: SessionData) async -> [AutoFocus] {
		
		let regExMovePos = /Moving Focuser to position (?<pos>.+?)/
		let regExHFR = /Average HFR: (?<hfr>.+?), HFR MAD: (?<mad>.+?), Detected Stars (?<stars>.+?),.+/
		let regExValidateFinal = /Validating HFR at final focus position (?<pos>.+?)/
		
		var results = [AutoFocus]()
		
		for log in self.logs {
			if !log.lines.isEmpty {
				
				var begin: Date? = nil
				var initialPos: Int? = nil
				var initialHFR: Float? = nil
				var finalPos: Int? = nil
				var finalHFR: Float? = nil
				var position = Queue<Int>()
				var positions: [AutoFocuserPoint] = []
				var afState: NINAAutoFocusState = .none
				for line in log.lines {
					if line.text.contains("Starting Category:") && line.text.contains(" Item: RunAutofocus") {
						begin = line.time
						afState = .initial
						positions = []
						initialHFR = nil
						finalHFR = nil
						finalPos = nil
						initialPos = nil
						position.removeAll()
					} else if begin != nil {
						if line.text.contains("Finishing Category:") && line.text.contains(" Item: RunAutofocus") {
							let end = line.time
							
							if let initHFR = initialHFR, let finHFR = finalHFR, positions.count > 0,
								let initPos = initialPos, let finPos = finalPos {
								
								positions.sort(by: { $0.position < $1.position })
								
								let startAF = JulianDate.fromLocalDate(date: begin!, timeZone: session.currentTimeZone)
								let endAF = JulianDate.fromLocalDate(date: end, timeZone: session.currentTimeZone)
								
								let prevImages = session.getPreviousImages(for: startAF, limit: 5)
														.compactMap({ getAutoFocusImage( $0 ) })
								let nextImages = session.getNextImages(for: endAF, limit: 5)
														.compactMap({ getAutoFocusImage($0)})
								
								
								results.append(AutoFocus(start: begin!, end: end,
														 initialPosition: initPos, initialHFR: initHFR,
														 finalPosition: finPos, finalHFR: finHFR,
														 points: positions,
														 previousImages: prevImages,
														 nextImages: nextImages))
							}
							
							begin = nil
							afState = .none
							positions = []
							initialHFR = nil
							finalHFR = nil
							finalPos = nil
							initialPos = nil
							position.removeAll()
						}
						else if line.text.contains("Waiting on initial HFR analysis") {
							afState = .initial
						} 
						else if line.text.contains("Moving Focuser to position") {
							if let matches = try? regExMovePos.wholeMatch(in: line.text) {
								switch afState {
									case .initial:
										initialPos = Int(matches.pos)
									case .final:
										finalPos = Int(matches.pos)
									case .moving:
										if let pos = Int(matches.pos) {
											position.enque(pos)
										}
									default:
										break
								}
							}
						}
						else if line.text.contains("Validating HFR at final") {
							afState = .final
							position.removeAll()
							if let matches = try? regExValidateFinal.wholeMatch(in: line.text) {
								finalPos = Int(matches.pos)
							}
						}
						else if line.text.contains("Average HFR:") {
							if let matches = try? regExHFR.wholeMatch(in: line.text) {
								if let hfr = Float(matches.hfr),
								   let mad = Float(matches.mad),
								   let stars = Int(matches.stars) {
									
									if afState == .initial {
										initialHFR = hfr
										afState = .moving
									} else if afState == .final {
										finalHFR = hfr
									} else if afState == .moving, let pos = position.deque() {
										positions.append(AutoFocuserPoint(position: pos, HFR: hfr, mad: mad, stars: stars))
									}
								} else {
									position.deque()
								}
							}
							else {
								position.deque()
							}
						}
					}
				}
			}
		}
		
		return results
	}
	
	
	func scanLogFolder(_ url: URL, dates: [DateOfSession], timeZone: TimeZone) -> [URL] {
	
		var files = Set<URL>()
		var fileURLS = Stack<URL>()
		
		if let enumerator = FileManager.default.enumerator(at: url, includingPropertiesForKeys: [.isRegularFileKey], options: [.skipsHiddenFiles, .skipsPackageDescendants]) {
			for case let fileURL as URL in enumerator {
				fileURLS.push(fileURL)
			}
			
			while fileURLS.count > 0 {
				if let fileURL = fileURLS.pop() {
					
					do {
						let fileAttributes = try fileURL.resourceValues(forKeys:[.isRegularFileKey, .isAliasFileKey])
						if fileAttributes.isAliasFile! {
							let destURL = try URL(resolvingAliasFileAt: fileURL)
							let destAttributes = try destURL.resourceValues(forKeys:[.isRegularFileKey, .isDirectoryKey])
							
							if destAttributes.isRegularFile! {
								files.insert(destURL)
							} else if destAttributes.isDirectory! {
								if let subEnum = FileManager.default.enumerator(at: destURL.resolvingSymlinksInPath(),
																				includingPropertiesForKeys: [.isRegularFileKey, .isSymbolicLinkKey],
																				options: [.skipsHiddenFiles, .skipsPackageDescendants]) {
									for case let fileURL as URL in subEnum {
										fileURLS.push(fileURL)
									}
								}
							}
							
						} else if fileAttributes.isRegularFile! && fileURL.pathExtension.lowercased() == "log" {
							files.insert(fileURL)
						}
					}
					catch {
						print(error, fileURL)
					}
				}
			}
		}
		
		if files.count > 1, dates.count > 0 {
			// filter by date
			
			var calendar = Calendar(identifier: .gregorian)
			calendar.timeZone = timeZone
			
			var filtered = [URL]()
			
			for file in files {
				// 20240508-220545-3.1.0.1006.8268-202405.log
				let fname = file.lastPathComponent
				let yyyy = fname[0...3]
				let mm = fname[4...5]
				let dd = fname[6...7]
				
				if let year = Int(yyyy), let month = Int(mm), let day = Int(dd) {
					
					let components = DateComponents(year: year,
													month: month,
													day: day,
													hour: 0,
													minute: 0,
													second: 0)
					
					if let fileDate = calendar.date(from: components) {
						if dates.contains(where: { abs($0.date.distance(to: fileDate)) < 1.5 * 86400.0 }) {
							filtered.append(file)
						}
					}
				}
			}
			
			if filtered.count > 0 {
				var filenames = Set<String>()
				var result = [URL]()
								
				for file in filtered {
					let fname = file.lastPathComponent
					if filenames.contains(fname) {
						continue
					}
					filenames.insert(fname)
					result.append(file)
				}
				
				return result
			}
		}
		else {
			var filenames = Set<String>()
			var result = [URL]()
							
			for file in files {
				let fname = file.lastPathComponent
				if filenames.contains(fname) {
					continue
				}
				filenames.insert(fname)
				result.append(file)
			}
			
			return result
		}
		
		return Array(files)
	}
	
	
	func readNINALog(_ url: URL) async {
		
		guard let data = try? Data(contentsOf: url, options: .uncached) else {
			return
		}
		
		if let text = String(data: data, encoding: .utf8) {
			let lines = text.components(separatedBy: "\r\n")
			if lines.count > 0 {
				var logLines = [NINALogLine]()
	
				let dateFormatter = DateFormatter()
				dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSS"
				let regExCategory = /.* Category: (?<cat>.+?), Item: (?<item>[^\s]+?) .*/
				var categories = Set<NINACategory>()
			
				for line in lines {
					let parts = line.components(separatedBy: "|")
					if !parts.isEmpty && parts.count >= 6 {
						if let date = dateFormatter.date(from: parts[0]),
						   let type = NINALogType(rawValue: parts[1]),
						   let lineImdex = Int(parts[4]) {
							
							let codeFile = parts[2].trimmingCharacters(in: .whitespaces)
							let name =  parts[3].trimmingCharacters(in: .whitespaces)
							let text = parts[5].trimmingCharacters(in: .whitespaces)
							
							
							logLines.append(NINALogLine(id: UUID(),
														time: date,
														level: type,
														source: codeFile,
														member: name, line: lineImdex,
														text: text,
														timeText: date.formatted(date: .omitted, time: .shortened),
													    attachedLines: []))
							
							
							if let m = try? regExCategory.wholeMatch(in: text) {
								let category = String(m.cat)
								let item = String(m.item).trimmingCharacters(in: ["'",",",":"])
								
								if category.contains("Instruction Set") {
									continue
								}
								
								categories.insert(NINACategory(category: category, item: item))
							}
						}
					}
					else if let lastLine = logLines.last, (lastLine.level == .warn || lastLine.level == .error) {
						var attached = Array(lastLine.attachedLines)
						attached.append(line)
						attached.removeAll(where: { $0 == ""})
						logLines.removeLast()
						logLines.append(NINALogLine(id: UUID(),
													time: lastLine.time,
													level: lastLine.level,
													source: lastLine.source,
													member: lastLine.member,
													line: lastLine.line,
													text: lastLine.text,
													timeText: lastLine.timeText,
													attachedLines: attached))
					}
				}
				
				if logLines.count > 0 {
					let begin = logLines.first!.time
					let end = logLines.last!.time
					
					let log = NINALog(begin: begin, end: end, url: url, lines: logLines.sorted(by: { $0.time < $1.time }))
					let cat = categories
					await MainActor.run {
						self.logs.append(log)
						self.categories.formUnion(cat)
					}
				}
			}
		}
	}
	
	private func getLevels(_ level: NINALogType) -> Set<NINALogType> {
		switch level {
			case .debug: return [NINALogType.debug, NINALogType.info, NINALogType.warn, NINALogType.error]
			case .info: return [NINALogType.info, NINALogType.warn, NINALogType.error]
			case .warn: return [NINALogType.warn, NINALogType.error]
			case .error: return [NINALogType.error]
			case .empty: return [NINALogType.debug, NINALogType.info, NINALogType.warn, NINALogType.error]
		}
		
	}
	
	func getLogData(between begin: Date, and end: Date, level: NINALogType) -> [NINALogLine] {
		
		var result = [NINALogLine]()
		let levels = getLevels(level)
		
		let beginOfDay = begin.startOfDay
		let endOfDay = end.endOfDay
		
		for log in logs {
			for line in log.lines.filter({ levels.contains($0.level) && $0.time >= beginOfDay && $0.time <= endOfDay }) {
				result.append(line)
			}
		}
		
		return result.sorted(by: { $0.time < $1.time }).distinct()
	}
	
	func hasLogData(startingAt begin: Date, duration: TimeInterval) -> Bool {
		
		
		let end = begin.addingTimeInterval(duration)
		
		for log in logs {
			for _ in log.lines.filter({ $0.time >= begin && $0.time <= end }) {
				return true
			}
		}
		
		return false 
	}
	
	
	func getLogData(startingAt begin: Date, duration: TimeInterval) -> [NINALogLine] {
		
		var result = [NINALogLine]()
		let end = begin.addingTimeInterval(duration)
		
		for log in logs {
			for line in log.lines.filter({ $0.time >= begin && $0.time <= end }) {
				result.append(line)
			}
		}
		
		return result.sorted(by: { $0.time < $1.time }).distinct()
	}
	
	
	func getLogData(for sessionDate: DateOfSession) -> [NINALogLine] {
		
		var result = [NINALogLine]()
		
		if sessionDate.text == "Any" {
			for log in logs {
				for line in log.lines {
					result.append(line)
				}
			}
			
			return result.sorted(by: { $0.time < $1.time }).distinct()
		}
		
		let begin = sessionDate.date.midDay
		let end = begin.addingTimeInterval(86400.0)
		for log in logs {
			for line in log.lines.filter({ $0.time >= begin && $0.time <= end }) {
				result.append(line)
			}
		}
		
		return result.sorted(by: { $0.time < $1.time }).distinct()
	}
	
	
	func getLogData(level: NINALogType) -> [NINALogLine] {
		
		var result = [NINALogLine]()
		let levels = getLevels(level)
		
		for log in logs {
			for line in log.lines.filter({ levels.contains($0.level) }) {
				result.append(line)
			}
		}
		
		return result.sorted(by: { $0.time < $1.time }).distinct()
	}
	
}
