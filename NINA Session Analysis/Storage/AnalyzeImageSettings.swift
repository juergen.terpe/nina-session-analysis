//
//  AnalyzeImageSettings.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 05.04.24.
//

import Foundation
import SwiftUI


class AnalyzeImageSettings {
	var meanTolerance: Float = 50.0
	var hfrTolerance: Float = 30.0
	var starCountTolerance: Float = 30.0
	var starEccentricityTolerance: Float = 0.45
	var rmsTolerance: Float = 80.0
	var humidityTolerance: Float = 60.0
	var cloudCoverageTolerance: Float = 10.0
	var windSpeedTolerance: Float = 10.0
	
	var hfrToleranceForFilter: [String : Float] = [:]
	var starCountToleranceForFilter: [String : Float] = [:]
	var meanToleranceForFilter: [String : Float] = [:]
	
	func readSettings() {
		let settings = UserDefaults.standard
		self.meanTolerance = settings.float(forKey: "meanToleranceValue")
		self.hfrTolerance = settings.float(forKey: "hfrToleranceValue")
		self.starCountTolerance = settings.float(forKey: "starCountToleranceValue")
		self.starEccentricityTolerance = settings.float(forKey: "starEccentricityToleranceValue")
		self.humidityTolerance = settings.float(forKey: "humidityToleranceValue")
		self.cloudCoverageTolerance = settings.float(forKey: "cloudCoverageToleranceValue")
		self.windSpeedTolerance = settings.float(forKey: "windSpeedToleranceValue")
		self.rmsTolerance = settings.float(forKey: "rmsToleranceValue")
	}
	
	
	func store() {
		let settings = UserDefaults.standard
		settings.set(self.meanTolerance, forKey: "meanToleranceValue")
		settings.set(self.hfrTolerance, forKey: "hfrToleranceValue")
		settings.set(self.starCountTolerance, forKey: "starCountToleranceValue")
		settings.set(self.starEccentricityTolerance, forKey: "starEccentricityToleranceValue")
		settings.set(self.humidityTolerance, forKey: "humidityToleranceValue")
		settings.set(self.cloudCoverageTolerance, forKey: "cloudCoverageToleranceValue")
		settings.set(self.windSpeedTolerance, forKey:  "windSpeedToleranceValue")
		settings.set(self.rmsTolerance, forKey: "rmsToleranceValue")
		
		for hfrFilter in self.hfrToleranceForFilter.keys {
			if let val = self.hfrToleranceForFilter[hfrFilter] {
				settings.set(val, forKey: "FilterHFR_\(hfrFilter)")
			}
		}
		
		for meanFilter in self.meanToleranceForFilter.keys {
			if let val = self.meanToleranceForFilter[meanFilter] {
				settings.set(val, forKey: "FilterMEAN_\(meanFilter)")
			}
		}
		
		for scFilter in self.starCountToleranceForFilter.keys {
			if let val = self.starCountToleranceForFilter[scFilter] {
				settings.set(val, forKey: "FilterStarCount_\(scFilter)")
			}
		}
		
	}
	
	
	func getHFRTolerance(_ filter: String) -> Float {
		if let hfr = hfrToleranceForFilter[filter] {
			return hfr/100.0
		}
		let settings = UserDefaults.standard
		let settingsHFR = settings.float(forKey: "FilterHFR_\(filter)")
		if settingsHFR > 0.1 {
			self.hfrToleranceForFilter[filter] = settingsHFR
			return settingsHFR/100.0
		}
				
		return hfrTolerance/100.0
	}
	
	func getStarCountTolerance(_ filter: String) -> Float {
		if let sc = self.starCountToleranceForFilter[filter] {
			return sc/100.0
		}
		let settings = UserDefaults.standard
		let settingsSC = settings.float(forKey: "FilterStarCount_\(filter)")
		if settingsSC > 0.1 {
			self.starCountToleranceForFilter[filter] = settingsSC
			return settingsSC/100.0
		}
		
		return starCountTolerance/100.0
	}
	
	func getMeanTolerance(_ filter: String) -> Float {
		if let mt = self.meanToleranceForFilter[filter] {
			return mt
		}
		let settings = UserDefaults.standard
		let settingsMean = settings.float(forKey: "FilterMEAN_\(filter)")
		if settingsMean > 0.0 {
			self.meanToleranceForFilter[filter] = settingsMean
			return settingsMean/100.0
		}

		return meanTolerance/100.0
	}
	
	func getStarEccentricityTolerance() -> Float {
		return self.starEccentricityTolerance
	}
	
	func getRMSTolerance() -> Float {
		return self.rmsTolerance/100.0
	}
	
	func getHumidityTolerance() -> Float {
		return self.humidityTolerance/100.0
	}
	
	func getCloudCoverageTolerance() -> Float {
		return self.cloudCoverageTolerance/100.0
	}
	
	func getWindSpeedTolerance() -> Float {
		return self.windSpeedTolerance/100.0
	}
}
