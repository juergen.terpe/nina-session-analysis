//
//  SessionImageStatistics.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 05.05.24.
//

import Foundation
import SigmaSwiftStatistics

extension SessionData {
	
	func correlationHFRRMS(images: [SessionImage]) -> Double? {
		let y = images.filter({ $0.hfr != nil }).map({ Double($0.hfr!) })
		let x = images.filter({ $0.rms != nil }).map({ Double($0.rms!) })
	
		if x.count == y.count && x.count > 0 {
			return Sigma.pearson(x: x, y: y)
		}
		return nil
	}
	
	func correlationEccentricityGuiding(images: [SessionImage]) -> Double? {
		let x = images.filter({ $0.relationRMSRADEC != nil }).map({ Double($0.relationRMSRADEC!) })
		let y = images.filter({ $0.eccentricity != nil }).map({ Double($0.eccentricity!) })
	
		if x.count == y.count && x.count > 0 {
			return Sigma.pearson(x: x, y: y)
		}
		return nil
	}
	
	func correlationAirmassHFR(images: [SessionImage]) -> Double? {
		let x = images.filter({ $0.airmass != nil }).map({ Double($0.airmass!) })
		let y = images.filter({ $0.hfr != nil }).map({ Double($0.hfr!) })
	
		if x.count == y.count && x.count > 0 {
			return Sigma.pearson(x: x, y: y)
		}
		return nil
	}
	
	func correlationAltitudeHFR(images: [SessionImage]) -> Double? {
		let x = images.filter({ $0.altitude != nil }).map({ Double($0.altitude!) })
		let y = images.filter({ $0.hfr != nil }).map({ Double($0.hfr!) })
	
		if x.count == y.count && x.count > 0 {
			return Sigma.pearson(x: x, y: y)
		}
		return nil
	}
	
	func correlationAltitudeMean(images: [SessionImage]) -> Double? {
		let x = images.filter({ $0.altitude != nil }).map({ Double($0.altitude!) })
		let y = images.filter({ $0.mean != nil }).map({ Double($0.mean!) })
	
		if x.count == y.count && x.count > 0 {
			return Sigma.pearson(x: x, y: y)
		}
		return nil
	}
	
	func correlationSunAltMean(images: [SessionImage]) -> Double? {
		let x = images.filter({ $0.sunAlt != nil }).map({ Double($0.sunAlt!) })
		let y = images.filter({ $0.mean != nil }).map({ Double($0.mean!) })
	
		if x.count == y.count && x.count > 0 {
			return Sigma.pearson(x: x, y: y)
		}
		return nil
	}
	
	func correlationMoonAltMean(images: [SessionImage]) -> Double? {
		let x = images.filter({ $0.moonAlt != nil }).map({ Double($0.moonAlt!) })
		let y = images.filter({ $0.mean != nil }).map({ Double($0.mean!) })
	
		if x.count == y.count && x.count > 0 {
			return Sigma.pearson(x: x, y: y)
		}
		return nil
	}
	
	func correlationDeltaTMean(images: [SessionImage]) -> Double? {
		let x = images.filter({ $0.deltaTemperature != nil }).map({ Double($0.deltaTemperature!) })
		let y = images.filter({ $0.mean != nil }).map({ Double($0.mean!) })
	
		if x.count == y.count && x.count > 0 {
			return Sigma.pearson(x: x, y: y)
		}
		return nil
	}
}
