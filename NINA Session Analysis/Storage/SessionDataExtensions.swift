//
//  SessionDataExtensions.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 05.04.24.
//

import Foundation
import SwiftUI

extension SessionData {
	
	public var hasMountPositions: Bool {
		self.sessionImages.contains(where: { $0.ra != nil && $0.dec != nil })
	}
	
	func resetDefaults() {
		let defaults = UserDefaults.standard
		let dictionary = defaults.dictionaryRepresentation()
		dictionary.keys.forEach { key in
			defaults.removeObject(forKey: key)
		}
	}
	
	
	public func analyzeImages() {
		
		let colorCamera = isColorCamera()
		let factor: Float = colorCamera ? 1.41 : 1.0
		let pxScale = getPixelScale()
		
		let settings = self.analyzationSettings
		
		let rmsMaxValue = pxScale != nil ? pxScale! * settings.rmsTolerance / 100.0  +  pxScale!/2.0 * factor : nil
		
		for target in self.targets {
			for filter in self.filters {
				for exposure in self.exposures {
					let images = self.getImages(for: target, exposure: exposure, filter: filter)
					let (minHFR, _) = minMaxHFR(images: images)
					let (minMean, _) = minMaxMean(images: images)
					
					for image in images {
						image.issues = []
						
						if image.aduMin != nil && image.aduMin! < 5 {
							image.issues.insert(SessionIssues.aduMinZero)
						}
						
						if image.aduMax != nil && image.aduMax! == 65535 {
							image.issues.insert(SessionIssues.aduMaxLimitReached)
						}
						
						if minHFR != nil && image.hfr != nil && image.hfr! > ((1.05 * minHFR!) + minHFR! * settings.getHFRTolerance(filter)) {
							image.issues.insert(SessionIssues.hfrIncreased)
						}
						
						if minMean != nil && image.mean != nil && image.mean! > ((1.10 * minMean!) + minMean! * settings.getMeanTolerance(filter)) {
							image.issues.insert(SessionIssues.highMeanValue)
						}
						
						if image.eccentricity != nil && image.eccentricity! > settings.getStarEccentricityTolerance() {
							image.issues.insert(SessionIssues.elongatedStars)
						}
						
						if rmsMaxValue != nil {
							
							if image.rms != nil && image.rms! > rmsMaxValue! {
								image.issues.insert(SessionIssues.badGuiding)
							}
							
							if image.rmsRA != nil && image.rmsRA! > rmsMaxValue! {
								image.issues.insert(SessionIssues.badGuiding)
							}
							
							if image.rmsDEC != nil && image.rmsDEC! > rmsMaxValue! {
								image.issues.insert(SessionIssues.badGuiding)
							}
						}
						
						if image.humidity != nil && image.humidity! > settings.getHumidityTolerance() {
							image.issues.insert(SessionIssues.highHumidity)
						}
						
						if image.cloudCover != nil && image.cloudCover! > settings.getCloudCoverageTolerance() {
							image.issues.insert(SessionIssues.highCloudCoverage)
						}
						
						if image.windSpeed != nil && image.windSpeed! > settings.getWindSpeedTolerance() {
							image.issues.insert(SessionIssues.highWindSpeeds)
						}
					}
					
				}
			}
		}
		
		self.analyzeGuiding(for: self.sessionImages, pixelScaleThreshold: settings.getRMSTolerance())
		
		self.analyzeAvailable = true
	}
	
	public func minMaxMean(images: [SessionImage]) -> (min: Float?, max: Float?) {
		var minMean: Float? = nil
		var maxMean: Float? = nil
		
		for image in images {
			if minMean == nil {
				minMean = image.mean
			} else if image.mean != nil {
				minMean = min(minMean!, image.mean!)
			}
			
			if maxMean == nil {
				maxMean = image.mean
			} else if image.mean != nil {
				maxMean = max(maxMean!, image.mean!)
			}
		}
		return (min: minMean, max: maxMean)
	}

	public func minMaxHFR(images: [SessionImage]) -> (min: Float?, max: Float?) {
		var minHFR: Float? = nil
		var maxHFR: Float? = nil
		
		for image in images.filter({ $0.hfr != nil && $0.hfr! > 0.0 }) {
			if minHFR == nil {
				minHFR = image.hfr
			} else if image.mean != nil {
				minHFR = min(minHFR!, image.hfr!)
			}
			
			if maxHFR == nil {
				maxHFR = image.hfr
			} else if image.hfr != nil {
				maxHFR = max(maxHFR!, image.hfr!)
			}
		}
		return (min: minHFR, max: maxHFR)
	}

	
	public func getSessionInfo() -> [SessionInfo] {
		var result = [SessionInfo]()
		
		let telescopes = Array(self.sessionImages.map { $0.telescope }.distinct())
		let cameras = Array(self.sessionImages.map { $0.camera }.filter({ $0 != nil }).map({ $0! }).distinct())
		
		var index = 0
		for camera in cameras {
			
			for telescope in telescopes {
				
				let images = self.sessionImages.filter({ $0.telescope == telescope && $0.camera == camera })
				if images.count > 0, let first = images.first {
					
					result.append(SessionInfo(id: index, name:  LocalizedStringKey("Telescope"), value: telescope))
					index += 1
					result.append(SessionInfo(id: index, name:  LocalizedStringKey("Focal length"), value: first.FocalLength))
					index += 1
					result.append(SessionInfo(id: index, name:  LocalizedStringKey("Camera"), value: camera))
					index += 1
					
					result.append(SessionInfo(id: index, name:  LocalizedStringKey("Camera PixelSize"), value: first.PixelSize))
					index += 1
					
					result.append(SessionInfo(id: index, name:  LocalizedStringKey("Image dimension"), value: first.ImageSizePixel))
					index += 1
					
					if let res = first.PixelResolution {
						result.append(SessionInfo(id: index, name:  LocalizedStringKey("Camera Resolution"), value: String(format: "%.2f\"", res)))
						index += 1
						
						if let fov = first.ImageResolution {
							result.append(SessionInfo(id: index, name:  LocalizedStringKey("Image FOV"),
													  value: String(format: "%.1f' x %.1f' (%.1f° x %.1f°)", fov.x, fov.y, fov.x/60.0, fov.y/60.0)))
							index += 1
						}
					}
					
					let filters = Array(images.map({ $0.filter }).distinct())
					if filters.count > 1 {
						for filter in filters {
							result.append(SessionInfo(id: index, name:  LocalizedStringKey("Filter"), value: filter))
							index += 1
							
							let imagesWithFilter = images.filter({ $0.filter == filter })
							
							let exposures = Array(imagesWithFilter.compactMap({ $0.exposure }).distinct())
							for exp in exposures {
								let expCount = imagesWithFilter.filter({ $0.exposure != nil && abs($0.exposure! - exp) < 1.0 }).count
								let totalSec = Float(expCount) * exp
								let totalMin = totalSec/60.0
								let totalHours = totalMin/60.0
								let hours = Int(totalHours)
								let minuts = Int(frac(Double(totalHours))*60 + 0.5)
								
								result.append(SessionInfo(id: index, name:  LocalizedStringKey(String(format: "Exposure %.1fs", exp)),
														  value: String(format: "%dh %dmin", hours, minuts),
														  indent: 1))
								index += 1
							}
							
						}
					} else {
						let exposures = Array(images.compactMap({ $0.exposure }).distinct())
						for exp in exposures {
							let expCount = images.filter({ $0.exposure != nil && abs($0.exposure! - exp) < 1.0 }).count
							let totalSec = Float(expCount) * exp
							let totalMin = totalSec/60.0
							let totalHours = totalMin/60.0
							let hours = Int(totalHours)
							let minuts = Int(frac(Double(totalHours))*60 + 0.5)
							
							result.append(SessionInfo(id: index, name: LocalizedStringKey(String(format: "Exposure %.1fs", exp)),
													  value: String(format: "%dh %dmin", hours, minuts), indent: 1))
							index += 1
						}
						
					}
					
					result.append(SessionInfo(id: index, name: "", value: "", divider: true))
					index += 1
					
					if self.guiding.guidingSessions.count > 0 {
						var hash = Set<Float>()
						
						for gs in self.guiding.guidingSessions {
							if let fl = gs.focalLength, let bn = gs.binning, let pxS = gs.pixelScale {
								if !hash.contains(pxS) {
									hash.insert(pxS)
									
									result.append(SessionInfo(id: index, name:  LocalizedStringKey("Guiding Focal Length"), value: String(format: "%.1fmm", fl)))
									index += 1
									result.append(SessionInfo(id: index, name:  LocalizedStringKey("Guiding Binning"), value: String(format: "%d x %d", bn, bn)))
									index += 1
									result.append(SessionInfo(id: index, name:  LocalizedStringKey("Guiding Resolution(\"/px)"), value: String(format: "%.2fμm", pxS)))
									index += 1
									
									if pxS < 0.5 {
										result.append(SessionInfo(id: index, name:  LocalizedStringKey("Hint"), 
																  value: LocalizedStringKey("Guiding resolution too large! Should be greater than 0.5\"/px").stringValue()!, hint: true))
										index += 1
									}
									

									if let pxRes = first.PixelResolution {
										let d = pxRes / pxS
										result.append(SessionInfo(id: index, name:  LocalizedStringKey("Guiding : ImageScale"), value: String(format: "1 : %.1f", pxS)))
										index += 1
										
										if d > 5.0 {
											result.append(SessionInfo(id: index, name:  LocalizedStringKey("Hint"), 
																	  value: LocalizedStringKey("Guiding scale relation too large!").stringValue()!, hint: true))
											index += 1
										}
									}
								}
							}
						}
					}
				}
			}
			
		}
		
		
		
		let aduMin = self.sessionImages.filter({ $0.aduMin != nil && $0.isFits }).map({ $0.aduMin! }).distinct().sorted()
		if let lowestADU = aduMin.first {
			if lowestADU == 0 {
				result.append(SessionInfo(id: index, name: "Hint", 
										  value: LocalizedStringKey("ADU(min) = 0 found! Increase the camera offset!").stringValue()!, hint: true))
				index += 1
			}
		}
		
		let aduMax = self.sessionImages.filter({ $0.aduMax != nil }).map({ $0.aduMax! }).distinct().sorted()
		if let highestAduMax = aduMax.last {
			if highestAduMax >= 65535 {
				result.append(SessionInfo(id: index, name: "Hint", 
										  value: LocalizedStringKey("You have saturated pixel (probably stars)! Consider reducing exposure time!").stringValue()!, hint: true))
				index += 1
			}
		}
		
		return result
	}
	
	
	public func coverage() async -> [DataSummary] {
		
		var summary: [DataSummary] = []
		
		let images = self.sessionImages.filter({ $0.startExposureUTC != nil && $0.exposure != nil && $0.startExposure != nil })
		
		for image in images {
			
			let exp = Double(image.exposure!)
			let jd1 = JulianDate.fromDate(date: image.startExposureUTC!)
			let jd2 = JulianDate.fromDate(date: image.startExposureUTC!.addingTimeInterval(exp))
						
			let ninaLogAvailable = self.ninaLogs.hasLogData(startingAt: image.startExposure!.addingTimeInterval(-exp),
															 duration: 2.0 * exp)
			
			let guideLogAvailable = self.guiding.hasFrames(between: jd1, and: jd2)
			
			summary.append(DataSummary(image: image.FileName, guideLog: guideLogAvailable, ninaLog: ninaLogAvailable,
									   metaData: image.metaData, date: image.startExposure!,
									   exposure: image.Exposure, filter: image.filter,
									   bad: image.bad))
		}
		for image in self.missingDataImages {
			summary.append(DataSummary(image: image.FileName, guideLog: false, ninaLog: false,
									   metaData: image.metaData, date: image.startExposure,
									   exposure: image.Exposure, filter: image.filter,
									   bad: image.bad))
		}
		
		return summary
	}
	
	private func malus(for image: SessionImage) -> Float {
		if image.bad {
			return Float.greatestFiniteMagnitude
		}
		
		if let hfr = image.hfr, let mean = image.mean, let numStars = image.numStars {
			return hfr * mean / Float(numStars)
		}
		
		return Float.greatestFiniteMagnitude
	}
	
	public func bestImage(for target: String, count: Int = 1) -> [SessionImage] {
		let images = self.sessionImages.filter({ $0.target == target })
		if images.isEmpty {
			return []
		}
		
		return Array(images.sorted(by: { malus(for: $0) < malus(for: $1) }).prefix(count))
	}
	
	
	public func bestImages(count: Int = 1) async -> [SessionImage] {
		guard !self.sessionImages.isEmpty else {
			return []
		}
	
		let targets = self.sessionImages.map( { $0.target } ).distinct()
		guard targets.count > 0 else {
			return []
		}
		
		var best: [SessionImage] = []
		
		for target in targets {
			let images = bestImage(for: target, count: 10)
			best.append(contentsOf: images)
		}
		
		return best
	}
	
}
