//
//  HintsData.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 01.05.24.
//

import Foundation
import SwiftUI

enum HintsType {
	case info
	case warn
	case critical
}

enum HintsGroup {
	case general
	case nina
	case imaging
	case rotator
	case guiding
}


struct HintsData: Identifiable {
	let id: String
	let type: HintsType
	let group: HintsGroup
	let text: LocalizedStringKey
	let filter: String
	let exposure: String
	let images: [SessionImage]?
	let logs: [NINALogLine]?
	
	
	init(id: String, type: HintsType, group: HintsGroup, filter: String, exposure: String, text: LocalizedStringKey, images: [SessionImage]? = nil) {
		self.id = id 
		self.type = type
		self.group = group
		self.filter = filter
		self.exposure = exposure
		self.text = text
		self.images = images
		self.logs = nil
	}
	
	init(id: String, type: HintsType, group: HintsGroup,
		 logs: [NINALogLine],
		 text: LocalizedStringKey) {
		self.id = id
		self.type = type
		self.group = group
		self.filter = ""
		self.exposure = ""
		self.text = text
		self.logs = logs
		self.images = nil
	}
	
}

extension HintsData {
	var icon: some View {
		Image(systemName: getIconName())
			.font(.system(size: 30))
			.foregroundStyle(getIconColor())
	}
	
	private func getIconName() -> String {
		if self.group == .nina {
			return "exclamationmark.warninglight"
		}
		
		switch self.type {
			case .info: return "checkmark"
			case .warn: return "questionmark"
			case .critical: return "light.beacon.max"
		}
	}
	
	private func getIconColor() -> Color {
		if self.group == .nina {
			return Color.red
		}
		
		switch self.type {
			case .info: return Color.green
			case .warn: return Color.yellow
			case .critical: return Color.red
		}
	}
	
	
}
