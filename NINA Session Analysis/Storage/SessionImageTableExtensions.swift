//
//  SessionImageTableExtensions.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 05.04.24.
//

import Foundation
import SwiftUI

enum TableImageColumnId: Int, CaseIterable {
	case index = 10
	case fileName = 12
	case filter = 13
	case marking = 15
	case exposureStart = 30
	case exposureEnd = 40
	case exposure = 50
	case focuserPosition = 55
	case altitude = 60
	case azimuth = 70
	case pierSide = 80
	case airmass = 90
	case hfr = 100
	case hfrStdDev = 110
	case fwhm = 120
	case eccentricity = 130
	case numStars = 140
	case mean = 150
	case median = 160
	case aduMin = 170
	case aduMax = 180
	case moonAlt = 190
	case moonAz = 191
	case moonAngle = 192
	case sunAlt = 200
	case sunAz = 201
	case sunAngle = 202
	case rms = 210
	case rmsRA = 220
	case rmsDEC = 230
	case relRMSRADEC = 235
	case peakToPeakRA = 240
	case peakToPeakDEC = 250
	case avgGuideRA = 260
	case avgGuideDEC = 270
	case eastPulseCount = 281
	case westPulseCount = 282
	case northPulseCount = 283
	case southPulseCount = 284
	

	case drift = 340
	case cloudCoverags = 350
	case airPressure = 360
	case humidity = 370
	case dewPoint = 375
	case windSpeed = 380
	case windDir = 385
	case windGust = 390
	case ambientTemperature = 400
	case skyTemperature = 410
	case skyBrightness = 420
	case skyQuality = 430
	case deltaTemperature = 440
	
	var element: KeyPath<SessionImage, String> {
		switch self {
			case .index: return \.Index
			case .exposureStart: return \.ExposureBeginTime
			case .exposureEnd: return \.ExposureEndTime
			case .exposure: return \.Exposure
			case .altitude: return \.Altitude
			case .azimuth: return \.Azimuth
			case .pierSide: return \.pierSide
			case .airmass: return \.Airmass
			case .focuserPosition: return \.FocuserPosition
			case .hfr: return \.HFR
			case .hfrStdDev: return \.HFRStdDev
			case .fwhm: return \.FWHM
			case .eccentricity: return \.Eccentricity
			case .numStars: return \.NumStars
			case .mean: return \.Mean
			case .median: return \.Median
			case .aduMin: return \.ADUMin
			case .aduMax: return \.ADUMax
			case .moonAlt: return \.MoonAltitude
			case .moonAz: return \.MoonAzimuth
			case .moonAngle: return \.MoonAngle
			case .sunAlt: return \.SunAltitude
			case .sunAz: return \.SunAzimuth
			case .sunAngle: return \.SunAngle
			case .rms: return \.RMS
			case .rmsRA: return \.RMSRA
			case .rmsDEC: return \.RMSDEC
			case .relRMSRADEC: return \.RelationRMSRADEC
			case .peakToPeakRA: return \.GuidingRAPeakToPeak
			case .peakToPeakDEC: return \.GuidingDECPeakToPeak
			case .avgGuideRA: return \.AvgGuidingRA
			case .avgGuideDEC: return \.AvgGuidingDEC
			case .marking: return \.BAD
			case .drift: return \.DriftPixel
			case .cloudCoverags: return \.CloudCover
			case .airPressure: return \.Pressure
			case .humidity: return \.Humidity
			case .windSpeed: return \.WindSpeed
			case .fileName: return \.FileName
			case .filter: return \.filter
			case .dewPoint: return \.DewPoint
			case .ambientTemperature: return \.AmbientTemperature
				//default: return ""
				
				
			case .eastPulseCount: return \.EastPulseCount
			case .westPulseCount: return \.WestPulseCount
			case .northPulseCount: return \.NorthPulseCount
			case .southPulseCount: return \.SouthPulseCount
			case .windDir: return \.WindDirection
			case .windGust: return \.WindGust
			case .skyTemperature: return \.SkyTemperature
			case .skyBrightness: return \.SkyBrightness
			case .skyQuality: return \.SkyQuality
			case .deltaTemperature: return \.DeltaTemperature
		}
	}
	
	var name: String {
		switch self {
			case .index : return "Index"
			case .fileName: return "Filename"
			case .marking: return "Bad"
			case .exposureStart: return "Exposure (Start)"
			case .exposureEnd: return "Exposure (End)"
			case .exposure: return "Exposure"
			case .altitude: return "Altitude"
			case .azimuth: return "Azimuth"
			case .pierSide: return "PierSide"
			case .airmass: return "Airmass"
			case .focuserPosition: return "Focuser"
			case .hfr: return "HFR"
			case .hfrStdDev: return "HFR(StdDev)"
			case .fwhm: return "FWHM"
			case .eccentricity: return "Eccentricity"
			case .numStars: return "# Stars"
			case .mean: return "Mean"
			case .median: return "Median"
			case .aduMin: return "ADU (min)"
			case .aduMax: return "ADU (max)"
			case .moonAlt: return "Moon Altitude"
			case .moonAz: return "Moon Azimuth"
			case .moonAngle: return "Moon Angle"
			case .sunAlt: return "Sun Altitude"
			case .sunAz: return "Sun Azimuth"
			case .sunAngle: return "Sun Angle"
			case .rms: return "RMS"
			case .rmsRA: return "RMS (α)"
			case .rmsDEC: return "RMS (δ)"
			case .relRMSRADEC: return "RMS Excentricity"
			case .drift: return "Drift (px)"
			case .cloudCoverags: return "Cloud Coverage"
			case .airPressure: return "Air Pressure"
			case .humidity: return "Humidity"
			case .dewPoint: return "DewPoint"
			case .windSpeed: return "Wind Speed"
			case .ambientTemperature: return "Ambient Temperature"
			case .skyTemperature: return "Sky Temperature"
			case .filter: return "Filter"
			case .peakToPeakRA: return "Peaks (α)"
			case .peakToPeakDEC: return "Peaks (δ)"
			case .avgGuideRA: return "Peaks avg (α)"
			case .avgGuideDEC: return "Peaks avg (δ)"
			case .eastPulseCount: return "#Pulses (E)"
			case .westPulseCount: return "#Pulses (W)"
			case .northPulseCount: return "#Pulses (N)"
			case .southPulseCount: return "#Pulses (S)"
			case .skyQuality: return "SkyQuality"
			case .skyBrightness: return "Sky Brightness"
			
			case .windDir: return "Wind Direction"
			case .windGust: return "Wind Gust"
			case .deltaTemperature: return "Delta Temperature"
		}
	}
	
	var columnTitle: LocalizedStringKey {
		switch self {
			
			case .index: return LocalizedStringKey("Index")
			case .fileName: return LocalizedStringKey("Filename")
			case .filter: return LocalizedStringKey("Filter")
			case .marking: return LocalizedStringKey("Bad")
			case .exposureStart: return LocalizedStringKey("Exposure (Start)")
			case .exposureEnd: return LocalizedStringKey("Exposure (End)")
			case .exposure: return LocalizedStringKey("Exposure")
			case .altitude: return LocalizedStringKey("Altitude")
			case .azimuth: return LocalizedStringKey("Azimuth")
			case .pierSide: return LocalizedStringKey("PierSide")
			case .airmass: return LocalizedStringKey("Airmass")
			case .focuserPosition: return LocalizedStringKey("Focuser")
			case .hfr: return LocalizedStringKey("HFR")
			case .hfrStdDev: return LocalizedStringKey("HFR(StdDev)")
			case .fwhm: return LocalizedStringKey("FWHM")
			case .eccentricity: return LocalizedStringKey("Eccentricity")
			case .numStars: return LocalizedStringKey("#Stars")
			case .mean: return LocalizedStringKey("Mean")
			case .median: return LocalizedStringKey("Median")
			case .aduMin: return LocalizedStringKey("ADU (min)")
			case .aduMax: return LocalizedStringKey("ADU (max)")
			case .moonAlt: return LocalizedStringKey("🌒 Altitude")
			case .moonAz: return LocalizedStringKey("🌒 Azimuth")
			case .moonAngle: return LocalizedStringKey("Δ 🌒")
			case .sunAlt: return LocalizedStringKey("🌞 Altitude")
			case .sunAz: return LocalizedStringKey("🌞 Azimuth")
			case .sunAngle: return LocalizedStringKey("Δ 🌞")
				
			case .rms: return LocalizedStringKey("RMS")
			case .rmsRA: return LocalizedStringKey("RMS(α)")
			case .rmsDEC: return LocalizedStringKey("RMS(δ)")
			case .relRMSRADEC: return LocalizedStringKey("e RMS")
			case .peakToPeakRA: return LocalizedStringKey("Peak (α)")
			case .peakToPeakDEC: return LocalizedStringKey("Peaks (δ)")
			case .avgGuideRA: return LocalizedStringKey("Peaks avg (α)")
			case .avgGuideDEC: return LocalizedStringKey("Peaks avg (δ)")
			case .eastPulseCount: return LocalizedStringKey("#Pulses (E)")
			case .westPulseCount: return LocalizedStringKey("#Pulses (W)")
			case .northPulseCount: return LocalizedStringKey("#Pulses (N)")
			case .southPulseCount: return LocalizedStringKey("#Pulses (S)")
			case .drift: return LocalizedStringKey("Drift (px)")
			case .cloudCoverags: return LocalizedStringKey("Cloud Coverage")
			case .airPressure: return LocalizedStringKey("Air Pressure")
			case .humidity: return LocalizedStringKey("Humidity")
			case .dewPoint: return LocalizedStringKey("DewPoint")
			case .windSpeed: return LocalizedStringKey("Wind Speed")
			case .windDir: return LocalizedStringKey("Wind Direction")
			case .windGust: return LocalizedStringKey("Wind Gust")
			case .ambientTemperature: return LocalizedStringKey("Ambient Temperature")
			case .skyTemperature: return LocalizedStringKey("Sky Temperature")
			case .skyBrightness: return LocalizedStringKey("Sky Brightness")
			case .skyQuality: return LocalizedStringKey("Sky Quality")
			case .deltaTemperature: return LocalizedStringKey("Δ Temperature")
		}
	}
	
	var helpText: LocalizedStringKey {
		switch self {
			
			case .index: return LocalizedStringKey("Index of the image")
			case .fileName: return LocalizedStringKey("Image Filename")
			case .filter: return LocalizedStringKey("Filter used to capture this image")
			case .marking: return LocalizedStringKey("Checked, if marked as bad image")
			case .exposureStart: return LocalizedStringKey("Begin time of exposure")
			case .exposureEnd: return LocalizedStringKey("End time of exposure")
			case .exposure: return LocalizedStringKey("Exposure duration in seconds")
			case .altitude: return LocalizedStringKey("Altitude of the target during exposure")
			case .azimuth: return LocalizedStringKey("Azimuth of the target during exposure")
			case .pierSide: return LocalizedStringKey("PierSide of the mount during exposure")
			case .airmass: return LocalizedStringKey("Airmass during exposure")
			case .focuserPosition: return LocalizedStringKey("Focuser position during exposure")
			case .hfr: return LocalizedStringKey("Average star HFR value")
			case .hfrStdDev: return LocalizedStringKey("Standard deviation of HFR")
			case .fwhm: return LocalizedStringKey("Average star FWHM value")
			case .eccentricity: return LocalizedStringKey("Average star eccentricity")
			case .numStars: return LocalizedStringKey("Number of detected stars")
			case .mean: return LocalizedStringKey("Mean ADU value (more or less the sky background)")
			case .median: return LocalizedStringKey("Median ADU value (more or less the sky background)")
			case .aduMin: return LocalizedStringKey("Minimum ADU value of the darkest pixel")
			case .aduMax: return LocalizedStringKey("Maxium ADU value of the brighest pixel")
			case .moonAlt: return LocalizedStringKey("Moon Altitude during exposure")
			case .moonAz: return LocalizedStringKey("Moon Azimuth during exposure")
			case .moonAngle: return LocalizedStringKey("Angular separation between target and moon.")
			case .sunAlt: return LocalizedStringKey("Sun Altitude during exposure")
			case .sunAz: return LocalizedStringKey("Sun Azimuth during exposure")
			case .sunAngle: return LocalizedStringKey("Angular separation between target and sun.")
				
			case .rms: return LocalizedStringKey("Guiding RMS (root mean square) value.")
			case .rmsRA: return LocalizedStringKey("Guidng RMS (root mean square) value for right ascension.")
			case .rmsDEC: return LocalizedStringKey("Guidng RMS (root mean square) value for Declination.")
			case .relRMSRADEC: return LocalizedStringKey("Guiding RMS eccentricity defined by the relation between guding in right ascension and declination.")
			case .peakToPeakRA: return LocalizedStringKey("Right ascension max peaks distance.")
			case .peakToPeakDEC: return LocalizedStringKey("Declination max peaks distance.")
			case .avgGuideRA: return LocalizedStringKey("Average peaks in right ascension.")
			case .avgGuideDEC: return LocalizedStringKey("Average peaks in declination.")
			case .eastPulseCount: return LocalizedStringKey("Number of guiding pulses in direction east")
			case .westPulseCount: return LocalizedStringKey("Number of guiding pulses in direction west")
			case .northPulseCount: return LocalizedStringKey("Number of guiding pulses in direction north")
			case .southPulseCount: return LocalizedStringKey("Number of guiding pulses in direction south")
			case .drift: return LocalizedStringKey("Drift of the image center to the centered target in pixel.")
			case .cloudCoverags: return LocalizedStringKey("Cloud coverage during exposure")
			case .airPressure: return LocalizedStringKey("Air Pressure during exposure")
			case .humidity: return LocalizedStringKey("Humidity during exposure")
			case .dewPoint: return LocalizedStringKey("Dew point temperature during exposure.")
			case .windSpeed: return LocalizedStringKey("Wind speed in km/h during exposure.")
			case .windDir: return LocalizedStringKey("Wind Direction during exposure")
			case .windGust: return LocalizedStringKey("Wind gust speeds in km/h during exposure.")
			case .ambientTemperature: return LocalizedStringKey("Ambient Temperature during exposure.")
			case .skyTemperature: return LocalizedStringKey("Sky Temperature during exposure.")
			case .skyBrightness: return LocalizedStringKey("Sky Brightness during exposure.")
			case .skyQuality: return LocalizedStringKey("Sky Quality during exposure.")
			case .deltaTemperature: return LocalizedStringKey("Temperature difference Sky Temperature minus Ambient Temperature normally used to detect clouds.")
		}
	}
	
	
}



struct TableImageColumn: Identifiable {
	var id: TableImageColumnId
	var element: KeyPath<SessionImage, String>
	
	var enabled: Bool {
		didSet {
			self.store()
		}
	}
	
	var name: LocalizedStringKey {
		self.id.columnTitle
	}
	
	var helpText: LocalizedStringKey {
		self.id.helpText
	}
	
	init(item: TableImageColumnId, element: KeyPath<SessionImage, String>, enabled: Bool) {
		self.id = item
		self.enabled = enabled
		self.element = element
			
		let defaults = UserDefaults.standard
		let key = "TableImageColumn_" + self.id.name
		self.enabled = defaults.bool(forKey: key)
	}
		
	func store() {
		let defaults = UserDefaults.standard
		defaults.set(self.enabled, forKey: "TableImageColumn_" + self.id.name)
	}
}



extension SessionData {
	
	var tableDataColumns: [TableImageColumn] {
		
		var tableData = [TableImageColumn]()
		
		for column in self.tableColumns.filter({ $0.enabled })
			.sorted(by: { c1, c2 in c1.id.rawValue < c2.id.rawValue }) {
			tableData.append(column)
		}
		 
		return tableData
	}
	
}

extension SessionImage {
	func columnText(for columnId: TableImageColumnId) -> String {
		switch columnId {
			case .index: return self.Index
			
			case .exposureStart: return self.ExposureBeginTime
			case .exposureEnd: return self.ExposureEndTime
			case .exposure: return self.Exposure
			case .altitude: return self.Altitude
			case .azimuth: return self.Azimuth
			case .pierSide: return self.pierSide
			case .airmass: return self.Airmass
			case .focuserPosition: return self.FocuserPosition
			case .hfr: return self.HFR
			case .hfrStdDev: return self.HFRStdDev
			case .fwhm: return self.FWHM
			case .eccentricity: return self.Eccentricity
			case .numStars: return self.NumStars
			case .mean: return self.Mean
			case .median: return self.Median
			case .aduMin: return self.ADUMin
			case .aduMax: return self.ADUMax
			case .moonAlt: return self.MoonAltitude
			case .moonAz: return self.MoonAzimuth
			case .moonAngle: return self.MoonAngle
			case .sunAlt: return self.SunAltitude
			case .sunAz: return self.SunAzimuth
			case .sunAngle: return self.SunAngle
			case .rms: return self.RMS
			case .rmsRA: return self.RMSRA
			case .rmsDEC: return self.RMSDEC
			case .relRMSRADEC: return self.RelationRMSRADEC
			case .peakToPeakRA: return self.GuidingRAPeakToPeak
			case .peakToPeakDEC: return self.GuidingDECPeakToPeak
			case .avgGuideRA: return self.AvgGuidingRA
			case .avgGuideDEC: return self.AvgGuidingDEC
			case .marking: return self.bad ? "BAD" : ""
			case .drift: return self.DriftPixel
			case .cloudCoverags: return self.CloudCover
			case .airPressure: return self.Pressure
			case .humidity: return self.Humidity
			case .windSpeed: return self.WindSpeed
			case .fileName: return self.FileName
			case .filter: return self.filter
			case .dewPoint: return self.DewPoint
			case .ambientTemperature: return self.AmbientTemperature
			//default: return ""
			
			
			case .eastPulseCount: return self.EastPulseCount
			case .westPulseCount: return self.WestPulseCount
			case .northPulseCount: return self.NorthPulseCount
			case .southPulseCount: return self.SouthPulseCount
			case .windDir: return self.WindDirection
			case .windGust: return self.WindGust
			case .skyTemperature: return self.SkyTemperature
			case .skyBrightness: return self.SkyBrightness
			case .skyQuality: return self.SkyQuality
			case .deltaTemperature: return self.DeltaTemperature
				
		}
	}
	
	func columnColor(for colummId: TableImageColumnId) -> Color {
		switch colummId {
			case .sunAlt:
				if let sunAlt = self.sunAlt {
					if sunAlt > -12.0 {
						return Color.red
					} else if sunAlt > -18.0 {
						return Color.yellow
					}
					return Color.green
				}
				return Color.gray
			case .moonAlt:
				if let moonAlt = self.moonAlt {
					if moonAlt > 0.0 {
						return Color.yellow
					}
					return Color.green
				}
				return Color.gray
			case .moonAngle:
				if let moonAngle = self.moonAngle {
					if moonAngle < 30.0 {
						return Color.red
					} else if moonAngle < 60.0 {
						return Color.yellow
					}
					return Color.green
				}
				return Color.gray
			case .hfr: return self.issues.contains(.hfrIncreased) ? Color.red : Color.green
			case .fwhm: return self.issues.contains(.hfrIncreased) ? Color.red : Color.green
			case .mean: return self.issues.contains(.highMeanValue) ? Color.red : Color.green
			case .eccentricity: return self.issues.contains(.elongatedStars) ? Color.red : Color.green
			case .humidity: return self.issues.contains(.highHumidity) ? Color.yellow : Color.primary
			case .cloudCoverags: return self.issues.contains(.highCloudCoverage) ? Color.yellow : Color.primary
			case .windSpeed: return self.issues.contains(.highWindSpeeds) ? Color.yellow : Color.primary
			case .rms: return self.issues.contains(.badGuiding) ? Color.red : Color.primary
			case .rmsRA: return self.issues.contains(.badGuiding) ? Color.red : Color.primary
			case .rmsDEC: return self.issues.contains(.badGuiding) ? Color.red : Color.primary
			
			default: return Color.primary
			
		}
	}
	
	/*
	func columnMinWidth(for columnId: TableImageColumnId) -> CGFloat {
		switch columnId {
			case 10: return 50
			case 40: return 40
			case 50: return 50
			case 60: return 50
			case 70: return 40
			case 80: return 40
			case 90: return 40
			case 100: return 40
			case 110: return 40
			case 120: return 50
			case 130: return 40
			case 140: return 40
			case 150: return 40
			default: return 60
		}
	}*/
}
