//
//  FilterData.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 03.05.24.
//

import Foundation
import SwiftUI



class FilterData: Hashable, Identifiable {
	
	let id: UUID = UUID()
	let filterName: String
	
	var color: Color {
		didSet {
			
			if let hex = color.toHex() {
				UserDefaults.standard.setValue(hex, forKey: "FilterColor_\(filterName)")
			}
		}
	}
	
	var overwrite: Bool {
		didSet {
			UserDefaults.standard.setValue(overwrite, forKey: "FilterOverwrite_\(filterName)")
		}
	}
	
	var hfrTolerance: Float {
		didSet {
			if self.overwrite {
				UserDefaults.standard.setValue(hfrTolerance, forKey: "FilterHFR_\(filterName)")
			}
		}
	}
	
	var meanTolerance: Float {
		didSet {
			if self.overwrite {
				UserDefaults.standard.setValue(meanTolerance, forKey: "FilterMEAN_\(filterName)")
			}
		}
	}
	
	var starCountTolerance: Float {
		didSet {
			if self.overwrite {
				UserDefaults.standard.setValue(starCountTolerance, forKey: "FilterStarCount_\(filterName)")
			}
		}
	}
	
	
	
	init(filterName: String, color: Color, overwrite: Bool, hfr: Float, mean: Float, starCount: Float) {
		self.filterName = filterName
		self.color = color
		self.overwrite = overwrite
		self.hfrTolerance = hfr
		self.meanTolerance = mean
		self.starCountTolerance = starCount
	}
		
	func hash(into hasher: inout Hasher) {
		hasher.combine(filterName)
	}
	
	
	static func == (lhs: FilterData, rhs: FilterData) -> Bool {
		lhs.filterName == rhs.filterName
	}
}
