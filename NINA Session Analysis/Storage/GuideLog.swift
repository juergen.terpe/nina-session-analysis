//
//  GuidLog.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 31.03.24.
//

import Foundation

class GuidingFrame: Identifiable {
	let id: UUID = UUID()
	var frame: Int = 0
	var timeOffset: Float = 0.0
	var time: Double = 0.0
	var exposure: Float = 0.0
	var dx: Float = 0.0
	var dy: Float = 0.0
	var raRawDistance: Float = 0.0
	var decRawDistance: Float = 0.0
	var raGuideDistance: Float = 0.0
	var decGuideDistance: Float = 0.0
	var raDuration: Float = 0.0
	var raDirection: String = ""
	var decDuration: Float = 0.0
	var decDirection: String = ""
	var xStep: Float = 0.0
	var yStep: Float = 0.0
	var starMass: Float = 0.0
	var snr: Float = 0.0
	var settlingAfterDither: Bool = false
	var pxScale: Float?
	var binning: Int?
	var focalLength: Float?
	var errorCode: Int = 0
	var drop: Bool = false
	var comment: String = ""
}

class DitherFrame: Identifiable {
	let id: UUID = UUID()
	var begin: Double
	var end: Double
	var lockPosition: CGPoint
	var dx: Float?
	var dy: Float?
	
	init(begin: Double, end: Double, lockPosition: CGPoint, dx: Float?, dy: Float?) {
		self.begin = begin
		self.end = end
		self.lockPosition = lockPosition
		self.dx = dx
		self.dy = dy
	}
}



class GuidingData: Identifiable {
	let id : UUID
	var guidingStart: JulianDate
	var guidingEnd: JulianDate
	var maxSNR: Float = 0.0
	var maxStarMass: Float = 0.0
	var raRate: Float = 13.5
	var decRate: Float = 13.5
	var focalLength: Float?
	var pixelScale: Float?
	var camera: String = ""
	var declination: Float = 0.0
	var binning: Int?
	var driftAnalysis: DriftAnalysis?
	
	var frames: [GuidingFrame] = []
	var ditherData: [DitherFrame] = []
	var periodicCurve: PeriodicGuidingCurve?
	
	var profile: String?
	var exposure: Float?
	var ditherAxes: String?
	var ditherScale: Float?
	var noiseReduction: String?
	var searchRegionSize: Float?
	var starMassTolerance: String?
	var multiStarMode: String?
	var normRateRA: Float?
	var normRateDEC: Float?
	var raGuideAlgorithm: String?
	var raGuideControlGain: Float?
	var raGuidingHysteresis: Float?
	var raGuidingAggressiveness: Float?
	var raPredictionGain: Float?
	var raMinMove: Float?
	var decGuideAlgorithm: String?
	var decAggressiveness: Float?
	var decMinMove: Float?
	var backlashCompensation: Bool?
	var backlashPulse: Float?
	var raMaxPulseDuration: Float?
	var decMaxPulseDuration: Float?
	var decGuideMode: String?
	var raStart: Float?
	var decStart: Float?
	var hourAngleStart: Float?
	var altStart: Float?
	var azStart: Float?
	var pierSide: String?
	var rotatorAngle: String?
	
	var images: [UUID] = []
	
	init(start: JulianDate, end: JulianDate? = nil) {
		self.guidingStart = start
		self.guidingEnd = end ?? start
		self.id = UUID()
	}
	
	var duration: Double {
		return (self.guidingEnd.jd - self.guidingStart.jd) * 86400.0
	}
	
	func getSessionTime(timeZone: TimeZone)-> String {
		let d1 = self.guidingStart.toDateTime(timeZone)
		let d2 = self.guidingEnd.toDateTime(timeZone)
		
		if Calendar.current.isDate(d1, inSameDayAs: d2) {
			return "\(d1.formatted(date: .abbreviated, time: .omitted)): \(d1.formatted(date: .omitted, time: .shortened)) - \(d2.formatted(date: .omitted, time: .shortened))"
		}
		
		return "\(d1.formatted(date: .abbreviated, time: .shortened)) - \(d2.formatted(date: .abbreviated, time: .shortened))"
	}
	
	func getSessionInfo() -> [String] {
		var infos = [String]()
		if let raGuideAlgorithm {
			
			if let raGuideControlGain, let raMinMove {
				infos.append("α: \(raGuideAlgorithm): Aggressiveness \(raGuideControlGain), MinMove: \(raMinMove)")
			} else if let raGuidingAggressiveness, let raMinMove, let raGuidingHysteresis {
				infos.append("α: \(raGuideAlgorithm): Aggressiveness \(raGuidingAggressiveness), Hysteresis: \(raGuidingHysteresis), MinMove: \(raMinMove)")
			} else if let raGuidingAggressiveness, let raMinMove {
				infos.append("α: \(raGuideAlgorithm): Aggressiveness \(raGuidingAggressiveness), MinMove: \(raMinMove)")
			}
		}
		
		if let decGuideAlgorithm, let decAggressiveness, let decMinMove {
			infos.append("δ: \(decGuideAlgorithm): Aggressiveness \(decAggressiveness), MinMove: \(decMinMove)")
		}
		
		return infos
	}
	
	private func isDithering(_ data: GuideGraphData, ditherings: [DitherGraphData]) -> Bool {
		if let _ = ditherings.first(where: { $0.time1 <= data.time && $0.time2 >= data.time }) {
			return true
		}
		return false
	}
	
	func analyse() {
		
		if let pxScale = self.pixelScale {
			let dec = self.declination
			
			let analysis = GuidingDriftAnalysis()
			self.driftAnalysis = analysis.calculateStatistics(frames: self.frames,
															  pixelScale: Double(pxScale), dec: Double(dec))
			
			/*
			let curveAnalyzer = PeriodicGuidingAnalyzer()
			self.periodicCurve = curveAnalyzer.analyze(data: self.frames, start: self.guidingStart.jd)
			*/
		}
	}
	
	func getGuideGraphDataWithoutDithering() -> [GuideGraphData] {
		let dithers = getDitherGraphData()
		let guideData = getGuideGraphData()
		return guideData.filter({ !isDithering($0, ditherings: dithers) })
	}
	
	func getGuideGraphData() -> [GuideGraphData] {
		
		var data = [GuideGraphData]()
		
		if frames.count > 0 {
			for frame in frames {
				let t = (frame.time - self.guidingStart.jd) * 86400.0
				
				if frame.drop {
					data.append(GuideGraphData(id: frame.id,
											   time: Float(t), ra: 0, dec: 0, pulseRA: 0, pulseDEC: 0, drop: true,
											   exposure: frame.exposure/1000.0))
				} else {
					let ra = frame.raRawDistance
					let dec = frame.decRawDistance
					
					var raDir: Float = 1.0
					if frame.raDirection == "W" {
						raDir = -1.0
					}
					let pulseRA = frame.raDuration * raDir * raRate/1000.0
					
					var decDir: Float = 1.0
					if frame.decDirection == "S" {
						decDir = -1.0
					}
					let pulseDEC = frame.decDuration * decDir * decRate/1000.0
					
					data.append(GuideGraphData(id: frame.id, time: Float(t), ra: ra, dec: dec, 
											   pulseRA: pulseRA, pulseDEC: pulseDEC, snr: frame.snr,
											   exposure: frame.exposure/1000.0))
				}
			}
		}
		return data
	}
	
	func getDitherGraphData() -> [DitherGraphData] {
		var result = [DitherGraphData]()
		
		for dither in self.ditherData {
			let t1 = (dither.begin - self.guidingStart.jd) * 86400.0
			let t2 = (dither.end - self.guidingStart.jd) * 86400.0
			result.append(DitherGraphData(id: dither.id, time1: Float(t1), time2: Float(t2), jdd: JulianDate(julianDate: dither.begin)))
		}
		return result
	}
	
}

final class CalibrationFrame : Sendable {
	let direction: String
	let step: Int
	let dx: Float
	let dy: Float
	let position: CGPoint
	let dist: Float
	
	init(direction: String, step: Int, dx: Float, dy: Float, position: CGPoint, dist: Float) {
		self.direction = direction
		self.step = step
		self.dx = dx
		self.dy = dy
		self.position = position
		self.dist = dist
	}
}

class CalibrationData: Identifiable {
	let id = UUID()
	var calibrationDate: Date
	var ra: Float?
	var dec: Float?
	var hourAngle: Float?
	var altitude: Float?
	var azimuth: Float?
	var lockPosition: CGPoint = CGPoint.zero
	var starPosition: CGPoint = CGPoint.zero
	var hfd: Float?
	var westAngle: Float?
	var westRate: Float?
	var northAngle: Float?
	var northRate: Float?
	var mount: String = ""
	var stepSize: Int?
	var calibrationDistance: Float?
	var westFrames: [CalibrationFrame] = []
	var northFrames: [CalibrationFrame] = []
	
	var text: String {
		self.calibrationDate.formatted()
	}
	
	init(date: Date) {
		self.calibrationDate = date
	}
	
	func getCalibrationDate(timeZone: TimeZone) -> String {
		self.calibrationDate
			.addingTimeInterval(TimeInterval(timeZone.secondsFromGMT(for: self.calibrationDate)))
			.formatted(date: .abbreviated, time: .shortened)
	}
	
	var meridianDistance: Float? {
		if let ha = self.hourAngle {
			return ha * 15.0
		}
		return nil
	}
}

struct LostStarEvents: Identifiable {
	let id: UUID
	let jd: JulianDate
	let comment: String
	let timeZone: TimeZone
}

struct DitheringEvents: Identifiable {
	var id: UUID
	let jd1: JulianDate
	let jd2: JulianDate
	let dx: Float?
	let dy: Float?
	let timeZone: TimeZone
	
	var text: String {
		return "\(jd1.toDateTime(timeZone).formatted(date: .omitted, time: .standard)) - \(jd2.toDateTime(timeZone).formatted(date: .omitted, time: .standard))"
			 + " dx: \(dx != nil ? String(format: "%.1f", dx!) : "") dy: \(dy != nil ? String(format: "%.1f", dy!) : "")"
			 + " Duration: \(String(format: "%.1f", (jd2.jd - jd1.jd)*86400.0))s "
	}
}


class GuidingSessionData {
	private var guidingData: [GuidingData] = []
	private var calibrationData: [CalibrationData] = []
	
	var count: Int {
		return self.guidingData.count
	}
	
	public var timeZone: TimeZone = TimeZone.current
	
	var guidingSessions: [GuidingData] {
		return self.guidingData
	}
	
	func getGuidingSessions(onlyImages: Bool, minDuration: Int) -> [GuidingData] {
		if onlyImages {
			let filtered = self.guidingData.filter({ $0.images.count > 0  }).sorted(by: { $0.guidingStart < $1.guidingStart })
			
			if minDuration == 0 {
				return filtered
			}
			
			return filtered.filter({ $0.duration >= Double(minDuration * 60)})
		} else {
			if minDuration == 0 {
				return self.guidingData.sorted(by: { $0.guidingStart < $1.guidingStart })
			}
			return self.guidingData.filter({ $0.duration >= Double(minDuration * 60)})
								   .sorted(by: { $0.guidingStart < $1.guidingStart })
		}
	}
	
	
	func clear() {
		self.guidingData.removeAll()
		self.calibrationData.removeAll()
	}
	
	var Calibrations: [CalibrationData] {
		return self.calibrationData
	}
	
	
	func lostStarEvents(_ sessionId: GuidingData.ID) -> [LostStarEvents] {
		var result = [LostStarEvents]()
		
		if let gd = self.guidingData.first(where: { $0.id == sessionId }) {
			for frame in gd.frames.filter({ $0.drop }) {
				result.append(LostStarEvents(id: frame.id, jd: JulianDate(julianDate: frame.time), comment: frame.comment, timeZone: self.timeZone))
			}
		}
		return result
	}
	
	func ditheringEvents(_ sessionId: GuidingData.ID) -> [DitheringEvents] {
		var result = [DitheringEvents]()
		if let gd = self.guidingData.first(where: { $0.id == sessionId })  {
			for frame in gd.ditherData {
				result.append(DitheringEvents(id: frame.id, 
											  jd1: JulianDate(julianDate: frame.begin),
											  jd2: JulianDate(julianDate: frame.end), dx: frame.dx, dy: frame.dy, timeZone: self.timeZone))
			}
		}
		return result 
	}
	
	func getGuidingRates(between jd1: JulianDate, and jd2: JulianDate) -> (ra: Float, dec: Float) {
		for gd in self.guidingData {
			if gd.guidingEnd < jd1 {
				continue
			}
			else if gd.guidingStart > jd2 {
				continue
			}
			
			return (ra: gd.raRate, dec: gd.decRate)
		}
		return (ra: 7.5, dec: 7.5)
	}
	
	func hasFrames(between jd1: JulianDate, and jd2: JulianDate) -> Bool {
		for gd in self.guidingData {
			
			if gd.guidingEnd < jd1 {
				continue
			}
			else if gd.guidingStart > jd2 {
				continue
			}
			
			for frame in gd.frames {
				let expOffset = Double(frame.exposure/1000.0)/86400.0
				
				if ((jd1.jd-expOffset) <= frame.time) && ((jd2.jd + expOffset) >= frame.time) {
					return true
				}
				else if frame.time > jd2.jd {
					break
				}
			}
		}
		
		return false 
	}
	
	func getFrames(between jd1: JulianDate, and jd2: JulianDate) -> [GuidingFrame] {
		var result = [GuidingFrame]()
		
		for gd in self.guidingData {
			
			if gd.guidingEnd < jd1 {
				continue
			}
			else if gd.guidingStart > jd2 {
				continue
			}
			
			for frame in gd.frames {
				let expOffset = Double(frame.exposure/1000.0)/86400.0
				
				if ((jd1.jd-expOffset) <= frame.time) && ((jd2.jd + expOffset) >= frame.time) {
					result.append(frame)
				}
				else if frame.time > jd2.jd {
					break
				}
			}
		}
		
		result.sort { f1, f2 in
			f1.time < f2.time
		}
		return result
	}
	
	
	private func readTime(_ str: String, timeZone: TimeZone) -> JulianDate {
		// # 2023-03-15 20:34:49
		
		let parts = str.components(separatedBy: " ")
		let date = parts[0]
		let time = parts[1]
		let ymd = date.components(separatedBy: "-")
		let hms = time.components(separatedBy: ":")
		let year = Int(ymd[0])!
		let month = Int(ymd[1])!
		let day = Int(ymd[2])!
		let hour = Int(hms[0])!
		let minute = Int(hms[1])!
		let sec = Int(hms[2])!
		
		var dateComponents = DateComponents()
		dateComponents.year = year
		dateComponents.month = month
		dateComponents.day = day
		dateComponents.hour = hour
		dateComponents.minute = minute
		dateComponents.second = sec
		dateComponents.timeZone = timeZone

		var calendar = Calendar(identifier: Calendar.Identifier.gregorian)
		calendar.timeZone = timeZone
		let dateTime = calendar.date(from: dateComponents)!
		let dateTimeUTC = dateTime.addingTimeInterval(-TimeInterval(timeZone.secondsFromGMT(for: dateTime)))
		
		let jd = JulianDate.fromDate(date: dateTimeUTC)
		
		return jd
	}
	
	func read(_ url: URL, timeZone: TimeZone) async {
		
		guard let data = try? Data(contentsOf: url, options: .uncached) else {
			return
		}
		
		if let text = String(data: data, encoding: .utf8) {
			let lines = text.components(separatedBy: "\r\n")
			if lines.count > 0 {
				var raRate = 7.5
				var decRate = 7.5
				var declination: Float = 0.0
				var pxScale: Float?
				var binning: Int?
				var focalLength: Float?
				var camera: String = ""
				//var gain: String = ""
				
				var current: GuidingData?
				var isSettling = false
				var isCalibrating = false
				
				var currentCalibration: CalibrationData?
				var lastFrameTime: Double?
				var ditherFrame: DitherFrame? = nil
				var currentExposureTime: Float = 0.0
				
				for i in 0..<lines.count {
					let line = lines[i]
					
					if line.starts(with: " Camera = ") {
						let regEx = /Camera = (?<camera>.*?),.+/
						if let matches = try? regEx.wholeMatch(in: line) {
							camera = String(matches.camera)
							//gain = String(matches.gain)
						}
					} else if line.starts(with: "Calibration Begins at") {
						let temp = line.replacingOccurrences(of: "Calibration Begins at ", with: "").trimmingCharacters(in: .whitespacesAndNewlines)
						let jd = readTime(temp, timeZone: timeZone)
						currentCalibration = CalibrationData(date: jd.toDateTime())
						isCalibrating = true
						
					}
					else if line.starts(with: "Calibration complete,") {
						isCalibrating = false
						if let calib = currentCalibration {
							self.calibrationData.append(calib)
						}
						currentCalibration = nil
					}
					else if isCalibrating && currentCalibration != nil {
						if line.starts(with: "Lock position =") {
							let regEx = /Lock position = (?<lpx>.+?), (?<lpy>.+?), Star position = (?<spx>.+?), (?<spy>.+?), HFD = (?<hfd>.+?) px/
							
							if let matches = try? regEx.firstMatch(in: line) {
								currentCalibration!.lockPosition = CGPoint(x: Double(matches.lpx)!, y: Double(matches.lpy)!)
								currentCalibration!.starPosition = CGPoint(x: Double(matches.spx)!, y: Double(matches.spy)!)
								currentCalibration!.hfd = Float(matches.hfd)
							}
						}
						else if line.starts(with: "Mount =") {
							let regEx = /Mount = (?<mount>.+?), Calibration Step = (?<stepSize>.+?) ms, Calibration Distance = (?<calDist>.+?) px, Assume orthogonal axes = .+/
							if let matches = try? regEx.firstMatch(in: line) {
								currentCalibration!.mount = String(matches.mount).trimmingCharacters(in: .whitespacesAndNewlines)
								currentCalibration!.stepSize = Int(matches.stepSize)
								currentCalibration!.calibrationDistance = Float(matches.calDist)
							}
						}
						else if line.starts(with: "RA =") {
							let regEx = /RA = (?<ra>.+?) hr, Dec = (?<dec>.+?) deg, Hour angle = (?<hourAngle>.+?) hr, Pier side = (?<pS>.+?), .+, Alt = (?<alt>.+?) deg, Az = (?<az>.+?) deg/
							
							if let matches = try? regEx.firstMatch(in: line) {
								currentCalibration!.azimuth = Float(matches.az)
								currentCalibration!.altitude = Float(matches.alt)
								currentCalibration!.ra = Float(matches.ra)
								currentCalibration!.dec = Float(matches.dec)
								currentCalibration!.hourAngle = Float(matches.hourAngle)
								declination = Float(matches.dec) ?? 0.0
							}
						}
						else if line.starts(with: "West calibration complete.") {
							let regEx = /West calibration complete. Angle = (?<angle>.+?) deg, Rate = (?<rate>.+?) px\/sec, Parity = .+/
							if let matches = try? regEx.firstMatch(in: line) {
								currentCalibration!.westAngle = Float(matches.angle)
								currentCalibration!.westRate = Float(matches.rate)
							}
						}
						else if line.starts(with: "North calibration complete.") {
							let regEx = /North calibration complete. Angle = (?<angle2>.+?) deg, Rate = (?<rate2>.+?) px\/sec, Parity = .+/
							
							if let matches = try? regEx.firstMatch(in: line) {
								currentCalibration!.northAngle = Float(matches.angle2)
								currentCalibration!.northRate = Float(matches.rate2)
							}
						}
						else if line.starts(with: "West,") {
							let regEx = /West,(?<step>.+?),(?<dx>.+?),(?<dy>.+?),(?<px>.+?),(?<py>.+?),(?<dist>.+?)/
							if let matches = try? regEx.firstMatch(in: line) {
								
								let step = Int(matches.step)
								let dx = Float(matches.dx)
								let dy = Float(matches.dy)
								var position: CGPoint?
								if let px = Float(matches.px), let py = Float(matches.py) {
									position = CGPoint(x: Double(px), y: Double(py))
								}
								let dist = Float(matches.dist)
								
								if step != nil && dx != nil && dy != nil && position != nil && dist != nil {
									currentCalibration!.westFrames.append(CalibrationFrame(direction: "West", step: step!, dx: dx!, dy: dy!, position: position!, dist: dist!))
								}
							}
						}
						else if line.starts(with: "North,") {
							let regEx = /North,(?<step>.+?),(?<dx>.+?),(?<dy>.+?),(?<px>.+?),(?<py>.+?),(?<dist>.+?)/
							if let matches = try? regEx.firstMatch(in: line) {
								
								let step = Int(matches.step)
								let dx = Float(matches.dx)
								let dy = Float(matches.dy)
								var position: CGPoint?
								if let px = Float(matches.px), let py = Float(matches.py) {
									position = CGPoint(x: Double(px), y: Double(py))
								}
								let dist = Float(matches.dist)
								
								if step != nil && dx != nil && dy != nil && position != nil && dist != nil {
									currentCalibration!.northFrames.append(CalibrationFrame(direction: "North", step: step!, dx: dx!, dy: dy!, position: position!, dist: dist!))
								}
							}
						}
					}
					else if line.starts(with: "Exposure =") {
						let regEx = /Exposure = (?<exp>.+?) ms/
						if let matches = try? regEx.wholeMatch(in: line) {
							print("Exposure = \(matches.exp) ms")
							currentExposureTime = Float(matches.exp) ?? 1.0
						}
					}
					else if line.starts(with: "RA Guide Speed") {
						let regEx = /RA Guide Speed = (?<ra>.+?) a-s.+, Dec Guide Speed = (?<dec>.+?) a-s.+/
						
						if let matches = try? regEx.wholeMatch(in: line) {
							print("RA Guide Speed = \(matches.ra), DEC Guide Speed = \(matches.dec)")
							raRate = Double(matches.ra) ?? 7.5
							decRate = Double(matches.dec) ?? 7.5
						}
					}
					else if line.starts(with: "Guiding Ends at") {
						if current != nil && current!.frames.count > 0 {
							for frame in current!.frames {
								frame.snr /= current!.maxSNR
								frame.starMass /= current!.maxStarMass
							}
							
							let jd = JulianDate(julianDate: current!.frames.last!.time)
							current!.guidingEnd = jd
							
							current!.analyse()
							self.guidingData.append(current!)
							ditherFrame = nil
							isSettling = false
							isCalibrating = false
						}
						current = nil
					}
					else if line.starts(with: "Guiding Begins at") {
						let temp = line.replacingOccurrences(of: "Guiding Begins at ", with: "").trimmingCharacters(in: .whitespacesAndNewlines)
						let jd = readTime(temp, timeZone: timeZone)
												
						if current != nil {
							current!.guidingEnd = jd
							
							for frame in current!.frames {
								frame.snr /= current!.maxSNR
								frame.starMass /= current!.maxStarMass
							}
							
							current!.analyse()
							self.guidingData.append(current!)
							ditherFrame = nil
							isSettling = false
							isCalibrating = false
						}
						
						current = GuidingData(start: jd)
						current!.camera = camera
						current!.focalLength = focalLength
						current!.binning = binning
						current!.pixelScale = pxScale
						current!.declination = declination
						current!.exposure = currentExposureTime
					}
					else if line.starts(with: "Pixel scale =") {
						let regEx = /Pixel scale = (?<pxScl>.+?) arc-sec\/px, Binning = (?<binn>.+?), Focal length = (?<fl>.+?) mm/
						if let matches = try? regEx.wholeMatch(in: line) {
							pxScale = Float(matches.pxScl)
							binning = Int(matches.binn)
							focalLength = Float(matches.fl)
							
							current?.focalLength = focalLength
							current?.binning = binning
							current?.pixelScale = pxScale
						}
					}
					else if line.starts(with: "Equipment Profile") {
						let regEx = /Equipment Profile = (?<profile>.+?)/
						if let matches = try? regEx.wholeMatch(in: line) {
							current?.profile = String(matches.profile)
						}
					}
					else if line.starts(with: "Dither =") {
						let regExDither = /Dither\s*=\s*(?<axes>[^,]+)/ // Mehr Spielraum für Leerzeichen
						let regExScale = /.+, Dither scale\s*=\s*(?<scale>[\d.]+)/ // Zahl mit Punkt
						let regExNoise = /.+, Image noise reduction\s*=\s*(?<noiseReduction>[^,]+)/ // Alles bis zum Komma

						if let matches = try? regExDither.firstMatch(in: line) {
							current?.ditherAxes = String(matches.axes)
						}
						
						if let matches = try? regExScale.firstMatch(in: line) {
							current?.ditherScale = Float(matches.scale)
						}
						if let matches = try? regExNoise.firstMatch(in: line) {
							current?.noiseReduction = String(matches.noiseReduction)
						}
					}
					else if line.starts(with: "Search region") {
						let regEx = /Search region = (?<searchRegSize>.+?) px, (?<starMassTolerance>.+?), (?<multiStarMode>.+?), list size/
						if let matches = try? regEx.firstMatch(in: line) {
							current?.searchRegionSize = Float(matches.searchRegSize)
							current?.starMassTolerance = String(matches.starMassTolerance)
							current?.multiStarMode = String(matches.multiStarMode)
						}
					}
					else if line.starts(with: "Norm rates RA =") {
						let regEx = /Norm rates RA = (?<ra>.+?).\/s @ dec 0, Dec = (?<dec>.+?).\/s/
						if let matches = try? regEx.wholeMatch(in: line) {
							current?.normRateRA = Float(matches.ra)
							current?.normRateDEC = Float(matches.dec)
						}
					}
					else if line.starts(with: "X guide algorithm =") {
						let regEx = /X guide algorithm = (?<algorithm>.+?), Control gain = (?<gain>.+?)/
						if let matches = try? regEx.firstMatch(in: line) {
							current?.raGuideAlgorithm = String(matches.algorithm)
							current?.raGuideControlGain = Float(matches.gain)
						} else {
							// X guide algorithm = Hysteresis, Hysteresis = 0.250, Aggression = 0.500, Minimum move = 0.080"
							let regEx2 = /X guide algorithm = (?<algorithm>.+?), Hysteresis = (?<hysteresis>.+?), Aggression = (?<aggr>.+?), Minimum move = (?<minMove>.+?)/
							if let matches2 = try? regEx2.firstMatch(in: line) {
								current?.raGuideAlgorithm = String(matches2.algorithm)
								current?.raGuidingHysteresis = Float(matches2.hysteresis)
								current?.raGuidingAggressiveness = Float(matches2.aggr)
								current?.raMinMove = Float(matches2.minMove)
							} else {
								// X guide algorithm = Lowpass2, Aggressiveness = 70.000, Minimum move = 0.100
								let regEx3 = /X guide algorithm = (?<algorithm>.+?), Aggressiveness = (?<aggressiveness>.+?), Minimum move = (?<move>.+?)/
								if let matches3 = try? regEx3.firstMatch(in: line) {
									current?.raGuideAlgorithm = String(matches3.algorithm)
									current?.raGuidingAggressiveness = Float(matches3.aggressiveness)
									current?.raMinMove = Float(matches3.move)
								}
							}
							
							
						}
					}
					else if line.starts(with: "Prediction gain =") {
						let regEx = /Prediction gain = (?<gain>.+?)/
						if let matches = try? regEx.firstMatch(in: line) {
							current?.raPredictionGain = Float(matches.gain)
						}
					}
					else if line.starts(with: "Minimum move =") {
						let regEx = /Minimum move = (?<move>.+?)/
						if let matches = try? regEx.firstMatch(in: line) {
							current?.raMinMove = Float(matches.move)
						}
					}
					else if line.starts(with: "Y guide algorithm ") {
						let regEx = /Y guide algorithm = (?<algorithm>.+?), Aggressiveness = (?<aggressiveness>.+?), Minimum move = (?<move>.+?)/
						if let matches = try? regEx.firstMatch(in: line) {
							current?.decMinMove = Float(matches.move)
							current?.decGuideAlgorithm = String(matches.algorithm)
							current?.decAggressiveness = Float(matches.aggressiveness)
						} else {
							// Y guide algorithm = Resist Switch, Minimum move = 0.180 Aggression = 100% FastSwitch = enabled
							let regEx2 = /Y guide algorithm = (?<algorithm>.+?), Minimum move = (?<move>.+?) Aggression = (?<aggressiveness>.+?)% FastSwitch = (?<fastSwitch>.+?)/
							if let matches2 = try? regEx2.firstMatch(in: line) {
								current?.decMinMove = Float(matches2.move)
								current?.decGuideAlgorithm = String(matches2.algorithm)
								if let aggr = Float(matches2.aggressiveness) {
									current?.decAggressiveness =  aggr / 100.0
								}
							}
						}
							
					}
					else if line.starts(with: "Backlash comp") {
						let regEx = /Backlash comp = (?<comp>.+?), pulse = (?<pulse>.+?) ms/
						if let matches = try? regEx.wholeMatch(in: line) {
							current?.backlashCompensation = String(matches.comp) != "disabled"
							current?.backlashPulse = Float(matches.pulse)
						}
					}
					else if line.starts(with: "Max RA duration") {
						let regEx = /Max RA duration = (?<raDur>.+?), Max DEC duration = (?<decDur>.+?), DEC guide mode = (?<decMode>.+?)/
						if let matches = try? regEx.wholeMatch(in: line) {
							current?.raMaxPulseDuration = Float(matches.raDur)
							current?.decMaxPulseDuration = Float(matches.decDur)
							current?.decGuideMode = String(matches.decMode)
						}
					}
					else if line.starts(with: "RA =") {
						let regEx = /RA = (?<ra>.+?) hr, Dec = (?<dec>.+?) deg, Hour angle = (?<hourAngle>.+?) hr, Pier side = (?<pierSide>.+?), Rotator pos = (?<rotatorPos>.+?), Alt = (?<alt>.+?) deg, Az = (?<az>.+?) deg/
						if let matches = try? regEx.wholeMatch(in: line) {
							current?.raStart = Float(matches.ra)
							current?.decStart = Float(matches.dec)
							current?.hourAngleStart = Float(matches.hourAngle)
							current?.pierSide = String(matches.pierSide)
							current?.rotatorAngle = String(matches.rotatorPos)
							current?.altStart = Float(matches.alt)
							current?.azStart = Float(matches.az)
						}
					}
					else if line.starts(with: "INFO: SETTLING STATE CHANGE, Settling started") {
						isSettling = true
						continue
					}
					else if line.starts(with: "INFO: SETTLING STATE CHANGE, Settling complete") {
						isSettling = false
						continue
					}
					else if line.starts(with: "INFO: DITHER by"), let lastFrame = lastFrameTime {
						
						let regEx = /INFO: DITHER by (?<dx>.+?), (?<dy>.+?), new lock pos = (?<x>.+?), (?<y>.+?)/
						if let matches = try? regEx.wholeMatch(in: line) {
							let x = Double(matches.x)!
							let y = Double(matches.y)!
							let dx = Float(matches.dx)
							let dy = Float(matches.dy)
							
							ditherFrame = DitherFrame(begin: lastFrame, end: lastFrame, lockPosition: CGPoint(x: x, y: y), dx: dx, dy: dy)
						}
					}
					else if line.starts(with: "INFO: Guiding parameter change") {
						let regEx = /INFO: Guiding parameter change, (?<key>.+?) = .+/
						if let matchesKey = try? regEx.wholeMatch(in: line) {
							if matchesKey.key == "Exposure" {
								let regEx2 = /INFO: Guiding parameter change, Exposure = (?<val>.+?) ms/
								if let matches = try? regEx2.wholeMatch(in: line), let value = Float(matches.val) {
									currentExposureTime = value
								}
							}
							
						}
					}
					else if line.starts(with: "Frame,") {
						continue
					}
					else if current != nil {
						let parts = line.components(separatedBy: ",")
						if parts.count < 18 {
							continue
						}
						
						let frame = GuidingFrame()
						let frameNo = Int(parts[0])!
						let timeoffset = Float(parts[1])!
						if parts[2].starts(with: "\"DROP\"") {
							frame.frame = frameNo
							frame.timeOffset = timeoffset
							frame.time = current!.guidingEnd.jd + Double(timeoffset)/86400.0
							lastFrameTime = frame.time
							frame.comment = parts.last ?? ""
							frame.drop = true 
							current!.frames.append(frame)
						
							continue
						}
						
						let dx = Float(parts[3])
						let dy = Float(parts[4])
						let raRaw = Float(parts[5])
						let decRaw = Float(parts[6])
						let raGuide = Float(parts[7])
						let decGuide = Float(parts[8])
						
						if raGuide == nil || decGuide == nil {
							continue
						}
						
						let raDuration = Float(parts[9])
						let raDirection = parts[10]
						let decDuration = Float(parts[11])
						let decDirection = parts[12]
						let xStep = Float(parts[13])
						let yStep = Float(parts[14])
						let starMass = Float(parts[15])
						let snr = Float(parts[16])
						let errorCode = Int(parts[17])
						
						frame.frame = frameNo
						frame.timeOffset = timeoffset
						frame.time = current!.guidingEnd.jd + Double(timeoffset)/86400.0
						
						if !isSettling {
							
							if ditherFrame != nil && current != nil {
								ditherFrame!.end = frame.time
								current!.ditherData.append(ditherFrame!)
							}
							ditherFrame = nil
						}
												
						lastFrameTime = frame.time
						frame.dx = dx ?? 0.0
						frame.dy = dy ?? 0.0
						frame.raRawDistance = raRaw ?? 0.0
						frame.decRawDistance = decRaw ?? 0.0
						frame.raGuideDistance = raGuide ?? 0.0
						frame.decGuideDistance = decGuide ?? 0.0
						frame.raDuration = raDuration ?? 0.0
						frame.decDuration = decDuration ?? 0.0
						frame.raDirection = raDirection
						frame.decDirection = decDirection
						frame.xStep = xStep ?? 0.0
						frame.yStep = yStep ?? 0.0
						frame.starMass = starMass ?? 0.0
						frame.snr = snr ?? 0.0
						frame.errorCode = errorCode ?? 0
						frame.settlingAfterDither = isSettling
						frame.pxScale = pxScale
						frame.binning = binning
						frame.focalLength = focalLength
						frame.exposure = currentExposureTime
						
						current!.maxSNR = max(snr ?? 0.0, current!.maxSNR)
						current!.maxStarMass = max(starMass ?? 0.0, current!.maxStarMass)
						current!.raRate = Float(raRate)
						current!.decRate = Float(decRate)
						current!.declination = declination
						current!.frames.append(frame)
						
						isCalibrating = false
					}
					
				}
				
				if current != nil && current!.frames.count > 0 {
					for frame in current!.frames {
						frame.snr /= current!.maxSNR
						frame.starMass /= current!.maxStarMass
					}
					
					let jd = JulianDate(julianDate: current!.frames.last!.time).addSeconds(5.0)
					current!.guidingEnd = jd
					current!.focalLength = focalLength
					current!.binning = binning
					current!.pixelScale = pxScale
					current!.declination = declination
					current!.analyse()
										
					self.guidingData.append(current!)
				}
			}
			
		}
		
		
		self.calibrationData.sort(by: { $0.calibrationDate < $1.calibrationDate } )
	}
}

extension GuidingFrame {
	
	static func extract(jd: JulianDate, data: [[GuidingFrame]], type: GuidingIssueType) -> [GuidingIssueGraphData] {
		var results = [GuidingIssueGraphData]()
		for mvm in data {
			if mvm.count > 1 {
				let frame1 = mvm.first!
				let frame2 = mvm.last!
				let t1 = Float((frame1.time - jd.jd) * 86400.0)
				let t2 = Float((frame2.time - jd.jd) * 86400.0)
				
				let amounts = mvm.map { frame in
					let t = Float((frame.time - jd.jd) * 86400.0)
					let value = frame.raRawDistance
					
					return (t, value)
				}
				
				results.append(GuidingIssueGraphData(type: type, time1: t1, time2: t2, amounts: amounts))
			}
		}
		
		return results
	}
}
