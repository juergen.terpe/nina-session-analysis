//
//  SessionHints.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 01.05.24.
//

import Foundation

extension SessionData {
	
	func createHints(_ date: DateOfSession) async -> [HintsData] {
		
		var hints = [HintsData]()
		hints.append(contentsOf: createImagingHints(date))
		hints.append(contentsOf: createRotatorHints(date))
		hints.append(contentsOf: createGuidingHints(date))
		hints.append(contentsOf: createErrorHints(date))
		
		return hints
	}
	
	func createImagingHints(_ date: DateOfSession) -> [HintsData] {
		var hints = [HintsData]()
		
		let allImages = self.sessionImages.filter({ $0.SessionDate != nil && $0.SessionDate!.text == date.text || date.text == "Any" })
		let settings = self.analyzationSettings
		
		let colorCamera = isColorCamera()
		let factor: Float = colorCamera ? 1.41 : 1.0
		let pxScale = getPixelScale()
		let rmsMaxValue = pxScale != nil ? pxScale! * settings.getRMSTolerance() + pxScale!/2.0 * factor : nil
		
		let filters = allImages.map { $0.filter }.distinct()
		
		for filter in filters {
			let filterImages = allImages.filter({ $0.filter == filter })
			let exposures = filterImages.map { $0.Exposure }.distinct()
		
			for exposure in exposures {
				
				let images = filterImages.filter({ $0.Exposure == exposure})
				if images.isEmpty {
					continue
				}
				
				if images.allSatisfy({ $0.isFits }) {
					
					let zeroOffset = images.filter({ $0.offset != nil && $0.offset! == 0 })
					if zeroOffset.count > 0 {
						hints.append(HintsData(id: "IMG_ZERO_OFFSET", type: .warn, group: .imaging,
											   filter: filter, exposure: exposure,
											   text: "\(zeroOffset.count) images are captured using an offset of zero! Set an appropriate offset for your camera in N.I.N.A.!",
											   images: zeroOffset))
						
					} else if let minOffset = images.filter({ $0.offset != nil && $0.offset! > 0 }).map({ $0.offset! }).min() {
						hints.append(HintsData(id: "IMG_ZERO_OFFSET", type: .info, group: .imaging,
											   filter: filter, exposure: exposure,
											   text: "Camera offset was set to \(minOffset)"))
					}
					
					
					let lowADU = images.filter({ $0.aduMin != nil && $0.aduMin! < 20 })
					if lowADU.count > 0 {
						hints.append(HintsData(id: "IMG_LOW_ADU", type: .warn, group: .imaging,
											   filter: filter, exposure: exposure,
											   text: "Lights are not background limited! Increase exposure time and/or gain!",
											   images: lowADU))
					} else if let _ = images.first(where: { $0.offset != nil && $0.aduMin != nil }) {
						hints.append(HintsData(id: "IMG_NO_LOW_ADU", type: .info, group: .imaging,
											   filter: filter, exposure: exposure,
											   text: "Lights are background limited! Gain and exposure time are okay!"))
					}
				}
				
				let highADU = images.filter({ $0.aduMax != nil && $0.aduMax! >= 65534 })
				if highADU.count > 0 {
					hints.append(HintsData(id: "IMG_HIGH_ADU", type: .warn, group: .imaging,
										   filter: filter, exposure: exposure,
										   text: "Lights have saturated pixels, but this can be normal for brighter stars. Please check gain and exposure time.",
										   images: highADU))
				} else {
					hints.append(HintsData(id: "IMG_NO_HIGH_ADU", type: .info, group: .imaging,
										   filter: filter, exposure: exposure,
										   text: "Lights have not any saturated pixels, well done!"))
				}
				
				let (minMean, maxMean) = minMaxMean(images: images)
				if minMean != nil && maxMean != nil {
					if maxMean! >= minMean! + minMean! * settings.getHFRTolerance(filter) {
						let images = images.filter({ $0.mean != nil && $0.mean! > minMean! + minMean! * settings.getHFRTolerance(filter)})
						if images.count > 0 {
							hints.append(HintsData(id: "IMG_HIGH_MEAN", type: .warn, group: .imaging,
												   filter: filter, exposure: exposure,
												   text: "\(images.count) images have a higher mean than the defined mean tolerance!",
												   images: images))
						}
					} else {
						hints.append(HintsData(id: "IMG_NO_HIGH_MEAN",type: .info, group: .imaging,
											   filter: filter, exposure: exposure,
											   text: "All lights have an acceptable mean!"))
					}
					
					if minMean! > 8192 {
						hints.append(HintsData(id: "IMG_VERY_HIGH_MEAN", type: .warn, group: .imaging,
											   filter: filter, exposure: exposure,
											   text: "Lights have a very high mean value! Consider reducing exposure time and/or gain or use appropriate filter to reduce light pollution!"))
					}
				}
				
				if let covAltMean = self.correlationAltitudeMean(images: images), abs(covAltMean) > 0.66 {
					hints.append(HintsData(id: "IMG_MEAN_TARGETALT", type: .warn, group: .imaging,
										   filter: filter, exposure: exposure,
										   text: "Target altitude during exposure did affect the mean value! Correlation: \(covAltMean, specifier: "%.2f")"))
				}
				
				let highSunAltitude = images.filter({ $0.sunAlt != nil && $0.sunAlt! > -18.0 })
				if highSunAltitude.count > 0 {
					let higherSunAltitude = highSunAltitude.filter({ $0.sunAlt != nil && $0.sunAlt! > -12.0})
					if higherSunAltitude.count > 0 {
						hints.append(HintsData(id: "IMG_NAUTICAL_TWI", type: .critical, group: .imaging,
											   filter: filter, exposure: exposure,
											   text: "\(higherSunAltitude.count) lights are captured during nautical twilight! Don't do that!",
											   images: higherSunAltitude))
					} else {
						hints.append(HintsData(id: "IMG_ASTRO_TWI", type: .warn, group: .imaging,
											   filter: filter, exposure: exposure,
											   text: "\(highSunAltitude.count) lights are captured during astronomical twilight!",
											   images: highSunAltitude))
						
						let (minMean2, maxMean2) = minMaxMean(images: highSunAltitude)
						if minMean2 != nil && maxMean2 != nil {
							
							if minMean2! > 2048 {
								hints.append(HintsData(id: "IMG_ASTROTWI_HIGH_MEAN", type: .info, group: .imaging,
													   filter: filter, exposure: exposure,
													   text: "Lights captured during astronomical twilight have high mean values! Reduce exposure time or gain or use filters!"))
								
							} else {
								hints.append(HintsData(id: "IMG_ASTROTWI_MEAN_OK", type: .info, group: .imaging,
													   filter: filter, exposure: exposure,
													   text: "Lights captured during astronomical twilight have a mean value between \(minMean2!, specifier: "%.1f") and \(maxMean2!, specifier: "%.1f")!"))
							}
						}
					}
				}
				
				
				let highRMS = images.filter( { rmsMaxValue != nil && $0.rms != nil && $0.rms! > rmsMaxValue! })
				if highRMS.count > 0 {
					if highRMS.count == images.count {
						hints.append(HintsData(id: "IMG_HIGH_RMS_ALL", type: .warn, group: .imaging,
											   filter: filter, exposure: exposure,
											   text: "All lights are captured with poor guiding!", images: highRMS))
					} else {
						hints.append(HintsData(id: "IMG_HIGH_RMS", type: .warn, group: .imaging,
											   filter: filter, exposure: exposure,
											   text: "\(highRMS.count) lights are captured with poor guiding! Check the HFR and eccentricity!", images: highRMS))
					}
					
					if let cov = self.correlationAirmassHFR(images: highRMS), cov > 0.66 {
						hints.append(HintsData(id: "IMG_AFFECTEDBY_AIRMASS", type: .warn, group: .imaging,
											   filter: filter, exposure: exposure,
											   text: "\(highRMS.count) lights are affected by Airmass!", images: highRMS))
					}
					
					
				} else {
					hints.append(HintsData(id: "IMG_GOOD_GUIDING", type: .info, group: .imaging,
										   filter: filter, exposure: exposure,
										   text: "All lights are captured with guiding good enough for your image resolution!"))
				}
				
				
				if highSunAltitude.count > 0 {
					
					if let cor = self.correlationSunAltMean(images: highSunAltitude), abs(cor) > 0.66 {
						hints.append(HintsData(id: "IMG_MEAM_SUNALT", type: .warn, group: .imaging,
											   filter: filter, exposure: exposure,
											   text: "Sun altitude during exposure did affect the mean value! Correlation: \(cor, specifier: "%.2f")"))
					}
				}
					
				let highMoonAltitude = images.filter({ $0.moonAlt != nil && $0.moonAlt! > -0.5 })
				if highMoonAltitude.count > 0 {
					
					if let cor = self.correlationMoonAltMean(images: highMoonAltitude), abs(cor) > 0.66 {
						hints.append(HintsData(id: "IMG_MEAN_MOONALT", type: .warn, group: .imaging,
											   filter: filter, exposure: exposure,
											   text: "Moon altitude during exposure did affect the mean value! Correlation: \(cor, specifier: "%.2f")"))
					}
				}
				
				if let cor = self.correlationDeltaTMean(images: images), abs(cor) > 0.66 {
					hints.append(HintsData(id: "IMG_MEAN_DELTATEMP", type: .warn, group: .imaging,
										   filter: filter, exposure: exposure,
										   text: "ΔT correlated to mean value, correlation: \(cor, specifier: "%.2f"). Perhaps there were some cirrus clouds?"))
				}
				
				let eccentricities = images.filter({ $0.eccentricity != nil
					&& $0.eccentricity! > settings.getStarEccentricityTolerance() })
				
				if eccentricities.count > 0 {
					if eccentricities.count == images.count {
						hints.append(HintsData(id: "IMG_ECC_ALL", type: .critical, group: .imaging,
											   filter: filter, exposure: exposure,
											   text: "All lights have a star eccentricity higher than the defined star eccentricity tolerance! Check your tilt and backfocus and try to improve your guiding!"))
					} else {
						hints.append(HintsData(id: "IMG_ECC_SOME", type: .warn, group: .imaging,
											   filter: filter, exposure: exposure,
											   text: "\(eccentricities.count) lights have a star eccentricity higher than the defined star eccentricity tolerance!", images: eccentricities))
					}
				} else {
					hints.append(HintsData(id: "IMG_ECC_NONE", type: .info, group: .imaging,
										   filter: filter, exposure: exposure,
										   text: "All lights have a star eccentricity lower than the defined star eccentricity tolerance!"))
				}
				
				let (minHFR, maxHFR) = minMaxHFR(images: images)
				if maxHFR != nil && minHFR != nil {
					if maxHFR! >= minHFR! + minHFR! * settings.getHFRTolerance(filter) {
						let images = images.filter({ $0.hfr != nil && $0.hfr! > minHFR! + minHFR! * settings.getHFRTolerance(filter)})
						if images.count > 0 {
							
							hints.append(HintsData(id: "IMG_HIGH_HFR", type: .warn, group: .imaging,
												   filter: filter, exposure: exposure,
												   text: "\(images.count) lights have a higher HFR than the defined HFR tolerance!",
												   images: images))
						}
					} else {
						hints.append(HintsData(id: "IMG_HFR_OK", type: .info, group: .imaging,
											   filter: filter, exposure: exposure,
											   text: "All lights have an acceptable HFR!"))
					}
				}
			}
		}
			
		return hints
	}

	func createRotatorHints(_ date: DateOfSession) -> [HintsData] {
		
		var hints = [HintsData]()
		
		let allImages = self.sessionImages.filter({ $0.SessionDate != nil && $0.SessionDate!.text == date.text || date.text == "Any" })
		
		let filters = allImages.map { $0.filter }.distinct()
		
		for filter in filters {
			let filterImages = allImages.filter({ $0.filter == filter })
			let exposures = filterImages.map { $0.Exposure }.distinct()
			
			for exposure in exposures {
				
				let images = filterImages.filter({ $0.Exposure == exposure})
				if images.isEmpty {
					continue
				}
								
				let wrongImages = images.filter({ $0.targetRotation != nil && $0.rotatorAngle != nil
					&& deltaAngle($0.targetRotation!, $0.rotatorAngle!) > 5.0})
				
				if wrongImages.count > 0 {
					
					if wrongImages.count == images.count {
						hints.append(HintsData(id: "ROTATOR_NOT_CORRECT_ALL", type: .warn, group: .rotator,
											   filter: filter, exposure: exposure,
											   text: "Image rotation does not meet the required target rotation for all lights! Use the Manual Rotator and slew and rotate in N.I.N.A. to avoid this issue!"))
						
					} else {
						hints.append(HintsData(id: "ROTATOR_NOT_CORRECT_SOME", type: .warn, group: .rotator,
											   filter: filter, exposure: exposure,
											   text: "Image rotation does not meet the required target rotation for \(images.count) lights!",
											   images: images))
					}
				} else {
					hints.append(HintsData(id: "ROTATOR_CORRECT_ALL", type: .info, group: .rotator,
										   filter: filter, exposure: exposure,
										   text: "Image rotation meets the required target rotation for all lights!"))
				}
			}
		}
		
		return hints
	}

	func createGuidingHints(_ date: DateOfSession) -> [HintsData] {
		
		var hints = [HintsData]()
		let allImages = self.sessionImages.filter({ $0.SessionDate != nil && $0.SessionDate!.text == date.text || date.text == "Any" })
		let filters = allImages.map { $0.filter }.distinct()
		
		for filter in filters {
			let filterImages = allImages.filter({ $0.filter == filter })
			let exposures = filterImages.map { $0.Exposure }.distinct()
			
			for exposure in exposures {
				
				let images = filterImages.filter({ $0.Exposure == exposure} )
				if images.isEmpty {
					continue
				}
				
				if let covHFR = self.correlationHFRRMS(images: images), abs(covHFR) > 0.66 {
					hints.append(HintsData(id: "GUIDING_HFR_RMS_CORR", type: .warn, group: .guiding,
										   filter: filter, exposure: exposure,
										   text: "Guiding RMS seems to affect HFR/FWHM values reducing image sharpness! Correlation: \(covHFR, specifier: "%.2f")"))
				}
				
				if let covECC = self.correlationEccentricityGuiding(images: images), abs(covECC) > 0.66 {
					hints.append(HintsData(id: "GUIDING_ECC_RMS_CORR", type: .warn, group: .guiding,
										   filter: filter, exposure: exposure,
										   text: "Bad guiding affected the star shapes, check the star eccentricities! Correlation: \(covECC, specifier: "%.2f")"))
				}
 
				
				if let covAM = self.correlationAirmassHFR(images: images), abs(covAM) > 0.66 {
					hints.append(HintsData(id: "GUIDING_AIRMASS_RMS_CORR", type: .warn, group: .guiding,
										   filter: filter, exposure: exposure,
										   text: "Higher HFR values are affected by Airmass! Correlation: \(covAM, specifier: "%.2f")"))
				}
				
				if self.guiding.guidingSessions.count > 0 {
					var hash = Set<Float>()
					
					for gs in self.guiding.guidingSessions {
						if let pxS = gs.pixelScale, let first = images.first {
							if !hash.contains(pxS) {
								hash.insert(pxS)
								
								if pxS < 0.5 {
									hints.append(HintsData(id: "GUIDING_RES_TOO_LARGE", type: .warn, group: .guiding,
														   filter: filter, exposure: exposure,
														   text: "Guiding resolution too large! Should be greater than 0.5\"/px"))
									
								}
								
								if let pxRes = first.PixelResolution {
									let d = pxRes / pxS
									
									if d > 5.0 {
										hints.append(HintsData(id: "GUIIDNG_SCALE_TOO_LARGE", type: .warn, group: .guiding,
															   filter: filter, exposure: exposure,
															   text: "Guiding scale relation too large (1 :\(pxS, specifier: "%.1f"))!"))
									} else {
										hints.append(HintsData(id: "GUIIDNG_SCALE_OK", type: .info, group: .guiding,
															   filter: filter, exposure: exposure,
															   text: "Guiding pixel scale relation okay (1 :\(pxS, specifier: "%.1f"))"))
										
									}
								}
							}
						}
					}
					
					let lostStars: [LostStarEvents] = self.guiding.guidingSessions.flatMap({ self.guiding.lostStarEvents($0.id) })
					if lostStars.count > 0 {
						hints.append(HintsData(id: "GUIDING_LOST_STARS", type: .info, group: .guiding,
											   filter: filter, exposure: exposure,
											   text: "Lost stars during guiding! Increase SNR Threshold in PHD2 or exposure time/gain, if this happens very often!"))
					}
				}
			}
		}
		
		return hints
	}
	
	private func createErrorHints(_ date: DateOfSession) -> [HintsData] {
		var hints = [HintsData]()
		let logData = self.ninaLogs.getLogData(for: date).filter { $0.level == NINALogType.warn || $0.level == NINALogType.error }
		if !logData.isEmpty {
			// group the found errors and count the number of occurances
			let errorGroups = logData.groupBy(keyFunc: { "\($0.source).\($0.member)" })
			for key in errorGroups.keys {
				if let values = errorGroups[key] {
					if values.count == 1 {
						hints.append(HintsData(id: key, type: .warn, group: .nina,
											   logs: values,
											   text: "Error in N.I.N.A. log"))
					} else {
						hints.append(HintsData(id: key, type: .warn, group: .nina,
											   logs: values,
											   text: "\(values.count) errors in N.I.N.A. log"))
					}
				}
			}
		}
		
		return hints
	}
	
	
	private func deltaAngle(_ a1: Float, _ a2: Float) -> Float {
		let delta = abs(a1 - a2)
		if delta < 90.0 {
			return delta
		}
		return abs(delta - 180.0)
	}
}
