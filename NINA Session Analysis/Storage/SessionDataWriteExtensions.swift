//
//  SessionDataWriteExtensions.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 16.04.24.
//

import Foundation

struct FitsWriteSettings {
	var hfr: Bool
	var fwhm: Bool 
	var eccentricity: Bool
	var mean: Bool
	var median: Bool
	var aduMin: Bool
	var aduMax: Bool
	var numStars: Bool
	var rms: Bool
	var rmsRA: Bool
	var rmsDEC: Bool
	var overwrite: Bool
	var path: URL?
	
}

typealias FITSWriteProgressCallback = (Float) -> Void

extension SessionData {
	
	func write(images: [SessionImage], settings: FitsWriteSettings, onProgress: FITSWriteProgressCallback) async {
		
		let total = Float(images.count)
		var current = 0.0
		for image in images {
			writeFITSHeader(image: image, settings: settings) {
				current += 1
				onProgress(Float(current)/total)
			}
		}
	}
	
	
	func writeFITSHeader(image: SessionImage, settings: FitsWriteSettings, onProgress: () -> Void) {
		
		/*
		if let fits = try? FitsFile.read(contentsOf: image.url) {
			
			onProgress()
			
			let header = fits.prime.headerUnit
			
			if settings.hfr, let hfr = image.hfr {
				header["HFR"] = hfr
			}
			
			if settings.fwhm, let fwhm = image.fwhm {
				header["FWHM"] = fwhm
			}
			
			if settings.eccentricity, let ecc = image.eccentricity {
				header["ECCEN"] = ecc
			}
			
			if settings.mean, let mean = image.mean {
				header["MEAN"] = mean
			}
			
			if settings.median, let median = image.median {
				header["MEDIAN"] = median
			}
			
			if settings.aduMin, let aduMin = image.aduMin {
				header["ADUMIN"] = aduMin
			}
			
			if settings.aduMax, let aduMax = image.aduMax {
				header["ADUMAX"] = aduMax
			}
			
			if settings.numStars, let numStars = image.numStars {
				header["NSTARS"] = numStars
			}
			
			if settings.rms, let rms = image.rms {
				header["RMS"] = rms
			}
			
			if settings.rmsRA, let rmsRA = image.rmsRA {
				header["RMSRA"] = rmsRA
			}
			
			if settings.rmsDEC, let rmsDEC = image.rmsDEC {
				header["RMSDEC"] = rmsDEC
			}
			
			if let pathURL = settings.path {
				let url = pathURL.appendingPathComponent(image.FileName)
				fits.write(to: url) { error in
					print(error.localizedDescription)
				} onCompleation: {
					onProgress()
				}
			} else {
				fits.write(to: image.url) { error in
					print(error.localizedDescription)
				} onCompleation: {
					onProgress()
				}
			}
		}*/
		
	}
	
}
