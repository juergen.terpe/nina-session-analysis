//
//  GuideGraphData.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 31.03.24.
//

import Foundation

struct GuideGraphData: Identifiable {
	let id: UUID
	var time: Float
	var ra: Float
	var dec: Float
	var pulseRA: Float
	var pulseDEC: Float
	var snr: Float?
	var drop: Bool = false
	var error: Int = 0
	var exposure: Float
	
	func getValue(_ type: GuideGraphLineType) -> Float {
		switch type {
			case .ra: return ra
			case .dec: return dec
			case .snr: return -(snr ?? 0.0)
		}
	}
}

struct DitherGraphData: Identifiable {
	let id: UUID 
	let time1: Float
	let time2: Float
	let jdd: JulianDate
}



enum GuidingIssueType {
	case overshootRA
	case insufficientRA
	case overshootDEC
	case insufficientDEC
}


struct GuidingIssueGraphData: Identifiable {
	let id: UUID = UUID()
	let type: GuidingIssueType
	let time1: Float
	let time2: Float
	let amounts: [(time: Float, value: Float)] 
}
