//
//  SessionIssues.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 05.04.24.
//

import Foundation

enum SessionIssues: Int, Identifiable {
	case aduMinZero = 1
	case aduMaxLimitReached = 2
	case zeroOffset = 3
	case elongatedStars = 4
	case badGuiding = 5
	case highMeanValue = 9
	case hfrIncreased = 10
	case balanceIssue = 11
	case badFocus = 12
	case badCalibration = 13
	case ditherSettleIssue = 14
	case driftedFrame = 15
    case highHumidity = 16
    case highWindSpeeds = 17
	case highCloudCoverage = 18
	
	var id: Int {
		self.rawValue
	}
	
	var imageName: String {
		switch self {
			case .aduMinZero: return "0.square"
			case .aduMaxLimitReached: return "gauge.with.dots.needle.100percent"
			case .zeroOffset: return "00.square"
			case .elongatedStars: return "capsule.fill"
			case .badGuiding: return "alternatingcurrent"
			case .highMeanValue: return "moon.haze.fill"
			case .hfrIncreased: return "aqi.low"
			case .balanceIssue: return "signpost.left"
			case .badFocus: return "wrongwaysign"
			case .badCalibration: return "yieldsign.fill"
			case .ditherSettleIssue: return "exclamationmark.transmission"
			case .driftedFrame: return "tortoise"
			case .highHumidity: return "humidity"
			case .highWindSpeeds: return "wind"
			case .highCloudCoverage: return "cloud.moon.fill"
		}
	}
	
	var issueText: String {
		switch self {
			case .aduMinZero: return "ADU(min)"
			case .aduMaxLimitReached: return "ADU(max)"
			case .zeroOffset: return "Offset"
			case .elongatedStars: return "Elongated Stars"
			case .badGuiding: return "Bad Guiding"
			case .highMeanValue: return "Mean"
			case .hfrIncreased: return "HFR"
			case .balanceIssue: return "Bad Balance"
			case .badFocus: return "Bad Focus"
			case .badCalibration: return "Bad Calibration"
			case .ditherSettleIssue: return "Dither Settling"
			case .driftedFrame: return "Drifted"
			case .highHumidity: return "Humidity"
			case .highWindSpeeds: return "Wind"
			case .highCloudCoverage: return "Cloud Coverage"
		}
	}
}
