//
//  ImageMetadata.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 30.03.24.
//

import Foundation

struct ImageMetadata {
	/*
	 var index = row["ExposureNumber"]
	 var filepath = row["FilePath"]
	 var mean = row["ADUMean"]
	 var median = row["ADUMedian"]
	 var aduMin = row["ADUMin"]
	 var aduMax = row["ADUMax"]
	 var numStars = row["DetectedStars"]
	 var hfr = row["HFR"]
	 var hfrStDev = row["HFRStDev"]
	 var fwhm = row["FWHM"]
	 var ecc = row["Eccentricity"]
	 var rms = row["GuidingRMSArcSec"]
	 var rmsRA = row["GuidingRMSRAArcSec"]
	 var rmsDEC = row["GuidingRMSDECArcSec"]
	 */
	
	var index: Int
	var filePath: String
	var mean: Float?
	var median: Int?
	var aduMin: Int?
	var aduMax: Int?
	var numStars: Int?
	var hfr: Float?
	var hfrStdDev: Float?
	var fwhm: Float?
	var eccentricity: Float?
	var rms: Float?
	var rmsRA: Float?
	var rmsDEC: Float?
	
	init(index: String, filePath: String, mean: String, median: String,
		 aduMin: String, aduMax: String, numStars: String, hfr: String, hfrStdDev: String, fwhm: String,
		 eccentricity: String, rms: String, rmsRA: String, rmsDEC: String) {
		
		self.index = (ImageMetadata.parseInt(index) ?? -1) + 1
		self.filePath = URL(fileURLWithPath: filePath).lastPathComponent
		self.mean = ImageMetadata.parseFloat(mean)
		self.median = ImageMetadata.parseInt(median)
		self.aduMin = ImageMetadata.parseInt(aduMin)
		self.aduMax = ImageMetadata.parseInt(aduMax)
		self.numStars = ImageMetadata.parseInt(numStars)
		if let hfrValue = ImageMetadata.parseFloat(hfr), hfrValue > 0.0 {
			self.hfr = hfrValue
			
			self.hfrStdDev = ImageMetadata.parseFloat(hfrStdDev)
			if let fwhm = ImageMetadata.parseFloat(fwhm), fwhm > 0.0 {
				self.fwhm = fwhm
			}
			self.eccentricity = ImageMetadata.parseFloat(eccentricity)
			
		} else {
			self.hfr = nil
			self.hfrStdDev = nil
			self.fwhm = nil
			self.eccentricity = nil
		}
				
		self.rms = ImageMetadata.parseFloat(rms)
		self.rmsRA = ImageMetadata.parseFloat(rmsRA)
		self.rmsDEC = ImageMetadata.parseFloat(rmsDEC)
	}
	
	private static func parseFloat(_ value: String) -> Float? {
		if value.isEmpty {
			return nil
		}
		if let val = Float(value), !val.isNaN {
			return val
		}
		return nil
	}
	
	private static func parseInt(_ value: String) -> Int? {
		if value.isEmpty {
			return nil
		}
		if let val = Int(value) {
			return val
		}
		return nil
	}
	
}
