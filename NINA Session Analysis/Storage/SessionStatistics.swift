//
//  SessionStatistics.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 01.04.24.
//

import Foundation

public struct StatisticsValue {
	var min: Float
	var max: Float
	var mean: Float
}

public struct SessionStatistics {
	var hfr: StatisticsValue?
	var fwhm: StatisticsValue?
	var numStars: StatisticsValue?
	var eccentricty: StatisticsValue?
	var median: StatisticsValue?
	var mean: StatisticsValue?
	var rms: StatisticsValue?
	var rmsRA: StatisticsValue?
	var rmsDEC: StatisticsValue?
}

extension SessionData {
	
	func getSessionStatistics() -> SessionStatistics? {
		let images = self.filteredImages
		
		if images.count == 0 {
			return nil
		}
		
		
		let hfrValues = images.map { $0.hfr }.filter({ $0 != nil }).map({ $0! })
		let hfr = getStatisticsValue(hfrValues)
		
		let fwhmValues = images.map { $0.fwhm }.filter({ $0 != nil }).map({ $0! })
		let fwhm = getStatisticsValue(fwhmValues)
		
		let starValues = images.map { $0.numStars }.filter({ $0 != nil }).map({ Float($0!) })
		let numStars = getStatisticsValue(starValues)
		
		let eccValues = images.map { $0.eccentricity }.filter({ $0 != nil }).map({ Float($0!) })
		let ecc = getStatisticsValue(eccValues)
		
		let medValues = images.map { $0.median }.filter({ $0 != nil }).map({ Float($0!) })
		let median = getStatisticsValue(medValues)
		
		let meanValues = images.map { $0.mean }.filter({ $0 != nil }).map({ Float($0!) })
		let mean = getStatisticsValue(meanValues)
		
		let rmsValues = images.map { $0.rms }.filter({ $0 != nil }).map({ Float($0!) })
		let rms = getStatisticsValue(rmsValues)
		
		let rmsRAValues = images.map { $0.rmsRA }.filter({ $0 != nil }).map({ Float($0!) })
		let rmsRA = getStatisticsValue(rmsRAValues)
		
		let rmsDECValues = images.map { $0.rmsDEC }.filter({ $0 != nil }).map({ Float($0!) })
		let rmsDEC = getStatisticsValue(rmsDECValues)
		
		return SessionStatistics(hfr: hfr, fwhm: fwhm, numStars: numStars, eccentricty: ecc, median: median, mean: mean, rms: rms, rmsRA: rmsRA, rmsDEC: rmsDEC)
	}
	
	private func getStatisticsValue(_ values: [Float]) -> StatisticsValue? {
		if values.count == 0 {
			return nil
		}
		
		var minV: Float?
		var maxV: Float?
		var sum: Float = 0.0
		for v in values {
			sum += v
			if minV == nil {
				minV = v
			} else {
				minV = min(v, minV!)
			}
			
			if maxV == nil {
				maxV = v
			} else {
				maxV = max(v, maxV!)
			}
		}
		
		return StatisticsValue(min: minV!, max: maxV!, mean: sum/Float(values.count))
	}
	
}
