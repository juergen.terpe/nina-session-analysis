//
//  MissingData.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 25.09.24.
//

import Foundation

struct DataSummary: Identifiable {
	let id = UUID()
	
	let image: String
	let guideLog: Bool
	let ninaLog: Bool
	let metaData: MetaDataState
	let date: Date?
	let exposure: String
	let filter: String
	let bad: Bool
	
	init(image: String, guideLog: Bool, ninaLog: Bool, metaData: MetaDataState, date: Date?, exposure: String, filter: String, bad: Bool) {
		self.image = image
		self.guideLog = guideLog
		self.ninaLog = ninaLog
		self.metaData = metaData
		self.date = date
		self.bad = bad
		self.exposure = exposure
		self.filter = filter
	}
}
