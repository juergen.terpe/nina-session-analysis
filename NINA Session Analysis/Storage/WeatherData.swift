//
//  WeatherData.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 02.05.24.
//

import Foundation

struct WeatherData {
	//ExposureNumber,ExposureStart,Temperature,DewPoint,Humidity,Pressure,WindSpeed,WindDirection,WindGust,CloudCover,SkyTemperature,SkyBrightness,SkyQuality
	let exposureNumber: Int
	let exposureStart: Date
	let temp: Float?
	let dewPoint: Float?
	let humidity: Float?
	let pressure: Float?
	let windSpeed: Float?
	let windDir: Float?
	let windGust: Float?
	let cloudCover: Float?
	let skyTemp: Float?
	let skyBrightness: Float?
	let skyQuality: Float?
}
