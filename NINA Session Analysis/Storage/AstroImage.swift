//
//  AstroImage.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 19.06.24.
//

import Foundation
import CoreImage
import Accelerate


enum BayerMatrix: Int8 {
	case none = 0
	case rggb = 1
	case rgbg = 2
	case grgb = 3
	case grbg = 4
	case gbgr = 5
	case gbrg = 6
	case bgrg = 7
	case bggr = 8
}



class AstroImage {
	let width: Int
	let height: Int
	var bayerMatrix: BayerMatrix?
	var cgImage: CGImage?
	var imageData: [UInt16]?
	var bitDepth: Int
	var fits: Bool
	var id: UUID = UUID()

	init(cgImage: CGImage, bitDepth: Int, bayerMatrix: BayerMatrix? = nil, fits: Bool = true) {
		self.cgImage = cgImage
		self.width = cgImage.width
		self.height = cgImage.height
		self.bayerMatrix = bayerMatrix
		self.bitDepth = bitDepth
		self.fits = fits
	}

	init?(imageData: [UInt16], width: Int, height: Int, bayerMatrix: BayerMatrix, bitDepth: Int, fits: Bool = true) {
		self.imageData = imageData
		self.width = width
		self.height = height
		self.bayerMatrix = bayerMatrix
		self.bitDepth = bitDepth
		self.fits = fits
	}

	func debayerAndStretch() -> (stretched: AstroImage?, debayered: AstroImage?) {
		guard let bayerMatrix = self.bayerMatrix else { return (nil, nil) }
		let imageProcessor = ImageProcessor()

		if fits, let imageData = self.imageData {
			if let texture = imageProcessor.createTexture(from: imageData, width: width, height: height, bitDepth: bitDepth) {
				if let debayered = imageProcessor.debayer(image: texture, bayerPattern: bayerMatrix) {
					
					if let stretched = imageProcessor.stretch(image: debayered, targetBackground: 0.2) {
						if let cgImage = imageProcessor.createImage(from: stretched),
						   let cgImage2 = imageProcessor.createImage(from: debayered){
							return (stretched: AstroImage(cgImage: cgImage, bitDepth: bitDepth, fits: fits),
									debayered: AstroImage(cgImage: cgImage2, bitDepth: bitDepth, fits: fits))
						}
						
					} else if let cgImage = imageProcessor.createImage(from: debayered) {
						return (stretched: nil, debayered: AstroImage(cgImage: cgImage, bitDepth: bitDepth, fits: fits))
					}
				}
			}
		} else if let cgImage = self.cgImage {
			if let texture = imageProcessor.createTexture(from: cgImage, bitDepth: bitDepth) {
				if let debayered = imageProcessor.debayer(image: texture, bayerPattern: bayerMatrix) {
					
					if let stretched = imageProcessor.stretch(image: debayered, targetBackground: 0.2) {
						if let cgImage = imageProcessor.createImage(from: stretched),
						   let cgImage2 = imageProcessor.createImage(from: debayered){
							return (stretched: AstroImage(cgImage: cgImage, bitDepth: bitDepth, fits: fits),
									debayered: AstroImage(cgImage: cgImage2, bitDepth: bitDepth, fits: fits))
						}
					}
					
					if let cgImage = imageProcessor.createImage(from: debayered) {
						return (stretched: nil, debayered: AstroImage(cgImage: cgImage, bitDepth: bitDepth, fits: fits))
					}
				}
			}
		}
		return (nil, nil)
	}

	func debayer() -> AstroImage {
		guard let bayerMatrix = self.bayerMatrix else { return self }
		let imageProcessor = ImageProcessor()

		if fits, let imageData = self.imageData {
			if let texture = imageProcessor.createTexture(from: imageData, width: width, height: height, bitDepth: bitDepth) {
				if let debayered = imageProcessor.debayer(image: texture, bayerPattern: bayerMatrix) {
					if let cgImage = imageProcessor.createImage(from: debayered) {
						return AstroImage(cgImage: cgImage, bitDepth: bitDepth, fits: fits)
					}
				}
			}
		} else if let cgImage = self.cgImage {
			if let texture = imageProcessor.createTexture(from: cgImage, bitDepth: bitDepth) {
				if let debayered = imageProcessor.debayer(image: texture, bayerPattern: bayerMatrix) {
					if let cgImage = imageProcessor.createImage(from: debayered) {
						return AstroImage(cgImage: cgImage, bitDepth: bitDepth, fits: fits)
					}
				}
			}
		}
		return self
	}
}
