//
//  TipsStore.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 23.09.24.
//

import Foundation
import StoreKit

let tipIds = [
	"com.stardust.NINASessionAnalysis.TinyTip",
	"com.stardust.NINASessionAnalysis.MediumTip",
	"com.stardust.NINASessionAnalysis.LargeTip"
]


@Observable
@MainActor final class TipsStore {
	var tips: [Product] = []
	var activeTransactions: Set<StoreKit.Transaction> = []
	var purchased = false
	
	var hasAlreadyPurchased: Bool {
		
		if self.purchased {
			return true 
		}
		
		if let purchaseDate = UserDefaults.standard.object(forKey: "lastPurchase") as? Date {
			if abs(purchaseDate.timeIntervalSinceNow) < 160 { // show again after 160 days
				purchased = true
				return true
			}
		}
		return false
	}
	
	
	func fetch() async {
		
		do {
			self.tips = try await Product.products(for: tipIds)
				.sorted(by: { $0.price < $1.price })
			
			print("\(self.tips.count) tips fetched.")
		}
		catch {
			print(error)
			self.tips = []
		}
	}
	
	func purchase(_ product: Product) async {
		do {
			let result = try await product.purchase()
			switch result {
				case .success(let verificationResult):
					if let transaction = try? verificationResult.payloadValue {
						activeTransactions.insert(transaction)
						await transaction.finish()
						
						UserDefaults.standard.set(Date(), forKey: "lastPurchase")
						purchased = true
					}
				case .userCancelled:
					break
				case .pending:
					break
				@unknown default:
					break
			}
		}
		catch {
			print(error)
		}
	}
	
}
