//
//  Sun.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 30.03.24.
//

import Foundation

public class Sun {
	
	public static func getPosition(_ t: Double) -> Vector3 {
		let eps = 23.43929111 * Constants.Rad
		return Matrix3x3.rotateX(-eps) * getEclipticalPosition(t)
	}
	
	public static func getEclipticalPosition(_ t: Double) -> Vector3 {
		// Mean Anomaly and ecliptical longitude
		let M  = Constants.PI2 * frac( 0.993133 + 99.997361 * t)
		let L  = Constants.PI2 * frac ( 0.7859453 + M / Constants.PI2 +
										(6893.0*sin(M)+72.0*sin(2.0*M)+6191.2*t) / 1296.0e3)
		
		return Vector3(polar: Polar(phi: L, theta: 0.0))
	}
	
	public static func getAltAz(_ jd: JulianDate, long: Double, lat: Double) -> (azimuth: Double, altitude: Double)  {
		let pos = getPosition(jd.t)
		let polar = pos.toPolar()
		let lmst = jd.toLmst(long * Constants.Rad)
		
		let tau = lmst - polar.phi
		return convertToHorizontal(polar.theta, tau: tau, lat: lat * Constants.Rad)
	}
}
