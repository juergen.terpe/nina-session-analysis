//
//  Nutation.swift
//  AstronomicalCalculations
//
//  Created by Jürgen Terpe on 10.09.16.
//  Copyright © 2016 Stardust. All rights reserved.
//

import Foundation

final public class Nutation {
	
	struct NutationCoefficient {
		var D: Double
		var M: Double
		var Mprime: Double
		var F: Double
		var omega: Double
		var sincoeff1: Double
		var sincoeff2: Double
		var coscoeff1: Double
		var coscoeff2: Double
		
		init(_ D: Int, _ M: Int, _ Mprime: Int, _ F: Int, _ omega: Int, _ sincoeff1: Int, _ sincoeff2: Double, _ coscoeff1: Int, _ coscoeff2: Double) {
			self.D = Double(D)
			self.M = Double(M)
			self.Mprime = Double(Mprime)
			self.F = Double(F)
			self.omega = Double(omega)
			self.sincoeff1 = Double(sincoeff1)
			self.sincoeff2 = sincoeff2
			self.coscoeff1 = Double(coscoeff1)
			self.coscoeff2 = coscoeff2
		}
	}
	
	private static let coefficients: [NutationCoefficient] = [
	
		NutationCoefficient(  0,  0,  0,  0,  1, -171996,  -174.2,  92025,     8.9    ),
		NutationCoefficient( -2,  0,  0,  2,  2,  -13187,    -1.6,   5736,    -3.1    ),
		NutationCoefficient(  0,  0,  0,  2,  2,   -2274,    -0.2,    977,    -0.5    ),
		NutationCoefficient(  0,  0,  0,  0,  2,    2062,     0.2,   -895,     0.5    ),
		NutationCoefficient(  0,  1,  0,  0,  0,    1426,    -3.4,     54,    -0.1    ),
		NutationCoefficient(  0,  0,  1,  0,  0,     712,     0.1,     -7,       0    ),
		NutationCoefficient( -2,  1,  0,  2,  2,    -517,     1.2,    224,    -0.6    ),
		NutationCoefficient(  0,  0,  0,  2,  1,    -386,    -0.4,    200,       0    ),
		NutationCoefficient(  0,  0,  1,  2,  2,    -301,       0,    129,    -0.1    ),
		NutationCoefficient( -2, -1,  0,  2,  2,     217,    -0.5,    -95,     0.3    ),
		NutationCoefficient( -2,  0,  1,  0,  0,    -158,       0,      0,       0    ),
		NutationCoefficient( -2,  0,  0,  2,  1,     129,     0.1,    -70,       0    ),
		NutationCoefficient(  0,  0, -1,  2,  2,     123,       0,    -53,       0    ),
		NutationCoefficient(  2,  0,  0,  0,  0,      63,       0,      0,       0    ),
		NutationCoefficient(  0,  0,  1,  0,  1,      63,     0.1,    -33,       0    ),
		NutationCoefficient(  2,  0, -1,  2,  2,     -59,       0,     26,       0    ),
		NutationCoefficient(  0,  0, -1,  0,  1,     -58,    -0.1,     32,       0    ),
		NutationCoefficient(  0,  0,  1,  2,  1,     -51,       0,     27,       0    ),
		NutationCoefficient( -2,  0,  2,  0,  0,      48,       0,      0,       0    ),
		NutationCoefficient(  0,  0, -2,  2,  1,      46,       0,    -24,       0    ),
		NutationCoefficient(  2,  0,  0,  2,  2,     -38,       0,     16,       0    ),
		NutationCoefficient(  0,  0,  2,  2,  2,     -31,       0,     13,       0    ),
		NutationCoefficient(  0,  0,  2,  0,  0,      29,       0,      0,       0    ),
		NutationCoefficient( -2,  0,  1,  2,  2,      29,       0,    -12,       0    ),
		NutationCoefficient(  0,  0,  0,  2,  0,      26,       0,      0,       0    ),
		NutationCoefficient( -2,  0,  0,  2,  0,     -22,       0,      0,       0    ),
		NutationCoefficient(  0,  0, -1,  2,  1,      21,       0,    -10,       0    ),
		NutationCoefficient(  0,  2,  0,  0,  0,      17,    -0.1,      0,       0    ),
		NutationCoefficient(  2,  0, -1,  0,  1,      16,       0,     -8,       0    ),
		NutationCoefficient( -2,  2,  0,  2,  2,     -16,     0.1,      7,       0    ),
		NutationCoefficient(  0,  1,  0,  0,  1,     -15,       0,      9,       0    ),
		NutationCoefficient( -2,  0,  1,  0,  1,     -13,       0,      7,       0    ),
		NutationCoefficient(  0, -1,  0,  0,  1,     -12,       0,      6,       0    ),
		NutationCoefficient(  0,  0,  2, -2,  0,      11,       0,      0,       0    ),
		NutationCoefficient(  2,  0, -1,  2,  1,     -10,       0,      5,       0    ),
		NutationCoefficient(  2,  0,  1,  2,  2,     -8,        0,      3,       0    ),
		NutationCoefficient(  0,  1,  0,  2,  2,      7,        0,     -3,       0    ),
		NutationCoefficient( -2,  1,  1,  0,  0,     -7,        0,      0,       0    ),
		NutationCoefficient(  0, -1,  0,  2,  2,     -7,        0,      3,       0    ),
		NutationCoefficient(  2,  0,  0,  2,  1,     -7,        0,      3,       0    ),
		NutationCoefficient(  2,  0,  1,  0,  0,      6,        0,      0,       0    ),
		NutationCoefficient( -2,  0,  2,  2,  2,      6,        0,     -3,       0    ),
		NutationCoefficient( -2,  0,  1,  2,  1,      6,        0,     -3,       0    ),
		NutationCoefficient(  2,  0, -2,  0,  1,     -6,        0,      3,       0    ),
		NutationCoefficient(  2,  0,  0,  0,  1,     -6,        0,      3,       0    ),
		NutationCoefficient(  0, -1,  1,  0,  0,      5,        0,      0,       0    ),
		NutationCoefficient( -2, -1,  0,  2,  1,     -5,        0,      3,       0    ),
		NutationCoefficient( -2,  0,  0,  0,  1,     -5,        0,      3,       0    ),
		NutationCoefficient(  0,  0,  2,  2,  1,     -5,        0,      3,       0    ),
		NutationCoefficient( -2,  0,  2,  0,  1,      4,        0,      0,       0    ),
		NutationCoefficient( -2,  1,  0,  2,  1,      4,        0,      0,       0    ),
		NutationCoefficient(  0,  0,  1, -2,  0,      4,        0,      0,       0    ),
		NutationCoefficient( -1,  0,  1,  0,  0,     -4,        0,      0,       0    ),
		NutationCoefficient( -2,  1,  0,  0,  0,     -4,        0,      0,       0    ),
		NutationCoefficient(  1,  0,  0,  0,  0,     -4,        0,      0,       0    ),
		NutationCoefficient(  0,  0,  1,  2,  0,      3,        0,      0,       0    ),
		NutationCoefficient(  0,  0, -2,  2,  2,     -3,        0,      0,       0    ),
		NutationCoefficient( -1, -1,  1,  0,  0,     -3,        0,      0,       0    ),
		NutationCoefficient(  0,  1,  1,  0,  0,     -3,        0,      0,       0    ),
		NutationCoefficient(  0, -1,  1,  2,  2,     -3,        0,      0,       0    ),
		NutationCoefficient(  2, -1, -1,  2,  2,     -3,        0,      0,       0    ),
		NutationCoefficient(  0,  0,  3,  2,  2,     -3,        0,      0,       0    ),
		NutationCoefficient(  2, -1,  0,  2,  2,     -3,        0,      0,       0    ),
	]
	
	private static func modulo360(_ d: Double) -> Double {
		let fd = frac(d/360.0) * 360.0
		if fd < 0.0 {
			return fd + 360.0
		}
		return fd
	}
	
	public static func getNutationInLongitude(_ t: Double) -> Double {
	
		let t2 = t*t
		let t3 = t2*t
	
		let D = modulo360(297.85036 + 445267.111480*t - 0.0019142*t2 + t3 / 189474.0)
		let M = modulo360(357.52772 + 35999.050340*t - 0.0001603*t2 - t3 / 300000.0)
		let Mprime = modulo360(134.96298 + 477198.867398*t + 0.0086972*t2 + t3 / 56250.0)
		let F = modulo360(93.27191 + 483202.017538*t - 0.0036825*t2 + t3 / 327270.0)
		let omega = modulo360(125.04452 - 1934.136261*t + 0.0020708*t2 + t3 / 450000.0)
	
		var sum = 0.0
		for nc in Nutation.coefficients {
			let a1 = nc.D * D + nc.M * M
			let a2 = nc.Mprime * Mprime + nc.F * F + nc.omega * omega
			
			let arg =  Constants.Rad * (a1 +  a2)
			sum += (nc.sincoeff1 + nc.sincoeff2 * t) * sin(arg) * 0.0001
		}
	
		return sum
	}
	
	/// Gets the mean obliquity of the ecliptic in degrees.
	///
	/// - Parameter t: time in julian centuries
	/// - Returns: obliquity of ecliptic in degrees.
	public static func getMeanObliquityOfEcliptic(_ t: Double) -> Double {
	
		let u  = t / 100.0
		let u2 = u*u
		let u3 = u2*u
		let u4 = u2*u2
		let u5 = u4*u
		let u6 = u5*u
		let u7 = u6*u
		let u8 = u7*u
		let u9 = u8*u
		let u10 = u9*u
		
	
		return 23.43929111111111
			- 1.30025833333333 * u
			- 0.00043055555556 * u2
			+ 0.55534722222222 * u3
			- 0.01427222222222 * u4
			- 0.06935277777778 * u5
			- 0.00027777777778 * u6
			+ 0.00197777777778 * u7
			+ 0.00774166666667 * u8
			+ 0.00160833333333 * u9
			+ 0.00068055555556 * u10
	}
	
	
	/// Gets the nutation in ecliptic obliquity in arcseconds (degrees)
	///
	/// - Parameter t: time in julian centuries
	/// - Returns: Nutation of ecliptic obliquity in arcseconds (degrees)
	public static func getNutationInObliquity(_ t: Double) -> Double {
	
		let t2 = t*t
		let t3 = t2*t
	
		let D = modulo360(297.85036 + 445267.111480*t - 0.0019142*t2 + t3 / 189474.0)
		let M = modulo360(357.52772 + 35999.050340*t - 0.0001603*t2 - t3 / 300000.0)
		let Mprime = modulo360(134.96298 + 477198.867398*t + 0.0086972*t2 + t3 / 56250.0)
		let F = modulo360(93.27191 + 483202.017538*t - 0.0036825*t2 + t3 / 327270.0)
	
		let omega = modulo360(125.04452 - 1934.136261*t + 0.0020708*t2 + t3 / 450000.0)
		var sum = 0.0
		for nc in Nutation.coefficients {
			
			let arg = Constants.Rad * (nc.D * D + nc.M * M + nc.Mprime * Mprime + nc.F * F + nc.omega * omega)
			sum += (nc.coscoeff1 + nc.coscoeff2 * t) * cos(arg) * 0.0001
		}
	
		return sum
	}
}
