//
//  Helpfunctions.swift
//  Sky Calendar
//
//  Created by Jürgen Terpe on 02.01.16.
//  Copyright © 2016 Stardust. All rights reserved.
//

import Foundation


public func sign(_ v: Double) -> Double {
	return v >= 0.0 ? 1.0 : -1.0
}

public func ddd(_ d: Int, min: Int, sec: Double) -> Double {
	var sign : Double = 1.0
	
	if (d<0) || (min<0) || (sec<0.0) {
		sign = -1.0
	}
	
	let d1 = Double(abs(d))
	let d2 = Double(abs(min))/60.0
	let d3 = fabs(sec)/3600.0
	return  sign * (d1 + d2 + d3)
}

public struct DegreeMinuteSeconds : CustomStringConvertible
{
	public var degree: Int
	public var minute: Int
	public var seconds: Double
	
	public var description: String {
		get {
			let secondsFormatter = NumberFormatter()
			secondsFormatter.maximumFractionDigits = 2
			secondsFormatter.minimumFractionDigits = 2
			secondsFormatter.minimumIntegerDigits = 2
			
		 	let negative = degree<0 || minute<0 || seconds<0
			let deg = abs(degree)
			let min = abs(minute)
			let sec = fabs(seconds)
			
			let pred = negative ? "-" : ""
			
			if deg > 0 {
				return pred + "\(deg)° \(min)' \(secondsFormatter.string(from: NSNumber(value: sec))!)\""
			}
			else if min > 1 {
				return pred + "\(min)' \(secondsFormatter.string(from: NSNumber(value: sec))!)\""
			}
			else {
				let totalSeconds = Double(min) * 60.0 + sec
				return pred + "\(secondsFormatter.string(from: NSNumber(value: totalSeconds))!)\""
			}
		}
	}
}

public func dms(_ d: Angle) -> DegreeMinuteSeconds {
	return dms(d.toDegrees())
}

public func dms(_ d: Double) -> DegreeMinuteSeconds {
	var x = fabs(d)
	var degree = Int(x)
	x = (x-Double(degree))*60.0
 	var minute = Int(x)
	var seconds = (x-Double(minute))*60.0
	
	if d<0.0 {
		if degree != 0 {
			degree *= -1
		}
		else if minute != 0 {
			minute *= -1
		}
		else {
			seconds *= -1.0
		}
		if seconds <= -60.0 {
			seconds += 60.0
			minute -= 1
		}
		if minute <= -60 {
			minute += 60
			degree -= 1
		}
	}
	else {
		if seconds >= 60.0 {
			seconds -= 60.0
			minute += 1
		}
		if minute >= 60 {
			minute -= 60
			degree += 1
		}
	}
	
	return DegreeMinuteSeconds(degree: degree, minute: minute, seconds: seconds)
}

/// Formats the angle in degrees, minuts and seconds (if less than 2°)
///
/// - Parameters:
///   - angle: the angle in degrees
public func formatDegreeMinuteSecondsAuto(_ angle: Double) -> String {
	if fabs(angle) < 2.0 {
		return formatDegreeMinuteSeconds(angle, withSeconds: true)
	}
	return formatDegreeMinuteSeconds(angle, withSeconds: false)
}

public func formatHourMinuteSeconds(_ angle: Angle) -> String {
	return formatHourMinuteSeconds(angle.toDegrees())
}

public func formatHourMinuteSeconds(_ angle: Double) -> String {
	let hms = dms(angle)
	return String(format: "%02dh %02d' %02.1f\"", hms.degree, hms.minute, hms.seconds)
}

public func formatDegreeMinuteSeconds(_ angle: Angle, withSeconds sec: Bool = true) -> String {
	return formatDegreeMinuteSeconds(angle.toDegrees(), withSeconds: sec)
}

/// Formats the angle in degrees, minuts and seconds
///
/// - Parameters:
///   - angle: the angle in degrees
///   - withSeconds: includes the seconds, otherwise rounds
public func formatDegreeMinuteSeconds(_ angle: Double, withSeconds: Bool = true) -> String {
	var degMinSec = dms(angle)
	
	if degMinSec.degree == 0 {
		return String(format: "%02d' %02.1f\"", degMinSec.minute, degMinSec.seconds)
	} else {
		if !withSeconds {
			if degMinSec.seconds > 30.0 {
				degMinSec.minute += 1
				if degMinSec.minute >= 60 {
					degMinSec.degree += 1
					degMinSec.minute -= 60
				}
			}
			
			return String(format: "%02d° %02d'", degMinSec.degree, degMinSec.minute)
		} else {
			return String(format: "%02d° %02d' %02.1f\"", degMinSec.degree, degMinSec.minute, degMinSec.seconds)
		}
	}
}

public func formatLatitude(_ angle: Double, withSeconds: Bool = true) -> String {
	var degMinSec = dms(angle)
	
	let deg = abs(degMinSec.degree)
	let min = abs(degMinSec.minute)
	let sec = fabs(degMinSec.seconds)
	let postfix = angle < 0.0 ? "S" : "N"
	
	if !withSeconds {
		if degMinSec.seconds > 30.0 {
			degMinSec.minute += 1
			if degMinSec.minute >= 60 {
				degMinSec.degree += 1
				degMinSec.minute -= 60
			}
		}
		
		return String(format: "%02d° %02d' ", deg, min) + postfix
	} else {
		return String(format: "%02d° %02d' %02.1f\" ", deg, min, sec) + postfix
	}
}

public func formatLongitude(_ angle: Double, withSeconds: Bool = true) -> String {
	var degMinSec = dms(angle)
	
	let deg = abs(degMinSec.degree)
	let min = abs(degMinSec.minute)
	let sec = fabs(degMinSec.seconds)
	let postfix = angle < 0.0 ? "W" : "E"
	
	if !withSeconds {
		if degMinSec.seconds > 30.0 {
			degMinSec.minute += 1
			if degMinSec.minute >= 60 {
				degMinSec.degree += 1
				degMinSec.minute -= 60
			}
		}
		
		return String(format: "%02d° %02d' ", deg, min) + postfix
	} else {
		return String(format: "%02d° %02d' %02.1f\" ", deg, min, sec) + postfix
	}
}

public func formatTimeSpan(_ d: Double) -> String {
	let date = Foundation.Date(timeIntervalSince1970:  TimeInterval(d))
		
	let dateFormatter = DateFormatter()
	dateFormatter.dateFormat = "HH:mm:ss"
	dateFormatter.timeZone = TimeZone(identifier: "UTC")
	return dateFormatter.string(from: date)
}

public func formatTimeSpan(_ d: Double, timeStyle: DateFormatter.Style) -> String {
	let date = Foundation.Date(timeIntervalSince1970:  TimeInterval(d))
	
	let dateFormatter = DateFormatter()
	dateFormatter.timeStyle = timeStyle
	dateFormatter.timeZone = TimeZone(identifier: "UTC")
	return dateFormatter.string(from: date)
}

public func formatTimeSpanDays(_ d: Double, _ approximationPhrase: Bool = false, _ timeRemainingPhrase: Bool = false,
							   _ unitsStyle: DateComponentsFormatter.UnitsStyle = .abbreviated) -> String {
	
	if fabs(d) > 3652.5 {
		let years = fabs(d)/365.25
		return String(format: "%3.1f %@", years, NSLocalizedString("years", comment: "years"))
	}
	
	let formatter = DateComponentsFormatter()
	if fabs(d) > 360.0 {
		formatter.allowedUnits = [.year, .month, .day ]
		formatter.allowsFractionalUnits = true
	} else if fabs(d) > 30.0 {
		formatter.allowedUnits = [.month, .day, .hour ]
		formatter.allowsFractionalUnits = true
	} else if fabs(d) > 1.0 {
		formatter.allowedUnits = [.day, .hour ]
		formatter.allowsFractionalUnits = true
	}else if fabs(d) > 0.5 {
		formatter.allowedUnits = [.day, .hour, .minute ]
	} else {
		formatter.allowedUnits = [.hour, .minute, .second ]
	}
	formatter.zeroFormattingBehavior = .dropLeading
	formatter.collapsesLargestUnit = false
	formatter.includesApproximationPhrase = approximationPhrase
	formatter.unitsStyle = unitsStyle
	let result = formatter.string(from: fabs(d)*86400.0)!

	if timeRemainingPhrase {
		if d < 0.0 {
			if let culture = Locale.preferredLanguages.first {
				if culture.hasPrefix("de") {
					return "vor " + result
				} else {
					return result + " ago"
				}
			}
		} else {
			return "in " + result
		}
	}
	return result
}


public func addTheorem(_ c1: Double, _ s1: Double, _ c2: Double, _ s2: Double, c: inout Double, s: inout Double)
{
	c = c1 * c2 - s1 * s2
	s = s1 * c2 + c1 * s2
}

public func limit(_ x: Double, between lower: Double, and upper: Double) -> Double {
	
	if x < lower {
		return lower
	}
	else if x > upper {
		return upper
	}
	return x
}

public func sincos(_ x: Double) -> (Double, Double) {
	var sA: Double = 0.0
	var cA: Double = 0.0
	__sincos(x, &sA, &cA)

	return (sA, cA)
}

public func sincos(_ x: Float) -> (Float, Float) {
	var sA: Double = 0.0
	var cA: Double = 0.0
	__sincos(Double(x), &sA, &cA)
	
	return (Float(sA), Float(cA))
}

public func frac(_ x: Double) -> Double {
	return x - floor(x)
}

public func frac(_ x: Float) -> Float {
	return x - floor(x)
}

public func clamp(_ x: Double, min minX: Double, max maxX: Double) -> Double {
	return max(min(x, maxX), minX)
}

public func modulo(_ x: Double, _ y: Double) -> Double {
	return y * frac(x/y)
}

public func signum(_ x: Double) -> Double {
	return x < 0.0 ? -1.0 : 1.0 
}


public func julianDateFor(_ year: Int, month: Int, day: Int, hour: Int, min: Int, sec: Double) -> Double {
	var mon = month
	var yy = year
	
	if mon<=2
	{
		mon += 12
		yy -= 1
	}
	
	var b : Int
	
	if (10000*yy+100*mon+day) <= 15821004 {
		b = -2 + ((yy+4716)/4) - 1179     // Jul. calendar
	}
	else {
		b = (yy/400)-(yy/100)+(yy/4)  // Greg. calendar
	}
	
	let daysForMonths = (Int)(30.6001 * (Double)(mon+1))
	let midnight = 365.0 * (Double)(yy) - 679004.0 + (Double)(b + daysForMonths + day)
	let fracOfDay   = ddd(hour, min: min, sec: sec) / 24.0
	
	return midnight + fracOfDay
}

public func cubeRoot(_ x: Double) -> Double {
	return exp(log(x) / 3.0)
}

public func arcTangens(_ sinx: Double, _ cosx: Double) -> Double {
	if cosx == 0.0 {
		if sinx > 0.0 {
			return Double.pi / 2.0
		}
		else {
			return 3.0 * Double.pi / 2.0
		}
	}
	else {
		if cosx > 0.0 {
			return atan(sinx / cosx)
		}
		else {
			return Double.pi + atan(sinx / cosx)
		}
	}
}

public func julianDate(_ year: Int, days: Double) -> Double {
	let jd0 = julianDateFor(year, month: 1, day: 1, hour: 0, min: 0, sec: 0.0)
	return jd0 + days
}

public func getYearFromJulianDate(_ jd: Double) -> Int {
	var b, c: Int
	
	let a = Int(jd+2400001.0)
	
	if a < 2299161 {
		// Julian Calendar
		b = 0
		c = a + 1524
	}
	else {
		// Gregorian Calendar
		b = Int((Double(a)-1867216.25)/36524.25)
		c = a +  b - (b/4) + 1525
	}
	
	let d = Int((Double(c)-122.1)/365.25)
	let e = 365*d + d/4
	let f = Int( Double(c-e)/30.6001)
	let month = f - 1 - 12*(f/14)
	
	return d - 4715 - ((7+month)/10)
}

public func getCalendarDate(_ jd: Double) -> (day: Int, month: Int, year: Int, hour: Int, minuts: Int, seconds: Double) {
	var b, c: Int
		
	let a = Int(jd+2400001.0)
		
	if a < 2299161 {
		// Julian Calendar
		b = 0
		c = a + 1524
	}
	else {
		// Gregorian Calendar
		b = Int((Double(a)-1867216.25)/36524.25)
		c = a +  b - (b/4) + 1525
	}
		
	let d = Int((Double(c)-122.1)/365.25)
	let e = 365*d + d/4
	let f = Int( Double(c-e)/30.6001)
		
	let day = c - e - Int(30.6001*Double(f))
	let month = f - 1 - 12*(f/14)
	let year  = d - 4715 - ((7+month)/10)
		
	let fracOfDay = jd - floor(jd)
	let hours = 24.0*fracOfDay
		
	let hms = dms(hours)
	
	return (day, month, year, hms.degree, hms.minute, hms.seconds)
}

public func getGreenwichMeanSideralTime(_ jd: Double) -> Double {
	let SecondsPerDay = 86400.0
	
	let jd0 = floor(jd)
	let ut = SecondsPerDay * (jd - jd0)
	
	let t0 = (jd0-51544.5)/36525.0
	let t  = (jd-51544.5)/36525.0
	
	let gmst  = 24110.54841 + 8640184.812866*t0 + 1.0027379093*ut
				+ (0.093104-6.2e-6*t)*t*t      // [sec]
	
	return (Double.pi * 2.0/SecondsPerDay) * modulo(gmst, SecondsPerDay)
}

public func getApparentGreenwichSiderealTime(_ jd: Double) -> Double {
	let SecondsPerDay = 86400.0
	let t = (jd - 51544.5)/36525.0
	
	let meanObliquity = Nutation.getMeanObliquityOfEcliptic(t)
	let trueObliquity = meanObliquity + Nutation.getNutationInObliquity(t) / 3600.0
	let nutationInLongitude = Nutation.getNutationInLongitude(t)

	let jd0 = floor(jd)
	let ut = SecondsPerDay * (jd - jd0)
	
	let t0 = (jd0-51544.5)/36525.0
	
	let gmst  = 24110.54841 + 8640184.812866*t0 + 1.0027379093*ut
		+ (0.093104-6.2e-6*t)*t*t      // [sec]
	
	let amst = gmst + (nutationInLongitude  * cos(Constants.Rad * trueObliquity) / 54000.0)
	
	return (Double.pi * 2.0/SecondsPerDay) * modulo(amst, SecondsPerDay)
}

public func getAngularDistance(_ pos1: Vector3, pos2: Vector3) -> Double {
	let phi1 = pos1.getPolarPhi()
	let theta1 = pos1.getPolarTheta()
	
	let phi2 = pos2.getPolarPhi()
	let theta2 = pos2.getPolarTheta()
	
	let sinTheta1 = sin(theta1)
	let sinTheta2 = sin(theta2)
	let cosTheta1 = cos(theta1)
	let cosTheta2 = cos(theta2)
	
	let d = sinTheta1 * sinTheta2 + cosTheta1 * cosTheta2 * cos(phi1-phi2)
	return acos(d)
}

public func getAltAz(pos: Vector3, jd: JulianDate, long: Double, lat: Double) -> (azimuth: Double, altitude: Double)  {
	let polar = pos.toPolar()
	let lmst = jd.toLmst(long * Constants.Rad)
		
	let tau = lmst - polar.phi
	return convertToHorizontal(polar.theta, tau: tau, lat: lat * Constants.Rad)
}


/// Computs the difference ET-UT for the given julian date in seconds
///
/// - Parameter t: the time in julian centuries
/// - Returns: the time difference ET-UT in seconds
public func getEphemerisTimeDifference(_ t: Double) -> Double {
	return getEphemeridsTimeDifferenceFromJulianDate(t*36525.0+51544.5)
}

/// Computs the difference ET-UT for the given julian date in seconds
///
/// - Parameter jd: the julian date
/// - Returns: the time difference ET-UT in seconds
public func getEphemeridsTimeDifferenceFromJulianDate(_ jd: Double) -> Double {
	let calDate = getCalendarDate(jd)
	let y = calDate.year
	switch y
	{
	case 1600...1699:
		let t = Double(y) - 1600.0
		let t2 = t*t
		let t3 = t2*t
		let yc = Double(y) - 1955.0
		let c = -0.000012932*yc*yc
		return 120.0 - 0.9808 * t
					 - 0.01532 * t2
					 + t3/7129.0 + c

	case 1700...1799:
		let t = Double(y) - 1600.0
		let t2 = t*t
		let t3 = t2*t
		let t4 = t2*t2
		let yc = Double(y) - 1955.0
		let c = -0.000012932*yc*yc
		return 8.83 + 0.1603 * t
					- 0.0059285 * t2
					+ 0.00013336 * t3
					- t4/1174000.0 + c

	case 1800...1859:
		let t = Double(y) - 1800.0
		let t2 = t*t
		let t3 = t2*t
		let t4 = t2*t2
		let t5 = t4*t
		let t6 = t5*t
		let t7 = t6*t
		let yc = Double(y) - 1955.0
		let c = -0.000012932*yc*yc
		
		return 13.72 - 0.332447*t
					 + 0.0068612*t2
					 + 0.0041116*t3
					 - 0.00037436*t4
					 + 0.0000121272*t5
					 - 0.0000001699*t6
					 + 0.000000000875*t7
					 + c
	case 1860...1899:
		let t = Double(y) - 1800.0
		let t2 = t*t
		let t3 = t2*t
		let t4 = t2*t2
		let t5 = t4*t
		let yc = Double(y) - 1955.0
		let c = -0.000012932*yc*yc
		
		return 7.62 + 0.5737*t
					- 0.251754*t2
					+ 0.01680668*t3
					- 0.0004473624*t4
					+ t5/233174.0
					+ c
	case 1900...1919:
		let t = Double(y) - 1900.0
		let t2 = t*t
		let t3 = t2*t
		let t4 = t2*t2
		let yc = Double(y) - 1955.0
		let c = -0.000012932*yc*yc
		
		return -2.79 + 1.494119*t
					 - 0.0598939*t2
					 + 0.0061966*t3
					 - 0.000197*t4
					 + c
		
	case 1920...1940:
		let t = Double(y) - 1920.0
		let t2 = t*t
		let t3 = t2*t
		
		let yc = Double(y) - 1955.0
		let c = -0.000012932*yc*yc
		
		return 21.20 + 0.84493*t
					 - 0.076100*t2
					 + 0.0020936*t3
					 + c
	case 1941...1960:
		let t = Double(y) - 1950.0
		let t2 = t*t
		let t3 = t2*t

		let yc = Double(y) - 1955.0
		let c = -0.000012932*yc*yc
		
		return 29.07 + 0.407*t - t2/233.0 + t3/2547.0 + c
	case 1961...1985:
		let t = Double(y) - 1975.0
		let t2 = t*t
		let t3 = t2*t
		
		return 45.45 + 1.067*t - t2/260.0 - t3/718.0
	case 1986...2004:
		let t = Double(y) - 2000.0
		let t2 = t*t
		let t3 = t2*t
		let t4 = t2*t2
		let t5 = t4*t
		
		return 63.86 + 0.3345*t
					 - 0.060374*t2
					 + 0.0017275*t3
					 + 0.000651814*t4
					 + 0.00002373599*t5

	case 2005...2050:
		let t = Double(y) - 2000.0
		let t2 = t*t
		
		let yc = Double(y) - 1955.0
		let c = -0.000012932*yc*yc
		
		return 62.92 + 0.32217*t + 0.005589*t2 + c
	case 2051...2150:
		let u = ((Double(y) - 1820.0)/100.0)
		let u2 = u * u
		
		let yc = Double(y) - 1955.0
		let c = -0.000012932*yc*yc
		
		return -20.0 + 32.0*u2 - 0.5628*(2150.0 - Double(y)) + c
	case 2151...2800:
		let u = ((Double(y) - 1820.0)/100.0)
		let u2 = u * u
		
		let yc = Double(y) - 1955.0
		let c = -0.000012932*yc*yc
		
		return -20.0 + 32.0*u2 + c
	default:
		return 0.0
	}
	
}


public func magToBrightness(_ m: Double) -> Double {
	return exp(-0.4 * m * log(10.0))
}

public func brightnessToMag(_ b: Double) -> Double {
	return (-2.5 * log(b) / log(10.0))
}

/// describes the value f as rational fraction
///
/// - Parameters:
///   - f: the value
///   - maxDenominator: the maximum denominator value
/// - Returns: the fraction (numerator/denominator)
public func fraction(for value: Double, maxDenominator: Int = 20) -> (numerator: Int, denominator: Int) {
	var h = [0, 1, 0]
	var k = [1, 0, 0]
	var n = 1
	var negative = false
	var f = value
	
	if maxDenominator <= 1 {
		return (numerator: Int(f), denominator: 1)
	}
	
	if f < 0.0 {
		negative = true
		f = -f
	}
	
	while f != floor(f) {
		n <<= 1
		f *= 2.0
	}
	var d = Int(f)
	
	for i in 0..<64 {
		let a = n != 0 ? d/n : 0
		if i != 0 && a == 0 {
			break
		}
		
		var x = d
		d = n
		n = x % n
		x = a
		
		if k[1] * a + k[0] >= maxDenominator {
			x = (maxDenominator - k[0]) / k[1]
			if x * 2 >= a || k[1] >= maxDenominator {
				h[2] = x * h[1] + h[0]
				h[0] = h[1]
				h[1] = h[2]
				
				k[2] = x * k[1] + k[0]
				k[0] = k[1]
				k[1] = k[2]
				break 
			} else {
				break
			}
		}
		
		h[2] = x * h[1] + h[0]
		h[0] = h[1]
		h[1] = h[2]
		
		k[2] = x * k[1] + k[0]
		k[0] = k[1]
		k[1] = k[2]
	}
	
	return (numerator: negative ? -h[1] : h[1], denominator: k[1])
}

public func rationalApproximation(of x0: Double, withPrecision eps: Double = 1.0E-1) -> (numerator: Int, denominator: Int) {
	var x = x0
	var a = x.rounded(.down)
	var (h1, k1, h, k) = (1, 0, Int(a), 1)
	
	while x - a > eps * Double(k) * Double(k) {
		x = 1.0/(x - a)
		a = x.rounded(.down)
		(h1, k1, h, k) = (h, k, h1 + Int(a) * h, k1 + Int(a) * k)
	}
	return (h, k)
}

public struct QuarterRational {
	public var numerator: Int
	public var denominator: Int
	
	public init(numerator: Int, denominator: Int) {
		self.numerator = numerator
		self.denominator = denominator
	}
	
	public static func get(for x0: Double) -> QuarterRational {
		let (n,d) = rationalApproximation(of: x0, withPrecision: 0.25)
		return QuarterRational(numerator: n, denominator: d)
	}
	
	public var text: String {
		if numerator >= denominator {
			let f = numerator / denominator
			let d = f * denominator
			let rest = numerator - d
			
			if denominator == 2 {
				if rest == 1 {
					return String(format: "%d½", f)
				} else {
					return String(format: "%d", f)
				}
			} else if denominator == 3 {
				if rest == 1 {
					return String(format: "%d⅓", f)
				} else {
					return String(format: "%d⅔", f)
				}
			} else if denominator == 4 {
				if rest == 1 {
					return String(format: "%d¼", f)
				} else {
					return String(format: "%d¾", f)
				}
			} else if denominator == 1 {
				return String(format: "%d", numerator)
			}
		} else if denominator == 2 {
			return "½"
		} else if denominator == 3 {
			if numerator == 1 {
				return "⅓"
			} else if numerator == 2 {
				return "⅔"
			}
		} else if denominator == 4 {
			if numerator == 1 {
				return "¼"
			} else if numerator == 2 {
				return "½"
			} else if numerator == 3 {
				return "¾"
			}
		} 
		return ""
	}
}

public func lock(_ lockObject: AnyObject, closure: () -> ()) {
	objc_sync_enter(lockObject)
	closure()
	objc_sync_exit(lockObject)
}


