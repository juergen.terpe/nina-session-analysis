//
//  CGRectExtensions.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 30.03.24.
//

import Foundation

public extension CGRect {
	func getCenter() -> CGPoint {
		return CGPoint(x: self.midX, y: self.midY)
	}
	
	static func from(left: CGFloat, top: CGFloat, right: CGFloat, bottom: CGFloat) -> CGRect {
		let width = abs(right - left)
		let height = abs(top - bottom)
	
		return CGRect(origin: CGPoint(x: left, y: top),
					  size: CGSize(width: width, height: height))
	}
	
	/// Creates a rectangle from center point and radius
	///
	/// - Parameters:
	///   - center: the rectangle's center
	///   - radius: the radius
	/// - Returns: the rectangle
	static func from(center: CGPoint, radius: CGFloat) -> CGRect {
		let size = 2.0 * radius
		return CGRect(origin: CGPoint(x: center.x - radius, y: center.y - radius),
					  size: CGSize(width: size, height: size))
	}
	
	/// Creates a rectangle from center point and size
	///
	/// - Parameters:
	///   - center: the center of the rectangle
	///   - size: the size of the rectangle
	/// - Returns: the rectangle
	static func from(center: CGPoint, size: CGSize) -> CGRect {
		return CGRect(origin: CGPoint(x: center.x - size.width/2, y: center.y - size.height/2),
					  size: size)
	}
	
	/// Creates a rectangle from size
	///
	/// - Parameters:
	///   - size: the size of the rectangle
	/// - Returns: the rectangle
	static func from(size: CGSize) -> CGRect {
		return CGRect(origin: .zero,  size: size)
	}
	
	static func from(points: [CGPoint]) -> CGRect {
		let minX = points.min { p1, p2 in p1.x < p2.x }!
		let maxX = points.max { p1, p2 in p1.x > p2.x }!
		let minY = points.min { p1, p2 in p1.y < p2.y }!
		let maxY = points.max { p1, p2 in p1.y > p2.y }!
		
		let width = abs(maxX.x - minX.x)
		let height = abs(maxY.y - minY.y)
		
		return CGRect(x: minX.x, y: minY.y, width: width, height: height)
	}
}
