//
//  Stack.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 16.05.24.
//

import Foundation

public struct Stack<Element> {

	private var storage: [Element] = []

	public init() { }
	
	
	public var count: Int {
		return self.storage.count
	}
	
	
	public mutating func push(_ element: Element) {
		storage.append(element)
	}

	@discardableResult
	public mutating func pop() -> Element? {
		return storage.popLast()
	}
	
}


public struct Queue<Element> {
	private var storage: ContiguousArray<Element?>
	private var head = 0
	private var tail = 0
	private var capacity: Int

	public init(capacity: Int = 64) {
		self.capacity = capacity
		self.storage = ContiguousArray(repeating: nil, count: capacity)
	}

	public init(_ elements: [Element]) {
		self.capacity = max(elements.count, 64)
		self.storage = ContiguousArray(repeating: nil, count: self.capacity)
		self.tail = elements.count
		for (index, element) in elements.enumerated() {
			self.storage[index] = element
		}
	}

	public var count: Int {
		tail - head
	}

	public var isEmpty: Bool {
		head == tail
	}

	public mutating func removeAll() {
		head = 0
		tail = 0
		storage = ContiguousArray(repeating: nil, count: capacity)
	}

	public mutating func enque(_ element: Element) {
		if tail == capacity {
			resize()
		}
		storage[tail] = element
		tail += 1
	}

	@discardableResult
	public mutating func deque() -> Element? {
		guard head < tail, let element = storage[head] else { return nil }
		storage[head] = nil
		head += 1
		return element
	}

	private mutating func resize() {
		capacity *= 2
		var newStorage = ContiguousArray<Element?>(repeating: nil, count: capacity)
		let newCount = tail - head
		for i in 0..<newCount {
			newStorage[i] = storage[head + i]
		}
		storage = newStorage
		head = 0
		tail = newCount
	}
}

/*
public struct Queue<Element> {

	private var storage: [Element] = []

	public init() { }
	
	public init(_ elements: [Element]) {
		self.storage = elements
	}
	
	public var count: Int {
		self.storage.count
	}
	
	public var isEmpty: Bool {
		self.storage.isEmpty
	}
	
	public mutating func removeAll() {
		self.storage = []
	}
	
	public mutating func enque(_ element: Element) {
		storage.append(element)
	}

	@discardableResult
	public mutating func deque() -> Element? {
		if storage.count > 0 {
			return storage.removeFirst()
		}
		return nil 
	}
	
}*/

