//
//  JulianDate.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 30.03.24.
//

import Foundation

public struct JulianDate : Comparable, Hashable, Equatable, Sendable {
	private static let EPOCH_JAN0_12H_1900 = 15019.5 // Dec 31.5 1899 = Dec 31 1899 12h UTC
	private static let EPOCH_JAN1_00H_1900 = 15020.0 // Jan  1.0 1900 = Jan  1 1900 00h UTC
	private static let EPOCH_JAN1_12H_2000 = 51544.5 // Jan  1.5 2000 = Jan  1 2000 12h UTC
	
	private var date: Double // Julian date

	public static let J2000 = 2451545.0
	
	
	public init(julianDate: Double) {
		self.date = julianDate
	}
	
	public init(century t: Double) {
		self.date = t * 36525.0 + 51544.5
	}
	
	public init(year: Int, dayOfYear doy: Double) {
		let beginOfYear = julianDateFor(year, month: 1, day: 0,
										hour: 0, min: 0, sec: 0)
		self.date = beginOfYear + doy
	}
	
	
	public init(day: Int, month: Int, year: Int, hour: Int = 0, minute: Int = 0, seconds: Double = 0.0) {
		
		self.date = julianDateFor(year, month: month, day: day, hour: hour, min: minute, sec: seconds)
	}

	/// Creates a julian date from day with fraction, month and year
	///
	/// - Parameters:
	///   - day: the day with fractions
	///   - month: the month
	///   - year: the year
	public init(day: Double, month: Int, year: Int) {
		self.date = julianDateFor(year, month: month, day: Int(day), hour: 0, min: 0, sec: 0)
		self.date += frac(day)
	}
	
	/// Creates a julian date from a date and an optional time zone offset.
	///
	/// - Parameters:
	///   - date: the date
	///   - timeZoneOffset: a timezone offset in seconds or 0 to create a julian date for greenwich
	public init(date: Date, timeZoneOffset: Int = 0) {
		
		// seconds is in UTC!!!
		let seconds = date.timeIntervalSinceReferenceDate // seconds since 01 Jan 2001 00:00:00 UTC
		let jd = julianDateFor(2001, month: 1, day: 1, hour: 0, min: 0, sec: 0.0)
		
		self.date = jd + seconds/86400.0 + Double(timeZoneOffset)/86400.0
	}
	
	public func toDouble() -> Double {
		return self.date
	}
	
	public static func now() -> JulianDate {
		
		return JulianDate(date: Date())
	}
	
	
	public static func fromDate(date: Date) -> JulianDate {
		
		// seconds is in UTC!!!
		let seconds = date.timeIntervalSinceReferenceDate // seconds since 01 Jan 2001 00:00:00 UTC
		
		var jd = julianDateFor(2001, month: 1, day: 1, hour: 0, min: 0, sec: 0.0)
		jd += seconds/86400.0 
		
		return JulianDate(julianDate: jd)
	}
	
	public static func fromLocalDate(date: Date, timeZone: TimeZone = .current) -> JulianDate {
		
		
		let offset = TimeInterval(timeZone.secondsFromGMT(for: date))
		let seconds = date.timeIntervalSinceReferenceDate // seconds since 01 Jan 2001 00:00:00 UTC
						  - offset
		
		var jd = julianDateFor(2001, month: 1, day: 1, hour: 0, min: 0, sec: 0.0)
		jd += seconds/86400.0
		
		return JulianDate(julianDate: jd)
	}
	
	
		
	public func getCalendarDate() -> (day: Int, month: Int, year: Int, hour: Int, minuts: Int, seconds: Double) {
		var b, c: Int
		
		let a = Int(jd+2400001.0)
		
		if a < 2299161 {
			// Julian Calendar
			b = 0
			c = a + 1524
		}
		else {
			// Gregorian Calendar
			b = Int((Double(a)-1867216.25)/36524.25)
			c = a +  b - (b/4) + 1525
		}
		
		let d = Int((Double(c)-122.1)/365.25)
		let e = 365*d + d/4
		let f = Int( Double(c-e)/30.6001)
		
		let day = c - e - Int(30.6001*Double(f))
		let month = f - 1 - 12*(f/14)
		let year  = d - 4715 - ((7+month)/10)
		
		let fracOfDay = jd - floor(jd)
		let hours = 24.0*fracOfDay
		
		let hms = dms(hours)
		
		return (day, month, year, hms.degree, hms.minute, hms.seconds)
	}
	
	public func getYear() -> Int {
		let calendarDate = getCalendarDate()
		return calendarDate.year
	}
		
	public func FromJan0_12h_1900() -> Double { return self.date - JulianDate.EPOCH_JAN0_12H_1900 }
	public func FromJan1_00h_1900() -> Double { return self.date - JulianDate.EPOCH_JAN1_00H_1900 }
	public func FromJan1_12h_2000() -> Double { return self.date - JulianDate.EPOCH_JAN1_12H_2000 }
	
	public var jd: Double {
		get { return self.date }
	}
	
	public var t: Double {
		get { return (self.date - 51544.5)/36525.0 }
	}
	
	public var locale: JulianDate {
		get {
			let offset = self.getGMTOffset() // in seconds
			return self.addSeconds(+offset)
		}
	}
	
	public var greenwich: JulianDate {
		get {
			let offset = self.getGMTOffset() // in seconds
			return self.addSeconds(-offset)
		}
	}
	
	public func getGMTOffset(timeZone: TimeZone = TimeZone.current) -> TimeInterval {
		return TimeInterval(timeZone.secondsFromGMT(for: self.toDate()))
	}
	
	public static func < (lhs: JulianDate, rhs: JulianDate) -> Bool {
		return lhs.jd < rhs.jd
	}
	
	public static func <= (lhs: JulianDate, rhs: JulianDate) -> Bool {
		return lhs.jd <= rhs.jd
	}
	
	public static func > (lhs: JulianDate, rhs: JulianDate) -> Bool {
		return lhs.jd > rhs.jd
	}
	
	public static func >= (lhs: JulianDate, rhs: JulianDate) -> Bool {
		return lhs.jd >= rhs.jd
	}
	
	public static func == (lhs: JulianDate, rhs: JulianDate) -> Bool {
		return fabs(lhs.jd - rhs.jd) < 1.0E-12
	}
	
	public func hash(into hasher: inout Hasher) {
		hasher.combine(self.jd)
	}
	
}

public extension JulianDate {
	
	func localeTime() -> Double {
		return frac(self.locale.jd)*24.0
	}
	
	func formatLmst(longitude: Double) -> String {
		let lmst = self.toLmst(longitude) * Constants.Deg/15.0
		let h = Int(lmst)
		let mf = frac(lmst) * 60.0
		let m = Int(mf)
		let sf = frac(mf) * 60.0
		let s = Int(sf)

		return String(format: "%02d:%02d:%02d", h, m, s)
	}
	
	func formatJulianDate() -> String {
		return String(format: "%.5f", (self.jd + 2400000.5))
	}
	
	func addSeconds(_ secs: Double) -> JulianDate {
		return JulianDate(julianDate: self.date + secs/86400.0)
	}
	
	func addMinutes(_ minutes: Double) -> JulianDate {
		return JulianDate(julianDate: self.date + minutes/1440.0)
	}
	
	func addHours(_ hours: Double) -> JulianDate {
		return JulianDate(julianDate: self.date + hours/24.0)
	}
	
	func roundToHours() -> JulianDate {
		let cd = self.getCalendarDate()
		return JulianDate(day: cd.day, month: cd.month, year: cd.year,
						  hour: cd.hour, minute: 0, seconds: 0.0)
	}
	
	func lastFullHour() -> JulianDate {
		let cd = self.getCalendarDate()
		return JulianDate(day: cd.day, month: cd.month, year: cd.year,
						  hour: cd.hour, minute: 0, seconds: 0.0)
	}

	func nextFullHour() -> JulianDate {
		let cd = self.getCalendarDate()
		if cd.minuts > 0 || cd.seconds > 0 {
			return JulianDate(day: cd.day, month: cd.month, year: cd.year,
							  hour: cd.hour + 1, minute: 0, seconds: 0.0)
		}
		return JulianDate(day: cd.day, month: cd.month, year: cd.year,
						  hour: cd.hour, minute: 0, seconds: 0.0)
	}
	
	func nextFullMinute() -> JulianDate {
		let cd = self.getCalendarDate()
		if cd.seconds > 0 {
			return JulianDate(day: cd.day, month: cd.month, year: cd.year,
							  hour: cd.hour, minute: cd.minuts + 1, seconds: 0.0)
		}
		return JulianDate(day: cd.day, month: cd.month, year: cd.year,
						  hour: cd.hour, minute: cd.minuts, seconds: 0.0)
	}
	
	func lastFullMinute(step: Int = 1) -> JulianDate {
		let cd = self.getCalendarDate()
		let minuts = cd.minuts + Int((cd.seconds + 0.5)/60.0)
		let min = (minuts / step) * step
		
		return JulianDate(day: cd.day, month: cd.month, year: cd.year,
						  hour: cd.hour, minute: min, seconds: 0.0)
	}
	
	
	func add(hours: Int, minuts: Int, seconds: Double = 0.0) -> JulianDate {
		return addHours(Double(hours) + Double(minuts)/60.0 + Double(seconds)/3600.0)
	}
	
	
	
	func addTimeZoneOffset(timeZone: TimeZone = TimeZone.current) -> JulianDate {
		let offset = timeZone.secondsFromGMT(for: self.toDate())
		return addSeconds(Double(offset))
	}
	
	func subtractTimeZoneOffset(timeZone: TimeZone = TimeZone.current) -> JulianDate {
		let offset = timeZone.secondsFromGMT(for: self.toDate())
		return addSeconds(-Double(offset))
	}
	
	func addDays(_ days: Double) -> JulianDate {
		return JulianDate(julianDate: self.date + days)
	}
	
	func addMonths(_ months: Int, timeZone: TimeZone = TimeZone.current) -> JulianDate {
		let date = self.toDate()
		var calendar = Calendar.current
		calendar.timeZone = timeZone
		var dateComponent = DateComponents()
		dateComponent.month = months
		dateComponent.timeZone = timeZone
		let newDate = calendar.date(byAdding: dateComponent, to: date)!
		return JulianDate.fromDate(date: newDate)
	}
	
	func getBeginOfDay() -> JulianDate {
		return JulianDate(julianDate: floor(self.date))
	}
	
	func highNoon(timeZone: TimeZone = TimeZone.current) -> JulianDate {
		return JulianDate(julianDate: floor(self.date) + 0.5)
	}
	
	func getEndOfDay() -> JulianDate {
		return JulianDate(julianDate: floor(self.date) + 0.9999999)
	}
	
	static func -(left: JulianDate, right: JulianDate) -> Double {
		return left.date - right.date
	}
	
	
	static func getMonth(_ jd: Double) -> Int {
		
		var b, c: Int
		let a = Int(jd+2400001.0)
		
		if a < 2299161
		{
			// Julian Calendar
			b = 0
			c = a + 1524
		}
		else
		{
			// Gregorian Calendar
			b = Int((Double(a)-1867216.25)/36524.25)
			c = a +  b - (b/4) + 1525
		}
		
		let d = Int((Double(c)-122.1)/365.25)
		let e = 365*d + d/4
		let f = Int( Double(c-e)/30.6001)
		
		return Int(f - 1 - 12*(f/14))
	}
	
	
	
	
	
	// The angle, in radians, measuring eastward from the Vernal Equinox to
	// the prime meridian.
	//
	/// Calculate Greenwich mean sideral time for the julian date.
	///
	/// - Returns: The angle, in radians, measuring eastward from the Vernal Equinox to the prime meridian.
	func toGmst() -> Double {
		return getGreenwichMeanSideralTime(self.date)
	}
	
	/// Calculate the local mean sideral time for the julian date.
	///
	/// - Parameter longitude: the longitude in radians
	/// - Returns: The local mean sideral time in radians
	func toLmst(_ longitude: Double) -> Double {
		return (toGmst() + longitude).truncatingRemainder(dividingBy: Constants.PI2)
	}
	
	func toDate() -> Date {
		let jd1970 = JulianDate(day: 1, month: 1, year: 1970)
		let timeInterval = (floor(self.jd) - jd1970.jd) * 86400.0
		return Date(timeIntervalSince1970: TimeInterval(timeInterval))
	}
	
	func toDateTime() -> Date {
		let jd1970 = JulianDate(day: 1, month: 1, year: 1970)
		let timeInterval = (self.jd - jd1970.jd) * 86400.0 
		return Date(timeIntervalSince1970: TimeInterval(timeInterval))
	}
	
	func toDateTime(_ timeZone: TimeZone) -> Date {
		let date = toDateTime()
		return date.addingTimeInterval(TimeInterval(timeZone.secondsFromGMT(for: date)))
	}
	
	static func slerp(from: JulianDate, to: JulianDate, t: Double) -> JulianDate {
		let diff = to.jd - from.jd
		return from.addDays(diff * t)
	}
}
