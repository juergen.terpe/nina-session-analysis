//
//  Matrix3x3.swift
//  Sky Calendar
//
//  Created by Jürgen Terpe on 02.01.16.
//  Copyright © 2016 Stardust. All rights reserved.
//

import Foundation
import simd

public struct Matrix3x3 {
	private var mat = double3x3()
	
	public init()
	{}
	
	public init(e1: Vector3, e2: Vector3, e3: Vector3) {
		self.mat = double3x3([e1.toDouble3(), e2.toDouble3(), e3.toDouble3()])
	}
	
	public init(r1: Vector3, r2: Vector3, r3: Vector3) {
		self.mat = double3x3(rows: [r1.toDouble3(), r2.toDouble3(), r3.toDouble3()])
	}
	
	public init(_ mat: double3x3) {
		self.mat = mat
	}

	public static func identity() -> Matrix3x3 {
		let diag = double3x3(diagonal: Double3(1,1,1))
		return Matrix3x3(diag)
	}
	
	public func toDouble3x3() -> double3x3 {
		return self.mat
	}
	
	public func inverse() -> Matrix3x3 {
		return Matrix3x3.inverse(self)
	}
	
	public func transpose() -> Matrix3x3 {
		return Matrix3x3.transpose(self)
	}
	
	public func toFloat3x3() -> float3x3 {
		let columns = self.mat.columns
		let c1 = Float3(Float(columns.0.x), Float(columns.0.y), Float(columns.0.z))
		let c2 = Float3(Float(columns.1.x), Float(columns.1.y), Float(columns.1.z))
		let c3 = Float3(Float(columns.2.x), Float(columns.2.y), Float(columns.2.z))
		
		return float3x3(c1, c2, c3)
	}
	
	public static func rotateX(_ angle: Double) -> Matrix3x3 {
		let (s,c) = sincos(angle)
		var m = Matrix3x3.identity()
		
		m.mat[1][1] = +c
		m.mat[2][1] = +s
		m.mat[1][2] = -s
		m.mat[2][2] = +c
		
		return m
	}
	
	public static func rotateY(_ angle: Double) -> Matrix3x3 {
		let (s,c) = sincos(angle)
		var m = Matrix3x3.identity()
		
		m.mat[0][0] = +c
		m.mat[2][0] = -s
		m.mat[0][2] = +s
		m.mat[2][2] = +c
		
		return m
	}
	
	public static func rotateZ(_ angle: Double) -> Matrix3x3 {
		let (s,c) = sincos(angle)
		var m = Matrix3x3.identity()
		
		m.mat[0][0] = +c
		m.mat[1][0] = +s
		m.mat[0][1] = -s
		m.mat[1][1] = +c
		
		return m
	}
	
	public static func column(_ matrix: Matrix3x3, index: VecIndex) -> Vector3 {
		return Vector3(x: matrix.mat[index.rawValue][0],
		               y: matrix.mat[index.rawValue][1],
		               z: matrix.mat[index.rawValue][2])
	}
	
	public static func row(_ matrix: Matrix3x3, index: VecIndex) -> Vector3 {
		return Vector3(x: matrix.mat[0][index.rawValue],
		               y: matrix.mat[1][index.rawValue],
		               z: matrix.mat[2][index.rawValue])
	}
	
	public static func transpose(_ matrix: Matrix3x3) -> Matrix3x3 {
		return Matrix3x3(matrix.mat.transpose)
	}
	
	public static func inverse(_ matrix: Matrix3x3) -> Matrix3x3 {
		return Matrix3x3(matrix.mat.inverse)
	}
	
	public static func *(scalar: Double, matrix: Matrix3x3) -> Matrix3x3 {
		let d33 = matrix.mat * scalar
		return Matrix3x3(d33)
	}

	public static func *(matrix: Matrix3x3, scalar: Double) -> Matrix3x3 {
		return scalar*matrix
	}

	public static func /(matrix: Matrix3x3, scalar: Double) -> Matrix3x3 {
		return matrix * (1.0/scalar)
	}

	public static func *(matrix: Matrix3x3, vector: Vector3) -> Vector3 {
		return Vector3(v: matrix.mat * vector.toDouble3())
	}

	public static func *(vector: Vector3, matrix: Matrix3x3) -> Vector3 {
		return Vector3(v: matrix.mat * vector.toDouble3())
	}

	public static prefix func -(matrix: Matrix3x3) -> Matrix3x3 {
		
		var m = Matrix3x3()
		for i in 0 ..< 3 {
			for j in 0 ..< 3 {
				m.mat[i][j] = -matrix.mat[i][j]
			}
		}
		return m
	}

	public static func +(left: Matrix3x3, right: Matrix3x3) -> Matrix3x3 {
		return Matrix3x3(left.mat + right.mat)
	}

	public static func -(left: Matrix3x3, right: Matrix3x3) -> Matrix3x3 {
		return Matrix3x3(left.mat - right.mat)
	}

	public static func *(left: Matrix3x3, right: Matrix3x3) -> Matrix3x3 {
		return Matrix3x3(left.mat * right.mat)
	}

}







