//
//  Quaternion.swift
//  AstronomicalCalculations
//
//  Created by Jürgen Terpe on 07.02.17.
//  Copyright © 2017 Stardust. All rights reserved.
//

import Foundation

import Foundation

public struct Quaternion : Equatable {
	
	public let re: Double
	public let im: Vector3
	public let isReal: Bool
	
	public init(_ re: Double, _ im: Vector3) {
		self.re = re
		self.im = im
		isReal = im == Vector3.zero()
	}
	
	public init(axis: Vector3, angle: Double) {
		assert(axis.length() == 1.0, "axis is not a unit-length vector")
		let halfAngle = angle/2.0
		self.init(cos(halfAngle), axis * sin(halfAngle))
	}
	
	public init(from a: Vector3, to b: Vector3, opposing180Axis: Vector3 = Vector3.zAxis()) {
		let aNormal = a.toUnit()
		let bNormal = b.toUnit()
		let dotProduct = dot(aNormal, bNormal)
		if dotProduct >= 1.0 {
			self = Quaternion.identity()
		}
		else if dotProduct < (-1.0 + Double.leastNormalMagnitude) {
			self = Quaternion.init(axis: opposing180Axis, angle: Double.pi)
		}
		else {
			
			// explained at: http://lolengine.net/blog/2013/09/18/beautiful-maths-quaternion-from-vectors
			let s = sqrt((1.0 + dotProduct) * 2.0)
			let xyz = cross(aNormal, bNormal) / s
			self = Quaternion.init(s * 0.5, xyz)
			//self = Quaternion.init(axis: xyz, angle: s * 0.5)
		}
	}
	
	public static func identity() -> Quaternion {
		return Quaternion(1.0, .zero())
	}
	
	public var squareLength: Double {
		return re * re + im * im
	}
	
	public var length: Double {
		return norm
	}
	
	public var norm: Double {
		return sqrt(squareLength)
	}
	
	public func unit() -> Quaternion {
		return self / length
	}
	
	public func conjugate() -> Quaternion {
		return Quaternion(re, -im)
	}
	
	public func reciprocal() -> Quaternion {
		return self.conjugate() / squareLength
	}
	
	
	/// rotates the vector by the quaternion
	///
	/// - Parameter v: the vector to be rotated
	/// - Returns: the rotated vector
	public func rotate(_ v: Vector3) -> Vector3 {
		return 2.0 * dot(self.im, v) * self.im
			   + (self.re * self.re - dot(self.im, self.im)) * v
			   + 2.0 * self.re * cross(self.im, v)
	}
	
}

public func == (lhs: Quaternion, rhs: Quaternion) -> Bool {
	return lhs.re == rhs.re && lhs.im == rhs.im
}

public func + (lhs: Quaternion, rhs: Quaternion) -> Quaternion {
	return Quaternion(lhs.re + rhs.re, lhs.im + rhs.im)
}

public func - (lhs: Quaternion, rhs: Quaternion) -> Quaternion {
	return Quaternion(lhs.re - rhs.re, lhs.im - rhs.im)
}

public func * (lhs: Double, rhs: Quaternion) -> Quaternion {
	return Quaternion(rhs.re * lhs, rhs.im * lhs)
}

public func * (lhs: Quaternion, rhs: Double) -> Quaternion {
	return rhs * lhs
}

public func / (quaternion: Quaternion, scalar: Double) -> Quaternion {
	return 1.0/scalar * quaternion
}

/// Dot product
public func dot(_ lhs: Quaternion, _ rhs: Quaternion) -> Double {
	return lhs.re * rhs.re + lhs.im * rhs.im
}

/// Dot product
public func * (_ lhs: Quaternion, _ rhs: Quaternion) -> Double {
	return dot(lhs, rhs)
}

public func multiply(_ lhs: Quaternion, _ rhs: Quaternion) -> Quaternion {
	let re = lhs.re * rhs.re - lhs.im * rhs.im
	let im = lhs.re * rhs.im + rhs.re * lhs.im + cross(lhs.im, rhs.im)
	return Quaternion(re, im)
}

infix operator ×: AdditionPrecedence
public extension Quaternion {
	/// Multiplication
	static func × (_ lhs: Quaternion, _ rhs: Quaternion) -> Quaternion {
		return multiply(lhs, rhs)
	}
}

extension Quaternion: CustomStringConvertible {
	
	public var description: String {
		return "(re: \(re), im: \(im))"
	}
	
}
