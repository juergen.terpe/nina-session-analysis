//
//  LinearRegression.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 06.04.24.
//

import Foundation

class LinearRegression {
	
	static func average(_ input: [Double]) -> Double {
		return input.reduce(0, +) / Double(input.count)
	}
	
	static func multiply(_ a: [Double], _ b: [Double]) -> [Double] {
		return zip(a,b).map(*)
	}
	
	static func regression(_ xs: [Double], _ ys: [Double]) -> (intercept: Double, slope: Double) {
		let sum1 = average(multiply(ys, xs)) - average(xs) * average(ys)
		let sum2 = average(multiply(xs, xs)) - pow(average(xs), 2)
		let slope = sum1 / sum2
		let intercept = average(ys) - slope * average(xs)
		return (intercept: intercept, slope: slope)
	}
}
