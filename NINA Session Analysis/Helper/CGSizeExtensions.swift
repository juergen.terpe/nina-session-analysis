//
//  CGSizeExtensions.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 31.03.24.
//

import Foundation

public extension CGSize {
	
	var minDimension: CGFloat {
		min(self.width, self.height)
	}
}
