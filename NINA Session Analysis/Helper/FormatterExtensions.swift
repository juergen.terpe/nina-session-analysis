//
//  FormatterExtensions.swift
//  Sky Calendar
//
//  Created by Jürgen Terpe on 02.12.18.
//  Copyright © 2018 Stardust. All rights reserved.
//

import Foundation

public enum DistanceUnit {
	case au
	case km
	case lightSpeed
	case earthRadii
	case moonDistance
}

public extension Distance {
	
	func getDistanceText(_ unit: DistanceUnit) -> String {
		
		switch unit {
			
		case .au: return String(format: "%2.2f AU", self.inAU())
		case .km:
			if Locale.current.usesMetricSystem {
				let value = self.inKm()
				if value > 1_000_000.0 {
					return String(format: NSLocalizedString("%6.2f Mio. km", comment: "Mio. km"), value/1_000_000.0)
				} else {
					return String(format: NSLocalizedString("%6.2f km", comment: "km"), value)
				}
			}
			else {
				let value = self.inKm() / Constants.Miles
				if value > 1_000_000.0 {
					return String(format: NSLocalizedString("%6.2f million mi", comment: "Mio. mi"), value/1_000_000.0)
				} else {
					return String(format: NSLocalizedString("%6.2f mi", comment: "mi"), value)
				}
			}
		case .lightSpeed:
			
			let deltaLightSecs = self.inAU() / Constants.c_light * Constants.SecPerDay
			let deltaLightMin = deltaLightSecs/60.0
			let deltaLightHours = deltaLightSecs/3600.0
			if deltaLightHours > 1.0 {
				return String(format: "%2.2f ", deltaLightHours) + NSLocalizedString("light hours", comment: "light hours")
			}
			else if deltaLightMin > 1.0 {
				return String(format: "%2.2f ", deltaLightMin) + NSLocalizedString("light min", comment: "light min")
			}
			else {
				return String(format: "%2.2f ", deltaLightSecs) + NSLocalizedString("light sec", comment: "light sec")
			}
		case .earthRadii:
			let er = self.inEarthRadii()
			return String(format: "%2.2f ", er) + NSLocalizedString("Earth radii", comment: "Earth radii")
		case .moonDistance:
			let md = self.inKm() / 384400.0
			return String(format: "%2.2f ", md) + NSLocalizedString("Moon distance", comment: "Moon distance")
		}
		
		
	}
	
	func getDistanceText() -> String {
		let distAU = self.inAU()
		if distAU < 0.01 {
			return getDistanceText(DistanceUnit.km)
		}
		return "\(getDistanceText(.au))\n\(getDistanceText(.km))\n\(getDistanceText(.lightSpeed))"
	}
	
}



public func formatVisualDiameter(_ d: Double) -> String {
	let degMinSec = dms(d)
	if degMinSec.degree > 0 {
		return String(format: "%02d° %02d' %03.1f''", degMinSec.degree, degMinSec.minute, degMinSec.seconds)
	}
	else if degMinSec.minute > 0 {
		return String(format: "%02d' %03.1f''", degMinSec.minute, degMinSec.seconds)
	}
	else if degMinSec.seconds < 10.0 {
		return String(format: "%03.2f''", degMinSec.seconds)
	}
	else {
		return String(format: "%03.1f''", degMinSec.seconds)
	}
}

public func formatElongation(_ d: Double) -> String {
	let degMinSec = dms(fabs(d))
	if degMinSec.degree > 0 {
		return String(format: "%02d° %02d' %03.1f''", degMinSec.degree, degMinSec.minute, degMinSec.seconds)
	}
	else if degMinSec.minute > 0 {
		return String(format: "%02d' %03.1f''", degMinSec.minute, degMinSec.seconds)
	}
	else {
		return String(format: "%03.1f''", degMinSec.seconds)
	}
}

public func formatMagnitude(_ mag: Float) -> String {
	return String(format: "%.2f mag", mag)
}

public func formatPhaseAngle(_ angle: Double) -> String {
	return String(format: "%.2f°", angle * Constants.Deg)
}

public func formatPositionAngle(_ angle: Double) -> String {
	return String(format: "%.2f°", angle * Constants.Deg)
}
