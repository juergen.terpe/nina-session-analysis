//
//  Keyboard.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 18.07.24.
//

import CoreGraphics

extension CGKeyCode
{
	static let kVK_Option     : CGKeyCode = 0x3A
	static let kVK_RightOption: CGKeyCode = 0x3D
	
	var isPressed: Bool {
		CGEventSource.keyState(.combinedSessionState, key: self)
	}
	
	static var optionKeyPressed: Bool {
		return Self.kVK_Option.isPressed || Self.kVK_RightOption.isPressed
	}
}
