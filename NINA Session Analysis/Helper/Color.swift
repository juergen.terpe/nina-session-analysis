//
//  Color.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 11.04.24.
//

import SwiftUI

// MARK: - RGBA

#if canImport(UIKit)
import UIKit
typealias PlatformColor = UIColor
#endif

#if canImport(AppKit)
import AppKit
typealias PlatformColor = NSColor
#endif

extension Color {

	static var controlBackgroundColor: Color {
		return Color("ControlBackgroundColor")
	}
	
	static var controlBackgroundTransparent: Color {
		return Color("ControlBackgroundColor").opacity(0.5)
	}
	
	
	static var controlBackgroundDarkColor: Color {
		return Color("ControlBackgroundDarkColor")
	}
	
	
	static var menuBackgroundColor: Color {
		return Color("MenuBackgroundColor")
	}
	
	static var controlBackgroundLightColor: Color {
		return Color("ControlBackgroundLightColor")
	}
	
	static var badColor: Color {
		return Color.red.opacity(0.6)
	}
	
		
	init(hex: String) {
		let hex = hex.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
		var int: UInt64 = 0
		Scanner(string: hex).scanHexInt64(&int)
		let a, r, g, b: UInt64
		switch hex.count {
		case 3: // RGB (12-bit)
			(a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
		case 6: // RGB (24-bit)
			(a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
		case 8: // ARGB (32-bit)
			(a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
		default:
			(a, r, g, b) = (1, 1, 1, 0)
		}

		self.init(
			.sRGB,
			red: Double(r) / 255,
			green: Double(g) / 255,
			blue:  Double(b) / 255,
			opacity: Double(a) / 255
		)
	}
	
	func toHex() -> String? {
			let uic = NSColor(self)
			guard let components = uic.cgColor.components, components.count >= 3 else {
				return nil
			}
			let r = Float(components[0])
			let g = Float(components[1])
			let b = Float(components[2])
			var a = Float(1.0)

			if components.count >= 4 {
				a = Float(components[3])
			}

			if a != Float(1.0) {
				return String(format: "%02lX%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255), lroundf(a * 255))
			} else {
				return String(format: "%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255))
			}
		}
	
}


extension Color {
	
	func interpolate(to endColor: Color, _ t: Float) -> Color {
		return self.interpolate(to: endColor, CGFloat(t))
	}
	
	func interpolate(to endColor: Color, _ t: CGFloat) -> Color {
		let a = rgba
		let b = endColor.rgba

		let red   = a.r + (b.r - a.r) * t
		let green = a.g + (b.g - a.g) * t
		let blue  = a.b + (b.b - a.b) * t
		let alpha = a.a + (b.a - a.a) * t

		return Color(red: Double(red), green: Double(green), blue: Double(blue), opacity: Double(alpha))
	}
}



extension Color {
	var rgba: (r: Double, g: Double, b: Double, a: Double) {
		var red: CGFloat = 0
		var green: CGFloat = 0
		var blue: CGFloat = 0
		var alpha: CGFloat = 0

		var platformColor = PlatformColor(self)

#if canImport(AppKit)
		platformColor = platformColor.usingColorSpace(.deviceRGB) ?? PlatformColor(self)
#endif
		platformColor.getRed(&red, green: &green, blue: &blue, alpha: &alpha)

		return (Double(red), Double(green), Double(blue), Double(alpha))
	}

	var rgb: (r: Double, g: Double, a: Double) {
		(rgba.r, rgba.g, rgba.b)
	}
}
