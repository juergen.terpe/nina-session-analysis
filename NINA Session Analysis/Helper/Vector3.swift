//
//  Vector3.swift
//  Sky Calendar
//
//  Created by Jürgen Terpe on 02.01.16.
//  Copyright © 2016 Stardust. All rights reserved.
//

import Foundation
import simd

public enum VecIndex : Int
{
	case x = 0
	case y = 1
	case z = 2
}



public struct Vector3 : CustomDebugStringConvertible, CustomReflectable, Sendable {
	
	internal var vec: Double3
	
	public init() {
		self.vec = Double3(0,0,0)
	}
	
	public init(v: Double3) {
		self.vec = v
	}
	
	public init(x: Double, y: Double, z: Double) {
		self.vec = Double3(x, y, z)
	}
	
	public init(all: Double) {
		self.vec = Double3(all, all, all)
	}
	
	public init(polar: Polar) {
		let (sinEl,cosEl) = sincos(polar.theta)
		let (sinPhi,cosPhi) = sincos(polar.phi)
	
		self.vec = Double3(polar.r * cosPhi * cosEl, polar.r * sinPhi * cosEl, polar.r * sinEl)
	}
	
	public subscript(index: VecIndex) -> Double {
		get {
			return self.vec[index.rawValue]
		}
		set(newValue) {
			self.vec[index.rawValue] = newValue
		}
	}
	
	public var debugDescription: String {
		get {
			return "x=\(self.X) y=\(self.Y) z=\(self.Z)"
		}
	}
	
	public var customMirror: Mirror {
		let children = KeyValuePairs<String, Any>(dictionaryLiteral:
			("x", self.X), ("y", self.Y), ("z", self.Z))
		
		return Mirror(Vector3.self, children: children,
					  displayStyle: Mirror.DisplayStyle.struct,
					   ancestorRepresentation: .suppressed)
	}
	
	
	public var X: Double {
		get { return self.vec.x }
		set(newValue) {
			self.vec.x = newValue
		}
	}
	
	public var Y: Double {
		get { return self.vec.y }
		set(newValue) {
			self.vec.y = newValue
		}
	}
	
	public var Z: Double {
		get { return self.vec.z }
		set(newValue) {
			self.vec.z = newValue
		}
	}	
	
	public static func zero() -> Vector3 {
		return Vector3(x: 0, y: 0, z: 0)
	}
	
	public static func xAxis() -> Vector3 {
		return Vector3(x: 1, y: 0, z: 0)
	}
	
	public static func yAxis() -> Vector3 {
		return Vector3(x: 0, y: 1, z: 0)
	}
	
	public static func zAxis() -> Vector3 {
		return Vector3(x: 0, y: 0, z: 1)
	}
	
	public func toDouble3() -> Double3 {
		return self.vec
	}
	
	public func toDouble4(_ w: Double = 1.0) -> Double4 {
		return Double4(x: self.vec.x, y: self.vec.y, z: self.vec.z, w: w)
	}
	
	public func toFloat3() -> SIMD3<Float> {
		return SIMD3<Float>(Float(self.vec.x), Float(self.vec.y), Float(self.vec.z))
	}
	
	public func scale(_ scale: Vector3) -> Vector3 {
		return Vector3(x: self.X * scale.X, y: self.Y * scale.Y, z: self.Z * scale.Z)
	}
	
	public func toUnit() -> Vector3 {
		let len = simd.length(self.vec)
		if len > 0.0 {
			return Vector3(x: self.vec.x/len, y: self.vec.y/len, z: self.vec.z/len)
		}
		return self
	}
	
	public func getPolarPhi() -> Angle {
		if self.vec.x == 0.0 && self.vec.y == 0.0 {
			return Angle.radians(0.0)
		}
		else {
			let phi = atan2(self.vec.y, self.vec.x)
			if phi < 0.0 {
				return Angle.radians(phi + 2.0*Constants.PI)
			}
			return Angle.radians(phi)
		}
	}

	public func getPolarTheta() -> Angle {
		let x = self.vec.x
		let y = self.vec.y
		let z = self.vec.z
		
		let rho = sqrt(x * x + y * y)
		
		if z == 0.0 && rho == 0.0 {
			return Angle.radians(0.0)
		}
		else {
			return Angle.radians(atan2(z, rho))
		}
	}

	public func length() -> Double {
		return simd.length(self.vec)
	}
	
	public func sqrLength() -> Double {
		return length_squared(self.vec)
	}
	
	public func toPolar() -> Polar {
		let x = self.vec.x
		let y = self.vec.y
		let z = self.vec.z
		
		let rhoSqr = x * x + y * y
		
  		let r = sqrt(rhoSqr + z * z)

		var phi: Double = 0.0
		var theta: Double = 0.0
		
		if x == 0.0 && y == 0.0 {
			phi = 0.0
		}
		else {
			phi = atan2(y, x)
		}
		
		if phi < 0.0 {
			phi += 2.0*Constants.PI
		}
		
  		let rho = sqrt(rhoSqr)
		if z == 0.0 && rho == 0.0 {
			theta = 0.0
		}
  		else {
			theta = atan2(z, rho)
		}
		
		return Polar(az: phi, elev: theta, r: r)
	}
	
	public func toPolarDegree() -> Polar
	{
		var polar = self.toPolar()
		polar.phi *= Constants.Deg
		polar.theta *= Constants.Deg
		
		return polar
	}
}

public extension Polar {
	
	/// Computes the relative position to another polar position. The resulting distance is always a unit distance
	///
	/// - Parameter to: the related position
	/// - Returns: the relative position
	func relative(to other: Polar) -> Polar {
		let dphi = other.phi - self.phi
		let dtheta = other.theta - self.theta
		return Polar(phi: dphi, theta: dtheta)
	}
}


public func == (_ lhs: Vector3, _ rhs: Vector3) -> Bool {
	return (lhs.X == rhs.X) && (lhs.Y == rhs.Y) && (lhs.Z == rhs.Z)
}

public func += (vec: inout Vector3, other: Vector3) {
	vec.vec = vec.vec + other.vec
}

public func -= (vec: inout Vector3, other: Vector3) {
	vec.vec = vec.vec - other.vec
}

public func /= (vec: inout Vector3, scalar: Double) {
	vec.vec = 1.0/scalar * vec.vec
}

public func dot(_ left: Vector3, _ right: Vector3) -> Double {
	return dot(left.vec, right.vec)
}

public func angle(_ left: Vector3, _ right: Vector3) -> Angle {
	let dotValue = dot(left, right)
	return Angle.radians(acos(dotValue/(norm(left)*norm(right))))
}

public func angle(_ left: Double3, _ right: Double3) -> Angle {
	let dotValue = dot(left, right)
	return Angle.radians(acos(dotValue/(length(left)*length(right))))
}

public func angle2PI(_ left: Vector3, _ right: Vector3) -> Angle {
	return Angle.radians(atan2( norm(cross(left, right)), dot(left, right)))
}



public func norm(_ vec: Vector3) -> Double {
	return length(vec.vec)
}

public func * (left: Vector3, right: Vector3) -> Double {
	return dot(left.vec, right.vec)
}

public func * (left: Vector3, right: Double) -> Vector3 {
	return Vector3(x: left.vec[0]*right, y: left.vec[1]*right, z: left.vec[2]*right)
}

public func * (left: Double, right: Vector3) -> Vector3 {
	return Vector3(x: right.vec[0]*left, y: right.vec[1]*left, z: right.vec[2]*left)
}

public func / (left: Vector3, right: Double) -> Vector3 {
	return Vector3(x: left.vec[0]/right, y: left.vec[1]/right, z: left.vec[2]/right)
}

public prefix func - (vec: Vector3) -> Vector3 {
	return Vector3(v: -vec.vec)
	//return Vector3(x: -vec.vec[0], y: -vec.vec[1], z: -vec.vec[2])
}

public func +(left: Vector3, right: Vector3) -> Vector3 {
	return Vector3(v: left.vec + right.vec)
	//return Vector3(x: left.vec[0]+right.vec[0], y: left.vec[1]+right.vec[1], z: left.vec[2]+right.vec[2])
}

public func -(left: Vector3, right: Vector3) -> Vector3 {
	return Vector3(v: left.vec - right.vec)
	//return Vector3(x: left.vec[0]-right.vec[0], y: left.vec[1]-right.vec[1], z: left.vec[2]-right.vec[2])
}

public func cross(_ left: Vector3, _ right: Vector3) -> Vector3 {
	return Vector3(v: cross(left.vec, right.vec))
	/*
	return Vector3(x: left.vec[1]*right.vec[2] - left.vec[2]*right.vec[1],
		y: left.vec[2]*right.vec[0] - left.vec[0]*right.vec[2],
		z: left.vec[0]*right.vec[1] - left.vec[1]*right.vec[0]) */
}


public extension Vector3  {

	func rotate(_ rotation: Quaternion) -> Vector3 {
		assert(rotation.length == 1.0, "rotation is not a unit-length quaternion")
		return self + cross(rotation.im + rotation.im, (cross(rotation.im, self) + rotation.re * self))
	}

	
	/// Rotates this vector around x axis
	///
	/// - Parameter angle: the angle in radians
	/// - Returns: the rotated vector
	func rotateX(angle: Double) -> Vector3 {
		return Matrix3x3.rotateX(angle) * self
	}

	/// Rotates this vector around y axis
	///
	/// - Parameter angle: the angle in radians
	/// - Returns: the rotated vector
	func rotateY(angle: Double) -> Vector3 {
		return Matrix3x3.rotateY(angle) * self
	}
	
	/// Rotates this vector around the z axis
	///
	/// - Parameter angle: the angle in radians
	/// - Returns: the rotated vector
	func rotateZ(angle: Double) -> Vector3 {
		return Matrix3x3.rotateZ(angle) * self
	}

	/// Returns the inverse vector in the opposite direction
	///
	/// - Returns: the vector in opposite direction
	func inverse() -> Vector3 {
		return Vector3(v: -self.vec)
	}
	
	/// Linear interpolates between two vectors
	///
	/// - Parameters:
	///   - b: the other vector
	///   - t: interpolation parameter [0,1]
	/// - Returns: the interpolated vector
	func lerp(with b: Vector3, _ t: Double) -> Vector3 {
		return self * t + b * (1.0 - t)
	}
	
	
	/// Project vector onto another vector
	///
	/// - Parameters:
	///   - p: the vector to project
	///   - n: the other vector
	/// - Returns: the projected vector
	static func project(_ p: Vector3, on n: Vector3) -> Vector3 {
		let s = p * n 
		return s * n
	}
	
	
	/// Project vector onto plane
	///
	/// - Parameters:
	///   - p: the vector to project
	///   - n: the normal vector of the plane
	/// - Returns: the projected vector on the plane
	static func project(_ p: Vector3, plane n: Vector3) -> Vector3 {
		let a = cross(p, n).toUnit()
		let b = cross(a, n).toUnit()
		
		let p1 = project(p, on: a)
		let p2 = project(p, on: b)
		
		return p1 + p2
	}
	
	
	/// Rejects vector onto plane
	///
	/// - Parameters:
	///   - p: the vector to project
	///   - n: the normal vector of the plane
	/// - Returns: the rejected vector on the plane
	static func reject(_ p: Vector3, on n: Vector3) -> Vector3 {
		return n - project(p, on: n)
	}
	

	static func from(ra: Double, dec: Double) -> Vector3 {
		let (sinEl,cosEl) = sincos(dec)
		let (sinPhi,cosPhi) = sincos(ra)
		
		return Vector3(v: Double3(cosPhi * cosEl, sinPhi * cosEl, sinEl))
	}

	static func from(azimuth: Double, altitude: Double) -> Vector3 {
		let (sinEl,cosEl) = sincos(altitude)
		let (sinPhi,cosPhi) = sincos(azimuth)
		
		return Vector3(v: Double3(cosPhi * cosEl, sinPhi * cosEl, sinEl))
	}
	
	
	static func center(vectors: [Vector3]) -> Vector3 {
		var x: Double = 0.0
		var y: Double = 0.0
		var z: Double = 0.0
		
		for v in vectors {
			let u = v.toUnit()
			x += u.X
			y += u.Y
			z += u.Z
		}
		let invLen = 1.0 / Double(vectors.count)
		
		return Vector3(x: x * invLen, y: y * invLen, z: z * invLen)
	}
	
	static func centerAngle(vectors vecs: [Vector3]) -> (center: Vector3, angle: Angle) {
		let c = center(vectors: vecs)
		var maxAngle: Angle = Angle.radians(0.0)
		
		for v in vecs {
			maxAngle = max(maxAngle, angle(c, v))
		}
		
		return (center: c, angle: maxAngle)
	}

	func getPositionAngle(to v: Vector3) -> Angle {
		
		let dra = self.getPolarPhi() - v.getPolarPhi()
		let (sindra, cosdra) = sincos(dra)
			
		let d1 = self.getPolarTheta()
		let d2 = v.getPolarTheta()
		let (sind1, cosd1) = sincos(d1)
		let (sind2, cosd2) = sincos(d2)
			
		let denom = sind1 * cosd2 - cosd1 * sind2 * cosdra
		return Angle.radians(atan2(cosd1 * sindra, denom))
	}
	
}

/*
extension Vector3 : CustomDebugStringConvertible {
	public var debugDescription: String {
		get {
			return "x=\(self.X) y=\(self.Y) z=\(self.Z)"
		}
	}
}*/


public func cubicInterpolate(_ t: Double, _ v1: Vector3, _ v2: Vector3, _ v3: Vector3) -> Vector3  {
	let x = interpolate(t, v1.X, v2.X, v3.X)
	let y = interpolate(t, v1.Y, v2.Y, v3.Y)
	let z = interpolate(t, v1.Z, v2.Z, v3.Z)
	
	return Vector3(x: x, y: y, z: z)
}

public func interpolate(_ n: Double, _ y1: Double, _ y2: Double, _ y3: Double) -> Double  {
	let a = y2 - y1
	let b = y3 - y2
	let c = b - a
	
	return y2 + (n/2.0) * (a + b + n * c)
}

public func interpolateVector(_ t: Double, _ v1: Vector3, _ v2: Vector3, _ v3: Vector3, _ v4: Vector3, _ v5: Vector3) -> Vector3  {
	let x = interpolate(t, v1.X, v2.X, v3.X, v4.X, v5.X)
	let y = interpolate(t, v1.Y, v2.Y, v3.Y, v4.Y, v5.Y)
	let z = interpolate(t, v1.Z, v2.Z, v3.Z, v4.Z, v5.Z)
	
	return Vector3(x: x, y: y, z: z)
}

public func interpolate(_ n: Double, _ y1: Double, _ y2: Double, _ y3: Double, _ y4: Double, _ y5: Double) -> Double  {
	let a = y2 - y1
	let b = y3 - y2
	let c = y4 - y3
	let d = y5 - y4
	let e = b - a
	let f = c - b
	let g = d - c
	let h = f - e
	let j = g - f
	let k = j - h

	let n2 = n*n
	
	return y3 + (n/2.0) * (b + c) + (n2/2.0)*f + n*(n2-1.0)/12.0*(h+j) + n2*(n2-1.0)/24.0*k
}


