//
//  ValueFormatter.swift
//  Sky Calendar
//
//  Created by Jürgen Terpe on 03.01.19.
//  Copyright © 2019 Stardust. All rights reserved.
//

import Foundation

#if os(iOS)
import UIKit
#elseif os(macOS)
import AppKit
#endif

final public class ValueFormatter {
	
	
	public static func formatVisualDiameter(_ d: Angle) -> String {
		let degMinSec = dms(d.toDegrees())
		if degMinSec.degree > 0 {
			return String(format: "%02d° %02d' %03.1f''", degMinSec.degree, degMinSec.minute, degMinSec.seconds)
		}
		else if degMinSec.minute > 0 {
			return String(format: "%02d' %03.1f''", degMinSec.minute, degMinSec.seconds)
		}
		else if degMinSec.seconds < 10.0 {
			return String(format: "%03.2f''", degMinSec.seconds)
		}
		else {
			return String(format: "%03.1f''", degMinSec.seconds)
		}
	}
	
	public static func formatVisualDiameterLowAccuracy(_ d: Angle) -> String {
		let degMinSec = dms(d.toDegrees())
		if degMinSec.degree > 0 {
			return String(format: "%02d° %03.1f'", degMinSec.degree, Double(degMinSec.minute) + Double(degMinSec.seconds)/60.0)
		}
		else if degMinSec.minute > 0 {
			return String(format: "%03.1f'", Double(degMinSec.minute) + Double(degMinSec.seconds)/60.0)
		}
		else if degMinSec.seconds < 10.0 {
			return String(format: "%03.1f''", degMinSec.seconds)
		}
		else {
			return String(format: "%03.1f''", degMinSec.seconds)
		}
	}
	
	/// Formats the elongation.
	///
	/// - Parameter d: the elongation in degrees
	/// - Returns: the formatted elongation
	public static func formatElongation(_ d: Angle) -> String {
		let degMinSec = dms(fabs(d.toDegrees()))
		if degMinSec.degree > 0 {
			return String(format: "%02d° %02d' %03.1f''", degMinSec.degree, degMinSec.minute, degMinSec.seconds)
		}
		else if degMinSec.minute > 0 {
			return String(format: "%02d' %03.1f''", degMinSec.minute, degMinSec.seconds)
		}
		else {
			return String(format: "%03.1f''", degMinSec.seconds)
		}
	}
	
	/// Formats the horizontal parallax
	/// - Parameter d: the parallax angle in degrees 
	public static func formatHorizontalParallax(_ d: Double) -> String {
		let degMinSec = dms(fabs(d))
		if degMinSec.degree > 0 {
			return String(format: "%02d° %02d' %03.1f''", degMinSec.degree, degMinSec.minute, degMinSec.seconds)
		}
		else if degMinSec.minute > 0 {
			return String(format: "%02d' %03.1f''", degMinSec.minute, degMinSec.seconds)
		}
		else {
			return String(format: "%03.1f''", degMinSec.seconds)
		}
	}
	
	
	public static func formatSeparationPopular(_ d: Angle) -> String {
		let dd = fabs(d.toDegrees())
		if dd > 0.125 && dd < 2.0 {
			// in moon diameters a 0.5°
			let ratio = dd / 0.5
			let relatedFraction = QuarterRational.get(for: ratio)
			return String(format: NSLocalizedString("about %@x of moon's visual diameter (%@)",
													comment: "about 1/4x moon diameter"),
						  relatedFraction.text,
						  String(format: "%2.1f°", dd))
		} else {
			let degMinSec = dms(d)
			if degMinSec.degree > 0 {
				
				return String(format: "%02d° %03.1f'", degMinSec.degree, Double(degMinSec.minute) + degMinSec.seconds/60.0)
			}
			else if degMinSec.minute > 0 {
				return String(format: "%02d' %03.1f''", degMinSec.minute, degMinSec.seconds)
			}
			else {
				return String(format: "%03.1f''", degMinSec.seconds)
			}
		}
	}
	
	public static func formatSeparationExact(_ d: Angle) -> String {
		let degMinSec = dms(d.abs())
		if degMinSec.degree > 0 {
			return String(format: "%02d° %02d' %03.1f''", degMinSec.degree, degMinSec.minute, degMinSec.seconds)
		}
		else if degMinSec.minute > 0 {
			return String(format: "%02d' %03.1f''", degMinSec.minute, degMinSec.seconds)
		}
		else {
			return String(format: "%03.1f''", degMinSec.seconds)
		}
	}
	
	public static func formatMagnitude(_ mag: Float) -> String {
		return String(format: "%.2f mag", mag)
	}
	
	public static func formatMagnitude(_ mag: Double) -> String {
		return String(format: "%.2f mag", mag)
	}
	
	
	public static func formatPhaseAngle(_ angle: Angle) -> String {
		return String(format: "%.2f°", angle.toDegrees())
	}
	
	public static func formatPhase(_ phase: Double) -> String {
		return String(format: "%.1f%%", phase * 100.0)
	}
	
	public static func formatPositionAngle(_ angle: Angle) -> String {
		return String(format: "%.2f°", angle.toDegrees())
	}

	public static func formatRectascension(_ d: Angle) -> String {
		let degMinSec = dms(d/15.0)
		return String(format: "%02dh %02d' %03.1f''", degMinSec.degree, degMinSec.minute, degMinSec.seconds)
	}
	
	public static func formatDeclination(_ d: Angle) -> String {
		let degMinSec = dms(d)
		return String(format: "%02d° %02d' %03.1f''", degMinSec.degree, degMinSec.minute, degMinSec.seconds)
	}
	
	public static func formatRectascensionLowAccuracy(_ d: Angle) -> String {
		let degMinSec = dms(d/15.0)
		let minuts = Double(degMinSec.minute) + degMinSec.seconds/60.0
		
		return String(format: "%02dh %03.1f'", degMinSec.degree, minuts)
	}
	
	public static func formatDeclinationLowAccuracy(_ d: Angle) -> String {
		let degMinSec = dms(d)
		let minuts = Double(degMinSec.minute) + degMinSec.seconds/60.0
		return String(format: "%02d° %03.1f'", degMinSec.degree, minuts)
	}
	
	public static func formatLongitude(_ d: Angle) -> String {
		let degMinSec = dms(d)
		return String(format: "%02d° %02d' %02.1f", degMinSec.degree, degMinSec.minute, degMinSec.seconds)
	}
	
	public static func formatLatitude(_ d: Angle) -> String {
		let degMinSec = dms(d)
		return String(format: "%02d° %02d' %02.1f", degMinSec.degree, degMinSec.minute, degMinSec.seconds)
	}
	
	public static func formatEclipticalLongitude(_ d: Angle) -> String {
		let degMinSec = dms(d)
		let minSec = Double(degMinSec.minute) + degMinSec.seconds/60.0
		return String(format: "%02d° %04.1f'", degMinSec.degree, minSec)
	}
	
	public static func formatEclipticalLatitude(_ d: Angle) -> String {
		let degMinSec = dms(d)
		let minSec = Double(degMinSec.minute) + degMinSec.seconds/60.0
		return String(format: "%02d° %04.1f'", degMinSec.degree, minSec)
	}
	
	public static func formatAzimuth(_ d: Angle) -> String {
		let degMinSec = dms(d)
		let minSec = Double(degMinSec.minute) + degMinSec.seconds/60.0
		return String(format: "%02d° %04.1f'", degMinSec.degree, minSec)
	}
	
	public static func format(hourAngle: Angle) -> String {
		let ha = hourAngle.toDegrees()/15.0
		let h = Int(ha)
		let mf = frac(ha) * 60.0
		let m = Int(mf)
		let sf = frac(mf) * 60.0
		let s = Int(sf)

		return String(format: "%02d:%02d:%02d", h, m, s)
	}
	
	public static func formatAzimuthWithCompassDirection(_ d: Angle, separator: String = "\n") -> String {
		let degMinSec = dms(d)
		let minSec = Double(degMinSec.minute) + degMinSec.seconds/60.0
		let compass = CompassDirection.from(azimuth: d)
		
		return String(format: "%02d° %04.1f'%@%@", degMinSec.degree, minSec, separator, compass.localized)
	}
	
	public static func formatAzimuthWithShortCompassDirection(_ d: Angle, separator: String = "\n") -> String {
		let degMinSec = dms(d)
		let minSec = Double(degMinSec.minute) + degMinSec.seconds/60.0
		let compass = CompassDirection.from(azimuth: d)
		
		return String(format: "%02d° %04.1f'%@%@", degMinSec.degree, minSec, separator, compass.localizedShort)
	}
	
	public static func formatAzimuthConstrained(_ d: Angle, fov: Double, separator: String = ", ") -> String {
		let degMinSec = dms(d)
		
		if fov > 30.0 {
			let deg = Double(degMinSec.degree) + (Double(degMinSec.minute) + degMinSec.seconds/60.0) / 60.0
			let compass = CompassDirection.from(azimuth: d)
			
			return String(format: "%04.1f°%@%@", deg, separator, compass.localizedShort)

		} else if fov > 5.0 {
			let minSec = Double(degMinSec.minute) + degMinSec.seconds/60.0
			let compass = CompassDirection.from(azimuth: d)
			
			return String(format: "%02d° %04.1f'%@%@", degMinSec.degree, minSec, separator, compass.localizedShort)
		} else {
			let compass = CompassDirection.from(azimuth: d)
			return String(format: "%02d° %02d' %04.1f' %@%@", degMinSec.degree, degMinSec.minute, degMinSec.seconds, separator, compass.localizedShort)
		}
	}
	
	public static func formatAltitudeConstrained(_ d: Angle, fov: Double) -> String {
		let degMinSec = dms(abs(d))
		
		if fov > 30.0 {
			let deg = Double(degMinSec.degree) + (Double(degMinSec.minute) + degMinSec.seconds/60.0) / 60.0
			if d >= 0.0 {
				return String(format: " %04.1f°", deg)
			} else {
				return String(format: "-%04.1f°", deg)
			}
		} else if fov > 5.0 {
			let minSec = Double(degMinSec.minute) + degMinSec.seconds/60.0
			if d >= 0.0 {
				return String(format: " %02d° %04.1f'", degMinSec.degree, minSec)
			} else {
				return String(format: "-%02d° %04.1f'", degMinSec.degree, minSec)
			}
		} else {
			if d >= 0.0 {
				return String(format: " %02d° %02d' %04.1f\"", degMinSec.degree, degMinSec.minute, degMinSec.seconds)
			} else {
				return String(format: "-%02d° %02d' %04.1f\"", degMinSec.degree, degMinSec.minute, degMinSec.seconds)
			}

		}
		
	}
	
	public static func formatAltitude(_ d: Angle) -> String {
		return formatAltitude(d.toDegrees())
	}
	
	public static func formatAltitude(_ d: Double) -> String {
		let degMinSec = dms(abs(d))
		let minSec = Double(degMinSec.minute) + degMinSec.seconds/60.0
		if d >= 0.0 {
			return String(format: "%02d° %04.1f'", degMinSec.degree, minSec)
		} else {
			return String(format: "-%02d° %04.1f'", degMinSec.degree, minSec)
		}
	}
	
	public static func formatAltitudeLowAccuracy(_ d: Angle) -> String {
		return formatAltitudeLowAccuracy(d.toDegrees())
	}
	
	public static func formatAltitudeLowAccuracy(_ d: Double) -> String {
		return String(format: "%04.1f°", d)
	}
	
	
	
	/// Formats a time span in seconds
	/// - Parameter d: the time span in seconds
	public static func formatTimeSpan(_ d: Double) -> String {
		if d < 86400.0 {
			
			let date = Foundation.Date(timeIntervalSince1970:  TimeInterval(d))
			
			let dateFormatter = DateFormatter()
			dateFormatter.dateFormat = "HH:mm:ss"
			dateFormatter.timeZone = TimeZone(identifier: "UTC")
			return dateFormatter.string(from: date)
		} else {
			let hours = Int(d / 3600.0)
			let minuts = Int((d - Double(hours) * 3600.0) / 60.0)
			let seconds = Int(d - Double(hours) * 3600.0 - Double(minuts) * 60.0)
			
			return String(format: "%02d:%02d:%02d", hours, minuts, seconds)
		}
	}

	/// Formats a time span in seconds
	/// - Parameter d: the time span in seconds
	/// - Parameter d: the time style 
	public static func formatTimeSpan(_ d: Double, timeStyle: DateComponentsFormatter.UnitsStyle = .positional) -> String {
		let time = TimeInterval(d)
		
		let formatter = DateComponentsFormatter()
		if abs(d) > 3600.0 {
			let mins = frac(abs(d)/3600.0) * 60.0
			if mins >= 1.0 {
				formatter.allowedUnits = [.hour, .minute]
			} else {
				formatter.allowedUnits = [.hour]
			}
		} else {
			let secs = frac(abs(d)/60.0) * 60.0
			if secs >= 1.0 {
				formatter.allowedUnits = [.minute, .second]
			} else {
				formatter.allowedUnits = [.minute]
			}
		}
		
		formatter.unitsStyle = timeStyle
		formatter.allowsFractionalUnits = true
		formatter.zeroFormattingBehavior = [.pad]
		return formatter.string(from: time)!
	}

	public static func formatTimeSpanDays(_ d: Double, _ approximationPhrase: Bool = false, _ timeRemainingPhrase: Bool = false,
								   _ unitsStyle: DateComponentsFormatter.UnitsStyle = .abbreviated) -> String {
		
		if fabs(d) > 3652.5 {
			let years = fabs(d)/365.25
			return String(format: "%3.1f %@", years, NSLocalizedString("years", comment: "years"))
		}
		
		let formatter = DateComponentsFormatter()
		if fabs(d) > 360.0 {
			formatter.allowedUnits = [.year, .month, .day ]
			formatter.allowsFractionalUnits = true
		} else if fabs(d) > 30.0 {
			
			let hours = Int(frac(fabs(d)) * 24.0)
			formatter.allowedUnits = hours == 0 ? [ .month, .day ] : [ .month, .day, .hour ]
			formatter.allowsFractionalUnits = true
		} else if fabs(d) > 1.0 {
			let hours = Int(frac(fabs(d)) * 24.0)
			formatter.allowedUnits = hours == 0 ? [ .day ] : [.day, .hour ]
			formatter.allowsFractionalUnits = true
		}else if fabs(d) > 0.5 {
			
			let hours = Int(frac(fabs(d)) * 24.0)
			let minuts = Int(frac(fabs(d)) * 1440.0)
			
			if minuts == 0 {
				formatter.allowedUnits = hours == 0 ? [ .day ] : [.day, .hour ]
			} else {
				formatter.allowedUnits = [.day, .hour, .minute ]
			}
		} else {
			formatter.allowedUnits = [.hour, .minute, .second ]
		}
		formatter.zeroFormattingBehavior = .dropLeading
		formatter.collapsesLargestUnit = false
		formatter.includesApproximationPhrase = approximationPhrase
		formatter.unitsStyle = unitsStyle
		let result = formatter.string(from: fabs(d)*86400.0)!

		if timeRemainingPhrase {
			if d < 0.0 {
				if let culture = Locale.preferredLanguages.first {
					if culture.hasPrefix("de") {
						return "vor " + result
					} else {
						return result + " ago"
					}
				}
			} else {
				return "in " + result
			}
		}
		return result
	}
	
}
