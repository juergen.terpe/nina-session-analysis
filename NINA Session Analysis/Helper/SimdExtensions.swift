//
//  SimdExtensions.swift
//  AstronomicalCalculations
//
//  Created by Jürgen Terpe on 13.03.18.
//  Copyright © 2018 Stardust. All rights reserved.
//

import simd

public typealias Double2 = SIMD2<Double>
public typealias Double3 = SIMD3<Double>
public typealias Double4 = SIMD4<Double>


public typealias Float2 = SIMD2<Float>
public typealias Float3 = SIMD3<Float>
public typealias Float4 = SIMD4<Float>

public extension Double3 {
	
	static let xAxis: Double3 = Double3(1, 0, 0)
	static let yAxis: Double3 = Double3(0, 1, 0)
	static let zAxis: Double3 = Double3(0, 0, 1)
}


public extension double4x4 {
	
	/// Creates a rotation around x
	///
	/// - Parameter angle: the angle in radians
	/// - Returns: the double4x4 matrix
	static func rotateX(angle: Double) -> double4x4 {
		let (s,c) = sincos(angle)
		return double4x4(rows: [Double4(1.0, 0.0, 0.0, 0.0),
								Double4(0.0,   c,  -s, 0.0),
								Double4(0.0,   s,   c, 0.0),
								Double4(0.0, 0.0, 0.0, 1.0)])
	}
	
	/// Creates a rotation around y
	///
	/// - Parameter angle: the angle in radians
	/// - Returns: the double4x4 matrix
	static func rotateY(angle: Double) -> double4x4 {
		let (s,c) = sincos(angle)
		return double4x4(rows: [Double4(  c, 0.0,  -s, 0.0),
								Double4(0.0, 1.0, 0.0, 0.0),
								Double4(  s, 0.0,   c, 0.0),
								Double4(0.0, 0.0, 0.0, 1.0)])
	}
	
	/// Creates a rotation around z
	///
	/// - Parameter angle: the angle in radians
	/// - Returns: the double4x4 matrix
	static func rotateZ(angle: Double) -> double4x4 {
		let (s,c) = sincos(angle)
		return double4x4(rows: [Double4(  c,  -s, 0.0, 0.0),
								Double4(  s,   c, 0.0, 0.0),
								Double4(0.0, 0.0, 1.0, 0.0),
								Double4(0.0, 0.0, 0.0, 1.0)])
	}
	
	
	
}

public extension simd_quatf {
	
	/// Clamps the quaternion to rotate only by a maximum angle per axis
	/// - Parameter bounds: the max. rotation angles for each axis.
	/// - Returns: the clamped quaternion
	func clamp(bounds: Float3) -> simd_quatf {
		let x = self.imag.x / self.real
		let y = self.imag.y / self.real
		let z = self.imag.z / self.real
		

		let angleX: Float = 2.0 * atan(x)
		let cx: Float = min(max(angleX, -bounds.x), bounds.x)
		let qx: Float = tan(0.5 * cx)
		 
		let angleY: Float = 2.0 * atan(y)
		let cy: Float = min(max(angleY, -bounds.y), bounds.y)
		let qy: Float = tan(0.5 * cy)
		 
		let angleZ: Float = 2.0 * atan(z)
		let cz: Float = min(max(angleZ, -bounds.z), bounds.z)
		let qz: Float = tan(0.5 * cz)
		
		return simd_quatf.init(ix: qx, iy: qy, iz: qz, r: 1.0).normalized
	}
	
	func toEuler() -> Float3 {
		let sqw = self.real * self.real
		let sqx = self.imag.x * self.imag.x
		let sqy = self.imag.y * self.imag.y
		let sqz = self.imag.z * self.imag.z
		let unit = sqx + sqy + sqz + sqw // if normalised is one, otherwise is correction factor
		let test = self.imag.x * self.imag.y + self.imag.z * self.real
		
		if test > 0.499*unit { // singularity at north pole
			let heading = 2.0 * atan2(self.imag.x, self.real)
			let attitude = Float.pi/2.0
			
			return Float3(x: attitude, y: heading, z: 0.0)
		}
		if test < -0.499*unit { // singularity at south pole
			let heading = -2.0 * atan2(self.imag.x, self.real)
			let attitude = Float.pi/2.0
			return Float3(x: attitude, y: heading, z: 0.0)
		}
		
		let heading: Float = atan2(2.0 * self.imag.y * self.real - 2.0 * self.imag.x * self.imag.z, sqx - sqy - sqz + sqw)
		let attitude: Float = asin(2.0 * test/unit)
		let bank: Float = atan2(2.0 * self.imag.x * self.real - 2.0 * self.imag.y * self.imag.z, -sqx + sqy - sqz + sqw)
		
		return Float3(x: attitude, y: heading, z: bank)
	}
	
}
