//
//  Polar.swift
//  Sky Calendar
//
//  Created by Jürgen Terpe on 02.01.16.
//  Copyright © 2016 Stardust. All rights reserved.
//

import Foundation
import simd



/// Defines polar coordinates
public struct Polar {
	public var phi: Double
	public var theta: Double
	public var r: Double
	
	/// Creates a new empty instance of polar coordinates
	public init() {
		self.phi = 0.0
		self.theta = 0.0
		self.r = 0.0
	}
	
	/// Creates a new instance with these polar coordinates
	///
	/// - Parameters:
	///   - az: The azimuth
	///   - elev: The elevation
	///   - r: The distance in AU (default 1)
	public init(az: Double, elev: Double, r: Double = 1.0) {
		self.phi = az
		self.theta = elev
		self.r = r
	}

	public init(az: Angle, elev: Angle, r: Double = 1.0) {
		self.phi = az.toRadians()
		self.theta = elev.toRadians()
		self.r = r
	}

	
	/// Creates a new instance with these polar coordinates
	///
	/// - Parameters:
	///   - phi: The angle phi (along the xy plane)
	///   - theta: The angle theta (to the z axis)
	///   - r: The distance (default 1)
	public init(phi: Double, theta: Double, r: Double = 1.0) {
		self.phi = phi
		self.theta = theta
		self.r = r
	}

	public init(phi: Angle, theta: Angle, r: Double = 1.0) {
		self.phi = phi.toRadians()
		self.theta = theta.toRadians()
		self.r = r
	}
	
	/// Creates a new instance with these polar coordinates.
	///
	/// - Parameters:
	///   - ra: The rectascension
	///   - dec: The declination
	///   - r: The distance (default 1)
	public init(ra: Double, dec: Double, r: Double = 1.0) {
		self.phi = ra
		self.theta = dec
		self.r = r
	}
	
	/// Adds to polar values. Only the angles are added, for the distance the avarage is taken
	///
	/// - Parameters:
	///   - lhs: the left hand value
	///   - rhs: the right hand value
	/// - Returns: the sum of both polar values
	public static func +(lhs: Polar, rhs: Polar) -> Polar {
		return Polar(ra: lhs.phi + rhs.phi, dec: lhs.theta + rhs.theta, r: 0.5*(lhs.r + rhs.r))
	}

	/// Subtracts to polar values. Only the angles are subtracted, the average distance of both is taken
	///
	/// - Parameters:
	///   - lhs: the left hand value
	///   - rhs: the right hand value
	/// - Returns: the difference
	public static func -(lhs: Polar, rhs: Polar) -> Polar {
		return Polar(ra: lhs.phi - rhs.phi, dec: lhs.theta - rhs.theta, r: 0.5*(lhs.r + rhs.r))
	}

	public var phiAngle: Angle {
		return Angle.radians(self.phi)
	}
	
	public var thetaAngle: Angle {
		return Angle.radians(self.theta)
	}
}

public struct RotatedPolarRectangle {
	public var leftTop: simd_double2
	public var rightTop: simd_double2
	public var rightBottom: simd_double2
	public var leftBottom: simd_double2
}


// MARK: - Some extensions for polar coordinates
public extension Polar {
	/// Converts the polar coordinates into a 3-dimensional vector
	///
	/// - Returns: The vector (3D)
	func toVector() -> Vector3 {
		return Vector3(polar: self)
	}
	
	/// Computes the difference from a straight line through all 3 positions.
	/// When the returned value is 0.0 all 3 positions are on a straight line.
	/// All 3 positions must be in the same coordinate system and refered to the same equinox!
	/// - Parameters:
	///   - pos2: The position 2
	///   - pos3: The position 3
	/// - Returns: The difference to a straight line through these 3 positions
	func getStraightLineValue(with pos2: Polar, and pos3: Polar) -> Double {
		let term1 = tan(self.theta) * sin(pos2.phi - pos3.phi)
		let term2 = tan(pos2.theta) * sin(pos3.phi - self.phi)
		let term3 = tan(pos3.theta) * sin(self.phi - pos2.phi)
		
		return term1 + term2 + term3
	}
	
	func rotatedRectangle(width: Double, height: Double, angle: Double) -> RotatedPolarRectangle {
		let (sina, cosa) = sincos(angle)
		let w = width/2.0
		let h = height/2.0
		
		
		
		// column-wise
		let matrix2 = double2x2(SIMD2<Double>(sina, -cosa), SIMD2<Double>(cosa, -sina))
		let position = simd_double2(self.phi, self.theta)
		let offset1 = matrix2 * simd_double2(w, h)
		let offset2 = matrix2 * simd_double2(-w, h)
		let offset3 = matrix2 * simd_double2(w, -h)
		
		let leftTop = position + offset1
		let rightBottom = position - offset1
		let rightTop = position + offset2
		let leftBottom = position + offset3
		
		return RotatedPolarRectangle(leftTop: leftTop, rightTop: rightTop,
									 rightBottom: rightBottom, leftBottom: leftBottom)
	}
	
	static func phiDistance(between phi1: Double, and phi2: Double) -> Angle {
		let v1 = Vector3(polar: Polar(phi: phi1, theta: 0.0))
		let v2 = Vector3(polar: Polar(phi: phi2, theta: 0.0))
		
		return angle(v1, v2)
	}
	
	static func thetaDistance(between theta1: Double, and theta2: Double) -> Angle {
		let v1 = Vector3(polar: Polar(phi: 0.0, theta: theta1))
		let v2 = Vector3(polar: Polar(phi: 0.0, theta: theta2))
		
		return angle(v1, v2)
	}
}

