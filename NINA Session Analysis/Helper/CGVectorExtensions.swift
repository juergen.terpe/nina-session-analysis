//
//  CGVectorExtensions.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 07.04.24.
//

import Foundation

extension CGVector {
	
	var magnitude: CGFloat {
		return sqrt(dx*dx + dy*dy)
	}
	
	// Dot product of two vectors
	static func dotProduct(_ lhs: CGVector, _ rhs: CGVector) -> CGFloat {
		return lhs.dx * rhs.dx + lhs.dy * rhs.dy
	}
	static func angleBetween(_ lhs: CGVector, _ rhs: CGVector) -> CGFloat {
		return acos(dotProduct(lhs, rhs) / (lhs.magnitude * rhs.magnitude))
	}
}
