//
//  CompassDirection.swift
//  AstronomicalCalculations
//
//  Created by Jürgen Terpe on 28.02.18.
//  Copyright © 2018 Stardust. All rights reserved.
//

import Foundation

public enum CompassDirection : Float, CustomStringConvertible, CaseIterable {
	
	case east = 90.0
	case eastSouthEast = 112.5
	case southEast = 135.0
	case southSouthEast = 157.5
	case south = 180.0
	case southSouthWest = 202.5
	case southWest = 225.0
	case westSouthWest = 247.5
	case west = 270.0
	case westNorthWest = 292.5
	case northWest = 315.0
	case northNorthWest = 337.5
	case north = 0.0
	case northNorthEast = 22.5
	case northEast = 45.0
	case eastNorthEast = 67.5
	
	public var azimuth: Double {
		return Double(self.rawValue)
	}
	
	public var localized: String {
		switch self {
		case .east: 			return NSLocalizedString("East", comment: "East")
		case .eastSouthEast:	return NSLocalizedString("East-southeast", comment: "East-southeast")
		case .southEast:		return NSLocalizedString("Southeast", comment: "Southeast")
		case .southSouthEast:	return NSLocalizedString("South-southeast", comment: "South-southeast")
		case .south:			return NSLocalizedString("South", comment: "South")
		case .southSouthWest:	return NSLocalizedString("South-southwest", comment: "South-southwest")
		case .southWest:		return NSLocalizedString("Southwest", comment: "Southwest")
		case .westSouthWest:	return NSLocalizedString("West-southwest", comment: "West-southwest")
		case .west:				return NSLocalizedString("West", comment: "West")
		case .westNorthWest:	return NSLocalizedString("West-northwest", comment: "West-northwest")
		case .northWest:		return NSLocalizedString("Northwest", comment: "Northwest")
		case .northNorthWest:	return NSLocalizedString("North-northwest", comment: "North-northwest")
		case .north:			return NSLocalizedString("North", comment: "North")
		case .northNorthEast:	return NSLocalizedString("North-northeast", comment: "North-northeast")
		case .northEast:		return NSLocalizedString("Northeast", comment: "Northeast")
		case .eastNorthEast:	return NSLocalizedString("East-northeast", comment: "East-northeast")
		}
	}
	
	public var localizedShort: String {
		switch self {
		case .east: 			return NSLocalizedString("E", comment: "East")
		case .eastSouthEast:	return NSLocalizedString("ESE", comment: "East-southeast")
		case .southEast:		return NSLocalizedString("SE", comment: "Southeast")
		case .southSouthEast:	return NSLocalizedString("SSE", comment: "South-southeast")
		case .south:			return NSLocalizedString("S", comment: "South")
		case .southSouthWest:	return NSLocalizedString("SSW", comment: "South-southwest")
		case .southWest:		return NSLocalizedString("SW", comment: "Southwest")
		case .westSouthWest:	return NSLocalizedString("WSW", comment: "West-southwest")
		case .west:				return NSLocalizedString("W", comment: "West")
		case .westNorthWest:	return NSLocalizedString("WNW", comment: "West-northwest")
		case .northWest:		return NSLocalizedString("NW", comment: "Northwest")
		case .northNorthWest:	return NSLocalizedString("NNW", comment: "North-northwest")
		case .north:			return NSLocalizedString("N", comment: "North")
		case .northNorthEast:	return NSLocalizedString("NNE", comment: "North-northeast")
		case .northEast:		return NSLocalizedString("NE", comment: "Northeast")
		case .eastNorthEast:	return NSLocalizedString("ENE", comment: "East-northeast")
		}
	}
	
	
	public var description: String {
		return self.localizedShort
	}
	
	
	/// Creates the compass direction
	/// - Parameter azimuth: azimuth in radians
	public static func from(azimuth: Angle) -> CompassDirection {
		let Delta:Float = 11.25
		let a = Float(modulo(azimuth.toDegrees(), 360.0))
		
		switch a {
		case (CompassDirection.east.rawValue-Delta)...(CompassDirection.east.rawValue+Delta):
			return .east
		case (CompassDirection.west.rawValue-Delta)...(CompassDirection.west.rawValue+Delta):
			return .west
		case (CompassDirection.south.rawValue-Delta)...(CompassDirection.south.rawValue+Delta):
			return .south
		case (CompassDirection.eastSouthEast.rawValue-Delta)...(CompassDirection.eastSouthEast.rawValue+Delta):
			return .eastSouthEast
		case (CompassDirection.southEast.rawValue-Delta)...(CompassDirection.southEast.rawValue+Delta):
			return .southEast
		case (CompassDirection.southSouthEast.rawValue-Delta)...(CompassDirection.southSouthEast.rawValue+Delta):
			return .southSouthEast
		case (CompassDirection.southSouthWest.rawValue-Delta)...(CompassDirection.southSouthWest.rawValue+Delta):
			return .southSouthWest
		case (CompassDirection.southWest.rawValue-Delta)...(CompassDirection.southWest.rawValue+Delta):
			return .southWest
		case (CompassDirection.westSouthWest.rawValue-Delta)...(CompassDirection.westSouthWest.rawValue+Delta):
			return .westSouthWest
		case (CompassDirection.westNorthWest.rawValue-Delta)...(CompassDirection.westNorthWest.rawValue+Delta):
			return .westNorthWest
		case (CompassDirection.northWest.rawValue-Delta)...(CompassDirection.northWest.rawValue+Delta):
			return .northWest
		case (CompassDirection.northNorthWest.rawValue-Delta)...(CompassDirection.northNorthWest.rawValue+Delta):
			return .northNorthWest
		case (CompassDirection.northNorthEast.rawValue-Delta)...(CompassDirection.northNorthEast.rawValue+Delta):
			return .northNorthEast
		case (CompassDirection.northEast.rawValue-Delta)...(CompassDirection.northEast.rawValue+Delta):
			return .northEast
		case (CompassDirection.eastNorthEast.rawValue-Delta)...(CompassDirection.eastNorthEast.rawValue+Delta):
			return .eastNorthEast
		
		default:
			return .north
		
		}
	}
}
