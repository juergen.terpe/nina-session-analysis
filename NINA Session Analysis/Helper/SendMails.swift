//
//  SendMails.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 23.09.24.
//

import AppKit

class SendEmail {
	
	public static func send(subject: String, message: String) {
		let service = NSSharingService(named: .composeEmail)!
		service.recipients = ["juergen.terpe@ideviceapps.de"]
		service.subject = subject
		
		// convert String to NSAttributedString
		let data = message.data(using: .utf8)
		let attributedString = NSAttributedString(html: data!, documentAttributes: nil)
		
		service.perform(withItems: [attributedString!])
	}
}
