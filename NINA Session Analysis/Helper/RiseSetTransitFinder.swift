//
//  RiseSetTransitFinder.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 10.04.24.
//

import Foundation

public enum RiseSetTransitTarget {
	case sun
	case moon
	case civil
	case nautical
	case astronomical
}

public class RiseSetTransitFinder {
	
	private static func getSinh0(_ obj: RiseSetTransitTarget) -> Double {
		switch obj {
			case .sun: return sin(Constants.Rad*(-50.0/60.0))  // sin(-50'); Hoehe fuer die Sonne
			case .moon: return sin(Constants.Rad*(8.0/60.0))
			case .civil: return sin(Constants.Rad * -6.0)
			case .nautical: return sin(Constants.Rad * -12.0)
			case .astronomical: return sin(Constants.Rad * -18.0)
			
		}
	}
	
	private static func getPosition(obj: RiseSetTransitTarget, _ t: Double) -> Vector3 {
		if obj == .sun || obj == .nautical || obj == .astronomical || obj == .civil {
			return Sun.getPosition(t)
		} else {
			return Moon.getPosition(t)
		}
	}
	
	
	public static func findRise(object obj: RiseSetTransitTarget, lambda: Double, phi: Double, julianDate: JulianDate) -> JulianDate? {
		let SideralDay =  0.9972696
		
		let jdMidnight = floor(julianDate.jd)
		let localSideralTimeMidnight = getGreenwichMeanSideralTime(jdMidnight) + lambda
		var t = (jdMidnight - 51544.5)/36525.0
		var posPolar = getPosition(obj: obj, t).toPolar()
		var dc = posPolar.theta
		var ra = posPolar.phi
		let sinh0 = getSinh0(obj)
		let (sinPhi, cosPhi) = sincos(phi)
		
		let (sindc, cosdc) = sincos(dc)
		var halfDayArc = (sinh0 - sindc * sinPhi ) / (cosdc*cosPhi)
		if fabs(halfDayArc) < 1.0 {
			halfDayArc = acos(halfDayArc)
		}
		else {
			return nil
		}
		
		var localSideralTime = localSideralTimeMidnight + 12.0 / (SideralDay * 12.0/Double.pi)
		var deltaTau = (localSideralTime-ra) + halfDayArc
		var deltaLocalTime = SideralDay * (12.0/Double.pi) * (modulo(deltaTau + Double.pi, Constants.PI2) - Double.pi)
		var setTime = 12.0 - deltaLocalTime
		var jdd = jdMidnight + setTime/24.0
		var delta = 1.0
		
		
		while abs(delta) > 0.5/1440.0 {
			t = (jdd - 51544.5)/36525.0
			posPolar = getPosition(obj: obj, t).toPolar()
			dc = posPolar.theta
			ra = posPolar.phi
			
			let (sindc, cosdc) = sincos(dc)
			var halfDayArc = (sinh0 - sindc * sinPhi ) / (cosdc*cosPhi)
			if fabs(halfDayArc) < 1.0 {
				halfDayArc = acos(halfDayArc)
			} else {
				break
			}
			
			localSideralTime = localSideralTimeMidnight + 12.0 / (SideralDay * 12.0/Double.pi)
			deltaTau = (localSideralTime-ra) + halfDayArc
			deltaLocalTime = SideralDay * (12.0/Double.pi) * (modulo(deltaTau + Double.pi, Constants.PI2) - Double.pi)
			setTime = 12.0 - deltaLocalTime
			let jdd2 = jdMidnight + setTime/24.0
			delta = jdd2 - jdd
			jdd = jdd2
		}
		return JulianDate(julianDate: jdd)
	}
	
	
	public static func findSet(object obj: RiseSetTransitTarget, lambda: Double, phi: Double, julianDate: JulianDate) -> JulianDate? {
		let SideralDay =  0.9972696
		
		let jdMidnight = floor(julianDate.jd)
		let localSideralTimeMidnight = getGreenwichMeanSideralTime(jdMidnight) + lambda
		var t = (jdMidnight - 51544.5)/36525.0
		var posPolar = getPosition(obj: obj, t).toPolar()
		var dc = posPolar.theta
		var ra = posPolar.phi
		let sinh0 = getSinh0(obj)
		let (sinPhi, cosPhi) = sincos(phi)
		
		let (sindc, cosdc) = sincos(dc)
		var halfDayArc = (sinh0 - sindc * sinPhi ) / (cosdc*cosPhi)
		if fabs(halfDayArc) < 1.0 {
			halfDayArc = acos(halfDayArc)
		}
		else {
			return nil
		}
		
		var localSideralTime = localSideralTimeMidnight + 12.0 / (SideralDay * 12.0/Double.pi)
		var deltaTau = (localSideralTime-ra) - halfDayArc
		var deltaLocalTime = SideralDay * (12.0/Double.pi) * (modulo(deltaTau + Double.pi, Constants.PI2) - Double.pi)
		var setTime = 12.0 - deltaLocalTime
		var jdd = jdMidnight + setTime/24.0
		var delta = 1.0
		
		while abs(delta) > 0.5/1440.0 {
			t = (jdd - 51544.5)/36525.0
			posPolar = getPosition(obj: obj, t).toPolar()
			dc = posPolar.theta
			ra = posPolar.phi
			
			let (sindc, cosdc) = sincos(dc)
			var halfDayArc = (sinh0 - sindc * sinPhi ) / (cosdc*cosPhi)
			if fabs(halfDayArc) < 1.0 {
				halfDayArc = acos(halfDayArc)
			} else {
				break
			}
			
			localSideralTime = localSideralTimeMidnight + 12.0 / (SideralDay * 12.0/Double.pi)
			deltaTau = (localSideralTime-ra) - halfDayArc
			deltaLocalTime = SideralDay * (12.0/Double.pi) * (modulo(deltaTau + Double.pi, Constants.PI2) - Double.pi)
			setTime = 12.0 - deltaLocalTime
			let jdd2 = jdMidnight + setTime/24.0
			delta = jdd2 - jdd
			
			jdd = jdd2
		}
		return JulianDate(julianDate: jdd)
	}
	
	private static func sinAltMoon(_ jd0: Double, _ hour: Double,
								   _ alt: Double,
								   _ lambda: Double, _ cosPhi: Double, _ sinPhi: Double) -> (alt: Double, tau: Double) {
		let jd = jd0 + hour/24.0
		let t  = (jd-51544.5)/36525.0
		
		let pos = getPosition(obj: .moon, t).toPolar()
		let ra = pos.phi
		let dec = pos.theta
		
		let gst = getGreenwichMeanSideralTime(jd)
		var tau = gst + lambda - ra
		if tau > Constants.PI {
			tau -= Constants.PI2
		} else if tau < -Constants.PI {
			tau += Constants.PI2
		}
		
		let (sindec, cosdec) = sincos(dec)
		return (alt: sinPhi * sindec + cosPhi * cosdec * cos(tau) - sin(alt), tau: tau)
	}
	
	private static func quadraticInterpolation(_ yMinus: Double, y0: Double, yPlus: Double) -> (xe: Double, ye: Double, roots: [Double]?)? {
		// coeffitients Parabola y=a*x^2+b*x+c
		let a  = 0.5*(yPlus+yMinus) - y0
		let b  = 0.5*(yPlus-yMinus)
		let c  = y0
		
		// Diskriminant of y=a*x^2+b*x+c
		let dis = b*b - 4.0*a*c
		
		if (dis >= 0.0) {
			// extreme value
			let xe = -b/(2.0*a)
			let ye = (a*xe+b) * xe + c
			
			var roots: [Double]? = nil
			let dx = 0.5 * sqrt(dis)/fabs(a)
			
			var root1 = xe - dx
			let root2 = xe + dx
			
			if fabs(root1) <= 1.0 {
				if root1 < -1.0 {
					root1 = root2
				}
				
				if roots == nil {
					roots = []
				}
				
				roots!.append(root1)
			}
			if fabs(root2) <= 1.0 {
				if roots == nil {
					roots = []
				}
				
				roots!.append(root2)
			}
			
			return (xe: xe, ye: ye, roots: roots)
		}
		return nil
	}
	
	public static func findMoonRise(lambda: Double, phi: Double, julianDate: JulianDate) -> JulianDate? {
		let jd0 = julianDate.jd
		let Step = 1.0
		
		let (sinPhi, cosPhi) = sincos(phi)
		let alt = Constants.Rad*(8.0/60.0)
		
		var hour = Step
		var yMinus = sinAltMoon(jd0, hour-Step, alt, lambda, cosPhi, sinPhi)
		var localRiseTime: Double?
		
		repeat {
			let y0  = sinAltMoon(jd0, hour, alt, lambda, cosPhi, sinPhi)
			let yPlus = sinAltMoon(jd0, hour+Step, alt, lambda, cosPhi, sinPhi)
			
			if let result = quadraticInterpolation(yMinus.alt, y0: y0.alt, yPlus: yPlus.alt) {
				if let roots = result.roots {
					if roots.count == 1 {
						if yMinus.alt < 0.0 {
							// from below horizon to above
							localRiseTime = hour + roots[0]*Step
						}
					} else if roots.count == 2 {
						if result.ye < 0.0 {
							localRiseTime = hour + roots[1]*Step
						}
						else {
							localRiseTime = hour + roots[0]*Step
						}
					}
				}
			}
			
			yMinus = yPlus
			hour += 2.0*Step
			if hour > 26.0 {
				return nil
			}
			
		}
		while localRiseTime == nil
		
		return JulianDate(julianDate: jd0 + localRiseTime!/24.0)
	}
	
	
	public static func findMoonSet(lambda: Double, phi: Double, julianDate: JulianDate) -> JulianDate? {
		let jd0 = julianDate.jd
		let Step = 1.0
		
		let (sinPhi, cosPhi) = sincos(phi)
		let alt = Constants.Rad*(8.0/60.0)
		
		var hour = Step
		var yMinus =  sinAltMoon(jd0, hour-Step, alt, lambda, cosPhi, sinPhi)
		var localSetTime: Double?
		
		repeat {
			let y0  =  sinAltMoon(jd0, hour, alt, lambda, cosPhi, sinPhi)
			let yPlus =  sinAltMoon(jd0, hour+Step, alt, lambda, cosPhi, sinPhi)
			
			if let result = quadraticInterpolation(yMinus.alt, y0: y0.alt, yPlus: yPlus.alt) {
				if let roots = result.roots {
					if roots.count == 1 {
						if yMinus.alt > 0.0 {
							// from above horizon to below
							localSetTime = hour + roots[0]*Step
						}
					} else if roots.count == 2 {
						if result.ye > 0.0 {
							localSetTime = hour + roots[1]*Step
						}
						else {
							localSetTime = hour + roots[0]*Step
						}
					}
				}
			}
			
			yMinus = yPlus
			hour += 2.0*Step
			
			if hour > 26.0 {
				return nil
			}
			
		}
		while localSetTime == nil
		
		return JulianDate(julianDate: jd0 + localSetTime!/24.0)
	}
}
