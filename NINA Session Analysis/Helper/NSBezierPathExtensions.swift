//
//  NSBezierPathExtensions.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 31.03.24.
//

import Foundation
import AppKit

public extension NSBezierPath {
	
	
	private static func clamp(min: CGFloat, value: CGFloat, max: CGFloat) -> CGFloat {
		if value < min {
			return min
		} else if value > max {
			return max
		}
		return value
	}
	
	static func getCrescent(at center: CGPoint, radius: CGFloat, phase: CGFloat, angle: CGFloat) -> NSBezierPath? {
		precondition(phase >= 0.0 && phase <= 1.0, "Phase must be in range [0,1]")
		
		if phase > 0.0 && phase <= 0.5 {
			let phaseAngle = CGFloat.pi * clamp(min: 0.00001, value: phase, max: 0.499999)
			let (sinA, cosA) = sincos(phaseAngle)
			
			let path = NSBezierPath()
			path.appendArc(withCenter: center, radius: radius, startAngle: -CGFloat.pi/2.0, endAngle: CGFloat.pi/2.0, clockwise: true)
			path.appendArc(withCenter: CGPoint(x: center.x - radius * sinA/cosA, y: center.y),
						radius: radius/cosA,
						startAngle: CGFloat.pi/2.0 - phaseAngle,
						endAngle: phaseAngle - CGFloat.pi/2.0,
						clockwise: false)
			path.close()
			
			
			var transform = AffineTransform.identity
			transform.translate(x: center.x, y: center.y)
			transform.rotate(byRadians: -CGFloat.pi/2.0 - angle)
			transform.translate(x: -center.x, y: -center.y)
			path.transform(using: transform)
						
			return path
			
		} else if phase > 0.5 && phase <= 1.0 {
			let phaseAngle = CGFloat.pi * clamp(min: 0.00001, value: 1.0 - phase, max: 0.499999)
			let (sinA, cosA) = sincos(phaseAngle)
			
			
			let path = NSBezierPath()
			path.appendArc(withCenter: center, radius: radius,
									startAngle: -CGFloat.pi/2.0,
									endAngle: CGFloat.pi/2.0,
									clockwise: true)
			path.appendArc(withCenter: CGPoint(x: center.x + radius * sinA/cosA, y: center.y),
						radius: radius/cosA,
						startAngle: CGFloat.pi/2.0 + phaseAngle,
						endAngle: 3.0*CGFloat.pi/2.0 - phaseAngle,
						clockwise: true)
			path.close()
			
			var transform = AffineTransform.identity
			transform.translate(x: center.x, y: center.y)
			transform.rotate(byRadians: -CGFloat.pi/2.0 - angle)
			transform.translate(x: -center.x, y: -center.y)
			path.transform(using: transform)
			
			return path
		}
		
		return nil
	}
	
	
}
