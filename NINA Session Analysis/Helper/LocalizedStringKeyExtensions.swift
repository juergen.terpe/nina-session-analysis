//
//  LocalizedStringKeyExtensions.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 02.05.24.
//

import Foundation
import SwiftUI

extension LocalizedStringKey {
	var stringKey: String? {
		Mirror(reflecting: self).children.first(where: { $0.label == "key" })?.value as? String
	}
	
	func stringValue(locale: Locale = .current) -> String? {
		guard let stringKey = self.stringKey else { return nil }
		if let language = locale.language.languageCode?.identifier {
			guard let path = Bundle.main.path(forResource: language, ofType: "lproj") else { return stringKey }
			guard let bundle = Bundle(path: path) else { return stringKey }
			let localizedString = NSLocalizedString(stringKey, bundle: bundle, comment: "")
			return localizedString
		}
		return stringKey
	}
}
