//
//  Angle.swift
//  
//
//  Created by Jürgen Terpe on 09.05.21.
//

import Foundation

public enum Angle {
	case radians(Double)
	case degrees(Double)
}


public extension Angle {
	func toDegrees() -> Double {
		switch self {
			case .radians(let a):
				return a * Constants.Deg
			case .degrees(let a):
				return a
		}
	}
	
	func toRadians() -> Double {
		switch self {
			case .radians(let a):
				return a
			case .degrees(let a):
				return a * Constants.Rad
		}
	}
	
	func toArcSeconds() -> Double {
		switch self {
			case .radians(let a):
				return a * Constants.Deg * 3600.0
			case .degrees(let a):
				return a * 3600.0
		}
	}
	
	func abs() -> Angle {
		switch self {
			case .radians(let a):
				return Angle.radians(fabs(a))
			case .degrees(let a):
				return Angle.degrees(fabs(a))
		}
	}
	
	static prefix func - (_ a: Angle) -> Angle {
		switch a {
			case .radians(let x):
				return Angle.radians(-x)
			case .degrees(let x):
				return Angle.degrees(-x)
		}
	}
		
	static func - (_ a: Angle, _ b: Angle) -> Angle {
		return Angle.radians(a.toRadians() - b.toRadians())
	}
	
	static func -= (a: inout Angle, b: Angle)  {
		a = Angle.radians(a.toRadians() - b.toRadians())
	}
	
	static func + (_ a: Angle, _ b: Angle) -> Angle {
		return Angle.radians(a.toRadians() + b.toRadians())
	}
	
	static func += (a: inout Angle, b: Angle) {
		a = Angle.radians(a.toRadians() + b.toRadians())
	}
	
	static func < (_ a: Angle, _ b: Angle) -> Bool {
		return a.toRadians() < b.toRadians()
	}
	
	static func < (_ a: Angle, _ b: Double) -> Bool {
		return a.toRadians() < b
	}
	
	static func <= (_ a: Angle, _ b: Angle) -> Bool {
		return a.toRadians() <= b.toRadians()
	}
	
	static func <= (_ a: Angle, _ b: Double) -> Bool {
		return a.toRadians() <= b
	}
	
	static func * (_ a: Angle, _ b: Double) -> Angle {
		switch a {
			case .degrees(let x):
				return Angle.degrees(x * b)
			case .radians(let x):
				return Angle.radians(x * b)
		}
	}
	
	static func * (_ b: Double, _ a: Angle) -> Angle {
		switch a {
			case .degrees(let x):
				return Angle.degrees(x * b)
			case .radians(let x):
				return Angle.radians(x * b)
		}
	}
	
	static func / (_ a: Angle, _ b: Double) -> Angle {
		switch a {
			case .degrees(let x):
				return Angle.degrees(x / b)
			case .radians(let x):
				return Angle.radians(x / b)
		}
	}
	
	
	static func > (_ a: Angle, _ b: Angle) -> Bool {
		return a.toRadians() > b.toRadians()
	}
	
	static func > (_ a: Angle, _ b: Double) -> Bool {
		return a.toRadians() > b
	}
	
	static func >= (_ a: Angle, _ b: Angle) -> Bool {
		return a.toRadians() >= b.toRadians()
	}
	
	static func >= (_ a: Angle, _ b: Double) -> Bool {
		return a.toRadians() >= b
	}
	
}

public func sincos(_ a: Angle) -> (Double, Double) {
	return sincos(a.toRadians())
}

public func sin(_ a: Angle) -> Double {
	return sin(a.toRadians())
}

public func cos(_ a: Angle) -> Double {
	return cos(a.toRadians())
}

public func tan(_ a: Angle) -> Double {
	return tan(a.toRadians())
}

public func abs(_ a: Angle) -> Angle {
	switch a {
		case .radians(let a):
			return Angle.radians(abs(a))
		case .degrees(let a):
			return Angle.degrees(abs(a))
	}
}

public func max(_ a: Angle, _ b: Angle) -> Angle {
	switch a {
		case .degrees(let x):
			return Angle.degrees(max(x, b.toDegrees()))
		case .radians(let x):
			return Angle.radians(max(x, b.toRadians()))
	}
}

public func min(_ a: Angle, _ b: Angle) -> Angle {
	switch a {
		case .degrees(let x):
			return Angle.degrees(min(x, b.toDegrees()))
		case .radians(let x):
			return Angle.radians(min(x, b.toRadians()))
	}
}




