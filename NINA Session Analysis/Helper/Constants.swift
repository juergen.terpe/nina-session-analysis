//
//  Constants.swift
//  Sky Calendar
//
//  Created by Jürgen Terpe on 02.01.16.
//  Copyright © 2016 Stardust. All rights reserved.
//

import Foundation

final public class Constants
{
	public static let PI    = Double.pi
	public static let PI2   = 2.0*PI
	public static let Rad   = PI / 180.0
	public static let Deg   = 180.0 / PI
	public static let Arcs  = 3600.0*180.0/PI
	public static let STR   = 1.0/Arcs
	
	public static let R_Earth   =   6378.137     // [km]
	public static let R_Sun     = 696000.0       // [km]
	public static let R_Moon    =   1738.0       // [km]
	public static let R_Jupiter =  71492.0		 // [km]
	public static let F_Earth   = 1.0 / 298.26   // Earth flattening (WGS '72)
	public static let EarthRatio = 0.996647186822103  // ratio of earth's polar radius to equatorial radius

	public static let MJD_J2000 = 51544.5        // MJD der Epoche J2000.0
	public static let T_J2000   =  0.0           // Epoche J2000.0
	public static let T_B1950   = -0.500002108   // Epoche B1950
	
	public static let kGauss    = 0.01720209895  // Gravitationskonstante
	public static let GM_Sun    = kGauss*kGauss  // [AE^3/d^2]
	
	public static let AU        = 149597870.700  // Astronomische Einheit [km]
	
	public static let MinPerDay   = 1440.0       // Minutes per day (solar)
	public static let SecPerDay   = 86400.0       // Seconds per day (solar)
	public static let OmegaE      = 1.00273790934 // Earth rotation per sidereal day
	
	public static let c_light   = 173.14463362   // Lichtgeschwindigkeit [AU/d]}
	public static let c_lightKm = 299792.458     // Lichtgeschwindigkeit [km/s]}
	
	
	
	
	/// Exact number of days from one new moon to the next new moon
	public static let synodicMonth = 29.530588861
	public static let SideralDayLength = 1.00273781191135448
	
	/// The base Julian day for Ernest W. Brown's numbered series of
	/// lunations = 1923 Jan 17 02:41 UT.
	public static let lunationBase   = 2423436.40347
	public static let JulianDateBase = 2400000.5
	
	public static let Miles = 1.609344
	
	public static let LightSpeed = 299792.458 		// km/s
	public static let LightYear = 9460730472580.800 // km
	public static let Parsec = 3.08567758149137E+13 // km
	
	public static let ParsecToLightYear = 3.26156
	
	
}
