//
//  NSImageExtensions.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 13.09.24.
//

import AppKit

extension NSImage {
	func toCGImage() -> CGImage? {
		if let imageData = self.tiffRepresentation {
			if let source = CGImageSourceCreateWithData(imageData as CFData, nil) {
				if let maskRef = CGImageSourceCreateImageAtIndex(source, Int(0), nil) {
					return maskRef
				}
			}
		}
		
		return self.cgImage(forProposedRect: nil, context: nil, hints: nil)
	}
}
