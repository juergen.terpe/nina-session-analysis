//
//  CoordinateConversions.swift
//  Sky Calendar
//
//  Created by Jürgen Terpe on 02.01.16.
//  Copyright © 2016 Stardust. All rights reserved.
//

import Foundation
import CoreLocation

public func getNutationMatrix(_ t: Double) -> Matrix3x3 {
	let ls = Constants.PI2*frac(0.993133 +   99.997306*t)   // Mittlere Anomalie der Sonne
	let D  = Constants.PI2*frac(0.827362 + 1236.853087*t)   // Laengendifferenz Sonne-Mond
	let F  = Constants.PI2*frac(0.259089 + 1342.227826*t)   // Mittleres Argument der Breite
	let N  = Constants.PI2*frac(0.347346 -    5.372447*t)   // Laenge des aufsteigenden Knotens
	
	let dpsi1 = -17.2*sin(N) - 1.319*sin(2.0*(F-D+N))
	let dpsi2 = -0.227*sin(2.0*(F+N)) + 0.206*sin(2.0*N)
	let dpsi3 =  0.143*sin(ls)
	let dpsi  = (dpsi1 + dpsi2 + dpsi3) / Constants.Arcs
	let deps1 = +9.203*cos(N) + 0.574*cos(2.0*(F-D+N))
	let deps2 = +0.098*cos(2.0*(F+N)) - 0.090*cos(2.0*N)
	let deps  = (deps1 + deps2) / Constants.Arcs
	
	let eps  = 0.4090928-2.2696E-4*t
	
	return  Matrix3x3.rotateX(-eps-deps) * Matrix3x3.rotateZ(-dpsi) * Matrix3x3.rotateX(+eps)
}


/// gets the obliquity of the ecliptic for a given time t in julian centuries
///
/// - Parameter t: the time in julian centuries
/// - Returns: the ecliptic's obliquity in degrees
public func getEclipticObliquity(_ t: Double) -> Double {
	if fabs(t) < 2.0 {
		return (((1.813e-3*t - 5.9e-4)*t + 46.8150)*t + 84381.448)/3600.0
	}
 	else {
		// This expansion is from Laskar, cited above.
		// Bretagnon and Simon say, in Planetary Programs and Tables, that it
		// is accurate to 0.1" over a span of 6000 years. Laskar estimates the
		// precision to be 0.01" after 1000 years and a few seconds of arc
		// after 10000 years.
		let T = t/10.0
		return (((((((((( 2.45e-10*T + 5.79e-9)*T + 2.787e-7)*T
			+ 7.12e-7)*T - 3.905e-5)*T - 2.4967e-3)*T
			- 5.138e-3)*T + 1.99925)*T - 0.0155)*T - 468.093)*T
			+ 84381.448)/3600.0
	}
}


public func getEquatorialToEclipticalMatrix(_ t: Double) -> Matrix3x3
{
	let eps = getEclipticObliquity(t)
	return Matrix3x3.rotateX(eps * Constants.Rad)
}

public func getEclipticalToEquatorialMatrix(_ t: Double) -> Matrix3x3
{
	return Matrix3x3.transpose(getEquatorialToEclipticalMatrix(t))
}

/// Converts the position into horizontal coordinates
///
/// - Parameters:
///   - dec: the declination
///   - tau: the hour angle
///   - lat: the latitude of the observer
/// - Returns: the azimuth and altitude
public func convertToHorizontal(_ dec: Double, tau: Double, lat: Double) -> (azimuth: Double, altitude: Double) {
	let pEqu = Vector3(polar: Polar(az: tau, elev: dec))
	let pHor = Matrix3x3.rotateY(Double.pi/2.0 - lat) * pEqu
	let polar = pHor.toPolar()
	
	return (azimuth: modulo(polar.phi + Double.pi, 2.0*Double.pi), altitude: polar.theta)
}


/// Converts horizontal coordinates into rotated equatorial coordinates
///
/// - Parameters:
///   - elev: the elevation in radians
///   - azimuth: the azimuth in radians
///   - location: the observer location
/// - Returns: hour angle and declination
public func convertToEquatorial(elev: Double,
								azimuth: Double,
								location: CLLocation) -> (hourAngle: Double, dec: Double) {
	let lat = location.coordinate.latitude * Constants.Rad
	let (sinAz, cosAz) = sincos(azimuth)
	let (sinEl, cosEl) = sincos(elev)
	let (sinPhi, cosPhi) = sincos(lat)
	let hourAngle = atan2(sinAz, cosAz * sinPhi + sinEl/cosEl * cosPhi)
	let dec = asin(sinPhi * sinEl - cosPhi * cosEl * cosAz)
	
	return (hourAngle: hourAngle, dec: dec)
}

/// Computes a matrix to convert into the horizontal system
///
/// - Parameters:
///   - lmst: the local mean sideral time
///   - latitude: the latitude of the observer
/// - Returns: the matrix to convert into the horizontal system
public func getHorizontalMatrix(latitude: Double) -> Matrix3x3 {
	return Matrix3x3.rotateY(Double.pi/2.0 - latitude)
}


/*******************************************************
 * longitude: in radians
 * latitude: in radians

 * returns the observer vector in kilometers
 */


/// Gets the geocentric observer position vector (unit kilometers)
///
/// - Parameters:
///   - longitude: the observer's longitude in radians
///   - latitude: the observer's latitude in radians
///   - altitude: the observer's altitude above sea level in meters (0 default)
/// - Returns: the observer's geocentric position vector
public func getGeocentricObserverPosition(longitude: Double, latitude: Double, altitude: Double = 0) -> Vector3
{
	let f = 1.0/298.254
	let eSqr = f*(2.0-f)
	let (sinPhi, cosPhi) = sincos(latitude)
	let (sinTheta, cosTheta) = sincos(longitude)
	
	let r = Constants.R_Earth/sqrt(1.0 - eSqr * sinPhi * sinPhi) + altitude/1000.0
	return Vector3(x: r * cosPhi * cosTheta,
	               y: r * cosPhi * sinTheta,
	               z: r * sinPhi * (1.0 - eSqr))
}

public func convertToTopocentric(_ jd: Double, equatorialPos: Vector3, observerPos: Vector3) -> Vector3
{
	let gmst = getGreenwichMeanSideralTime(jd)
	let observer = Matrix3x3.rotateZ(-gmst)*observerPos
	
	return equatorialPos - observer
}


