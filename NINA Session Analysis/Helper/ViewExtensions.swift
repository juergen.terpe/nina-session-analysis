//
//  ViewExtensions.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 31.03.24.
//

import SwiftUI

public extension View {
	
	func typeErased() -> AnyView {
		AnyView(self)
	}
	
	func embedIntoGroup() -> some View {
		return Group {
			self
		}
	}
	
	func offset(angle: SwiftUI.Angle, radius: CGFloat) -> some View {
		let s = CGFloat(sin(angle.radians)) * radius
		let c = CGFloat(cos(angle.radians)) * radius
		
		return self.offset(x: s, y: c)
	}
	
	func position(from point: CGPoint, angle: SwiftUI.Angle, radius: CGFloat) -> some View {
		let s = CGFloat(sin(angle.radians)) * radius
		let c = CGFloat(cos(angle.radians)) * radius
		
		return self.position(x: point.x + s, y: point.y + c)
	}
	
	
	@ViewBuilder func `popoverTipConditionally`<Content: View>(_ condition: Bool, modifier: (Self) -> Content) -> some View {
		if condition {
			modifier(self)
		}
		else {
			self
		}
	}
	
}


extension GeometryProxy {
	
	var center: CGPoint {
		return CGPoint(x: self.size.width/2.0, y: self.size.height/2.0)
	}
	
	var radius: CGFloat {
		return min(self.size.width, self.size.height)/2.0
	}
	
	var diameter: CGFloat {
		return min(self.size.width, self.size.height)
	}
	
}
