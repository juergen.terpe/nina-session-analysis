//
//  MoonPhaseImageGenerator.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 31.03.24.
//

import Foundation

public class MoonPhaseImageGenerator {
	private var baseImage: NSImage!
	private var size: CGSize
	private var center: CGPoint
	private var radius: CGFloat
	private var renderer: NSGraphicsImageRenderer
	
	
	public init(size: CGSize) {
		self.size = size
		self.center = CGPoint(x: self.size.width/2, y: self.size.height/2)
		self.radius = min(self.center.x, self.center.y)
		self.renderer = UIGraphicsImageRenderer(size: self.size)
		self.cache = [:]
	
		NotificationCenter.default.addObserver(self, selector: #selector(onLowMemory(_:)), name: UIApplication.didReceiveMemoryWarningNotification, object: nil)
	}

	deinit {
		NotificationCenter.default.removeObserver(self)
		self.mutex.lock { [weak self] in
			self?.cache.removeAll()
		}
	}
	
	
	private func getBaseImage() -> UIImage {
		if self.baseImage != nil {
			return self.baseImage
		}
		let image = UIImage(named: "MoonImage", in: Bundle.main, compatibleWith: nil)!
		self.baseImage = image.resize(size: size)
		return self.baseImage
	}
	
	public func isCached(elongation e: Int) -> Bool {
		return getCachedImage(elongation: e) != nil
	}
	
	private func getCachedImage(elongation: Int) -> UIImage? {
		var image: UIImage? = nil
		self.mutex.lock { [weak self] in
			image = self?.cache[elongation]
		}
		return image
	}
	
	public func getNearestCacheImage(elongation: Int) -> UIImage? {
		let minElong = max(0, elongation - 2)
		let maxElong = min(elongation + 2, 360)
		
		for i in minElong...maxElong {
			if let imageFromCache = getCachedImage(elongation: i) {
				return imageFromCache
			}
		}
		return nil
	}
	
	public func getPhaseImage(elongation: Int, with alpha: CGFloat = 0.66, and scale: CGFloat = 1.0) -> UIImage {
		if let imageFromCache = self.cache[elongation] {
			return imageFromCache
		}
		
		let RadiusOffset: CGFloat = 0.5 + self.radius/100.0
		
		if elongation == 0 || elongation == 360 {
			let image = renderer.image { [unowned self] context in
				context.cgContext.translateBy(x: 0, y: size.height)
				context.cgContext.scaleBy(x: scale, y: -scale)
				let baseImage = getBaseImage()
				let rect = CGRect(origin: .zero, size: self.size)
				context.cgContext.draw(baseImage.cgImage!, in: rect)
				context.cgContext.setBlendMode(CGBlendMode.darken)
				context.cgContext.setAlpha(alpha)
				context.cgContext.fillEllipse(in: rect)
			}
			self.mutex.lock { [weak self] in
				self?.cache[elongation] = image
			}
			return image
			
		} else if elongation == 180 {
			return getBaseImage()
		} else if elongation <= 90 {
			let image = renderer.image { [unowned self] context in
				context.cgContext.translateBy(x: 0, y: size.height)
				context.cgContext.scaleBy(x: scale, y: -scale)
				let baseImage = getBaseImage()
				context.cgContext.draw(baseImage.cgImage!, in: CGRect(origin: .zero, size: self.size))
				 //CGFloat(180-elongation)/CGFloat(180.0)
				let phase = CGFloat(1.0 - 0.5 * (1.0 - cos(Double(elongation) * Double.pi/180.0)))
				
				if let path = UIBezierPath.getCrescent(at: self.center, radius: self.radius + RadiusOffset,
													   phase: phase, angle: CGFloat.pi/2.0) {
					context.cgContext.setFillColor(UIColor.black.cgColor)
					context.cgContext.addPath(path.cgPath)
					context.cgContext.setAlpha(alpha)
					context.cgContext.setBlendMode(CGBlendMode.darken)
					context.cgContext.fillPath()
				}
			}
			self.mutex.lock { [weak self] in
				self?.cache[elongation] = image
			}
			return image
		} else if elongation <= 180 {
			let image = renderer.image { [unowned self] context in
				context.cgContext.translateBy(x: 0, y: size.height)
				context.cgContext.scaleBy(x: scale, y: -scale)
				let baseImage = getBaseImage()
				context.cgContext.draw(baseImage.cgImage!, in: CGRect(origin: .zero, size: self.size))
				//let phase = CGFloat(180-elongation)/CGFloat(180.0)
				let phase = CGFloat(1.0 - 0.5 * (1.0 - cos(Double(elongation) * Double.pi/180.0)))
				
				if let path = UIBezierPath.getCrescent(at: self.center, radius: self.radius + RadiusOffset,
													   phase: phase, angle: CGFloat.pi/2.0) {
					context.cgContext.setFillColor(UIColor.black.cgColor)
					context.cgContext.addPath(path.cgPath)
					context.cgContext.setAlpha(alpha)
					context.cgContext.setBlendMode(CGBlendMode.darken)
					context.cgContext.fillPath()
				}
			}
			self.mutex.lock { [weak self] in
				self?.cache[elongation] = image
			}
			return image
			
		} else if elongation <= 270 {
			let image = renderer.image { [unowned self] context in
				context.cgContext.translateBy(x: 0, y: size.height)
				context.cgContext.scaleBy(x: scale, y: -scale)
				let baseImage = getBaseImage()
				
				context.cgContext.draw(baseImage.cgImage!, in: CGRect(origin: .zero, size: self.size))
				//let phase = CGFloat(elongation - 180)/CGFloat(180.0)
				let phase = CGFloat(1.0 - 0.5 * (1.0 - cos(Double(elongation) * Double.pi/180.0)))
				if let path = UIBezierPath.getCrescent(at: self.center, radius: self.radius + RadiusOffset,
													   phase: phase, angle: -CGFloat.pi/2.0) {
					context.cgContext.setFillColor(UIColor.black.cgColor)
					context.cgContext.addPath(path.cgPath)
					context.cgContext.setAlpha(alpha)
					context.cgContext.setBlendMode(CGBlendMode.darken)
					context.cgContext.fillPath()
				}
			}
			self.mutex.lock { [weak self] in
				self?.cache[elongation] = image
			}
			return image
			
		} else if elongation < 360 {
			let image = renderer.image { [unowned self] context in
				context.cgContext.translateBy(x: 0, y: size.height)
				context.cgContext.scaleBy(x: scale, y: -scale)
				let baseImage = getBaseImage()
				context.cgContext.draw(baseImage.cgImage!, in: CGRect(origin: .zero, size: self.size))
				//let phase = CGFloat(elongation - 180)/CGFloat(180.0)
				let phase = CGFloat(1.0 - 0.5 * (1.0 - cos(Double(elongation) * Double.pi/180.0)))
				if let path = UIBezierPath.getCrescent(at: self.center, radius: self.radius + RadiusOffset,
													   phase: phase, angle: -CGFloat.pi/2.0) {
					context.cgContext.setFillColor(UIColor.black.cgColor)
					context.cgContext.addPath(path.cgPath)
					context.cgContext.setAlpha(alpha)
					context.cgContext.setBlendMode(CGBlendMode.darken)
					context.cgContext.fillPath()
				}
			}
			self.mutex.lock { [weak self] in
				self?.cache[elongation] = image
			}
			return image
		}
		
		return getBaseImage()
	}
	
}
