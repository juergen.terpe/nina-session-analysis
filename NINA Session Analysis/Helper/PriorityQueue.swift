//
//  PriorityQueue.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 01.03.25.
//


struct PriorityQueue<T> {
    private var elements: [(T, Float)] = []
    private let ascending: Bool
    private let priority: (T) -> Float

    init(ascending: Bool, priority: @escaping (T) -> Float) {
        self.ascending = ascending
        self.priority = priority
    }

    var isEmpty: Bool { elements.isEmpty }
    var count: Int { elements.count }

    mutating func push(_ value: T) {
        let priorityValue = priority(value)
        elements.append((value, priorityValue))
        siftUp(from: elements.count - 1)
    }

    mutating func pop() -> T? {
        guard !elements.isEmpty else { return nil }
        if elements.count == 1 { return elements.removeLast().0 }
        elements.swapAt(0, elements.count - 1)
        let popped = elements.removeLast().0
        siftDown(from: 0)
        return popped
    }

    private mutating func siftUp(from index: Int) {
        var child = index
        var parent = (child - 1) / 2
        while child > 0 && compare(elements[child].1, elements[parent].1) {
            elements.swapAt(child, parent)
            child = parent
            parent = (child - 1) / 2
        }
    }

    private mutating func siftDown(from index: Int) {
        var parent = index
        while true {
            let left = 2 * parent + 1
            let right = left + 1
            var candidate = parent

            if left < elements.count && compare(elements[left].1, elements[candidate].1) {
                candidate = left
            }
            if right < elements.count && compare(elements[right].1, elements[candidate].1) {
                candidate = right
            }
            if candidate == parent { return }
            elements.swapAt(parent, candidate)
            parent = candidate
        }
    }

    private func compare(_ a: Float, _ b: Float) -> Bool {
        return ascending ? a < b : a > b
    }
}