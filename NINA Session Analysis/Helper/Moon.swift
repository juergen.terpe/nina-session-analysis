//
//  Moon.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 30.03.24.
//

import Foundation

public class Moon {
	
	public static func getEclipticalPosition(_ t: Double) -> Vector3 {
		
		// Mittlere Elemente der Mondbahn
		// mittlere Laenge [Umlaeufe]
		let L_0 = frac(0.606433 + 1336.855225 * t)
		// Mittlere Anomalie des Mondes
		let l  = Constants.PI2 * frac( 0.374897 + 1325.552410 * t)
		// Mittlere Anomalie der Sonne
		let ls = Constants.PI2 * frac( 0.993133 +   99.997361 * t)

		// Laengendiff. Mond-Sonnne
		let D  = Constants.PI2 * frac (0.827361 + 1236.853086 * t)
		// Knotenabstand
		let F  = Constants.PI2 * frac (0.259086 + 1342.227825 * t)
		 
		// Stoerungen in Laenge und Breite
		let dL = +22640*sin(l) - 4586*sin(l-2*D) + 2370*sin(2*D) +  769*sin(2*l)
			 - 668*sin(ls) - 412*sin(2*F) - 212*sin(2*l-2*D) - 206*sin(l+ls-2*D)
			 + 192*sin(l+2*D) - 165*sin(ls-2*D) - 125*sin(D) - 110*sin(l+ls)
			 + 148*sin(l-ls) - 55*sin(2*F-2*D)
		let S  = F + (dL+412*sin(2*F)+541*sin(ls)) / Constants.Arcs
		let h  = F-2*D
		let N  = -526*sin(h) + 44*sin(l+h) - 31*sin(-l+h) - 23*sin(ls+h)
			 + 11*sin(-ls+h) - 25*sin(-2*l+F) + 21*sin(-l+F)
	
		// Ekliptikale Laenge und Breite
		let l_Moon = Constants.PI2 * frac( L_0 + dL/1296.0e3 ) // [rad]
		let b_Moon = (18520.0*sin(S) + N ) / Constants.Arcs   // [rad]
		  
		return Vector3(polar: Polar(phi: l_Moon, theta: b_Moon))
	}
	
	public static func getPosition(_ t: Double) -> Vector3 {
		let eps = 23.43929111 * Constants.Rad
		return Matrix3x3.rotateX(-eps) * getEclipticalPosition(t)
	}
	
	public static func getAltAz(_ jd: JulianDate, long: Double, lat: Double) -> (azimuth: Double, altitude: Double)  {
		let pos = getPosition(jd.t)
		let polar = pos.toPolar()
		let lmst = jd.toLmst(long * Constants.Rad)
		
		let tau = lmst - polar.phi
		return convertToHorizontal(polar.theta, tau: tau, lat: lat * Constants.Rad)
	}
	
	public static func getElongation(_ jd: JulianDate) -> Double {
		let pMoon = Moon.getEclipticalPosition(jd.t)
		let pSun = Sun.getEclipticalPosition(jd.t)
		
		return getAngularDistance(pSun, pos2: pMoon) * Constants.Deg
	}
	
	public static func getIlluminatedDiscAngle(_ jd: JulianDate) -> Double {
		let eps = getEclipticObliquity(jd.t)
		let pMoon = Moon.getEclipticalPosition(jd.t).rotateX(angle: eps)
		let pSun = Sun.getEclipticalPosition(jd.t).rotateX(angle: eps)
		
		let polarMoon = pMoon.toPolar()
		let polarSun = pSun.toPolar()
		
		let (sinThetaSun, cosThetaSun) = sincos(polarSun.theta)
		let (sinPhiMoon, cosPhiMoon) = sincos(polarMoon.phi)
		
		let dRa = polarSun.phi - polarMoon.phi
		let (sindRa, cosdRa) = sincos(dRa)
		
		let cosDec0sindRa = cosThetaSun * sindRa
		let sinDecsMinusCosDecsCosdRa = sinThetaSun * cosPhiMoon - cosThetaSun * sinPhiMoon * cosdRa
		
		return atan2(cosDec0sindRa, sinDecsMinusCosDecsCosdRa) * Constants.Deg
	}
}
