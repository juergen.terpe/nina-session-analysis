//
//  ArrayExtensions.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 10.04.24.
//

import Foundation

extension Array where Element: Hashable {
	func distinct() -> [Element] {
		var seen: Set<Element> = []
		return filter { seen.insert($0).inserted }
	}
	
	func orderBy<T: Comparable>(by compare: (Element) -> T) -> [Element] {
		return self.sorted(by: { compare($0) < compare($1) })
	}
}

extension Array {
	
	func groupBy<U : Hashable>(keyFunc: (Element) -> U) -> [U:[Element]] {
		var dict: [U:[Element]] = [:]
		for el in self {
			let key = keyFunc(el)
			if case nil = dict[key]?.append(el) { dict[key] = [el] }
		}
		return dict
	}
	
	func chunked(into size: Int) -> [[Element]] {
		return stride(from: 0, to: count, by: size).map {
			Array(self[$0 ..< Swift.min($0 + size, count)])
		}
	}
}


func getMedian(_ array: inout [Float]) -> Float {
	if array.count < 20 {
	   array.sort()
	   return array[array.count / 2]
	}
	
	let n = array.count
	let k = n / 2
	return quickSelect(&array, k: k)
}

private func quickSelect(_ array: inout [Float], k: Int) -> Float {
	var left = 0
	var right = array.count - 1
	
	while left < right {
		let pivotIndex = partition(&array, left: left, right: right)
		if pivotIndex == k {
			return array[k]
		} else if pivotIndex < k {
			left = pivotIndex + 1
		} else {
			right = pivotIndex - 1
		}
	}
	return array[left]
}

private func partition(_ array: inout [Float], left: Int, right: Int) -> Int {
	let pivotValue = array[right]
	var storeIndex = left
	for i in left..<right {
		if array[i] < pivotValue {
			array.swapAt(i, storeIndex)
			storeIndex += 1
		}
	}
	array.swapAt(storeIndex, right)
	return storeIndex
}


func getMedian(_ array: inout [UInt16]) -> UInt16 {
	let n = array.count
	let k = n / 2
	return quickSelect(&array, k: k)
}


private func quickSelect(_ array: inout [UInt16], k: Int) -> UInt16 {
	var left = 0
	var right = array.count - 1
	
	while left < right {
		let pivotIndex = partition(&array, left: left, right: right)
		if pivotIndex == k {
			return array[k]
		} else if pivotIndex < k {
			left = pivotIndex + 1
		} else {
			right = pivotIndex - 1
		}
	}
	return array[left]
}

private func partition(_ array: inout [UInt16], left: Int, right: Int) -> Int {
	let pivotValue = array[right]
	var storeIndex = left
	for i in left..<right {
		if array[i] < pivotValue {
			array.swapAt(i, storeIndex)
			storeIndex += 1
		}
	}
	array.swapAt(storeIndex, right)
	return storeIndex
}
