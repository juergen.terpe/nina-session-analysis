//
//  CGImageExtensions.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 08.07.24.
//
import Foundation
import CoreGraphics
import CoreImage
import CoreImage.CIFilterBuiltins
import ImageIO
import UniformTypeIdentifiers
import Accelerate

public enum Channel: Int, Comparable {
	public static func < (lhs: Channel, rhs: Channel) -> Bool {
		return lhs.rawValue < rhs.rawValue
	}
	
	case l = 0
	case r = 1
	case g = 2
	case b = 3
}


public extension CGImage {
	
	func isColorImage() -> Bool {
		guard let colorSpace = self.colorSpace else {
			return false
		}
		
		// Überprüfe die Anzahl der Farbkomponenten im Farbraum
		let colorSpaceModel = colorSpace.model
		switch colorSpaceModel {
		case .monochrome:
			return false
		case .rgb, .cmyk, .lab:
			return true
		default:
			return false
		}
	}
	
	func toGrayscale() -> CGImage? {
		//Auto Adjustment to Input Image
		let inputImage = CIImage(cgImage: self)
		guard let currentFilter = CIFilter(name: "CIPhotoEffectTonal") else { return nil }
		currentFilter.setValue(inputImage, forKey: kCIInputImageKey)
		
		let context = CIContext(options: nil)
		if let output = currentFilter.outputImage {
			return context.createCGImage(output, from: output.extent)
		}
		    
		return nil 
	}
	
	func changeContrast(value: CGFloat = 1.0) -> CGImage {
		let inputImage = CIImage(cgImage: self)
		let parameters = [
			"inputContrast": NSNumber(value: value)
		]
		let outputImage = inputImage.applyingFilter("CIColorControls", parameters: parameters)

		let context = CIContext(options: nil)
		return context.createCGImage(outputImage, from: outputImage.extent)!
	}
	
	
	func applyGaussianBlur(radius: CGFloat) -> CGImage? {
		let inputImage = CIImage(cgImage: self)
		let filter = CIFilter.gaussianBlur()
		filter.inputImage = inputImage
		filter.radius = 2.0
		
		guard let outputImage = filter.outputImage else { return nil }
		let context = CIContext()
		return context.createCGImage(outputImage, from: outputImage.extent)
	}
	
	
	func resize(by factor: CGFloat) -> CGImage? {
		let newWidth = Int(CGFloat(self.width) * factor)
		let newHeight = Int(CGFloat(self.height) * factor)
		
		let colorSpace = CGColorSpaceCreateDeviceGray()
		let context = CGContext(data: nil, width: newWidth, height: newHeight, bitsPerComponent: self.bitsPerComponent, bytesPerRow: 0, space: colorSpace,
								bitmapInfo: CGImageAlphaInfo.none.rawValue)
		
		context?.interpolationQuality = .high
		context?.draw(self, in: CGRect(x: 0, y: 0, width: CGFloat(newWidth), height: CGFloat(newHeight)))
		
		return context?.makeImage()
	}
	
	func extractChannelValues() -> [Channel: [UInt16]]? {
		let extractor = ChannelExtractor()
		return extractor.extractChannels(cgImage: self)
	}
	
	func extractPixels() -> [UInt16]? {
		let width = self.width
		let height = self.height
		let bitsPerComponent = self.bitsPerComponent
		let bytesPerPixel = self.bitsPerPixel / 8
		let pixelCount = width * height

		guard let data = self.dataProvider?.data else { return nil }
		let bytes = CFDataGetBytePtr(data)
		guard let rawData = UnsafeRawPointer(bytes)  else { return nil }

		var pixels = [UInt16](repeating: 0, count: pixelCount)

		if bitsPerComponent == 8 {
			let pixelBuffer = rawData.bindMemory(to: UInt8.self, capacity: pixelCount * bytesPerPixel)
			for y in 0..<height {
				for x in 0..<width {
					let index = y * width + x
					pixels[index] = UInt16(Float(UInt16.max) * (Float(pixelBuffer[index]) / 255.0))
				}
			}
		} else if bitsPerComponent == 16 {
			let pixelBuffer = rawData.bindMemory(to: UInt16.self, capacity: pixelCount * bytesPerPixel / 2)
			for y in 0..<height {
				for x in 0..<width {
					let index = y * width + x
					pixels[index] = pixelBuffer[index].bigEndian
				}
			}
		} else if bitsPerComponent == 32 {
			let pixelBuffer = rawData.bindMemory(to: UInt32.self, capacity: pixelCount * bytesPerPixel / 4)
			for y in 0..<height {
				for x in 0..<width {
					let index = y * width + x
					pixels[index] = UInt16(Float(UInt16.max) * (Float(pixelBuffer[index]) / 4294967295.0))
				}
			}
		} else {
			return nil
		}

		return pixels
	}
	
	func extractFloatPixels() -> [Float]? {
		let image = self 
		
		let width = image.width
		let height = image.height
		let bytesPerPixel = image.bitsPerPixel / 8
		let bitsPerComponent = image.bitsPerComponent
		let bitmapInfo = image.bitmapInfo

		guard let colorSpace = image.colorSpace else { return nil }
		let bytesPerRow = bytesPerPixel * width

		// Erstelle einen Kontext, um die Pixeldaten zu extrahieren
		guard let context = CGContext(data: nil,
									  width: width,
									  height: height,
									  bitsPerComponent: bitsPerComponent,
									  bytesPerRow: bytesPerRow,
									  space: colorSpace,
									  bitmapInfo: bitmapInfo.rawValue) else {
			return nil
		}

		context.draw(image, in: CGRect(x: 0, y: 0, width: width, height: height))

		// Extrahiere die Pixeldaten
		guard let data = context.data else { return nil }

		// Konvertiere Pixeldaten zu float-Array
		let pixelCount = width * height
		var floatPixels = [Float](repeating: 0.0, count: pixelCount * bytesPerPixel / (bitsPerComponent / 8))

		if bitsPerComponent == 8 {
			let pixelBuffer = data.bindMemory(to: UInt8.self, capacity: pixelCount * bytesPerPixel)
			for i in 0..<floatPixels.count {
				floatPixels[i] = Float(pixelBuffer[i]) / 255.0
			}
		} else if bitsPerComponent == 16 {
			let pixelBuffer = data.bindMemory(to: UInt16.self, capacity: pixelCount * bytesPerPixel / 2)
			for i in 0..<floatPixels.count {
				floatPixels[i] = Float(pixelBuffer[i]) / 65535.0
			}
		} else if bitsPerComponent == 32 {
			let pixelBuffer = data.bindMemory(to: UInt32.self, capacity: pixelCount * bytesPerPixel / 4)
			for i in 0..<floatPixels.count {
				floatPixels[i] = Float(pixelBuffer[i]) / 4294967295.0
			}
		} else {
			return nil
		}

		return floatPixels
	}
	
	

	func extractLuminanceFromCGImage() -> [[Float]]? {
		// Bildbreite und -höhe abrufen
		let width = self.width
		let height = self.height
		
		// Pixeldaten abrufen
		guard let dataProvider = self.dataProvider,
			  let pixelData = dataProvider.data as Data? else {
			return nil
		}
		
		let bitsPerComponent = self.bitsPerComponent
		let bytesPerPixel = self.bitsPerPixel / 8
		let isGrayscale = self.colorSpace?.model == .monochrome

		var luminanceArray = [[Float]](repeating: [Float](repeating: 0.0, count: width), count: height)
		
		// Parallele Verarbeitung über DispatchQueue
		DispatchQueue.concurrentPerform(iterations: height) { y in
			for x in 0..<width {
				let pixelIndex = y * self.bytesPerRow + x * bytesPerPixel
				
				if isGrayscale {
					// Graustufenbild: Direkter Zugriff auf den Graustufenwert
					let pixelValue = pixelData[pixelIndex]
					luminanceArray[y][x] = Float(pixelValue) / 65535.0
				} else if bitsPerComponent == 16 {
					// Farbbild mit 16 Bit pro Kanal
					let r = Float(pixelData[pixelIndex]) / 65535.0
					let g = Float(pixelData[pixelIndex + 2]) / 65535.0
					let b = Float(pixelData[pixelIndex + 4]) / 65535.0
					luminanceArray[y][x] = 0.299 * r + 0.587 * g + 0.114 * b
				} else {
					// Farbbild mit 8 Bit pro Kanal
					let r = Float(pixelData[pixelIndex]) / 255.0
					let g = Float(pixelData[pixelIndex + 1]) / 255.0
					let b = Float(pixelData[pixelIndex + 2]) / 255.0
					luminanceArray[y][x] = 0.299 * r + 0.587 * g + 0.114 * b
				}
			}
		}
		
		return luminanceArray
	}
	
	
	static func createGray(from pixels: [UInt16], width: Int, height: Int) -> CGImage? {
		let bytesPerRow = width * MemoryLayout<UInt16>.size
		
		let bitsPerComponent = MemoryLayout<UInt16>.size * 8
		let bitsPerPixel = bitsPerComponent
		let colorSpace = CGColorSpaceCreateDeviceGray()
		let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.none.rawValue)
		
		let converted = pixels.map { $0.bigEndian }
		
		let cfData = CFDataCreate(nil, converted, converted.count * MemoryLayout<UInt16>.size)!
		let dataProvider = CGDataProvider(data: cfData)!
		
		return CGImage(width: width,
					   height: height,
					   bitsPerComponent: bitsPerComponent,
					   bitsPerPixel: bitsPerPixel,
					   bytesPerRow: bytesPerRow,
					   space: colorSpace,
					   bitmapInfo: bitmapInfo,
					   provider: dataProvider,
					   decode: nil,
					   shouldInterpolate: false,
					   intent: .defaultIntent)
		
	}
	
	func writeAsTIFF(to url: URL) {
		guard let destination = CGImageDestinationCreateWithURL(url as CFURL, UTType.tiff.identifier as CFString, 1, nil) else {
			print("Error: Could not create image destination")
			return
		}
		
		// Ensure that the image is saved with 16 bits per component
		let options: [CFString: Any] = [
			kCGImagePropertyTIFFCompression: NSNumber(value: 5), // LZW compression
			kCGImagePropertyDepth: 16
		]
		
		CGImageDestinationAddImage(destination, self, options as CFDictionary)
		
		if CGImageDestinationFinalize(destination) {
			print("Image successfully saved to \(url)")
		} else {
			print("Error: Could not finalize the image destination")
		}
	}
	
	func getPNGData() -> Data? {
		guard let mutableData = CFDataCreateMutable(nil, 0),
			let destination = CGImageDestinationCreateWithData(mutableData, "public.png" as CFString, 1, nil) else { return nil }
		
		CGImageDestinationAddImage(destination, self, nil)
		guard CGImageDestinationFinalize(destination) else { return nil }
		return mutableData as Data
	}
	
}
