//
//  StarOctree.swift
//  AstronomicalCalculations
//
//  Created by Jürgen Terpe on 19.05.18.
//  Copyright © 2018 Stardust. All rights reserved.
//

import Foundation
import simd

public struct StarOctreeIndex {
	public var index: Int32
	public var magnitude: Float
	public var position: Double3
}

final public class StarOctree {
	private var items: [StarOctreeIndex] = []
	private var children: [StarOctree]?
	private var scale: Double = 1.0
	private var cellCenterPos: Double3 = Double3(0,0,0)
	private let SQRT3 = 1.732050807568877
	private let level: UInt16
	
	fileprivate enum XYZ: Int {
		case xPos = 1
		case yPos = 2
		case zPos = 4
	}
	
	public init() {
		self.cellCenterPos = Double3(0,0,0)
		self.scale = 1.0
		self.level = 0
		self.items.reserveCapacity(20)
	}
	
	public init(center: Double3, scale: Double, level: UInt16) {
		self.cellCenterPos = center
		self.scale = scale
		self.level = level
		//self.items.reserveCapacity(getEstimatedCapacity(level))
	}
	
	private func getEstimatedCapacity(_ level: UInt16) -> Int {
		switch level {
		case 0...4: return 20
		case 5: return 50
		case 6: return 100
		case 7: return 150
		default: return 250
		}
	}
	
	public func insert(star: StarOctreeIndex) {
		if let magLimit = getMagLimit() {
			if star.magnitude < magLimit {
				self.items.append(star)
			} else if let child = getChild(star.position) {
				child.insert(star: star)
			} else {
				if self.children == nil {
					split()
				}
				if let child = getChild(star.position) {
					child.insert(star: star)
				} else {
					self.items.append(star)
				}
			}
		}
		else {
			if let child = getChild(star.position) {
				child.insert(star: star)
			} else {
				self.items.append(star)
			}
		}
	}
	
	/// finds star ids nearby a given position.
	///
	/// - Parameters:
	///   - pos: the position
	///   - radius: the maximum separation in radians
	///   - maxMag: the maximum magnitude
	/// - Returns: the ids of stars nearby
	public func find(near pos: Double3, radius: Double, maxMagnitude maxMag: Float) -> [Int] {
		let nodeDistance = length(pos - self.cellCenterPos) - self.scale * SQRT3
		if nodeDistance > radius {
			guard let _ = self.children else {
				return []
			}
		}
		
		var result = [Int]()
		result.reserveCapacity(self.level == 0 ? 1000 : 100)
		
		let rsqr = radius * radius
		if self.items.count > 0 {
			for star in self.items {
				if star.magnitude <= maxMag {
					if length_squared(pos - star.position) <= rsqr {
						result.append(Int(star.index))
					}
				} else {
					break
				}
			}
		}

		if let magLimit = getMagLimit() {
			if maxMag > magLimit {
				guard let children = self.children else {
					return result
				}
				
				for child in children {
					result.append(contentsOf: child.find(near: pos, radius: radius, maxMagnitude: maxMag))
				}
			}
		} else {
			guard let children = self.children else {
				return result
			}
			
			for child in children {
				result.append(contentsOf: child.find(near: pos, radius: radius, maxMagnitude: maxMag))
			}
		}
	
		return result
	}
	
	private func split() {
		self.children = [StarOctree]()
		self.children!.reserveCapacity(8)
		let newScale = self.scale * 0.5
		let nextLevel = self.level + 1
		for i in 0..<8 {
			var cp = self.cellCenterPos
			cp.x += ((i & XYZ.xPos.rawValue) != 0) ? newScale : -newScale
			cp.y += ((i & XYZ.yPos.rawValue) != 0) ? newScale : -newScale
			cp.z += ((i & XYZ.zPos.rawValue) != 0) ? newScale : -newScale
				
			self.children!.append(StarOctree(center: cp, scale: newScale, level: nextLevel))
		}
	}
	
	private func getMagLimit() -> Float? {
		switch self.level {
		case 0:
			return 4.0
		case 1:
			return 5.0
		case 2:
			return 6.0
		case 3:
			return 7.0
		case 4:
			return 8.0
		case 5:
			return 9.0
		case 6:
			return 10.0
		case 7:
			return 11.0
		default:
			return nil
		}
	}
	
	private func getChild(_ pos: Double3) -> StarOctree? {
		guard let children = self.children else {
			return nil
		}
		
		var child = 0
		child     |= pos.x < cellCenterPos.x ? 0 : XYZ.xPos.rawValue
		child     |= pos.y < cellCenterPos.y ? 0 : XYZ.yPos.rawValue
		child     |= pos.z < cellCenterPos.z ? 0 : XYZ.zPos.rawValue
		
		return children[child]
	}
}
