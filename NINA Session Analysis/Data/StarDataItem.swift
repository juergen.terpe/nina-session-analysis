//
//  StarDataItem.swift
//  AstronomicalCalculations
//
//  Created by Jürgen Terpe on 08.10.17.
//  Copyright © 2017 Stardust. All rights reserved.
//

import Foundation
import simd

fileprivate struct StarDataRecord : Instantiable {
	fileprivate var raEncoded: UInt32
	fileprivate var decEncoded: UInt32
	fileprivate var vMag: Int16
	fileprivate var plx: UInt16
	fileprivate var pra: Float
	fileprivate var pdec: Float

	init() {
		self.raEncoded = 0
		self.decEncoded = 0
		self.vMag = 0
		self.plx = 0
		self.pra = 0.0
		self.pdec = 0.0
	}
}


public struct StarDataItem {
	private var index: Int32
	private var ra: Double
	private var dec: Double
	private var vMagEncoded: Int16
	private var colorId: UInt8
	private var parallax: UInt16
	private var pmRa: Float
	private var pmDec: Float
	
	private init(index: Int32, ra: Double, dec: Double, vMag: Int16, colorId: UInt8, plx: UInt16, pmRA: Float, pmDE: Float) {
		self.index = index
		self.ra = ra
		self.dec = dec
		self.vMagEncoded = vMag
		self.colorId = colorId
		self.parallax = plx
		self.pmRa = pmRA
		self.pmDec = pmDE
	}
	
	private static func decode(deg: UInt32) -> Double {
		let sign = deg & 0x80000000
		let d = deg & 0x7FFFFFFF
		let sec = d & 0x3FFF
		let min = (d >> 14) & 0x3F
		let dd = (d >> 20)
		
		if sign != 0 {
			return -(Double(dd) + Double(min)/60.0 + Double(sec)/360000.0)
		} else {
			return (Double(dd) + Double(min)/60.0 + Double(sec)/360000.0)
		}
	}
	
	public static func readAll(from reader: BinaryReader) -> [StarDataItem] {
		let count = reader.readInt32()
		let records: [StarDataRecord] = reader.readArray(Int(count))
		let colorIds: [UInt8] = reader.readUInt8Array(Int(count))
		var result = [StarDataItem]()
		result.reserveCapacity(records.count)
		for (index, rec) in records.enumerated() {
			let color = colorIds[index]
			
			let item = StarDataItem(index: Int32(index),
									ra: decode(deg: rec.raEncoded),
									dec: decode(deg: rec.decEncoded),
									vMag: rec.vMag,
									colorId: color,
									plx: rec.plx,
									pmRA: rec.pra,
									pmDE: rec.pdec)
			result.append(item)
		}
		return result
	}
	
	
	public func getApparent(_ t: Double, precNutMatrix: Matrix3x3, vEarth: Vector3) -> Vector3  {
		let pm = Double2(Double(self.pmRa), Double(self.pmDec)) * (100.0/3600000.0)
		let pos = Double2(self.ra, self.dec)
		let ppos = (pos + pm * t) * Constants.Rad
		return (precNutMatrix * Vector3.from(ra: ppos.x, dec: ppos.y) + vEarth).toUnit()
	}
	
	public func getApparent(_ t: Double, precNutMatrix: Matrix3x3) -> Vector3  {
		let pm = Double2(Double(self.pmRa), Double(self.pmDec)) * (100.0/3600000.0)
		let pos = Double2(self.ra, self.dec)
		let ppos = (pos + pm * t) * Constants.Rad
		return precNutMatrix * Vector3.from(ra: ppos.x, dec: ppos.y)
	}
	
	public func getPosition(_ t: Double) -> Vector3  {
		let pm = Double2(Double(self.pmRa), Double(self.pmDec)) * (100.0/3600000.0)
		let pos = Double2(self.ra, self.dec)
		let ppos = (pos + pm * t) * Constants.Rad
		return Vector3.from(ra: ppos.x, dec: ppos.y)
	}
	
	
	public func getMagnitude() -> Float? {
		if self.vMagEncoded != 0 {
			return Float(self.vMagEncoded)/100.0
		}
		return nil 
	}
	
	
	public var colorIndex: UInt8 {
		return self.colorId
	}
	
	/// Gets the polar coordinates 
	public var positionPolar: Polar {
		return Polar(ra: self.ra * Constants.Rad, dec: self.dec * Constants.Rad)
	}
	
	public var position: Vector3 {
		return Vector3.from(ra: self.ra * Constants.Rad, dec: self.dec * Constants.Rad)
	}
	
	public var id: Int32 {
		return self.index
	}
	
	public var hasName: Bool {
		if let names = StarDatabase.shared.getStarNames(self.index) {
			return names.hasCommonName() || names.hasBayerName()
		}
		return false 
	}
	
	public var hasCommonName: Bool {
		if let names = StarDatabase.shared.getStarNames(self.index) {
			return names.hasCommonName()
		}
		return false
	}
	
	
	public func getName() -> String? {
		if let names = StarDatabase.shared.getStarNames(self.index) {
			return names.names.first
		}
		return nil 
	}
}
