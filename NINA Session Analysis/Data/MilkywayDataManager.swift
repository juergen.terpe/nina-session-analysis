//
//  MilkywayDataManager.swift
//  AstronomicalCalculations
//
//  Created by Jürgen Terpe on 04.03.17.
//  Copyright © 2017 Stardust. All rights reserved.
//

import Foundation
import CoreGraphics

public struct MilkywayDataRegion: Sendable, Identifiable {
	public let id: UUID = UUID()
	public let positions: [Vector3]
	public let intensity: CGFloat
}

final public class MilkywayDataManager {
	public static let shared = MilkywayDataManager()
	
	struct MilkywayDot : Instantiable {
		var ra: Float
		var dec: Float
		var gray: Int16
		
		init() {
			self.ra = 0.0
			self.dec = 0.0
			self.gray = 0
		}
	}
	
	private var milkyway: [UInt32: [MilkywayDataRegion]] = [:]
	
	public init() {
		self.load()
	}
	
	public func getMilkywayData(center: Vector3, fov: Double, minIntensity: CGFloat) -> [MilkywayDataRegion] {

		if self.milkyway.count == 0 {
			self.load()
		}
		
		var result = [MilkywayDataRegion]()
		for blockId in findBlocks(for: center, in: fov) {
			if let block = getBlock(blockId: blockId, minIntensity) {
				result.append(contentsOf: block)
			}
		}
		return result
	}
	
	private func findBlocks(for center: Vector3, in fieldOfView: Double) -> [UInt32] {
		var blockIds = [UInt32]()
		let fovRad = fieldOfView * Constants.Rad
		let BlockRadiusRad = sqrt(2.0*15.0*15.0)/2.0
		
		for ra in stride(from: 0, to: 360, by: 15) {
			for dec in stride(from: -90, to: 90, by: 15) {
				let blockCenter = Vector3(polar: Polar(ra: (Double(ra)+7.5)*Constants.Rad,
				                                      dec: (Double(dec)+7.5)*Constants.Rad))
				let a = angle(center, blockCenter)
				if a <= fovRad + BlockRadiusRad {
					let x = UInt32( Int32( (Float(ra) + 7.5) / 15.0) << 16)
					let y = UInt32((90.0 + Float(dec) + 7.5) / 15.0)
					blockIds.append(UInt32(x | y))
				}
			}
		}
		return blockIds
	}
	
	private func getBlock(blockId: UInt32, _ minIntensity: CGFloat) -> [MilkywayDataRegion]? {
		if let block = self.milkyway[blockId] {
			return block.filter { $0.intensity >= minIntensity }
		}
		return nil 
	}
	
	private func getBlockId(ra: Float, dec: Float) -> UInt32 {
		let x = UInt32(Int32(ra / 15.0) << 16)
		let y = UInt32((90.0 + dec) / 15.0)

		return UInt32(x | y)
	}
	
	private func createDataRegion(_ block: [MilkywayDot]) -> [MilkywayDataRegion] {
		var result = [MilkywayDataRegion]()
		for group in block.groupBy(keyFunc: { $0.gray }) {
			let intensity = CGFloat(group.key)/255.0
			let positions = group.value.map {  Vector3(polar: Polar(ra: Double($0.ra) * Constants.Rad,
			                                  						dec: Double($0.dec) * Constants.Rad)) }
			result.append(MilkywayDataRegion(positions: positions, intensity: intensity))
		}
		return result
	}
	
	private func load() {
		var result = [UInt32 : [MilkywayDataRegion]]()
		
        if let filePath = Bundle.main.path(forResource: "Milkyway", ofType:"dat") {
			if let content = try? Data(contentsOf: URL(fileURLWithPath: filePath)) {
				let reader = BinaryReader(data: content)
				
				let count = reader.readInt32()
				for _ in 0..<count {
					let key = reader.readUInt32()
					let blockCount = reader.readInt32()
					let temp: [MilkywayDot] = reader.readArray(Int(blockCount))
					let block = createDataRegion(temp)
					result[key] = block
				}
			}
		}
		self.milkyway = result
	}
	
	public func preloadData() async {
		if self.milkyway.count == 0 {
		 	self.load()
		}
	}
	
	public func releaseAll() {
		self.milkyway.removeAll()
	}
}
