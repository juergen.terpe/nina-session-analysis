//
//  StarConstellationLines.swift
//  AstronomicalCalculations
//
//  Created by Jürgen Terpe on 11.10.17.
//  Copyright © 2017 Stardust. All rights reserved.
//

import Foundation

public struct StarConstellationLines {
	public var constellationId: UInt8
	public var line: [(from: Int, to: Int)]
	
	public static func read(from reader: BinaryReader) -> StarConstellationLines {
		let id = reader.readUInt8()
		let count = reader.readInt16()
		var lines = [(from: Int, to: Int)]()
		
		for _ in 0..<count {
			let from = Int(reader.readInt32())
			let to = Int(reader.readInt32())
			
			lines.append((from: from, to: to))
		}
		
		return StarConstellationLines(constellationId: id, line: lines)
	}
	
}
