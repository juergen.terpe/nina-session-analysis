//
//  StarDatabase.swift
//  AstronomicalCalculations
//
//  Created by Jürgen Terpe on 08.10.17.
//  Copyright © 2017 Stardust. All rights reserved.
//

import Foundation
import AppKit


final public class StarDatabase {
	public static let shared = StarDatabase()
	
	/// The star data for output on the UI
	private var stars: [StarDataItem] = []
	private var starTree: StarOctree?
	
	/// All star names
	private var starNames: [Int32 : Starnames] = [:]
	
	private var constellationLines: [UInt8 : StarConstellationLines] = [:]
	private var partOfConstellationLine = Set<Int>()
	
	
	/// Loads the star data to be used for showing information
	private func loadStars() {
		
		lock(self) {
			
			if self.stars.count == 0 {
                if let path = Bundle.init(for: StarDatabase.self).path(forResource: "Stars", ofType: "dat"),
                	let dataContent = try? Data(contentsOf: URL(fileURLWithPath: path)) {
					load(stars: dataContent)
				}
			}
		}
	}

	private func getStarTree() -> StarOctree? {
		if let tree = self.starTree {
			return tree
		}
		buildOctree()
		return self.starTree
	}
	
	
	/// Loads the star names
	private func loadStarNames() {
        if let path = Bundle.init(for: StarDatabase.self).path(forResource: "Starnames", ofType: "dat"),
			let dataContent = try? Data(contentsOf: URL(fileURLWithPath: path)) {
			load(starnames: dataContent)
		}
	}

	private func loadConstellationLines() {
        if let path = Bundle.init(for: StarDatabase.self).path(forResource: "ConstellationLines", ofType: "dat"),
			let dataContent = try? Data(contentsOf: URL(fileURLWithPath: path)) {
			load(constellationLines: dataContent)
		}
	}
	
	
	/// Loads the star data (containing all data to show)
	///
	/// - Parameter starData: the uncompressed binary data
	private func load(stars data: Data) {
		lock(self) { [unowned self] in
			
			if self.starTree == nil {
				let reader = BinaryReader(data: data)
				self.stars = StarDataItem.readAll(from: reader)
				
				DispatchQueue.global(qos: .default).async {
					self.buildOctree()
				}
			}
		}
	}
	
	private func buildOctree() {
		
		if self.starTree != nil {
			return
		}
		
		let start = Date()
		let octree = StarOctree()
		for (index, star) in self.stars.enumerated() {
			if let mag = star.getMagnitude(), mag < 9.0 {
				octree.insert(star: StarOctreeIndex(index: Int32(index), magnitude: mag, position: star.position.toDouble3()))
			}
		}
		self.starTree = octree
		print("time creating star octree: \(Date().timeIntervalSince(start))s")
	}
	
	/// Loads the star names
	///
	/// - Parameter data: the uncompressed binary data
	private func load(starnames data: Data) {
		let reader = BinaryReader(data: data)
		let count = reader.readInt32()
		
		var result = [Int32 : Starnames]()
		result.reserveCapacity(Int(count))
		
		for _ in 0 ..< count {
			let item = Starnames.read(from: reader)
			result[item.id] = item
		}
		
		self.starNames = result
	}
	
	private func load(constellationLines data: Data) {
		let reader = BinaryReader(data: data)
		let count = reader.readInt32()
		
		var result = [UInt8 : StarConstellationLines]()
		result.reserveCapacity(Int(count))
		var partOfIds = Set<Int>()
		
		for _ in 0 ..< count {
			let item = StarConstellationLines.read(from: reader)
			result[item.constellationId] = item
			for line in item.line {
				partOfIds.insert(line.from)
				partOfIds.insert(line.to)
			}
		}
		
		self.constellationLines = result 
		self.partOfConstellationLine = partOfIds
	}
	
	/*
	private func load(orientationLines data: Data) {
		let reader = BinaryReader(data: data)
		let count = reader.readInt32()
		
		var result = [StarOrientationLine]()
		result.reserveCapacity(Int(count))
		
		for _ in 0 ..< count {
			let item = StarOrientationLine.read(from: reader)
			result.append(item)
		}
		
		self.orientationLines = result
	}*/
	
	
	
	
	public func preload() async {
		self.loadStars()
		self.loadStarNames()
		self.loadConstellationLines()
	}
	
	public func preloadIfNeeded() {
		DispatchQueue.global().async { [unowned self] in 
			if self.stars.count == 0 {
				self.loadStars()
			}

			if self.starNames.count == 0 {
				self.loadStarNames()
			}
			if self.constellationLines.count == 0 {
				self.loadConstellationLines()
			}
		}
	}
	
	public func releaseAll() {
		lock(self)	{ [unowned self] in
			self.starNames.removeAll()
			self.stars.removeAll()
			print("releasing star octree...")
			self.starTree = nil
			self.constellationLines.removeAll()
		}
	}
	
	public func getVisibleStars(maxMagnitude mag: Float = 6.0) -> [StarDataItem] {
		
		if self.stars.count == 0 {
			self.loadStars()
		}
		
		var result = [StarDataItem]()
		
		lock(self)	{
			
			for star in self.stars {
				if let magStar = star.getMagnitude() {
					
					if magStar <= mag {
						result.append(star)
					} else {
						break
					}
				}
			}
		}
		return result
	}
	
	
	public func getVisibleStars(inside pos: Vector3, radius: Angle,
								maxMagnitude mag: Float = 999.9,
								withNameOnly: Bool = false) -> [StarDataItem] {
		if self.stars.count == 0 {
			self.loadStars()
		}
		
		var result = [StarDataItem]()
		
		let visRadius = radius.toRadians()
		
		lock(self)	{
			if self.stars.count > 0 {
								
				if let tree = getStarTree() {
					let indices = tree.find(near: pos.toDouble3(),
											radius: visRadius, maxMagnitude: mag)
					for index in indices {
						let star = self.stars[index]
						if withNameOnly && !star.hasName {
							continue
						}
							
						result.append(star)
					}
				}
			}
		}
		
		return result 
	}
	
	/// Gets all stars
	///
	/// - Returns: An array of stars
	public func getAllStars() -> [StarDataItem] {
		if self.stars.count == 0 {
			self.loadStars()
		}
		
		return self.stars
	}
	
	public func getStars(condition: (StarDataItem) -> Bool) -> [StarDataItem] {
		return getAllStars().filter{ condition($0) }
	}
	
	/*
	
	/// Gets all stars with name
	///
	/// - Returns: stars with name
	public func getStarsWithNames(includeConstellation: Bool = true) -> [StarWithName] {
		let starNames = getStarNames()
		var starsWithName = [StarWithName]()
		for (index, name) in starNames {
			if let starData = getStarData(by: Int(index)),
			   let mag = starData.getMagnitude() {
				
				if !includeConstellation {
					let bayer = name.bayer.count > 0 ? name.bayer.first!.components(separatedBy: " ").first : nil
					let flamsteed = name.flamsteed.count > 0 ? name.flamsteed.first!.components(separatedBy: " ").first : nil
					
					let star = StarWithName(id: index, mag: mag,
											position: starData.position,
											commonName: name.names.first,
											bayer: bayer,
											flamsteed: flamsteed)
					starsWithName.append(star)
					
				} else {
					let star = StarWithName(id: index, mag: mag,
											position: starData.position,
											commonName: name.names.first,
											bayer: name.bayer.first,
											flamsteed: name.flamsteed.first)
					starsWithName.append(star)
				}
				
				
			}
		}
		return starsWithName
	}
	
	public func getStarsWithCommonNames() -> [StarWithName] {
		let starNames = getStarNames()
		var starsWithName = [StarWithName]()
		for (index, name) in starNames {
			if name.hasCommonName(),
			   let starData = getStarData(by: Int(index)),
			   let mag = starData.getMagnitude() {
				
			   	let star = StarWithName(id: index, mag: mag,
									position: starData.position,
									commonName: name.names.first,
									bayer: name.bayer.first,
									flamsteed: name.flamsteed.first)
				starsWithName.append(star)
			}
		}
		return starsWithName
	}
	*/
	
	private func getStarNames() -> [Int32 : Starnames] {
		if self.starNames.count == 0 {
			loadStarNames()
		}
		return self.starNames
	}
	
	/// Gets the star by its index (or internal id)
	///
	/// - Parameter index: the star index
	/// - Returns: The star data or nil if this id is unknown
	public func getStarData(by index: Int) -> StarDataItem? {
		if self.stars.count == 0 {
			self.loadStars()
		}
		return self.stars[index]
	}
	
	
	/// Gets a dictionary of all star constellation lines
	/// (containing the ids of the stars to be connected)
	/// - Returns: A dictionary with all star constellations and its line connections
	public func getConstellationLines() -> [UInt8 : StarConstellationLines] {
		if self.constellationLines.count == 0 {
			loadConstellationLines()
		}
		
		return self.constellationLines
	}
	
	/// Gets the lines for a single constellation
	public func getConstellationLines(for index: UInt8) -> StarConstellationLines? {
		if self.constellationLines.count == 0 {
			loadConstellationLines()
		}
		
		return self.constellationLines[index]
	}
	
}

public extension StarDatabase {
	
	
	/// Checks if a star name exists for this star
	///
	/// - Parameter id: the internal star id
	/// - Returns: true if a star name exists, false otherwise
	func hasStarNames(_ id: Int32) -> Bool {
		if let _ = getStarNames(id) {
			return true
		}
		return false
	}
	
	/// Gets the star names
	///
	/// - Parameter id: the internal star id
	/// - Returns: the star names or nil if no star name exists/is available
	func getStarNames(_ id: Int32) -> Starnames? {
		let starnamesInternal = getStarNames()
		return starnamesInternal[id]
	}

	
	/// Checks if the star with this id is part of a constellation line
	///
	/// - Parameter id: the internal star id
	/// - Returns: true if part of a constellation line, false otherwise
	func isPartOfConstellationLine(_ id: Int32) -> Bool {
		if self.partOfConstellationLine.count == 0 {
			loadConstellationLines()
		}
		return self.partOfConstellationLine.contains(Int(id))
	}
	
}
