//
//  StarNames.swift
//  AstronomicalCalculations
//
//  Created by Jürgen Terpe on 11.12.16.
//  Copyright © 2016 Stardust. All rights reserved.
//

import Foundation

public enum StarNameType {
	case common
	case bayer
	case flamsteed
}

public struct Starnames : Instantiable {
	public var id: Int32
	public var names: [String]
	public var bayer: [String]
	public var flamsteed: [String]
	
	public init() {
		self.id = 0
		self.names = []
		self.bayer = []
		self.flamsteed = []
	}
		
	public static func read(from reader: BinaryReader) -> Starnames {
		var starNames = Starnames()
		starNames.id = reader.readInt32()
		starNames.names = reader.readShortStringArray(Int(reader.readInt8()))
		starNames.bayer = reader.readShortStringArray(Int(reader.readInt8()))
		starNames.flamsteed = reader.readShortStringArray(Int(reader.readInt8()))
		
		return starNames
	}
}

public extension Starnames {
	func hasCommonName() -> Bool {
		return self.names.count > 0
	}
	func hasBayerName() -> Bool {
		return self.bayer.count > 0
	}
	func hasFlamsteedName() -> Bool {
		return self.flamsteed.count > 0
	}
	
	var name: String {
		get {
			if hasCommonName() {
				return self.names.first!
			} else if hasBayerName() {
				return self.bayer.first!
			} else {
				return self.flamsteed.first!
			}
		}
	}
}

