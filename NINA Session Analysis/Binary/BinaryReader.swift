//
//  BinaryReader.swift
//  Utilities
//
//  Created by Jürgen Terpe on 07.12.16.
//  Copyright © 2016 Stardust. All rights reserved.
//

import Foundation

public protocol Instantiable {
	init()
}

fileprivate extension Data {
	func copyBytes<T>(as _: T.Type, from range: Range<Data.Index>) -> [T] {
		let subData = self.subdata(in: range)
		return subData.withUnsafeBytes { (bytes: UnsafePointer<T>) in
			Array(UnsafeBufferPointer(start: bytes, count: subData.count / MemoryLayout<T>.stride))
		}
	}
}


extension Data {
	
	init<T>(from value: T) {
		var value = value
		self = withUnsafePointer(to: &value) {
			Data(buffer: UnsafeBufferPointer(start: $0, count: 1))
		}
	}
	
	func to<T>(type: T.Type) -> T {
		return self.withUnsafeBytes { $0.load(as: T.self) }
	}
	
	init<T>(fromArray values: [T]) {
		self = values.withUnsafeBufferPointer {
			Data(buffer: $0)
		}
	}
}




final public class BinaryReader {
	private var data: Data
	private var position: Int = 0
	
	public init(data: Data) {
		self.data = data
		self.position = 0
	}
	
	public func readInt8() -> Int8 {
		let endPosition = self.position + MemoryLayout<Int8>.size
		let subData = self.data.subdata(in: self.position..<endPosition)
		self.position = endPosition
		return subData.to(type: Int8.self)
	}
	
	public func readUInt8() -> UInt8 {
		let endPosition = self.position + MemoryLayout<UInt8>.size
		let subData = self.data.subdata(in: self.position..<endPosition)
		self.position = endPosition
		return subData.to(type: UInt8.self)
	}
	
	public func readInt16() -> Int16 {
		let endPosition = self.position + MemoryLayout<Int16>.size
		let subData = self.data.subdata(in: self.position..<endPosition)
		self.position = endPosition
		return subData.to(type: Int16.self)
	}
	
	public func readUInt16() -> UInt16 {
		let endPosition = self.position + MemoryLayout<UInt16>.size
		let subData = self.data.subdata(in: self.position..<endPosition)
		self.position = endPosition
		return subData.to(type: UInt16.self)
	}
	
	public func readInt32() -> Int32 {
		let endPosition = self.position + MemoryLayout<Int32>.size
		let subData = self.data.subdata(in: self.position..<endPosition)
		self.position = endPosition
		return subData.to(type: Int32.self)
	}
	
	public func readUInt32() -> UInt32 {
		let endPosition = self.position + MemoryLayout<UInt32>.size
		let subData = self.data.subdata(in: self.position..<endPosition)
		self.position = endPosition
		return subData.to(type: UInt32.self)
	}
	
	public func readInt64() -> Int64 {
		let endPosition = self.position + MemoryLayout<Int64>.size
		let subData = self.data.subdata(in: self.position..<endPosition)
		self.position = endPosition
		return subData.to(type: Int64.self)
	}
	
	public func readUInt64() -> UInt64 {
		let endPosition = self.position + MemoryLayout<UInt64>.size
		let subData = self.data.subdata(in: self.position..<endPosition)
		self.position = endPosition
		return subData.to(type: UInt64.self)
	}
	
	public func readFloat() -> Float {
		let endPosition = self.position + MemoryLayout<Float>.size
		let subData = self.data.subdata(in: self.position..<endPosition)
		self.position = endPosition
		return subData.to(type: Float.self)
	}
	
	public func readDouble() -> Double {
		let endPosition = self.position + MemoryLayout<Double>.size
		let subData = self.data.subdata(in: self.position..<endPosition)
		self.position = endPosition
		return subData.to(type: Double.self)
	}
	
	
	public func readString(_ encoding: String.Encoding = .utf8) -> String {
		let strLen = readInt32()
		if strLen > 0 {
			let endPosition = self.position + Int(strLen)
			let subData = self.data.subdata(in: self.position..<endPosition)
			self.position = endPosition
			return String(data: subData, encoding: encoding)!
		}
		return ""
	}
	
	public func readShortString(_ encoding: String.Encoding = .utf8) -> String {
		let strLen = readUInt8()
		if strLen > 0 {
			let endPosition = self.position + Int(strLen)
			let subData = self.data.subdata(in: self.position..<endPosition)
			self.position = endPosition
			return String(data: subData, encoding: encoding)!
		}
		return ""
	}
	
	public func readCharacter() -> Character {
		let cValue = readUInt8()
		return Character(Unicode.Scalar(cValue))
	}
	
	public func readInt8Array(_ count: Int) -> [Int8] {
		let endPosition = self.position + count * MemoryLayout<Int8>.stride
		let subData = self.data.subdata(in: self.position..<endPosition)
		self.position = endPosition
		
		return subData.withUnsafeBytes{ (bytes: UnsafePointer<Int8>) in
			Array(UnsafeBufferPointer(start: bytes, count: subData.count / MemoryLayout<Int8>.stride))
		}
	}
	
	public func readUInt8Array(_ count: Int) -> [UInt8] {
		let endPosition = self.position + count * MemoryLayout<UInt8>.stride
		let subData = self.data.subdata(in: self.position..<endPosition)
		self.position = endPosition
		return subData.withUnsafeBytes{ (bytes: UnsafePointer<UInt8>) in
			Array(UnsafeBufferPointer(start: bytes, count: subData.count / MemoryLayout<UInt8>.stride))
		}
	}
	
	public func readInt16Array(_ count: Int) -> [Int16] {
		let endPosition = self.position + count * MemoryLayout<Int16>.stride
		let subData = self.data.subdata(in: self.position..<endPosition)
		self.position = endPosition
		return subData.withUnsafeBytes{ (bytes: UnsafePointer<Int16>) in
			Array(UnsafeBufferPointer(start: bytes, count: subData.count / MemoryLayout<Int16>.stride))
		}
	}
	
	public func readUInt16Array(_ count: Int) -> [UInt16] {
		let endPosition = self.position + count * MemoryLayout<UInt16>.stride
		let subData = self.data.subdata(in: self.position..<endPosition)
		self.position = endPosition
		return subData.withUnsafeBytes{ (bytes: UnsafePointer<UInt16>) in
			Array(UnsafeBufferPointer(start: bytes, count: subData.count / MemoryLayout<UInt16>.stride))
		}
	}
	
	public func readInt32Array(_ count: Int) -> [Int32] {
		let endPosition = self.position + count * MemoryLayout<Int32>.stride
		let subData = self.data.subdata(in: self.position..<endPosition)
		self.position = endPosition
		return subData.withUnsafeBytes{ (bytes: UnsafePointer<Int32>) in
			Array(UnsafeBufferPointer(start: bytes, count: subData.count / MemoryLayout<Int32>.stride))
		}
	}
	
	public func readUInt16Array(_ count: Int) -> [UInt32] {
		let endPosition = self.position + count * MemoryLayout<UInt32>.stride
		let subData = self.data.subdata(in: self.position..<endPosition)
		self.position = endPosition
		return subData.withUnsafeBytes{ (bytes: UnsafePointer<UInt32>) in
			Array(UnsafeBufferPointer(start: bytes, count: subData.count / MemoryLayout<UInt32>.stride))
		}
	}
	
	public func readInt64Array(_ count: Int) -> [Int64] {
		let endPosition = self.position + count * MemoryLayout<Int64>.stride
		let subData = self.data.subdata(in: self.position..<endPosition)
		self.position = endPosition
		return subData.withUnsafeBytes{ (bytes: UnsafePointer<Int64>) in
			Array(UnsafeBufferPointer(start: bytes, count: subData.count / MemoryLayout<Int64>.stride))
		}
	}
	
	public func readUInt64Array(_ count: Int) -> [UInt64] {
		let endPosition = self.position + count * MemoryLayout<UInt64>.stride
		let subData = self.data.subdata(in: self.position..<endPosition)
		self.position = endPosition
		return subData.withUnsafeBytes{ (bytes: UnsafePointer<UInt64>) in
			Array(UnsafeBufferPointer(start: bytes, count: subData.count / MemoryLayout<UInt64>.stride))
		}
	}
	
	public func readDoubleArray(_ count: Int) -> [Double] {
		let endPosition = self.position + count * MemoryLayout<Double>.stride
		let subData = self.data.subdata(in: self.position..<endPosition)
		self.position = endPosition
		return subData.withUnsafeBytes{ (bytes: UnsafePointer<Double>) in
			Array(UnsafeBufferPointer(start: bytes, count: subData.count / MemoryLayout<Double>.stride))
		}
	}
	
	public func readFloatArray(_ count: Int) -> [Float] {
		let endPosition = self.position + count * MemoryLayout<Float>.stride
		let subData = self.data.subdata(in: self.position..<endPosition)
		self.position = endPosition
		return subData.withUnsafeBytes{ (bytes: UnsafePointer<Float>) in
			Array(UnsafeBufferPointer(start: bytes, count: subData.count / MemoryLayout<Float>.stride))
		}
	}
	
	public func readStringArray(_ count: Int, encoding: String.Encoding = .utf8) -> [String] {
		var result = [String]()
		for _ in 0..<count {
			result.append(readString(encoding))
		}
		return result
	}
	
	public func readShortStringArray(_ count: Int, encoding: String.Encoding = .utf8) -> [String] {
		var result = [String]()
		for _ in 0..<count {
			result.append(readShortString(encoding))
		}
		return result
	}
	
	public func read<T: Instantiable>() -> T {
		let endPosition = self.position + MemoryLayout<T>.size
		var output = T()
		
		let _ = withUnsafeMutableBytes(of: &output) {
			self.data.copyBytes(to: $0, from: self.position..<endPosition)
		}

		self.position = endPosition
		
		return output
	}
	
	public func readArray<T: Instantiable>(count: Int, size: Int) -> [T] {
		let bytesCount = count * size
		let endPosition = self.position + bytesCount
		
		let output = data.copyBytes(as: T.self, from: self.position..<endPosition)
		self.position = endPosition
		return output
	}
	
	
	public func readArray<T: Instantiable>(_ count: Int) -> [T] {
		let bytesCount = count * MemoryLayout<T>.stride
		let endPosition = self.position + bytesCount
		
		let output = data.copyBytes(as: T.self, from: self.position..<endPosition)
		self.position = endPosition
		return output
	}
	
	public func readSubData(_ length: Int) -> Data {
		let endPosition = self.position + length
		let subData = self.data.subdata(in: self.position..<endPosition)
		self.position = endPosition
		return subData
	}
	
	public func readRemainingData() -> Data {
		let length = self.data.count - self.position
		let endPosition = self.position + length
		let subData = self.data.subdata(in: self.position..<endPosition)
		self.position = endPosition
		return subData
	}
	
	public func readSubBlock(_ length: Int) -> BinaryReader {
		return BinaryReader(data: readSubData(length))
	}
}
