//
//  TiltInspector.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 12.08.24.
//
import CoreImage
import SwiftUI
import Charts

enum TiltTileRectType: CaseIterable {
	case leftTop
	case leftMiddle
	case leftBottom
	case middleTop
	case middleMiddle
	case middleBottom
	case rightTop
	case rightMiddle
	case rightBottom
}


class TiltTileRect {
	var stars: [DetectedStar] = []
	var hfd: Float = 0.0
	var delta: Float = 0.0
	let rect: CGRect
	let type: TiltTileRectType
	var hfdDistribution:  [BinnedDataValue] = []
	var eccDistribution:  [BinnedDataValue] = []
	var meanHFD: Float? = nil
	var stdDevHFD: Float? = nil
	var maxHFD: Float? = nil
	var meanECC: Float? = nil
	var stdDevECC: Float? = nil
	var maxECC: Float? = nil
	
	
	init(rect: CGRect, type: TiltTileRectType) {
		self.rect = rect
		self.type = type
	}
	
	func add(star: DetectedStar) {
		self.stars.append(star)
	}
	
	@discardableResult
	fileprivate func hfdValues(max: Int) -> Float? {
		if self.stars.isEmpty {
			return nil
		}
		var values = self.stars.map{ $0.hfd }
		self.hfd = getMedian(&values)
		
		return self.hfd
	}
	
	fileprivate func createDistributions() {
		if !self.stars.isEmpty {
			let hfdDistributions = self.stars.map{ $0.hfd }
			let eccDistributions = self.stars.map{ $0.eccentricity }
			
			let binsHFD = NumberBins(
				data: hfdDistributions,
				desiredCount: 15
			)
			
			let binsEcc = NumberBins(
				data: eccDistributions,
				desiredCount: 15
			)
			
			let groupsHFD = Dictionary(
				grouping: hfdDistributions,
				by: binsHFD.index
			)
			
			let groupsEcc = Dictionary(
				grouping: eccDistributions,
				by: binsEcc.index
			)
			
			let meanHFD = hfdDistributions.reduce(0, +) / Float(hfdDistributions.count)
			let stdDevHFD = hfdDistributions.map { abs($0 - meanHFD) }.reduce(0.0, +) / Float(hfdDistributions.count)
			let maxHFD = hfdDistributions.max()
						
			let meanECC = eccDistributions.reduce(0, +) / Float(eccDistributions.count)
			let stdDevECC = eccDistributions.map { abs($0 - meanECC) }.reduce(0.0, +) / Float(eccDistributions.count)
			let maxECC = eccDistributions.max()
			
			self.hfdDistribution = groupsHFD.map { key, values in
				return BinnedDataValue(
					index: key,
					range: binsHFD[key],
					frequency: values.count
				)
			}
			
			self.eccDistribution = groupsEcc.map { key, values in
				return BinnedDataValue(
					index: key,
					range: binsEcc[key],
					frequency: values.count
				)
			}
			
			self.stdDevHFD = stdDevHFD
			self.meanHFD = meanHFD
			self.maxHFD = maxHFD
			
			self.stdDevECC = stdDevECC
			self.meanECC = meanECC
			self.maxECC = maxECC
		}
	}
	
	var center: CGPoint {
		return rect.center
	}
	
	var size: CGFloat {
		return min(rect.width, rect.height)
	}
	
	
	var vector: CGVector {
		switch self.type {
			case .leftTop:
				return CGVector(dx: -1.0, dy: -1.0)
			case .leftMiddle:
				return CGVector(dx: -1.0, dy: 0.0)
			case .leftBottom:
				return CGVector(dx: -1.0, dy: +1.0)
			case .middleTop:
				return CGVector(dx: 0.0, dy: -1.0)
			case .middleMiddle:
				return CGVector(dx: 0.0, dy: 0.0)
			case .middleBottom:
				return CGVector(dx: 0.0, dy: +1.0)
			case .rightTop:
				return CGVector(dx: 1.0, dy: -1.0)
			case .rightMiddle:
				return CGVector(dx: 1.0, dy: 0.0)
			case .rightBottom:
				return CGVector(dx: 1.0, dy: 1.0)
		}
	}
}

struct TiltInspectionResult {
	var tiles: [TiltTileRectType : TiltTileRect]
	var hfdOut: Float
	var hfdIn: Float
	var hfdWorst: Float
	var hfdBest: Float
	var tiltHfd: Float
	var medianHFD: Float
	
	var tiltPercentage: Float {
		return 100.0 * tiltHfd / medianHFD
	}
	
	var tilt: (text: LocalizedStringKey, color: Color) {
		let percent = self.tiltPercentage
		if percent < 4.0 {
			return (text: LocalizedStringKey("No Tilt"), color: Color.green)
		} else if percent < 10.0 {
			return (text: LocalizedStringKey("Neglectable"), color: Color.green)
		} else if percent < 16.0 {
			return (text: LocalizedStringKey("Acceptable"), color: Color.yellow)
		} else {
			return (text: LocalizedStringKey("Serious"), color: Color.red)
		}
	}
	
	var offAxis: Float {
		return hfdOut - hfdIn
	}

}


class TiltInspector {
	
	
	func process(_ cgImage: CGImage, stars: [DetectedStar]) async -> TiltInspectionResult? {
		let width = cgImage.width
		let height = cgImage.height
		
		let imageCenter = CGPoint(x: Double(width)/2.0, y: Double(height)/2.0)
		let circle = hypot(Double(width)/2.0, Double(height)/2.0)
		let outerCircle = circle * 0.75
		let innerCircle = circle * 0.25
		
		let tileWidth = width / 3
		let tileHeight = height / 3
		
		var tiles = [TiltTileRectType : TiltTileRect]()
		var centerHFD: Float?
		var hfdInner = [Float]()
		var hfdOuter = [Float]()
		var hfdAll = [Float]()
		
		var starRects = [CGRect]()
		let starRectRadius = 0.5 * Double(min(tileWidth, tileHeight)) / 10.0
				
		for type in TiltTileRectType.allCases {
			let rect = createRectangle(type, width: tileWidth, height: tileHeight)
			let tile = TiltTileRect(rect: rect, type: type)
			tiles[type] = tile
		}
		
		let maxHFD = 4.0 * (stars.min(by: { $0.hfd < $1.hfd })?.hfd ?? 4.0)
		
		for star in stars.filter({ $0.hfd <= maxHFD  && $0.hfd > 1.8 }).reversed() {
			
			let starRect = CGRect.from(center: star.position, radius: starRectRadius)
			if let _ = starRects.first(where: { $0.intersects(starRect)} ) {
				continue
			} else {
				
				starRects.append(starRect)
				
				for tile in tiles.values {
					if tile.stars.count < 250, tile.rect.contains(star.position) {
						tile.add(star: star)
						break
					}
				}
				
				let distCenter = hypot(star.position.x - imageCenter.x, star.position.y - imageCenter.y)
				if distCenter >= outerCircle, hfdOuter.count < 250 {
					hfdOuter.append(star.hfd)
				} else if distCenter < innerCircle, hfdInner.count < 250 {
					hfdInner.append(star.hfd)
				}
				hfdAll.append(star.hfd)
			}
		}
		
		if hfdInner.isEmpty || hfdOuter.isEmpty { return nil }
		
		let hfdIn = getMedian(&hfdInner)// hfdInner.sorted()[hfdInner.count/2]
		let hfdOut = getMedian(&hfdOuter)//.sorted()[hfdOuter.count/2]
		//let hfdMedian = getMedian(&hfdAll)//.sorted()[hfdAll.count/2]
		
		
		if let minCount = tiles.values.map({ $0.stars.count }).min() {
			
			if let cHFD = tiles[.middleMiddle]?.hfdValues(max: minCount) {
				centerHFD = cHFD
			}
			
			guard let centerHFD else { return nil }
			
			for tile in tiles.values {
				
				if let hfd = tile.hfdValues(max: minCount) {
					tile.delta = hfd - centerHFD
				}
				tile.createDistributions()
			}
			
			if let tile1 = tiles[.leftTop], let tile2 = tiles[.rightTop],
			   let tile3 = tiles[.leftBottom], let tile4 = tiles[.rightBottom] {
				let hfdBest = min(min(tile1.hfd, tile2.hfd),
								  min(tile3.hfd, tile4.hfd))
				
				let hfdWorst = max(max(tile1.hfd, tile2.hfd),
								   max(tile3.hfd, tile4.hfd))
				
				let outerHfds = [tile1.hfd, tile2.hfd, tile3.hfd,
								 tile4.hfd].sorted()
				
				let hfdMedian = (outerHfds[1] + outerHfds[2])/2.0
				
				let tiltHFD = hfdWorst - hfdBest
				
				return TiltInspectionResult(tiles: tiles, hfdOut: hfdOut, hfdIn: hfdIn,
											hfdWorst: hfdWorst, hfdBest: hfdBest,
											tiltHfd: tiltHFD, medianHFD: hfdMedian)
			}
		}
		
		return nil
	}
	
	private func createRectangle(_ type: TiltTileRectType, width: Int, height: Int) -> CGRect {
		let size = CGSize(width: width, height: height)
		switch type {
			case .leftTop:
				return CGRect(origin: CGPoint(x: 0, y: 0),
							  size: size)
			case .leftMiddle:
				return CGRect(origin: CGPoint(x: 0, y: height),
							  size: size)
			case .leftBottom:
				return CGRect(origin: CGPoint(x: 0, y: 2*height),
							  size: size)
			case .middleTop:
				return CGRect(origin: CGPoint(x: width, y: 0),
							  size: size)
			case .middleMiddle:
				return CGRect(origin: CGPoint(x: width, y: height),
							  size: size)
			case .middleBottom:
				return CGRect(origin: CGPoint(x: width, y: 2*height),
							  size: size)
			case .rightTop:
				return CGRect(origin: CGPoint(x: 2*width, y: 0),
							  size: size)
			case .rightMiddle:
				return CGRect(origin: CGPoint(x: 2*width, y: height),
							  size: size)
			case .rightBottom:
				return CGRect(origin: CGPoint(x: 2*width, y: 2*height),
							  size: size)
		}
	}
}
