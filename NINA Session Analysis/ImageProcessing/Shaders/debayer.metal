//
//  debayer.metal
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 21.07.24.
//

#include <metal_stdlib>
using namespace metal;

enum BayerPattern {
	RGGB,
	BGGR,
	GBRG,
	GRBG,
	RGBG,
	GRGB,
	GBGR,
	BGRG
};


inline uint2 offsetToBGGR(BayerPattern pattern) {
	switch(pattern) {
		case RGGB: return uint2(1, 1);
		case BGGR: return uint2(0, 0);
		case GBRG: return uint2(0, 1);
		case GRBG: return uint2(1, 0);
		case RGBG: return uint2(1, 1);
		case GRGB: return uint2(1, 0);
		case GBGR: return uint2(0, 1);
		case BGRG: return uint2(0, 0);
		default: return uint2(0, 0);
	}
}

inline ushort safeRead(texture2d<ushort, access::read> tex, int x, int y) {
	int w = tex.get_width();
	int h = tex.get_height();
	x = clamp(x, 0, w - 1);
	y = clamp(y, 0, h - 1);
	return tex.read(uint2(x, y)).r;
}

kernel void debayer(texture2d<ushort, access::read> inTexture [[texture(0)]],
					texture2d<ushort, access::write> outTexture [[texture(1)]],
					constant int &bayerPattern [[buffer(0)]],
					uint2 gid [[thread_position_in_grid]]) {

	if (gid.x >= inTexture.get_width() || gid.y >= inTexture.get_height()) {
		return;
	}

	BayerPattern pattern = static_cast<BayerPattern>(bayerPattern);
	uint2 offset = offsetToBGGR(pattern);

	float R = 0.0, G = 0.0, B = 0.0;

	// Bilineare Interpolation mit sicherem Lesen
	if ((gid.x % 2 == offset.x) && (gid.y % 2 == offset.y)) {
		R =  float(safeRead(inTexture, gid.x, gid.y));
		G = (float(safeRead(inTexture, gid.x - 1, gid.y)) +
			 float(safeRead(inTexture, gid.x + 1, gid.y)) +
			 float(safeRead(inTexture, gid.x, gid.y - 1)) +
			 float(safeRead(inTexture, gid.x, gid.y + 1))) * 0.25;
		B = (float(safeRead(inTexture, gid.x - 1, gid.y - 1)) +
			 float(safeRead(inTexture, gid.x + 1, gid.y - 1)) +
			 float(safeRead(inTexture, gid.x - 1, gid.y + 1)) +
			 float(safeRead(inTexture, gid.x + 1, gid.y + 1))) * 0.25;

	} else if ((gid.x % 2 != offset.x) && (gid.y % 2 != offset.y)) {
		B =  float(safeRead(inTexture, gid.x, gid.y));
		G = (float(safeRead(inTexture, gid.x - 1, gid.y)) +
			 float(safeRead(inTexture, gid.x + 1, gid.y)) +
			 float(safeRead(inTexture, gid.x, gid.y - 1)) +
			 float(safeRead(inTexture, gid.x, gid.y + 1))) * 0.25;
		R = (float(safeRead(inTexture, gid.x - 1, gid.y - 1)) +
			 float(safeRead(inTexture, gid.x + 1, gid.y - 1)) +
			 float(safeRead(inTexture, gid.x - 1, gid.y + 1)) +
			 float(safeRead(inTexture, gid.x + 1, gid.y + 1))) * 0.25;

	} else if ((gid.x % 2 == offset.x) && (gid.y % 2 != offset.y)) {
		G = float(safeRead(inTexture, gid.x, gid.y));
		R = (float(safeRead(inTexture, gid.x, gid.y - 1)) +
			 float(safeRead(inTexture, gid.x, gid.y + 1))) * 0.5;
		B = (float(safeRead(inTexture, gid.x - 1, gid.y)) +
			 float(safeRead(inTexture, gid.x + 1, gid.y))) * 0.5;

	} else if ((gid.x % 2 != offset.x) && (gid.y % 2 == offset.y)) {
		G = float(safeRead(inTexture, gid.x, gid.y));
		B = (float(safeRead(inTexture, gid.x, gid.y - 1)) +
			 float(safeRead(inTexture, gid.x, gid.y + 1))) * 0.5;
		R = (float(safeRead(inTexture, gid.x - 1, gid.y)) +
			 float(safeRead(inTexture, gid.x + 1, gid.y))) * 0.5;
	}

	R = clamp(R, 0.0, 65535.0);
	G = clamp(G, 0.0, 65535.0);
	B = clamp(B, 0.0, 65535.0);

	ushort4 pixel = ushort4(ushort(R), ushort(G), ushort(B), 65535);
	outTexture.write(pixel, gid);
}
