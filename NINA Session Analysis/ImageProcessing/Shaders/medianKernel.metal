//
//  medianKernel.metal
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 24.07.24.
//

#include <metal_stdlib>
using namespace metal;

struct Params {
	uint pixelCount;
};

kernel void medianAndStdDevKernel(device atomic_uint *histogram [[ buffer(1) ]],
								  device float *results [[ buffer(2) ]],
								  constant Params &params [[ buffer(0) ]],
								  uint2 gid [[ thread_position_in_grid ]]) {
	if (gid.x != 0 || gid.y != 0) return;

	uint halfPixelCount = params.pixelCount / 2;
	uint cumulativeCount = 0;
	uint medianIndex = 0;

	// Compute Median
	for (int i = 0; i < 65536; i++) {
		cumulativeCount += atomic_load_explicit(&histogram[i], memory_order_relaxed);
		if (cumulativeCount >= halfPixelCount) {
			medianIndex = i;
			results[0] = float(i) / 65535.0;
			break;
		}
	}

	
	uint occurrences = 0;
	int idxDown = medianIndex;
	int idxUp = medianIndex;
	results[1] = 0.0;
	
	while (true) {
		if (idxDown >= 0 && idxDown != idxUp) {
			occurrences += atomic_load_explicit(&histogram[idxDown], memory_order_relaxed) + atomic_load_explicit(&histogram[idxUp], memory_order_relaxed);
		} else {
			occurrences += atomic_load_explicit(&histogram[idxUp], memory_order_relaxed);
		}

		if (occurrences > halfPixelCount) {
			results[1] = abs( float(idxUp) / 65535.0 - results[0]);
			break;
		}

		idxUp += 1;
		idxDown -= 1;
		if (idxUp > 65535) {
			break;
		}
	}
	
	/*
	// Compute Standard Deviation
	float mean = results[0] * 65535.0;
	float varianceSum = 0.0;
	cumulativeCount = 0;

	for (int i = 0; i < 65536; i++) {
		uint binCount = atomic_load_explicit(&histogram[i], memory_order_relaxed);
		cumulativeCount += binCount;
		float diff = float(i) - mean;
		varianceSum += diff * diff * float(binCount);
	}

	float variance = varianceSum / float(params.pixelCount);
	results[1] = sqrt(variance) / 65535.0;
	*/
}

