//
//  calculateStarHFRKernel.metal
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 28.07.24.
//

#include <metal_stdlib>
using namespace metal;


kernel void calculateHFR(texture2d<ushort, access::read> inTexture [[texture(0)]],
						 device float *output [[buffer(0)]],
						 constant int &width [[buffer(1)]],
						 constant int &height [[buffer(2)]],
						 constant int2 &position [[buffer(3)]],
						 constant float &localThreshold [[buffer(4)]],
						 constant int &apertureRadius [[buffer(5)]],
						 uint2 gid [[thread_position_in_grid]]) {
	if (gid.x >= inTexture.get_width() || gid.y >= inTexture.get_height()) {
		return;
	}

	float totalFlux = 0.0;
	float halfFlux = 0.0;
	float flux = 0.0;
	float distance = 0.0;
	float sx = 0.0;
	float sy = 0.0;
	
	float fluxSum = 0.0;
	float weightedDistanceX = 0.0;
	float weightedDistanceY = 0.0;
	
	// First pass to calculate totalFlux
	for (int dy = -apertureRadius; dy <= apertureRadius; dy++) {
		for (int dx = -apertureRadius; dx <= apertureRadius; dx++) {
			int2 offset = int2(dx, dy);
			int2 coord = position + offset;

			if (coord.x >= 0 && coord.y >= 0 && coord.x < width && coord.y < height) {
				distance = length(float2(dx, dy));
				if (distance < float(apertureRadius)) {
					ushort lum = inTexture.read(uint2(coord)).r;
					float pixelValue = float(lum) / 65535.0;
					if (pixelValue > localThreshold) {
						flux = pixelValue - localThreshold;
						fluxSum += flux;
					}
				}
			}
		}
	}

	totalFlux = fluxSum;
	halfFlux = totalFlux / 2.0;

	fluxSum = 0.0;
	weightedDistanceX = 0.0;
	weightedDistanceY = 0.0;

	// Second pass to find the HFR
	for (int dy = -apertureRadius; dy <= apertureRadius; dy++) {
		for (int dx = -apertureRadius; dx <= apertureRadius; dx++) {
			int2 offset = int2(dx, dy);
			int2 coord = position + offset;

			if (coord.x >= 0 && coord.y >= 0 && coord.x < width && coord.y < height) {
				distance = length(float2(dx, dy));
				if (distance <= float(apertureRadius)) {
					ushort lum = inTexture.read(uint2(coord)).r;
					float pixelValue = float(lum) / 65535.0;
					if (pixelValue > localThreshold) {
						flux = pixelValue - localThreshold;
						fluxSum += flux;
						weightedDistanceX += flux * dx * dx;
						weightedDistanceY += flux * dy * dy;
						
						if (fluxSum >= halfFlux) {
							sx = sqrt(weightedDistanceX / totalFlux);
							sy = sqrt(weightedDistanceY / totalFlux);
							output[0] = distance;
							output[1] = sx;
							output[2] = sy;
							return;
						}
						
					}
				}
			}
		}
	}
}
