//
//  erodeDilate.metal
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 06.09.24.
//

#include <metal_stdlib>
using namespace metal;

kernel void erosion(texture2d<float, access::read> inTexture [[texture(0)]],
					texture2d<float, access::write> outTexture [[texture(1)]],
					constant uint &kernelSize [[buffer(0)]],
					uint2 gid [[thread_position_in_grid]]) {
	int offset = int(kernelSize) / 2;
	float minVal = FLT_MAX;

	for (int ky = -offset; ky <= offset; ky++) {
		for (int kx = -offset; kx <= offset; kx++) {
			float2 sampleCoord = float2(gid.x + kx, gid.y + ky);
			minVal = min(minVal, inTexture.read(uint2(sampleCoord)).r);
		}
	}

	outTexture.write(minVal, gid);
}

kernel void dilation(texture2d<float, access::read> inTexture [[texture(0)]],
					 texture2d<float, access::write> outTexture [[texture(1)]],
					 constant uint &kernelSize [[buffer(0)]],
					 uint2 gid [[thread_position_in_grid]]) {
	int offset = int(kernelSize) / 2;
	float maxVal = -FLT_MAX;

	for (int ky = -offset; ky <= offset; ky++) {
		for (int kx = -offset; kx <= offset; kx++) {
			float2 sampleCoord = float2(gid.x + kx, gid.y + ky);
			maxVal = max(maxVal, inTexture.read(uint2(sampleCoord)).r);
		}
	}

	outTexture.write(maxVal, gid);
}
