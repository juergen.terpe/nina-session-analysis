//
//  Sobel.metal
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 21.01.25.
//

#include <metal_stdlib>
using namespace metal;

// Kernel-Funktion für die NormalMap-Berechnung
kernel void computeNormalMap(texture2d<float> inputTexture [[ texture(0) ]],
							 texture2d<float, access::write> outputTexture [[ texture(1) ]],
							 constant float2 &textureSize [[ buffer(0) ]],
							 uint2 gid [[ thread_position_in_grid ]]) {
	
	constexpr sampler sample(filter::linear, address::repeat);
	
	if (gid.x >= inputTexture.get_width() || gid.y >= inputTexture.get_height()) {
		return;
	}

	// Texel-Positionen
	float2 texCoord = float2(gid) / textureSize;

	// Sobel-Filter-Kerne
	float3x3 sobelX = float3x3(
		-1.0,  0.0,  1.0,
		-2.0,  0.0,  2.0,
		-1.0,  0.0,  1.0);

	float3x3 sobelY = float3x3(
		-1.0, -2.0, -1.0,
		 0.0,  0.0,  0.0,
		 1.0,  2.0,  1.0);

	// Sample umgebende Texel
	float luminance[3][3];
	for (int y = -1; y <= 1; y++) {
		for (int x = -1; x <= 1; x++) {
			float2 offset = float2(x, y) / textureSize;
			float3 color = inputTexture.sample(sample, texCoord + offset).rgb;
			luminance[y + 1][x + 1] = dot(color, float3(0.299, 0.587, 0.114)); // Luminanz
		}
	}

	// Sobel-Gradienten berechnen
	float gradX = 0.0;
	float gradY = 0.0;
	for (int y = 0; y < 3; y++) {
		for (int x = 0; x < 3; x++) {
			gradX += sobelX[y][x] * luminance[y][x];
			gradY += sobelY[y][x] * luminance[y][x];
		}
	}

	// Normale berechnen
	float3 normal = normalize(float3(-gradX, -gradY, 1.0));
	normal = normal * 0.5 + 0.5; // In den [0, 1]-Bereich bringen

	// In die Ausgabe-Textur schreiben
	outputTexture.write(float4(normal, 1.0), gid);
}
