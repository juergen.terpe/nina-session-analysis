//
//  3DContour.metal
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 07.12.24.
//

#include <metal_stdlib>
using namespace metal;

// Beleuchtungsstruktur
struct Light {
	float3 position;      // Lichtposition im Raum
	float3 color;         // Farbe des Lichts
	float intensity;      // Lichtintensität
	float padding;      // 4 Bytes (manuelles Padding, falls nötig)
};



struct VertexIn {
	float3 position [[attribute(0)]];
	float2 texCoord [[attribute(1)]];
	float3 normal [[attribute(2)]];
};

struct VertexOut {
	float4 position [[position]];
	float2 texCoord;
	float3 normal;
};

vertex VertexOut contour3D_main(VertexIn in [[stage_in]],
								constant float4x4 &viewProjectionMatrix [[buffer(1)]]) {
	VertexOut out;
	out.position = viewProjectionMatrix * float4(in.position, 1.0);
	out.texCoord = in.texCoord;
	out.normal = in.normal;
	return out;
}

fragment float4 contour3Dfragment_main(VertexOut in [[stage_in]],
									   texture2d<float> texture [[texture(0)]],
									   constant Light &light [[buffer(1)]]) {
	constexpr sampler textureSampler(mag_filter::linear, min_filter::linear);
	float4 texColor = texture.sample(textureSampler, in.texCoord);
	
	
	// Normalisieren der Normalen
	float3 normal = normalize(in.normal);
	
	// Lichtvektor berechnen
	float3 lightDir = normalize(light.position - in.position.xyz);
	float diff = max(dot(normal, lightDir), 0.0);
	
	// Spekulare Beleuchtung (optional, für Glanzlichter)
	float3 viewDir = normalize(-in.position.xyz); // Betrachterrichtung
	float3 reflectDir = reflect(-lightDir, normal);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), 16.0) * light.intensity;
	
	float3 ambientColor = float3(0.2, 0.2, 0.2); // Globale minimale Beleuchtung
	
	// Beleuchtung anwenden
	float3 lighting = diff * light.color + spec;
	
	// Endgültige Farbe berechnen
	float3 color = texColor.xyz * lighting;
	
	float3 ambient = ambientColor * texColor.xyz; // Materialfarbe berücksichtigen
	color += ambient; // Zur finalen Farbe hinzufügen
	
	return float4(color, 1.0);
	
}
