//
//  calculateStarFluxAndEccentricityKernel.metal
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 28.07.24.
//

#include <metal_stdlib>
using namespace metal;

kernel void calculateFluxShapeAndPSF(texture2d<ushort, access::read> inTexture [[texture(0)]],
									 device float *output [[buffer(0)]],
									 constant int &width [[buffer(1)]],
									 constant int &height [[buffer(2)]],
									 constant int2 &position [[buffer(3)]],
									 constant int &apertureRadius [[buffer(4)]],
									 uint2 gid [[thread_position_in_grid]]) {
	if (gid.x >= inTexture.get_width() || gid.y >= inTexture.get_height()) {
		return;
	}

	float xSum = 0.0;
	float ySum = 0.0;
	float xxSum = 0.0;
	float yySum = 0.0;
	float xySum = 0.0;
	float totalFlux = 0.0;
	float psf[256]; // Assuming max aperture size
	int count = 0;

	for (int dy = -apertureRadius; dy <= apertureRadius; dy++) {
		for (int dx = -apertureRadius; dx <= apertureRadius; dx++) {
			int2 offset = int2(dx, dy);
			int2 coord = position + offset;

			if (coord.x >= 0 && coord.y >= 0 && coord.x < width && coord.y < height) {
				ushort lum = inTexture.read(uint2(coord)).r;
				float pixelValue = float(lum) / 65535.0;
				psf[count++] = pixelValue;
				xSum += pixelValue * float(dx);
				ySum += pixelValue * float(dy);
				xxSum += pixelValue * float(dx * dx);
				yySum += pixelValue * float(dy * dy);
				xySum += pixelValue * float(dx * dy);
				totalFlux += pixelValue;
			}
		}
	}

	if (totalFlux == 0.0) {
		output[0] = 0.0;
		output[1] = 0.0;
		output[2] = 0.0;
		for (int i = 3; i < 259; i++) {
			output[i] = 0.0;
		}
		return;
	}

	float fluxShape = (xxSum + yySum) / (totalFlux * float(apertureRadius * apertureRadius));
	float eccentricity = sqrt((xxSum - yySum) * (xxSum - yySum) + 4.0 * xySum * xySum) / (xxSum + yySum);

	output[0] = fluxShape;
	output[1] = eccentricity;
	output[2] = totalFlux;
	for (int i = 0; i < count; i++) {
		output[3 + i] = psf[i];
	}
}
