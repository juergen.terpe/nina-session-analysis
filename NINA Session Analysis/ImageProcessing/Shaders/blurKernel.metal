//
//  blurKernel.metal
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 18.09.24.
//

#include <metal_stdlib>
using namespace metal;

kernel void blurKernel(device const float* inImage [[buffer(0)]],
					   device float* outImage [[buffer(1)]],
					   constant int& kernelSize [[buffer(2)]],
					   constant int& width [[buffer(3)]],
					   constant int& height [[buffer(4)]],
					   uint2 gid [[thread_position_in_grid]]) {
	// Berechne das Offset basierend auf kernelSize
	int offset = kernelSize / 2;

	// Umwandeln von gid.x und gid.y in vorzeichenbehaftete ints
	int gidX = int(gid.x);
	int gidY = int(gid.y);

	// Nur innerhalb der Bildgrenzen operieren
	if (gidX >= offset && gidX < (width - offset) && gidY >= offset && gidY < (height - offset)) {
		float sum = 0.0;
		float weightSum = 0.0;

		// Standardabweichung für den Gauss-Filter
		float sigma = float(offset) / 2.0;
		float twoSigmaSquared = 2.0 * sigma * sigma;

		// Schleifen über das Kernel-Fenster
		for (int ky = -offset; ky <= offset; ++ky) {
			for (int kx = -offset; kx <= offset; ++kx) {
				// Berechne den Index des Nachbarpixels
				int neighborX = gidX + kx;
				int neighborY = gidY + ky;

				// Sicherstellen, dass wir nur innerhalb der Bildgrenzen operieren
				if (neighborX >= 0 && neighborX < width && neighborY >= 0 && neighborY < height) {
					// Berechnung des Abstands zum Kernel-Zentrum
					float distanceSquared = float(kx * kx + ky * ky);

					// Gewicht basierend auf einer Gaußfunktion
					float weight = exp(-distanceSquared / twoSigmaSquared);

					// Summiere gewichtete Pixel
					sum += inImage[neighborY * width + neighborX] * weight;
					weightSum += weight;
				}
			}
		}

		// Mittelwert basierend auf den Gewichten berechnen
		outImage[gidY * width + gidX] = sum / weightSum;
	} else {
		// Falls außerhalb des bearbeitbaren Bereichs, setzen wir den ursprünglichen Wert
		outImage[gid.y * width + gid.x] = inImage[gid.y * width + gid.x];
	}
}
