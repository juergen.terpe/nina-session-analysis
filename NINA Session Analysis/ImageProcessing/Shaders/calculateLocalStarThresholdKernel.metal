//
//  calculateLocalStarThresholdKernel.metal
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 28.07.24.
//

#include <metal_stdlib>
using namespace metal;

kernel void calculateLocalThreshold(texture2d<ushort, access::read> inTexture [[texture(0)]],
									device float *output [[buffer(0)]],
									constant int &width [[buffer(1)]],
									constant int &height [[buffer(2)]],
									constant int2 &position [[buffer(3)]],
									constant int &radius [[buffer(4)]],
									uint2 gid [[thread_position_in_grid]]) {
	if (gid.x >= inTexture.get_width() || gid.y >= inTexture.get_height()) {
		return;
	}

	const int maxSampleCount = 1024;
	float pixelValues[maxSampleCount];
	int count = 0;

	for (int dy = -radius; dy <= radius; dy++) {
		for (int dx = -radius; dx <= radius; dx++) {
			int2 offset = int2(dx, dy);
			int2 coord = position + offset;

			if (coord.x >= 0 && coord.y >= 0 && coord.x < width && coord.y < height) {
				float distance = length(float2(dx, dy));
				if (distance <= float(radius)) {
					ushort lum = inTexture.read(uint2(coord)).r;
					float pixelValue = float(lum) / 65535.0;
					if (count < maxSampleCount) {
						pixelValues[count++] = pixelValue;
					}
				}
			}
		}
	}

	// Sort pixel values
	for (int i = 0; i < count - 1; i++) {
		for (int j = i + 1; j < count; j++) {
			if (pixelValues[i] > pixelValues[j]) {
				float temp = pixelValues[i];
				pixelValues[i] = pixelValues[j];
				pixelValues[j] = temp;
			}
		}
	}

	// Calculate median
	float median = 0.0;
	if (count % 2 == 0) {
		median = (pixelValues[count / 2] + pixelValues[count / 2 - 1]) / 2.0;
	} else {
		median = pixelValues[count / 2];
	}

	output[0] = median;
}
