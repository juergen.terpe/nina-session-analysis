//
//  TiledImageShaders.metal
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 27.07.24.
//

#include <metal_stdlib>
using namespace metal;

struct VertexIn {
	float4 position [[attribute(0)]];
	float2 texCoord [[attribute(1)]];
};

struct VertexOut {
	float4 position [[position]];
	float2 texCoord;
};

vertex VertexOut tiledVertex(VertexIn in [[stage_in]],
							 constant uint2 &tileIndex [[buffer(1)]],
							 constant uint2 &gridSize [[buffer(2)]]) {
	VertexOut out;
	out.position = in.position;

	// Calculate tile offset
	float2 tileOffset = float2(tileIndex) / float2(gridSize);

	// Scale texCoord based on the gridSize and add the offset
	out.texCoord = in.texCoord / float2(gridSize) + tileOffset;
	
	return out;
}

fragment float4 tiledFragment(VertexOut in [[stage_in]],
							  texture2d<float> texture [[texture(0)]],
							  constant float &contrast [[buffer(0)]]) {
	constexpr sampler textureSampler (mag_filter::linear, min_filter::linear);
	float4 color = texture.sample(textureSampler, in.texCoord);
	
	// Apply contrast adjustment
	color.rgb = ((color.rgb - 0.5) * max(contrast, 0.0) + 0.5);
	
	return color;
}
