//
//  mtfStretch.metal
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 24.07.24.
//

#include <metal_stdlib>
using namespace metal;

struct Params {
	uint2 imageSize;
	float3 stretch;
	float3 shadows;
	int bitDepth;
};

struct ParamsGray {
	uint2 imageSize;
	float stretch;
	float shadows;
	int bitDepth;
};

inline float mtf(float stretch, float x) {
	if (x > 0.0) {
		if (x < 1.0) {
			return min(1.0, (stretch - 1.0) * x / ((2.0 * stretch - 1.0) * x - stretch));
		}
		return 1.0;
	}
	return 0.0;
}

inline float normalize(float val, float scale) {
	return max(0.0, min(1.0, val / scale));
}

inline float denormalize(float val) {
	return max(0.0, min(65535.0, val * 65535.0));
}

kernel void mtfStretchKernel(texture2d<ushort, access::read> inTexture [[ texture(1) ]],
							 texture2d<ushort, access::write> outTexture [[ texture(2) ]],
							 constant Params &params [[ buffer(0) ]],
							 uint2 gid [[ thread_position_in_grid ]]) {
	if (gid.x >= params.imageSize.x || gid.y >= params.imageSize.y) return;

	ushort4 pixel = inTexture.read(gid);
	float scale = (float)((1 << params.bitDepth));

	ushort r = ushort( denormalize(mtf(params.stretch.x, normalize(float(pixel.r), scale) - params.shadows.x)) );
	ushort g = ushort( denormalize(mtf(params.stretch.y, normalize(float(pixel.g), scale) - params.shadows.y)) );
	ushort b = ushort( denormalize(mtf(params.stretch.z, normalize(float(pixel.b), scale) - params.shadows.z)) );

	outTexture.write(ushort4(r, g, b, 65535), gid);
}


kernel void mtfStretchGrayKernel(texture2d<ushort, access::read> inTexture [[ texture(1) ]],
							 texture2d<ushort, access::write> outTexture [[ texture(2) ]],
							 constant ParamsGray &params [[ buffer(0) ]],
							 uint2 gid [[ thread_position_in_grid ]]) {
	if (gid.x >= params.imageSize.x || gid.y >= params.imageSize.y) return;

	ushort4 pixel = inTexture.read(gid);
	float scale = (float)((1 << params.bitDepth));
	ushort r = ushort( denormalize(mtf(params.stretch, normalize(float(pixel.r), scale) - params.shadows)) );
	outTexture.write(ushort(r), gid);
}



kernel void linearFitKernel(texture2d<ushort, access::read> inTexture [[ texture(1) ]],
							 texture2d<ushort, access::write> outTexture [[ texture(2) ]],
							 constant float3 &offsets [[ buffer(0) ]],
							 uint2 gid [[ thread_position_in_grid ]]) {
	
	ushort4 pixel = inTexture.read(gid);

	ushort r = ushort( (float(pixel.r)/65535.0 + offsets.x)*65535.0 );
	ushort g = ushort( (float(pixel.g)/65535.0 + offsets.y)*65535.0 );
	ushort b = ushort( (float(pixel.b)/65535.0 + offsets.z)*65535.0 );

	outTexture.write(ushort4(r, g, b, 65535), gid);
}
