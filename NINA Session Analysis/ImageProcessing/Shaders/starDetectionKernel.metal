//
//  starDetectionKernel.metal
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 28.07.24.
//
#include <metal_stdlib>
using namespace metal;

kernel void starDetection(texture2d<ushort, access::read> inTexture [[texture(0)]],
						device int *output [[buffer(0)]],
						constant float &median [[buffer(1)]],
						constant float &stddev [[buffer(2)]],
						uint2 gid [[thread_position_in_grid]]) {
	if (gid.x >= inTexture.get_width() || gid.y >= inTexture.get_height()) {
		return;
	}

	int width = inTexture.get_width();
	int height = inTexture.get_height();
	ushort lum = inTexture.read(gid).r;
	float pixelValue = float(lum) / 65535.0;
	
	// Calculate the threshold
	float threshold = median + stddev * 2.0;

	// Check if the pixel is above the threshold
	if (pixelValue > threshold) {
		int2 position = int2(gid);
		int2 center = position;
		bool foundMaximum = false;
		int maxIterations = 10;  // Maximum number of iterations to prevent infinite loop

		for (int iter = 0; iter < maxIterations && !foundMaximum; iter++) {
			foundMaximum = true;
			for (int dy = -1; dy <= 1; dy++) {
				for (int dx = -1; dx <= 1; dx++) {
					int2 neighborPos = center + int2(dx, dy);
					if (neighborPos.x >= 0 && neighborPos.y >= 0 && neighborPos.x < width && neighborPos.y < height) {
						ushort neighborLum = inTexture.read(uint2(neighborPos)).r;
						float neighborValue = float(neighborLum) / 65535.0;
						if (neighborValue > pixelValue) {
							pixelValue = neighborValue;
							center = neighborPos;
							foundMaximum = false;
						}
					}
				}
			}
		}

		// Check if the current pixel is the local maximum
		if (center.x == position.x && center.y == position.y) {
			// Mark the center of the star
			for (int i = 1; i < output[0] * 2; i += 2) {
				if (output[i] == -1) {
					output[i] = int(center.x);
					output[i + 1] = int(center.y);
					break;
				}
			}
		}
	}
}


/*
kernel void starDetection(texture2d<ushort, access::read> inTexture [[texture(0)]],
						  device int *output [[buffer(0)]],
						  constant float &median [[buffer(1)]],
						  constant float &stddev [[buffer(2)]],
						  uint2 gid [[thread_position_in_grid]]) {
	if (gid.x >= inTexture.get_width() || gid.y >= inTexture.get_height()) {
		return;
	}

	ushort pixelValue = inTexture.read(gid).r;
	float brightness = float(pixelValue) / 65535.0; // Normalize to [0, 1] range

	// Threshold based on median and standard deviation
	float threshold = median + 2.0 * stddev;

	if (brightness > threshold) {
		bool hasBrightNeighbor = false;
		int neighborCount = 0;

		for (int y = -1; y <= 1; y++) {
			for (int x = -1; x <= 1; x++) {
				if (x == 0 && y == 0) continue;

				uint2 neighborPos = uint2(gid.x + x, gid.y + y);
				if (neighborPos.x < inTexture.get_width() && neighborPos.y < inTexture.get_height()) {
					ushort neighborPixelValue = inTexture.read(neighborPos).r;
					float neighborBrightness = float(neighborPixelValue) / 65535.0;
					if (neighborBrightness > brightness) {
						hasBrightNeighbor = true;
					}
					if (neighborBrightness > threshold) {
						neighborCount++;
					}
				}
			}
		}

		if (neighborCount >= 2 && !hasBrightNeighbor) {
			// Detected a star, write its (x, y) position into the output buffer
			for (int i = 1; i < output[0] * 2; i += 2) {
				if (output[i] == -1) {
					output[i] = int(gid.x);
					output[i + 1] = int(gid.y);
					break;
				}
			}
		}
	}
}*/
