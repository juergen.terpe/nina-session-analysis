//
//  extractChannels.metal
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 11.07.24.
//
#include <metal_stdlib>
using namespace metal;

struct ImageParams {
	uint width;
	uint height;
	uint bytesPerPixel;
	uint bitsPerComponent;
};

kernel void extractChannels(constant uint8_t *imageData [[buffer(0)]],
							device uint16_t *channelR [[buffer(1)]],
							device uint16_t *channelG [[buffer(2)]],
							device uint16_t *channelB [[buffer(3)]],
							constant ImageParams &params [[buffer(4)]],
							uint2 gid [[thread_position_in_grid]]) {
	if (gid.x >= params.width || gid.y >= params.height) return;

	uint index = gid.y * params.width + gid.x;
	uint offset = index * params.bytesPerPixel;

	if (params.bitsPerComponent == 8) {
		channelR[index] = uint16_t(float(imageData[offset]) / 255.0 * 65535.0);
		channelG[index] = uint16_t(float(imageData[offset + 1]) / 255.0 * 65535.0);
		channelB[index] = uint16_t(float(imageData[offset + 2]) / 255.0 * 65535.0);
	} else if (params.bitsPerComponent == 16) {
		constant uint16_t *pixelData = (constant uint16_t *)imageData;
		channelR[index] = pixelData[offset / 2];
		channelG[index] = pixelData[offset / 2 + 1];
		channelB[index] = pixelData[offset / 2 + 2];
	} else if (params.bitsPerComponent == 32) {
		constant uint32_t *pixelData = (constant uint32_t *)imageData;
		channelR[index] = uint16_t(float(pixelData[offset / 4]) / 4294967295.0 * 65535.0);
		channelG[index] = uint16_t(float(pixelData[offset / 4 + 1]) / 4294967295.0 * 65535.0);
		channelB[index] = uint16_t(float(pixelData[offset / 4 + 2]) / 4294967295.0 * 65535.0);
	}
}


