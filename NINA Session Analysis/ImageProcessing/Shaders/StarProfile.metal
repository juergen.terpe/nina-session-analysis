#include <metal_stdlib>
using namespace metal;

struct Uniforms {
	float4x4 modelMatrix;
	float4x4 viewMatrix;
	float4x4 projectionMatrix;
	float4x4 mvp;
};

struct VertexOut {
	float4 position [[position]];
	float height;
	float2 uv;
};

struct ControlPoint {
	float4 position [[attribute(0)]];
};

typedef struct {
	vector_float2 size;
	float height;
	uint maxTessellation;
} Terrain;

float getHeight(float3 rgb) {
	return 0.2126 * rgb.x + 0.7152 * rgb.y + 0.0722 * rgb.z;
}

float3 computeLighting(float3 normal, float3 lightDir, float3 lightColor) {
	float diff = max(dot(normalize(normal), normalize(lightDir)), 0.0);
	float3 ambient = float3(0.1, 0.1, 0.1); // Konstantes Umgebungslicht
	return ambient + lightColor * diff;
}

[[patch(quad, 4)]]
vertex VertexOut starProfileVertex(patch_control_point<ControlPoint> control_points [[stage_in]],
									constant Uniforms &uniforms [[buffer(11)]],
									float2 patch_coord [[position_in_patch]],
									texture2d<float> heightMap [[texture(0)]],
									constant Terrain &terrain [[buffer(6)]]) {
	float u = patch_coord.x;
	float v = patch_coord.y;

	float2 top = mix(
		control_points[0].position.xz,
		control_points[1].position.xz,
		u);
	float2 bottom = mix(
		control_points[3].position.xz,
		control_points[2].position.xz,
		u);

	VertexOut out;
	float2 interpolated = mix(top, bottom, v);
	float4 position = float4(interpolated.x, 0.0, interpolated.y, 1.0);

	float2 xy = (position.xz + terrain.size / 2.0) / terrain.size;
	constexpr sampler sample;
	float4 color = heightMap.sample(sample, xy);
	float height = getHeight(color.rgb) * terrain.height;
	position.y = height;

	out.position = uniforms.mvp * position;
	out.uv = xy;
	out.height = height;
	
	return out;
}

fragment float4 starProfileFragment(VertexOut in [[stage_in]],
									texture2d<float> starTexture [[texture(1)]],
									texture2d<float> normalMap [[texture(2)]]) {
	constexpr sampler sample(filter::linear, address::repeat);

	// Lichtparameter
	float3 lightDir = normalize(float3(-2.0, 2.0, 4));
	float3 lightColor = float3(1.0, 1.0, 1.0);

	// Normale aus der NormalMap lesen
	float3 normalFromMap = normalMap.sample(sample, in.uv).xyz * 2.0 - 1.0; // [0, 1] -> [-1, 1]

	// Beleuchtung berechnen
	float3 lighting = computeLighting(normalFromMap, lightDir, lightColor);

	// Farbe der Textur lesen
	float4 color = starTexture.sample(sample, in.uv);

	// Beleuchtung anwenden
	float3 finalColor = color.rgb * lighting; // Konstante Basisbeleuchtung
	return float4(finalColor, color.a);
}
