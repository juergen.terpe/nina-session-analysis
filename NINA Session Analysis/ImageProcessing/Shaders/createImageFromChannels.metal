//
//  createImageFromChnannels.metal
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 11.07.24.
//
#include <metal_stdlib>
using namespace metal;

struct ImageParams {
	uint width;
	uint height;
};

kernel void createImageFromChannels(texture2d<half, access::write> outTexture [[texture(0)]],
						 constant uint16_t *channelR [[buffer(0)]],
						 constant uint16_t *channelG [[buffer(1)]],
						 constant uint16_t *channelB [[buffer(2)]],
						 constant ImageParams &params [[buffer(3)]],
						 uint2 gid [[thread_position_in_grid]]) {
	if (gid.x >= params.width || gid.y >= params.height) return;
	
	uint index = gid.y * params.width + gid.x;
	half4 color;
	
	if (channelG != nullptr && channelB != nullptr) {
		color = half4(half(channelR[index]) / 65535.0,
					  half(channelG[index]) / 65535.0,
					  half(channelB[index]) / 65535.0,
					  1.0);
	} else {
		half mono = half(channelR[index]) / 65535.0;
		color = half4(mono, mono, mono, 1.0);
	}
	
	outTexture.write(color, gid);
}
