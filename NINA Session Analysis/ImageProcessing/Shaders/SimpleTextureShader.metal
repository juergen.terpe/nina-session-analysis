//
//  SimpleTextureShader.metal
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 11.12.24.
//

#include <metal_stdlib>
using namespace metal;

struct Vertex {
	float3 position [[attribute(0)]];
	float3 normal [[attribute(1)]];
	float2 texCoords [[attribute(2)]];
};

struct VertexOut {
	float4 position [[position]];
	float2 texCoords;
	float3 normal;
};

struct Uniforms {
	float4x4 mvpMatrix;
};

vertex VertexOut vertex_main(Vertex in [[stage_in]],
							 constant Uniforms &uniforms [[buffer(1)]]) {
	VertexOut out;
	out.position = uniforms.mvpMatrix * float4(in.position, 1.0);
	out.texCoords = in.texCoords;
	out.normal = normalize(in.normal);
	return out;
}

fragment float4 fragment_main(VertexOut in [[stage_in]],
							  texture2d<float> texture [[texture(0)]]) {
	constexpr sampler s(address::clamp_to_edge, filter::linear);
	return texture.sample(s, in.texCoords);
	/*
	// Basic diffuse lighting
	float3 lightDir = normalize(float3(0.5, 1.0, 1.0));
	float diff = max(dot(in.normal, lightDir), 0.0);
	return float4(color.rgb * diff, color.a);*/
}
