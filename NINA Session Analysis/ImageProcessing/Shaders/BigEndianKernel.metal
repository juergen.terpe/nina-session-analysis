//
//  BigEndianKernel.metal
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 25.07.24.
//

#include <metal_stdlib>
using namespace metal;

kernel void bigEndianKernel(texture2d<ushort, access::read> inTexture [[texture(0)]],
							   texture2d<ushort, access::write> outTexture [[texture(1)]],
							   uint2 gid [[thread_position_in_grid]]) {
	if (gid.x >= inTexture.get_width() || gid.y >= inTexture.get_height()) {
		return;
	}

	ushort4 pixel = inTexture.read(gid);

	// Convert each channel to Big-Endian
	pixel.r = (pixel.r << 8) | (pixel.r >> 8);
	pixel.g = (pixel.g << 8) | (pixel.g >> 8);
	pixel.b = (pixel.b << 8) | (pixel.b >> 8);
	pixel.a = (pixel.a << 8) | (pixel.a >> 8);

	outTexture.write(pixel, gid);
}
