//
//  histogramKernel.metal
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 24.07.24.
//

#include <metal_stdlib>
using namespace metal;

struct Params {
	uint2 imageSize;
	int bitDepth;
};

inline float normalize(float val, int bitDepth) {
	return val / (float)((1 << bitDepth));
}

inline float denormalize(float val) {
	return (val * 65535.0 );
}


inline ushort renormalize(ushort val, int bitDepth) {
	return denormalize(normalize(float(val), bitDepth));
}

kernel void histogramKernel(constant Params &params [[ buffer(0) ]],
							texture2d<ushort, access::read> inTexture [[ texture(0) ]],
							device atomic_uint *histogramR [[ buffer(1) ]],
							device atomic_uint *histogramG [[ buffer(2) ]],
							device atomic_uint *histogramB [[ buffer(3) ]],
							uint2 gid [[ thread_position_in_grid ]]) {
	if (gid.x >= params.imageSize.x || gid.y >= params.imageSize.y) {
		return;
	}

	ushort4 pixel = inTexture.read(gid);

	atomic_fetch_add_explicit(&histogramR[renormalize(pixel.r, params.bitDepth)], 1, memory_order_relaxed);
	atomic_fetch_add_explicit(&histogramG[renormalize(pixel.g, params.bitDepth)], 1, memory_order_relaxed);
	atomic_fetch_add_explicit(&histogramB[renormalize(pixel.b, params.bitDepth)], 1, memory_order_relaxed);
}


kernel void histogramGrayscaleKernel(constant Params &params [[ buffer(0) ]],
							texture2d<ushort, access::read> inTexture [[ texture(0) ]],
							device atomic_uint *histogram [[ buffer(1) ]],
							uint2 gid [[ thread_position_in_grid ]]) {
	if (gid.x >= params.imageSize.x || gid.y >= params.imageSize.y) {
		return;
	}

	ushort4 pixel = inTexture.read(gid);

	atomic_fetch_add_explicit(&histogram[renormalize(pixel.r, params.bitDepth)], 1, memory_order_relaxed);
}
