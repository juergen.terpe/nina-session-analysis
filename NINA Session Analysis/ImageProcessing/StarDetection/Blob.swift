//
//  Blob.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 11.08.24.
//

import Foundation
import CoreGraphics

final class Blob: Sendable {
	let centroid: CGPoint
	let minX: Int
	let minY: Int
	let maxX: Int
	let maxY: Int
	let pixels: [(x:Int, y:Int)]
	let flux: Float
	let hfd: Float
	let luminanceValues: [Float]
	let threshold: Float
	let snr: Float
	let background: Float
	
	init(centroid: CGPoint, minX: Int, minY: Int, maxX: Int, maxY: Int, pixels: [(x: Int, y: Int)],
		 flux: Float, hfd: Float, luminanceValues: [Float], threshold: Float, snr: Float, background: Float) {
		self.centroid = centroid
		self.minX = minX
		self.minY = minY
		self.maxX = maxX
		self.maxY = maxY
		self.pixels = pixels
		self.flux = flux
		self.hfd = hfd
		self.luminanceValues = luminanceValues
		self.threshold = threshold
		self.snr = snr
		self.background = background
	}
	
	
	var boundingBox: CGRect {
		return CGRect(x: minX, y: minY, width: maxX - minX + 1, height: maxY - minY + 1)
	}
	
	var radius: Float {
		let dx = maxX - minX
		let dy = maxY - minY
		let maxDistance = 1.0 + sqrt(Float(dx * dx + dy * dy))
		return maxDistance / 2.0
	}
	
	
	// Berechnet die Exzentrizität basierend auf den Hauptachsen
	func eccentricity() -> (ecc: Float, majorAxis: Float, minorAxis: Float)? {
		guard let (majorAxis, minorAxis) = axes() else {
			return nil
		}
		let e = sqrt(1 - (minorAxis * minorAxis) / (majorAxis * majorAxis))
		
		return (ecc: e, majorAxis: majorAxis, minorAxis: minorAxis)
	}
	
	// Berechnet die große und kleine Halbachse der Sternellipse
	private func axes() -> (Float, Float)? {
		var xx: Float = 0, yy: Float = 0, xy: Float = 0
		
		for (index, (x, y)) in pixels.enumerated() {
			let dx = Float(x) - Float(centroid.x)
			let dy = Float(y) - Float(centroid.y)
			let weight = luminanceValues[index]
			
			xx += dx * dx * weight
			yy += dy * dy * weight
			xy += dx * dy * weight
		}
		
		let a = xx + yy
		let b = sqrt((xx - yy) * (xx - yy) + 4 * xy * xy)
		
		let majorAxis = sqrt((a + b) / (2 * self.flux))
		let minorAxis = sqrt((a - b) / (2 * self.flux))
		
		return (majorAxis, minorAxis)
	}
	
	
	func semiMajorAxisDirection() -> CGVector {
		let n = CGFloat(pixels.count)
		
		// Covariance matrix components
		var sumXX: CGFloat = 0, sumXY: CGFloat = 0, sumYY: CGFloat = 0
		
		for (x, y) in pixels {
			let dx = CGFloat(x) - centroid.x
			let dy = CGFloat(y) - centroid.y
			sumXX += dx * dx
			sumXY += dx * dy
			sumYY += dy * dy
		}
		
		// Covariance matrix:
		// | sumXX  sumXY |
		// | sumXY  sumYY |
		let covarianceMatrix = [
			[sumXX / n, sumXY / n],
			[sumXY / n, sumYY / n]
		]
		
		// Eigenvalue decomposition to get the directions
		// (We need to compute the eigenvector of the largest eigenvalue)
		let trace = covarianceMatrix[0][0] + covarianceMatrix[1][1]
		let determinant = covarianceMatrix[0][0] * covarianceMatrix[1][1] - covarianceMatrix[0][1] * covarianceMatrix[1][0]
		let eigenValue1 = trace / 2 + sqrt(pow(trace / 2, 2) - determinant)
		let eigenValue2 = trace / 2 - sqrt(pow(trace / 2, 2) - determinant)
		
		let majorAxisVector: CGVector
		
		if eigenValue1 > eigenValue2 {
			majorAxisVector = CGVector(dx: covarianceMatrix[0][1], dy: eigenValue1 - covarianceMatrix[0][0])
		} else {
			majorAxisVector = CGVector(dx: covarianceMatrix[0][1], dy: eigenValue2 - covarianceMatrix[0][0])
		}
		
		return majorAxisVector
	}
	
	
	func calculateMoments() -> (mu20: Double, mu02: Double, mu11: Double) {
		var xMean: Double = 0.0
		var yMean: Double = 0.0
		let n = Double(pixels.count)
		
		// Berechne den Schwerpunkt (xMean, yMean)
		for pixel in self.pixels {
			xMean += Double(pixel.x)
			yMean += Double(pixel.y)
		}
		xMean /= n
		yMean /= n
		
		// Berechne die zweiten Momente
		var mu20: Double = 0.0
		var mu02: Double = 0.0
		var mu11: Double = 0.0
		
		for pixel in self.pixels {
			let xDiff = Double(pixel.x) - xMean
			let yDiff = Double(pixel.y) - yMean
			mu20 += xDiff * xDiff
			mu02 += yDiff * yDiff
			mu11 += xDiff * yDiff
		}
		
		return (mu20, mu02, mu11)
	}
	
	private func findEndPoints(orientation: Double) -> (CGPoint, CGPoint) {
		var minProjection: Double = Double.greatestFiniteMagnitude
		var maxProjection: Double = -Double.greatestFiniteMagnitude
		var minPoint = CGPoint.zero
		var maxPoint = CGPoint.zero
		
		// Vektor entlang der Hauptachse
		let axisVector = CGPoint(x: cos(orientation), y: sin(orientation))
		
		// Projektion der Pixel auf die Hauptachse und Bestimmung der Extrempunkte
		for pixel in self.pixels {
			let projection = Double(pixel.x) * Double(axisVector.x) + Double(pixel.y) * Double(axisVector.y)
			if projection < minProjection {
				minProjection = projection
				minPoint = CGPoint(x: pixel.x, y: pixel.y)
			}
			if projection > maxProjection {
				maxProjection = projection
				maxPoint = CGPoint(x: pixel.x, y: pixel.y)
			}
		}
		
		return (minPoint, maxPoint)
	}
	
	func isSatellite() -> Bool {
		let moments = calculateMoments()
		let orientation = atan2(2 * moments.mu11, moments.mu20 - moments.mu02) / 2.0
		
		/*
		let λ1 = (moments.mu20 + moments.mu02) / 2.0 + sqrt(pow((moments.mu20 - moments.mu02) / 2.0, 2) + pow(moments.mu11, 2))
		let λ2 = (moments.mu20 + moments.mu02) / 2.0 - sqrt(pow((moments.mu20 - moments.mu02) / 2.0, 2) + pow(moments.mu11, 2))
		let ecc = λ2 > 0 ? λ1 / λ2 : 1.0
		*/
		 
		let (minPoint, maxPoint) = findEndPoints(orientation: orientation)
		let bbox = self.boundingBox
		
		return isPointOnEdge(minPoint, in: bbox) && isPointOnEdge(maxPoint, in: bbox)
	}
	private func isPointOnEdge(_ point: CGPoint, in bbox: CGRect) -> Bool {
		// Prüfe, ob der Punkt auf einer der vier Kanten der Bounding Box liegt
		let onEdge = abs(point.x - bbox.minX) < 1.0
				  || abs(point.x - bbox.maxX) < 1.0
				  || abs(point.y - bbox.minY) < 1.0
				  || abs(point.y - bbox.maxY) < 1.0
		
		// Prüfe, ob der Punkt auf einer der vier Ecken der Bounding Box liegt
		let onCorner = (abs(point.x - bbox.minX) < 1.0 && abs(point.y - bbox.minY) < 1.0)
					|| (abs(point.x - bbox.minX) < 1.0 && abs(point.y - bbox.maxY) < 1.0)
					|| (abs(point.x - bbox.maxX) < 1.0 && abs(point.y - bbox.minY) < 1.0)
					|| (abs(point.x - bbox.maxX) < 1.0 && abs(point.y - bbox.maxY) < 1.0)

		return onEdge || onCorner
	}

}
