//
//  StarDetector.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 11.08.24.
//
import CoreImage
import Accelerate

class StarDetector {
	
	func process(_ cgImage: CGImage) async -> [DetectedStar] {
		if let luminance = cgImage.extractLuminanceFromCGImage() {
			let blurredLuminance = blur(luminance, kernelSize: 3)
			
			let threshold = calculateThreshold(from: blurredLuminance)
			
			let blobCounter = BlobCounter(width: cgImage.width, height: cgImage.height, threshold: threshold)
			let blobs = blobCounter.findBlobs(in: blurredLuminance)
			
			if blobs.isNotEmpty {
				return await processBlobs(blobs: blobs, threshold: threshold)
			}
		}
		return []
	}
	
	private func blur(_ image: [[Float]], kernelSize: Int) -> [[Float]] {
		
		if let blur = Blur() {
			return blur.process(image, kernelSize: kernelSize)
		} else {
		
			let height = image.count
			let width = image[0].count
			var result = Array<[Float]>(repeating: Array<Float>(repeating: 0.0, count: width), count: height)
			
			let offset = kernelSize / 2
			let kernelSizeSq = kernelSize * kernelSize
			
			var block = [Float](repeating: Float.greatestFiniteMagnitude, count: kernelSizeSq)
			
			for y in offset..<height-offset {
				for x in offset..<width-offset {
					var index = 0
					
					for ky in -offset...offset {
						for kx in -offset...offset {
							block[index] = image[y + ky][x + kx]
							index += 1
						}
					}
					
					var meanValue: Float = 0
					vDSP_meanv(block, 1, &meanValue, vDSP_Length(kernelSizeSq))
					result[y][x] = meanValue
				}
			}
			
			return result
		}
	}
	
	private func calculateHistogram(from luminanceArray: [Float]) -> [Int] {
		let maxLuminance = 65535
		var histogram = [Int](repeating: 0, count: maxLuminance + 1)
		
		for luminance in luminanceArray {
			let index = Int(luminance * 65535.0)
			histogram[index] += 1
		}
		
		return histogram
	}

	private func calculateMedian(from histogram: [Int], totalPixels: Int) -> Float {
		let halfTotalPixels = totalPixels / 2
		var cumulativeSum = 0
		
		for (index, count) in histogram.enumerated() {
			cumulativeSum += count
			if cumulativeSum >= halfTotalPixels {
				return Float(index)/65535.0
			}
		}
		
		return 0 // Fallback, sollte theoretisch nie erreicht werden
	}

	private func calculateMAD(from luminanceArray: [Float], median: Float) -> Float {
		let maxDeviation = 65535
		var deviationHistogram = [Int](repeating: 0, count: maxDeviation + 1)
		
		let lumMedian = Int(median * 65535.0)
		
		for luminance in luminanceArray {
			let deviation = abs(Int(luminance * 65535.0) - lumMedian)
			deviationHistogram[deviation] += 1
		}
		
		let totalPixels = luminanceArray.count
		let halfTotalPixels = totalPixels / 2
		var cumulativeSum = 0
		
		for (index, count) in deviationHistogram.enumerated() {
			cumulativeSum += count
			if cumulativeSum >= halfTotalPixels {
				return Float(index)/65535.0
			}
		}
		
		return 0.0 // Fallback, sollte theoretisch nie erreicht werden
	}

	private func calculateThreshold(from luminance: [[Float]]) -> Float {
		let luminanceArray = luminance.flatMap { $0 }
		
		let histogram = calculateHistogram(from: luminanceArray)
		
		let totalPixels = luminanceArray.count
		let median = calculateMedian(from: histogram, totalPixels: totalPixels)
		let mad = calculateMAD(from: luminanceArray, median: median)
		
		let k: Float = 3.0
		return Float(median) + k * mad
	}
	
	func filterDuplicateStars(from stars: [DetectedStar]) -> [DetectedStar] {
		var uniqueStars = [DetectedStar]()
		
		// Sortiere die Sterne nach Flux in absteigender Reihenfolge
		let sortedStars = stars.sorted { $0.flux > $1.flux }
		
		// Iteriere durch die sortierten Sterne und füge nur einzigartige hinzu
		for star in sortedStars {
			var isUnique = true
			
			for uniqueStar in uniqueStars {
				if star.overlaps(with: uniqueStar), star.isDuplicate(of: uniqueStar) {
					isUnique = false
					break
				}
			}
			
			if isUnique {
				uniqueStars.append(star)
			}
		}
		
		return uniqueStars
	}
		
	private func processBlobs(blobs: [Blob], threshold: Float) async -> [DetectedStar] {
		var detectedStars = [DetectedStar]()
		
		await withTaskGroup(of: DetectedStar?.self) { taskGroup in
			for blob in blobs {
				taskGroup.addTask { [self] in
					return await processBlob(blob)
				}
			}
			
			for await detectedStar in taskGroup {
				if let star = detectedStar {
					detectedStars.append(star)
				}
			}
		}
		
		return filter(from: detectedStars).sorted(by: { $0.hfd > $1.hfd })
	}

	func filterSatelliteTrails(from stars: [DetectedStar]) -> [DetectedStar] {
		return stars.filter { !$0.isSatelliteTrail() }
	}

	private func filter(from stars: [DetectedStar]) -> [DetectedStar] {
		return filterSatelliteTrails(from: filterDuplicateStars(from: stars))
	}
		
	func processBlob(_ blob: Blob) async -> DetectedStar? {
		
		let boundingBox = blob.boundingBox
		let flux = blob.flux
		
		guard let (eccentricity, majorAxis, minorAxis) = blob.eccentricity() else { return nil }
		let eccDir = blob.semiMajorAxisDirection()
		
		return DetectedStar(position: blob.centroid, boundingBox: boundingBox, flux: flux, hfd: blob.hfd,
							eccentricity: eccentricity, a: majorAxis, b: minorAxis, direction: eccDir,
							snr: blob.snr, background: blob.background)
	}
}
