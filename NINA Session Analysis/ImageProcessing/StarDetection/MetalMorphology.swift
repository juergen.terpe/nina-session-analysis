//
//  MetalMorphology.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 06.09.24.
//


import Metal

class MetalMorphology {
    let device: MTLDevice
    let commandQueue: MTLCommandQueue
    let erosionPipeline: MTLComputePipelineState
    let dilationPipeline: MTLComputePipelineState

    init?() {
        guard let device = MTLCreateSystemDefaultDevice() else {
            return nil
        }
        self.device = device
        self.commandQueue = device.makeCommandQueue()!
        
        // Load the Metal shaders (erosion and dilation)
        guard let defaultLibrary = device.makeDefaultLibrary(),
              let erosionFunction = defaultLibrary.makeFunction(name: "erosion"),
              let dilationFunction = defaultLibrary.makeFunction(name: "dilation") else {
            return nil
        }

        // Create compute pipelines
        erosionPipeline = try! device.makeComputePipelineState(function: erosionFunction)
        dilationPipeline = try! device.makeComputePipelineState(function: dilationFunction)
    }

    // Converts a [[Float]] array into a Metal texture
	func floatArrayToTexture(_ array: [[Float]], width: Int, height: Int) -> MTLTexture? {
		let descriptor = MTLTextureDescriptor.texture2DDescriptor(
			pixelFormat: .r32Float, width: width, height: height, mipmapped: false)
		descriptor.usage = [.shaderRead, .shaderWrite]

		guard let texture = device.makeTexture(descriptor: descriptor) else {
			return nil
		}

		// Sicherstellen, dass wir stabilen Speicher verwenden
		for y in 0..<height {
			array[y].withUnsafeBytes { rowPointer in
				texture.replace(region: MTLRegionMake2D(0, y, width, 1),
								mipmapLevel: 0, withBytes: rowPointer.baseAddress!,
								bytesPerRow: width * MemoryLayout<Float>.size)
			}
		}
		return texture
	}

    // Converts a Metal texture back into a [[Float]] array
	func textureToFloatArray(_ texture: MTLTexture, width: Int, height: Int) -> [[Float]] {
		var result = [[Float]](repeating: [Float](repeating: 0, count: width), count: height)

		for y in 0..<height {
			result[y].withUnsafeMutableBytes { rowPointer in
				texture.getBytes(rowPointer.baseAddress!,
								 bytesPerRow: width * MemoryLayout<Float>.size,
								 from: MTLRegionMake2D(0, y, width, 1),
								 mipmapLevel: 0)
			}
		}

		return result
	}
	
    // Executes erosion and dilation using Metal shaders
    func applyMorphology(input: [[Float]], width: Int, height: Int, kernelSize: Int) -> [[Float]] {
        guard let inputTexture = floatArrayToTexture(input, width: width, height: height) else {
            return input
        }

        // Create an output texture
        let descriptor = MTLTextureDescriptor.texture2DDescriptor(
            pixelFormat: .r32Float, width: width, height: height, mipmapped: false)
        descriptor.usage = [.shaderWrite, .shaderRead]
        let outputTexture = device.makeTexture(descriptor: descriptor)!

        let commandBuffer = commandQueue.makeCommandBuffer()!

        // Erosion
        let erosionEncoder = commandBuffer.makeComputeCommandEncoder()!
        erosionEncoder.setComputePipelineState(erosionPipeline)
        erosionEncoder.setTexture(inputTexture, index: 0)
        erosionEncoder.setTexture(outputTexture, index: 1)
        var kernelSizeUInt = UInt32(kernelSize)
        erosionEncoder.setBytes(&kernelSizeUInt, length: MemoryLayout<UInt32>.size, index: 0)
        let threadGroupSize = MTLSizeMake(8, 8, 1)
        let threadGroups = MTLSizeMake(
            (width + threadGroupSize.width - 1) / threadGroupSize.width,
            (height + threadGroupSize.height - 1) / threadGroupSize.height, 1)
        erosionEncoder.dispatchThreadgroups(threadGroups, threadsPerThreadgroup: threadGroupSize)
        erosionEncoder.endEncoding()

        // Dilation
        let dilationEncoder = commandBuffer.makeComputeCommandEncoder()!
        dilationEncoder.setComputePipelineState(dilationPipeline)
        dilationEncoder.setTexture(outputTexture, index: 0)
        dilationEncoder.setTexture(inputTexture, index: 1) // Reuse input texture as output
        dilationEncoder.setBytes(&kernelSizeUInt, length: MemoryLayout<UInt32>.size, index: 0)
        dilationEncoder.dispatchThreadgroups(threadGroups, threadsPerThreadgroup: threadGroupSize)
        dilationEncoder.endEncoding()

        commandBuffer.commit()
        commandBuffer.waitUntilCompleted()

        // Convert the result back to [[Float]]
        return textureToFloatArray(inputTexture, width: width, height: height)
    }
}
