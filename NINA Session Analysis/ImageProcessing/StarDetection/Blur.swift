//
//  Blur.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 18.09.24.
//

import Metal
import MetalKit

class Blur {
	private var device: MTLDevice!
	private var commandQueue: MTLCommandQueue!
	private var pipelineState: MTLComputePipelineState!

	init?() {
		// Metal initialisieren
		guard let device = MTLCreateSystemDefaultDevice() else {
			return nil
		}
		self.device = device
		self.commandQueue = device.makeCommandQueue()

		// Shader laden und Pipeline initialisieren
		do {
			let library = device.makeDefaultLibrary()
			let kernelFunction = library?.makeFunction(name: "blurKernel")
			pipelineState = try device.makeComputePipelineState(function: kernelFunction!)
		} catch {
			print("Error initializing Metal: \(error)")
			return nil
		}
	}

	func process(_ image: [[Float]], kernelSize: Int) -> [[Float]] {
		let height = image.count
		let width = image[0].count

		// 1. Input-Daten vorbereiten (bild in eine flache Array-Struktur konvertieren)
		let inputArray = image.flatMap { $0 }
		var outputArray = [Float](repeating: 0.0, count: inputArray.count)

		// 2. Metal Buffers erstellen
		guard let inputBuffer = device.makeBuffer(bytes: inputArray, length: inputArray.count * MemoryLayout<Float>.size,
												  options: .storageModeShared) else {
			return image
		}
		guard let outputBuffer = device.makeBuffer(bytes: &outputArray, length: outputArray.count * MemoryLayout<Float>.size,
												   options: .storageModeShared) else {
			return image
		}

		// 3. Kommando-Encoder erstellen
		guard let commandBuffer = commandQueue.makeCommandBuffer() else {
			return image
		}
		guard let commandEncoder = commandBuffer.makeComputeCommandEncoder() else {
			return image
		}

		// 4. Shader und Parameter setzen
		commandEncoder.setComputePipelineState(pipelineState)
		commandEncoder.setBuffer(inputBuffer, offset: 0, index: 0)
		commandEncoder.setBuffer(outputBuffer, offset: 0, index: 1)
		
		// Kernelgröße als Argument übergeben
		var kernelSizeVar = Int32(kernelSize)
		commandEncoder.setBytes(&kernelSizeVar, length: MemoryLayout<Int32>.size, index: 2)
		var widthVar = Int32(width)
		commandEncoder.setBytes(&widthVar, length: MemoryLayout<Int32>.size, index: 3)
		var heightVar = Int32(height)
		commandEncoder.setBytes(&heightVar, length: MemoryLayout<Int32>.size, index: 4)
		
		// 5. Threads pro Block und Grid berechnen
		let gridSize = MTLSize(width: width, height: height, depth: 1)
		let threadGroupSize = MTLSize(width: 16, height: 16, depth: 1)
		commandEncoder.dispatchThreads(gridSize, threadsPerThreadgroup: threadGroupSize)

		commandEncoder.endEncoding()
		commandBuffer.commit()
		commandBuffer.waitUntilCompleted()
		
		let outputPointer = outputBuffer.contents().bindMemory(to: Float.self, capacity: outputArray.count)
		for i in 0..<outputArray.count {
			outputArray[i] = outputPointer[i]
		}

		// 7. Ergebnis in 2D-Array konvertieren
		let result = stride(from: 0, to: outputArray.count, by: width).map {
			Array(outputArray[$0..<min($0 + width, outputArray.count)])
		}
		
		return result
	}
}
