//
//  DetectedStar.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 11.08.24.
//

import Foundation
import CoreGraphics

struct DetectedStar: Sendable, Identifiable, Hashable {
	let id = UUID()
	
	let position: CGPoint       // Position (Mitte des Sterns)
	let boundingBox: CGRect     // Begrenzungsrahmen
	let flux: Float             // Gesamtflux
	let hfd: Float              // Half Flux Diameter
	let eccentricity: Float    	// Exzentrizität
	let a: Float               	// Große Halbachse
	let b: Float                // Kleine Halbachse
	let direction: CGVector
	let snr: Float
	let background: Float
	
	func overlaps(with other: DetectedStar) -> Bool {
		return boundingBox.intersects(other.boundingBox)
	}
	
	var radius: Float {
		return Float(boundingBox.maxDimension)/2.0 + 0.5
	}
	
	var aspectRatio: Float? {
		guard a > 0.0 else { return nil }
		return b / a
	}
	
	func isSatelliteTrail() -> Bool {
		// Kriterium: Sehr lange und schmale Objekte
		guard let aspectRatio = aspectRatio else { return false }
		return aspectRatio < 0.1 && (boundingBox.width > 50 || boundingBox.height > 50)
	}
	
	func distance(to other: DetectedStar) -> CGFloat {
		let dx = position.x - other.position.x
		let dy = position.y - other.position.y
		return sqrt(dx * dx + dy * dy)
	}
	
	private func distanceSqr(to other: DetectedStar) -> CGFloat {
		let dx = position.x - other.position.x
		let dy = position.y - other.position.y
		return dx * dx + dy * dy
	}
	
	// Überprüft, ob der aktuelle Stern der gleiche ist wie der andere (doppelte Erkennung)
	func isDuplicate(of other: DetectedStar) -> Bool {
		let distance = self.distanceSqr(to: other)
		
		// Schwellenwerte für die Erkennung von doppelten Sternen
		let sizeThreshold: CGFloat = 5.0 * 5.0  // Minimaler Abstand der Sternmitten
		let fluxRatioThreshold: Float = 0.5  // Minimales Verhältnis der Helligkeit
		
		if distance < sizeThreshold {
			let fluxRatio = min(self.flux, other.flux) / max(self.flux, other.flux)
			
			// Behalte den Stern bei, wenn er viel schwächer ist als der andere (unabhängig)
			if fluxRatio > fluxRatioThreshold {
				return true
			}
		}
		
		return false
	}
	
}
