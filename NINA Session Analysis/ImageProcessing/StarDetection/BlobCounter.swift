//
//  BlobCounter.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 11.08.24.
//

import Foundation

class BlobCounter {
	let width: Int
	let height: Int
	let threshold: Float
	let minHFD: Float
	let maxHFD: Float
	let neighbors = [(0, -1), (0, 1), (-1, 0), (1, 0)]
	let rasterSize = 100
	
	// 2D-Array zur Verfolgung der bereits besuchten Pixel
	var visited: [[Bool]]
	var thresholdGrid: [[Float]]
	
	
	init(width: Int, height: Int, threshold: Float, minHFD: Float = 1.5, maxHFD: Float = 100.0) {
		self.width = width
		self.height = height
		self.threshold = threshold
		self.minHFD = minHFD
		self.maxHFD = maxHFD
		self.visited = Array(repeating: Array(repeating: false, count: width), count: height)
		
		self.thresholdGrid = Array(
			repeating: Array(repeating: 0, count: (width + rasterSize - 1) / rasterSize),
			count: (height + rasterSize - 1) / rasterSize
		)
	}

	private func createLocalThresholdRaster(width: Int, height: Int, maxHFD: Float, luminanceValues: [[Float]]) {
		let radius = Int(maxHFD + 1.0)
		
		for gridY in 0..<thresholdGrid.count {
			for gridX in 0..<thresholdGrid[0].count {
				let x = gridX * rasterSize
				let y = gridY * rasterSize
				self.thresholdGrid[gridY][gridX] = getLocalThreshold(x: x, y: y, radius: radius, luminanceValues)
			}
		}
	}
	
	
	// Findet alle Blobs im Bild
	func findBlobs(in luminanceValues: [[Float]]) -> [Blob] {
		var blobs = [Blob]()
		blobs.reserveCapacity(4200)
		
		createLocalThresholdRaster(width: width, height: height, maxHFD: maxHFD, luminanceValues: luminanceValues)
		
		for y in 0..<height {
			for x in 0..<width {
				
				if visited[y][x] {
					continue
				}
				
				let localThreshold = getInterpolatedThreshold(x: x, y: y)
				let luminance = luminanceValues[y][x]

				if luminance >= localThreshold {
					
					if isPeak(x: x, y: x, luminanceValues: luminanceValues,
							  centerLuminance: luminance, width: width, height: height) {
						
						if let blob = createBlob(x: x, y: y, luminanceValues: luminanceValues,
												 threshold: localThreshold) {
							blobs.append(blob)
						}
					}
				}
			}
		}

		return blobs
	}
	
	private func isPeak(x: Int, y: Int, luminanceValues: [[Float]],
						centerLuminance: Float,
						width: Int, height: Int) -> Bool {
		for (dx, dy) in neighbors {
			let nx = x + dx
			let ny = y + dy
			if nx < 0 || ny < 0 || nx >= width || ny >= height { continue }
			
			if luminanceValues[ny][nx] > centerLuminance {
				return false // Es gibt einen helleren Nachbarn, also ist es kein Peak
			}
		}
		
		return true // Keiner der Nachbarn war heller -> lokales Maximum
	}
	
	
	private func getLocalThreshold(x: Int, y: Int, radius: Int, _ luminanceValues: [[Float]]) -> Float {
		let rsqr = radius*radius
		let r2 = (radius+2)*(radius+2)
		var bgValues = [Float]()
		
		for i in -radius...radius {
			if x + i < 0 || x + i >= width { continue }
			
			for j in -radius...radius {
				if y + j < 0 || y + j >= height { continue }
				
				let d = i*i + j*j
				if d > rsqr && d < r2 {
					let bg = luminanceValues[y+j][x+i]
					bgValues.append(bg)
				}
			}
		}
		
		let starBckgnd = getMedian(&bgValues)
		for i in 0..<bgValues.count {
			bgValues[i] = abs(bgValues[i] - starBckgnd)
		}
		let mad = getMedian(&bgValues)
		return starBckgnd + 3.5 * mad
	}
	
	private func getInterpolatedThreshold(x: Int, y: Int) -> Float {
		let gridX = x / rasterSize
		let gridY = y / rasterSize
		
		let x1 = gridX * rasterSize
		let y1 = gridY * rasterSize
		let x2 = min(x1 + rasterSize, width - 1)
		let y2 = min(y1 + rasterSize, height - 1)
		
		let dx = Float(x - x1) / Float(x2 - x1)
		let dy = Float(y - y1) / Float(y2 - y1)

		let T00 = thresholdGrid[gridY][gridX]
		let T10 = (gridX + 1 < thresholdGrid[0].count) ? thresholdGrid[gridY][gridX + 1] : T00
		let T01 = (gridY + 1 < thresholdGrid.count) ? thresholdGrid[gridY + 1][gridX] : T00
		let T11 = (gridX + 1 < thresholdGrid[0].count && gridY + 1 < thresholdGrid.count) ? thresholdGrid[gridY + 1][gridX + 1] : T00
		
		// Bilineare Interpolation
		let Txy = (1 - dx) * (1 - dy) * T00 +
				  dx * (1 - dy) * T10 +
				  (1 - dx) * dy * T01 +
				  dx * dy * T11

		return Txy
	}
	
	private func createBlob(x: Int, y: Int, luminanceValues: [[Float]], threshold: Float) -> Blob? {
		var pixels = [(x: Int, y: Int)]()
		var luminances = [Float]()
		var flux: Float = 0
		var minX = x, minY = y, maxX = x, maxY = y

		// Flood-Fill mit Warteschlange (erste Phase: großzügigere Erfassung)
		var queue = Queue<(x: Int, y: Int)>()
		queue.enque((x: x, y: y))
		visited[y][x] = true

		let radius = Int(self.maxHFD + 2.0) // Etwas größere Erfassung
		let rsqr = radius * radius
		var totalLuminance: Float = 0.0
		
		var sumX: Float = 0.0
		var sumY: Float = 0.0
		var totalFlux: Float = 0.0

		while !queue.isEmpty {
			if let (currentX, currentY) = queue.deque() {
				let d = (x-currentX) * (x-currentX) + (y-currentY) * (y-currentY)
				if d > rsqr {
					continue // Kein harter Abbruch mehr, sondern sanftere Begrenzung
				}

				let luminance = luminanceValues[currentY][currentX]
				totalLuminance += luminance

				let lum = max(0, luminance - threshold)
				sumX += Float(currentX) * lum
				sumY += Float(currentY) * lum
				totalFlux += lum

				pixels.append((x: currentX, y: currentY))
				luminances.append(luminance)
				flux += luminance

				// Aktualisiere Grenzen des Blobs
				minX = min(minX, currentX)
				minY = min(minY, currentY)
				maxX = max(maxX, currentX)
				maxY = max(maxY, currentY)

				// Nachbarn prüfen (oben, unten, links, rechts)
				for (dx, dy) in neighbors {
					let newX = currentX + dx
					let newY = currentY + dy

					if newX >= 0 && newX < width && newY >= 0 && newY < height && !visited[newY][newX] {
						visited[newY][newX] = true

						let neighborLuminance = luminanceValues[newY][newX]
						let gradient = abs(neighborLuminance - luminance)

						if neighborLuminance >= threshold && gradient < 0.3 * luminance { // Weniger restriktiv
							queue.enque((x: newX, y: newY))
						}
					}
				}
			}
		}

		guard pixels.count > 5 else {
			return nil
		}

		let centroidX = sumX / totalFlux
		let centroidY = sumY / totalFlux

		if centroidX < 0 || centroidX >= Float(width) || centroidY < 0 || centroidY >= Float(height) {
			return nil
		}

		let centroid = CGPoint(x: CGFloat(centroidX), y: CGFloat(centroidY))
		let blobRadius = calculateBlobRadius(blobPixels: pixels)

		let (bbMinX, bbMinY, bbMaxX, bbMaxY) = calculateBoundingBox(centroid: centroid, blobRadius: blobRadius, width: width, height: height)
		
		if !isValidBlob(blobRadius: blobRadius, centroid: centroid,
						minX: bbMinX, maxX: bbMaxX, minY: bbMinY, maxY: bbMaxY,
						luminanceValues: luminanceValues) {
			return nil
		}

		guard let hfd = getHFD(pixels: pixels, centroid: centroid, threshold: threshold,
							   blobRadius: blobRadius, luminanceValues: luminanceValues),
			  hfd > self.minHFD else {
			return nil
		}

		let (background, snr) = calculateBackgroundAndSNR(blobPixels: pixels, centroid: centroid,
														  radius: blobRadius, luminanceValues: luminanceValues)

		return Blob(centroid: centroid,
					minX: bbMinX, minY: bbMinY, maxX: bbMaxX, maxY: bbMaxY,
					pixels: pixels, flux: flux, hfd: hfd,
					luminanceValues: luminances,
					threshold: threshold, snr: snr, background: background)
	}

	private func calculateBoundingBox(centroid: CGPoint, blobRadius: Float, width: Int, height: Int) -> (minX: Int, minY: Int, maxX: Int, maxY: Int) {
		let cx = Int(round(centroid.x))
		let cy = Int(round(centroid.y))
		let r = Int(ceil(blobRadius))
		
		let minX = max(0, cx - r)
		let minY = max(0, cy - r)
		let maxX = min(width - 1, cx + r)
		let maxY = min(height - 1, cy + r)
		
		return (minX, minY, maxX, maxY)
	}
	
	private func getSubPixelValue(cx: Float, cy: Float, dx: Float, dy: Float, threshold: Float, _ luminanceValues: [[Float]]) -> Float? {
		guard cx >= dx, cy >= dy, cx < Float(width)-dx-1.0, cy < Float(height)-dy-1.0 else {
			return nil
		}
		
		let x = cx + dx
		let y = cy + dy
		let ix = Int(x)
		let iy = Int(y)
		
		if ix < 0 || iy < 0 {
			return nil
		}
		
		let fx = frac(x)
		let fy = frac(y)
		
		var sum = (luminanceValues[iy][ix] - threshold) * (1.0-fx)*(1.0-fy)
		if ix + 1 < width {
			sum += (luminanceValues[iy][ix+1] - threshold) * fx*(1.0-fy)
			if iy + 1 < height {
				sum += (luminanceValues[iy+1][ix+1] - threshold) * fx*fy
			}
		}
		
		if iy + 1 < height {
			sum += (luminanceValues[iy+1][ix] - threshold) * (1.0-fx)*fy
		}
		
		return sum
		
	}
	
	
	// Schnelle Medianberechnung ohne Sortierung
	private func quickSelectMedian(_ array: inout [Float]) -> Float {
		let n = array.count
		let k = n / 2
		return quickSelect(&array, k: k)
	}

	private func quickSelect(_ array: inout [Float], k: Int) -> Float {
		var left = 0
		var right = array.count - 1
		
		while left < right {
			let pivotIndex = partition(&array, left: left, right: right)
			if pivotIndex == k {
				return array[k]
			} else if pivotIndex < k {
				left = pivotIndex + 1
			} else {
				right = pivotIndex - 1
			}
		}
		return array[left]
	}

	private func partition(_ array: inout [Float], left: Int, right: Int) -> Int {
		let pivotValue = array[right]
		var storeIndex = left
		for i in left..<right {
			if array[i] < pivotValue {
				array.swapAt(i, storeIndex)
				storeIndex += 1
			}
		}
		array.swapAt(storeIndex, right)
		return storeIndex
	}
	
	
	private func getHFD(pixels: [(x: Int, y: Int)], centroid: CGPoint, threshold: Float, blobRadius: Float, luminanceValues: [[Float]]) -> Float? {
		var totalFlux: Float = 0.0
		var coreFlux: Float = 0.0
		var outerFlux: Float = 0.0
		var corePixelCount = 0
		var outerPixelCount = 0
		var pixelBrightnesses = [Float]()
		
		let blobSize = pixels.count
		let coreThreshold = 0.5 * blobRadius
		
		let cx = Float(centroid.x)
		let cy = Float(centroid.y)
		
		var sumFluxR: Float = 0.0
		
		for (x, y) in pixels {
			let dx = cx - Float(x)
			let dy = cy - Float(y)
			
			guard let pixelValue = getSubPixelValue(cx: cx, cy: cy, dx: dx, dy: dy, threshold: threshold, luminanceValues) else {
				continue
			}
			
			let distance = Float(hypot(dx, dy))
			totalFlux += pixelValue
			pixelBrightnesses.append(pixelValue)
			
			sumFluxR += pixelValue * distance
			
			if distance < coreThreshold {
				coreFlux += pixelValue
				corePixelCount += 1
			} else {
				outerFlux += pixelValue
				outerPixelCount += 1
			}
		}
		
		if totalFlux < 1e-3 { return nil } // Mindesthelligkeitsschwelle
				
		if pixelBrightnesses.isEmpty {
			return nil
		}
		
		// Überprüfe den Lichtabfall: der Kern muss signifikant heller sein als der äußere Bereich
		let averageCoreFlux = corePixelCount > 0 ? coreFlux / Float(corePixelCount) : 0.0
		let averageOuterFlux = outerPixelCount > 0 ? outerFlux / Float(outerPixelCount) : 0.0
				
		let medianFlux = getMedian(&pixelBrightnesses)
		let meanBrightness = totalFlux / Float(blobSize)
		let standardDeviation = sqrt(pixelBrightnesses.reduce(0) { $0 + pow($1 - meanBrightness, 2) } / Float(blobSize))
		
		let brightnessRatio = averageCoreFlux / (averageOuterFlux + 1e-5) // Schutz vor Division durch Null
		let dynamicThresholdRatio = (medianFlux + standardDeviation) / (averageOuterFlux + 1e-5)
		
		// Wenn der Kern nicht signifikant heller ist, geben wir nil zurück, da es kein Stern ist
		if brightnessRatio < dynamicThresholdRatio {
			return nil
		}
		
		return min(2.5 * blobRadius, 2.0 * sumFluxR / totalFlux)
	}
	
	 
	// Berechnung des Himmelshintergrunds und SNR
	private func calculateBackgroundAndSNR(blobPixels: [(x: Int, y: Int)], centroid: CGPoint, radius: Float, luminanceValues: [[Float]]) -> (Float, Float) {
		let ringThickness: Int = 5
		var backgroundSum: Float = 0.0
		var backgroundPixelCount: Int = 0
		let blobRadius = Int(radius)
		
		let cx = Int(centroid.x)
		let cy = Int(centroid.y)
		
		// Ring um den Stern
		for y in (cy - ringThickness)...(cy + ringThickness) {
			
			if abs(y - cy) > blobRadius {
				
				for x in (cx - ringThickness)...(cx + ringThickness) {
					if abs(x - cx) > blobRadius && x >= 0 && x < width && y >= 0 && y < height {
						backgroundSum += luminanceValues[y][x]
						backgroundPixelCount += 1
					}
				}
			}
		}

		let background = backgroundSum / Float(backgroundPixelCount)
		let snr = blobPixels.reduce(0.0) { $0 + luminanceValues[$1.y][$1.x] } / background

		return (background, snr)
	}

	// Berechnung des Blobradius
	private func calculateBlobRadius(blobPixels: [(x: Int, y: Int)]) -> Float {
		var maxSqrDistance: Float = 0.0
		
		let fx = blobPixels[0].x
		let fy = blobPixels[0].y
		
		for (x, y) in blobPixels {
			let distance2 = Float((x - fx) * (x - fx) + (y - fy) * (y - fy))
			maxSqrDistance = max(maxSqrDistance, distance2)
		}
		return sqrt(maxSqrDistance) / 2.0
	}

	// Filter, um Satellitenspuren zu erkennen und zu verwerfen
	private func isValidBlob(blobRadius: Float, centroid: CGPoint,
							 minX: Int, maxX: Int, minY: Int, maxY: Int,
							 luminanceValues: [[Float]]) -> Bool {
		let ringRadius = Int(blobRadius) + 5
		var ringLuminances = [Float]()

		let cx = Int(centroid.x)
		let cy = Int(centroid.y)
		let centerLuminance = luminanceValues[cy][cx] - threshold
				
		let r2 = blobRadius * blobRadius
		let rr2 = Float(ringRadius * ringRadius)
		
		// Prüfe Ring um den Stern
		for y in (minY - ringRadius)...(maxY + ringRadius) {
			
			if y >= 0 && y < height {
				
				let dy2 = Float((y - cy) * (y - cy))
				
				for x in (minX - ringRadius)...(maxX + ringRadius) {
					
					if x >= 0 && x < width {
						let dx2 = Float((x - cx) * (x - cx))
						
						let distanceToCentroid2 = dx2 + dy2
						if distanceToCentroid2 > r2 && distanceToCentroid2 < rr2 {
							ringLuminances.append(luminanceValues[y][x] - threshold)
						}
					}
				}
			}
		}

		if ringLuminances.isEmpty {
			return false
		}
				
		let medianLum = getMedian(&ringLuminances)
		return medianLum < threshold * 3.0 && centerLuminance > 2.0 * medianLum
	}
}
