//
//  ImageFromChannels.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 11.07.24.
//

import Metal
import MetalKit

fileprivate struct ImageParams {
	var width: UInt32
	var height: UInt32
}


class ImageFromChannels {
	let device: MTLDevice
	let commandQueue: MTLCommandQueue
	let pipelineState: MTLComputePipelineState
	
	init() {
		guard let device = MTLCreateSystemDefaultDevice() else {
			fatalError("Metal is not supported on this device")
		}
		
		self.device = device
		self.commandQueue = device.makeCommandQueue()!
		
		let library = device.makeDefaultLibrary()
		let kernelFunction = library?.makeFunction(name: "createImageFromChannels")
		self.pipelineState = try! device.makeComputePipelineState(function: kernelFunction!)
	}
	
	func createImage(from channels: [Channel: [UInt16]], width: Int, height: Int) -> CGImage? {
		let pixelCount = width * height
		
		// Erstellen der Textur
		let textureDescriptor = MTLTextureDescriptor.texture2DDescriptor(pixelFormat: .rgba16Float, width: width, height: height, mipmapped: false)
		textureDescriptor.usage = .shaderWrite
		guard let outTexture = device.makeTexture(descriptor: textureDescriptor) else { return nil }
		
		// Erstellen der Metal Buffers
		guard let bufferR = device.makeBuffer(bytes: channels[Channel.r]!, length: pixelCount * MemoryLayout<UInt16>.size, options: []),
			  let bufferG = channels[Channel.g] != nil ? device.makeBuffer(bytes: channels[Channel.g]!, length: pixelCount * MemoryLayout<UInt16>.size, options: []) : nil,
			  let bufferB = channels[Channel.b] != nil ? device.makeBuffer(bytes: channels[Channel.b]!, length: pixelCount * MemoryLayout<UInt16>.size, options: []) : nil
		else { return nil }
		
		var imageParams = ImageParams(width: UInt32(width), height: UInt32(height))
		let paramsBuffer = device.makeBuffer(bytes: &imageParams, length: MemoryLayout<ImageParams>.size, options: [])
		
		// Erstellen eines Command Buffers und Encoder
		guard let commandBuffer = commandQueue.makeCommandBuffer(),
			  let computeEncoder = commandBuffer.makeComputeCommandEncoder() else { return nil }
		
		computeEncoder.setComputePipelineState(pipelineState)
		computeEncoder.setTexture(outTexture, index: 0)
		computeEncoder.setBuffer(bufferR, offset: 0, index: 0)
		computeEncoder.setBuffer(bufferG, offset: 0, index: 1)
		computeEncoder.setBuffer(bufferB, offset: 0, index: 2)
		computeEncoder.setBuffer(paramsBuffer, offset: 0, index: 3)
		
		let gridSize = MTLSize(width: width, height: height, depth: 1)
		let threadGroupSize = MTLSize(width: 16, height: 16, depth: 1)
		computeEncoder.dispatchThreads(gridSize, threadsPerThreadgroup: threadGroupSize)
		
		computeEncoder.endEncoding()
		commandBuffer.commit()
		commandBuffer.waitUntilCompleted()
		
		// CGImage aus der Textur erstellen
		let bytesPerPixel = 8
		let bytesPerRow = bytesPerPixel * width
		var rawData = [UInt16](repeating: 0, count: pixelCount * 4)
		let region = MTLRegionMake2D(0, 0, width, height)
		outTexture.getBytes(&rawData, bytesPerRow: bytesPerRow, from: region, mipmapLevel: 0)
		
		let cfData = CFDataCreate(nil, rawData.withUnsafeBufferPointer { pointer in
			return UnsafeRawPointer(pointer.baseAddress!)
		}, rawData.count * MemoryLayout<UInt16>.size)
		
		let dataProvider = CGDataProvider(data: cfData!)
		let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue | CGImageByteOrderInfo.order16Little.rawValue)
		let colorSpace = CGColorSpaceCreateDeviceRGB()
		
		let cgImage = CGImage(width: width,
							  height: height,
							  bitsPerComponent: 16,
							  bitsPerPixel: 64,
							  bytesPerRow: bytesPerRow,
							  space: colorSpace,
							  bitmapInfo: bitmapInfo,
							  provider: dataProvider!,
							  decode: nil,
							  shouldInterpolate: false,
							  intent: .defaultIntent)
		
		return cgImage
	}
}
