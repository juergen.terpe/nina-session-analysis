//
//  ChannelExtractor.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 11.07.24.
//

import Metal
import MetalKit

fileprivate struct ImageParams {
	var width: UInt32
	var height: UInt32
	var bytesPerPixel: UInt32
	var bitsPerComponent: UInt32
}

class ChannelExtractor {
	let device: MTLDevice
	let commandQueue: MTLCommandQueue
	let pipelineState: MTLComputePipelineState
	
	init() {
		guard let device = MTLCreateSystemDefaultDevice() else {
			fatalError("Metal is not supported on this device")
		}
		
		self.device = device
		self.commandQueue = device.makeCommandQueue()!
		
		let library = device.makeDefaultLibrary()
		let kernelFunction = library?.makeFunction(name: "extractChannels")
		self.pipelineState = try! device.makeComputePipelineState(function: kernelFunction!)
	}
	
	func extractChannels(cgImage: CGImage) -> [Channel: [UInt16]]? {
		let width = cgImage.width
		let height = cgImage.height
		let bytesPerPixel = cgImage.bitsPerPixel / 8
		let bitsPerComponent = cgImage.bitsPerComponent
		let pixelCount = width * height

		guard let colorSpace = cgImage.colorSpace else { return nil }
		guard let data = cgImage.dataProvider?.data, let bytes = CFDataGetBytePtr(data) else {
			return nil
		}

		if colorSpace.model == .rgb {
			
			let bufferR = device.makeBuffer(length: pixelCount * MemoryLayout<UInt16>.size, options: .storageModeShared)
			let bufferG = device.makeBuffer(length: pixelCount * MemoryLayout<UInt16>.size, options: .storageModeShared)
			let bufferB = device.makeBuffer(length: pixelCount * MemoryLayout<UInt16>.size, options: .storageModeShared)
			let imageBuffer = device.makeBuffer(bytes: bytes, length: pixelCount * bytesPerPixel, options: [])

			var imageParams = ImageParams(width: UInt32(width), height: UInt32(height), bytesPerPixel: UInt32(bytesPerPixel), bitsPerComponent: UInt32(bitsPerComponent))
			let paramsBuffer = device.makeBuffer(bytes: &imageParams, length: MemoryLayout<ImageParams>.size, options: [])

			guard let commandBuffer = commandQueue.makeCommandBuffer(),
				  let computeEncoder = commandBuffer.makeComputeCommandEncoder() else { return nil }

			computeEncoder.setComputePipelineState(pipelineState)
			computeEncoder.setBuffer(imageBuffer, offset: 0, index: 0)
			computeEncoder.setBuffer(bufferR, offset: 0, index: 1)
			computeEncoder.setBuffer(bufferG, offset: 0, index: 2)
			computeEncoder.setBuffer(bufferB, offset: 0, index: 3)
			computeEncoder.setBuffer(paramsBuffer, offset: 0, index: 4)

			let gridSize = MTLSize(width: width, height: height, depth: 1)
			let threadGroupSize = MTLSize(width: 16, height: 16, depth: 1)
			computeEncoder.dispatchThreads(gridSize, threadsPerThreadgroup: threadGroupSize)

			computeEncoder.endEncoding()
			commandBuffer.commit()
			commandBuffer.waitUntilCompleted()

			let redPointer = bufferR?.contents().assumingMemoryBound(to: UInt16.self)
			let greenPointer = bufferG?.contents().assumingMemoryBound(to: UInt16.self)
			let bluePointer = bufferB?.contents().assumingMemoryBound(to: UInt16.self)

			guard let redBuffer = redPointer, let greenBuffer = greenPointer, let blueBuffer = bluePointer else {
				return nil
			}

			let redChannel = Array(UnsafeBufferPointer(start: redBuffer, count: pixelCount))
			let greenChannel = Array(UnsafeBufferPointer(start: greenBuffer, count: pixelCount))
			let blueChannel = Array(UnsafeBufferPointer(start: blueBuffer, count: pixelCount))

			return [.r: redChannel, .g: greenChannel, .b: blueChannel]
		} else if colorSpace.model == .monochrome {
			var pixels = [UInt16](repeating: 0, count: pixelCount)
			if bitsPerComponent == 8 {
				for i in 0..<pixels.count {
					pixels[i] = UInt16(Float(UInt16.max) * (Float(bytes[i]) / 255.0))
				}
			} else if bitsPerComponent == 16 {
				let rawPointer = UnsafeRawPointer(bytes)
				let pixelBuffer = rawPointer.bindMemory(to: UInt16.self, capacity: pixelCount * bytesPerPixel / 2)
				for i in 0..<pixels.count {
					pixels[i] = pixelBuffer[i]
				}
			} else if bitsPerComponent == 32 {
				let rawPointer = UnsafeRawPointer(bytes)
				let pixelBuffer = rawPointer.bindMemory(to: UInt32.self, capacity: pixelCount * bytesPerPixel / 4)
				for i in 0..<pixels.count {
					pixels[i] = UInt16(Float(UInt16.max) * (Float(pixelBuffer[i]) / 4294967295.0))
				}
			}
			return [.l: pixels]
		}

		return nil
	}

	func extractChannels2D(cgImage: CGImage) -> [[[UInt16]]]? {
		let width = cgImage.width
		let height = cgImage.height
		let bytesPerPixel = cgImage.bitsPerPixel / 8
		let bitsPerComponent = cgImage.bitsPerComponent
		let pixelCount = width * height

		guard let colorSpace = cgImage.colorSpace else { return nil }
		guard let data = cgImage.dataProvider?.data, let bytes = CFDataGetBytePtr(data) else {
			return nil
		}

		if colorSpace.model == .rgb {
			let bufferR = device.makeBuffer(length: pixelCount * MemoryLayout<UInt16>.size, options: .storageModeShared)
			let bufferG = device.makeBuffer(length: pixelCount * MemoryLayout<UInt16>.size, options: .storageModeShared)
			let bufferB = device.makeBuffer(length: pixelCount * MemoryLayout<UInt16>.size, options: .storageModeShared)
			let imageBuffer = device.makeBuffer(bytes: bytes, length: pixelCount * bytesPerPixel, options: [])

			var imageParams = ImageParams(width: UInt32(width), height: UInt32(height), bytesPerPixel: UInt32(bytesPerPixel), bitsPerComponent: UInt32(bitsPerComponent))
			let paramsBuffer = device.makeBuffer(bytes: &imageParams, length: MemoryLayout<ImageParams>.size, options: [])

			guard let commandBuffer = commandQueue.makeCommandBuffer(),
				  let computeEncoder = commandBuffer.makeComputeCommandEncoder() else { return nil }

			computeEncoder.setComputePipelineState(pipelineState)
			computeEncoder.setBuffer(imageBuffer, offset: 0, index: 0)
			computeEncoder.setBuffer(bufferR, offset: 0, index: 1)
			computeEncoder.setBuffer(bufferG, offset: 0, index: 2)
			computeEncoder.setBuffer(bufferB, offset: 0, index: 3)
			computeEncoder.setBuffer(paramsBuffer, offset: 0, index: 4)

			let gridSize = MTLSize(width: width, height: height, depth: 1)
			let threadGroupSize = MTLSize(width: 16, height: 16, depth: 1)
			computeEncoder.dispatchThreads(gridSize, threadsPerThreadgroup: threadGroupSize)

			computeEncoder.endEncoding()
			commandBuffer.commit()
			commandBuffer.waitUntilCompleted()

			let redPointer = bufferR?.contents().assumingMemoryBound(to: UInt16.self)
			let greenPointer = bufferG?.contents().assumingMemoryBound(to: UInt16.self)
			let bluePointer = bufferB?.contents().assumingMemoryBound(to: UInt16.self)

			guard let redBuffer = redPointer, let greenBuffer = greenPointer, let blueBuffer = bluePointer else {
				return nil
			}

			var result = [[[UInt16]]](repeating: [[UInt16]](repeating: [0, 0, 0], count: width), count: height)
			
			for y in 0..<height {
				for x in 0..<width {
					let index = y * width + x
					result[y][x] = [redBuffer[index], greenBuffer[index], blueBuffer[index]]
				}
			}
			
			return result
		} else if colorSpace.model == .monochrome {
			var pixels = [[UInt16]](repeating: [UInt16](repeating: 0, count: width), count: height)
			if bitsPerComponent == 8 {
				for y in 0..<height {
					for x in 0..<width {
						let index = y * width + x
						pixels[y][x] = UInt16(Float(UInt16.max) * (Float(bytes[index]) / 255.0))
					}
				}
			} else if bitsPerComponent == 16 {
				let rawPointer = UnsafeRawPointer(bytes)
				let pixelBuffer = rawPointer.bindMemory(to: UInt16.self, capacity: pixelCount * bytesPerPixel / 2)
				for y in 0..<height {
					for x in 0..<width {
						let index = y * width + x
						pixels[y][x] = pixelBuffer[index]
					}
				}
			} else if bitsPerComponent == 32 {
				let rawPointer = UnsafeRawPointer(bytes)
				let pixelBuffer = rawPointer.bindMemory(to: UInt32.self, capacity: pixelCount * bytesPerPixel / 4)
				for y in 0..<height {
					for x in 0..<width {
						let index = y * width + x
						pixels[y][x] = UInt16(Float(UInt16.max) * (Float(pixelBuffer[index]) / 4294967295.0))
					}
				}
			}
			return pixels.map { $0.map { [$0, $0, $0] } }  // Monochrome als RGB ausgeben
		}

		return nil
	}
	
	func extractChannelsSequential(cgImage: CGImage) -> [UInt16]? {
		let width = cgImage.width
		let height = cgImage.height
		let bytesPerPixel = cgImage.bitsPerPixel / 8
		let bitsPerComponent = cgImage.bitsPerComponent
		let pixelCount = width * height

		guard let colorSpace = cgImage.colorSpace else { return nil }
		guard let data = cgImage.dataProvider?.data, let bytes = CFDataGetBytePtr(data) else {
			return nil
		}

		var result = [UInt16](repeating: 0, count: pixelCount * 3) // Speicher für R, G, B

		if colorSpace.model == .rgb {
			if bitsPerComponent == 8 {
				for i in 0..<pixelCount {
					let pixelStart = i * bytesPerPixel
					result[i * 3] = UInt16(bytes[pixelStart]) << 8  // R
					result[i * 3 + 1] = UInt16(bytes[pixelStart + 1]) << 8  // G
					result[i * 3 + 2] = UInt16(bytes[pixelStart + 2]) << 8  // B
				}
			} else if bitsPerComponent == 16 {
				let rawPointer = UnsafeRawPointer(bytes)
				let pixelBuffer = rawPointer.bindMemory(to: UInt16.self, capacity: pixelCount * bytesPerPixel / 2)
				for i in 0..<pixelCount {
					let pixelStart = i * (bytesPerPixel / 2)
					result[i * 3] = pixelBuffer[pixelStart]  // R
					result[i * 3 + 1] = pixelBuffer[pixelStart + 1]  // G
					result[i * 3 + 2] = pixelBuffer[pixelStart + 2]  // B
				}
			}
		} else if colorSpace.model == .monochrome {
			if bitsPerComponent == 8 {
				for i in 0..<pixelCount {
					let gray = UInt16(bytes[i]) << 8
					result[i * 3] = gray
					result[i * 3 + 1] = gray
					result[i * 3 + 2] = gray
				}
			} else if bitsPerComponent == 16 {
				let rawPointer = UnsafeRawPointer(bytes)
				let pixelBuffer = rawPointer.bindMemory(to: UInt16.self, capacity: pixelCount * bytesPerPixel / 2)
				for i in 0..<pixelCount {
					let gray = pixelBuffer[i]
					result[i * 3] = gray
					result[i * 3 + 1] = gray
					result[i * 3 + 2] = gray
				}
			} else if bitsPerComponent == 32 {
				let rawPointer = UnsafeRawPointer(bytes)
				let pixelBuffer = rawPointer.bindMemory(to: UInt32.self, capacity: pixelCount * bytesPerPixel / 4)
				for i in 0..<pixelCount {
					let gray = UInt16(Float(UInt16.max) * (Float(pixelBuffer[i]) / 4294967295.0))
					result[i * 3] = gray
					result[i * 3 + 1] = gray
					result[i * 3 + 2] = gray
				}
			}
		}

		return result
	}
}

