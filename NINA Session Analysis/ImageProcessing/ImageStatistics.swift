//
//  ImageStatistics.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 09.07.24.
//
import Foundation
import CoreImage

struct ImageStatistics {
	
	var count: Int
	var stDev: Double
	var mean: Double
	var median: Double
	var medianAbsoluteDeviation: Double
	var max: Int
	var maxOccurrences: Int
	var min: Int
	var minOccurrences: Int
	var histogram: [Int] = []
	
	
	var minOccurrencesPercentage: Double {
		Double(minOccurrences) / Double(count) * 100.0
	}
	
	var maxOccurrencesPercentage: Double {
		Double(maxOccurrences) / Double(count) * 100.0
	}
	
	static func create(from imageArray: [UInt16]) -> ImageStatistics {
		var sum: Double = 0.0
		var squareSum: Double = 0.0
		
		let count = imageArray.count
		var minV = UInt16.max
		var oldMin = minV
		var maxV = UInt16(0)
		var oldMax = maxV
		var maxOccurrences = 0
		var minOccurrences = 0
			
			
		var pixelValueCounts = Array(repeating: 0, count: Int(UInt16.max) + 1)
			
		for i in 0..<count {
			let val = imageArray[i]
			
			let dVal = Double(val)
			sum += dVal
			squareSum += dVal * dVal
			pixelValueCounts[Int(val)] += 1

			minV = minV < val ? minV : val
			if minV != oldMin {
				minOccurrences = 0
			}
			if val == minV {
				minOccurrences += 1
			}

			maxV = maxV > val ? maxV : val
			if maxV != oldMax {
				maxOccurrences = 0
			}
			if val == maxV {
				maxOccurrences += 1
			}

			oldMin = minV
			oldMax = maxV
		}
			
		let mean = sum / Double(count)
		let variance = (squareSum - Double(count) * mean * mean) / Double(count)
		let stdev = sqrt(variance)

		var occurrences = 0
		var median1 = 0, median2 = 0
		let medianlength = count >> 1
			
		/* Determine median out of histogram array */
		for i in 0..<Int(UInt16.max) {
			occurrences += pixelValueCounts[i]
			if occurrences > medianlength {
				median1 = i
				median2 = i
				break
			} else if occurrences == medianlength {
				median1 = i
				for j in (i+1)..<Int(UInt16.max) {
					if (pixelValueCounts[j] > 0) {
						median2 = j
						break
					}
				}
				break
			}
		}
		let median = Double(median1 + median2) / 2.0
		
		var medianAbsoluteDeviation = 0.0
		occurrences = 0
		var idxDown = median1
		var idxUp = median2
		
		while true {
			if idxDown >= 0 && idxDown != idxUp {
				occurrences += pixelValueCounts[idxDown] + pixelValueCounts[idxUp]
			} else {
				occurrences += pixelValueCounts[idxUp]
			}

			if occurrences > medianlength {
				medianAbsoluteDeviation = abs(Double(idxUp) - median)
				break
			}

			idxUp+=1
			idxDown-=1
			if idxUp > UInt16.max {
				break
			}
		}
			
		print("minmax \(minV) \(maxV)")
		
		return ImageStatistics(count: count,
							   stDev: stdev,
							   mean: mean,
							   median: median,
							   medianAbsoluteDeviation: medianAbsoluteDeviation,
							   max: Int(maxV),
							   maxOccurrences: maxOccurrences,
							   min: Int(minV),
							   minOccurrences: minOccurrences,
							   histogram: pixelValueCounts)
	}
	
}

struct ImageStatisticsChannel {
	let channel: Channel
	let statistics: ImageStatistics
}


extension ImageStatistics {
	static func create(from image: CGImage) async -> [Channel : ImageStatistics]? {
		guard let channelValues = image.extractChannelValues() else {
			return nil
		}
		
		var result = [Channel : ImageStatistics]()
		
		await withTaskGroup(of: ImageStatisticsChannel.self) { group in
			
			for channel in channelValues.keys {
				if let channelData = channelValues[channel] {
					group.addTask {
						return ImageStatisticsChannel(channel: channel, statistics: ImageStatistics.create(from: channelData))
					}
				}
			}
			
			for await statistics in group {
				print("minmax \(statistics.statistics.min) \(statistics.statistics.max)")
				
				result[statistics.channel] = statistics.statistics
			}
		}
		
		return result
	}
}
