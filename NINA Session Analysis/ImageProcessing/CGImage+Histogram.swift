//
//  CGImage+Histogram.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 06.10.24.
//

import Foundation
import Accelerate


extension CGImage {

	
	
	/// The function calculates the histogram for each channel completely separately from the others.
	/// - Returns: A tuple contain the three histograms for the corresponding channels. Each of the three histograms will be an array with 256 elements.
	func histogram() async -> [ Channel : [UInt] ]? {
		if self.isColorImage() {
			return await histogramColor()
		} else {
			return await histogramGrayscale()
		}
	}
	
	func histogramGrayscale() async -> [Channel : [UInt]]? {
		let format = vImage_CGImageFormat(
			bitsPerComponent: 8,
			bitsPerPixel: 8,
			colorSpace: CGColorSpaceCreateDeviceGray(),
			bitmapInfo: CGBitmapInfo(rawValue: CGImageAlphaInfo.none.rawValue),
			renderingIntent: .defaultIntent)!
		
		guard var sourceBuffer = try? vImage_Buffer(cgImage: self, format: format) else {
			return nil
		}

		defer {
			sourceBuffer.free()
		}
		
		var histogramBinZero  = [vImagePixelCount](repeating: 0, count: 256)
		
		histogramBinZero.withUnsafeMutableBufferPointer { zeroPtr in
			let error = vImageHistogramCalculation_Planar8(&sourceBuffer,
														   zeroPtr.baseAddress!,
														   vImage_Flags(kvImageNoFlags))

			guard error == kvImageNoError else {
				fatalError("Error calculating histogram: \(error)")
			}
		}
		
		return [Channel.l : histogramBinZero]
	}
	
	
	func histogramColor() async -> [Channel : [UInt]]? {
		let format = vImage_CGImageFormat(
			bitsPerComponent: 8,
			bitsPerPixel: 32,
			colorSpace: CGColorSpaceCreateDeviceRGB(),
			bitmapInfo: CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue),
			renderingIntent: .defaultIntent)!

		guard var sourceBuffer = try? vImage_Buffer(cgImage: self, format: format) else {
			return nil
		}

		defer {
			sourceBuffer.free()
		}

		var histogramBinZero  = [vImagePixelCount](repeating: 0, count: 256)
		var histogramBinOne   = [vImagePixelCount](repeating: 0, count: 256)
		var histogramBinTwo   = [vImagePixelCount](repeating: 0, count: 256)
		var histogramBinThree = [vImagePixelCount](repeating: 0, count: 256)

		histogramBinZero.withUnsafeMutableBufferPointer { zeroPtr in
			histogramBinOne.withUnsafeMutableBufferPointer { onePtr in
				histogramBinTwo.withUnsafeMutableBufferPointer { twoPtr in
					histogramBinThree.withUnsafeMutableBufferPointer { threePtr in

						var histogramBins = [zeroPtr.baseAddress, onePtr.baseAddress,
											 twoPtr.baseAddress, threePtr.baseAddress]

						histogramBins.withUnsafeMutableBufferPointer { histogramBinsPtr in
							
							let error = vImageHistogramCalculation_ARGB8888(&sourceBuffer,
																			histogramBinsPtr.baseAddress!,
																			vImage_Flags(kvImageNoFlags))

							guard error == kvImageNoError else {
								fatalError("Error calculating histogram: \(error)")
							}
						}
					}
				}
			}
		}

		return [Channel.r : histogramBinZero, Channel.g : histogramBinOne, Channel.b : histogramBinTwo]
	}
}
