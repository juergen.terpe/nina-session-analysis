//
//  ImageStaticsValues.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 13.10.24.
//

import Foundation
import CoreImage


struct ImageStatisticsValues: Identifiable {
	let id = UUID()
	let channel: Channel
	let mean: Float
	let avgDev: Float
	let median: UInt16
	let mad: UInt16
	let min: UInt16
	let max: UInt16
	
}


extension ImageStatisticsValues {
	
	static func from(image: AstroImage) async -> [ImageStatisticsValues] {
		let imageProcessor = ImageProcessor()
		let stat = imageProcessor.createStatistics(from: image)
		
		return stat
	}
}
