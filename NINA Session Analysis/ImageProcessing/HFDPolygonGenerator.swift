//
//  HFDPolygonGenerator.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 12.08.24.
//

import CoreGraphics


struct Polygon {
	var vertices: [CGPoint]
	var hfd: Float
}

class HFDPolygonGenerator {
	private var stars: [DetectedStar]
	private var imageWidth: CGFloat
	private var imageHeight: CGFloat
	private var hfdThreshold: Float
	private var maxLevels: Int
	
	init(stars: [DetectedStar], imageWidth: CGFloat, imageHeight: CGFloat, hfdThreshold: Float = 0.2, maxLevels: Int = 6) {
		self.stars = stars.sorted { $0.hfd < $1.hfd }
		self.imageWidth = imageWidth
		self.imageHeight = imageHeight
		self.hfdThreshold = hfdThreshold
		self.maxLevels = maxLevels
	}

	func process() -> [Polygon] {
		var polygons: [Polygon] = []
		var processedStars: Set<CGPoint> = []
		
		while let seedStar = findNextSeedStar(excluding: processedStars) {
			var polygonVertices: [CGPoint] = []
			var currentStars: Set<CGPoint> = [seedStar.position]
			var currentHFD = seedStar.hfd
			
			var frontier: [(CGPoint, CGPoint)] = findTriangleEdges(around: seedStar.position, in: currentStars)

			while !frontier.isEmpty {
				var newFrontier: [(CGPoint, CGPoint)] = []
				
				for edge in frontier {
					if let newStar = findNextStar(for: edge, within: currentHFD + hfdThreshold, excluding: currentStars) {
						let newVertex = newStar.position
						currentStars.insert(newVertex)
						polygonVertices.append(newVertex)
						newFrontier.append((edge.0, newVertex))
						newFrontier.append((newVertex, edge.1))
					}
				}
				
				frontier = newFrontier
			}
			
			if !polygonVertices.isEmpty {
				polygons.append(Polygon(vertices: convexHull(of: Array(currentStars)), hfd: currentHFD))
				processedStars.formUnion(currentStars)
			}
		}
		
		return polygons
	}

	private func findNextSeedStar(excluding processed: Set<CGPoint>) -> DetectedStar? {
		return stars.first { !processed.contains($0.position) }
	}
	
	private func findTriangleEdges(around point: CGPoint, in starSet: Set<CGPoint>) -> [(CGPoint, CGPoint)] {
		var nearestEdges: [(CGPoint, CGPoint)] = []
		var nearestStars = stars.filter { !starSet.contains($0.position) }
		
		if nearestStars.count < 2 { return [] }
		
		nearestStars.sort { distance($0.position, point) < distance($1.position, point) }
		
		let firstStar = nearestStars[0]
		let secondStar = nearestStars[1]
		
		nearestEdges.append((point, firstStar.position))
		nearestEdges.append((firstStar.position, secondStar.position))
		nearestEdges.append((secondStar.position, point))
		
		return nearestEdges
	}

	private func findNextStar(for edge: (CGPoint, CGPoint), within hfdLimit: Float, excluding starSet: Set<CGPoint>) -> DetectedStar? {
		return stars.first { !starSet.contains($0.position) && $0.hfd <= hfdLimit }
	}
	
	private func convexHull(of points: [CGPoint]) -> [CGPoint] {
		guard points.count > 2 else { return points }

		let sortedPoints = points.sorted { $0.x < $1.x || ($0.x == $1.x && $0.y < $1.y) }
		
		var lowerHull: [CGPoint] = []
		for point in sortedPoints {
			while lowerHull.count >= 2 && crossProduct(lowerHull[lowerHull.count - 2], lowerHull[lowerHull.count - 1], point) <= 0 {
				lowerHull.removeLast()
			}
			lowerHull.append(point)
		}
		
		var upperHull: [CGPoint] = []
		for point in sortedPoints.reversed() {
			while upperHull.count >= 2 && crossProduct(upperHull[upperHull.count - 2], upperHull[upperHull.count - 1], point) <= 0 {
				upperHull.removeLast()
			}
			upperHull.append(point)
		}
		
		lowerHull.removeLast()
		upperHull.removeLast()
		
		return lowerHull + upperHull
	}
	
	private func crossProduct(_ a: CGPoint, _ b: CGPoint, _ c: CGPoint) -> CGFloat {
		return (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x)
	}
	
	private func distance(_ p1: CGPoint, _ p2: CGPoint) -> CGFloat {
		return sqrt(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2))
	}
}
