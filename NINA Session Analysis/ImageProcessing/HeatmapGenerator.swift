//
//  HeatmapGenerator.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 14.08.24.
//

import CoreGraphics
import Cocoa


import CoreGraphics
import Cocoa
import SwiftUI



class HFDHeatmapGenerator {
	private var stars: [DetectedStar]
	private var imageWidth: CGFloat
	private var imageHeight: CGFloat
	private var tileSize: CGFloat
	private var maxHFD: Float = 8.0 // Maximale HFD-Schwelle
	private var colorThresholds: [Float] = [0.2, 0.5, 1.0, 1.5, 2.0] // Beispielschwellenwerte
	
	init(stars: [DetectedStar], imageWidth: CGFloat, imageHeight: CGFloat, tileSize: CGFloat = 20.0) {
		self.stars = stars
		self.imageWidth = imageWidth
		self.imageHeight = imageHeight
		self.tileSize = tileSize
	}
	
	func generateHeatmap() -> CGImage? {
		let rows = Int(ceil(imageHeight / tileSize))
		let cols = Int(ceil(imageWidth / tileSize))
		
		var tileHFDValues = Array(repeating: [Float](), count: rows * cols)
		
		// Zuordnung der Sterne zu Kacheln und Median-Filterung
		for star in stars {
			if star.hfd > maxHFD { continue } // Outlier Rejection
			let row = min(rows - 1, max(0, Int(star.position.y / tileSize)))
			let col = min(cols - 1, max(0, Int(star.position.x / tileSize)))
			tileHFDValues[row * cols + col].append(star.hfd)
		}
		
		// Berechnung des minimalen HFD-Werts im gesamten Bild
		var minHFD: Float = Float.greatestFiniteMagnitude
		for values in tileHFDValues {
			if let minValue = values.min() {
				minHFD = min(minHFD, minValue)
			}
		}
		
		// Berechnung des durchschnittlichen HFD-Werts für jede Kachel
		var averageHFDs = [Float](repeating: 0.0, count: rows * cols)
		for (i, values) in tileHFDValues.enumerated() {
			if values.count >= 3 { // Median-Filter: mindestens 3 Sterne erforderlich
				let sortedValues = values.sorted()
				averageHFDs[i] = sortedValues[sortedValues.count / 2] // Median
			} else {
				averageHFDs[i] = -1.0 // Kennzeichne leere oder fast leere Kacheln
			}
		}
		
		// Interpolation von Werten für leere Kacheln
		for row in 0..<rows {
			for col in 0..<cols {
				let index = row * cols + col
				if averageHFDs[index] == -1.0 {
					averageHFDs[index] = interpolateHFD(row: row, col: col, rows: rows, cols: cols, averageHFDs: averageHFDs)
				}
			}
		}
		
		// Bild erzeugen
		guard let context = CGContext(
			data: nil,
			width: Int(imageWidth),
			height: Int(imageHeight),
			bitsPerComponent: 8,
			bytesPerRow: Int(imageWidth) * 4,
			space: CGColorSpaceCreateDeviceRGB(),
			bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue
		) else {
			return nil
		}
		
		// Farben für jede Kachel bestimmen und mit Gradienten arbeiten
		for row in 0..<rows {
			for col in 0..<cols {
				let avgHFD = averageHFDs[row * cols + col]
				let color = colorForHFD(avgHFD - minHFD) // Differenz zum minimalen HFD
				
				let rect = CGRect(x: CGFloat(col) * tileSize, y: CGFloat(row) * tileSize, width: tileSize, height: tileSize)
				context.setFillColor(color.cgColor)
				context.fill(rect)
				
				// Farbverlauf berechnen (einfache Variante)
				if row < rows - 1 || col < cols - 1 {
					let nextRowHFD = averageHFDs[min(row + 1, rows - 1) * cols + col]
					let nextColHFD = averageHFDs[row * cols + min(col + 1, cols - 1)]
					let gradientColors = [color.cgColor, colorForHFD((nextRowHFD + nextColHFD) / 2 - minHFD).cgColor]
					let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: gradientColors as CFArray, locations: [0, 1])!
					context.drawLinearGradient(gradient, start: rect.origin, end: CGPoint(x: rect.maxX, y: rect.maxY), options: [])
				}
			}
		}
		
		return context.makeImage()
	}
	
	private func interpolateHFD(row: Int, col: Int, rows: Int, cols: Int, averageHFDs: [Float]) -> Float {
		var sum: Float = 0.0
		var count: Int = 0
		
		for i in max(0, row-1)...min(rows-1, row+1) {
			for j in max(0, col-1)...min(cols-1, col+1) {
				let neighborIndex = i * cols + j
				if averageHFDs[neighborIndex] > 0 {
					sum += averageHFDs[neighborIndex]
					count += 1
				}
			}
		}
		
		return count > 0 ? sum / Float(count) : 0.0
	}
	
	private func colorForHFD(_ hfdDiff: Float) -> NSColor {
		switch hfdDiff {
		case ..<colorThresholds[0]:
			return NSColor.black
		case ..<colorThresholds[1]:
			return NSColor.darkGray
		case ..<colorThresholds[2]:
			return NSColor.blue
		case ..<colorThresholds[3]:
			return NSColor.orange
		case ..<colorThresholds[4]:
			return NSColor.red
		default:
			return NSColor.red
		}
	}
}





