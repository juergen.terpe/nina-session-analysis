//
//  ImageProcessor.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 24.07.24.
//

import Metal
import MetalKit

fileprivate struct ParamsHistogram {
	fileprivate var imageSize: SIMD2<UInt32>
	fileprivate var bitDepth: Int
}

fileprivate struct ParamsMedian {
	fileprivate var pixelCount: UInt
}

fileprivate struct ParamsStretch {
	fileprivate var imageSize: SIMD2<UInt32>
	fileprivate var stretch: SIMD3<Float>
	fileprivate var shadows: SIMD3<Float>
	fileprivate var bitDepth: Int
}

fileprivate struct ParamsGrayStretch {
	fileprivate var imageSize: SIMD2<UInt32>
	fileprivate var stretch: Float
	fileprivate var shadows: Float
	fileprivate var bitDepth: Int
}


struct ImageProcessorTexture {
	let texture: MTLTexture
	let width: Int
	let height: Int
	let bitDepth: Int
}


class ImageProcessor {
	var device: MTLDevice!
	var commandQueue: MTLCommandQueue!
	var histogramPipelineState: MTLComputePipelineState!
	var histogramGrayscalePipelineState: MTLComputePipelineState!
	var medianPipelineState: MTLComputePipelineState!
	var stretchPipelineState: MTLComputePipelineState!
	var stretchGrayPipelineState: MTLComputePipelineState!
	var linearFitPipelineState: MTLComputePipelineState!
	var debayerPipelineState: MTLComputePipelineState!
	var bigEndianPipelineState: MTLComputePipelineState!
	var hfrPipelineState: MTLComputePipelineState!
	var localStarThresholdPipelineState: MTLComputePipelineState!
	var fluxShapeAndPSFFunctionPipelineState: MTLComputePipelineState!
	var starDetectorPipelineState: MTLComputePipelineState!
	
	init() {
		self.device = MTLCreateSystemDefaultDevice()!
		self.commandQueue = device.makeCommandQueue()
		
		guard let library = device.makeDefaultLibrary() else {
			fatalError("Unable to create default library")
		}
		
		let histogramFunction = library.makeFunction(name: "histogramKernel")
		let histogramGrayscaleFunction = library.makeFunction(name: "histogramGrayscaleKernel")
		let medianFunction = library.makeFunction(name: "medianAndStdDevKernel")
		let stretchFunction = library.makeFunction(name: "mtfStretchKernel")
		let stretchGrayFunction = library.makeFunction(name: "mtfStretchGrayKernel")
		let linearFitFunction = library.makeFunction(name: "linearFitKernel")
		let debayerFunction = library.makeFunction(name: "debayer")
		let bigEndianFunction = library.makeFunction(name: "bigEndianKernel")
		
		do {
			self.histogramPipelineState = try device.makeComputePipelineState(function: histogramFunction!)
			self.histogramGrayscalePipelineState = try device.makeComputePipelineState(function: histogramGrayscaleFunction!)
			self.medianPipelineState = try device.makeComputePipelineState(function: medianFunction!)
			self.stretchPipelineState = try device.makeComputePipelineState(function: stretchFunction!)
			self.stretchGrayPipelineState = try device.makeComputePipelineState(function: stretchGrayFunction!)
			self.linearFitPipelineState = try device.makeComputePipelineState(function: linearFitFunction!)
			self.debayerPipelineState = try device.makeComputePipelineState(function: debayerFunction!)
			self.bigEndianPipelineState = try device.makeComputePipelineState(function: bigEndianFunction!)
		} catch {
			fatalError("Unable to create pipeline states: \(error)")
		}
	}
	
	private func createMTLTexture(from imageData: [UInt16], width: Int, height: Int, bitDepth: Int) -> MTLTexture? {
		let pixelFormat: MTLPixelFormat = .r16Uint // Monochrome 16-Bit
		
		let textureDescriptor = MTLTextureDescriptor.texture2DDescriptor(
			pixelFormat: pixelFormat,
			width: width,
			height: height,
			mipmapped: false
		)
		textureDescriptor.usage = [.shaderRead, .shaderWrite]
		
		guard let texture = device.makeTexture(descriptor: textureDescriptor) else {
			print("Failed to create texture")
			return nil
		}
		
		let bytesPerRow = width * MemoryLayout<UInt16>.size
		let region = MTLRegionMake2D(0, 0, width, height)
		texture.replace(region: region, mipmapLevel: 0, withBytes: imageData, bytesPerRow: bytesPerRow)
		
		return texture
	}
	
	func createTexture(from imageData: [UInt16], width: Int, height: Int, bitDepth: Int) -> ImageProcessorTexture? {
		guard let texture = createMTLTexture(from: imageData, width: width, height: height, bitDepth: bitDepth) else {
			return nil
		}
		
		return ImageProcessorTexture(texture: texture, width: width, height: height, bitDepth: bitDepth)
	}
	
	func createTexture(from image: CGImage, bitDepth: Int) -> ImageProcessorTexture? {
		
		let width = image.width
		let height = image.height
		
		if image.isColorImage() {
			guard let texture = createRGBTexture(from: image) else {
				return nil
			}
			
			return ImageProcessorTexture(texture: texture, width: width, height: height, bitDepth: bitDepth)
		} else {
			
			let bitsPerComponent = 16
			let bytesPerPixel = 2 // UInt16
			let bytesPerRow = width * bytesPerPixel
			
			// Create a CGContext to extract pixel data
			guard let colorSpace = CGColorSpace(name: CGColorSpace.linearGray),
				  let context = CGContext(data: nil, width: width, height: height, bitsPerComponent: bitsPerComponent,
										  bytesPerRow: bytesPerRow, space: colorSpace,
										  bitmapInfo: CGImageAlphaInfo.none.rawValue) else {
				return nil
			}
			
			// Draw the CGImage into the CGContext
			context.draw(image, in: CGRect(x: 0, y: 0, width: width, height: height))
			
			// Extract pixel data from CGContext
			guard let pixelData = context.data else { return nil }
			
			// Create a Metal texture from pixel data
			let textureDescriptor = MTLTextureDescriptor()
			textureDescriptor.pixelFormat = .r16Uint
			textureDescriptor.width = width
			textureDescriptor.height = height
			textureDescriptor.usage = [.shaderRead, .shaderWrite]
			
			guard let inputTexture = device.makeTexture(descriptor: textureDescriptor) else { return nil }
			inputTexture.replace(region: MTLRegionMake2D(0, 0, width, height), mipmapLevel: 0, withBytes: pixelData, bytesPerRow: bytesPerRow)
			
			return ImageProcessorTexture(texture: inputTexture, width: width, height: height, bitDepth: bitDepth)
		}
	}
	
	func createRGBTexture(from image: CGImage) -> MTLTexture? {
		// Bildgröße und Pixelformat
		let width = image.width
		let height = image.height
		let pixelFormat: MTLPixelFormat = .rgba16Uint // 16-Bit per channel
		
		// Erstellen eines neuen MTLTextureDescriptor
		let textureDescriptor = MTLTextureDescriptor.texture2DDescriptor(pixelFormat: pixelFormat, width: width, height: height, mipmapped: false)
		textureDescriptor.usage = [.shaderRead, .shaderWrite]
		
		// Erstellen der MTLTexture
		guard let texture = device.makeTexture(descriptor: textureDescriptor) else {
			print("Failed to create texture")
			return nil
		}
		
		// Bytes per row berechnen
		let bytesPerRow = width * 8 // 8 Bytes per pixel (16 bits)
		
		
		// Byte-Puffer für Bilddaten erstellen
		guard let context = CGContext(data: nil,
									  width: width,
									  height: height,
									  bitsPerComponent: 16,
									  bytesPerRow: bytesPerRow,
									  space: CGColorSpaceCreateDeviceRGB(),
									  bitmapInfo: CGImageAlphaInfo.noneSkipLast.rawValue | CGBitmapInfo.byteOrder16Little.rawValue) else {
			print("Failed to create CGContext")
			return nil
		}
		
		// Bild in den Kontext zeichnen
		context.draw(image, in: CGRect(x: 0, y: 0, width: width, height: height))
		
		// Bilddaten extrahieren
		guard let data = context.data else {
			print("Failed to get image data from context")
			return nil
		}
		
		// Daten in die Textur kopieren
		let region = MTLRegionMake2D(0, 0, width, height)
		texture.replace(region: region, mipmapLevel: 0, withBytes: data, bytesPerRow: bytesPerRow)
		
		return texture
	}
	
	func createGrayscaleTexture(from image: CGImage) -> MTLTexture? {
		// Bildgröße und Pixelformat
		let width = image.width
		let height = image.height
		let pixelFormat: MTLPixelFormat = .r16Uint // 16-Bit Graustufen

		// Erstellen eines neuen MTLTextureDescriptor
		let textureDescriptor = MTLTextureDescriptor.texture2DDescriptor(pixelFormat: pixelFormat, width: width, height: height, mipmapped: false)
		textureDescriptor.usage = [.shaderRead, .shaderWrite]

		// Erstellen der MTLTexture
		guard let texture = device.makeTexture(descriptor: textureDescriptor) else {
			print("Failed to create texture")
			return nil
		}

		// Bytes per row berechnen
		let bytesPerRow = width * 2 // 2 Bytes per pixel (16 bits)

		// Byte-Puffer für Bilddaten erstellen
		guard let context = CGContext(data: nil,
									  width: width,
									  height: height,
									  bitsPerComponent: 16,
									  bytesPerRow: bytesPerRow,
									  space: CGColorSpaceCreateDeviceGray(),
									  bitmapInfo: CGImageAlphaInfo.none.rawValue | CGBitmapInfo.byteOrder16Little.rawValue) else {
			print("Failed to create CGContext")
			return nil
		}

		// Bild in den Kontext zeichnen
		context.draw(image, in: CGRect(x: 0, y: 0, width: width, height: height))

		// Bilddaten extrahieren
		guard let data = context.data else {
			print("Failed to get image data from context")
			return nil
		}

		// Daten in die Textur kopieren
		let region = MTLRegionMake2D(0, 0, width, height)
		texture.replace(region: region, mipmapLevel: 0, withBytes: data, bytesPerRow: bytesPerRow)

		return texture
	}
	
	private func convertToBigEndian(_ imageTexture: ImageProcessorTexture, _ bigEndian: Bool) -> MTLTexture? {
		if bigEndian {
			guard let texture = bigEndianTransform(inputTexture: imageTexture.texture,
												   width: imageTexture.width, height: imageTexture.height) else {
				return nil
			}
			
			return texture
		}
		return imageTexture.texture
	}
	
	
	func createImage(from imageTexture: ImageProcessorTexture, bigEndian: Bool = true) -> CGImage? {
		
		guard let texture = convertToBigEndian(imageTexture, bigEndian) else {
			return nil
		}
		
		let width = imageTexture.width
		let height = imageTexture.height
		
		if imageTexture.texture.pixelFormat == .rgba16Uint {
			let bytesPerPixel = 8 // 4 * UInt16
			let bytesPerRow = bytesPerPixel * width
			let data = UnsafeMutableRawPointer.allocate(byteCount: bytesPerRow * height, alignment: 1)
			texture.getBytes(data, bytesPerRow: bytesPerRow, from: MTLRegionMake2D(0, 0, width, height), mipmapLevel: 0)
			
			let colorSpace = CGColorSpaceCreateDeviceRGB()
			let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.noneSkipLast.rawValue)
			guard let context = CGContext(data: data, width: width, height: height, bitsPerComponent: 16,
										  bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo.rawValue) else {
				data.deallocate()
				return nil
			}
			
			let image = context.makeImage()
			data.deallocate()
			return image
		}
		else if imageTexture.texture.pixelFormat == .r16Uint {
			
			let bytesPerPixel = 2 // 1 * UInt16
			let bytesPerRow = bytesPerPixel * width
			let data = UnsafeMutableRawPointer.allocate(byteCount: bytesPerRow * height, alignment: 1)
			texture.getBytes(data, bytesPerRow: bytesPerRow, from: MTLRegionMake2D(0, 0, width, height), mipmapLevel: 0)
			
			let colorSpace = CGColorSpaceCreateDeviceGray()
			let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.none.rawValue)
			guard let context = CGContext(data: data, width: width, height: height, bitsPerComponent: 16,
										  bytesPerRow: bytesPerRow, space: colorSpace, bitmapInfo: bitmapInfo.rawValue) else {
				data.deallocate()
				return nil
			}
			
			let image = context.makeImage()
			data.deallocate()
			return image
			
		}
		
		return nil 
	}
	
	
	func debayer(image: ImageProcessorTexture, bayerPattern: BayerMatrix) -> ImageProcessorTexture? {
		
		if bayerPattern == .none {
			return image 
		}
		
		
		// Create an empty texture for the output
		guard let outputTexture = makeEmptyTexture(width: image.width, height: image.height,
												   format: .rgba16Uint) else {
			return nil
		}
		
		// Set up the Metal command buffer and encoder
		guard let commandBuffer = commandQueue.makeCommandBuffer(),
			  let commandEncoder = commandBuffer.makeComputeCommandEncoder() else {
			return nil
		}
		
		commandEncoder.setComputePipelineState(self.debayerPipelineState)
		commandEncoder.setTexture(image.texture, index: 0)
		commandEncoder.setTexture(outputTexture, index: 1)
		
		// Set the bayer pattern type
		var patternType = bayerPatternIndex(bayerPattern)
		commandEncoder.setBytes(&patternType, length: MemoryLayout<Int>.size, index: 0)
		
		let threadGroupSize = MTLSize(width: 8, height: 8, depth: 1)
		let threadGroups = MTLSize(width: (image.width + threadGroupSize.width - 1) / threadGroupSize.width,
								   height: (image.height + threadGroupSize.height - 1) / threadGroupSize.height,
								   depth: 1)
		
		commandEncoder.dispatchThreadgroups(threadGroups, threadsPerThreadgroup: threadGroupSize)
		commandEncoder.endEncoding()
		commandBuffer.commit()
		commandBuffer.waitUntilCompleted()
		
		return ImageProcessorTexture(texture: outputTexture, width: image.width, height: image.height, bitDepth: image.bitDepth)
	}
	
	private func makeEmptyTexture(width: Int, height: Int, format: MTLPixelFormat) -> MTLTexture? {
		let textureDescriptor = MTLTextureDescriptor()
		textureDescriptor.pixelFormat = format
		textureDescriptor.width = width
		textureDescriptor.height = height
		textureDescriptor.usage = [.shaderRead, .shaderWrite]
		return device.makeTexture(descriptor: textureDescriptor)
	}
	
	private func bayerPatternIndex(_ pattern: BayerMatrix) -> Int {
		switch pattern {
			case .none: return 0
			case .rggb: return 1
			case .rgbg: return 2
			case .grgb: return 3
			case .grbg: return 4
			case .gbgr: return 5
			case .gbrg: return 6
			case .bgrg: return 7
			case .bggr: return 8
		}
	}
	
	private func midtonesTransferFunction(midToneBalance: Float,
										  _ x: Float) -> Float {
		if x > 0.0 {
			if x < 1.0 {
				return min(1.0, (midToneBalance - 1) * x / ((2 * midToneBalance - 1) * x - midToneBalance))
			}
			return 1.0
		}
		return 0.0
	}
	
	
	func stretch(image: ImageProcessorTexture, targetBackground: Float) -> ImageProcessorTexture? {
		
		
		if let stretchedTexture = self.stretchTexture(inputTexture: image.texture,
										   width: image.width, height: image.height,
													  targetMedian: targetBackground, bitDepth: image.bitDepth) {
			
			return ImageProcessorTexture(texture: stretchedTexture,
										 width: image.width, height: image.height, bitDepth: image.bitDepth)
		}
		
		return nil
	}
	
	
	private func bigEndianTransform(inputTexture: MTLTexture, width: Int, height: Int) -> MTLTexture? {
		let descriptor = MTLTextureDescriptor.texture2DDescriptor(pixelFormat: inputTexture.pixelFormat,
																  width: width, height: height, mipmapped: false)
		descriptor.usage = [.shaderRead, .shaderWrite]
		guard let outputTexture = createTexture(descriptor: descriptor) else {
			print("Error creating output texture")
			return nil
		}
		
		let commandBuffer = commandQueue.makeCommandBuffer()!
		let commandEncoder = commandBuffer.makeComputeCommandEncoder()!
		commandEncoder.setComputePipelineState(bigEndianPipelineState)
		
		commandEncoder.setTexture(inputTexture, index: 0)
		commandEncoder.setTexture(outputTexture, index: 1)
		
		let threadsPerThreadgroup = MTLSize(width: 16, height: 16, depth: 1)
		let threadgroupsPerGrid = MTLSize(width: (width + 15) / 16, height: (height + 15) / 16, depth: 1)
		commandEncoder.dispatchThreadgroups(threadgroupsPerGrid, threadsPerThreadgroup: threadsPerThreadgroup)
		
		commandEncoder.endEncoding()
		commandBuffer.commit()
		commandBuffer.waitUntilCompleted()
		
		return outputTexture
	}
	
	private func createTexture(descriptor: MTLTextureDescriptor) -> MTLTexture? {
		return device.makeTexture(descriptor: descriptor)
	}
	
	private func computeHistogram(inputTexture: MTLTexture, width: Int, height: Int, bitDepth: Int) -> (MTLBuffer, MTLBuffer, MTLBuffer) {
		let histogramR = device.makeBuffer(length: 65536 * MemoryLayout<UInt32>.size, options: .storageModeShared)!
		let histogramG = device.makeBuffer(length: 65536 * MemoryLayout<UInt32>.size, options: .storageModeShared)!
		let histogramB = device.makeBuffer(length: 65536 * MemoryLayout<UInt32>.size, options: .storageModeShared)!
		memset(histogramR.contents(), 0, 65536 * MemoryLayout<UInt32>.size)
		memset(histogramG.contents(), 0, 65536 * MemoryLayout<UInt32>.size)
		memset(histogramB.contents(), 0, 65536 * MemoryLayout<UInt32>.size)
		
		let commandBuffer = commandQueue.makeCommandBuffer()!
		let commandEncoder = commandBuffer.makeComputeCommandEncoder()!
		commandEncoder.setComputePipelineState(histogramPipelineState)
		
		var params = ParamsHistogram(imageSize: SIMD2<UInt32>(UInt32(width), UInt32(height)), bitDepth: bitDepth)
		commandEncoder.setBytes(&params, length: MemoryLayout<ParamsHistogram>.size, index: 0)
		commandEncoder.setTexture(inputTexture, index: 0)
		commandEncoder.setBuffer(histogramR, offset: 0, index: 1)
		commandEncoder.setBuffer(histogramG, offset: 0, index: 2)
		commandEncoder.setBuffer(histogramB, offset: 0, index: 3)
		
		let threadsPerThreadgroup = MTLSize(width: 16, height: 16, depth: 1)
		let threadgroupsPerGrid = MTLSize(width: (width + 15) / 16, height: (height + 15) / 16, depth: 1)
		commandEncoder.dispatchThreadgroups(threadgroupsPerGrid, threadsPerThreadgroup: threadsPerThreadgroup)
		
		commandEncoder.endEncoding()
		commandBuffer.commit()
		commandBuffer.waitUntilCompleted()
		
		return (histogramR, histogramG, histogramB)
	}
	
	private func computeHistogramGrayscale(inputTexture: MTLTexture, width: Int, height: Int, bitDepth: Int) -> MTLBuffer {
		let histogram = device.makeBuffer(length: 65536 * MemoryLayout<UInt32>.size, options: .storageModeShared)!
		memset(histogram.contents(), 0, 65536 * MemoryLayout<UInt32>.size)
		
		let commandBuffer = commandQueue.makeCommandBuffer()!
		let commandEncoder = commandBuffer.makeComputeCommandEncoder()!
		commandEncoder.setComputePipelineState(histogramGrayscalePipelineState)
		
		var params = ParamsHistogram(imageSize: SIMD2<UInt32>(UInt32(width), UInt32(height)), bitDepth: bitDepth)
		commandEncoder.setBytes(&params, length: MemoryLayout<ParamsHistogram>.size, index: 0)
		commandEncoder.setTexture(inputTexture, index: 0)
		commandEncoder.setBuffer(histogram, offset: 0, index: 1)
		
		let threadsPerThreadgroup = MTLSize(width: 16, height: 16, depth: 1)
		let threadgroupsPerGrid = MTLSize(width: (width + 15) / 16, height: (height + 15) / 16, depth: 1)
		commandEncoder.dispatchThreadgroups(threadgroupsPerGrid, threadsPerThreadgroup: threadsPerThreadgroup)
		
		commandEncoder.endEncoding()
		commandBuffer.commit()
		commandBuffer.waitUntilCompleted()
		
		return histogram
	}
		
	private func calculateMedian(from histogram: MTLBuffer, pixelCount: Int) -> Float {
		let halfTotalPixels = pixelCount / 2
		var median1: Int = 0
		var median2: Int = 0
		var cumulativeSum: UInt32 = 0
		
		let histogramData = histogram.contents().assumingMemoryBound(to: UInt32.self)
		
		for index in 0..<65536 {
			let count = histogramData[index]
			cumulativeSum += count
			if cumulativeSum > halfTotalPixels {
				median1 = index
				median2 = index
				break
			} else if cumulativeSum == halfTotalPixels {
				median1 = index
				for index2 in (index + 1)..<65536 {
					if histogramData[index2] > 0 {
						median2 = index2
						break
					}
				}
			}
		}
		
		let median = Float(median1 + median2) / 2.0
		return median/65535.0
	}
	
	private func calculateMean(from histogram: MTLBuffer, pixelCount: Int) -> (mean: Float, avgDev: Float) {
		let histogramData = histogram.contents().assumingMemoryBound(to: UInt32.self)
		var calculativeSum: Float = 0.0
		var totalCount: Int = 0
		
		for index in 0..<65536 {
			let count = histogramData[index]
			calculativeSum += Float(count) * Float(index)
			totalCount += Int(count)
		}
		
		let mean = calculativeSum / Float(totalCount)
		
		calculativeSum = 0.0
		for index in 0..<65536 {
			let count = histogramData[index]
			let values = Float(count) * Float(index)
			calculativeSum += abs(values - Float(count) * mean)
		}
		let avgDev = calculativeSum / Float(totalCount)
		return (mean: mean, avgDev: avgDev)
	}
	
	private func calculateMinMax(from histogram: MTLBuffer, pixelCount: Int) -> (UInt16, UInt16) {
		var min: Int = 0
		var max: Int = 0
		
		let histogramData = histogram.contents().assumingMemoryBound(to: UInt32.self)
		
		for index in 0..<65536 {
			if histogramData[index] > 0 {
				if min == 0 {
					min = index
				}
				max = index
			}
		}
		
		return (min: UInt16(min), max: UInt16(max))
	}
	
	
	private func calculateMedianAndStdDev(from histogram: MTLBuffer, pixelCount: Int) -> (UInt16, UInt16) {
		let halfTotalPixels = pixelCount / 2
		
		var median1: Int = 0
		var median2: Int = 0
		var cumulativeSum: UInt32 = 0
		
		 
		let histogramData = histogram.contents().assumingMemoryBound(to: UInt32.self)
		
		for index in 0..<65536 {
			cumulativeSum += histogramData[index]
			if cumulativeSum > halfTotalPixels {
				median1 = index
				median2 = index
				break
			} else if cumulativeSum == halfTotalPixels {
				median1 = index
				for index2 in (index + 1)..<65536 {
					if histogramData[index2] > 0 {
						median2 = index2
						break
					}
				}
			}
		}
		
		let median = (median1 + median2) / 2
		var sum: Double = 0.0
		for index in 0..<65536 {
			let count = histogramData[index]
			let delta = Double(abs(index - median))
			sum += Double(count) * delta
		}
		
		let medianAbsoluteDeviation = sum / Double(pixelCount)
		
		return (UInt16(median), UInt16(medianAbsoluteDeviation + 0.5))
	}

	
	private func stretchTexture(inputTexture: MTLTexture, width: Int, height: Int,
								targetMedian: Float, bitDepth: Int) -> MTLTexture? {
		let shadowsClipping: Float = -2.8
		let factor: Float = 1.4826
		
		let descriptor = MTLTextureDescriptor.texture2DDescriptor(pixelFormat: inputTexture.pixelFormat,
																  width: width, height: height, mipmapped: false)
		descriptor.usage = [.shaderRead, .shaderWrite]
		guard let outputTexture = createTexture(descriptor: descriptor) else {
			print("Error creating output texture")
			return nil
		}
		
		if inputTexture.pixelFormat == .rgba16Uint {
			let (histogramR, histogramG, histogramB) = computeHistogram(inputTexture: inputTexture,
																		width: width, height: height,
																		bitDepth: bitDepth)
			let widthHeight = width * height
			
			let (medianR, medDevR) = calculateMedianAndStdDev(from: histogramR, pixelCount: widthHeight)
			let (medianG, medDevG) = calculateMedianAndStdDev(from: histogramG, pixelCount: widthHeight)
			let (medianB, medDevB) = calculateMedianAndStdDev(from: histogramB, pixelCount: widthHeight)
			
			let commandBuffer = commandQueue.makeCommandBuffer()!
			let commandEncoder = commandBuffer.makeComputeCommandEncoder()!
			commandEncoder.setComputePipelineState(stretchPipelineState)
				
			let medR = Float(medianR)/65535.0
			let medG = Float(medianG)/65535.0
			let medB = Float(medianB)/65535.0
			
			let medianDevR = Float(medDevR)/65535.0
			let medianDevG = Float(medDevG)/65535.0
			let medianDevB = Float(medDevB)/65535.0
			
			let shadowsR = medR + shadowsClipping * medianDevR * factor
			let shadowsG = medG + shadowsClipping * medianDevG * factor
			let shadowsB = medB + shadowsClipping * medianDevB * factor
			let stretchR = midtonesTransferFunction(midToneBalance: targetMedian, medR - shadowsR)
			let stretchG = midtonesTransferFunction(midToneBalance: targetMedian, medG - shadowsG)
			let stretchB = midtonesTransferFunction(midToneBalance: targetMedian, medB - shadowsB)
			
			var params = ParamsStretch(imageSize: SIMD2<UInt32>(UInt32(width), UInt32(height)),
									   stretch: SIMD3<Float>(stretchR, stretchG, stretchB),
									   shadows: SIMD3<Float>(shadowsR, shadowsG, shadowsB),
									   bitDepth: bitDepth)
			
			commandEncoder.setBytes(&params, length: MemoryLayout<ParamsStretch>.stride, index: 0)
			commandEncoder.setTexture(inputTexture, index: 1)
			commandEncoder.setTexture(outputTexture, index: 2)
			
			let threadsPerThreadgroup = MTLSize(width: 16, height: 16, depth: 1)
			let threadgroupsPerGrid = MTLSize(width: (width + 15) / 16, height: (height + 15) / 16, depth: 1)
			commandEncoder.dispatchThreadgroups(threadgroupsPerGrid, threadsPerThreadgroup: threadsPerThreadgroup)
			
			commandEncoder.endEncoding()
			commandBuffer.commit()
			commandBuffer.waitUntilCompleted()
			
			return outputTexture
		}
		else if inputTexture.pixelFormat == .r16Uint {
			let histogram = computeHistogramGrayscale(inputTexture: inputTexture, width: width, height: height, bitDepth: bitDepth)
			let widthHeight = width * height
			
			let (median, medDev) = calculateMedianAndStdDev(from: histogram, pixelCount: widthHeight)
			let med = Float(median)/65535.0
			let mDev = Float(medDev)/65535.0
			
			let shadows = med + shadowsClipping * mDev * factor
			let stretch = midtonesTransferFunction(midToneBalance: targetMedian, med - shadows)
			
			let commandBuffer = commandQueue.makeCommandBuffer()!
			let commandEncoder = commandBuffer.makeComputeCommandEncoder()!
			commandEncoder.setComputePipelineState(stretchGrayPipelineState)
			
			var params = ParamsGrayStretch(imageSize: SIMD2<UInt32>(UInt32(width), UInt32(height)),
									   stretch: stretch,
									   shadows: shadows,
									   bitDepth: bitDepth)
			
			commandEncoder.setBytes(&params, length: MemoryLayout<ParamsGrayStretch>.stride, index: 0)
			commandEncoder.setTexture(inputTexture, index: 1)
			commandEncoder.setTexture(outputTexture, index: 2)
			
			let threadsPerThreadgroup = MTLSize(width: 16, height: 16, depth: 1)
			let threadgroupsPerGrid = MTLSize(width: (width + 15) / 16, height: (height + 15) / 16, depth: 1)
			commandEncoder.dispatchThreadgroups(threadgroupsPerGrid, threadsPerThreadgroup: threadsPerThreadgroup)
			
			commandEncoder.endEncoding()
			commandBuffer.commit()
			commandBuffer.waitUntilCompleted()
			
			return outputTexture
			
		}
		print("Texture not stretched...\(inputTexture.pixelFormat)")
		
		return inputTexture
	}
	
	private func linearFit(inputTexture: MTLTexture, width: Int, height: Int, bitDepth: Int) -> MTLTexture? {
		let descriptor = MTLTextureDescriptor.texture2DDescriptor(pixelFormat: .rgba16Uint, width: width, height: height, mipmapped: false)
		descriptor.usage = [.shaderRead, .shaderWrite]
		guard let outputTexture = createTexture(descriptor: descriptor) else {
			print("Error creating output texture")
			return nil
		}
		
		print("LinearFit: compute histogram...")
		let (histogramR, histogramG, histogramB) = computeHistogram(inputTexture: inputTexture,
																	width: width, height: height,
																	bitDepth: bitDepth)
		let widthHeight = width * height
		
		print("LinearFit: compute medians...")
		let medianR = calculateMedian(from: histogramR, pixelCount: widthHeight)
		print("LinearFit: median(R)=\(medianR)")
		let medianG = calculateMedian(from: histogramG, pixelCount: widthHeight)
		print("LinearFit: median(G)=\(medianG)")
		let medianB = calculateMedian(from: histogramB, pixelCount: widthHeight)
		print("LinearFit: median(B)=\(medianB)")
		
		let median = max(medianR, medianG, medianB)
		let offsetR = median - medianR
		let offsetG = median - medianG
		let offsetB = median - medianB
	
		print("offset: R=\(offsetR), G=\(offsetG), B=\(offsetB)")
		
		var offsets = SIMD3<Float>(offsetR, offsetG, offsetB)
		
		let commandBuffer = commandQueue.makeCommandBuffer()!
		let commandEncoder = commandBuffer.makeComputeCommandEncoder()!
		commandEncoder.setComputePipelineState(linearFitPipelineState)
		
		commandEncoder.setBytes(&offsets, length: MemoryLayout<SIMD3<Float>>.stride, index: 0)
		commandEncoder.setTexture(inputTexture, index: 1)
		commandEncoder.setTexture(outputTexture, index: 2)
		
		let threadsPerThreadgroup = MTLSize(width: 16, height: 16, depth: 1)
		let threadgroupsPerGrid = MTLSize(width: (width + 15) / 16, height: (height + 15) / 16, depth: 1)
		commandEncoder.dispatchThreadgroups(threadgroupsPerGrid, threadsPerThreadgroup: threadsPerThreadgroup)
		
		commandEncoder.endEncoding()
		commandBuffer.commit()
		commandBuffer.waitUntilCompleted()
		
		return outputTexture
	}
	 
}


extension ImageProcessor {
	
	private func getTexture(from image: AstroImage) -> ImageProcessorTexture? {
		if let cgImage = image.cgImage {
			return self.createTexture(from: cgImage, bitDepth: image.bitDepth)
		}
		if let imageData = image.imageData {
			return createTexture(from: imageData, width: image.width, height: image.height, bitDepth: image.bitDepth)
		}
		return nil 
	}
	
	
	func createStatistics(from image: AstroImage) -> [ImageStatisticsValues] {
		
		guard let texture = getTexture(from: image) else {
			return []
		}
		
		let pixelCount = texture.width * texture.height
		
		if let cgImage = image.cgImage, cgImage.isColorImage() {
			let (histR, histG, histB) = self.computeHistogram(inputTexture: texture.texture,
											 width: texture.width, height: texture.height, bitDepth: texture.bitDepth)
			
			let (medR, madR) = self.calculateMedianAndStdDev(from: histR, pixelCount: pixelCount)
			let (meanR, avgDevR) = self.calculateMean(from: histR, pixelCount: pixelCount)
			let (minR, maxR) = self.calculateMinMax(from: histR, pixelCount: pixelCount)
			let (medG, madG) = self.calculateMedianAndStdDev(from: histG, pixelCount: pixelCount)
			let (meanG, avgDevG) = self.calculateMean(from: histG, pixelCount: pixelCount)
			let (minG, maxG) = self.calculateMinMax(from: histG, pixelCount: pixelCount)
			let (medB, madB) = self.calculateMedianAndStdDev(from: histB, pixelCount: pixelCount)
			let (meanB, avgDevB) = self.calculateMean(from: histB, pixelCount: pixelCount)
			let (minB, maxB) = self.calculateMinMax(from: histB, pixelCount: pixelCount)
			
			return [
				ImageStatisticsValues(channel: .r, mean: meanR, avgDev: avgDevR, median: medR, mad: madR, min: minR, max: maxR),
				ImageStatisticsValues(channel: .g, mean: meanG, avgDev: avgDevG, median: medG, mad: madG, min: minG, max: maxG),
				ImageStatisticsValues(channel: .b, mean: meanB, avgDev: avgDevB, median: medB, mad: madB, min: minB, max: maxB)
			]
			
		} else {
			
			let hist = self.computeHistogramGrayscale(inputTexture: texture.texture,
										   width: texture.width, height: texture.height, bitDepth: texture.bitDepth)
			
			let (med, mad) = self.calculateMedianAndStdDev(from: hist, pixelCount: pixelCount)
			let (mean, avgDev) = self.calculateMean(from: hist, pixelCount: pixelCount)
			let (min, max) = self.calculateMinMax(from: hist, pixelCount: pixelCount)
			
			return [
				ImageStatisticsValues(channel: .r, mean: mean, avgDev: avgDev, median: med, mad: mad, min: min, max: max)
			]
		}
		
	}
}
