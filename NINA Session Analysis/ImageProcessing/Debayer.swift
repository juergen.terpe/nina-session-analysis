//
//  Debayer.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 09.07.24.
//
import CoreGraphics
import CoreImage
import Metal
import MetalPerformanceShaders

/
class DebayerFilter: CIFilter {
	
	var image: AstroImage?
	var bayerPattern: String = "RGGB"
	
	private var metalDevice: MTLDevice!
	private var metalCommandQueue: MTLCommandQueue!
	private var metalLibrary: MTLLibrary!
	private var metalFunction: MTLFunction!
	private var metalPipelineState: MTLComputePipelineState!
	
	override init() {
		super.init()
		metalDevice = MTLCreateSystemDefaultDevice()
		metalCommandQueue = metalDevice.makeCommandQueue()
		metalLibrary = metalDevice.makeDefaultLibrary()
		metalFunction = metalLibrary.makeFunction(name: "debayerRGGB") // Change function name based on bayerPattern
		metalPipelineState = try! metalDevice.makeComputePipelineState(function: metalFunction)
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override var outputImage: CIImage? {
		guard let inputImage = self.image else { return nil }
		
		// Extract FITS data
		guard let floatPixels = inputImage.getImageData(),
			  floatPixels.count > 0 else {
			return nil
		}
		
		let width = inputImage.width
		let height = inputImage.height
		
		// Metal processing
		let inputTextureDescriptor = MTLTextureDescriptor.texture2DDescriptor(pixelFormat: .r32Float,
																			  width: width, height: height,
																			  mipmapped: false)
		inputTextureDescriptor.usage = [.shaderRead, .shaderWrite]
		
		let outputTextureDescriptor = MTLTextureDescriptor.texture2DDescriptor(pixelFormat: .rgba32Float,
																			   width: width, height: height,
																			   mipmapped: false)
		outputTextureDescriptor.usage = [.shaderRead, .shaderWrite]
		
		guard let inputTexture = metalDevice.makeTexture(descriptor: inputTextureDescriptor),
			  let outputTexture = metalDevice.makeTexture(descriptor: outputTextureDescriptor) else {
			return nil
		}
		
		// Load input image data into the input texture
		let region = MTLRegionMake2D(0, 0, width, height)
		let bytesPerRow = width * MemoryLayout<Float>.size
		inputTexture.replace(region: region, mipmapLevel: 0, withBytes: floatPixels, bytesPerRow: bytesPerRow)
		
		let commandBuffer = metalCommandQueue.makeCommandBuffer()!
		let commandEncoder = commandBuffer.makeComputeCommandEncoder()!
		
		commandEncoder.setComputePipelineState(metalPipelineState)
		commandEncoder.setTexture(inputTexture, index: 0)
		commandEncoder.setTexture(outputTexture, index: 1)
		
		let threadsPerGroup = MTLSize(width: 8, height: 8, depth: 1)
		let numThreadgroups = MTLSize(width: (width + threadsPerGroup.width - 1) / threadsPerGroup.width,
									  height: (height + threadsPerGroup.height - 1) / threadsPerGroup.height,
									  depth: 1)
		
		commandEncoder.dispatchThreadgroups(numThreadgroups, threadsPerThreadgroup: threadsPerGroup)
		commandEncoder.endEncoding()
		
		commandBuffer.commit()
		commandBuffer.waitUntilCompleted()
		
		return createCIImage(from: outputTexture)
	}
	
	func createCIImage(from texture: MTLTexture) -> CIImage? {
		let width = texture.width
		let height = texture.height

		let rowBytes = width * 4 * MemoryLayout<Float>.size
		let length = rowBytes * height
		var pixelData = [Float](repeating: 0, count: length)

		texture.getBytes(&pixelData, bytesPerRow: rowBytes, from: MTLRegionMake2D(0, 0, width, height), mipmapLevel: 0)

		let colorSpace = CGColorSpaceCreateDeviceRGB()
		let bitmapInfo: CGBitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)

		guard let providerRef = CGDataProvider(data: NSData(bytes: &pixelData, length: length)) else {
			return nil
		}

		let cgImage = CGImage(width: width,
							  height: height,
							  bitsPerComponent: 8,
							  bitsPerPixel: 32,
							  bytesPerRow: rowBytes,
							  space: colorSpace,
							  bitmapInfo: bitmapInfo,
							  provider: providerRef,
							  decode: nil,
							  shouldInterpolate: false,
							  intent: .defaultIntent)

		return CIImage(cgImage: cgImage!)
	}
	
}

extension DebayerFilter {
	
	static func debayer(image: AstroImage) -> AstroImage? {
		let filter = DebayerFilter()
		filter.image = image
		if let outputImage = filter.outputImage {
			let context = CIContext(options: nil)
			if let cgImage = context.createCGImage(outputImage, from: outputImage.extent) {
				return AstroImage(cgImage: cgImage)
			}
		}
		return nil
	}
	
}
