//
//  MainMenu.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 29.03.24.
//

import SwiftUI
import PureSwiftUI
import QuickLook

enum MenuItem {
	case data
	case hints
	case overview
	case timingStatistics
	case table
	case dithering
	case guiding
	case ninaLog
	case imageAnalysis
	case sky
	case astrometry
	case stacking
	
}


struct MainMenu: View {
	@Binding var menuItem: MenuItem
	var session: SessionData
	@Binding var menuDisabled: Bool 
	@State private var animateData = false
	@State private var animateOverview = false
	@State private var userGuideUrl: URL?
	
	@ViewBuilder
	private func InputDataButton() -> some View {
		
		VStack(spacing: 4) {
			Image(systemName: $menuItem.wrappedValue == .data ? "tray.and.arrow.down.fill" : "tray.and.arrow.down")
				.font(.system(size: 30))
				.symbolRenderingMode(.palette)
				.contentTransition(.symbolEffect(.replace))
				.foregroundStyle($menuItem.wrappedValue == .data ? Color.primary : .secondary,
								 $menuItem.wrappedValue == .data ? Color.accentColor : .gray)
			Text("Data").font(.headline)
				.foregroundColor($menuItem.wrappedValue == .data ? .accentColor : .gray)
		}
		.padding()
		.frame(width: 120)
		.contentShape(Rectangle())
		.background($menuItem.wrappedValue == .data
					? RoundedRectangle(8).fill(Color.accentColor.opacity(0.1))
					: RoundedRectangle(8).fill(Color.clear))
		.onTapGesture(perform: {
			$menuItem.wrappedValue = .data
		})
		.buttonStyle(PlainButtonStyle())
		.keyboardShortcut(KeyboardShortcut("I", modifiers: EventModifiers.option))
		.help("Data_help")
	}
	
	@ViewBuilder
	private func HintsButton() -> some View {
		VStack(spacing: 4) {
			Image(systemName: $menuItem.wrappedValue == .hints ? "list.clipboard" : "clipboard")
				.font(.system(size: 30))
				.symbolRenderingMode(.palette)
				.contentTransition(.symbolEffect(.replace))
				.foregroundStyle($menuItem.wrappedValue == .hints ? Color.primary : .secondary,
								 $menuItem.wrappedValue == .hints ? Color.accentColor : .gray)
			Text("Hints")
				.font(.headline)
				.foregroundColor($menuItem.wrappedValue == .hints ? .accentColor : .gray)
		}
		.padding()
		.frame(width: 120)
		.contentShape(Rectangle())
		.background($menuItem.wrappedValue == .hints
					? RoundedRectangle(8).fill(Color.accentColor.opacity(0.1))
					: RoundedRectangle(8).fill(Color.clear))
		.onTapGesture(perform: {
			$menuItem.wrappedValue = .hints
		})
		.opacity(session.sessionImages.count == 0 || session.readingData ? 0.25 : 1.0)
		.disabledIf(session.sessionImages.count == 0 || session.readingData)
		.keyboardShortcut(KeyboardShortcut("H", modifiers: EventModifiers.command))
		.help("Hints_help")
	}
	
	@ViewBuilder
	private func TimingStatisticsButton() -> some View {
		VStack(spacing: 4) {
			Image(systemName: $menuItem.wrappedValue == .timingStatistics ? "clock.badge.questionmark" : "clock.badge.questionmark")
				.font(.system(size: 30))
				.symbolRenderingMode(.palette)
				.contentTransition(.symbolEffect(.replace))
				.foregroundStyle($menuItem.wrappedValue == .timingStatistics ? Color.primary : .secondary,
								 $menuItem.wrappedValue == .timingStatistics ? Color.accentColor : .gray)
			Text("Times")
				.font(.headline)
				.foregroundColor($menuItem.wrappedValue == .timingStatistics ? .accentColor : .gray)
		}
		.padding()
		.frame(width: 120)
		.contentShape(Rectangle())
		.background($menuItem.wrappedValue == .timingStatistics
					? RoundedRectangle(8).fill(Color.accentColor.opacity(0.1))
					: RoundedRectangle(8).fill(Color.clear))
		.onTapGesture(perform: {
			$menuItem.wrappedValue = .timingStatistics
		})
		.opacity(session.ninaLogs.count == 0 || session.readingData ? 0.25 : 1.0)
		.disabledIf(session.ninaLogs.count == 0 || session.readingData)
		.keyboardShortcut(KeyboardShortcut("T", modifiers: EventModifiers.command))
		.help("Times_help")
	}
	
	@ViewBuilder
	private func OverviewButton() -> some View {
		
		VStack(spacing: 4) {
			Image(systemName: $menuItem.wrappedValue == .overview ? "chart.bar.xaxis.ascending.badge.clock" : "chart.bar.xaxis.ascending.badge.clock")
				.font(.system(size: 30))
				.symbolRenderingMode(.palette)
				.contentTransition(.symbolEffect(.replace))
				.foregroundStyle($menuItem.wrappedValue == .overview ? Color.primary : .secondary,
								 $menuItem.wrappedValue == .overview ? Color.accentColor : .gray)
			Text("Overview")
				.font(.headline)
				.foregroundColor($menuItem.wrappedValue == .overview ? .accentColor : .gray)
		}
		.padding()
		.frame(width: 120)
		.contentShape(Rectangle())
		.background($menuItem.wrappedValue == .overview
					? RoundedRectangle(8).fill(Color.accentColor.opacity(0.1))
					: RoundedRectangle(8).fill(Color.clear))
		.onTapGesture(perform: {
			$menuItem.wrappedValue = .overview
		})
		.opacity(session.sessionImages.count == 0 || session.readingData ? 0.25 : 1.0)
		.disabledIf(session.sessionImages.count == 0 || session.readingData)
		.keyboardShortcut(KeyboardShortcut("O", modifiers: EventModifiers.command))
		.help("Overview_help")
		
	}
	
	@ViewBuilder
	private func TableButton() -> some View {
		VStack(spacing: 4) {
			Image(systemName: $menuItem.wrappedValue == .table ? "tablecells.fill.badge.ellipsis" : "tablecells.badge.ellipsis")
				.font(.system(size: 30))
				.symbolRenderingMode(.palette)
				.contentTransition(.symbolEffect(.replace))
				.foregroundStyle($menuItem.wrappedValue == .table ? Color.primary : .secondary,
								 $menuItem.wrappedValue == .table ? Color.accentColor : .gray)
			Text("Table")
				.font(.headline)
				.foregroundColor($menuItem.wrappedValue == .table ? .accentColor : .gray)
		}
		.padding()
		.frame(width: 120)
		.contentShape(Rectangle())
		.background($menuItem.wrappedValue == .table
					? RoundedRectangle(8).fill(Color.accentColor.opacity(0.1))
					: RoundedRectangle(8).fill(Color.clear))
		.onTapGesture(perform: {
			$menuItem.wrappedValue = .table
		})
		.opacity(session.sessionImages.count == 0 || session.readingData ? 0.25 : 1.0)
		.disabledIf(session.sessionImages.count == 0 || session.readingData)
		.keyboardShortcut(KeyboardShortcut("T", modifiers: EventModifiers.option))
		.help("Table_help")
	}
	
	@ViewBuilder
	private func DitheringButton() -> some View {
		VStack(spacing: 4) {
			Image(systemName: $menuItem.wrappedValue == .dithering ? "camera.viewfinder" : "camera.viewfinder")
				.font(.system(size: 30))
				.symbolRenderingMode(.palette)
				.contentTransition(.symbolEffect(.replace))
				.foregroundStyle($menuItem.wrappedValue == .dithering ? Color.primary : .secondary,
								 $menuItem.wrappedValue == .dithering ? Color.accentColor : .gray)
			Text("Drift")
				.font(.headline)
				.foregroundColor($menuItem.wrappedValue == .dithering ? .accentColor : .gray)
		}
		.padding()
		.frame(width: 120)
		.contentShape(Rectangle())
		.background($menuItem.wrappedValue == .dithering
					? RoundedRectangle(8).fill(Color.accentColor.opacity(0.1))
					: RoundedRectangle(8).fill(Color.clear))
		.onTapGesture(perform: {
			$menuItem.wrappedValue = .dithering
		})
		.opacity(!session.hasDriftData() || session.readingData ? 0.25 : 1.0)
		.disabledIf(!session.hasDriftData() || session.readingData)
		.keyboardShortcut(KeyboardShortcut("D", modifiers: EventModifiers.option))
		.help("Drift_help")
		
	}
	
	@ViewBuilder
	private func GuidingButton() -> some View {
		VStack(spacing: 4) {
			Image(systemName: $menuItem.wrappedValue == .guiding ? "arrow.up.left.and.down.right.and.arrow.up.right.and.down.left" : 												   "arrow.up.left.and.down.right.and.arrow.up.right.and.down.left")
				.font(.system(size: 30))
				.symbolRenderingMode(.palette)
				.contentTransition(.symbolEffect(.replace))
				.foregroundStyle($menuItem.wrappedValue == .guiding ? Color.primary : .secondary,
								 $menuItem.wrappedValue == .guiding ? Color.accentColor : .gray)
			Text("Guiding")
				.font(.headline)
				.foregroundColor($menuItem.wrappedValue == .guiding ? .accentColor : .gray)
		}
		.padding()
		.frame(width: 120)
		.contentShape(Rectangle())
		.background($menuItem.wrappedValue == .guiding
					? RoundedRectangle(8).fill(Color.accentColor.opacity(0.1))
					: RoundedRectangle(8).fill(Color.clear))
		.onTapGesture(perform: {
			$menuItem.wrappedValue = .guiding
		})
		.opacity(!session.guideLogAvailable || session.readingData ? 0.25 : 1.0)
		.disabledIf(!session.guideLogAvailable || session.readingData)
		.keyboardShortcut(KeyboardShortcut("D", modifiers: EventModifiers.option))
		.help("Guiding_help")
	}
	
	@ViewBuilder
	private func NINALogButton() -> some View {
		VStack(spacing: 4) {
			Image(systemName: $menuItem.wrappedValue == .ninaLog ? "doc.plaintext" : "doc.plaintext")
				.font(.system(size: 30))
				.symbolRenderingMode(.palette)
				.contentTransition(.symbolEffect(.replace))
				.foregroundStyle($menuItem.wrappedValue == .ninaLog ? Color.accentColor : .secondary,
								 $menuItem.wrappedValue == .ninaLog ? Color.accentColor : .gray)
			Text("N.I.N.A. Log")
				.font(.headline)
				.multilineTextAlignment(.center)
				.foregroundColor($menuItem.wrappedValue == .ninaLog ? .accentColor : .gray)
		}
		.padding()
		.frame(width: 120)
		.contentShape(Rectangle())
		.background($menuItem.wrappedValue == .ninaLog
					? RoundedRectangle(8).fill(Color.accentColor.opacity(0.1))
					: RoundedRectangle(8).fill(Color.clear))
		.onTapGesture(perform: {
			$menuItem.wrappedValue = .ninaLog
		})
		.opacity(session.ninaLogs.count == 0 || session.readingData ? 0.25 : 1.0)
		.disabledIf(session.ninaLogs.count == 0 || session.readingData)
		.keyboardShortcut(KeyboardShortcut("L", modifiers: EventModifiers.option))
		.help("Log_help")
		
	}
	
	@ViewBuilder
	private func ImageAnalysisButton() -> some View {
		VStack(spacing: 0) {
			Image(systemName: "rectangle.split.3x3")
				.font(.system(size: 30))
				.contentTransition(.symbolEffect(.replace))
				.foregroundStyle($menuItem.wrappedValue == .imageAnalysis ? Color.accentColor : .gray)
				.padding(.bottom, 4)
			Text("Image Analysis")
				.font(.headline)
				.multilineTextAlignment(.center)
				.foregroundColor($menuItem.wrappedValue == .imageAnalysis ? .accentColor : .gray)
			
		}
		.padding()
		.frame(width: 120)
		.contentShape(Rectangle())
		.background($menuItem.wrappedValue == .imageAnalysis
					? RoundedRectangle(8).fill(Color.accentColor.opacity(0.1))
					: RoundedRectangle(8).fill(Color.clear))
		.onTapGesture(perform: {
			$menuItem.wrappedValue = .imageAnalysis
		})
		.opacity(session.sessionImages.count == 0  || session.readingData ? 0.25 : 1.0)
		.disabledIf(session.sessionImages.count == 0 || session.readingData)
		.keyboardShortcut(KeyboardShortcut("A", modifiers: EventModifiers.option))
		.help("ImageAnalysis_help")
		
	}
	
	@ViewBuilder
	private func SkyButton() -> some View {
		VStack(spacing: 4) {
			Image(systemName: $menuItem.wrappedValue == .sky ? "moon.stars.circle" : "moon.stars.circle")
				.font(.system(size: 30))
				.symbolRenderingMode(.palette)
				.contentTransition(.symbolEffect(.replace))
				.foregroundStyle($menuItem.wrappedValue == .sky ? Color.primary : .secondary,
								 $menuItem.wrappedValue == .sky ? Color.accentColor : .gray)
			Text("Sky")
				.font(.headline)
				.foregroundColor($menuItem.wrappedValue == .sky ? .accentColor : .gray)
		}
		.padding()
		.frame(width: 120)
		.contentShape(Rectangle())
		.background($menuItem.wrappedValue == .sky
					? RoundedRectangle(8).fill(Color.accentColor.opacity(0.1))
					: RoundedRectangle(8).fill(Color.clear))
		.onTapGesture(perform: {
			$menuItem.wrappedValue = .sky
		})
		.opacity(session.sessionImages.count == 0  || session.readingData ? 0.25 : 1.0)
		.disabledIf(session.sessionImages.count == 0 || session.readingData)
		.keyboardShortcut(KeyboardShortcut("S", modifiers: EventModifiers.option))
		.help("Sky_help")
		
	}
	
	@ViewBuilder
	private func StackButton() -> some View {
		VStack(spacing: 4) {
			Image(systemName: "sum")
				.font(.system(size: 30))
				.symbolRenderingMode(.palette)
				.contentTransition(.symbolEffect(.replace))
				.foregroundStyle($menuItem.wrappedValue == .stacking ? Color.primary : .secondary,
								 $menuItem.wrappedValue == .stacking ? Color.accentColor : .gray)
			Text("Stacking")
				.font(.headline)
				.foregroundColor($menuItem.wrappedValue == .stacking ? .accentColor : .gray)
		}
		.padding()
		.frame(width: 120)
		.contentShape(Rectangle())
		.background($menuItem.wrappedValue == .stacking
					? RoundedRectangle(8).fill(Color.accentColor.opacity(0.1))
					: RoundedRectangle(8).fill(Color.clear))
		.onTapGesture(perform: {
			$menuItem.wrappedValue = .stacking
		})
		.opacity(session.sessionImages.count == 0  || session.readingData ? 0.25 : 1.0)
		.disabledIf(session.sessionImages.count == 0 || session.readingData)
	}
	
	
	@ViewBuilder
	private func ManualButton() -> some View {
		
		Button {
			if let url = Bundle.main.url(forResource: "NINASessionAnalysisHandbook_eng", withExtension: "pdf") {
				NSWorkspace.shared.open(url)
			}
		} label: {
			VStack(spacing: 4) {
				Image(systemName: "questionmark.circle")
					.font(.system(size: 30))
					.symbolRenderingMode(.palette)
					.contentTransition(.symbolEffect(.replace))
					.foregroundStyle(Color.accentColor.opacity(0.8))
				Text("Help")
					.font(.headline)
					.foregroundColor(Color.accentColor.opacity(0.8))
			}
			.padding()
			.frame(width: 120)
			.contentShape(.rect)
		}
		.buttonStyle(.plain)
		.help("Show a short manual.")
		
	}
	
	
    var body: some View {
		
		ScrollView(.vertical) {
			
			VStack(spacing: 2) {
				
				InputDataButton()
				
				HintsButton()
				
				TimingStatisticsButton()
				
				OverviewButton()
				
				TableButton()
				
				DitheringButton()
				
				GuidingButton()
				
				NINALogButton()
				
				ImageAnalysisButton()
				
				SkyButton()
				
				//StackButton()

				ManualButton()
				
				Spacer()
			}
			.disabled(self.menuDisabled)
			
		}
		.frame(width: 120)
		.background(Color.controlBackgroundColor.opacity(0.3))
		.id(session.id)
		
    }
}

