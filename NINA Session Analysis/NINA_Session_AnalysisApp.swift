//
//  NINA_Session_AnalysisApp.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 29.03.24.
//

import SwiftUI
import TipKit
import SwiftRater

class AppDelegate: NSObject, NSApplicationDelegate {
	func applicationShouldTerminateAfterLastWindowClosed(_ sender: NSApplication) -> Bool {
		return true
	}
	
	func applicationDidFinishLaunching(_ notification: Notification) {
		SwiftRater.daysUntilPrompt = 14
		SwiftRater.usesUntilPrompt = 10
		SwiftRater.significantUsesUntilPrompt = 3
		SwiftRater.daysBeforeReminding = 7
		SwiftRater.showLaterButton = true
		SwiftRater.debugMode = false
		
		SwiftRater.alertTitle = LocalizedStringKey("RateTitle").stringValue()
		SwiftRater.alertMessage = LocalizedStringKey("RateComments").stringValue()
		SwiftRater.alertRateTitle = LocalizedStringKey("RateButtonTitle").stringValue()
		SwiftRater.appLaunched()
	
		if let helpMenu = NSApp.helpMenu, let helpItem = helpMenu.items.first {
			   helpItem.target = self
			   helpItem.action = #selector(openHelpPDF)
		}
	}
	
	
	@objc func openHelpPDF() {
		if let url = Bundle.main.url(forResource: "NINASessionAnalysisHandbook_eng", withExtension: "pdf") {
			NSWorkspace.shared.open(url)
		}
	}
}


@main
struct NINA_Session_AnalysisApp: App {
	@NSApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
	
	@State private var data: SessionData = SessionData()
	
	init() {
		
		UserDefaults.standard.register(defaults: [
			"TableImageColumn_Index" : true,
			"TableImageColumn_Filename": true,
			"TableImageColumn_Bad": true,
			"TableImageColumn_Exposure (Start)": true,
			"TableImageColumn_Exposure": true,
			"TableImageColumn_Altitude": true,
			"TableImageColumn_Azimuth": true,
			"TableImageColumn_PierSide": true,
			"TableImageColumn_Airmass": true,
			"TableImageColumn_HFR": true,
			"TableImageColumn_FWHM": true,
			"TableImageColumn_Eccentricity": true,
			"TableImageColumn_NumStars": true,
			"TableImageColumn_Mean": true,
			"TableImageColumn_Median": true,
			"TableImageColumn_ADU (min)": true,
			"TableImageColumn_ADU (max)": true,
			"TableImageColumn_Moon Altitude": true,
			"TableImageColumn_Moon Angle": true,
			"TableImageColumn_Sun Altitude": true,
			"TableImageColumn_Sun Angle": true,
			"TableImageColumn_RMS": true,
			"TableImageColumn_RMS (α)": true,
			"TableImageColumn_RMS (δ)": true,
			"TableImageColumn_Drift (px)": true,
			"TableImageColumn_Cloud Coverage": true,
			"TableImageColumn_Air Pressure": true,
			"TableImageColumn_Humidity": true,
			"TableImageColumn_DewPoint": true,
			"TableImageColumn_Ambient Temperature": true,
			"TableImageColumn_Filter": true,
			"OverviewCharts_Airmass": true,
			"OverviewCharts_Cloud Coverage": true,
			"OverviewCharts_Dew Point": true,
			"OverviewCharts_Moon Angle": true,
			"OverviewCharts_HFR": true,
			"OverviewCharts_FWHM": true,
			"OverviewCharts_Eccentricity": true,
			"OverviewCharts_RMS": true,
			"OverviewCharts_#Stars": true,
			"OverviewCharts_Moon Altitude": true,
			"OverviewCharts_Drift (px)": true,
			"meanToleranceValue": 50.0,
			"hfrToleranceValue": 20.0,
			"starCountToleranceValue": 30.0,
			"starEccentricityToleranceValue": 0.5,
			"humidityToleranceValue": 80.0,
			"cloudCoverageToleranceValue": 30.0,
			"windSpeedToleranceValue": 25.0,
			"rmsToleranceValue": 60.0,
			
			"ImageQuality": 30,
			"NINAHost": "",
			"NINAPort": "1888",
			"NINAProtocol": "http",
			
			"NINACameraEnabled": true,
			"NINAFocuserEnabled": false,
			"NINAFilterWheelEnabled": false,
			"NINAGuiderEnabled": true,
			"NINADomeEnabled": false,
			"NINARotatorEnabled": false,
			"NINASafetyMonitorEnabled": false,
			"RequestTimeout": 10
		])
		
		//data.resetDefaults()
		data.setup()
		
		updateRunCount()
		
		Task {
			await MilkywayDataManager.shared.preloadData()
			await StarDatabase.shared.preload()
		}
	}
	
	private func updateRunCount() {
		if let runCount = UserDefaults.standard.value(forKey: "runCount") as? Int {
			UserDefaults.standard.setValue(runCount + 1, forKey: "runCount")
		} else {
			UserDefaults.standard.setValue(1, forKey: "runCount")
		}
	}
	
	
	
    var body: some Scene {
        WindowGroup {
			
			if #available(macOS 15, *) {
				MainView()
					.environment(data)
					.containerBackground(.thinMaterial, for: .window)
					.task {
						try? Tips.resetDatastore()
						
						try? Tips.configure([
							.displayFrequency(.immediate),
							.datastoreLocation(.applicationDefault)
						])
					}
			}
			else {
				MainView()
					.environment(data)
					.task {
						try? Tips.resetDatastore()
						
						try? Tips.configure([
							.displayFrequency(.immediate),
							.datastoreLocation(.applicationDefault)
						])
					}
			}
			 
        }
		.defaultSize(width: 1440, height: 900)
		.windowResizability(.contentMinSize)

		WindowGroup (id: "InternalViewer", for: SessionImage.ID.self){ imageId in
			InternalImageView(imageId: imageId)
				.environment(data)
		}
		.commandsRemoved()
		.defaultSize(width: 1280, height: 960)
		
    }
}
