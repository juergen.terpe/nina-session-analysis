//
//  RegistrationData.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 01.03.25.
//
import CoreGraphics

actor RegistrationData {
	var registrationData: [RegistrationEntry] = []
	
	init() {}
	
	init(entries: [RegistrationEntry]) {
		self.registrationData = entries
	}
	
	func append(entry: RegistrationEntry) async {
		registrationData.append(entry)
	}
	
	var allEntries: [RegistrationEntry] {
		return registrationData
	}
	
	var count: Int {
		return registrationData.count
	}
	
	func groupLights() -> [RegistrationData] {
		var clusters: [[RegistrationEntry]] = []
		let tolerance = 1.25
		
		for entry in registrationData {
			let frameSizeRA = (Double(entry.width) * Double(entry.pixelScale)) / 3600.0
			let frameSizeDEC = (Double(entry.height) * Double(entry.pixelScale)) / 3600.0
			
			var added = false
			for i in 0..<clusters.count {
				if let ref = clusters[i].first,
				   abs(ref.ra - entry.ra) < frameSizeRA * tolerance,
				   abs(ref.dec - entry.dec) < frameSizeDEC * tolerance {
					clusters[i].append(entry)
					added = true
					break
				}
			}
			
			if !added {
				clusters.append([entry])
			}
		}
		
		var results = [RegistrationData]()
		
		for cluster in clusters {
			results.append(RegistrationData(entries: cluster))
		}
		
		return results
	}
		
	func selectReferenceLight() -> RegistrationEntry? {
		return self.registrationData.min {
			($0.avgHFR, $0.median, $0.mean) < ($1.avgHFR, $1.median, $1.mean)
		}
	}
	
}
