//
//  StarPosition.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 01.03.25.
//
import CoreGraphics

struct StarPosition: Codable, Hashable {
	var position: CGPoint
	var hfd: Float
	
	static func == (lhs: StarPosition, rhs: StarPosition) -> Bool {
		lhs.position == rhs.position
	}
	
	func hash(into hasher: inout Hasher) {
		position.hash(into: &hasher)
	}
}

struct RegistrationEntry: Codable {
	var id:  String
	var filter: String
	var ra: Double
	var dec: Double
	var avgHFR: Float
	var mean: Float
	var median: Float
	var numStars: Int
	var stars: [StarPosition]
	var pixelScale: Float
	var width: Int
	var height: Int 
}
