//
//  ImageProcessingUnit.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 01.03.25.
//
import SwiftUI

@Observable
public class ImageProcessingUnit {
	let session: SessionData
	
	init(session: SessionData) {
		self.session = session
	}
	
	func process() async {
		
		let register = RegisterImagesProcessor()
		await register.process(images: session.sessionImages)
	}
}
