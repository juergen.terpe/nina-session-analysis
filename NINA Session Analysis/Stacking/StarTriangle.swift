//
//  StarTriangle.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 01.03.25.
//
import CoreGraphics


struct StarTriangle {
    let star1: StarPosition
    let star2: StarPosition
    let star3: StarPosition
    let angles: (CGFloat, CGFloat, CGFloat)
    let sideLengths: (CGFloat, CGFloat, CGFloat)
	let area: CGFloat
}
