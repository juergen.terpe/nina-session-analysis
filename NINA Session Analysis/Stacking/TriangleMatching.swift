//
//  MatchedTriangle.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 01.03.25.
//


import Foundation

struct MatchedTriangle {
    let reference: StarTriangle
    let target: StarTriangle
    let error: CGFloat
}

class TriangleMatching {
    let referenceTriangles: [StarTriangle]
    let targetTriangles: [StarTriangle]
    
	let maxTriangles = 600 // Maximal zu überprüfende Dreiecke
	let earlyExitThreshold = 200 // Falls so viele Matches gefunden wurden, frühzeitig abbrechen
	
    init(referenceTriangles: [StarTriangle], targetTriangles: [StarTriangle]) {
        self.referenceTriangles = referenceTriangles
        self.targetTriangles = targetTriangles
    }
    
    func findBestMatches() -> [(StarTriangle, StarTriangle)] {
        var matches: [MatchedTriangle] = []
		  
        for refTriangle in referenceTriangles.prefix(maxTriangles) {
            if let bestMatch = findBestMatch(for: refTriangle) {
                matches.append(bestMatch)
            }
			
			if matches.count >= earlyExitThreshold {
				break
			}
        }
        
        return matches.sorted { $0.error < $1.error }.map { ($0.reference, $0.target) }
    }
    
	private func findBestMatch(for reference: StarTriangle) -> MatchedTriangle? {
		let toleranceAngle: CGFloat = 2.0 // Toleranz in Grad
		let toleranceSize: CGFloat = 0.05 // 5% Abweichung der Seitenlängen
		let toleranceHFD: CGFloat = 0.2  // 20% Abweichung der HFR-Werte
		
		var bestMatch: MatchedTriangle?
		var minError: CGFloat = .greatestFiniteMagnitude
		
		for target in targetTriangles.prefix(maxTriangles) {
			let angleDiff = angleDifference(reference.angles, target.angles)
			if angleDiff > toleranceAngle {
				continue
			}
			
			let sizeDiff = sizeDifference(reference.sideLengths, target.sideLengths)
			if sizeDiff > toleranceSize {
				continue
			}
			
			let hfdDiff = hfdDifference(reference, target)
			
			if hfdDiff < toleranceHFD {
				let error = angleDiff + sizeDiff * 100.0 + hfdDiff * 50.0 // Gewichtung
				if error < minError {
					minError = error
					bestMatch = MatchedTriangle(reference: reference, target: target, error: error)
				}
			}
		}
		
		return bestMatch
	}
	
    private func angleDifference(_ refAngles: (CGFloat, CGFloat, CGFloat), _ targetAngles: (CGFloat, CGFloat, CGFloat)) -> CGFloat {
        let diffs = zip([refAngles.0, refAngles.1, refAngles.2], [targetAngles.0, targetAngles.1, targetAngles.2]).map { abs($0 - $1) }
        return diffs.reduce(0, +) / 3.0
    }
    
    private func sizeDifference(_ refSizes: (CGFloat, CGFloat, CGFloat), _ targetSizes: (CGFloat, CGFloat, CGFloat)) -> CGFloat {
        let diffs = zip([refSizes.0, refSizes.1, refSizes.2], [targetSizes.0, targetSizes.1, targetSizes.2]).map { abs($0 - $1) / max($0, $1) }
        return diffs.reduce(0, +) / 3.0
    }
	
	private func hfdDifference(_ refTriangle: StarTriangle, _ targetTriangle: StarTriangle) -> CGFloat {
		// Sortiere Sterne nach HFD, um eine konsistente Zuordnung zu haben
		let refHFDs = [refTriangle.star1.hfd, refTriangle.star2.hfd, refTriangle.star3.hfd].sorted()
		let targetHFDs = [targetTriangle.star1.hfd, targetTriangle.star2.hfd, targetTriangle.star3.hfd].sorted()
		
		// Berechne die mittlere relative Differenz
		var totalDiff: Float = 0.0
		for i in 0..<3 {
			totalDiff += abs(refHFDs[i] - targetHFDs[i]) / max(refHFDs[i], targetHFDs[i])
		}
		
		return CGFloat(totalDiff / 3.0)
	}
}
