//
//  RegistrationDataProcessor.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 01.03.25.
//

import Foundation
import CoreGraphics


class RegistrationDataProcessor {
	private let hfdMax: Float = 8.0
	
	init() {
	}
	
	func process(images: [SessionImage]) async -> RegistrationData? {
		let maxConcurrentTasks = ProcessInfo.processInfo.activeProcessorCount
		
		let registrationData = RegistrationData()
		
		await withTaskGroup(of: Void.self) { group in
			var activeTasks = 0
			
			for image in images {
				
				while activeTasks >= maxConcurrentTasks {
					await Task.yield() // Ermöglicht anderen Tasks die Ausführung, bevor weitere gestartet werden
				}
				
				activeTasks += 1
				
				group.addTask {
					defer { activeTasks -= 1 }
					if let regData = await self.processImage(image) {
						await registrationData.append(entry: regData)
					}
				}
			}
		}
		
		return registrationData
	}
	
	private func processImage(_ image: SessionImage) async ->  RegistrationEntry? {
		guard !image.bad, let ra = image.ra, let dec = image.dec, let mean = image.mean, let median = image.median,
			  let pixScale = image.pixSize, let width = image.sizeX, let height = image.sizeY else { return nil }
		let scale: Float = 3.0
		
		guard let imageData = await loadImageData(from: image, scale: scale) else { return nil }
		
		let stars = await detectStars(in: imageData, scale: scale)
		let avgHFR = stars.isEmpty ? 0.0 : stars.map { $0.hfd }.reduce(0, +) / Float(stars.count)
		
		let entry = RegistrationEntry(id: image.id.uuidString,
									  filter: image.filter,
									  ra: ra,
									  dec: dec,
									  avgHFR: avgHFR,
									  mean: mean,
									  median: Float(median),
									  numStars: stars.count,
									  stars: stars,
									  pixelScale: pixScale,
									  width: width,
									  height: height)
		
		print("Detected \(stars.count) stars with HFD < \(hfdMax) for registration...")
		
		return entry
		
		
	}
	
	private func detectStars(in image: CGImage, scale: Float) async -> [StarPosition] {
		let starDetector = StarDetector()
		let detectedStars = await starDetector.process(image)
		let factor = CGFloat(scale)
		return detectedStars
			.filter { $0.hfd <= hfdMax/scale && $0.hfd > 1.0 }
			.map({ StarPosition(position: CGPoint(x: $0.position.x * factor, y: $0.position.y * factor), hfd: $0.hfd * scale)})
	}
	
	private func loadImageData(from image: SessionImage, scale: Float) async -> CGImage? {
		if let imageData = await image.getImageData() {
			let (stretched, _ ) = imageData.debayerAndStretch()
			if let stretched, let cgImage = stretched.cgImage {
				return cgImage.resize(by: CGFloat(1.0/scale))
			}
		}
		return nil
	}
}
