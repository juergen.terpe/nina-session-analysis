//
//  RegisterImagesProcessor.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 01.03.25.
//
import CoreGraphics
import Foundation


struct HashablePoint: Hashable {
	let x: CGFloat
	let y: CGFloat
	
	init(x: CGFloat, y: CGFloat) {
		self.x = x
		self.y = y
	}
	
	var cgPoint: CGPoint {
		return CGPoint(x: x, y: y)
	}
	
	func hash(into hasher: inout Hasher) {
		hasher.combine(Int(x * 1000))
		hasher.combine(Int(y * 1000))
	}
	
	static func == (lhs: HashablePoint, rhs: HashablePoint) -> Bool {
		return abs(lhs.x - rhs.x) < 0.001 && abs(lhs.y - rhs.y) < 0.001
	}
}

class RegisterImagesProcessor {
	
	func process(images: [SessionImage]) async {
		
		if let registrationData = await getRegistrationData(from: images) {
			print("Registration data: \(await registrationData.count)")
			
			let groups = await registrationData.groupLights()
			
			for group in groups {
				print("Group: \(await group.count)")
				if let referenceLight = await group.selectReferenceLight() {
					await register(for: await group.allEntries, reference: referenceLight)
				}
			}
		}
	}
	
	private func getRegistrationData(from images: [SessionImage]) async -> RegistrationData? {
		
		let register = RegistrationDataProcessor()
		return await register.process(images: images)
	}
	
	private func register(for data: [RegistrationEntry], reference: RegistrationEntry) async {
		let referenceTriangles = findStarTriangles(in: reference)
			
		let start = Date.now
		
		await withTaskGroup(of: Void.self) { group in
			for entry in data where entry.id != reference.id {
				group.addTask {
					let matchedTriangles = self.matchTriangles(referenceTriangles, in: entry)
					print("matched triangle pairs \(matchedTriangles.count)")
					
					if let transformation = self.computeTransformation(from: matchedTriangles) {
						self.applyTransformation(transformation, to: entry)
					}
				}
			}
		}
	
		print("Light registration finished after \(Date.now.timeIntervalSince(start)) s")
	}
	
	private func findStarTriangles(in entry: RegistrationEntry) -> [StarTriangle] {
		let width = entry.width
		let height = entry.height
		let minSize: CGFloat = 25.0
		let maxSize: CGFloat = CGFloat(min(width, height)) / 2.0
		let gridSize = 50
		
		var grid = Dictionary<HashablePoint, Int>()
		var triangles: [StarTriangle] = []
		
		let stars = entry.stars.filter { $0.position.x > 100 && $0.position.y > 100 && $0.position.x < Double(width - 100) && $0.position.y < Double(height - 100) }
		
		for i in 0..<stars.count {
			let s1 = stars[i]
			let gridPos1 = HashablePoint(x: floor(s1.position.x / CGFloat(gridSize)), y: floor(s1.position.y / CGFloat(gridSize)))
			if let count = grid[gridPos1], count >= 3 {
				continue
			}
			
			for j in (i+1)..<stars.count {
				let s2 = stars[j]
				let gridPos2 = HashablePoint(x: floor(s2.position.x / CGFloat(gridSize)), y: floor(s2.position.y / CGFloat(gridSize)))
				if let count = grid[gridPos2], count >= 3 {
					continue
				}
				
				let d1 = distance(s1, s2)
				if d1 < minSize || d1 > maxSize {
					continue
				}
				
				for k in (j+1)..<stars.count {
					let s3 = stars[k]
					let gridPos3 = HashablePoint(x: floor(s3.position.x / CGFloat(gridSize)), y: floor(s3.position.y / CGFloat(gridSize)))
					if let count = grid[gridPos3], count >= 3 {
						continue
					}
					
					let d2 = distance(s2, s3)
					if d2 < minSize || d2 > maxSize {
						continue
					}
					
					let d3 = distance(s3, s1)
					if d3 < minSize || d3 > maxSize {
						continue
					}
				
					let (a, b, c) = computeAngles(a: d1, b: d2, c: d3)
					
					if a < 20.0 || a > 140.0 || b < 20.0 || b > 140.0 || c < 20.0 || c > 140.0 {
						continue
					}
					
					let area = triangleArea(a: d1, b: d2, c: d3)
					
					grid[gridPos1, default: 0] += 1
					grid[gridPos2, default: 0] += 1
					grid[gridPos3, default: 0] += 1
					
					triangles.append(StarTriangle(star1: s1, star2: s2, star3: s3, angles: (a, b, c), sideLengths: (d1, d2, d3), area: area))
				}
			}
		}
		
		return Array(triangles.sorted(by: { $0.area > $1.area}).prefix(2500))
	}
	
	private func distance(_ s1: StarPosition, _ s2: StarPosition) -> CGFloat {
		return hypot(CGFloat(s2.position.x - s1.position.x), CGFloat(s2.position.y - s1.position.y))
	}
	
	private func computeAngles(a: CGFloat, b: CGFloat, c: CGFloat) -> (a: CGFloat, b: CGFloat, c: CGFloat) {
		let angleA = acos((b * b + c * c - a * a) / (2 * b * c)) * (180.0 / .pi)
		let angleB = acos((a * a + c * c - b * b) / (2 * a * c)) * (180.0 / .pi)
		let angleC = 180.0 - angleA - angleB
		return (angleA, angleB, angleC)
	}
	
	private func triangleArea(a: CGFloat, b: CGFloat, c: CGFloat) -> CGFloat {
		let s = (a + b + c) / 2.0
		return sqrt(s * (s - a) * (s - b) * (s - c))
	}
	
	
	private func matchTriangles(_ referenceTriangles: [StarTriangle], in entry: RegistrationEntry) -> [(StarTriangle, StarTriangle)] {
		let entryTriangles = findStarTriangles(in: entry)
		
		let triangleMatching = TriangleMatching(referenceTriangles: referenceTriangles, targetTriangles: entryTriangles)
		return triangleMatching.findBestMatches()
	}
	
	private func computeTransformation(from matchedTriangles: [(StarTriangle, StarTriangle)]) -> CGAffineTransform? {
		// Transformation basierend auf übereinstimmenden Dreiecken berechnen
		return nil
	}
	
	private func applyTransformation(_ transform: CGAffineTransform, to entry: RegistrationEntry) {
		// Transformation auf das Bild anwenden
	}
	
}
