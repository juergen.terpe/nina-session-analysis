//
//  ImageHistogramView.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 06.10.24.
//

import AppKit
import SwiftUI

struct ImageHistogramView: View {
	@Binding var imageDataProvider: ImageAnalysisDataProvider
	@State private var model: HistogramViewModel = HistogramViewModel()
	@AppStorage("LogarithmicHistogram") private var logarithmicHistogram: Bool = false
	
	var body: some View {
		
		VStack {
			
			if let stretchedImage =  self.imageDataProvider.stretchedImage?.cgImage {
				
				VStack(alignment: .leading) {
					
					HStack {
						Image(decorative: stretchedImage, scale: 1.0)
							.resizable()
							.aspectRatio(contentMode: .fit)
					
						VStack {
							ForEach(self.model.statistics) { channelData in
								ImageStatisticsView(statistics: channelData)
							}
							
							Spacer()
							
							
						}
						.frame(maxWidth: 360)
					}
					
					HistogramView(model: self.$model)
						.frame(height: 200)
						.overlay {
							VStack {
								HStack {
									Spacer()
									
									Toggle(isOn: $logarithmicHistogram) {
										Text("Logarithmic Histogram")
									}
									.padding(.top, 8)
									.padding(.trailing, 42)
								}
								Spacer()
							}
						}
						.padding()
					
				}
			}
		}
		.onChange(of: self.imageDataProvider.id) { _, _ in
			Task {
				
				if let debayeredImage = self.imageDataProvider.imageForHistogram() {
					await self.model.update(with: debayeredImage, logarithmic: self.logarithmicHistogram)
				}
			}
		}
		.onChange(of: self.logarithmicHistogram) { _, _ in
			Task {
				
				if let debayeredImage = self.imageDataProvider.imageForHistogram() {
					await self.model.update(with: debayeredImage, logarithmic: self.logarithmicHistogram)
				}
			}
		}
		
		
	}
	
}
