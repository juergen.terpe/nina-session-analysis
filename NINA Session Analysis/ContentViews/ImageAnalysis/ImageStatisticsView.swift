//
//  ImageStatisticsView.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 12.10.24.
//

import SwiftUI

struct ImageStatisticsView: View {
	let statistics: ImageStatisticsValues
	
	func getChannelName() -> LocalizedStringKey {
		switch(statistics.channel) {
			case .l: return "Gray"
			case .r: return "Red"
			case .g: return "Green"
			case .b: return "Blue"
		}
	}
	
	func getChannelColor() -> Color {
		switch(statistics.channel) {
			case .l: return Color.gray
			case .r: return Color.red
			case .g: return Color.green
			case .b: return Color.blue
		}
	}
	
	var body: some View {
		
		VStack(alignment: .leading) {
		
			Text(getChannelName())
				.foregroundStyle(getChannelColor())
				.fontWeight(.semibold)
				.kerning(1.5)
			
			Divider()
			
			Grid(alignment: .leading, horizontalSpacing: 16, verticalSpacing: 4) {
				GridRow(alignment: .firstTextBaseline) {
					Text("Mean").fontWeight(.semibold).kerning(1.5)
						.contentShape(Rectangle())
						.help("Mean: average value of all pixels.")
					Text("\(statistics.mean, specifier: "%.1f") ADU")
					Text("\(statistics.mean/65536.0, specifier: "%.5f")")
				}
				
				GridRow(alignment: .firstTextBaseline) {
					Text("avgDev").fontWeight(.semibold).kerning(1.5)
						.contentShape(Rectangle())
						.help("avgDev: average deviation of all pixels from the mean value.")
					Text("\(statistics.avgDev, specifier: "%.1f") ADU")
					Text("\(statistics.avgDev/65536.0, specifier: "%.5f")")
				}
				
				GridRow(alignment: .firstTextBaseline) {
					Text("Median").fontWeight(.semibold).kerning(1.5)
						.contentShape(Rectangle())
						.help("Median: the central value of all pixel values sorted starting from the minimum value.")
					Text("\(statistics.median) ADU")
					Text("\(Double(statistics.median)/65536.0, specifier: "%.5f")")
				}
				
				GridRow(alignment: .firstTextBaseline) {
					Text("MAD").fontWeight(.semibold).kerning(1.5)
						.contentShape(Rectangle())
						.help("MAD: mean average deviation of all pixels from the median value.")
					Text("\(statistics.mad)")
					Text("\(Double(statistics.mad)/65536.0, specifier: "%.5f")")
				}
				
				GridRow(alignment: .firstTextBaseline) {
					Text("Minimum").fontWeight(.semibold).kerning(1.5)
						.contentShape(Rectangle())
						.help("Minimum: lowest value of all pixels.")
					Text("\(statistics.min) ADU")
					Text("\(Double(statistics.min)/65536.0, specifier: "%.5f")")
				}
				
				GridRow(alignment: .firstTextBaseline) {
					Text("Maximum").fontWeight(.semibold).kerning(1.5)
						.contentShape(Rectangle())
						.help("Maximum: highest value of all pixels.")
					Text("\(statistics.max) ADU")
					Text("\(Double(statistics.max)/65536.0, specifier: "%.5f")")
				}
			}
			.foregroundStyle(getChannelColor())
		}
		.padding()
	}
}
