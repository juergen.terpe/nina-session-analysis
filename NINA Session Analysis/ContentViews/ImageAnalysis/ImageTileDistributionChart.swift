//
//  ImageTileDistributionChart.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 04.10.24.
//

import SwiftUI
import Charts

enum ImageTileDistributionChartMode {
	case hfr
	case ecc
}


public struct ImageTileDistributionChart: View {
	let tile: TiltTileRect
	let mode: ImageTileDistributionChartMode
	
	func getData() -> [BinnedDataValue] {
		if mode == .hfr {
			return tile.hfdDistribution
		}
		return tile.eccDistribution
	}
	
	func getMeanAndStdDev() -> (mean: Float, stdDev: Float, max: Float)? {
		if mode == .hfr {
			if let mean = tile.meanHFD, let stdDev = tile.stdDevHFD, let max = tile.maxHFD {
				return (mean: mean, stdDev: stdDev, max: max)
			}
		} else if let mean = tile.meanECC, let stdDev = tile.stdDevECC, let max = tile.stdDevECC {
			return (mean: mean, stdDev: stdDev, max: max)
		}
		return nil
	}
	
	
	@available(macOS 15.0, *)
	func showNormalDistribution(mean: Float, stdDev: Float, maxValue: Float) -> some ChartContent {
		LinePlot(
				x: "Value",
				y: "Probability"
				
		) { x in
			Double(normalDistribution(x: Float(x), mean: mean, stdDev: stdDev) * maxValue)
		}
		.foregroundStyle(.yellow)
	}
	
	
	public var body: some View {
		
		let binnedData = getData()
		
		return Chart(binnedData, id: \.index) { element in
				BarMark(
					x: .value(
						mode == .hfr ? "HFR" : "Eccentricity",
						element.range
					),
					y: .value(
						"Frequency",
						element.frequency
					),
					stacking: .unstacked
				)
				.foregroundStyle(Color.blue)
				.accessibilityHidden(true)
				
			
				if let (mean, stdDev, max) = self.getMeanAndStdDev() {
					
					if #available(macOS 15, *) {
						showNormalDistribution(mean: mean, stdDev: stdDev, maxValue: max)
					}
				}
			
		}
		.chartXAxis {
			AxisMarks(values: .automatic) {
				AxisValueLabel().foregroundStyle(Color.white)
				AxisGridLine().foregroundStyle(Color.white)
			}
		}
		.chartYAxis {
			AxisMarks(values: .automatic) {
				AxisValueLabel().foregroundStyle(Color.white)
				AxisGridLine().foregroundStyle(Color.white)
			}
		}
		.padding()
	}
}
