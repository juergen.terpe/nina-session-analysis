//
//  StarAnalyzerProvider.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 12.08.24.
//
import SwiftUI

@Observable
class StarAnalyzeProvider {
	var stars: [DetectedStar] = []
	var largestStars: [DetectedStar] = []
	var tiltInspection: TiltInspectionResult? = nil
	var imageWidth: Int = 1
	var imageHeight: Int = 1
	var id: UUID = UUID()
	
	func clear() async {
		await MainActor.run {
			self.stars = []
			self.largestStars = []
			self.tiltInspection = nil
			self.imageWidth = 1
			self.imageHeight = 1
			self.id = UUID()
		}
	}
	
	func update(image: AstroImage?) async {
		if let cgImage = image?.cgImage {
			
			
			let starDetector = StarDetector()
			let stars = await starDetector.process(cgImage)
			let largest = Array(stars[..<min(2500, stars.count)])
			
			await MainActor.run {
				self.stars = stars
				self.largestStars = largest
				self.tiltInspection = nil
				self.imageWidth = cgImage.width
				self.imageHeight = cgImage.height
				self.id = UUID()
			}
			
			let tiltInspector = TiltInspector()
			if let result = await tiltInspector.process(cgImage, stars: stars) {
				await MainActor.run {
					self.tiltInspection = result
					self.id = UUID()
				}
			}
			
		} else {
			await MainActor.run {
				self.stars = []
				self.largestStars = []
				self.tiltInspection = nil
				self.imageWidth = 1
				self.imageHeight = 1
				self.id = UUID()
			}
		}
	}
	
	func getTileTiltPositions() -> [(pos: CGPoint, hfd: Float, vec: CGVector)] {
		
		var positions = [(pos: CGPoint, hfd: Float, vec: CGVector)]()
		
		guard let inspection = tiltInspection else { return [] }
		
		let tiles = inspection.tiles
		let scale = 0.2
		
		if let tile = tiles[TiltTileRectType.leftTop] {
			let center = tile.center
			let minXY = tile.size*scale
			let vec = tile.vector.scaled(CGFloat(tile.delta) * minXY)
			positions.append((pos: center.offset(vec.x, vec.y), hfd: tile.hfd, vec: tile.vector))
		}
		
		if let tile = tiles[TiltTileRectType.middleTop] {
			let center = tile.center
			let minXY = tile.size*scale
			let vec = tile.vector.scaled(CGFloat(tile.delta) * minXY)
			positions.append((pos: center.offset(vec.x, vec.y), hfd: tile.hfd, vec: tile.vector))
		}
		
		if let tile = tiles[TiltTileRectType.rightTop] {
			let center = tile.center
			let minXY = tile.size*scale
			let vec = tile.vector.scaled(CGFloat(tile.delta) * minXY)
			positions.append((pos: center.offset(vec.x, vec.y), hfd: tile.hfd, vec: tile.vector))
		}
		
		if let tile = tiles[TiltTileRectType.rightMiddle] {
			let center = tile.center
			let minXY = tile.size*scale
			let vec = tile.vector.scaled(CGFloat(tile.delta) * minXY)
			positions.append((pos: center.offset(vec.x, vec.y), hfd: tile.hfd, vec: tile.vector))
		}
		
		if let tile = tiles[TiltTileRectType.middleMiddle] {
			let center = tile.center
			let minXY = tile.size*scale
			let vec = tile.vector.scaled(CGFloat(tile.delta) * minXY)
			positions.append((pos: center.offset(vec.x, vec.y), hfd: tile.hfd, vec: tile.vector))
		}
		
		if let tile = tiles[TiltTileRectType.leftMiddle] {
			let center = tile.center
			let minXY = tile.size*scale
			let vec = tile.vector.scaled(CGFloat(tile.delta) * minXY)
			positions.append((pos: center.offset(vec.x, vec.y), hfd: tile.hfd, vec: tile.vector))
		}
		
		if let tile = tiles[TiltTileRectType.leftBottom] {
			let center = tile.center
			let minXY = tile.size*scale
			let vec = tile.vector.scaled(CGFloat(tile.delta)  * minXY)
			positions.append((pos: center.offset(vec.x, vec.y), hfd: tile.hfd, vec: tile.vector))
		}
		
		if let tile = tiles[TiltTileRectType.middleBottom] {
			let center = tile.center
			let minXY = tile.size*scale
			let vec = tile.vector.scaled(CGFloat(tile.delta) * minXY)
			positions.append((pos: center.offset(vec.x, vec.y), hfd: tile.hfd, vec: tile.vector))
		}
		
		if let tile = tiles[TiltTileRectType.rightBottom] {
			let center = tile.center
			let minXY = tile.size*scale
			let vec = tile.vector.scaled(CGFloat(tile.delta) * minXY)
			positions.append((pos: center.offset(vec.x, vec.y), hfd: tile.hfd, vec: tile.vector))
		}
		
		return positions
	}
}
