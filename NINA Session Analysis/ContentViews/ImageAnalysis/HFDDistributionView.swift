//
//  HFDDistributionView.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 12.08.24.
//
import SwiftUI



struct HFDDistributionView: View {
	@Binding var heatmapProvider: HFDHeatmapProvider
	
	var body: some View {
		ZStack {
			Color.clear
			
			if let image = self.heatmapProvider.heatmap {
				Image(image, scale: 1.0, label: Text("HFD"))
					.resizable()
					.aspectRatio(contentMode: .fit)
			}
		}
		
	}
	
}



