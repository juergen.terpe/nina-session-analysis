//
//  HistogramView.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 06.10.24.
//

import SwiftUI
import Charts
import PureSwiftUI

struct HistogramValue: Identifiable {
	let id = UUID()
	let channel: Channel
	let adu: Int
	let count: Float
}

struct HistogramValueChannel : Identifiable {
	let id: Channel
	let channel: Channel
	let values: [HistogramValue]
}


@Observable class HistogramViewModel {
	var statistics: [ImageStatisticsValues] = []
	var histogram: [HistogramValueChannel] = []
	
	func update(with image: AstroImage, logarithmic: Bool) async {
		
		if let cgImage = image.cgImage {
			async let statisticsTask = ImageStatisticsValues.from(image: image)
			async let histogramTask = processHistogram(from: cgImage, logarithmic: logarithmic)
		
			let histogramData = await histogramTask
			let data = await statisticsTask
				
			await MainActor.run {
				self.histogram = histogramData
				self.statistics = data
				
			}
		}
		
	
	}
	
	private func processHistogram(from image: CGImage, logarithmic: Bool) async -> [HistogramValueChannel] {
		var result = [HistogramValueChannel]()
		
		guard let histogram = await image.histogram() else {
			return result
		}
			
		await withTaskGroup(of: HistogramValueChannel.self) { group in
			for key in histogram.keys {
				if let data = histogram[key] {
					group.addTask {
						return self.process(key, data, logarithmic)
					}
				}
			}
			
			for await histogram in group {
				result.append(histogram)
			}
		}
	
		return result
	}
	
	private func process(_ channel: Channel, _ data: [UInt], _ logarithmic: Bool) -> HistogramValueChannel {
		var values = [HistogramValue]()
		for (adu, value) in data.enumerated() {
			
			if logarithmic {
				values.append(HistogramValue(channel: channel, adu: adu, count: log10(Float(value))))
			} else {
				values.append(HistogramValue(channel: channel, adu: adu, count: Float(value)))
			}
		}
		
		return HistogramValueChannel(id: channel, channel: channel, values: values)
	}
	
	
}


/// A SwiftUI Image Histogram View (for RGB channels)
public struct HistogramView: View {
	@Binding var model: HistogramViewModel
	
	public var body: some View {
		Chart {
			
			
			if let red = self.model.histogram.first(where: { $0.channel == .r }) {
				
				ForEach(red.values) { hv in
					LineMark(x: .value("ADU", hv.adu),
							y: .value("Count", hv.count)
					)
					.foregroundStyle(by: .value("ADU", "RED"))
					.accessibilityHidden(true)
				}
				
				if let stat = self.model.statistics.first(where: { $0.channel == .r }) {
					RuleMark(x: .value("MedianR", Float(stat.median)/65356.0 * 256.0))
						.lineStyle(StrokeStyle(lineWidth: 0.5, dash: [2]))
						.foregroundStyle(Color.red)
				}
			}
			
			if let green = self.model.histogram.first(where: { $0.channel == .g }) {
				ForEach(green.values) { hv in
					LineMark(x: .value("ADU", hv.adu),
							y: .value("Count", hv.count))
					.foregroundStyle(by: .value("ADU", "GREEN"))
					.accessibilityHidden(true)
				}
			
				if let stat = self.model.statistics.first(where: { $0.channel == .g }) {
					RuleMark(x: .value("MedianG", Float(stat.median)/65356.0 * 256.0))
						.lineStyle(StrokeStyle(lineWidth: 0.5, dash: [2]))
						.foregroundStyle(Color.green)
				}
			}
			
			if let blue = self.model.histogram.first(where: { $0.channel == .b }) {
				ForEach(blue.values) { hv in
					LineMark(x: .value("ADU", hv.adu),
							y: .value("Count", hv.count)
					)
					.foregroundStyle(by: .value("ADU", "BLUE"))
					.accessibilityHidden(true)
				}
				
				if let stat = self.model.statistics.first(where: { $0.channel == .b }) {
					RuleMark(x: .value("MedianB", Float(stat.median)/65356.0 * 256.0))
						.lineStyle(StrokeStyle(lineWidth: 0.5, dash: [2]))
						.foregroundStyle(Color.blue)
				}
			}
			
			if let gray = self.model.histogram.first(where: { $0.channel == .l }) {
				ForEach(gray.values) { hv in
					LineMark(x: .value("ADU", hv.adu),
							y: .value("Count", hv.count)
					)
					.foregroundStyle(by: .value("ADU", "LUM"))
					.accessibilityHidden(true)
				}
				
				if let stat = self.model.statistics.first(where: { $0.channel == .l }) {
					RuleMark(x: .value("MedianL", Float(stat.median)/65356.0 * 256.0))
						.lineStyle(StrokeStyle(lineWidth: 0.5, dash: [2]))
						.foregroundStyle(Color.gray)
				}
			}
			
		}
		
		.chartForegroundStyleScale([
			"RED": .red,
			"GREEN": .green,
			"BLUE": .blue,
			"LUM": .gray
		])
		.chartLegend(.hidden)
		.chartPlotStyle { plotContent in
			plotContent
				.background(.chartBackground.gradient.opacity(0.25))
		}
	}
}
