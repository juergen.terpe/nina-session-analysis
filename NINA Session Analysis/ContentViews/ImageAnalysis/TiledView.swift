//
//  TiledView.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 27.07.24.
//

import SwiftUI
import CoreImage
import PureSwiftUI


fileprivate enum Tile {
	case leftTop
	case leftMiddle
	case leftBottom
	case middleTop
	case middleMiddle
	case middleBottom
	case rightTop
	case rightMiddle
	case rightBottom
	
	static func getTile(x: Int, y: Int) -> Tile? {
		switch (x, y) {
			case (0, 0):
				return .leftTop
			case (0, 1):
				return .leftMiddle
			case (0, 2):
				return .leftBottom
			case (1, 0):
				return .middleTop
			case (1, 1):
				return .middleMiddle
			case (1, 2):
				return .middleBottom
			case (2, 0):
				return .rightTop
			case (2, 1):
				return .rightMiddle
			case (2, 2):
				return .rightBottom
			default:
				return nil
		}
	}
}

fileprivate struct TileData {
	let tileId: Tile
	let stars: [DetectedStar]
	let hfr: Float
	let ecc: Float
	let deltaHFR: Float?
	
	init(tileId: Tile, stars: [DetectedStar], hfr: Float, ecc: Float, deltaHFR: Float? = nil) {
		self.tileId = tileId
		self.stars = stars
		self.hfr = hfr
		self.ecc = ecc
		self.deltaHFR = deltaHFR
	}
	
}


// Observable object to manage the image and settings
@Observable
class TiledViewManager {
	var tileSize: CGFloat = 512
	var leftTop: CGImage? = nil
	fileprivate var imageId: UUID? = nil
	fileprivate var images: [Tile : CGImage] = [:]
		
	func update(imageId: UUID?, image: AstroImage?, tileSize size: Int, contrast: CGFloat) async {
		if let cgImage = image?.cgImage?.changeContrast(value: contrast) {
			
			let leftTop = await crop(image: cgImage, tileSize: size, tile: Tile.leftTop)
			let leftMiddle = await crop(image: cgImage, tileSize: size, tile: Tile.leftMiddle)
			let leftBottom = await crop(image: cgImage, tileSize: size, tile: Tile.leftBottom)
			 
			let middleTop = await crop(image: cgImage, tileSize: size, tile: Tile.middleTop)
			let middleMiddle = await crop(image: cgImage, tileSize: size, tile: Tile.middleMiddle)
			let middleBottom = await crop(image: cgImage, tileSize: size, tile: Tile.middleBottom)
			
			let rightTop = await crop(image: cgImage, tileSize: size, tile: Tile.rightTop)
			let rightMiddle = await crop(image: cgImage, tileSize: size, tile: Tile.rightMiddle)
			let rightBottom = await crop(image: cgImage, tileSize: size, tile: Tile.rightBottom)
			
			await MainActor.run {
				self.images[.leftTop] = leftTop
				self.images[.leftMiddle] = leftMiddle
				self.images[.leftBottom] = leftBottom
				
				self.images[.middleTop] = middleTop
				self.images[.middleMiddle] = middleMiddle
				self.images[.middleBottom] = middleBottom
				
				self.images[.rightTop] = rightTop
				self.images[.rightMiddle] = rightMiddle
				self.images[.rightBottom] = rightBottom
			}
			
			
		}
	}
	
	
	
	
	
	private func crop(image: CGImage, tileSize: Int, tile: Tile) async -> CGImage?  {
		var rect: CGRect = CGRect.zero
		let width = image.width
		let height = image.height
		
		let maxTileWidth = width / 3
		let maxTileHeight = height / 3
		let tw = min(tileSize, maxTileWidth)
		let th = min(tileSize, maxTileHeight)
		
		switch tile {
			case .leftTop:
				rect = CGRect(x: 0, y: 0, width: tw, height: th)
			case .leftMiddle:
				rect = CGRect(x: 0, y: height/2 - th/2, width: tw, height: th)
			case .leftBottom:
				rect = CGRect(x: 0, y: height - th, width: tw, height: th)
			case .rightTop:
				rect = CGRect(x: width - tw, y: 0, width: tw, height: th)
			case .rightMiddle:
				rect = CGRect(x: width - tw, y: height/2 - th/2, width: tw, height: th)
			case .rightBottom:
				rect = CGRect(x: width - tw, y: height - th, width: tw, height: th)
			case .middleTop:
				rect = CGRect(x: width/2 - tw/2, y: 0, width: tw, height: th)
			case .middleMiddle:
				rect = CGRect(x: width/2 - tw/2, y: height/2 - th/2, width: tw, height: th)
			case .middleBottom:
				rect = CGRect(x: width/2 - tw/2, y: height - th, width: tw, height: th)
		}
		
		return image.cropping(to: rect)
	}
	
}


struct TiledView: View {
	@Binding var imageDataProvider: ImageAnalysisDataProvider
	@State private var tileManager = TiledViewManager()
	@State private var tileSize: CGFloat = 512
	@State private var contrast: CGFloat = 1.1
	
	
	private func getTileColor(tile: TileData) -> Color {
		if let deltaHFR = tile.deltaHFR {
			print(deltaHFR)
			if abs(deltaHFR) < 0.05 {
				return Color.green
			} else if (abs(deltaHFR) < 0.1) {
				return Color.yellow
			} else if (abs(deltaHFR) < 0.125) {
				return Color.orange
			} else {
				return Color.red
			}
		}
		
		return Color.white
	}
	
	/*
	@ViewBuilder
	func statisticsView() -> some View {
		
		VStack {
			
			if let tiles = self.starAnalyzer.tiltInspection?.tiles {
				
				HStack {
					if let tile = tiles[.leftTop] {
						ImageTileDistributionChart(tile: tile, mode: statisticsMode)
					}
					
					if let tile = tiles[.middleTop] {
						ImageTileDistributionChart(tile: tile, mode: statisticsMode)
					}
					
					if let tile = tiles[.rightTop] {
						ImageTileDistributionChart(tile: tile, mode: statisticsMode)
					}
				}
				
				HStack {
					
					if let tile = tiles[.leftMiddle] {
						ImageTileDistributionChart(tile: tile, mode: statisticsMode)
					}
					
					if let tile = tiles[.middleMiddle] {
						ImageTileDistributionChart(tile: tile, mode: statisticsMode)
					}
					
					if let tile = tiles[.rightMiddle] {
						ImageTileDistributionChart(tile: tile, mode: statisticsMode)
					}
				}
				
				HStack {
					if let tile = tiles[.leftBottom] {
						ImageTileDistributionChart(tile: tile, mode: statisticsMode)
					}
					
					if let tile = tiles[.middleBottom] {
						ImageTileDistributionChart(tile: tile, mode: statisticsMode)
					}
					
					if let tile = tiles[.rightBottom] {
						ImageTileDistributionChart(tile: tile, mode: statisticsMode)
					}
				}
			}
		}
	}
	 */
	
	
	var body: some View {
		GeometryReader { geometry in
			
			let size = max(1, min(geometry.size.width - 170, geometry.size.height))
			
			HStack(spacing: 0) {
				
				VStack {
					Text("Size")
						.font(Font.system(size: 12).weight(.light))
						.kerning(1.2)
						.foregroundStyle(Color.primary)
						.padding(.vertical)
					
					VSlider(value: $tileSize, in: 256...1024)
						.frame(width: 20)
					Text("\(Int(tileSize))px")
						.padding(.vertical)
					
					Button {
						self.tileSize = 512
					} label: {
						Text("Reset")
					}
					.buttonStyle(.bordered)
					.padding(.bottom)
				}
				.frame(width: 74)
				.padding(.horizontal, 4)
				
				VStack {
					Text("Contrast")
						.font(Font.system(size: 12).weight(.light))
						.kerning(1.2)
						.foregroundStyle(Color.primary)
						.padding(.vertical)
					VSlider(value: $contrast, in: 0.85...1.25)
						.frame(width: 20)
					Text("\(contrast, specifier: "%.2f")")
						.padding(.vertical)
					
					Button {
						self.contrast = 1.1
					} label: {
						Text("Reset")
					}
					.buttonStyle(.bordered)
					.padding(.bottom)
				}
				.frame(width: 74)
				.padding(.horizontal, 4)
				
				
				
					
				ZStack {
					
					// Adding yellow grid overlay
					VStack(spacing: 0) {
						ForEach(0..<3) { x in
							HStack(spacing: 0) {
								ForEach(0..<3) { y in
									
									ZStack {
										if let tileId = Tile.getTile(x: x, y: y),
										   let image = self.tileManager.images[tileId] {
											Image(nsImage: NSImage(cgImage: image, size: NSSize(tileSize, tileSize)))
												.resizable()
												.aspectRatio(contentMode: .fill)
										}
										
										Rectangle()
											.stroke(Color.black, lineWidth: 2)
											.aspectRatio(1, contentMode: .fit)
										
									}
									
									
								}
							}
						}
					}
					
				}
				.frame(width: size, height: size)
				.padding(.horizontal, 8)
				
				.task {
					await self.tileManager.update(imageId: self.imageDataProvider.imageId,
												  image: self.imageDataProvider.stretchedImage, tileSize: Int(self.tileSize), contrast: self.contrast)
				}
				.onChange(of: self.tileSize, debounceTime: .milliseconds(250)) { newValue in
					Task {
						await self.tileManager.update(imageId: self.imageDataProvider.imageId,
													  image: self.imageDataProvider.stretchedImage, tileSize: Int(newValue), contrast: self.contrast)
					}
				}
				.onChange(of: self.contrast, debounceTime: .milliseconds(250)) { newValue in
					Task {
						await self.tileManager.update(imageId: self.imageDataProvider.imageId,
													  image: self.imageDataProvider.stretchedImage, tileSize: Int(self.tileSize), contrast: newValue)
					}
				}
				.onChange(of: self.imageDataProvider.id) { _, newValue in
					Task {
						await self.tileManager.update(imageId: self.imageDataProvider.imageId,
													  image: self.imageDataProvider.stretchedImage, tileSize: Int(self.tileSize), contrast: self.contrast)
					}
				}
				
				
				Spacer()
			}
			

			
		}
		.padding()
		
	}
}


