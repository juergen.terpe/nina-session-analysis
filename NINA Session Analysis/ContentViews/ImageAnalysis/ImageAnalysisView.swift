//
//  ImageAnalysisView.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 22.06.24.
//

import SwiftUI
import AppKit
import PureSwiftUI

fileprivate enum ImageAnalysisViewMode {
	case tiled
	case detectedStars
	case hfd
	case eccentricity
    case graphs
	case histogram
	case starProfiles
}


fileprivate enum ProcessingState {
	case none
	case loadingImage
	case detectingStars
	case detectingHFDContour
}


@Observable class ImageAnalysisDataProvider {
	var image: AstroImage? = nil
	var imageId: UUID? = nil
	var stretchedImage: AstroImage? = nil
	var debayeredImage: AstroImage? = nil
	var id: UUID = UUID()
	
	func clear() {
		self.image = nil
		self.stretchedImage = nil
		self.id = UUID()
	}
	
	var imageSize: CGSize? {
		if let image = self.image {
			return CGSize(width: image.width, height: image.height)
		}
		return nil
	}
	
	func updateImage(from session: SessionData, id: UUID) async {
		if let sessionImage = session.sessionImages.first(where: { $0.id == id}) {
			if let astroImage = await sessionImage.getImageData() {
				
				let (stretched, debayered) = astroImage.debayerAndStretch()
				
				await MainActor.run {
					self.image = astroImage
					self.stretchedImage = stretched
					self.debayeredImage = debayered
					self.imageId = id
					self.id = UUID()
				}
			}
		}
	}
	
	func imageForHistogram() -> AstroImage? {
		if let image = self.image {
			if !image.fits {
				return image
			}
			return self.debayeredImage
		}
		return nil
	}
}

@Observable
class ImageAnalysisProvider {
	var filters : [String] = []
	var selectedImageId: UUID? = nil
	var images : [SessionImage] = []
	
	private var session: SessionData? = nil
	private var updating = false
	
	init() {
		
		updating = true
		self.ignoreBadImages = UserDefaults.standard.bool(forKey: "ImageAnalysisIgnoreBadImages")
		updating = false
	}
	
	var selectedFilter : String = "" {
		didSet {
			if !updating {
				let images = session?.sessionImages.filter({ ($0.filter == selectedFilter || selectedFilter == "Any") && (!$0.bad || !ignoreBadImages) })
				self.images = images ?? []
				self.selectedImageId = images?.first?.id
			}
		}
	}
	
	var ignoreBadImages: Bool = false {
		didSet {
			
			if !updating {
				UserDefaults.standard.set(ignoreBadImages, forKey: "ImageAnalysisIgnoreBadImages")
				
				let images = session?.sessionImages.filter({ ($0.filter == selectedFilter || selectedFilter == "Any") && (!$0.bad || !ignoreBadImages) })
				self.images = images ?? []
				self.selectedImageId = images?.first?.id
			}
		}
	}
	
	
	public func update(session: SessionData) async {
		let filters = session.filters.sorted()
		if filters.isEmpty {
			await MainActor.run {
				self.filters = ["No Filter"]
				self.selectedFilter = "No Filter"
				self.images = session.sessionImages
				return
			}
		}
		
		let selectedFilter = filters[0]
		let images = session.sessionImages.filter({ ($0.filter == selectedFilter || selectedFilter == "Any") && (!$0.bad || !ignoreBadImages) })
		
		await MainActor.run {
			self.updating = true
			self.session = session
			self.filters = filters
			self.selectedFilter = selectedFilter
			self.images = images
			self.selectedImageId = images.first?.id
			self.updating = false
		}
	}
}


struct ImageAnalysisView: View {
	@State var session: SessionData
	@State private var dataProvider = ImageAnalysisDataProvider()
	@State private var starAnalyzer = StarAnalyzeProvider()
	@State private var hfdContourProvider = HFDContourPlotProvider()
	@State private var eccContourProvider = EccentrictyContourPlotProvider()
	@AppStorage("ContourSteps") private var contourSteps = 8.0
	
	@State private var processingImage: ProcessingState = ProcessingState.none
	@State private var viewMode = ImageAnalysisViewMode.tiled
	@State private var analysisProvider = ImageAnalysisProvider()
	
	@ViewBuilder
	func rowContextMenu( _ image: SessionImage) -> some View {
		if image.isFits {
			
			if self.session.quickFitsAvailable {
				Button {
					self.session.openInQuickFits( image )
				} label: {
					Text("Open in QuickFits")
				}
			}
			
			if self.session.fitsPreviewAvailable {
				Button {
					self.session.openInFitsPreview( image )
				} label: {
					Text("Open in FITS Preview")
				}
			}
		}
		
		if self.session.pixInsightAvailable {
			Button {
				self.session.openInPixInsight( image )
			} label: {
				Text("Open in PixInsight")
			}
		}
		
		if self.session.astapAvailable {
			Button {
				self.session.openInAstap( image )
			} label: {
				Text("Open in Astap")
			}
		}
	}
	
	private func getProcessingState(for image: SessionImage) -> LocalizedStringKey {
		switch self.processingImage {
			case .none: return ""
			case .detectingHFDContour: return "Detecting HFD Contour..."
			case .detectingStars: return "Detecting Stars..."
			case .loadingImage: return "Processing image \(image.FileName)"
		}
	}
	
	
	var body: some View {
		VStack {
			Color.clear.frame(height: 40)
						
			HStack(spacing: 4) {
				
				VStack(alignment: .trailing, spacing: 12) {
					
					Picker(selection: self.$analysisProvider.selectedFilter) {
						ForEach(self.analysisProvider.filters, id: \.self) { filter in
							Text(filter)
						}
					} label: {
						Text("Filter:")
					}

					Toggle(isOn: self.$analysisProvider.ignoreBadImages) {
						Text("Ignore Lights marked as BAD")
					}
					.toggleStyle(.checkbox)
										
					List(selection: self.$analysisProvider.selectedImageId) {
						ForEach(self.analysisProvider.images, id: \.self) { img in
							Text(img.FileName)
								.truncationMode(.middle)
								.lineLimit(1)
								.tag(img.id)
								.padding(.horizontal)
								.padding(.vertical, 4)
								.listRowSeparator(.hidden)
								.contextMenu {
									rowContextMenu(img)
								}
						}
					}
				}
				.frame(minWidth: 200, maxWidth: 380)
				.padding(.bottom, 16)
								
				VStack {
					
					ZStack {
						
						
						TabView(selection: $viewMode) {
							TiledView(imageDataProvider: self.$dataProvider)
								.tabItem {
									Text("Aberration Inspector")
								}
								.tag(ImageAnalysisViewMode.tiled)
							
							TiltView(imageDataProvider: self.$dataProvider, starAnalyzer: self.$starAnalyzer)
								.tabItem {
									Text("Tilt/Backfocus")
								}
								.tag(ImageAnalysisViewMode.detectedStars)
							
							
							HFDContourView(imageDataProvider: self.$dataProvider,
										   starAnalyzer: self.$starAnalyzer, provider: self.$hfdContourProvider,
										   contourSteps: self.$contourSteps)
								.tabItem {
									Text("HFD Contour")
								}
								.tag(ImageAnalysisViewMode.hfd)
						
						
							TiltStatisticsView(imageDataProvider: self.$dataProvider, starAnalyzer: self.$starAnalyzer)
								.tabItem {
									Text("Tilt Graphs")
								}
								.tag(ImageAnalysisViewMode.graphs)
							
							ImageHistogramView(imageDataProvider: self.$dataProvider)
								.tabItem {
									Text("Histogram")
								}
								.tag(ImageAnalysisViewMode.histogram)
							
							ImgeStarProfilesView(imageDataProvider: self.$dataProvider, starAnalyzer: self.$starAnalyzer)
								.tabItem {
									Text("Star Profiles")
								}
								.tag(ImageAnalysisViewMode.starProfiles)
							
							
						}
						
						if processingImage != .none {
							ProgressView {
								if let image = self.analysisProvider.images.first(where: { $0.id == self.analysisProvider.selectedImageId }) {
									Text(getProcessingState(for: image))
								} else {
									Text("Processing image  ...")
								}
							}
							.padding()
							.background(.thinMaterial).cornerRadius(8)
						}
						
					}
					.padding(8)
					
					
					
				}
				.onChange(of: self.analysisProvider.selectedImageId) { _, _ in
					if let imageId = self.analysisProvider.selectedImageId {
						self.processingImage = .loadingImage
						
						print("onChange selectedImageId")
						
						Task {
							await self.starAnalyzer.clear()
							await self.dataProvider.updateImage(from: self.session, id: imageId)
						}
						
					} else {
						self.dataProvider.clear()
					}
				}
			}.padding(.horizontal)
		}
		.task {
			await self.analysisProvider.update(session: self.session)
		}
		
		.onChange(of: self.dataProvider.id) { _, _ in
			self.processingImage = .detectingStars
			print("onChange dataProvider.id")
			
			Task {
				if let image = self.dataProvider.stretchedImage {
					
					await MainActor.run {
						self.processingImage = .detectingStars
					}
					
					await self.starAnalyzer.update(image: image)
					
					await MainActor.run {
						self.processingImage = .detectingHFDContour
					}
					
					await self.hfdContourProvider.update(stars: self.starAnalyzer.stars,
														 width: image.width, height: image.height,
														 grayscales: Int(self.contourSteps))
					
					await MainActor.run {
						self.processingImage = .none
					}
				}
			}
			
		}
		
	}
}
