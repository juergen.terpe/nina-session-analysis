//
//  HFDContourView.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 15.09.24.
//

import AppKit
import SwiftUI

public struct HFDContourView: View {
	@Binding var imageDataProvider: ImageAnalysisDataProvider
	@Binding var starAnalyzer: StarAnalyzeProvider
	@Binding var provider: HFDContourPlotProvider
	@Binding var contourSteps: Double
	@State private var processingImage: Bool = false
	
	private func project(_ point: CGPoint, _ sx: CGFloat, _ sy: CGFloat) -> CGPoint {
		return CGPoint(point.x * sx, point.y * sy)
	}
	
	private func project(_ rect: CGRect, _ sx: CGFloat, _ sy: CGFloat) -> CGRect {
		let origin = rect.origin
		let newOrigin = CGPoint(origin.x * sx, origin.y * sy)
		let size = CGSize(rect.size.width*sx, rect.size.height*sy)
		
		return CGRect(origin: newOrigin, size: size)
	}
	
	@ViewBuilder
	func showDetectedStars() -> some View {
		Canvas { context, size in
			
			let sx = size.width / CGFloat(self.starAnalyzer.imageWidth)
			let sy = size.height / CGFloat(self.starAnalyzer.imageHeight)
			
			var textRects = [CGRect]()
			
			context.addFilter(GraphicsContext.Filter.shadow(color: .black, radius: 3, blendMode: GraphicsContext.BlendMode.plusDarker))
			
			for star in self.provider.hfdValues {
				
				let center = project(star.position, sx, sy)
				let text = context.resolve(Text("\(star.hfd, specifier: "%.2f")")
					.font(.body.monospacedDigit().bold())
					.foregroundStyle(.yellow.opacity(0.8)))
					             
				let textSize = text.measure(in: size)
				let textRect = CGRect.from(center: center, size: textSize)
				if textRect.minX < 0 || textRect.minY < 0 || textRect.maxX > size.width || textRect.maxY > size.height {
					continue
				}
				
				if let _ = textRects.first(where: { $0.intersects(textRect)}) {
					continue
				}
				
				context.draw(text, in: textRect)
				textRects.append(textRect.insetBy(dx: -10, dy: -10))
			}
		}
	}
	
	public var body: some View {
		
		VStack(alignment: .leading) {
			
			HStack(spacing: 32) {
				
				if let minHFD = provider.minHFD, let maxHFD = provider.maxHFD {
					Text("HFD \(minHFD, specifier: "%.2f")px - \(maxHFD, specifier: "%.2f")px")
				} 
				
				Slider(value: self.$contourSteps, in: 6...64).frame(width: 600)

				Spacer()
			}
			.padding()
			
				
			if let image = self.provider.image {
				image
					.resizable()
					.aspectRatio(contentMode: .fit)
					.overlay {
						showDetectedStars()
					}
			} else {
				Spacer()
			
			}
		
		}
		.onChange(of: self.contourSteps, debounceTime: .milliseconds(500)) { _ in
			Task {
				await self.provider.updateImage(grayscales: Int(self.contourSteps))
			}
		}
		
	}
}
