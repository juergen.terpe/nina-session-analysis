//
//  PipelineStates.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 19.01.25.
//
import MetalKit

struct PipelineStates {
	static func createPSO(descriptor: MTLRenderPipelineDescriptor, device: MTLDevice) -> MTLRenderPipelineState {
		let pipelineState: MTLRenderPipelineState
		do {
			pipelineState = try device.makeRenderPipelineState(descriptor: descriptor)
		} catch {
			fatalError(error.localizedDescription)
		}
		return pipelineState
	}

	static func createComputePSO(function: String,
							   device: MTLDevice,
							   library: MTLLibrary) -> MTLComputePipelineState {
		guard let kernel = library.makeFunction(name: function) else {
			fatalError("Unable to create \(function) PSO - missing in library")
		}
		let pipelineState: MTLComputePipelineState
		do {
			pipelineState = try device.makeComputePipelineState(function: kernel)
		} catch {
			fatalError(error.localizedDescription)
		}
		return pipelineState
	}

	static func createRenderPSO(colorPixelFormat: MTLPixelFormat,
								device: MTLDevice,
								library: MTLLibrary,
								maxTessellation: Int) -> MTLRenderPipelineState {
		
		let vertexFunction = library.makeFunction(name: "starProfileVertex")
		let fragmentFunction = library.makeFunction(name: "starProfileFragment")
		let pipelineDescriptor = MTLRenderPipelineDescriptor()
		pipelineDescriptor.vertexFunction = vertexFunction
		pipelineDescriptor.fragmentFunction = fragmentFunction
		pipelineDescriptor.colorAttachments[0].pixelFormat = colorPixelFormat
		pipelineDescriptor.depthAttachmentPixelFormat = .depth32Float

		// set up vertex descriptor
		let vertexDescriptor = MTLVertexDescriptor()
		vertexDescriptor.attributes[0].format = .float3
		vertexDescriptor.attributes[0].offset = 0
		vertexDescriptor.attributes[0].bufferIndex = 0
		vertexDescriptor.layouts[0].stride = MemoryLayout<float3>.stride
		vertexDescriptor.layouts[0].stepFunction = .perPatchControlPoint
		pipelineDescriptor.vertexDescriptor = vertexDescriptor

		pipelineDescriptor.tessellationFactorStepFunction = .perPatch
		pipelineDescriptor.maxTessellationFactor = maxTessellation
		pipelineDescriptor.tessellationPartitionMode = .pow2

		return createPSO(descriptor: pipelineDescriptor, device: device)
	}
}
