
import CoreGraphics
import simd 

enum Settings {
  static var rotationSpeed: Float { 2.0 }
  static var translationSpeed: Float { 3.0 }
  static var mouseScrollSensitivity: Float { 0.1 }
  static var mousePanSensitivity: Float { 0.008 }
  static var touchZoomSensitivity: Float { 10 }
}

protocol Camera: Transformable {
  var projectionMatrix: float4x4 { get }
  var viewMatrix: float4x4 { get }
  mutating func update(size: CGSize)
  mutating func update(deltaTime: Float)
}

struct ArcballCamera: Camera {
	var transform = Transform()
	var aspect: Float = 1.0
	var fov = Float(54).degreesToRadians
	var near: Float = 0.1
	var far: Float = 100
	
	var projectionMatrix: float4x4 {
		float4x4(projectionFov: fov,
				 near: near,
				 far: far,
				 aspect: aspect)
	}
	
	let minDistance: Float = 0.0
	let maxDistance: Float = 60
	var target: float3 = [0, 0, 0]
	var distance: Float = 2

	mutating func update(size: CGSize) {
		aspect = Float(size.width / size.height)
	}

	var viewMatrix: float4x4 {
		return float4x4(eye: position, center: target, up: [0, 1, 0])
	}

	mutating func update(deltaTime: Float) {
    
		let rotateMatrix = float4x4(rotationYXZ: [-rotation.x, rotation.y, 0])
		let distanceVector = float4(0, 0, -distance, 0)
		let rotatedVector = rotateMatrix * distanceVector
		position = target + rotatedVector.xyz
  }
}
