//
//  MetalStarProfileRenderer.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 19.01.25.
//

import SwiftUI
import MetalKit
import MetalPerformanceShaders
import simd

struct Uniforms {
	var modelMatrix: float4x4
	var viewMatrix: float4x4
	var projectionMatrix: float4x4
	var mvp: float4x4
	
	init() {
		self.modelMatrix = float4x4.identity
		self.viewMatrix = float4x4.identity
		self.projectionMatrix = float4x4.identity
		self.mvp = float4x4.identity
	}
}
/*
struct Params {
	var width: UInt32
	var height: UInt32
	var tiling: UInt32
	
	init() {
		self.width = 64
		self.height = 64
		self.tiling = 16
	}
}*/

enum Attributes: Int {
  case Position = 0
  case Normal = 1
  case UV = 2
}

enum BufferIndices: Int {
  case Vertices = 0
  case UVs = 1
  case Uniforms = 11
  case Params = 12
}


@Observable
class MetalStarProfileRenderer: NSObject {
	private let device: MTLDevice
	private var commandQueue: MTLCommandQueue
	private var library: MTLLibrary
	private var pipelineState: MTLRenderPipelineState?
	private var depthStencilState: MTLDepthStencilState?
	private var tessellationPipelineState: MTLComputePipelineState?
	private var normalGenerator: NormalMapGeneratorMPS?
	
	private var timer: Float = 0
	private var uniforms = Uniforms()
	//private var params = Params()

	private var quad: Quad
	private var camera = ArcballCamera(distance: 2)
	private var lastTime: Double = CFAbsoluteTimeGetCurrent()
	
	// Buffers
	private var tessellationFactorsBuffer: MTLBuffer?
	private var controlPointsBuffer: MTLBuffer?
	
	
	var yRotate: Float = 0.0
	var rotate: Bool = false {
		didSet {
			self.lastTime = CFAbsoluteTimeGetCurrent()
		}
	}
	
	// Textures
	private var heightMap: MTLTexture? = nil
	private var normalMap: MTLTexture? = nil
	
	
	private let patches = (horizontal: 6, vertical: 6)
	private var patchCount: Int {
		patches.horizontal * patches.vertical
	}
	
	private var edgeFactors: [Float] = [4]
	private var insideFactors: [Float] = [4]
	
	
	private func getTessellationFactorsBuffer() -> MTLBuffer? {
		if self.tessellationFactorsBuffer != nil {
			return self.tessellationFactorsBuffer
		}
		
		let count = patchCount * (4 + 2)
		// 2
		let size = count * MemoryLayout<Float>.size / 2
		self.tessellationFactorsBuffer = self.device.makeBuffer(length: size, options: .storageModePrivate)
		return self.tessellationFactorsBuffer
	}
	
	
	private var terrain: Terrain?
	
	private func createTerrain(maxTessellation: Int) -> Terrain {
		return Terrain(
			size: [8, 8],
			height: 1.5,
			maxTessellation: UInt32(maxTessellation))
	}
	
	var modelMatrix: float4x4 {
		
		let currentTime = CFAbsoluteTimeGetCurrent()
		let deltaTime = (currentTime - lastTime)
		
		if self.rotate {
			self.yRotate = Float(deltaTime/60.0 * 360.0).truncatingRemainder(dividingBy: 360.0)
		} else {
			self.yRotate = 0.0
		}
		
		let rotation = float3(Float(-90).degreesToRadians, self.yRotate.degreesToRadians, 0)
		return float4x4(rotation: rotation) * float4x4(translation: float3(0, -0.6, 0))
	}
	
	
	override init() {
		guard
			let device = MTLCreateSystemDefaultDevice(),
			let commandQueue = device.makeCommandQueue() else {
			fatalError("GPU not available")
		}
		self.device = device
		self.commandQueue = commandQueue
		let library = device.makeDefaultLibrary()!
		self.library = library
		self.quad = Quad(vertices: Quad.Vertices, vertexBuffer: MetalStarProfileRenderer.createVertexBuffer(device: device))
		
		let controlPoints = Quad.createControlPoints(patches: patches, size: (2, 2))
		self.controlPointsBuffer = device.makeBuffer(bytes: controlPoints, length: MemoryLayout<float3>.stride * controlPoints.count)
		self.normalGenerator = try? NormalMapGeneratorMPS(device: self.device, library: library)
		super.init()
	
		self.camera.rotation = float3(Float(60).degreesToRadians, 0, 0)
	}
	
	func assignView(_ metalView: MTKView) {
		
		let maxTessellation = self.device.supportsFamily(.apple5) ? 64 : 16
		
		self.terrain = createTerrain(maxTessellation: maxTessellation)
		
		self.pipelineState = PipelineStates.createRenderPSO(colorPixelFormat: metalView.colorPixelFormat,
															device: device, library: library,
															maxTessellation: maxTessellation)
		
		self.depthStencilState = MetalStarProfileRenderer.buildDepthStencilState(device: device)
		
		self.tessellationPipelineState = PipelineStates.createComputePSO(function: "tessellation_main",
																		 device: device,
																		 library: library)
		
		metalView.device = device
		
		metalView.clearColor = MTLClearColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
		metalView.depthStencilPixelFormat = .depth32Float
		metalView.delegate = self
		mtkView(metalView, drawableSizeWillChange: metalView.bounds.size)
	}
	
	static func createVertexBuffer(device: MTLDevice) -> MTLBuffer {
		device.makeBuffer(
			bytes: Quad.Vertices,
			length: MemoryLayout<float3>.stride * Quad.Vertices.count,
			options: [])!
	}
	
	func update(image: CGImage, position pos: CGPoint, size sz: Int) {
		self.heightMap = updateHeightMapTexture(from: image, at: pos, with: CGFloat(sz), device: self.device)
		
		if let normalGenerator = self.normalGenerator {
			self.normalMap = normalGenerator.generateNormalMap(from: self.heightMap!)
		}
	}
	
	func updateHeightMapTexture(from image: CGImage, at center: CGPoint, with size: CGFloat, device: MTLDevice) -> MTLTexture? {
		let imageWidth = CGFloat(image.width)
		let imageHeight = CGFloat(image.height)

		// Ensure the cropping rectangle stays within the bounds of the image
		let cropRect = CGRect(
			x: max(0, min(center.x - size / 2, imageWidth - size)),
			y: max(0, min(center.y - size / 2, imageHeight - size)),
			width: min(size, imageWidth),
			height: min(size, imageHeight)
		)

		// Crop the image to the specified rectangle
		guard let croppedImage = image.cropping(to: cropRect) else {
			print("Failed to crop image.")
			return nil
		}

		// Create a CGContext for the cropped image
		let colorSpace = CGColorSpaceCreateDeviceRGB()
		let bytesPerPixel = 4
		let bytesPerRow = Int(cropRect.width) * bytesPerPixel
		let bitmapData = malloc(Int(cropRect.height) * bytesPerRow) // Allocate memory for pixel data

		guard let context = CGContext(
			data: bitmapData,
			width: Int(cropRect.width),
			height: Int(cropRect.height),
			bitsPerComponent: 8,
			bytesPerRow: bytesPerRow,
			space: colorSpace,
			bitmapInfo: CGImageAlphaInfo.noneSkipLast.rawValue
		) else {
			print("Failed to create CGContext.")
			free(bitmapData)
			return nil
		}

		context.translateBy(x: 0, y: cropRect.height)
		context.scaleBy(x: 1, y: -1)
		
		// Draw the cropped image into the context
		context.draw(croppedImage, in: CGRect(origin: .zero, size: cropRect.size))

		// Create the Metal texture descriptor
		let textureDescriptor = MTLTextureDescriptor()
		textureDescriptor.pixelFormat = .rgba8Unorm
		textureDescriptor.width = Int(cropRect.width)
		textureDescriptor.height = Int(cropRect.height)
		textureDescriptor.usage = [.shaderRead, .shaderWrite]

		// Create the Metal texture
		guard let texture = device.makeTexture(descriptor: textureDescriptor) else {
			print("Failed to create Metal texture.")
			free(bitmapData)
			return nil
		}

		// Copy pixel data into the Metal texture
		texture.replace(region: MTLRegionMake2D(0, 0, Int(cropRect.width), Int(cropRect.height)),
						mipmapLevel: 0,
						withBytes: bitmapData!,
						bytesPerRow: bytesPerRow)

		// Free the bitmap data
		free(bitmapData)

		return texture
	}
	
	
	static func buildDepthStencilState(device: MTLDevice) -> MTLDepthStencilState? {
		let descriptor = MTLDepthStencilDescriptor()
		descriptor.depthCompareFunction = .less
		descriptor.isDepthWriteEnabled = true
		return device.makeDepthStencilState(descriptor: descriptor)
	}
	
}


extension MetalStarProfileRenderer: MTKViewDelegate {
	
	func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) {
		camera.update(size: size)
		//params.width = UInt32(size.width)
		//params.height = UInt32(size.height)
	}
	
	func draw(in view: MTKView) {
		guard let commandBuffer = self.commandQueue.makeCommandBuffer() else {
			return
		}
		updateUniforms()

		tessellation(commandBuffer: commandBuffer)

		render(commandBuffer: commandBuffer, view: view)

		guard let drawable = view.currentDrawable else {
			return
		}
		commandBuffer.present(drawable)
		commandBuffer.commit()
	}
	
	func updateUniforms() {
		camera.update(deltaTime: 0)
		uniforms.projectionMatrix = camera.projectionMatrix
		uniforms.viewMatrix = camera.viewMatrix
		uniforms.modelMatrix = modelMatrix
		uniforms.mvp = uniforms.projectionMatrix * uniforms.viewMatrix * uniforms.modelMatrix
	}
	
	func tessellation(commandBuffer: MTLCommandBuffer) {
		guard let computeEncoder = commandBuffer.makeComputeCommandEncoder() else {
			return
		}
		guard let tessellationPipelineState else {
			return
		}
		
		guard let tessellationFactors = getTessellationFactorsBuffer() else {
			return
		}
				
		computeEncoder.setComputePipelineState(tessellationPipelineState)
		computeEncoder.setBytes(&edgeFactors,
								length: MemoryLayout<Float>.size * edgeFactors.count,
								index: 0)

		computeEncoder.setBytes(&insideFactors,
								length: MemoryLayout<Float>.size * insideFactors.count,
								index: 1)

		computeEncoder.setBuffer(tessellationFactors, offset: 0, index: 2)
		
		var cameraPosition = float4(camera.position, 0)
		computeEncoder.setBytes(&cameraPosition, length: MemoryLayout<float4>.stride, index: 3)
		var matrix = modelMatrix
		
		computeEncoder.setBytes(&matrix, length: MemoryLayout<float4x4>.stride, index: 4)
		computeEncoder.setBuffer(controlPointsBuffer, offset: 0, index: 5)
		computeEncoder.setBytes(&terrain, length: MemoryLayout<Terrain>.stride, index: 6)

		let width = min(patchCount, tessellationPipelineState.threadExecutionWidth)
		let gridSize = MTLSize(width: patchCount, height: 1, depth: 1)
		let threadsPerThreadgroup = MTLSize(width: width, height: 1, depth: 1)
		
		computeEncoder.dispatchThreadgroups(gridSize, threadsPerThreadgroup: threadsPerThreadgroup)
		computeEncoder.endEncoding()
	}
	
	func render(commandBuffer: MTLCommandBuffer, view: MTKView) {
		guard let descriptor = view.currentRenderPassDescriptor,
			  let renderEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: descriptor),
			  let pipelineState,
			  let heightMap,
			  let normalMap else {
			return
		}
		renderEncoder.setDepthStencilState(depthStencilState)

		/*
		renderEncoder.setFragmentBytes(&params,
									   length: MemoryLayout<Uniforms>.stride,
									   index: BufferIndices.Params.index)
		*/
		renderEncoder.setRenderPipelineState(pipelineState)

		renderEncoder.setVertexBytes(&uniforms,
									 length: MemoryLayout<Uniforms>.stride,
									 index: BufferIndices.Uniforms.index)

		// draw
		renderEncoder.setTessellationFactorBuffer(tessellationFactorsBuffer,
												  offset: 0,
												  instanceStride: 0)

		renderEncoder.setVertexBuffer(controlPointsBuffer,
									  offset: 0,
									  index: 0)

		renderEncoder.setTriangleFillMode(.fill)

		
		// set height map
		renderEncoder.setVertexTexture(heightMap, index: 0)
		renderEncoder.setVertexBytes(&terrain, length: MemoryLayout<Terrain>.stride, index: 6)
		renderEncoder.setFragmentTexture(heightMap, index: 1)
		renderEncoder.setFragmentTexture(normalMap, index: 2)
		
		
		renderEncoder.drawPatches(numberOfPatchControlPoints: 4,
								  patchStart: 0,
								  patchCount: patchCount,
								  patchIndexBuffer: nil,
								  patchIndexBufferOffset: 0,
								  instanceCount: 1,
								  baseInstance: 0)

		renderEncoder.endEncoding()
	}
}
