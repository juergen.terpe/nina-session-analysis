//
//  ImgeStarProfilesView.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 11.01.25.
//
import SwiftUI

struct StarCell: View {
	let star: DetectedStar
	
	var body: some View {
		VStack {
			HStack {
				Text("HFD: \(star.hfd, specifier: "%.2f")")
				Spacer()
				Text("ECC: \(star.eccentricity, specifier: "%.2f")")
			}
			HStack {
				Text("Aspect Ratio: \(star.aspectRatio ?? 1.0, specifier: "%.2f")")
				Spacer()
				Text("Flux: \(star.flux, specifier: "%.2f")")
			}
		}
		.padding(4)
		.contentShape(.rect)
	}
}

enum CellSize: Int, CaseIterable {
	case tiny = 128
	case small = 256
	case medium = 512
	case large = 1024
}


struct ImgeStarProfilesView: View {
	@Binding var imageDataProvider: ImageAnalysisDataProvider
	@Binding var starAnalyzer: StarAnalyzeProvider
	@State private var renderer: MetalStarProfileRenderer = MetalStarProfileRenderer()
	@State private var size: CellSize = CellSize.medium
	@State private var position: CGPoint? = nil
	@State private var scale: CGFloat = 1.6
	
	private func project(_ rect: CGRect, _ sx: CGFloat, _ sy: CGFloat) -> CGRect {
		let origin = rect.origin
		let newOrigin = CGPoint(origin.x * sx, origin.y * sy)
		let size = CGSize(rect.size.width*sx, rect.size.height*sy)
		
		return CGRect(origin: newOrigin, size: size)
	}
	
	private func project(_ point: CGPoint, _ sx: CGFloat, _ sy: CGFloat) -> CGPoint {
		return CGPoint(point.x * sx, point.y * sy)
	}
	
	func dragGesture(_ g: GeometryProxy) -> some Gesture {
		DragGesture(minimumDistance: 0, coordinateSpace: .local)
			.onChanged { value in
				
				if let stretchedImage =  self.imageDataProvider.stretchedImage?.cgImage {
					let width = stretchedImage.width
					let height = stretchedImage.height
					
					let sx = CGFloat(width) / g.size.width
					let sy = CGFloat(height) / g.size.height
					self.position = project(value.location, sx, sy)
				}
			}
	}
	
	func getSizeText(_ sz: CellSize) -> String {
		switch sz {
			case .tiny: return "128 px"
			case .small: return "256 px"
			case .medium: return "512 px"
			case .large: return "1024 px"
		}
	}
	
		
	var body: some View {
		
		VStack {
			HStack {
				Picker("Sample size", selection: self.$size) {
					ForEach(CellSize.allCases, id: \.self) { sz in
						Text(getSizeText(sz)).truncationMode(.middle).tag(sz)
					}
				}
				.frame(minWidth: 160, maxWidth: 180)
				.padding()
				
				Toggle(isOn: self.$renderer.rotate) {
					Text("Rotate")
				}
				.toggleStyle(CheckboxToggleStyle())
				.padding(.trailing)
				
				Slider(value: $scale, in: 1.5...3, step: 0.05) {
					Text("Scale").foregroundColor(.secondary)
				}
				.frame(minWidth: 200, maxWidth: 400)
				.padding()
				
				Spacer()
			}
			.frame(height: 30)
			
			if let stretchedImage =  self.imageDataProvider.stretchedImage?.cgImage {
				
				Image(decorative: stretchedImage, scale: 1.0)
					.resizable()
					.aspectRatio(contentMode: .fit)
					.overlay {
						Canvas { context, size in
							if let pos = self.position {
								let width = stretchedImage.width
								let height = stretchedImage.height
								
								let sx = size.width / CGFloat(width)
								let sy = size.height / CGFloat(height)
								
								let bounds = CGRect.from(center: pos, size: CGSize(width: self.size.rawValue/4, height: self.size.rawValue/4))
								let rect = project(bounds, sx, sy)
								
								let path = Path { path in
									path.move(to: rect.topLeading)
									path.addLine(to: rect.topTrailing)
									path.addLine(to: rect.bottomTrailing)
									path.addLine(to: rect.bottomLeading)
									path.addLine(to: rect.topLeading)
								}
								
								context.stroke(path, with: .color(Color.green), style: StrokeStyle(lineWidth: 1))
							}
							
						}
					}
					.overlay {
						GeometryReader { g in
							// 3D Star Profile
							if let pos = self.position {
								
								if Int(pos.y) < stretchedImage.height / 2 {
									VStack {
										Spacer()
										
										if Int(pos.x) < stretchedImage.width / 2 {
											HStack {
												Spacer()
												MetalStarProfileView(renderer: self.$renderer)
													.frame(width: g.size.minDimension/scale, height: g.size.minDimension/scale)
											}
										} else {
											HStack {
												MetalStarProfileView(renderer: self.$renderer)
													.frame(width: g.size.minDimension/scale, height: g.size.minDimension/scale)
												Spacer()
											}
										}
									}
								} else {
									VStack {
										if Int(pos.x) < stretchedImage.width / 2 {
											HStack {
												Spacer()
												MetalStarProfileView(renderer: self.$renderer)
													.frame(width: g.size.minDimension/scale, height: g.size.minDimension/scale)
											}
										} else {
											HStack {
												MetalStarProfileView(renderer: self.$renderer)
													.frame(width: g.size.minDimension/scale, height: g.size.minDimension/scale)
												Spacer()
											}
										}
										
										Spacer()
									}
								}
								
							}
						}
					}
					.overlay {
						GeometryReader { g in
							Color.white.opacity(0.001)
								.gesture(dragGesture(g))
						}
					}
					.popoverTip(ClickAndDragToSelectStarProfileTip(), arrowEdge: .bottom)
			}
			else {
				Color.clear
			}
		}
		.background(Color.controlBackgroundDarkColor)
		.onChange(of: self.position) { _, _ in
			if let image = self.imageDataProvider.stretchedImage?.cgImage, let pos = self.position {
				self.renderer.update(image: image, position: pos, size: self.size.rawValue)
			}
		}
		.onChange(of: self.size) { _, _ in
			if let image = self.imageDataProvider.stretchedImage?.cgImage, let pos = self.position {
				self.renderer.update(image: image, position: pos, size: self.size.rawValue)
			}
		}
		.onChange(of: self.imageDataProvider.id) { _, _ in
			if let image = self.imageDataProvider.stretchedImage?.cgImage, let pos = self.position {
				self.renderer.update(image: image, position: pos, size: self.size.rawValue)
			}
		}
		
	}
}
