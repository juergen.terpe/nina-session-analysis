//
//  Terrain.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 14.12.24.
//
import simd

struct Terrain {
	let size: float2
	let height: Float
	let maxTessellation: UInt32
}
