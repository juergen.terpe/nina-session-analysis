//
//  MetalView.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 19.01.25.
//


import SwiftUI
import MetalKit

struct MetalStarProfileView: View {
	@Binding var renderer: MetalStarProfileRenderer
	
	@State private var metalView = MTKView()
	@State private var previousTranslation = CGSize.zero
	@State private var previousScroll: CGFloat = 1

	var body: some View {
		MetalViewRepresentable(
			renderer: renderer,
			metalView: $metalView)
		/*
		.gesture(DragGesture(minimumDistance: 0)
			.onChanged { value in
				InputController.shared.touchLocation = value.location
				InputController.shared.touchDelta = CGSize(
					width: value.translation.width - previousTranslation.width,
					height: value.translation.height - previousTranslation.height)
				previousTranslation = value.translation
				// if the user drags, cancel the tap touch
				if abs(value.translation.width) > 1 || abs(value.translation.height) > 1 {
					InputController.shared.touchLocation = nil
			}
        }
        .onEnded {_ in
			previousTranslation = .zero
        })
		.gesture(MagnificationGesture()
			.onChanged { value in
				let scroll = value - previousScroll
				InputController.shared.mouseScroll.x = Float(scroll)
				* Settings.touchZoomSensitivity
				previousScroll = value
			}
			.onEnded {_ in
				previousScroll = 1
			})*/
  }
}

typealias ViewRepresentable = NSViewRepresentable

struct MetalViewRepresentable: ViewRepresentable {
	let renderer: MetalStarProfileRenderer?
	@Binding var metalView: MTKView
	
	func makeNSView(context: Context) -> some NSView {
		renderer?.assignView(metalView)
		return metalView
	}
	
	func updateNSView(_ uiView: NSViewType, context: Context) {
		updateMetalView()
	}

	func updateMetalView() {
		
	}
}

#Preview {
	VStack {
		MetalStarProfileView(renderer: .constant(MetalStarProfileRenderer()))
		Text("Metal View")
	}
}
