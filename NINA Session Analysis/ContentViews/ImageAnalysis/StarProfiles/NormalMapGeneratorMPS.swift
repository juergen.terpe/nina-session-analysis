//
//  NormalMapGeneratorMPS.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 21.01.25.
//


import Metal

class NormalMapGeneratorMPS {
	let device: MTLDevice
	let commandQueue: MTLCommandQueue
	let pipelineState: MTLComputePipelineState
	
	init(device: MTLDevice, library: MTLLibrary) throws {
		self.device = device
		self.commandQueue = device.makeCommandQueue()!
		
		// Shader laden
		let function = library.makeFunction(name: "computeNormalMap")!
		self.pipelineState = try device.makeComputePipelineState(function: function)
	}
	
	func generateNormalMap(from texture: MTLTexture) -> MTLTexture? {
		let descriptor = MTLTextureDescriptor.texture2DDescriptor(pixelFormat: .rgba8Unorm,
																  width: texture.width,
																  height: texture.height,
																  mipmapped: false)
		descriptor.usage = [.shaderRead, .shaderWrite]
		guard let outputTexture = device.makeTexture(descriptor: descriptor) else {
			return nil
		}
		
		guard let commandBuffer = commandQueue.makeCommandBuffer(),
			  let commandEncoder = commandBuffer.makeComputeCommandEncoder() else {
			return nil
		}
		
		commandEncoder.setComputePipelineState(pipelineState)
		commandEncoder.setTexture(texture, index: 0)
		commandEncoder.setTexture(outputTexture, index: 1)
		
		var textureSize = float2(Float(texture.width), Float(texture.height))
		commandEncoder.setBytes(&textureSize, length: MemoryLayout<float2>.stride, index: 0)
		
		let threadGroupSize = MTLSize(width: 16, height: 16, depth: 1)
		let threadGroups = MTLSize(width: (texture.width + 15) / 16,
								   height: (texture.height + 15) / 16,
								   depth: 1)
		commandEncoder.dispatchThreadgroups(threadGroups, threadsPerThreadgroup: threadGroupSize)
		
		commandEncoder.endEncoding()
		commandBuffer.commit()
		commandBuffer.waitUntilCompleted()
		
		return outputTexture
	}
}
