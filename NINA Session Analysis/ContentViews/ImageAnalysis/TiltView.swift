//
//  DetectedStarsView.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 11.08.24.
//
import SwiftUI
import AppKit





struct TiltView: View {
	@Binding var imageDataProvider: ImageAnalysisDataProvider
	@Binding var starAnalyzer: StarAnalyzeProvider
	@AppStorage("ShowStarBoxes") private var showStarBoxes: Bool = true
	@AppStorage("ShowTiltOctagon") private var showTiltOctagon: Bool = false
	@State private var processingImage: Bool = false
	
	
	private func project(_ rect: CGRect, _ sx: CGFloat, _ sy: CGFloat) -> CGRect {
		let origin = rect.origin
		let newOrigin = CGPoint(origin.x * sx, origin.y * sy)
		let size = CGSize(rect.size.width*sx, rect.size.height*sy)
		
		return CGRect(origin: newOrigin, size: size)
	}
	
	private func project(_ point: CGPoint, _ sx: CGFloat, _ sy: CGFloat) -> CGPoint {
		return CGPoint(point.x * sx, point.y * sy)
	}
	
	
	@ViewBuilder
	func showDetectedStars() -> some View {
		Canvas { context, size in
			
			let sx = size.width / CGFloat(starAnalyzer.imageWidth)
			let sy = size.height / CGFloat(starAnalyzer.imageHeight)
			
			var textRects = [CGRect]()
			
			for star in starAnalyzer.largestStars {
				
				let rect = project(star.boundingBox, sx, sy)
				
				let path = Path { path in
					path.move(to: rect.topLeading)
					path.addLine(to: rect.topTrailing)
					path.addLine(to: rect.bottomTrailing)
					path.addLine(to: rect.bottomLeading)
					path.addLine(to: rect.topLeading)
				}
				
				context.stroke(path, with: .color(Color.green), lineWidth: 0.5)
				
				let center = project(star.boundingBox.center, sx, sy)
				let text = context.resolve(Text("\(star.hfd, specifier: "%.2f")").font(.footnote).foregroundStyle(.green.opacity(0.5)))
				let textSize = text.measure(in: size)
				let textRect = CGRect.from(center: center.offset(0, rect.height/2 + textSize.height/2 + 2), size: textSize)
				if let _ = textRects.first(where: { $0.intersects(textRect)}) {
					continue
				}
				
				context.draw(text, in: textRect)
				textRects.append(textRect)
			}
		}
	}
	
	@ViewBuilder
	func showTilt() -> some View {
		Canvas { context, size in
			let sx = size.width / CGFloat(starAnalyzer.imageWidth)
			let sy = size.height / CGFloat(starAnalyzer.imageHeight)
			
			let points = starAnalyzer.getTileTiltPositions()
			if points.count == 9 {
				let path = Path { path in
					path.move(to: project(points[0].pos, sx, sy))
					path.addLine(to: project(points[1].pos, sx, sy))
					path.addLine(to: project(points[2].pos, sx, sy))
					path.addLine(to: project(points[3].pos, sx, sy))
					path.addLine(to: project(points[4].pos, sx, sy))
					path.addLine(to: project(points[5].pos, sx, sy))
					path.addLine(to: project(points[6].pos, sx, sy))
					path.addLine(to: project(points[7].pos, sx, sy))
					path.addLine(to: project(points[8].pos, sx, sy))
					path.addLine(to: project(points[3].pos, sx, sy))
					path.move(to: project(points[5].pos, sx, sy))
					path.addLine(to: project(points[0].pos, sx, sy))
					path.move(to: project(points[1].pos, sx, sy))
					path.addLine(to: project(points[4].pos, sx, sy))
					path.addLine(to: project(points[7].pos, sx, sy))
				}
				
				context.stroke(path, with: .color(Color.yellow), lineWidth: 1.5)
				
				context.addFilter(.shadow(radius: 2.0))
				
				for pt in points {
					let text1 = context.resolve( Text("\(pt.hfd, specifier: "%.2f")")
									   .font(.system(size: 16)).bold()
									   .foregroundStyle(Color.lightYellow))
					
					let t1Size = text1.measure(in: size)
					let offset = pt.vec.scaled(0.4 * t1Size.width/sx)
					context.draw(text1, in: CGRect.from(center: project(pt.pos.offset(offset), sx, sy), size: t1Size))
				}
				
			}
			
		}
	}
	
	@ViewBuilder
	var showImage: some View {
		Group {
			
			if let image =  self.imageDataProvider.stretchedImage?.cgImage {
				VStack {
					
					HStack {
						Toggle("Show Borders", isOn: $showStarBoxes)
							.padding()
						
						Toggle("Show Tilt Octagon", isOn: $showTiltOctagon)
							.padding()
						
						Spacer()
					}
					
					
					Image(decorative: image, scale: 1.0)
						.resizable()
						.aspectRatio(contentMode: .fit)
						.overlay {
							if showStarBoxes {
								showDetectedStars()
							}
							if showTiltOctagon {
								showTilt()
							}
						}
					
					HStack(spacing: 16) {
						
						Text("Detected Stars: \(self.starAnalyzer.stars.count)")
							.font(Font.title2)

						if let tiltHFD = self.starAnalyzer.tiltInspection?.tiltHfd,
						   let hfdBest = self.starAnalyzer.tiltInspection?.hfdBest,
						   let hfdWorst = self.starAnalyzer.tiltInspection?.hfdWorst,
						   let tiltPercent = self.starAnalyzer.tiltInspection?.tiltPercentage,
						   let tilt = self.starAnalyzer.tiltInspection?.tilt {
							Text("Tilt: \(tiltPercent, specifier: "%.1f")%")
								.font(Font.title2)
							Text(tilt.text).foregroundStyle(tilt.color)
								.font(Font.title2)
							Text("HFD: \(tiltHFD, specifier: "%.2f")px (\(hfdBest, specifier: "%.2f") - \(hfdWorst, specifier: "%.2f"))")
								.font(Font.title2)
						}
						
						if let offAxis = self.starAnalyzer.tiltInspection?.offAxis {
							Text("Off-Axis: \(offAxis, specifier: "%.2f")")
								.font(Font.title2)
							
							if abs(offAxis) > 0.5 {
								
								if abs(offAxis) > 1.0 {
									Text("Serious backfocus issue: adjust the distance between camera sensor and field flattener/coma corrector!")
										.font(Font.title2)
										.foregroundStyle(.red)
										.padding(.horizontal)
								} else {
									Text("Adjust the distance between camera sensor and field flattener/coma corrector.")
										.font(Font.title2)
										.foregroundStyle(.red)
										.padding(.horizontal)
								}
								
								
							}
							
						}
						
						Spacer()
					}
					.padding()
					
					Spacer()
				}
				
			}
		}
	}
	
	var body: some View {
		
		showImage
			.padding(.vertical, 32)
			.padding(.horizontal, 8)

	}
	
}
