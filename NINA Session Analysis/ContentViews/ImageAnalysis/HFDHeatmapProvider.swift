//
//  HFDPolygonProvider.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 12.08.24.
//

import SwiftUI
import CoreImage

@Observable
class HFDHeatmapProvider {
	var heatmap: CGImage? = nil
	
	func process(stars: [DetectedStar], in image: CGImage) async {
		let width = CGFloat(image.width)
		let height = CGFloat(image.height)
		
		
		let generator = HFDHeatmapGenerator(stars: stars, imageWidth: width, imageHeight: height, tileSize: 100)
		if let heatmap = generator.generateHeatmap() {
			await MainActor.run {
				self.heatmap = heatmap
			}
		}
		
		
	}
	
	
}
