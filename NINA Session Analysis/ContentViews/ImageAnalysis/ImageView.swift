//
//  ImageView.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 08.07.24.
//

import SwiftUI
import CoreGraphics
import CoreImage




struct ImageView: View {
	@Binding var imageDataProvider: ImageAnalysisDataProvider
	
	var body: some View {
		Group {
			if let img = self.imageDataProvider.stretchedImage?.cgImage {
				Image(nsImage: NSImage(cgImage: img, size: NSSize(width: img.width, height: img.height)))
					.resizable()
					.aspectRatio(contentMode: .fit)
			}
		}
		
	}
}
