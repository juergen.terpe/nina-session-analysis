//
//  EccentrictyContourPlotProvider.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 02.10.24.
//

import SwiftUI

struct EccPosition {
	let position: CGPoint
	let ecc: Float
}

@Observable
class EccentrictyContourPlotProvider {
	
	var grayscaleSteps: Int = 15
	var eccContour: [[Float]] = []
	var eccValues: [EccPosition] = []
	var minEcc: Float?
	var maxEcc: Float?
	var width: Int = 1
	var height: Int = 1
	var image: Image?
	
	
	private func getColor(x: Int, y: Int) -> Color {
		let c = round(Double(grayscaleSteps) * Double(eccContour[y][x]))/Double(grayscaleSteps)
		return Color(white: min(1.0, c))
	}
	
	private func group(stars: [DetectedStar]) -> [EccPosition] {
		var grouped = [EccPosition]()
		var usedIndices = Set<Int>()
	
		for i in 0..<stars.count {
			
			if usedIndices.contains(i) {
				continue
			}
			
			var closestDelta: Float = Float.greatestFiniteMagnitude
			var secondDelta: Float = Float.greatestFiniteMagnitude
			var closestIndex: Int? = nil
			var secondClosestIndex: Int? = nil
			
			let star1 = stars[i]
			
			
			for j in (i+1)..<stars.count {
				
				if usedIndices.contains(j) {
					continue
				}
				
				let star2 = stars[j]
				let dsqr = Float(pow(star1.position.x - star2.position.x, 2) + pow(star1.position.y - star2.position.y, 2))
				if dsqr < closestDelta {
					secondDelta = closestDelta
					closestDelta = dsqr
					secondClosestIndex = closestIndex
					closestIndex = j
				}
				else if dsqr < secondDelta {
					secondDelta = dsqr
					secondClosestIndex = j
				}
			}
			
			if let b = closestIndex, let c = secondClosestIndex, b != c {
				let star2 = stars[b]
				let star3 = stars[c]
				
				let mean = (star1.eccentricity + star2.eccentricity + star3.eccentricity)/3.0
				let x = (star1.position.x + star2.position.x + star3.position.x) / 3.0
				let y = (star1.position.y + star2.position.y + star3.position.y) / 3.0
				
				grouped.append(EccPosition(position: CGPoint(x: x, y: y), ecc: mean))
				usedIndices.insert(b)
				usedIndices.insert(c)
			}
		}
		
		return grouped.sorted(by: { $0.ecc > $1.ecc })
	}
	
	func updateImage(grayscales: Int) async {
		await MainActor.run {
			self.grayscaleSteps = grayscales
		}
		
		await self.renderImage()
	}
	
	func update(stars: [DetectedStar], width: Int, height: Int, grayscales: Int) async {
		
		if stars.isEmpty {
			await MainActor.run {
				self.image = nil
				self.width = 1
				self.height = 1
				self.eccContour = []
				self.eccValues = []
				self.grayscaleSteps = grayscales
				self.minEcc = nil
				self.maxEcc = nil
			}
		} else {
			
			print("EccContourPlotProvider.update begin...")
			
			let scaledown: Int = 1 + width / 1000
			let w = width / scaledown + 1
			let h = height / scaledown + 1
			
			let scaleX = Float(scaledown)
			let scaleY = Float(scaledown)
		
			let pixelsStar = Float(w*h)/Float(max(1,stars.count))
			let influencefactor: Float = 2.0 * Float(sqrt(pixelsStar))
			
			var hfdContour = Array(repeating: Array(repeating: Float(0.0), count: w), count: h)
			var minValue: Float = Float.greatestFiniteMagnitude
			var maxValue: Float = 0.0
			
			let sfx = 1.0 / CGFloat(scaleX)
			let sfy = 1.0 / CGFloat(scaleY)
			
			let groupedStars = group(stars: stars)
			
			for y in 0..<h {
				for x in 0..<w {
					var contourValue: Float = 0.0
					var influenceSum: Float = 0.0
					
					for star in groupedStars {
						let xs = star.position.x * sfx
						let ys = star.position.y * sfy
						let dist = Float(sqrt(pow(xs-CGFloat(x), 2.0) + pow(ys-CGFloat(y), 2.0)))
						let influence = influencefactor / (influencefactor + dist)
						influenceSum += influence
						contourValue += star.ecc * influence
					}
					
					contourValue /= influenceSum
					hfdContour[y][x] = contourValue
					
					minValue = min(minValue, contourValue)
					maxValue = max(maxValue, contourValue)
				}
			}
			
			print("e: min \(minValue) max \(maxValue)")
			
			let contours = rescale(hfdContour, w: w, h: h, minValue: minValue, maxValue: maxValue)
			
			let minEcc = minValue
			let maxEcc = maxValue
			
			await MainActor.run {
				self.width = w
				self.height = h
				self.eccContour = contours
				self.eccValues = groupedStars
				self.grayscaleSteps = grayscales
				self.minEcc = minEcc
				self.maxEcc = maxEcc
			}
			
			await self.renderImage()
			
			print("EccContourPlotProvider.update end...")
		}
	}
	
	private func rescale(_ hfdContour: [[Float]], w: Int, h: Int, minValue: Float, maxValue: Float) -> [[Float]] {
		var rescaled = Array(repeating: Array(repeating: Float(0.0), count: w), count: h)
		
		let delta = maxValue - minValue
		print("delta: \(delta) (\(minValue) - \(maxValue))")
		let factor = delta / 100.0
		print("factor: \(factor)")
		
		let scale = delta > 1.0 ? 1.0 / delta : 1.0
		for y in 0..<h {
			let row = hfdContour[y]
			
			for x in 0..<w {
				
				let value = round(1.0/factor * (row[x] - minValue)) * factor
				rescaled[y][x] = value * scale
			}
		}
		
		return rescaled
	}
	
	
	@MainActor private func renderImage() async {
		let width = self.width
		let height = self.height
		
		let image = Image(size: CGSize(width: width, height: height), label: Text("Image"), opaque: true) { context in
			for y in 0..<height {
				for x in 0..<width {
					
					let path = Path(ellipseIn: CGRect(origin: CGPoint(x: Double(x)-0.5, y: Double(y)-0.5), size: CGSize(width: 1, height: 1)))
					context.fill(path, with: .color(self.getColor(x: x, y: y)))
				}
			}
			
		}
		
		await MainActor.run {
			self.image = image
		}
		
	}
}
	
