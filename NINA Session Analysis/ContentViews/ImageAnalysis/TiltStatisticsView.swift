//
//  TiltStatisticsView.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 04.10.24.
//

import SwiftUI

struct TiltStatisticsView: View {
	@Binding var imageDataProvider: ImageAnalysisDataProvider
	@Binding var starAnalyzer: StarAnalyzeProvider
	@State private var statisticsMode: ImageTileDistributionChartMode = .hfr
	@State private var showTiledHFD: Bool = true
	@State private var showTiledEcc: Bool = false
	@State private var processingImage: Bool = false 
	
	@ViewBuilder
	func tiledGraphsView() -> some View {
		ZStack {
			
			Color.black
			
			if let image = self.imageDataProvider.stretchedImage?.cgImage {
				
				Image(decorative: image, scale: 1.0)
					.resizable()
					.aspectRatio(contentMode: .fill)
			}
			
			if let tiles = self.starAnalyzer.tiltInspection?.tiles {
				VStack(spacing: 0) {
					HStack {
						if let tile = tiles[.leftTop] {
							ImageTileDistributionChart(tile: tile, mode: statisticsMode)
								.padding()
								.background(.ultraThinMaterial.opacity(0.2))
								.cornerRadius(8)
						} else {
							Spacer()
						}
						Divider()
						if let tile = tiles[.middleTop] {
							ImageTileDistributionChart(tile: tile, mode: statisticsMode)
								.padding()
								.background(.ultraThinMaterial.opacity(0.2))
								.cornerRadius(8)
						} else {
							Spacer()
						}
						Divider()
						
						if let tile = tiles[.rightTop] {
							ImageTileDistributionChart(tile: tile, mode: statisticsMode)
								.padding()
								.background(.ultraThinMaterial.opacity(0.2))
								.cornerRadius(8)
						} else {
							Spacer()
						}
					}
					
					Divider()
					
					HStack {
						
						if let tile = tiles[.leftMiddle] {
							ImageTileDistributionChart(tile: tile, mode: statisticsMode)
								.padding()
								.background(.ultraThinMaterial.opacity(0.2))
								.cornerRadius(8)
						} else {
							Spacer()
						}
						Divider()
						
						if let tile = tiles[.middleMiddle] {
							ImageTileDistributionChart(tile: tile, mode: statisticsMode)
								.padding()
								.background(.ultraThinMaterial.opacity(0.2))
								.cornerRadius(8)
						} else {
							Spacer()
						}
						Divider()
						
						if let tile = tiles[.rightMiddle] {
							ImageTileDistributionChart(tile: tile, mode: statisticsMode)
								.padding()
								.background(.ultraThinMaterial.opacity(0.2))
								.cornerRadius(8)
						} else {
							Spacer()
						}
					}
					
					Divider()
					
					HStack {
						if let tile = tiles[.leftBottom] {
							ImageTileDistributionChart(tile: tile, mode: statisticsMode)
								.padding()
								.background(.ultraThinMaterial.opacity(0.2))
								.cornerRadius(8)
						} else {
							Spacer()
						}
						Divider()
						
						if let tile = tiles[.middleBottom] {
							ImageTileDistributionChart(tile: tile, mode: statisticsMode)
								.padding()
								.background(.ultraThinMaterial.opacity(0.2))
								.cornerRadius(8)
						} else {
							Spacer()
						}
						Divider()
						if let tile = tiles[.rightBottom] {
							ImageTileDistributionChart(tile: tile, mode: statisticsMode)
								.padding()
								.background(.ultraThinMaterial.opacity(0.2))
								.cornerRadius(8)
						} else {
							Spacer()
						}
					}
				}
			}
		}
	}
	
	
	var body: some View {
		VStack {
			HStack {
				Toggle(isOn: $showTiledHFD) {
					Text("HFR")
				}
				Toggle(isOn: $showTiledEcc) {
					Text("Eccentricity")
				}
			}
			.onChange(of: showTiledHFD) { _, _ in
				if showTiledHFD {
					self.statisticsMode = .hfr
					self.showTiledEcc = false
				}
				
			}
			.onChange(of: showTiledEcc)  { _, _ in
				if showTiledEcc {
					self.statisticsMode = .ecc
					self.showTiledHFD = false
				}
			}
			
			ZStack {
			
				Spacer()
				
				if self.statisticsMode == .hfr || self.statisticsMode == .ecc {
					tiledGraphsView()
				}
						
				
			}
		}
		
	}
}
