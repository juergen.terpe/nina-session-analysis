//
//  HintsView.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 01.05.24.
//

import SwiftUI

@Observable
class HintsDataModel {
	var hints: [String: [HintsData]] = [:]
	var ninaErrors: [HintsData] = []
	var imageCount: [String: Int] = [:]
	var filters: [String] = []
	var openHintId: String = ""
	
	func isOpen(_ hint: HintsData) -> Bool {
		return hint.id == openHintId
	}
	
	func update(session: SessionData, selected: String) async {
		if let selected = session.sessionDates.first(where: { $0.text == selected }) {
			
			let allImages = session.sessionImages.filter({ $0.SessionDate != nil && $0.SessionDate!.text == selected.text || selected.text == "Any" })
			let results = await session.createHints(selected)
			let normalResults = results.filter { $0.group != .nina }
			let ninaResults = results.filter { $0.group == .nina }
			
			let filters = normalResults.compactMap({ $0.filter }).distinct()
			var groupedHints = [String: [HintsData]]()
			
			for group in normalResults.groupBy(keyFunc: { $0.filter }) {
				groupedHints[group.key] = group.value
			}

			var imagesCounts = [String : Int]()
			for group in allImages.groupBy(keyFunc: { $0.filter }) {
				imagesCounts[group.key] = group.value.count
			}
						
			let resultingHints = groupedHints
			let imagesPerFilter = imagesCounts
			
			await MainActor.run {
				self.hints = resultingHints
				self.filters = filters
				self.imageCount = imagesPerFilter
				self.ninaErrors = ninaResults
			}
		} else {
			await MainActor.run {
				self.hints = [:]
				self.filters = []
				self.imageCount = [:]
				self.ninaErrors = []
			}
		}
	}
	
}

struct HintLine : View {
	@State var session: SessionData
	let hint: HintsData
	@State var hints: HintsDataModel
	
	@ViewBuilder
	func rowContextMenu( _ image: SessionImage) -> some View {
		if self.session.quickFitsAvailable {
			Button {
				self.session.openInQuickFits( image )
			} label: {
				Text("Open in QuickFits")
			}
		}
		
		if self.session.fitsPreviewAvailable {
			Button {
				self.session.openInFitsPreview( image )
			} label: {
				Text("Open in FITS Preview")
			}
		}
		
		if self.session.pixInsightAvailable {
			Button {
				self.session.openInPixInsight( image )
			} label: {
				Text("Open in PixInsight")
			}
		}
		
		if self.session.astapAvailable {
			Button {
				self.session.openInAstap( image )
			} label: {
				Text("Open in Astap")
			}
		}
	}
	
	
	
	@ViewBuilder
	func getLogsTable(_ logs: [NINALogLine]) -> some View {
		
		Grid(horizontalSpacing: 8, verticalSpacing: 4) {
		
			if logs.count > 100 {
				ForEach(logs.prefix(100), id: \.self.id) { log in
					GridRow(alignment: .firstTextBaseline) {
						Text(log.time.formatted(date: .omitted, time: .shortened))
							.fontWeight(.light)
							.monospacedDigit()
							.gridColumnAlignment(.leading)
						Text(log.lineText)
							.fontWeight(.medium)
							.multilineTextAlignment(.leading)
							.gridColumnAlignment(.leading)
					}
				}
				
				GridRow(alignment: .firstTextBaseline) {
					Text("...")
						.fontWeight(.light)
						.monospacedDigit()
						.foregroundStyle(Color.red)
						.gridColumnAlignment(.leading)
					Text("... \(logs.count)x")
						.font(.headline)
						.fontWeight(.bold)
						.foregroundStyle(Color.red)
						.gridColumnAlignment(.leading)
				}
				
				
			} else {
				ForEach(logs, id: \.self.id) { log in
					GridRow(alignment: .firstTextBaseline) {
						Text(log.time.formatted(date: .omitted, time: .shortened))
							.fontWeight(.light)
							.monospacedDigit()
							.gridColumnAlignment(.leading)
						Text(log.lineText)
							.fontWeight(.medium)
							.multilineTextAlignment(.leading)
							.gridColumnAlignment(.leading)
					}
				}
			}
			
			
		}
		.padding(.bottom)
	}
	
	@ViewBuilder
	func getImagesTable(_ images: [SessionImage]) -> some View {
		LazyVStack(alignment: .leading, spacing: 0) {
			
			HStack(spacing: 10) {
				Text("Bad").frame(width: 50, alignment: .center)
				Text("Index").frame(width: 40, alignment: .center)
				Text("Filename").frame(width: 400, alignment: .center)
				Text("HFR").frame(width: 60, alignment: .center)
				Text("Eccentricity").frame(width: 100, alignment: .center)
				Text("Mean").frame(width: 60, alignment: .center)
				Text("Median").frame(width: 60, alignment: .center)
				Text("Airmass").frame(width: 60, alignment: .center)
				Text("Sun Altitude").frame(width: 100, alignment: .center)
				Text("RMS").frame(width: 60, alignment: .center)
			}
			.padding(4)
			.background(Color.cyan.opacity(0.2).gradient)
			
			HLine().foregroundColor(Color.gray.opacity(0.3))
			
			ForEach(Array(zip(images.indices, images)), id: \.self.0) { index, image in
				HStack(alignment: .firstTextBaseline, spacing: 10) {
					Image(systemName: image.bad ? "checkmark.square" : "square")
						.imageScale(.large)
						.foregroundColor(image.bad ? .badColor : .green)
						.frame(width: 50, alignment: .center)
						.contentShape(Rectangle())
						.onTapGesture {
							image.toggleMarked()
							self.session.onContentUpdated()
						}
					
					Text(image.Index).frame(width: 40, alignment: .trailing)
					Text(image.FileName).truncationMode(.middle).lineLimit(1).frame(width: 400, alignment: .leading)
					Text(image.HFR).frame(width: 60, alignment: .trailing)
					Text(image.Eccentricity).frame(width: 100, alignment: .trailing)
					Text(image.Mean).frame(width: 60, alignment: .trailing)
					Text(image.Median).frame(width: 60, alignment: .trailing)
					Text(image.Airmass).frame(width: 60, alignment: .trailing)
					Text(image.SunAltitude).frame(width: 100, alignment: .trailing)
					Text(image.RMS).frame(width: 60, alignment: .trailing)
				}
				.padding(4)
				.background(index % 2 == 1 ? Color.blue.opacity(0.1) : Color.clear)
				.contentShape(Rectangle())
				.contextMenu {
					rowContextMenu(image)
				}
			}
			
		}
		.id(self.session.id)
	}
	
	
	var body: some View {
		VStack(alignment: .leading) {
			HStack {
				hint.icon.frame(width: 60)
					.padding(.trailing)
				
				VStack(alignment: .leading) {
					
					if hint.group == .nina {
						HStack(spacing: 0) {
							Text(hint.id)
								.font(.headline)
							Text(":  ")
								.font(.headline).padding(.trailing, 12)
							Text(hint.text)
								.font(.subheadline)
								.foregroundStyle(Color.red)
						}
						.frame(maxWidth: .infinity, alignment: .leading)
					} else {
						HStack(spacing: 8) {
							Text(hint.text)
								.font(.headline)
							 Text(" [Filter: \(hint.filter) Exposure: \(hint.exposure)]")
								.font(.subheadline)
						}
						.frame(maxWidth: .infinity, alignment: .leading)
					}
				}
				.frame(maxWidth: .infinity)
				
				if hint.images != nil {
					Image(systemName: "chevron.right")
						.imageScale(.large)
						.rotationEffect( self.hints.isOpen(hint) ? SwiftUI.Angle(degrees: 90.0) : SwiftUI.Angle(degrees: 0.0))
				} else if hint.logs != nil {
					Image(systemName: "chevron.right")
						.imageScale(.large)
						.rotationEffect( self.hints.isOpen(hint) ? SwiftUI.Angle(degrees: 90.0) : SwiftUI.Angle(degrees: 0.0))
				}
			}
			.padding()
			.contentShape(.rect)
			.onTapGesture {
				withAnimation(.easeInOut(duration: 0.3)) {
					if self.hints.openHintId == hint.id {
						self.hints.openHintId = ""
					} else {
						self.hints.openHintId = hint.id
					}
				}
			}
			
			if self.hints.isOpen(hint) {
				
				if let images = hint.images {
					
					HStack {
						Color.clear.frame(width: 100)
						
						getImagesTable(images)
					}.transition(.move(edge: .trailing))
				} else if let logs = hint.logs {
					HStack {
						Color.clear.frame(width: 100)
						
						getLogsTable(logs)
					}.transition(logs.count < 20 ? .move(edge: .trailing) : AnyTransition.identity)
				}
				
			}
			
		}
	}
}


struct HintsView: View {
	@State var session: SessionData
	@Binding var showSettings: Bool
	@State private var selectedSessionDate: String = "Any"
	@State private var hints: HintsDataModel = HintsDataModel()
	@AppStorage("ShowHintsForImages") private var imaging: Bool = true
	@AppStorage("ShowHintsForRotator") private var rotation: Bool = true
	@AppStorage("ShowHintsForGuiding") private var guiding: Bool = true
	
	func getHints(_ filter: String) -> [HintsData] {
		var allHints = self.hints.hints[filter] ?? []
		
		if imaging && rotation && guiding {
			return allHints
		}
		if !imaging {
			allHints = allHints.filter({ $0.group != .imaging })
		}
		if !imaging {
			allHints = allHints.filter({ $0.group != .rotator })
		}
		if !imaging {
			allHints = allHints.filter({ $0.group != .guiding })
		}
		
		return allHints
	}
	
	func getImageCount(_ filter: String) -> Int {
		if let count = self.hints.imageCount[filter] {
			return count
		}
		return 0
	}
	
    var body: some View {
		
		VStack {
			
			HStack {
				Toggle(isOn: $imaging) {
					Text("Images")
				}
				.toggleStyle(.checkbox)
				.padding()
				
				Toggle(isOn: $rotation) {
					Text("Rotator")
				}
				.toggleStyle(.checkbox)
				.padding()
				
				Toggle(isOn: $guiding) {
					Text("Guiding")
				}
				.toggleStyle(.checkbox)
				.padding()
				
				Spacer()
			}
			
			TabView(selection: $selectedSessionDate) {
				
				ForEach(session.sessionDates.map({ $0.text }), id: \.self) { sessionDate in
					
					ScrollView(.vertical) {
						ForEach(self.hints.filters, id: \.self) { filter in
							Section {
								ForEach(getHints(filter)) { hint in
									HintLine(session: self.session, hint: hint, hints: self.hints)
										.background(Color.blue.opacity(0.05).gradient)
										.cornerRadius(8)
								}
							} header: {
								HStack {
									Text("Filter: \(filter) - \(getImageCount(filter)) lights")
										.kerning(2.5)
										.font(.title2)
										.fontWeight(.semibold)
										.foregroundColor(.lightBlue)
										.padding()
									
									Spacer()
								}
								.background(Color.darkBlue.gradient.opacity(0.3))
								.cornerRadius(8)
							}
								
							
						}
						
						Section {
							ForEach(self.hints.ninaErrors) { hint in
								HintLine(session: self.session, hint: hint, hints: self.hints)
									.background(Color.blue.opacity(0.05).gradient)
									.cornerRadius(8)
							}
						} header: {
							HStack {
								Text("NINA: Errors and Warnings")
									.kerning(2.5)
									.font(.title2)
									.fontWeight(.semibold)
									.foregroundColor(.lightBlue)
									.padding()
								
								Spacer()
							}
							.background(Color.darkBlue.gradient.opacity(0.3))
							.cornerRadius(8)
						}
					
					}
					.task {
						await self.hints.update(session: self.session, selected: selectedSessionDate)
					}
					.onChange(of: session.id) { _, _ in
						Task {
							await self.hints.update(session: self.session, selected: selectedSessionDate)
						}
					}
					.onChange(of: selectedSessionDate) { _, _ in
						Task {
							await self.hints.update(session: self.session, selected: selectedSessionDate)
						}
					}
					.tabItem {
						Text(sessionDate)
					}
					.tag(sessionDate)
				}
			}
			.tabViewStyle(DefaultTabViewStyle())
			.padding()
		}
		.background(Color.controlBackgroundColor)
		.onAppear(perform: {
			self.selectedSessionDate = session.selectedSessionDate
			Task {
				await self.hints.update(session: self.session, selected: selectedSessionDate)
			}
		})
		.overlay {
			
			VStack {
				HStack {
					Spacer()
					if showSettings {
						AnalysationSettingsView(session: session)
							.transition(.move(edge: .top))
					}
				}
				Spacer()
			}
		}
    }
}

