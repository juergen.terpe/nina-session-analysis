//
//  InternalImageView.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 14.02.25.
//

import SwiftUI

@Observable
class InternalImageViewModel {
	var image: AstroImage?
	
	func update(from image: SessionImage) async {
		
		if let astroImage = await image.getImageData() {
			
			let (stretched, debayered) = astroImage.debayerAndStretch()
			await MainActor.run {
				
				if let stretched {
					self.image = stretched
				} else if let debayered {
					self.image = debayered
				} else {
					self.image = nil
				}
			}
		}
	}
	
	
}

/*
struct InternalImageView: View {
	@Binding var imageId: SessionImage.ID?
	@Environment(SessionData.self) private var session
	@State private var model = InternalImageViewModel()
	@State private var scale: CGFloat = 1.0
	
	var body: some View {
		ZStack {
			
			Color.clear
			
			if let image =  self.model.image?.cgImage {
				VStack {
					
					HStack {
						Text("Scale:")
						Slider(value: $scale, in: 0.1 ... 2)
						Spacer()
					}
					.padding()
					
					ScrollView([.horizontal, .vertical]) {
						Image(decorative: image, scale: 1.0/scale)
							.resizable()
							.aspectRatio(contentMode: .fit)
					}
					
				}
			}
		}
		.task {
			
			if let image = self.session.sessionImages.first(where: { $0.id == self.imageId}) {
				await model.update(from: image)
				
				if let image = model.image {
					let scaleFactor: CGFloat = 920.0 / CGFloat(image.height)
					await MainActor.run {
						self.scale = scaleFactor
					}
				}
				
			}
			
		}
    }
}
*/

import SwiftUI

struct InternalImageView: View {
	@Binding var imageId: SessionImage.ID?
	@Environment(SessionData.self) private var session
	@State private var model = InternalImageViewModel()
	@State private var zoom: CGFloat = 1.0
	let minZoom = 0.8
	let maxZoom = 10.0
	
	var body: some View {
		VStack {
			GeometryReader { geometry in
				VStack {
					HStack {
						
						Text("Scale:")
						Slider(value: $zoom, in: minZoom ... maxZoom)
							.frame(width: 400)
							.padding(.trailing)
						
						Button {
							zoomOut()
						} label: {
							Image(systemName: "minus.magnifyingglass")
								.imageScale(.medium)
						}
						.buttonStyle(.plain)
						.buttonRepeatBehavior(.enabled)
						.padding(.horizontal)

						Button {
							resetZoom()
						} label: {
							Image(systemName: "inset.filled.center.rectangle")
								.imageScale(.medium)
						}
						.buttonStyle(.plain)
						.padding(.horizontal)
						
						Button {
							zoomIn()
						} label: {
							Image(systemName: "plus.magnifyingglass")
								.imageScale(.medium)
						}
						.buttonStyle(.plain)
						.buttonRepeatBehavior(.enabled)
						.padding(.horizontal)
						
						
						Spacer()
					}
					.padding()
					
					
					ScrollView([.horizontal, .vertical]) {
						ZStack {
							if let image = model.image?.cgImage {
								Image(decorative: image, scale: 1.0)
									.resizable()
									.aspectRatio(contentMode: .fit)
									.frame(width: geometry.size.width * zoom, height: geometry.size.height * zoom)
							} else {
								ProgressView()
							}
						}
						.frame(maxWidth: .infinity, maxHeight: .infinity)
					}
					.clipped()
					
				
				}
			}
			
		}
		.task { await loadImage() }
		.onChange(of: imageId, { _, _ in
			Task {
				await loadImage()
			}
		})
	}
	
	private func zoomIn() {
		if zoom < maxZoom {
			zoom *= 1.2
		}
	}
	
	private func zoomOut() {
		if zoom > minZoom {
			zoom /= 1.2
		}
		
	}
	
	private func resetZoom() {
		zoom = 1.0
	}
	
	private func loadImage() async {
		if let image = self.session.sessionImages.first(where: { $0.id == self.imageId}) {
			await model.update(from: image)
		}
	}
}



