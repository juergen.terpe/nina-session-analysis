import SwiftUI

struct SurveyViewRegion {
	var ra: Double
	var dec: Double
	var size: Double
	var target: String
	var fov: Double
}



struct DeepSkySurveyView: View {

    var region: SurveyViewRegion
	var size: CGSize
    @State var sourceType = SkySurvaySource.hips2Fits
    @StateObject var imageDataSource: SkySurveyImageDownloader = SkySurveyImageDownloader()
   
    var body: some View {
        
		ZStack {
			
			Group {
				
				if (imageDataSource.surveyImage != nil) {
					Image(imageDataSource.surveyImage!, scale: 0.6, label: Text(region.target))
				} else {
					ProgressView()
				}
			}
			.onAppear {
				let imageSize = CGSize(width: size.width, height: size.height)
				
				self.imageDataSource.request(survey: self.sourceType,
											 ra: region.ra,
											 dec: region.dec,
											 fov: region.fov,
											 size: imageSize)
			}
			.onChange(of: $sourceType.wrappedValue) { _, type in
				
				
				
				let imageSize = CGSize(width: size.width, height: size.height)
				
				self.imageDataSource.request(survey: self.sourceType,
											 ra: region.ra,
											 dec: region.dec,
											 fov: region.fov,
											 size: imageSize)
			}
			
			
		}
        .clipped()
        
        
        
    }
}

