//
//  DitheringRectangle.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 30.03.24.
//

import SwiftUI


fileprivate struct DitherShape: Shape {
	var center: CGPoint
	var sx: CGFloat
	var sy: CGFloat
	var rotation: Double
	
	
	func path(in rect: CGRect) -> Path {

		let c = rect.center
		let cp = CGPoint(x: c.x + center.x, y: c.y + center.y)
		
		let move1 = CGAffineTransform.translation(-cp.x, -cp.y)
		let rot = CGAffineTransform.rotation(SwiftUI.Angle(degrees: rotation))
		let move2 = CGAffineTransform.translation(cp.x, cp.y)
		
		let transform = move1.concatenating(rot).concatenating(move2)
		
		var path = Path()
		path.addRect(CGRect.from(center: cp, size: CGSize(width: sx, height: sy)), 
					 transform: transform)
		
		return path
	}
}

struct DitheringRectangle: View {
	
	var region: SurveyViewRegion
	var size: CGSize
	var image: SessionImage
	var images: [SessionImage]
	@Binding var showTargetRotation: Bool
	@Binding var showAll: Bool
	@State var sourceType = SkySurvaySource.hips2Fits
	@StateObject var imageDataSource: SkySurveyImageDownloader = SkySurveyImageDownloader()
    
	func drawRectangle(image: SessionImage, size: CGSize) -> some View {
		if let imgRect = image.getImageRectangle(useTargetRotation: showTargetRotation) {
			let imageCenter = Polar(ra: region.ra * Constants.Rad, dec: region.dec * Constants.Rad)
			let fov = region.fov
			let minSize = min(size.width, size.height)
			
			let scale = 2.0 * minSize / fov
			
			let dphi = (imageCenter.phi - imgRect.center.phi) * Constants.Deg
			let dtheta = (imageCenter.theta - imgRect.center.theta) * Constants.Deg
			let cpt = CGPoint(x: dphi * scale,
							  y: dtheta * scale)
			
			let sx = imgRect.sx * scale
			let sy = imgRect.sy * scale
						
			return AnyView(DitherShape(center: cpt, sx: sx, sy: sy, rotation: imgRect.rotation)
				.stroke(Color.gray.opacity(0.15), lineWidth: 0.5))
		}
		return AnyView(Color.clear)
	}
	
	
	func drawRectangle(size: CGSize) -> some View {
		if let imgRect = image.getImageRectangle(useTargetRotation: showTargetRotation) {
			let imageCenter = Polar(ra: region.ra * Constants.Rad, dec: region.dec * Constants.Rad)
			let fov = region.fov
			let minSize = min(size.width, size.height)
			
			let scale = 2.0 * minSize / fov
			
			let dphi = (imageCenter.phi - imgRect.center.phi) * Constants.Deg
			let dtheta = (imageCenter.theta - imgRect.center.theta) * Constants.Deg
			let cpt = CGPoint(x: dphi * scale,
							  y: dtheta * scale)
			
			let sx = imgRect.sx * scale
			let sy = imgRect.sy * scale
						
			return AnyView(DitherShape(center: cpt, sx: sx, sy: sy, rotation: imgRect.rotation)
				.stroke(Color.yellow, lineWidth: 1.0))
		}
		return AnyView(Color.clear)
	}
	
    var body: some View {
		
		ZStack {
			
			if (self.imageDataSource.surveyImage != nil) {
				Image(decorative: self.imageDataSource.surveyImage!, scale: 1.0/NSScreen.main!.backingScaleFactor)
					.resizable(resizingMode: .stretch)
					.aspectRatio(contentMode: 	.fill)
					
				GeometryReader { g in
					ZStack {
						
						if showAll {
							
							ForEach(images, id: \.self.id) { image in
								drawRectangle(image: image, size: g.size)
							}
						}
						
						drawRectangle(size: g.size)
					}
				}
					
			}
		}
		.onAppear {
			let imageSize = CGSize(width: size.width, height: size.height)
			
			self.imageDataSource.request(survey: self.sourceType,
										 ra: region.ra,
										 dec: region.dec,
										 fov: region.fov,
										 size: imageSize)
		}
    }
}

