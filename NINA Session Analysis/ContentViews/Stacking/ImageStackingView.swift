//
//  ImageStackingView.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 01.03.25.
//
import SwiftUI

enum ImageStackingProcessingState {
	case none
	case stacking
	case finished
}

struct ImageStackingView: View {
	@State var session: SessionData
	@State private var processingState: ImageStackingProcessingState = .none
	
	var body: some View {
		
		VStack {
		   
			HStack {
				Button {
					self.processingState = .stacking
				} label: {
					Text("Stack").padding()
				}
				.padding()
				
				Spacer()
			}
				   
		    Spacer()
	    }
		.onChange(of: self.processingState) { _, _ in
			Task {
				let processor = ImageProcessingUnit(session: self.session)
				await processor.process()
			}
		}
	}
}
