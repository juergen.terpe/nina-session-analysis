//
//  MoonPhaseView.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 31.03.24.
//

import SwiftUI

struct MoonPhaseShape: Shape {
	var elongation: Float = 0.0
	
	private func clamp(min: CGFloat, value: CGFloat, max: CGFloat) -> CGFloat {
		if value < min {
			return min
		} else if value > max {
			return max
		}
		return value
	}
	
	func getCrescent(at center: CGPoint, radius: CGFloat, phase: CGFloat, angle: CGFloat) -> Path? {
		
		if phase > 0.0 && phase <= 0.5 {
			let phaseAngle = CGFloat.pi * clamp(min: 0.00001, value: phase, max: 0.499999)
			let (sinA, cosA) = sincos(phaseAngle)
			
			var path = Path()
			path.addArc(center: center, radius: radius+0.5, startAngle: SwiftUI.Angle(radians: -CGFloat.pi/2.0), endAngle: SwiftUI.Angle(radians: CGFloat.pi/2.0), clockwise: true)
			path.addArc(center: CGPoint(x: center.x + radius * sinA/cosA, y: center.y),
						radius: radius/cosA,
						startAngle: SwiftUI.Angle(radians: CGFloat.pi/2.0 - phaseAngle),
						endAngle: SwiftUI.Angle(radians: phaseAngle - CGFloat.pi/2.0),
						clockwise: false)
			
			path.closeSubpath()
			
			let transform = CGAffineTransform.identity
			let transform2 = transform.translatedBy(x: center.x, y: center.y)
			let transform3 = transform2.rotated(by: angle)
			let transform4 = transform3.translatedBy(x: -center.x, y: -center.y)
			
			return path.applying(transform4)
			
		} else if phase > 0.5 && phase <= 1.0 {
			let phaseAngle = CGFloat.pi * clamp(min: 0.00001, value: 1.0 - phase, max: 0.499999)
			let (sinA, cosA) = sincos(phaseAngle)
			
			var path = Path()
			path.addArc(center: center, radius: radius+0.5,
						startAngle: SwiftUI.Angle(radians: -CGFloat.pi/2.0),
						endAngle: SwiftUI.Angle(radians: CGFloat.pi/2.0),
						clockwise: true)
			path.addArc(center: CGPoint(x: center.x - radius * sinA/cosA + 0.5, y: center.y),
						radius: radius/cosA,
						startAngle: SwiftUI.Angle(radians: CGFloat.pi/2.0 + phaseAngle),
						endAngle: SwiftUI.Angle(radians: 3.0*CGFloat.pi/2.0 - phaseAngle),
						clockwise: true)
			
			
			path.closeSubpath()
			
			let transform = CGAffineTransform.identity
			let transform2 = transform.translatedBy(x: center.x, y: center.y)
			let transform3 = transform2.rotated(by: angle)
			let transform4 = transform3.translatedBy(x: -center.x, y: -center.y)
			
			return path.applying(transform4)
		}
		
		return nil
	}
	
	
	func path(in rect: CGRect) -> Path {
		if elongation < 1.0 || elongation > 359.0 {
			var path = Path()
			path.addEllipse(in: rect)
			
			return path
		} else if elongation >= 179.0 && elongation < 181.0 {
			return Path()
		} else if elongation <= 179.0 {
			let phase = CGFloat(1.0 - 0.5 * (1.0 - cos(Double(elongation) * Double.pi/180.0)))
			let radius = min(rect.size.width, rect.size.height)/2.0
			
			if let crescent = getCrescent(at: rect.center, radius: radius,
										  phase: phase, angle: 0.0) {
				return crescent
			}
		}
		else {
			let phase = CGFloat(1.0 - 0.5 * (1.0 - cos(Double(elongation) * Double.pi/180.0)))
			let radius = min(rect.size.width, rect.size.height)/2.0
			
			if let crescent = getCrescent(at: rect.center, radius: radius,
										  phase: phase, angle: CGFloat.pi) {
				return crescent
			}
		}
		
		return Path()
	}
}

@Observable
class MoonphaseViewModel {
	var elongation: Float = 0.0
	
	func update(jd: JulianDate) async {
		let elong = getMoonElongation(jd)
		await MainActor.run {
			self.elongation = elong
		}
	}
	
	private func getMoonElongation(_ jd: JulianDate) -> Float {
		let discAngle = Moon.getIlluminatedDiscAngle(jd)
		let elongation = Float(Moon.getElongation(jd))
		
		if discAngle < 90.0 {
			return 360.0 - elongation
		} else {
			return elongation
		}
	}
}


struct MoonPhaseView: View {
	let jd: JulianDate
	@State private var model = MoonphaseViewModel()
	
    var body: some View {
		
		Image("MoonImage")
			.resizable(resizingMode: .stretch)
			.aspectRatio(1.0, contentMode: .fit)
			.overlay {
				MoonPhaseShape(elongation: model.elongation)
					.fill(Color.black.opacity(0.85))
					.blur(radius: 1.5, opaque: false)
					//.padding(1.0)
			}
			.clipCircle()
			.task {
				await model.update(jd: jd)
			}
			
    }
}

struct MoonPhaseView2: View {
	@Binding var model: MoonphaseViewModel
	
	var body: some View {
		
		Image("MoonImage")
			.resizable(resizingMode: .stretch)
			.aspectRatio(1.0, contentMode: .fit)
			.overlay {
				MoonPhaseShape(elongation: model.elongation)
					.fill(Color.black.opacity(0.85))
					.blur(radius: 1.5, opaque: false)
					//.padding(1.0)
			}
			.clipCircle()
	}
}

