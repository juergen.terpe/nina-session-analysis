//
//  SkySphereView.swift
//  Sky Calendar
//
//  Created by Jürgen Terpe on 03.05.20.
//  Copyright © 2020 Jürgen Terpe. All rights reserved.
//
//

import SwiftUI
import simd
import CoreLocation
import PureSwiftUI
import Kroma


fileprivate extension Double4 {
	var xyz: Double3 {
		return Double3(self.x, self.y, self.z)
	}
}



struct SkySphereViewProjection: Sendable {
	private let projection: double4x4
	private let width2: Double
	private let height2: Double
	private let center: CGPoint
	private let radius: Double
	private let fov: Double
	private let near: Double
	private let far: Double
	private let jdd: JulianDate
	private let location: CLLocation
	private let zeroPlane: Double = 1.0
	
	fileprivate init(size: CGSize,
					 center: CGPoint,
					 jdd: JulianDate,
					 location: CLLocation) {
		self.fov = Double.pi/2.0
		self.near = 0.001
		self.far = 3.0
		self.center = center
		self.jdd = jdd
		self.location = location
		
		let aspect = Double(size.width/size.height)
		
		self.width2 = Double(size.width / 2)
		self.height2 = Double(size.height / 2)
		self.radius = min(self.width2, self.height2)
		
		let a = tan(fov / 2.0)
		let sy = 1.0 / a
		let sx = aspect * sy
		
		let z2 = -(far + near) / (far - near) // remap z to [0, 1]
		let w2 = 2.0 * far * near / (far - near) // remap z to [0, 1]
		
		let proj = double4x4(rows: [SIMD4<Double>( sx, 0.0, 0.0, 0.0),
									SIMD4<Double>(0.0,  sy, 0.0, 0.0),
									SIMD4<Double>(0.0, 0.0,  z2, -1.0),
									SIMD4<Double>(0.0, 0.0,  w2, 1.0)])

		let rotationMatrix = SkySphereViewProjection.createRotationMatrix(jdd, location)
		let scaleMatrix = SkySphereViewProjection.createScaleMatrix(radius)
		let viewMatrix = double4x4(rows: [SIMD4<Double>(1.0, 0.0, 0.0, 0.0),
										  SIMD4<Double>(0.0, 1.0, 0.0, 0.0),
										  SIMD4<Double>(0.0, 0.0, 1.0,  1.0),
										  SIMD4<Double>(0.0, 0.0, 1.0, 1.0)])
		
		let flip = double4x4(rows: [SIMD4<Double>(1.0, 0.0, 0.0, 0.0),
									SIMD4<Double>(0.0, 1.0, 0.0, 0.0),
									SIMD4<Double>(0.0, 0.0,-1.0, 0.0),
									SIMD4<Double>(0.0, 0.0, 0.0, 1.0)])
		
		let reverse = double4x4(rows: [SIMD4<Double>(1.0, 0.0, 0.0, 0.0),
									   SIMD4<Double>(0.0, 1.0, 0.0, 0.0),
									   SIMD4<Double>(0.0, 0.0, 1.0, 0.0),
									   SIMD4<Double>(0.0, 0.0, 0.0, 1.0)])
		
		self.projection = scaleMatrix * reverse * proj * flip * viewMatrix * rotationMatrix
	}
	
	public var viewCenter: CGPoint {
		return self.center
	}
	
	public var centerPoint: Vector3 {
		let phi = -self.jdd.toLmst(self.location.coordinate.longitude * Constants.Rad)
		let theta = self.location.coordinate.latitude * Constants.Rad
		let polar = Polar(ra: phi, dec: theta)
		return polar.toVector()
	}
	
	fileprivate static func createRotationMatrix(_ jdd: JulianDate, _ location: CLLocation) -> double4x4 {
		let phi = -jdd.toLmst(location.coordinate.longitude * Constants.Rad)
		let zenitDec = location.coordinate.latitude * Constants.Rad
		let alpha = -(Double.pi/2.0 - zenitDec)
		
		let (sinPhi,cosPhi) = sincos(phi)
		let (sinAlpha,cosAlpha) = sincos(alpha)
		
		return double4x4(rows: [Double4(sinPhi, cosPhi, 0.0, 0.0),
								Double4(cosPhi*cosAlpha, -sinPhi*cosAlpha, sinAlpha, 0.0),
								Double4(-cosPhi*sinAlpha, sinPhi*sinAlpha, cosAlpha, 0.0),
								Double4(0.0, 0.0, 0.0, 1.0)])
	}
	
	fileprivate static func createScaleMatrix(_ radius: Double) -> double4x4 {
		let sx = -radius
		let sy = radius

		return double4x4(rows: [Double4(sx, 0.0, 0.0, 0.0),
								Double4(0.0, sy, 0.0, 0.0),
								Double4(0.0, 0.0, 1.0, 0.0),
								Double4(0.0, 0.0, 0.0, 1.0)])
	}
	
	
	public func project(pos: Vector3) -> CGPoint? {
		
		return project(v: SIMD4<Double>(pos.X, pos.Y, pos.Z, 1.0))
	}
	
	public func project(from: Vector3, to: Vector3) -> (CGPoint, CGPoint)? {
		let v1 = SIMD4<Double>(from.X, from.Y, from.Z, 1.0)
		let v2 = SIMD4<Double>(to.X, to.Y, to.Z, 1.0)
		
		let p1 = self.projection * v1
		let pos1 = p1.xyz / p1.w
		
		let p2 = self.projection * v2
		let pos2 = p2.xyz / p2.w
		
		
		if (abs(pos1.x) > self.radius || abs(pos1.y) > self.radius)
			&& (abs(pos2.x) > self.radius || abs(pos2.y) > self.radius) {
			return nil
		}
		
		let pt1 = CGPoint(x: pos1.x + self.center.x,
						  y: pos1.y + self.center.y)
		let pt2 = CGPoint(x: pos2.x + self.center.x,
						  y: pos2.y + self.center.y)
		return (pt1, pt2)
	}
	
	
	fileprivate func project(v: SIMD4<Double>) -> CGPoint? {
		let p = self.projection * v
		let pos = p.xyz / p.w
		
		if abs(pos.x) > self.radius || abs(pos.y) > self.radius {
			return nil
		}
		
		return CGPoint(x: pos.x + self.center.x, y: pos.y + self.center.y)
	}
}

fileprivate struct StarPositionMag: Identifiable, Sendable {
	var id: Int32
	let pos: Vector3
	let mag: Float
	let name: String?
}

fileprivate struct ConstLine: Identifiable {
	var id: Int32
	var constellationId: UInt8
	public var from: Vector3
	public var to: Vector3
}

fileprivate struct StarsShape: Shape {
	let jdd: JulianDate
	let location: CLLocation
	let stars: [StarPositionMag]
	let maxStarMag: Float
	let projection: SkySphereViewProjection
	
	private func project(star: StarPositionMag, projection: SkySphereViewProjection) -> CGRect? {
		if let p = projection.project(pos: star.pos) {
			let radius = CGFloat(0.3 * pow(1.0 + maxStarMag - star.mag, 1.3))
			return CGRect.from(center: p, radius: radius)
		}
		
		return nil
	}
	
	func path(in rect: CGRect) -> Path {
		var path = Path()
		
		for star in stars {
			if let starCircle = project(star: star, projection: projection) {
				path.addEllipse(in: starCircle)
			}
		}
		
		return path
	}
}

fileprivate struct MilkywayShape: Shape {
	let region: MilkywayDataRegion
	let projection: SkySphereViewProjection
		
	func path(in rect: CGRect) -> Path {
		return Path { path in
			for pos in region.positions {
				if let pp = projection.project(pos: pos) {
					path.addEllipse(in: CGRect.from(center: pp, radius: 1.5))
				}
			}
		}
	}
}

fileprivate struct ConstellationsShape: Shape {
	let jdd: JulianDate
	let location: CLLocation
	let lines: [ConstLine]
	let projection: SkySphereViewProjection
	
	private func project(line: ConstLine) -> (CGPoint, CGPoint)? {
		
		if let pp = projection.project(from: line.from, to: line.to) {
			return pp
		}
		return nil
	}
	
	func path(in rect: CGRect) -> Path {
		
		var path = Path()
		for line in lines {
			if let li = project(line: line) {
				path.move(to: li.0)
				path.addLine(to: li.1)
			}
		}
		return path
	}
}


final public class SkySphereViewDataSource: ObservableObject {
	
	private var maxMag: Double = 6.0
	private var isDirty: Bool = false
	
	@Published fileprivate var stars: [StarPositionMag] = []
	@Published fileprivate var lines: [ConstLine] = []
	@Published var location: CLLocation
	@Published var jdd: JulianDate
	
	lazy var milkywayColors = {
		var colors = [Int : Color]()
		for intensity in stride(from: 0.01, to: 1.0, by: 0.01) {
			let c = Color.milkyway.lighter(by: 0.3*intensity)
			colors[Int(intensity*100)] = c
		}
		return colors
	}()
		
	public init() {
		self.location = CLLocation(latitude: 51.5, longitude: 10.0)
		self.jdd = JulianDate.now()
		self.maxMag = 5.5
		self.isDirty = true
		
	}
	
	public init(location: CLLocation, jdd: JulianDate, maxMagnitude: Double = 6.5) {
		self.location = location
		self.jdd = jdd
		self.maxMag = maxMagnitude
		self.isDirty = true
	}
	
	public func setup(location: CLLocation, jdd: JulianDate, maxMagnitude: Double = 6.5) {
		
		let dLong = abs(location.coordinate.longitude - self.location.coordinate.longitude) * 100.0
		let dLat = abs(location.coordinate.latitude - self.location.coordinate.latitude) * 100.0
		let dTime = abs(self.jdd - jdd) * 86400.0

		self.isDirty = dLong > 1.0 || dLat > 1.0 || dTime > 1.0 || self.stars.count == 0
		self.location = location
		self.jdd = jdd
		self.maxMag = maxMagnitude
		
		update()
	}
	
	public var julianDate: JulianDate {
		return self.jdd
	}
	
	public var maxMagnitude: Double {
		return self.maxMag
	}
	
	public var observerLocation: CLLocation {
		return self.location 
	}
	
	public func set(julianDate jd: JulianDate) {
		self.jdd = jd
		self.objectWillChange.send()
	}
	
	
	private func getStars() -> [StarPositionMag] {
		let stars = StarDatabase.shared.getVisibleStars(maxMagnitude: Float(maxMag))
		
		var result: [StarPositionMag] = []
		for star in stars {
			if let mag = star.getMagnitude() {
				let pos = transformPosition(star.positionPolar)
				let name = star.getName()
				
				result.append(StarPositionMag(id: star.id, pos: pos, mag: mag, name: name))
			}
		}
		return result
	}
	
	private func transformPosition(_ posPolar: Polar) -> Vector3 {
		return Vector3.from(ra: posPolar.phi, dec: posPolar.theta)
	}
	
	private func getConstellations() -> [ConstLine] {
		let constellationLines = StarDatabase.shared.getConstellationLines()
		if constellationLines.count == 0 {
			return []
		}
		
		let stars = StarDatabase.shared.getAllStars()
		if stars.count == 0 {
			return []
		}
		
		var constLines = [ConstLine]()
		var index: Int32 = 0
		for key in constellationLines.keys {
			if let lines = constellationLines[key] {
				
				for line in lines.line {
					let star1 = stars[line.from]
					let star2 = stars[line.to]
				
					let pos1 = transformPosition(star1.positionPolar)
					let pos2 = transformPosition(star2.positionPolar)
					
					constLines.append(ConstLine(id: index, constellationId: lines.constellationId, from: pos1, to: pos2))
					index += 1
				}
			}
		}
		return constLines
	}
	
	public func update() {
		
		if self.isDirty {
			
			DispatchQueue.global(qos: .userInitiated).async { [weak self] in
			
				if let stars = self?.getStars(), let lines = self?.getConstellations() {
				
					DispatchQueue.main.async { [weak self] in
						self?.stars = stars
						self?.lines = lines
						self?.isDirty = false
						self?.objectWillChange.send()
					}
				}
			}
		}
	}
}

protocol SkySphereViewContent {
	
	func drawContent(_ projection: SkySphereViewProjection) -> AnyView
}

struct SkySphereViewStyle {
	let starColor: Color
	let constellationLineColor: Color
	let directionLabelsColor: Color
	let backgroundColor: Color
	let gridColor: Color
	let borderColor: Color
	
	init(starColor: Color = Color.white,
		 constellationLineColor: Color = Color.gray.opacity(0.75),
		 directionLabelsColor: Color = Color.green.opacity(0.6),
		 gridColor: Color = Color.gray.opacity(0.4),
		 borderColor: Color = Color.gray.opacity(0.3),
		 backgroundColor: Color = Color.black) {
		self.starColor = starColor
		self.constellationLineColor = constellationLineColor
		self.directionLabelsColor = directionLabelsColor
		self.gridColor = gridColor
		self.borderColor = borderColor
		self.backgroundColor = backgroundColor
	}
	
}

struct SkySphereViewStyleEnvironmentKey: EnvironmentKey {
	static var defaultValue: SkySphereViewStyle = .init()
}

extension EnvironmentValues {
	var skySphereViewStyle: SkySphereViewStyle {
		get { self[SkySphereViewStyleEnvironmentKey.self] }
		set { self[SkySphereViewStyleEnvironmentKey.self] = newValue }
	}
}

extension View {
	func skySphereViewStyle(_ style: SkySphereViewStyle) -> some View {
		environment(\.skySphereViewStyle, style)
	}
}



struct SkySphereView: View {
	@ObservedObject var dataSource: SkySphereViewDataSource
	private let content: SkySphereViewContent?
	@Environment(\.skySphereViewStyle) var style
	@Environment(\.colorScheme) var colorScheme
	
	public init(source: SkySphereViewDataSource,
				content: SkySphereViewContent? = nil) {
		self.dataSource = source
		self.content = content
	}
	
	private func project(line: ConstLine, projection: SkySphereViewProjection) -> (Int32, CGPoint, CGPoint)? {
		if let p1 = projection.project(pos: line.from),
			let p2 = projection.project(pos: line.to) {
			return (line.id, p1, p2)
		}
		
		return nil
	}
	
	private func drawSkyContent(_ geometry: GeometryProxy) -> some View {
		let size: CGSize = CGSize(width: geometry.size.minDimension, height: geometry.size.minDimension)
		
		let projection = SkySphereViewProjection(size: size, center: geometry.center,
												 jdd: self.dataSource.jdd,
												 location: self.dataSource.location)
		
		return Canvas(rendersAsynchronously: true) { context, size in
			drawMilkyway(context, size, projection)
			drawLines(context, size, projection)
			drawStars(context, size, projection)
			drawLabels(context, size, projection)
		}
		.overlay {
			
			self.content?.drawContent(projection)
		}
		.clipCircle()
	}
	
	func getTextSize(text: String, withAttributes attrs: [NSAttributedString.Key : Any], constrainedTo box: NSSize) -> NSRect {
		let storage = NSTextStorage(string: text)
		let container = NSTextContainer(containerSize: NSSize(width: box.width, height: box.height))
		let layout = NSLayoutManager()
		layout.addTextContainer(container)
		storage.addLayoutManager(layout)
		storage.addAttributes(attrs, range: NSMakeRange(0, storage.length))
		container.lineFragmentPadding = 0.0
		let _ = layout.glyphRange(for: container)
		return layout.usedRect(for: container)
	}
	
	private func createLabel(star: StarPositionMag, projection: SkySphereViewProjection, font: NSFont, labels: inout [CGRect]) -> CGPoint? {
		
		if let p = projection.project(pos: star.pos), let name = star.name {
			let sizeRect = getTextSize(text: name, withAttributes: [NSAttributedString.Key.font : font], constrainedTo: NSSize(width: Int.max, height: Int.max))
			
			let p1 = p.offset(0, 14)
			let rect = CGRect(x: CGFloat(p1.x) - sizeRect.width, y: CGFloat(p1.y) - sizeRect.height, width: sizeRect.width, height: sizeRect.height)
			
			if labels.allSatisfy({ !$0.intersects(rect) }) {
				labels.append(rect.insetBy(dx: -5, dy: -15))
				return p1
			}
			
			let p2 = p.offset(0, -14)
			let rect2 = CGRect(x: CGFloat(p2.x) - sizeRect.width, y: CGFloat(p2.y) - sizeRect.height, width: sizeRect.width, height: sizeRect.height)
			
			if labels.allSatisfy({ !$0.intersects(rect2) }) {
				labels.append(rect2.insetBy(dx: -5, dy: -15))
				return p2
			}
			
		}
		return nil
	}
	
	private func drawLabels(_ context: GraphicsContext, _ size: CGSize, _ projection: SkySphereViewProjection){
		if self.dataSource.stars.count > 0 {
			let namedStars = self.dataSource.stars.filter({ $0.name != nil }).sorted(by: {s1, s2 in s1.mag < s2.mag})
			var rects: [CGRect] = [CGRect]()
			let font = NSFont.systemFont(ofSize: 11)
			
			var localContext = context
			localContext.addFilter(.shadow(radius: 2.0))
			localContext.addFilter(.shadow(radius: 3.0))
			localContext.opacity = 0.8
			
			for star in namedStars {
				if let p = createLabel(star: star, projection: projection, font: font, labels: &rects)  {
					let resolved = context.resolve(Text(star.name!).font(Font(font)))
					let sz = resolved.measure(in: size)
					let rect = CGRect.from(center: p, size: sz)
					
					localContext.draw(resolved, in: rect)
				}
			}
		}
		
	}
	
	private func drawMilkyway(_ context: GraphicsContext, _ size: CGSize, _ projection: SkySphereViewProjection)  {
		
		let regions = MilkywayDataManager.shared.getMilkywayData(center: projection.centerPoint, fov: 90.0, minIntensity: 0.0)
		let rect = CGRect(.zero, size)
		
		var localContext = context
		localContext.addFilter(.blur(radius: 2.5))
		localContext.opacity = 0.6
		
		if regions.count > 0 {
			for region in regions {
				if let color = self.dataSource.milkywayColors[Int(region.intensity*100)] {
					localContext.fill(
						MilkywayShape(region: region, projection: projection).path(in: rect),
						with: .color(color))
				}
			}
		}
	}
	
	
	private func drawStars(_ context: GraphicsContext, _ size: CGSize, _ projection: SkySphereViewProjection) {
		
		var localContext = context
		localContext.addFilter(.blur(radius: 0.5))
		localContext.addFilter(.shadow(color: Color.white, radius: 3.0))
				
		localContext.fill(
			StarsShape(jdd: self.dataSource.jdd, location: self.dataSource.location,
						   stars: self.dataSource.stars, maxStarMag: Float(self.dataSource.maxMagnitude),
						   projection: projection)
					.path(in: CGRect(.zero, size))
			, with: .color(style.starColor))
	}
	
	private func drawLines(_ context: GraphicsContext, _ size: CGSize, _ projection: SkySphereViewProjection) {
		
		context.stroke(
			ConstellationsShape(jdd: self.dataSource.jdd,
								location: self.dataSource.location,
								lines: self.dataSource.lines,
								projection: projection)
				.path(in: CGRect(.zero, size))
			, with: .color(style.constellationLineColor), lineWidth: 0.5)
	}
	
	@ViewBuilder
	func drawDirections() -> some View {
		GeometryReader { geometry in
			Text("N")
				.font(Font.system(size: 18, weight: .semibold, design: .rounded))
				.position(from: geometry.localCenter,
						  angle: SwiftUI.Angle(degrees: 180.0),
						  radius: 0.5 * geometry.size.minDimension)
			
			Text("E")
				.font(Font.system(size: 18, weight: .semibold, design: .rounded))
				.position(from: geometry.localCenter,
						  angle: SwiftUI.Angle(degrees: 270.0),
					  radius: 0.5 * geometry.size.minDimension)
			
			
			Text("S")
				.font(Font.system(size: 18, weight: .semibold, design: .rounded))
				.position(from: geometry.localCenter,
						  angle: SwiftUI.Angle(degrees: 0.0),
						  radius: 0.5 * geometry.size.minDimension)
			
			Text("W")
				.font(Font.system(size: 18, weight: .semibold, design: .rounded))
				.position(from: geometry.localCenter,
						  angle: SwiftUI.Angle(degrees: 90.0),
						  radius: 0.5 * geometry.size.minDimension)
		}
		.foregroundColor(style.directionLabelsColor)
		.shadow(color: colorScheme == .dark ? .black : .white, radius: 3.0)
		.shadow(color: colorScheme == .dark ? .black : .white, radius: 2.0)
		
	}
	
	
	
	
	var body: some View {
		GeometryReader { geometry in
		
			ZStack(alignment: .center) {
			
				Circle()
					.fill(RadialGradient(gradient: Gradient(colors: [Color.clear, Color.white.opacity(0.12)]),
										 center: .center,
										 startRadius: geometry.size.height * 0.3,
										 endRadius: geometry.size.height * 0.5))
					.position(geometry.center.x, geometry.center.y)
				
				self.drawSkyContent(geometry)
					.clipCircle()
				
				PolarGrid(rCount: 3, thetaCount: 5)
					.stroke(style.gridColor, lineWidth: 0.5)
					.frame(CGSize(width: geometry.size.minDimension, height: geometry.size.minDimension))
					.clipCircle()
				
				MeridianLine()
					.stroke(Color.green.opacity(0.2), lineWidth: 0.5)
					.frame(CGSize(width: geometry.size.minDimension, height: geometry.size.minDimension))
					.clipCircle()
				
				Rectangle()
					.subtracting(Circle().inset(by: 16))
					.fillColor(Color.controlBackgroundDarkColor)
					
				
				drawDirections()
			}
			
		}
		.onAppear {
			self.dataSource.update()
		}
		
	}
}




