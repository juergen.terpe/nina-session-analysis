//
//  MeridianLine.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 03.04.24.
//

import SwiftUI

struct MeridianLine: Shape {
	public func path(in rect: CGRect) -> Path {
		let w = rect.width
		let h = rect.height
		let maxRadius =  w > h ? w/2.0 : h/2.0
		
		return Path { path in
			let x = 0.0
			let y = 2.0 * maxRadius
			let adjustedPoint = CGPoint(x: x + rect.midX, y: y + rect.midY)
			let adjustedEnd = CGPoint(x: rect.midX - x, y: rect.midY - y)
			path.move(to: adjustedPoint)
			path.addLine(to: adjustedEnd)
		}
	}
}



