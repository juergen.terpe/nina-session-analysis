//
//  PolarGrid.swift
//  Sky Calendar
//
//  Created by Jürgen Terpe on 03.05.20.
//  Copyright © 2020 Jürgen Terpe. All rights reserved.
//

import SwiftUI


public struct PolarGrid: Shape {
    public var rCount: Int
    public var thetaCount: Int
    public init(rCount: Int, thetaCount: Int) {
        self.rCount = rCount
        self.thetaCount = thetaCount
    }
    
    public func path(in rect: CGRect) -> Path {
        let w = rect.width
        let h = rect.height
		let maxRadius =  w > h ? w/2.0 : h/2.0
		let thetaIncrement = CGFloat.pi / CGFloat(thetaCount + 1)
        let radialIncrement = maxRadius / CGFloat(rCount + 1)
        return Path { path in
            // for loop creates lines intersecting the center of the circles dividing the graph into subsections.
            for i in 0...(thetaCount) {
				let x = 2.0 * maxRadius * CGFloat(sin(CGFloat(i) * thetaIncrement))
				let y = 2.0 * maxRadius * CGFloat(cos(CGFloat(i) * thetaIncrement))
                let adjustedPoint = CGPoint(x: x + rect.midX, y: y + rect.midY)
                let adjustedEnd = CGPoint(x: rect.midX - x, y: rect.midY - y)
                path.move(to: adjustedPoint)
                path.addLine(to: adjustedEnd)
            }
            // for loop generates circles of increasing radius.
            for i in 0...(self.rCount + 1) {
                let radius = radialIncrement * CGFloat(i)
                let rect = CGRect(x: rect.midX-radius, y: rect.midY-radius, width: radius*2, height: radius*2)
                path.addEllipse(in: rect, transform: .identity)
            }
        }
    }
}
