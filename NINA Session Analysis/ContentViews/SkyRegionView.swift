//
//  SkyRegionView.swift
//  SkyToday
//
//  Created by Jürgen Terpe on 19.12.21.
//

import SwiftUI
import simd



fileprivate class Projection {
    private var projection: double4x4 = double4x4(diagonal: SIMD4<Double>(1,1,1,1))
    private var width2: Double
    private var height2: Double
    private var center: CGPoint
    private var radius: Double
    private let fov: Double
    private let near: Double
    private let far: Double
    private let sh: Double
    private let dec: Double
    fileprivate var scaleSize: Double = 1.0
    fileprivate let moveZ = double4x4(rows: [Double4(1.0, 0.0, 0.0, 0.0),
                                             Double4(0.0, 1.0, 0.0, 0.0),
                                             Double4(0.0, 0.0, 1.0, -1.0),
                                             Double4(0.0, 0.0, 0.0, 1.0)])
    
    fileprivate init(size: CGSize,
                sh: Double,
                dec: Double,
                fov: Double ,
                near: Double = 1.0, far: Double = 10.0) {
        self.fov = fov
        self.near = near
        self.far = far
        self.width2 = 1.0
        self.height2 = 1.0
        self.radius = 1.0
        self.center = CGPoint(x: size.width/2.0, y: size.height/2.0)
        self.sh = sh
        self.dec = dec
        updateProjection(size: size)
    }
    
    private func createRotationMatrix() -> double4x4 {
        let phi = -self.sh
        let alpha = -(Double.pi/2.0 - self.dec)
        
        let (sinPhi,cosPhi) = sincos(phi)
        let (sinAlpha,cosAlpha) = sincos(alpha)
        
        return double4x4(rows: [Double4(sinPhi, cosPhi, 0.0, 0.0),
                                Double4(cosPhi*cosAlpha, -sinPhi*cosAlpha, sinAlpha, 0.0),
                                Double4(-cosPhi*sinAlpha, sinPhi*sinAlpha, cosAlpha, 0.0),
                                Double4(0.0, 0.0, 0.0, 1.0)])
    }
    
    private func createScaleMatrix(_ size: CGSize) -> double4x4 {
        let sx = -Double(size.width)/2.0
        let sy = Double(size.height)/2.0

        return double4x4(rows: [Double4(sx, 0.0, 0.0, 0.0),
                                Double4(0.0, sy, 0.0, 0.0),
                                Double4(0.0, 0.0, 1.0, 0.0),
                                Double4(0.0, 0.0, 0.0, 1.0)])
    }

    
    
    private func updateProjection(size: CGSize) {
        
        let aspect = Double(size.width/size.height)
        
        self.width2 = Double(size.width / 2)
        self.height2 = Double(size.height / 2)
        self.radius = min(self.width2, self.height2)
        
        let range = tan(fov / 2.0) * near
        let sx = (2.0 * near) / (2.0 * range * aspect)
        let sy = near / range
        let sz = -(far + near) / (far - near)
        let pz = -(2.0 * far * near) / (far - near)
        
        let rotationMatrix = createRotationMatrix()
        let scaleMatrix = createScaleMatrix(size)
        
        self.scaleSize = radius/fov
        let proj = double4x4(rows: [SIMD4<Double>(sx, 0.0, 0.0, 0.0),
                                    SIMD4<Double>(0.0, sy, 0.0, 0.0),
                                 SIMD4<Double>(0.0, 0.0, sz, -1.0),
                                 SIMD4<Double>(0.0, 0.0, pz, 0.0)])
        
        self.projection = scaleMatrix * proj * moveZ * rotationMatrix
    }
    
    fileprivate func project(ra: Double, dec: Double) -> CGPoint? {
        let vec = Vector3(polar: Polar(ra: ra, dec: dec))
        return project(v: SIMD4<Double>(vec.X, vec.Y, vec.Z, 1.0))
    }
    
    fileprivate func project(vec: Vector3) -> CGPoint? {
        return project(v: SIMD4<Double>(vec.X, vec.Y, vec.Z, 1.0))
    }
    
    fileprivate func project(v: SIMD4<Double>) -> CGPoint? {
        let p = self.projection * v
        let z = p.z
        if z > 0.0  { // -0.2
            return nil
        }
            
        let x = p.x + self.width2
        let y = p.y + self.height2
        
        if x.isNaN || y.isNaN {
            return nil
        }
        
        return CGPoint(x: x, y: y)
    }
}

fileprivate struct ConstellationLinesShape: Shape {
    fileprivate let projection: Projection
    @ObservedObject var dataSource: SkyRegionViewDataSource

    func path(in rect: CGRect) -> Path {

        var path = Path()
        
        for line in self.dataSource.lines {
            if let p1 = projection.project(vec: line.from),
                let p2 = projection.project(vec: line.to) {
                
                path.move(to: p1)
                path.addLine(to: p2)
            }
        }
        
        return path
    }
}

fileprivate struct StarsShape: Shape {
    fileprivate let projection: Projection
    @ObservedObject var dataSource: SkyRegionViewDataSource
    var config: SkyRegionViewStyle

    private func project(star: StarMag, projection: Projection) -> CGRect? {
        
        if let p = projection.project(vec: star.pos) {
            let radius = max(0.5, CGFloat(self.config.starBrightness
                                    * pow(6.5 - Double(star.mag), self.config.starContrast)))
                
            return CGRect.from(center: p, radius: radius)
        }
        
        return nil
    }
    
    func path(in rect: CGRect) -> Path {

        var path = Path()
        
        for star in self.dataSource.stars {
            if let p = project(star: star, projection: projection) {
                path.addEllipse(in: p)
            }
        }
        
        return path
    }
}

struct SkyRegionViewStyle {
    var starColor: Color
    var starContrast: Double
    var starBrightness: Double
    var background: Color
    var lineColor: Color
   
    var drawMoon: Bool
    var drawSun: Bool
    var drawPlanets: Bool
    var scale: CGFloat
    var centerMarker: Bool
    
    init(starColor: Color = Color.white,
         starContrast: Double = 1.5,
         starBrightness: Double = 0.4,
         background: Color = Color.black,
         lineColor: Color = Color.gray.opacity(0.5),
         drawMoon: Bool = true,
         drawSun: Bool = true,
         drawPlanets: Bool = true,
         scale: CGFloat = 1.0,
         centerMarker: Bool = false) {
        
        self.starColor = starColor
        self.starContrast = starContrast
        self.starBrightness = starBrightness
        self.background = background
        self.lineColor = lineColor
        self.drawMoon = drawMoon
        self.drawSun = drawSun
        self.drawPlanets = drawPlanets
        self.scale = scale
        self.centerMarker = centerMarker
    }
    
}

struct SkyRegionViewStyleEnvironmentKey: EnvironmentKey {
    static var defaultValue: SkyRegionViewStyle = .init()
}

extension EnvironmentValues {
    var skyRegionViewStyle: SkyRegionViewStyle {
        get { self[SkyRegionViewStyleEnvironmentKey.self] }
        set { self[SkyRegionViewStyleEnvironmentKey.self] = newValue }
    }
}

extension View {
    func skyRegionViewStyle(_ style: SkyRegionViewStyle) -> some View {
        environment(\.skyRegionViewStyle, style)
    }
}


struct SkyRegionView: View {
    @StateObject var dataSource: SkyRegionViewDataSource = SkyRegionViewDataSource()
    @State var pulsing: Bool = false
    
    private var centerPosition: Vector3
    private var viewRadius: Double
    private var jdd: JulianDate
    @Environment(\.skyRegionViewStyle) var style
    
    
    public init(centerPosition: Vector3, radius: Double = 40.0, jdd: JulianDate = JulianDate.now()) {
        self.centerPosition = centerPosition
        self.viewRadius = radius
        self.jdd = jdd
    }
    
	private func getCenter() -> Polar {
		let center = self.dataSource.center
		return center.toPolar()
	}
    
    private func drawSkyContent(_ geometry: GeometryProxy) -> some View {
        
        let polarCenter = getCenter()
		let aspect = Double(geometry.size.width/geometry.size.width)
        let angle = self.dataSource.angle * aspect * Double(style.scale)
        
        let projection = Projection(size: geometry.size,
                                    sh: polarCenter.phi,
                                    dec: polarCenter.theta,
                                    fov: angle)
        
        return Group {
            
            drawConstellationLines(projection)
            drawStars(projection)
            //drawLabels(projection)
           
        }
    }
    
    private func project(position pos: Vector3, projection: Projection) -> CGPoint? {
        let polar = pos.toPolar()
        return projection.project(ra: polar.phi, dec: polar.theta)
    }
    
	/*
    private func createLabel(from star: StarMag, projection: Projection, font: UIFont, labels: inout [CGRect]) -> CGPoint? {
        
        if let p = projection.project(vec: star.pos), let name = star.name {
            let size = name.getSize(font: font)
            let rect = CGRect(x: CGFloat(p.X) - size.width, y: CGFloat(p.Y) - size.height, width: size.width, height: size.height)
            
            if labels.all(predicate: { !$0.intersects(rect) }) {
                labels.append(rect.insetBy(dx: -5, dy: -5))
                
                return p
            }
        }
        return nil
    }*/
    
    /*
    @ViewBuilder
    private func drawLabels(_ projection: Projection) -> some View {
        if self.dataSource.starCount > 0 {
            let namedStars = self.dataSource.stars.filter({ $0.name != nil })
            var rects: [CGRect] = [CGRect]()
            
            ForEach(namedStars) { star in
                if let p = createLabel(from: star, projection: projection, font: font, labels: &rects)  {
                    Text(star.name!)
                        //.font(Font())
                        .foregroundColor(Color.secondary)
                        .position(p.offset(x: 0, y: 14))
                        .shadow(color: Color.black, radius: 2)
                }
            }
        }
        
    }*/
    
    private func drawStars(_ projection: Projection) -> some View {
        StarsShape(projection: projection, dataSource: self.dataSource, config: self.style)
            .fill(self.style.starColor)
            .shadow(color: self.style.starColor.opacity(0.6), radius: 2)
    }
    
    private func drawConstellationLines(_ projection: Projection) -> some View {
        ConstellationLinesShape(projection: projection, dataSource: self.dataSource)
            .stroke(self.style.lineColor, lineWidth: 1.0)
    }
    
    var body: some View {
        GeometryReader { geometry in
            self.drawSkyContent(geometry)
        }
        .clipped()
        .onAppear {
            self.dataSource.set(jd: jdd)
            self.dataSource.set(center: self.centerPosition)
            self.dataSource.set(radius: self.viewRadius)
            self.dataSource.update()
        }
     
        
    }
}
