//
//  GuidingDataChart.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 18.05.24.
//

import SwiftUI
import Charts

enum GuideGraphDitherItem: String {
	case dither   = "Dither"
	case ditherDx = "Dither-dx"
	case ditherDy = "Dither-dy"
	case duration = "Duration"
	
	var name: LocalizedStringKey {
		switch self {
			case .dither:   return LocalizedStringKey("Dither (\")")
			case .ditherDx: return LocalizedStringKey("Dither dx (\")")
			case .ditherDy: return LocalizedStringKey("Dither dy (\")")
			case .duration: return LocalizedStringKey("Duration (s)")
		}
	}
}


fileprivate struct ChartData: Identifiable {
	var id: UUID = UUID()
	let index: Int
	let value1: Float
	let value2: Float
}

@Observable
fileprivate class GuidingChartData {
	var session: GuidingData? = nil
	var data: [ChartData] = []
	var minIndex: Int = 0
	var maxIndex: Int = 0
	var minValue1: Float = 0.0
	var maxValue1: Float = 1.0
	var minValue2: Float = 0.0
	var maxValue2: Float = 1.0
	var minYValue: Float = 0.0
	var maxYValue: Float = 1.0
	
	private func getData(_ item: GuideGraphDitherItem, frame: DitherFrame, scale: Float) -> Float {
		switch item {
			case .dither:   return sqrt(frame.dx! * frame.dx! + frame.dy! * frame.dy!) * scale
			case .ditherDx: return abs(frame.dx!) * scale
			case .ditherDy: return abs(frame.dy!) * scale
			case .duration: return Float((frame.end - frame.begin) * 86400.0)
		}
	}
	
	func getValues(at index: Int) -> (Float, Float)? {
		if self.minIndex <= index && self.maxIndex >= index, let dp = self.data.first(where: { $0.index == index}) {
			return (dp.value1, dp.value2)
		}
		return nil
	}
	
	func update(_ item1: GuideGraphDitherItem, _ item2: GuideGraphDitherItem, session: GuidingData) async {
		var result = [ChartData]()

		var min1: Float?
		var max1: Float?
		var min2: Float?
		var max2: Float?
		
		let pxScale = session.pixelScale ?? 1.0
		
		for (index, frame) in session.ditherData.enumerated() {
			let value1 = getData(item1, frame: frame, scale: pxScale)
			let value2 = getData(item2, frame: frame, scale: pxScale)
			
			if min1 == nil {
				min1 = value1
			} else {
				min1 = min(min1!, value1)
			}

			if max1 == nil {
				max1 = value1
			} else {
				max1 = max(max1!, value1)
			}
			
			if min2 == nil {
				min2 = value2
			} else {
				min2 = min(min2!, value2)
			}

			if max2 == nil {
				max2 = value2
			} else {
				max2 = max(max2!, value2)
			}
			
			result.append(ChartData(index: index, value1: value1, value2: value2))
		}
		
		let minIdx = result.map({ $0.index }).min() ?? 0
		let maxIdx = result.map({ $0.index }).max() ?? 0
		let minValue1 = min1 ?? 0.0
		let maxValue1 = max1 ?? 1.0
		let minValue2 = min2 ?? 0.0
		let maxValue2 = max2 ?? 1.0
		
		let minY = min(minValue1, minValue2)
		let maxY = max(maxValue1, maxValue2)
		
		let minYValue: Float = 0.9 * (minY / maxY)
		let maxYValue: Float = 1.4
		let resultingChartData = result
		
		await MainActor.run {
			self.maxIndex = maxIdx
			self.minIndex = minIdx
			self.minValue1 = minValue1
			self.maxValue1 = maxValue1 != minValue1 ? maxValue1 : maxValue1 * 1.1
			self.minValue2 = minValue2
			self.maxValue2 = maxValue2 != minValue2 ? maxValue2 : maxValue2 * 1.1
			self.minYValue = minYValue
			self.maxYValue = maxYValue
			self.data = resultingChartData
		}
	}
}


struct GuidingDitherChart: View {
	
	let session: GuidingData
	let item1: GuideGraphDitherItem
	let item2: GuideGraphDitherItem
	let color1: Color
	let color2: Color
	
	@State private var chartData: GuidingChartData = GuidingChartData()
	@State private var selectedIndex: Int?
	let strideBy = 6
	
	private func getFormattedValue(value: Float, _ item: GuideGraphDitherItem) -> String {
		switch item {
			case .ditherDx, .ditherDy, .dither:
				return String(format: "%.1f", value)
			case .duration:
				return String(format: "%.1f", value)
		}
	}
	
	var body: some View {
		ZStack {
			VStack {
				HStack {
					Text(item1.name)
					Text(" / ")
					Text(item2.name)
					
					Spacer()
				}
				
				Chart {
					ForEach(self.chartData.data, id: \.index) {  data in
						LineMark(
							x: .value("Index", data.index),
							y: .value(item1.rawValue, data.value1 / self.chartData.maxValue1)
						)
						.foregroundStyle(by: .value("Index", item1.rawValue))
						.accessibilityHidden(true)
						
						LineMark(
							x: .value("Index", data.index),
							y: .value(item2.rawValue, data.value2 / self.chartData.maxValue2)
						)
						.foregroundStyle(by: .value("Index", item2.rawValue))
						.accessibilityHidden(true)
						
						
						PointMark(
							x: .value("Index", data.index),
							y: .value(item1.rawValue, data.value1 / self.chartData.maxValue1)
						)
						.foregroundStyle(by: .value("Index", item1.rawValue))
						.accessibilityHidden(true)
						
						PointMark(
							x: .value("Index", data.index),
							y: .value(item2.rawValue, data.value2 / self.chartData.maxValue2)
						)
						.foregroundStyle(by: .value("Index", item2.rawValue))
						.accessibilityHidden(true)
						
						/*
						if let selectedIndex, let (v1, v2) = self.chartData.getValues(at: selectedIndex) {
							
							PointMark(
								x: .value("Index", selectedIndex),
								y: .value(item1.rawValue, v1 / self.chartData.maxValue1)
							)
							.foregroundStyle(Color.orange)
							.accessibilityHidden(true)
							
							PointMark(
								x: .value("Index", selectedIndex),
								y: .value(item1.rawValue, v2 / self.chartData.maxValue2)
							)
							.foregroundStyle(Color.orange)
							.accessibilityHidden(true)
							
						}
						*/
					}
				}
				.chartForegroundStyleScale([
					item1.rawValue: color1.gradient,
					item2.rawValue: color2.gradient,
				])
				//.chartScrollableAxes(.horizontal)
				//.chartXVisibleDomain(length: self.chartData.data.isNotEmpty ? min(self.chartData.data.count, 50) : 50)
				.chartXScale(domain: self.chartData.minIndex...self.chartData.maxIndex)
				.chartYAxis {
					
					let values = self.chartData.data.map { $0.value1 }
					if values.count > 0 {
						let min = self.chartData.minValue1
						let max = self.chartData.maxValue1
						let defaultStride = Array(stride(from: 0.0, to: 1.0, by: 1.0/Double(strideBy)))
						
						let stride = Array(stride(from: min,
												  through: max,
												  by: (max - min)/Float(strideBy)))
						AxisMarks(position: .leading, values: defaultStride) { axis in
							let value = stride[axis.index]
							AxisValueLabel("\(String(format: "%.1f", value))", centered: false)
								.foregroundStyle(color1)
						}
						
					}
					
					let values2 = self.chartData.data.map { $0.value2 }
					if values2.count > 0 {
						let min = self.chartData.minValue2
						let max = self.chartData.maxValue2
						
						let defaultStride = Array(stride(from: 0.0, to: 1.0, by: 1.0/Double(strideBy)))
						
						let stride = Array(stride(from: min,
												  through: max,
												  by: (max - min)/Float(strideBy)))
						AxisMarks(position: .trailing, values: defaultStride) { axis in
							let value = stride[axis.index]
							AxisValueLabel("\(String(format: "%.1f", value))", centered: false)
								.foregroundStyle(color2)
						}
						
					}
				}
				.chartXSelection(value: $selectedIndex)
				
			}
			.clipped()
			.task {
				await self.chartData.update(self.item1, self.item2, session: self.session)
			}
	
		}
	}
}

