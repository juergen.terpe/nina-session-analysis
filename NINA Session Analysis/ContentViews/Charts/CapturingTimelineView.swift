//
//  CapturingTimelineView.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 17.04.24.
//

import SwiftUI
import PureSwiftUI
import CoreLocation

struct HeightTime: Identifiable {
	let id = UUID()
	let time: JulianDate
	let alt: Double
}

struct AFTime: Identifiable {
	let id: UUID = UUID()
	let begin: JulianDate
	let end: JulianDate
	let autoFocus: AutoFocus
}

@Observable class CapturingTimelineData {
	let date: JulianDate
	let session: SessionData
	let sunRise: JulianDate?
	let sunSet: JulianDate?
	let midnight: JulianDate
	let civilDusk: JulianDate?
	let civilDawn: JulianDate?
	let nauticalDusk: JulianDate?
	let nauticalDawn: JulianDate?
	let astronomicalDusk: JulianDate?
	let astronomicalDawn: JulianDate?
	let sunAltitudes: [HeightTime]
	let moonAltitudes: [HeightTime]
		
	init?(date: JulianDate, session: SessionData) {
		self.date = date
		self.session = session
		if let loc = session.getLocation(for: date) {
			let long = loc.coordinate.longitude
			let lat = loc.coordinate.latitude
			
			let sunSet = CapturingTimelineData.getSunSet(loc, date)
			var sunRise = CapturingTimelineData.getSunRise(loc, date)
			if sunSet != nil && sunRise != nil {
				
				if sunRise!.jd < sunSet!.jd {
					sunRise = JulianDate(julianDate: sunRise!.jd + 1.0)
				}
				
				self.sunRise = sunRise
				self.sunSet = sunSet
				self.midnight = JulianDate(julianDate: 0.5*(sunRise!.jd + sunSet!.jd))
			}
			else {
				let sp = Sun.getAltAz(date, long: long, lat: lat)
				if sp.altitude > 0.0 {
					return nil
				}
				
				self.sunSet = date.getBeginOfDay().addHours(12.0)
				self.sunRise = self.sunSet!.addDays(1.0)
				self.midnight = JulianDate(julianDate: 0.5*(sunRise!.jd + sunSet!.jd))
			}
			
			let civilDusk = CapturingTimelineData.getCivilDusk(loc, date) // abend
			var civilDawn = CapturingTimelineData.getCivilDawn(loc, date) // morgen
			if civilDawn != nil && civilDusk != nil && civilDawn!.jd < civilDusk!.jd {
				civilDawn = JulianDate(julianDate: civilDawn!.jd + 1.0)
			}
			self.civilDusk = civilDusk
			self.civilDawn = civilDawn
			
			let nauticalDusk = CapturingTimelineData.getNauticalDusk(loc, date) // abend
			var nauticalDawn = CapturingTimelineData.getNauticalDawn(loc, date) // morgen
			if nauticalDawn != nil && nauticalDusk != nil && nauticalDawn!.jd < nauticalDusk!.jd {
				nauticalDawn = JulianDate(julianDate: nauticalDawn!.jd + 1.0)
			}
			self.nauticalDawn = nauticalDawn
			self.nauticalDusk = nauticalDusk
						
			let astronomicalDusk = CapturingTimelineData.getAstronomicalDusk(loc, date) // abend
			var astronomicalDawn = CapturingTimelineData.getAstronomicalDawn(loc, date) // morgen
			if astronomicalDawn != nil && astronomicalDusk != nil && astronomicalDawn!.jd < astronomicalDusk!.jd {
				astronomicalDawn = JulianDate(julianDate: astronomicalDawn!.jd + 1.0)
			}
			self.astronomicalDawn = astronomicalDawn
			self.astronomicalDusk = astronomicalDusk
			
			var sa = [HeightTime]()
			var ma = [HeightTime]()
			for jd in stride(from: sunSet!.jd, through: sunRise!.jd, by: 5.0/1440.0) {
				let jdd = JulianDate(julianDate: jd)
				let sp = Sun.getAltAz(jdd, long: long, lat: lat)
				let mp = Moon.getAltAz(jdd, long: long, lat: lat)
				sa.append(HeightTime(time: jdd, alt: sp.altitude*Constants.Deg))
				ma.append(HeightTime(time: jdd, alt: mp.altitude*Constants.Deg))
			}
			self.sunAltitudes = sa
			self.moonAltitudes = ma

			return
		}
		return nil
	}
	
	func findIndex(of image: SessionImage) -> Int? {
		return self.session.filteredImages.firstIndex(of: image)
	}

	func findPosition(of index: Int, in rect: CGRect) -> CGPoint? {
		guard let sunSet = self.sunSet, let sunRise = self.sunRise else { return nil }
		
		let dt = sunRise.jd - sunSet.jd
		let sx = rect.width / dt
		let sy = rect.height / 180.0
		let middleY = rect.midY
		let images = self.session.filteredImages.filter({ $0.altitude != nil && $0.beginLocal != nil && $0.endLocal != nil})
		if images.count > index {
			let image = images[index]
			if let alt = image.altitude, let beginLocal = image.beginLocal, let endLocal = image.endLocal {
				let x1 = (beginLocal.jd - sunSet.jd) * sx
				let x2 = (endLocal.jd - sunSet.jd) * sx
				let x = (x1 + x2) / 2.0
				let y = middleY - CGFloat(alt) * sy
				return CGPoint(x: x, y: y)
			}
		}
				
		return nil
	}
	
	func findImageIndex(at pos: CGPoint, in rect: CGRect) -> (Int, CGPoint)? {
		if let sunSet = self.sunSet, let sunRise = self.sunRise {
			let dt = sunRise.jd - sunSet.jd
			let sx = rect.width / dt
			let sy = rect.height / 180.0
			let middleY = rect.midY
			
			var minX = CGFloat.greatestFiniteMagnitude
			var minIndex: Int? = nil
			var minPos: CGPoint?
			
			let images = self.session.filteredImages.filter({ $0.altitude != nil && $0.beginLocal != nil && $0.endLocal != nil})
			
			for (index, image) in images.enumerated() {
				if let alt = image.altitude, let beginLocal = image.beginLocal, let endLocal = image.endLocal {
					let x1 = (beginLocal.jd - sunSet.jd) * sx
					let x2 = (endLocal.jd - sunSet.jd) * sx
					let x = (x1 + x2) / 2.0
					let y = middleY - CGFloat(alt) * sy
					
					let dx = abs(x-pos.x)
					
					if dx < minX {
						minIndex = index
						minX = dx
						minPos = CGPoint(x: x, y: y)
					}
				}
			}
			
			if minIndex != nil && minPos != nil && minX < 10.0 {
				return (minIndex!, minPos!)
			}
		}
		return nil
	}
	
	static func getSunRise(_ loc: CLLocation, _ date: JulianDate) -> JulianDate? {
		
		let long = loc.coordinate.longitude * Constants.Rad
		let lat = loc.coordinate.latitude * Constants.Rad
		
		return RiseSetTransitFinder.findRise(object: .sun, lambda: long, phi: lat, julianDate: date)
	}
	
	static func getSunSet(_ loc: CLLocation, _ date: JulianDate) -> JulianDate? {
		let long = loc.coordinate.longitude * Constants.Rad
		let lat = loc.coordinate.latitude * Constants.Rad
		
		return RiseSetTransitFinder.findSet(object: .sun, lambda: long, phi: lat, julianDate: date)
	}
	
	static func getCivilDawn(_ loc: CLLocation, _ date: JulianDate) -> JulianDate? {
		let long = loc.coordinate.longitude * Constants.Rad
		let lat = loc.coordinate.latitude * Constants.Rad
		
		return RiseSetTransitFinder.findRise(object: .civil, lambda: long, phi: lat, julianDate: date)
	}
	
	static func getCivilDusk(_ loc: CLLocation, _ date: JulianDate) -> JulianDate? {
		let long = loc.coordinate.longitude * Constants.Rad
		let lat = loc.coordinate.latitude * Constants.Rad
		
		return RiseSetTransitFinder.findSet(object: .civil, lambda: long, phi: lat, julianDate: date)
	}
	
	static func getNauticalDawn(_ loc: CLLocation, _ date: JulianDate) -> JulianDate? {
		let long = loc.coordinate.longitude * Constants.Rad
		let lat = loc.coordinate.latitude * Constants.Rad
		
		return RiseSetTransitFinder.findRise(object: .nautical, lambda: long, phi: lat, julianDate: date)
	}
	
	static func getNauticalDusk(_ loc: CLLocation, _ date: JulianDate) -> JulianDate? {
		let long = loc.coordinate.longitude * Constants.Rad
		let lat = loc.coordinate.latitude * Constants.Rad
		
		return RiseSetTransitFinder.findSet(object: .nautical, lambda: long, phi: lat, julianDate: date)
	}
	
	static func getAstronomicalDawn(_ loc: CLLocation, _ date: JulianDate) -> JulianDate? {
		let long = loc.coordinate.longitude * Constants.Rad
		let lat = loc.coordinate.latitude * Constants.Rad
		
		return RiseSetTransitFinder.findRise(object: .astronomical, lambda: long, phi: lat, julianDate: date)
	}
	
	static func getAstronomicalDusk(_ loc: CLLocation, _ date: JulianDate) -> JulianDate? {
		let long = loc.coordinate.longitude * Constants.Rad
		let lat = loc.coordinate.latitude * Constants.Rad
		
		return RiseSetTransitFinder.findSet(object: .astronomical, lambda: long, phi: lat, julianDate: date)
	}
}

struct HeightTimeShape: Shape {
	let altitudes: [HeightTime]
	let sunSet: JulianDate
	let sunRise: JulianDate
	
	func path(in rect: CGRect) -> Path {
		let dt = sunRise.jd - sunSet.jd
		let sx = rect.width / dt
		let sy = rect.height / 180.0
		let middleY = rect.midY
		
		var path = Path()
		
		for (index, alt) in altitudes.enumerated() {
			let x = (alt.time.jd - sunSet.jd) * sx
			let y = middleY - alt.alt * sy
			if index == 0 {
				path.move(to: CGPoint(x: x, y: y))
			} else {
				path.addLine(to: CGPoint(x: x, y: y))
			}
		}
		return path
	}
	
}

struct HeightLineShape: Shape {
	let altitudes = [-60.0, -30.0, 0.0, 30.0, 60.0]
	
	func path(in rect: CGRect) -> Path {
		let sy = rect.height / 180.0
		let middleY = rect.midY
		
		var path = Path()
		
		for alt in altitudes {
			
			let y = middleY - alt * sy
			path.move(to: CGPoint(x: rect.minX, y: y))
			path.addLine(to: CGPoint(x: rect.maxX, y: y))
			
		}
		return path
	}
	
}

struct TimeLineShape: Shape {
	let sunSet: JulianDate
	let sunRise: JulianDate
	
	func nextHour(_ jd: JulianDate) -> JulianDate {
		let h = Int(frac(jd.jd)*24.0)
		return JulianDate(julianDate: floor(jd.jd) + Double(h)/24.0)
	}
	
	func path(in rect: CGRect) -> Path {
		let dt = sunRise.jd - sunSet.jd
		let sx = rect.width / dt
		var path = Path()
		
		for h in stride(from: nextHour(sunSet).jd, through: nextHour(sunRise).jd, by: 1.0/24.0) {
			let x = (h - sunSet.jd) * sx
			if x < 0.0 {
				continue
			}
		
			path.move(to: CGPoint(x: x, y: rect.minY))
			path.addLine(to: CGPoint(x: x, y: rect.maxY))
			
		}
		return path
	}
	
}

struct TimeLineLabels: View {
	let sunSet: JulianDate
	let sunRise: JulianDate
	let geometry: GeometryProxy
	
	func nextHour(_ jd: JulianDate) -> JulianDate {
		let h = Int(frac(jd.jd)*24.0)
		return JulianDate(julianDate: floor(jd.jd) + Double(h)/24.0)
	}
	
	func getTimes(_ rect: CGRect) -> [(CGPoint, String)] {
		let dt = sunRise.jd - sunSet.jd
		let sx = rect.width / dt
		
		var result = [(CGPoint, String)]()
		for h in stride(from: nextHour(sunSet).jd, through: nextHour(sunRise).jd, by: 1.0/24.0) {
			
			if (h - sunSet.jd) < 0.0 {
				continue
			}
			let x = (h - sunSet.jd) * sx
			let hour = JulianDate(julianDate: h).toDateTime().formatted(date: .omitted, time: .shortened)
			result.append((CGPoint(x: x, y: rect.maxY-12), hour))
		}
		return result
	}
	
	var body: some View {
		Group {
			let hours = getTimes(geometry.frame(in: .local))
			
			ForEach(Array(zip(hours.indices, hours)), id: \.self.0) { index, hour in
				Text(hour.1).font(.body).position(hour.0)
					.foregroundColor(Color.white)
			}
		}
	}
}


struct CapturedShape: Shape {
	let sessionImages: [SessionImage]
	let sunSet: JulianDate
	let sunRise: JulianDate
	
	func path(in rect: CGRect) -> Path {
		let dt = sunRise.jd - sunSet.jd
		let sx = rect.width / dt
		let sy = rect.height / 180.0
		let middleY = rect.midY
		
		var path = Path()
		
		for image in self.sessionImages {
			if let alt = image.altitude, let beginLocal = image.beginLocal, let endLocal = image.endLocal {
				let x1 = (beginLocal.jd - sunSet.jd) * sx
				let x2 = (endLocal.jd - sunSet.jd) * sx
				let x = (x1 + x2) / 2.0
				let y = middleY - CGFloat(alt) * sy
				path.addEllipse(in: CGRect.from(center: CGPoint(x: x, y: y), radius: 4.0))
			}
		}
		return path
	}
	
}

struct MeridianFlipShape: Shape {
	let eastImages: [SessionImage]
	let westImages: [SessionImage]
	let sunSet: JulianDate
	let sunRise: JulianDate
	
	func path(in rect: CGRect) -> Path {
		let dt = sunRise.jd - sunSet.jd
		let sx = rect.width / dt
		
		var path = Path()
		
		if let lastWest = westImages.last(where: { $0.endLocal != nil }), let endLocal = lastWest.endLocal,
		   let firstEast = eastImages.first(where: { $0.beginLocal != nil }), let beginLocal = firstEast.beginLocal {
			let x1 = (endLocal.jd - sunSet.jd) * sx
			let x2 = (beginLocal.jd - sunSet.jd) * sx
			path.addRect(CGRect(x: x1, y: rect.minY, width: x2-x1, height: rect.height))
		}
		
		return path
	}
	
}



struct CapturingTimelineView: View {
	@State var data: CapturingTimelineData
	@State var date: DateOfSession
	@State var selectedImageIndex: Int?
	@State var readonly: Bool = false
	
	@State private var selectedImagePos: CGPoint?
	@State private var showTip = false
	
	func getGradient() -> some View {
		let sunSet = self.data.sunSet!
		let sunRise = data.sunRise!
		let range = (sunRise.jd - sunSet.jd)*1440.0
		
		if let civilDusk = self.data.civilDusk, let civilDawn = self.data.civilDawn {
			let c1 = (civilDusk.jd-sunSet.jd)*1440.0 / range // abend
			let c2 = (civilDawn.jd-sunSet.jd)*1440.0 / range // morgen
			
			if let nauticalDusk = self.data.nauticalDusk, let nauticalDawn = self.data.nauticalDawn {
				let n1 = (nauticalDusk.jd-sunSet.jd)*1440.0 / range // abend
				let n2 = (nauticalDawn.jd-sunSet.jd)*1440.0 / range // morgen
				
				
				if let astronomicalDusk = self.data.astronomicalDusk, let astronomicalDawn = self.data.astronomicalDawn {
					let a1 = (astronomicalDusk.jd-sunSet.jd)*1440.0 / range // abend
					let a2 = (astronomicalDawn.jd-sunSet.jd)*1440.0 / range // morgen
					
					return AnyView(
						LinearGradient(stops: [
							Gradient.Stop(color: Color.skyBlue, location: 0.0),
							Gradient.Stop(color: Color.civilBlue, location: c1),
							Gradient.Stop(color: Color.nauticalBlue, location: n1),
							Gradient.Stop(color: Color.black, location: a1),
							Gradient.Stop(color: Color.black, location: a2),
							Gradient.Stop(color: Color.nauticalBlue, location: n2),
							Gradient.Stop(color: Color.civilBlue, location: c2),
							Gradient.Stop(color: Color.skyBlue, location: 1.0),
						],
						startPoint: .leading, endPoint: .trailing))
				} else {
					return AnyView(
						LinearGradient(stops: [
							Gradient.Stop(color: Color.skyBlue, location: 0.0),
							Gradient.Stop(color: Color.civilBlue, location: c1),
							Gradient.Stop(color: Color.nauticalBlue, location: n1),
							Gradient.Stop(color: Color.nauticalDarkBlue, location: (n1+n2)/2.0),
							Gradient.Stop(color: Color.nauticalBlue, location: n2),
							Gradient.Stop(color: Color.civilBlue, location: c2),
							Gradient.Stop(color: Color.skyBlue, location: 1.0),
						],
						startPoint: .leading, endPoint: .trailing))
				}
			} else {
				return AnyView(
					LinearGradient(stops: [
						Gradient.Stop(color: Color.skyBlue, location: 0.0),
						Gradient.Stop(color: Color.civilBlue, location: c1),
						Gradient.Stop(color: Color.civilBlue, location: c2),
						Gradient.Stop(color: Color.skyBlue, location: 1.0),
					],
					startPoint: .leading, endPoint: .trailing))
			}
		} else {
			return AnyView(Color.skyBlue)
		}
	}
	
	func getHeightLables() -> some View {
		
		VStack {
			Spacer()
			ForEach([60, 30, 0, -30, -60], id: \.self) { h in
				
				HStack {
					Text("\(h)°").foregroundColor(.white)
					Spacer()
					Text("\(h)°").foregroundColor(.white)
				}.padding(.horizontal, 8)
				Spacer()
			}
		}
	}
	
	
	func getCapturedShape(sunSet: JulianDate, sunRise: JulianDate) -> some View {
		
		let images = self.data.session.filteredImages.filter({
			if let start = $0.startExposureUTC {
				let jd = JulianDate.fromDate(date: start)
				return sunSet.jd <= jd.jd && sunRise.jd >= jd.jd
			}
			return false
		})
		
		return Group {
			
			ForEach(self.data.session.filterColors) { fc in
				CapturedShape(sessionImages: images.filter({ $0.filter == fc.filterName }),
							  sunSet: sunSet, sunRise: sunRise)
					.fillColor(fc.color)
			}
		}
	}
	
	
	func getMeridianFlipShape(sunSet: JulianDate, sunRise: JulianDate) -> some View {
		
		let images = self.data.session.filteredImages.filter({
			if let start = $0.startExposureUTC {
				let jd = JulianDate.fromDate(date: start)
				return sunSet.jd <= jd.jd && sunRise.jd >= jd.jd
			}
			return false
		})
		
		return Group {
			
			let east = images.filter({ $0.pierSide == "East" })
			let west = images.filter({ $0.pierSide == "West"})
			if east.isNotEmpty && west.isNotEmpty {
				MeridianFlipShape(eastImages: east, westImages: west, sunSet: sunSet, sunRise: sunRise)
					.fillColor(Color.gray.opacity(0.25))
			}
		}
	}
	
	
	func getAutoFocusShape(sunSet: JulianDate, sunRise: JulianDate) -> some View {
		//let setTime = sunSet.toDateTime()
		//let riseTime = sunRise.toDateTime()
		//let tz = self.data.session.currentTimeZone
		let dt = sunRise.jd - sunSet.jd
		
		return Canvas { context, size in
			let rect = CGRect(origin: .zero, size: size)
			let sx = rect.width / dt
			
			let afRuns = self.data.session.autoFocuserRuns
				.filter({ $0.isSession(self.date) })
				.map({ AFTime(begin: JulianDate.fromDate(date: $0.start), end: JulianDate.fromDate(date: $0.end), autoFocus: $0) })
			
			for af in afRuns {
				
				var path = Path()
				let x1 = (af.begin.jd - sunSet.jd) * sx
				let x2 = (af.end.jd - sunSet.jd) * sx
				
				let afRect = CGRect(x: x1, y: rect.minY, width: x2-x1, height: rect.height)
				path.addRect(afRect)
				context.fill(path, with: .color(Color.green.opacity(0.3)))
				
				let text1 = Text("AutoFocus")
					.font(Font.callout.bold())
				let text2 = Text("\(af.autoFocus.finalHFR, specifier: "%.2f")")
					.font(Font.caption.bold())
				
				var resolvedText1 = context.resolve(text1)
				resolvedText1.shading = .color(Color.green)
				let text1Size = resolvedText1.measure(in: size)
				
				var resolvedText2 = context.resolve(text2)
				resolvedText2.shading = .color(Color.green)
				let text2Size = resolvedText2.measure(in: size)
				
				let text1Center = CGPoint(x: afRect.center.x, y: rect.maxY - 60)
				let afText1Rect = CGRect.from(center: text1Center, size: text1Size)
				let text2Center = CGPoint(x: text1Center.x, y: text1Center.y + text1Size.height + 2)
				let afText2Rect = CGRect.from(center: text2Center, size: text2Size)
				
				context.draw(resolvedText1, in: afText1Rect)
				context.draw(resolvedText2, in: afText2Rect)
			}
			
		}
		
	}
	
	
    var body: some View {
		GeometryReader { g in
			ZStack {
				getGradient()
				
				HeightLineShape()
					.strokeColor(Color.gray, lineWidth: 0.5)
				
				getHeightLables()
				
				if let sunSet = self.data.sunSet, let sunRise = self.data.sunRise {
					
					TimeLineShape(sunSet: sunSet, sunRise: sunRise)
						.strokeColor(Color.gray, lineWidth: 0.5)
					
					TimeLineLabels(sunSet: sunSet, sunRise: sunRise, geometry: g)
					
					HeightTimeShape(altitudes: self.data.sunAltitudes, sunSet: sunSet, sunRise: sunRise)
						.strokeColor(Color.yellow, lineWidth: 2.0)
					
					HeightTimeShape(altitudes: self.data.moonAltitudes, sunSet: sunSet, sunRise: sunRise)
						.strokeColor(Color.gray, lineWidth: 2.0)
					
					getMeridianFlipShape(sunSet: sunSet, sunRise: sunRise)
					
					getAutoFocusShape(sunSet: sunSet, sunRise: sunRise)
					
					getCapturedShape(sunSet: sunSet, sunRise: sunRise)
					
					if let selectedImageIndex {
						
						if let selectedImagePos {
							Circle().frame(width: 8, height: 8)
								.foregroundColor(Color.orange)
								.position(selectedImagePos)
							
							let image = data.session.filteredImages[selectedImageIndex]
							
							VStack(alignment: .leading) {
								Text(image.ExposureBeginTime).font(.headline).foregroundColor(Color.white)
								if self.data.session.filterColors.count > 1,
									let fc = self.data.session.filterColors.first(where: { $0.filterName == image.filter}) {
									Text("Filter: \(image.filter)").font(.headline)
										.foregroundColor(fc.color)
								}
								
								Text(image.Altitude).font(.headline).foregroundColor(Color.white)
								Text(image.FileName).font(.headline).foregroundColor(Color.white)
							}
							.padding()
							.background(.ultraThinMaterial).cornerRadius(8)
							.position(CGPoint(x: selectedImagePos.x, y: g.size.height - g.size.height/4))
							.transition(.opacity)
							
						} else if let imagePos = self.data.findPosition(of: selectedImageIndex, in: g.frame(in: .local)){
							Circle().frame(width: 8, height: 8)
								.foregroundColor(Color.orange)
								.position(imagePos)
							
							
							let image = data.session.filteredImages[selectedImageIndex]
							
							VStack(alignment: .leading) {
								Text(image.ExposureBeginTime).font(.headline).foregroundColor(Color.white)
								if self.data.session.filterColors.count > 1,
									let fc = self.data.session.filterColors.first(where: { $0.filterName == image.filter}) {
									Text("Filter: \(image.filter)").font(.headline)
										.foregroundColor(fc.color)
								}
								
								Text(image.Altitude).font(.headline).foregroundColor(Color.white)
								Text(image.FileName).font(.headline).foregroundColor(Color.white)
							}
							.padding()
							.background(.ultraThinMaterial).cornerRadius(8)
							.position(CGPoint(x: imagePos.x, y: g.size.height - g.size.height/4))
							.transition(.opacity)
						}
						
						
					}
				}
				
				VStack {
					HStack {
						Spacer()
						Image(systemName: "questionmark.circle")
							.resizable()
							.frame(width: 20, height: 20)
							.foregroundColor(Color.white(0.9))
							.zIndex(1)
							.contentShape(Circle())
							.onTapGesture {
								withAnimation(.easeInOut(duration: 0.2)) {
									showTip.toggle()
								}
							}
							.help("CapturingTimelineView_Help")
						
					}.padding()
					Spacer()
				}
				.overlay {
					if showTip {
						VStack {
							Spacer()
							
							ShowTipView(tip: InfoTip(tipText: "CapturingTimelineView_Help"))
								.transition(.asymmetric(insertion: .move(edge: .leading), removal: .move(edge: .trailing)))
							
						}
					}
				}
			}
			.onContinuousHover { hoverPhase in
				if !readonly {
				
					switch hoverPhase {
						case .active(let hoverLocation):
							if let (index, pos) = self.data.findImageIndex(at: hoverLocation, in: g.frame(in: .local)) {
								withAnimation(.easeOut(duration: 0.2)) {
									selectedImageIndex = index
									selectedImagePos = pos
								}
								
							} else {
								withAnimation(.easeOut(duration: 0.2)) {
									selectedImageIndex = nil
									selectedImagePos = nil
								}
								
							}
						case .ended:
							withAnimation(.easeOut(duration: 0.2)) {
								selectedImageIndex = nil
								selectedImagePos = nil
							}
							
					}
				}
			}
		}
		.cornerRadius(8)
    }
}

