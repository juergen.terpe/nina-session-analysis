//
//  CorrectionsEastWestGraph.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 14.04.24.
//

import SwiftUI
import Charts

@Observable
class EastWestGraphModel {
	var west: Float = 0.0
	var east: Float = 0.0
	var avg: Float = 0.0
	var maxValue: Float = 100.0
	
	
	func update(session: GuidingData) async {
		var east: Float = 0.0
		var west: Float = 0.0
		var avg: Float = 0.0
		
		for frame in session.getGuideGraphDataWithoutDithering() {
			if frame.pulseDEC > 0.0 {
				west += frame.pulseDEC
			} else if frame.pulseDEC < 0.0 {
				east += frame.pulseDEC
			}
			
			avg += frame.pulseDEC
		}
		
		let e = east
		let w = west
		let a = avg
		
		let maxV = max(Double(abs(east)), Double(west)) * 1.2
		
		await MainActor.run {
			self.east = e
			self.west = w
			self.avg = a
			self.maxValue = Float(maxV)
		}
	}
}



struct CorrectionsEastWestGraph: View {
	let session: GuidingData
	@State private var model = EastWestGraphModel()
		
	var body: some View {
		VStack {
			Text("East/West Distribution (ms)").font(.headline)
			
			Chart {
				RectangleMark(xStart: .value("East", model.east), xEnd: .value("East", 0.0), y: .value("Y",0.5), height: .fixed(20))
					.foregroundStyle(Color.cyan.gradient)
				
				RectangleMark(xStart: .value("West", model.west), xEnd: .value("West", 0.0), y: .value("Y",0.5), height: .fixed(20))
					.foregroundStyle(Color.mint.gradient)
				
				RectangleMark(xStart: .value("Avg", model.avg), xEnd: .value("Avg", 0.0), y: .value("Y",0.5), height: .fixed(20))
					.foregroundStyle(Color.blue)
				
			}
			.chartXScale(domain: -model.maxValue...model.maxValue)
			.chartYScale(domain: 0...2)
			.chartYAxis(.hidden)
			
			.frame(height: 50)
			.transition(.blurReplace)
		}
		.task {
			await model.update(session: session)
		}
		.onChange(of: self.session.id) { _, _ in
			Task {
				await model.update(session: self.session)
			}
		}
	}
}
