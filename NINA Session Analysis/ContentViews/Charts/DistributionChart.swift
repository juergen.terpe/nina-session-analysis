//
//  DistributionChart.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 01.04.24.
//

import SwiftUI
import Charts

struct BinnedDataValue: Identifiable {
	let id = UUID().uuidString
	let index: Int
	let range: ChartBinRange<Float>
	let frequency: Int
	var animated: Bool = false
}

func normalDistribution(x: Float, mean: Float, stdDev: Float) -> Float {
	let sqrt2PI: Float = 2.5066282746
	
	let a = exp( -1.0 * pow(x-mean, 2.0) / ( 2.0 * pow(stdDev,2.0) ) )
	let b = stdDev * sqrt2PI
	return a / b
}


@Observable
class BinnedData {
	private(set) var session: SessionData
	var data: [BinnedDataValue] = []
	private(set) var values: [Float] = []
	private(set) var mean: Float? = nil
	private(set) var stdDev: Float? = nil
	private(set) var item: GraphItem
	private(set) var guidingId: GuidingData.ID?
	private let count: Int
	private(set) var minValue: Float = 0.0
	private(set) var maxValue: Float = 1.0
	private(set) var id: UUID
	
	init(session: SessionData, item: GraphItem, count: Int) {
		self.session = session
		self.item = item
		self.count = count
		self.id = UUID()
		Task {
			await self.update()
		}
		
	}
	
	init(session: SessionData, item: GraphItem, guidingId: GuidingData.ID, count: Int) {
		self.session = session
		self.item = item
		self.count = count
		self.guidingId = guidingId
		self.id = UUID()
		Task {
			await self.update()
		}
	}
	
	init(session: SessionData, item: GraphItem) {
		self.session = session
		self.item = item
		self.count = 20
		self.id = UUID()
	}
	
	
	func setRange(for value: Float) {
		for d in self.data {
			if d.range.lowerBound <= value && d.range.upperBound >= value {
				self.selectedRange = d.range
				return
			}
		}
	}
	
	var selectedRange: ChartBinRange<Float>?
	
	var selectedImages: [SessionImage] {
		if let selectedRange = self.selectedRange {
			var images = [SessionImage]()
			for image in self.session.filteredImages {
				if let value = image.getValue(self.item) {
					if selectedRange.lowerBound < value && selectedRange.upperBound > value {
						images.append(image)
					}
				}
			}
			return images
		}
		return []
	}
	
	func updateSelectedImages() async {
		let middle = self.selectedRange != nil ? (self.selectedRange!.lowerBound + self.selectedRange!.upperBound)/2.0 : nil
		await update()
		self.session.onContentUpdated()
		
		if middle != nil {
			setRange(for: middle!)
		}
	}
	
	func update() async {
		let sessionData = getData()
		
		if sessionData.count > 0 {
			var mn: Float?
			var mx: Float?
			var sum: Float = 0.0
			
			for d in sessionData {
				mn = mn != nil ? min(mn!, d) : d
				mx = mx != nil ? max(mx!, d) : d
				
				sum += d
			}
			let delta = 0.05 * (mx! - mn!)
			let mean = sum / Float(sessionData.count)
			let stdDev = sessionData.map { abs($0 - mean) }.reduce(0.0, +) / Float(sessionData.count)
			
			self.minValue = mn! - delta
			self.maxValue = mx! + delta
			
			let bins = NumberBins(
				data: sessionData,
				desiredCount: self.count
			)
			
			let groups = Dictionary(
				grouping: sessionData,
				by: bins.index
			)
			
			let data = groups.map { key, values in
				return BinnedDataValue(
					index: key,
					range: bins[key],
					frequency: values.count
				)
			}
			
			await MainActor.run {
				
				self.data = data
				self.mean = mean
				self.stdDev = stdDev
				self.values = sessionData
			}
			
		}
		else {
			
			await MainActor.run {
				self.data = []
				self.mean = nil
				self.values = []
				self.stdDev = nil
			}
		}
	}
	
	func update(for guidingId: GuidingData.ID) async {
		let sessionData = getData(for: guidingId)
		
		
		if sessionData.count > 0 {
			var mn: Float?
			var mx: Float?
			var sum: Float = 0.0
			
			for d in sessionData {
				mn = mn != nil ? min(mn!, d) : d
				mx = mx != nil ? max(mx!, d) : d
				
				sum += d
			}
			let delta = 0.05 * (mx! - mn!)
			let mean = sum / Float(sessionData.count)
			let stdDev = sessionData.map { abs($0 - mean) }.reduce(0.0, +) / Float(sessionData.count)
			
			self.minValue = mn! - delta
			self.maxValue = mx! + delta
			
			let bins = NumberBins(
				data: sessionData,
				desiredCount: self.count
			)
			
			let groups = Dictionary(
				grouping: sessionData,
				by: bins.index
			)
			
			let data = groups.map { key, values in
				return BinnedDataValue(
					index: key,
					range: bins[key],
					frequency: values.count
				)
			}
			
			await MainActor.run {
				
				self.data = data
				self.mean = mean
				self.stdDev = stdDev
				self.values = sessionData
				self.guidingId = guidingId
				self.id = UUID()
			}
			
		}
		else {
			
			await MainActor.run {
				self.data = []
				self.mean = nil
				self.values = []
				self.stdDev = nil
				self.id = UUID()
			}
		}
	}
	
	private func getData() -> [Float] {
		var result = [Float]()
		
		for image in self.session.filteredImages {
			if let value = image.getValue(item), !value.isNaN {
				result.append(value)
			}
		}
		return result
	}
	
	private func getData(for guidingId: GuidingData.ID) -> [Float] {
		var result = [Float]()
		
		for image in self.session.getImages(for: guidingId) {
			if let value = image.getValue(item), !value.isNaN {
				result.append(value)
			}
		}
		return result
	}
}


struct DistributionChart: View {
	@State var binnedData: BinnedData
	@State var showTip = false
	let showNormalDist: Bool
	@Environment(\.colorScheme) var colorScheme
	
	func GetTipText(_ item: GraphItem) -> LocalizedStringKey {
		switch (item) {
			case .MEAN:
				return LocalizedStringKey("DISTRIBUTION_MEAN_HELP")
			case .MEDIAN:
				return LocalizedStringKey("DISTRIBUTION_MEDIAN_HELP")
			case .RMS:
				return LocalizedStringKey("DISTRIBUTION_RMS_HELP")
			case .ECCENTRICITY:
				return LocalizedStringKey("DISTRIBUTION_ECCENTRICITY_HELP")
			case .FWHM:
				return LocalizedStringKey("DISTRIBUTION_FWHM_HELP")
			case .HFR:
				return LocalizedStringKey("DISTRIBUTION_HFR_HELP")
			case .AIRMASS:
				return LocalizedStringKey("DISTRIBUTION_AIRMASS_HELP")
			case .NUMSTARS:
				return LocalizedStringKey("DISTRIBUTION_NUMSTARS_HELP")
			default:
				return LocalizedStringKey("No Information available yet!")
		}
		
	}
	
	@available(macOS 15.0, *)
	func showNormalDistribution(mean: Float, stdDev: Float, maxValue: Float) -> some ChartContent {
		LinePlot(
				x: "Value",
				y: "Probability"
				
		) { x in
			Double(normalDistribution(x: Float(x), mean: mean, stdDev: stdDev) * maxValue)
		}
		.lineStyle(StrokeStyle(lineWidth: 2.0))
		.foregroundStyle(colorScheme == .dark ? .lightYellow : .black)
	}
	
	
    var body: some View {
		
		VStack {
			HStack {
				Text(binnedData.item.rawValue).kerning(1.5)
				Spacer()
				Image(systemName: "questionmark.circle")
					.resizable()
					.frame(width: 20, height: 20)
					.foregroundColor(Color.primary)
					.zIndex(1)
					.contentShape(Circle())
					.onTapGesture {
						withAnimation(.easeInOut(duration: 0.2)) {
							showTip.toggle()
						}
					}
			}
			Chart {
				ForEach(self.binnedData.data, id: \.index) { element in
					BarMark(
						x: .value(
							binnedData.item.rawValue,
							element.range
						),
						y: .value(
							"Frequency",
							element.animated ? element.frequency : 0
						),
						stacking: .unstacked
					)
					.foregroundStyle(Color.blue.gradient)
					
					
					if let selectedRange = self.binnedData.selectedRange,
					   selectedRange.lowerBound <= element.range.lowerBound,
					   selectedRange.upperBound >= element.range.upperBound {
						BarMark(
							x: .value(
								binnedData.item.name,
								element.range
							),
							y: .value(
								"Frequency",
								element.frequency
							),
							stacking: .unstacked
						)
						.foregroundStyle(Color.orange)
						.accessibilityHidden(true)
						.annotation(position: .top, overflowResolution: AnnotationOverflowResolution(x: .disabled, y: .disabled)) {
							VStack(alignment: .center) {
								Text("\(element.range.lowerBound, specifier: "%.2f") - \(element.range.upperBound, specifier: "%.2f")")
								Text("\(element.frequency)")
							}
							.font(.caption)
							.padding(4)
							.background(.ultraThinMaterial).cornerRadius(8)
							.zIndex(100)
						}
					}
				}
				
				
				if let mean = self.binnedData.mean, let stdDev = binnedData.stdDev,
				   (showNormalDist || self.binnedData.selectedRange != nil) { // show only when range selected
					
					if #available(macOS 15, *) {
						showNormalDistribution(mean: mean, stdDev: stdDev, maxValue: binnedData.maxValue)
					}
				}
				
			}
			.chartXScale(domain: self.binnedData.minValue...self.binnedData.maxValue)
			.chartPlotStyle { plotContent in
				plotContent
					.background(.chartBackground.gradient.opacity(0.5))
			}
			.chartOverlay { (chartProxy: ChartProxy) in
				GeometryReader { geometry in
					
					Rectangle().fill(.primary.opacity(0.01)).containerShape(.rect)
						.onContinuousHover { hoverPhase in
							switch hoverPhase {
								case .active(let hoverLocation):
									guard let plotFrame = chartProxy.plotFrame else {
										return
									}
									let startX = geometry[plotFrame].origin.x
									let currentX = hoverLocation.x - startX
									
									if let val = chartProxy.value(atX: currentX, as: Float.self) {
										self.binnedData.setRange(for: val)
									}
								case .ended:
									self.binnedData.selectedRange = nil
							}
						}
				}
			}
			.onAppear {
				for (index, _) in self.binnedData.data.enumerated() {
					DispatchQueue.main.asyncAfter(deadline: .now() + 0.025 * Double(index)) {
						withAnimation(.easeInOut(duration: 0.4)) {
							self.binnedData.data[index].animated = true
						}
					}
				}
			}
			.onChange(of: self.binnedData.id) { _, _ in
				for (index, _) in self.binnedData.data.enumerated() {
					DispatchQueue.main.asyncAfter(deadline: .now() + 0.025 * Double(index)) {
						withAnimation(.easeInOut(duration: 0.4)) {
							self.binnedData.data[index].animated = true
						}
					}
				}
			}
		}
		.overlay {
			if showTip {
				VStack {
					ShowTipView(tip: InfoTip(tipText: GetTipText(self.binnedData.item)))
						.transition(.asymmetric(insertion: .move(edge: .leading), removal: .move(edge: .trailing)))
					Spacer()
				}
			}
		}
		//.transition(.blurReplace)
		
    }
}


struct DistributionChartDetailView: View {
	@State var binnedData: BinnedData
	@Environment(\.colorScheme) var colorScheme
	@Environment(\.openWindow) private var openWindow
	@Environment(\.dismissWindow) private var dismissWindow
	
	@ViewBuilder
	func columnContent(for id: TableImageColumnId, in image: SessionImage) -> some View {
		Group {
			if id == .marking {
				Button {
					image.toggleMarked()
					
					self.binnedData.session.onContentUpdated()
				} label: {
					
					HStack {
						Spacer()
						Image(systemName: image.bad ? "checkmark.square" : "square")
							.imageScale(.large)
							.foregroundColor(image.bad ? .red : .green)
						Spacer()
					}
				}
				.buttonStyle(PlainButtonStyle())
				
			} else {
				Text(image.columnText(for: id))
					.foregroundColor(image.bad ? .badColor : image.columnColor(for: id))
			}
		}
	}
	
	@available(macOS 15.0, *)
	func showNormalDistribution(mean: Float, stdDev: Float, maxValue: Float) -> some ChartContent {
		LinePlot(
				x: "Value",
				y: "Probability"
		) { x in
			Double(normalDistribution(x: Float(x), mean: mean, stdDev: stdDev) * maxValue)
		}
		.lineStyle(StrokeStyle(lineWidth: 2.0))
		.foregroundStyle(colorScheme == .dark ? .lightYellow : .black)
	}
	
	@ViewBuilder
	func columnContextMenu( _ image: SessionImage) -> some View {
		if image.isFits {
			
			Button {
				dismissWindow(id: "InternalViewer")
				openWindow(id: "InternalViewer", value: image.id)
			} label: {
				Text("Show in Viewer")
			}
			
			if self.binnedData.session.quickFitsAvailable {
				Button {
					self.binnedData.session.openInQuickFits( image )
				} label: {
					Text("Open in QuickFits")
				}
			}
			
			if self.binnedData.session.fitsPreviewAvailable {
				Button {
					self.binnedData.session.openInFitsPreview( image )
				} label: {
					Text("Open in FITS Preview")
				}
			}
		}
		
		if self.binnedData.session.pixInsightAvailable {
			Button {
				self.binnedData.session.openInPixInsight( image )
			} label: {
				Text("Open in PixInsight")
			}
		}
		
		if self.binnedData.session.astapAvailable {
			Button {
				self.binnedData.session.openInAstap( image )
			} label: {
				Text("Open in Astap")
			}
		}
	}
	
	
	var body: some View {
		VStack {
			
			Chart {
				
				ForEach(self.binnedData.data, id: \.index) { element in
					BarMark(
						x: .value(
							binnedData.item.name,
							element.range
						),
						y: .value(
							"Frequency",
							element.frequency
						),
						stacking: .unstacked
					)
					.annotation {
						VStack(alignment: .center) {
							Text("\(element.range.lowerBound, specifier: "%.2f") - \(element.range.upperBound, specifier: "%.2f")")
							Text("\(element.frequency)")
						}
						.font(.footnote)
						.padding()
						.zIndex(100)
					}
					
					if let selectedRange = self.binnedData.selectedRange,
					   selectedRange.lowerBound <= element.range.lowerBound,
					   selectedRange.upperBound >= element.range.upperBound {
						BarMark(
							x: .value(
								binnedData.item.name,
								element.range
							),
							y: .value(
								"Frequency",
								element.frequency
							),
							stacking: .unstacked
						)
						.foregroundStyle(Color.orange)
						.accessibilityHidden(true)
					}
				}
					
				if let mean = self.binnedData.mean, let stdDev = binnedData.stdDev {
					if #available(macOS 15, *) {
						showNormalDistribution(mean: mean, stdDev: stdDev, maxValue: binnedData.maxValue)
					}
				}
			}
			.chartXScale(domain: self.binnedData.minValue...self.binnedData.maxValue)
			.chartOverlay { (chartProxy: ChartProxy) in
				GeometryReader { geometry in
					Rectangle().fill(.primary.opacity(0.01)).containerShape(.rect)
						.onTapGesture(coordinateSpace: .local) { location in
							guard let plotFrame = chartProxy.plotFrame else {
								return
							}
							let startX = geometry[plotFrame].origin.x
							let currentX = location.x - startX
							
							if let val = chartProxy.value(atX: currentX, as: Float.self) {
								self.binnedData.setRange(for: val)
							}
						}
					
				}
			}
			.padding()
			.background(Color.chartBackground.cornerRadius(12))
			
			HStack {
				Button {
					for image in self.binnedData.selectedImages {
						image.Mark()
					}
					Task {
						await self.binnedData.updateSelectedImages()
					}
					
				} label: {
					Text("Mark All").foregroundColor(Color.red)
				}
				

				Button {
					for image in self.binnedData.selectedImages {
						image.UnMark()
					}
					Task {
						await self.binnedData.updateSelectedImages()
					}
				} label: {
					Text("Unmark All").foregroundColor(Color.green)
				}
				
				
				Spacer()
			}
			
			Table(self.binnedData.selectedImages) {
				
				TableColumnForEach(self.binnedData.session.tableDataColumns) { column in
					TableColumn(column.name) { image in
						
						columnContent(for: column.id, in: image)
							.contentShape(Rectangle())
							.contextMenu {
								columnContextMenu(image)
							}
					}
				}
			}
			.frame(maxHeight: 400)
		}
		
		
	}
}
