//
//  AutoFocusChart.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 20.05.24.
//

import SwiftUI
import Charts

fileprivate struct ChartData: Identifiable {
	let id: UUID = UUID()
	let position: Float
	let hfr: Float
	let mad: Float
}

@Observable
fileprivate class AutoFocusChartData {
	var data: [ChartData] = []
	var initialPosition: Float?
	var initialHFR: Float?
	var finalPosition: Float?
	var finalHFR: Float?
	var minPosition: Float = 0
	var maxPosition: Float = 65536
	var avgMad: Float = 0.2
	
	fileprivate func update(_ autoFocus: AutoFocus) async {
		var result = [ChartData]()
		var minPos: Float? = nil
		var maxPos: Float? = nil
		
		var mads: Float = 0.0
		for fp in autoFocus.points {
			if minPos == nil {
				minPos = Float(fp.position)
			} else {
				minPos = min(minPos!, Float(fp.position))
			}
			if maxPos == nil {
				maxPos = Float(fp.position)
			} else {
				maxPos = max(maxPos!, Float(fp.position))
			}
			
			mads += fp.mad
			result.append(ChartData(position: Float(fp.position), hfr: fp.HFR, mad: fp.mad))
		}
		
		if result.count > 0 {
			
			let avgMad = mads / Float(result.count)
			
			let finalResult = result
			let minPosition = minPos ?? 0.0
			let maxPosition = maxPos ?? 65535.0
			
			await MainActor.run {
				self.data = finalResult
				self.minPosition = minPosition * 0.995
				self.maxPosition = maxPosition * 1.005
				self.avgMad = avgMad
				self.initialHFR = autoFocus.initialHFR
				self.initialPosition = Float(autoFocus.initialPosition)
				self.finalHFR = autoFocus.finalHFR
				self.finalPosition = Float(autoFocus.finalPosition)
			}
		}
	}
}

struct AutoFocusChart: View {
	let autoFocus: AutoFocus
	@State private var chartData: AutoFocusChartData = AutoFocusChartData()
	
    var body: some View {
		VStack {
			HStack {
				Text("Autofocus: \(self.autoFocus.start.formatted(date: .abbreviated, time: .shortened) )")
					.kerning(1.5)
				Spacer()
			}
			HStack {
				Text("\(self.autoFocus.initialHFR, specifier: "%.2f") -> \(self.autoFocus.finalHFR, specifier: "%.2f")")
					.foregroundColor(self.autoFocus.finalHFR > self.autoFocus.initialHFR + self.chartData.avgMad/2.0 ? .red : .green)
					.padding(.trailing)
				
				Text("\(ValueFormatter.formatTimeSpan(self.autoFocus.duration, timeStyle: DateComponentsFormatter.UnitsStyle.short) )")
					.font(.callout).fontWeight(.light)
					.foregroundColor(.secondary)
				Spacer()
			}
			.padding(.bottom)
			
			Chart {
				ForEach(self.chartData.data) {  data in
					
					LineMark(
						x: .value("Position", data.position),
						y: .value("HFR", data.hfr)
					)
					.interpolationMethod(.catmullRom)
					.foregroundStyle(Color.blue)
					.accessibilityHidden(true)
					
					RectangleMark(x: .value("Position", data.position),
								  yStart: .value("HFRMin", data.hfr - data.mad),
								  yEnd: .value("HFRMax", data.hfr + data.mad))
					.foregroundStyle(Color.green.opacity(0.2))
					.accessibilityHidden(true)
					
					PointMark(
						x: .value("Position", data.position),
						y: .value("HFR", data.hfr)
					)
					.foregroundStyle(Color.green)
					.accessibilityHidden(true)
					.annotation(position: .top, spacing: 16, overflowResolution: AnnotationOverflowResolution(x: .disabled, y: .disabled)) {
						VStack {
							Text("\(Int(data.position))")
							Text("\(data.hfr, specifier: "%.2f")")
						}
						.font(.caption)
						.foregroundStyle(Color.green.opacity(0.75))
					}
				}
				
				
				if let fp = self.chartData.finalPosition, let fhfr = self.chartData.finalHFR {
					PointMark(
						x: .value("Position", fp),
						y: .value("HFR", fhfr)
					)
					.foregroundStyle(Color.yellow.opacity(0.85))
					.accessibilityHidden(true)
					.annotation(position: .bottom, spacing: 16, overflowResolution: AnnotationOverflowResolution(x: .disabled, y: .disabled)) {
						VStack {
							Text("\(Int(fp))")
							Text("\(fhfr, specifier: "%.2f")")
						}
						.font(.caption)
						.foregroundStyle(Color.yellow.opacity(0.75))
					}
				}
			}
			.chartXScale(domain: self.chartData.minPosition...self.chartData.maxPosition)
			.chartYAxis {
				AxisMarks(position: .leading)
				AxisMarks(position: .trailing)
			}
			.chartPlotStyle { plotContent in
				plotContent
					.background(.chartBackground.gradient.opacity(0.5))
			}
		}
		.task {
			await self.chartData.update(self.autoFocus)
		}
    }
}

