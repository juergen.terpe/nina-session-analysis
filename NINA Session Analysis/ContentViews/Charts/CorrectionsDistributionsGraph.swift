//
//  CorrectionsDistributionsGraph.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 14.04.24.
//

import SwiftUI
import Charts

enum CorrectionPulses: Int, Identifiable {
	var id: Int {
		self.rawValue
	}
	
	case north = 1
	case south = 2
	case east = 3
	case west = 4
}

struct CorrectionBins {
	let index: Int
	let range: ChartBinRange<Float>
	let frequency: Int
	var animated: Bool = false
}

@Observable
class CorrectionsDistributionsModel {
	var pulses: [CorrectionBins] = []
	
	
	func update(session: GuidingData, item: CorrectionPulses) async {
		var result = [Float]()
		for frame in session.getGuideGraphDataWithoutDithering() {
			switch item {
				case .north:
					if frame.pulseDEC > 0.0 {
						result.append(frame.pulseDEC)
					}
				case .south:
					if frame.pulseDEC < 0.0 {
						result.append(abs(frame.pulseDEC))
					}
				case .east:
					if frame.pulseRA < 0.0 {
						result.append(abs(frame.pulseRA))
					}
				case .west:
					if frame.pulseRA > 0.0 {
						result.append(abs(frame.pulseRA))
					}
			}
		}
		
		let pulses = getBinnedData(data: result)
		await MainActor.run {
			self.pulses = pulses
		}
	}
	
	func getBinnedData(data: [Float]) -> [CorrectionBins] {
		let bins = NumberBins(
			data: data,
			desiredCount: 30
		)
		
		let groups = Dictionary(
			grouping: data,
			by: bins.index
		)
		
		let preparedData = groups.map { key, values in
			return CorrectionBins(
				index: key,
				range: bins[key],
				frequency: values.count
			)
		}
		
		return preparedData
	}
	
}

struct CorrectionsDistributionsGraph: View {
	let session: GuidingData
	let item: CorrectionPulses
	var color: Color = Color.blue
	@State private var model = CorrectionsDistributionsModel()
	
	var headLine: LocalizedStringKey {
		switch item {
			case .north: return LocalizedStringKey("Guiding corrections to north (ms)")
			case .south: return LocalizedStringKey("Guiding corrections to south (ms)")
			case .east: return LocalizedStringKey("Guiding corrections to east (ms)")
			case .west: return LocalizedStringKey("Guiding corrections to west (ms)")
		}
	}
	
	var body: some View {
		VStack {
			Text(headLine).font(.headline)
			
			Chart(self.model.pulses, id: \.index) { element in
				BarMark(
					x: .value("Pulses", element.range),
					y: .value("Frequency", element.animated ? element.frequency : 0)
				)
				.foregroundStyle(color.gradient)
			}
			.chartXScale(domain: .automatic(includesZero: false))
			.chartPlotStyle { plotContent in
				plotContent
					.background(.chartBackground.gradient.opacity(0.5))
			}
			.transition(.blurReplace)
		}
		.task {
			await model.update(session: self.session, item: self.item)
			
			for (index, _) in self.model.pulses.enumerated() {
				DispatchQueue.main.asyncAfter(deadline: .now() + 0.025 * Double(index)) {
					withAnimation(.easeInOut(duration: 0.4)) {
						self.model.pulses[index].animated = true
					}
				}
			}
		}
		.onChange(of: self.session.id) { _, _ in
			Task {
				await model.update(session: self.session, item: self.item)
				
				for (index, _) in self.model.pulses.enumerated() {
					DispatchQueue.main.asyncAfter(deadline: .now() + 0.025 * Double(index)) {
						withAnimation(.easeInOut(duration: 0.4)) {
							self.model.pulses[index].animated = true
						}
					}
				}
			}
		}
	}
	
}


