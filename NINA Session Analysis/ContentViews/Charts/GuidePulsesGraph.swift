//
//  GuidePulsesGraph.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 13.04.24.
//

import SwiftUI
import Charts
import PureSwiftUI

@Observable
class GuidePulsesGraphModel {
	var pulses: [(Float, Float)] = []
	var east: [Range<Int>] = []
	var west: [Range<Int>] = []
	
	func update(sessionImages: [SessionImage], item: GraphItem) async {
		let data = getData(sessionImages: sessionImages, item: item)
		let (east, west) = getEastWestRanges(sessionImages: sessionImages)
		 
		await MainActor.run {
			self.pulses = data
			self.east = east
			self.west = west
		}
	}
	
	private func getData(sessionImages: [SessionImage], item: GraphItem) -> [(Float, Float)] {
		var result = [(Float, Float)]()
		for image in sessionImages {
			if let value = image.getValues(item) {
				result.append(value)
			}
		}
		return result
	}

	private func getEastWestRanges(sessionImages: [SessionImage]) -> ([Range<Int>], [Range<Int>]) {
		var east: [Range<Int>] = []
		var west: [Range<Int>] = []
		
		var eastStart: Int?
		var westStart: Int?
		var currentIndex: Int?
		for (index, image) in sessionImages.enumerated() {
			if eastStart == nil && image.pierSide == "East" {
				if westStart != nil {
					west.append(westStart!..<index)
					westStart = nil
				}
				
				eastStart = index
			}
			if westStart == nil && image.pierSide == "West" {
				if eastStart != nil {
					east.append(eastStart!..<index)
					eastStart = nil
				}
				westStart = index
			}
			currentIndex = index
		}
		
		if let current = currentIndex {
			if eastStart != nil {
				east.append(eastStart!..<current+1)
			} else if westStart != nil {
				west.append(westStart!..<current+1)
			}
		}
		
		return (east, west)
	}
}


struct GuidePulsesGraphFor: View {
	var sessionImages: [SessionImage]
	let item: GraphItem
	let height: CGFloat
	@State private var model = GuidePulsesGraphModel()
	@State private var showTip = false
	
	
	func keyY(_ index: Int) -> LocalizedStringKey {
		if item == GraphItem.PulsesRA {
			return index == 0 ? LocalizedStringKey("E") : LocalizedStringKey("W")
		} else {
			return index == 0 ? LocalizedStringKey("N") : LocalizedStringKey("S")
		}
	}
	
	func color(_ index: Int) -> Color {
		if item == GraphItem.PulsesRA {
			return index == 0 ? Color.cyan : Color.green
		} else {
			return index == 0 ? Color.orange : Color.yellow
		}
	}
	
	func sumLeft() -> Float {
		return model.pulses.map { $0.0 }.reduce(0, +)
	}

	func sumRight() -> Float {
		return model.pulses.map { $0.1 }.reduce(0, +)
	}
	
	func getAvgData() -> Float {
		return abs(sumRight()) - abs(sumLeft())
	}
	
	var maxValue: CGFloat {
		let l = sumLeft()
		let r = sumRight()
		
		return max(CGFloat(abs(l)), CGFloat(abs(r)))
	}
	
	var summary: some View {
		Chart {
			
			if item == .PulsesRA {
				RectangleMark(xStart: .value(keyY(0), -sumLeft()), xEnd: .value(keyY(0), 0), y: .value("East", 1))
					.foregroundStyle(color(0))
				
				RectangleMark(xStart: .value(keyY(1), -sumRight()), xEnd: .value(keyY(1), 0.0), y: .value("West", 1))
					.foregroundStyle(color(1))
				
				RectangleMark(xStart: .value("Avg", getAvgData()), xEnd: .value("Avg", 0.0), y: .value("Y", 1))
					.foregroundStyle(Color.red)
			} else {
				
				RectangleMark(xStart: .value(keyY(0), -sumLeft()), xEnd: .value(keyY(0), 0.0), y: .value("North", 1))
					.foregroundStyle(color(0))
				
				RectangleMark(xStart: .value(keyY(1), -sumRight()), xEnd: .value(keyY(1), 0.0), y: .value("South", 1))
					.foregroundStyle(color(1))
				
				RectangleMark(xStart: .value("Avg", getAvgData()), xEnd: .value("Avg", 0.0), y: .value("Y", 1))
					.foregroundStyle(Color.red)
			}
		}
		.chartXScale(domain: -maxValue...maxValue)
		.chartLegend(.visible)
		.frame(height: 80)
		.padding(.bottom, 24)
		.transition(.blurReplace)
		.overlay {
			VStack {
				Spacer()
				
				if item == .PulsesRA {
					HStack {
						
						Rectangle()
							.frame(width: 50, height: 8)
							.foregroundStyle(color(0))
						
						Text("East")
							.font(.body)
							.kerning(1.5)
							.fontWeight(.semibold)
							.foregroundStyle(color(0))
						Spacer()
						
						Rectangle()
							.frame(width: 50, height: 8)
							.foregroundStyle(color(1))
						
						Text("West")
							.font(.body)
							.kerning(1.5)
							.fontWeight(.semibold)
							.foregroundStyle(color(1))
					}
					.padding(.horizontal, 32)
					
				} else {
					HStack {
						
						Rectangle()
							.frame(width: 50, height: 8)
							.foregroundStyle(color(0))
						
						Text("North")
							.font(.body)
							.kerning(1.5)
							.fontWeight(.semibold)
							.foregroundStyle(color(0))
						Spacer()
						
						Rectangle()
							.frame(width: 50, height: 8)
							.foregroundStyle(color(1))
						
						Text("South")
							.font(.body)
							.kerning(1.5)
							.fontWeight(.semibold)
							.foregroundStyle(color(1))
					}
					.padding(.horizontal, 32)
				}
			}
		}
	}
	
	var header: some View {
		HStack {
			(Text(item.name) + Text(" ") + Text(item.unitText))
				.kerning(1.5)
				.padding(.leading)
				.padding(.trailing)
							
			Text("Pierside")
				.kerning(1.5)
				.foregroundStyle(Color.secondary)
				.padding(.leading)
				.padding(.vertical, 4)
			
			Text("West")
				.kerning(1.5)
				.foregroundStyle(Color.secondary)
				.padding(.horizontal)
				.padding(.vertical, 4)
				.background(Color.green.opacity(0.2).cornerRadius(8))
			
			Text("East")
				.kerning(1.5)
				.foregroundStyle(Color.secondary)
				.padding(.horizontal)
				.padding(.vertical, 4)
				.background(Color.blue.opacity(0.2).cornerRadius(8))
			
			Spacer()
							
			Image(systemName: "questionmark.circle")
				.resizable()
				.frame(width: 20, height: 20)
				.foregroundColor(Color.primary)
				.zIndex(1)
				.contentShape(Circle())
				.onTapGesture {
					withAnimation(.easeInOut(duration: 0.2)) {
						showTip.toggle()
					}
				}
				.help(item == GraphItem.PulsesRA ? "PulseGraphRA_Help" : "PulseGraphDEC_Help")
		}
	}
	
	
    var body: some View {
		VStack {
			header
			
			VStack {
				
				if model.pulses.count > 0 {
					
					Chart {
						
						ForEach(model.east, id: \.self.lowerBound) { range in
							RectangleMark(xStart: .value("East", range.lowerBound), xEnd: .value("East", range.upperBound),
										  y: .value("East", 1), height: .fixed(2*height))
								.foregroundStyle(Color.blue.opacity(0.2))
								
						}
						
						ForEach(model.west, id: \.self.lowerBound) { range in
							RectangleMark(xStart: .value("West", range.lowerBound), xEnd: .value("West", range.upperBound),
										  y: .value("West", 1), height: .fixed(2*height))
							.foregroundStyle(Color.green.opacity(0.2))
							.annotation(position: .top,
										overflowResolution: AnnotationOverflowResolution(x: .disabled, y: .disabled)) {
								Text("West").font(.footnote).foregroundStyle(Color.green)
							}
						}
						
						ForEach(Array(model.pulses.enumerated()), id: \.offset) { index, value in
							
							BarMark(
								x: .value(
									"\(index)",
									index
								),
								y: .value(
									keyY(0),
									value.0
								),
								stacking: .unstacked
							)
							.foregroundStyle(color(0).gradient)
							
							BarMark(
								x: .value(
									"\(index)",
									index
								),
								y: .value(
									keyY(1),
									value.1
								),
								stacking: .unstacked
							)
							.foregroundStyle(color(1).gradient)
							
							BarMark(
								x: .value(
									"Diff \(index)",
									index
								),
								y: .value(
									"Difference",
									abs(value.0) - abs(value.1)
								),
								stacking: .unstacked
							)
							.foregroundStyle(.red)
							
						}
					}
					.chartXScale(domain: 0...model.pulses.count)
					.chartScrollableAxes(.horizontal)
					.chartXVisibleDomain(length: min(model.pulses.count, 100))
					.chartYAxis {
						AxisMarks(position: .leading)
						AxisMarks(position: .trailing)
					}
					.chartPlotStyle { plotContent in
						plotContent
							.background(.chartBackground.gradient.opacity(0.5))
							
					}
					.frame(height: height)
					.padding(.horizontal)
					.transition(.blurReplace)
					.padding(.bottom)
					
					summary
				}
			}
			.overlay {
				if showTip {
					VStack {
						ShowTipView(tip: InfoTip(tipText: item == GraphItem.PulsesRA ? "PulseGraphRA_Help" : "PulseGraphDEC_Help"))
							.transition(.asymmetric(insertion: .move(edge: .leading), removal: .move(edge: .trailing)))
						Spacer()
					}
				}
			}
			.task {
				await self.model.update(sessionImages: self.sessionImages, item: self.item)
			}
			
			
		}
		
    }
}

