//
//  MergedDataChart.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 05.05.24.
//

import SwiftUI
import Charts

fileprivate struct ChartData: Identifiable {
	var id: UUID
	let index: Int
	let value1: Float
	let value2: Float
	let bad: Bool
	let filter: String
	let target: String
	let session: String
}

@Observable
fileprivate class MergedChartData {
	var session: SessionData? = nil
	var filteredImages: [SessionImage] = []
	var data: [ChartData] = []
	var filters: [String] = []
	var targets: [String] = []
	var sessions: [String] = []
	var minIndex: Int = 0
	var maxIndex: Int = 0
	var minValue1: Float = 0.0
	var maxValue1: Float = 1.0
	var minValue2: Float = 0.0
	var maxValue2: Float = 1.0
	var minYValue: Float = 0.0
	var maxYValue: Float = 1.0
	
	let colors = [Color.cyan, Color.indigo, Color.green, Color.purple, Color.mint, Color.pink, Color.brown]
		
	func update(_ item1: GraphItem, _ item2: GraphItem, session: SessionData) async {
		
		var result = [ChartData]()
		
		let images = session.filteredImages.filter({ $0.SessionDate != nil })
		
		var filters = Set<String>()
		var targets = Set<String>()
		var sessions = Set<String>()
		
		var min1: Float?
		var max1: Float?
		var min2: Float?
		var max2: Float?
		
		for image in images {
			if let value1 = image.getValue(item1), let value2 = image.getValue(item2) {
				result.append(ChartData(id: image.id, index: image.index, value1: value1,
										value2: value2, bad: image.bad,
										filter: image.filter, target: image.target, session: image.SessionDate!.text))
				
				if min1 == nil {
					min1 = value1
				} else {
					min1 = min(min1!, value1)
				}

				if max1 == nil {
					max1 = value1
				} else {
					max1 = max(max1!, value1)
				}
				
				if min2 == nil {
					min2 = value2
				} else {
					min2 = min(min2!, value2)
				}

				if max2 == nil {
					max2 = value2
				} else {
					max2 = max(max2!, value2)
				}
				
				filters.insert(image.filter)
				targets.insert(image.target)
				sessions.insert(image.SessionDate!.text)
			}
		}
		
		let minIdx = result.map({ $0.index }).min() ?? 0
		let maxIdx = result.map({ $0.index }).max() ?? 0
		let filtersSorted = Array(filters).sorted()
		let targetsSorted = Array(targets).sorted()
		let sessionsSorted = Array(sessions).sorted()
		let resultingChartData = result
		let minValue1 = min1 ?? 0.0
		let maxValue1 = max1 ?? 1.0
		let minValue2 = min2 ?? 0.0
		let maxValue2 = max2 ?? 1.0
		
		let minY = min(minValue1, minValue2)
		let maxY = max(maxValue1, maxValue2)
		
		let minYValue: Float = 0.9 * (minY / maxY)
		let maxYValue: Float = 1.1
		
		
		await MainActor.run {
			self.filteredImages = images
			self.filters = filtersSorted
			self.targets = targetsSorted
			self.sessions = sessionsSorted
			self.data = resultingChartData
			self.session = session
			self.maxIndex = maxIdx
			self.minIndex = minIdx
			self.minValue1 = minValue1 * 0.8
			self.maxValue1 = maxValue1 * 1.5
			self.minValue2 = minValue2 * 0.8
			self.maxValue2 = maxValue2 * 1.5
			self.minYValue = minYValue
			self.maxYValue = maxYValue
		}
	}
	
	func getImage(at index: Int) -> SessionImage? {
		if let images = self.session?.filteredImages {
			return images.first(where: { $0.index == index })
		}
		return nil
	}
	
	func getValues(at index: Int) -> (Float, Float)? {
		if self.minIndex <= index && self.maxIndex >= index, let dp = self.data.first(where: { $0.index == index}) {
			return (dp.value1, dp.value2)
		}
		return nil
	}
	
	func hasValue(for index: Int) -> Bool {
		if getValues(at: index) != nil {
			return true
		}
		return false
	}
	
	fileprivate func getColor(_ data: ChartData, color: Color) -> Color {
		if data.bad {
			return Color.badColor
		} else {
			if self.filters.count > 1 {
				if let fc = self.session?.filterColors.first(where: { $0.filterName == data.filter }) {
					return fc.color
				}
			}
			else if self.targets.count > 1 {
				if let index = self.targets.firstIndex(where: { $0 == data.target }) {
					return self.colors[index % self.colors.count]
				}
			}
			else if self.sessions.count > 1 {
				if let index = self.sessions.firstIndex(where: { $0 == data.session }) {
					return self.colors[index % self.colors.count]
				}
			}
			
			return color
		}
		
	}
}



struct MergedDataChart: View {
	
	@State var session: SessionData
	let item1: GraphItem
	let item2: GraphItem
	let color1: Color
	let color2: Color
	
	@State private var chartData: MergedChartData = MergedChartData()
	@State private var appeared: Bool = false
	@State private var selectedIndex: Int?
	@State private var showTip: Bool = false 
	let strideBy = 6
	
	private func getFormattedValue(_ image: SessionImage, item: GraphItem) -> String {
		if let val = image.getValue(item) {
			switch item {
				case .ADUMAX, .ADUMIN, .MEDIAN, .NUMSTARS:
					return String(format: "%.0f", val)
				case .DEWPOINT, .AMBTEMP, .CLOUDCVR, .FOCPOS, .DITHERDECPX, .DITHERPX, .DITHERRAPX,
					 .HUMIDITY, .MEAN, .MOONALT, .MOONANGL, .SUNALT, .SUNANGLE, .PRESSURE:
					return String(format: "%.1f", val)
				default:
					return String(format: "%.3f", val)
			}
		}
		return ""
	}
	
	
	private func getTipText(_ item1: GraphItem, _ item2: GraphItem) -> LocalizedStringKey {
		if item1 == .NUMSTARS {
			if item2 == .AIRMASS {
				return LocalizedStringKey("GRAPH_NUMSTARS_AIRMASS_HELP")
			} else if item2 == .MEAN {
				return LocalizedStringKey("GRAPH_NUMSTARS_MEAN_HELP")
			}
			
		} else if item1 == .ECCENTRICITY {
			if item2 == .RELRMSRADEC {
				return LocalizedStringKey("GRAPH_ECCENTRICITY_RELRMSRADEC_HELP")
			} else if item2 == .MaxPeak {
				return LocalizedStringKey("GRAPH_ECCENTRICITY_MAXPEAK_HELP")
			}
			
		} else if item1 == .HFR {
			if item2 == .AIRMASS {
				return LocalizedStringKey("GRAPH_HFR_AIRMASS_HELP")
			} else if item2 == .RMS {
				return LocalizedStringKey("GRAPH_HFR_RMS_HELP")
			} else if item2 == .WINDSPD { //
				return LocalizedStringKey("GRAPH_HFR_WINDSPD_HELP")
			} else if item2 == .WINDGUST { //
				return LocalizedStringKey("GRAPH_HFR_WINDGUST_HELP")
			}
			
		} else if item1 == .MEAN {
			if item2 == .AIRMASS {
				return LocalizedStringKey("GRAPH_MEAN_AIRMASS_HELP")
			} else if item2 == .SKYBRIGHTNESS { //
				return LocalizedStringKey("GRAPH_MEAN_SKYBRIGHTNESS_HELP")
			} else if item2 == .SkyQuality { //
				return LocalizedStringKey("GRAPH_MEAN_SKYQUALITY_HELP")
			}
			
		} else if item1 == .RMS {
			if item2 == .AIRMASS {
				return LocalizedStringKey("GRAPH_RMS_AIRMASS_HELP")
			}
		} else if item1 == .DELTAT {
			if item2 == .MEAN {
				return LocalizedStringKey("GRAPH_DELTAT_MEAN_HELP")
			}
		}
		
		return LocalizedStringKey("No Information available")
	}
	
    var body: some View {
		ZStack {
			VStack {
				HStack {
					Text(item1.name) + Text(" ") + Text(item1.unitText)
					Text(" / ")
					Text(item2.name) + Text(" ") + Text(item2.unitText)
					
					Spacer()
					
					Image(systemName: "questionmark.circle")
						.resizable()
						.frame(width: 20, height: 20)
						.padding(.bottom, 4)
						.foregroundColor(Color.primary)
						.zIndex(1)
						.contentShape(Circle())
						.onTapGesture {
							withAnimation(.easeInOut(duration: 0.2)) {
								showTip.toggle()
							}
						}
					
				}
				
				Chart {
					ForEach(self.chartData.data, id: \.index) {  data in
						LineMark(
							x: .value("Index", data.index),
							y: .value(item1.rawValue, data.value1 / self.chartData.maxValue1)
						)
						.interpolationMethod(.catmullRom)
						.foregroundStyle(by: .value("Index", item1.rawValue))
						.accessibilityHidden(true)
						
						LineMark(
							x: .value("Index", data.index),
							y: .value(item2.rawValue, data.value2 / self.chartData.maxValue2)
						)
						.interpolationMethod(.catmullRom)
						.foregroundStyle(by: .value("Index", item2.rawValue))
						.accessibilityHidden(true)
						
						PointMark(
							x: .value("Index", data.index),
							y: .value(item1.rawValue, data.value1 / self.chartData.maxValue1)
						)
						.foregroundStyle(self.chartData.getColor(data, color: color1))
						/*
						.symbol {
							Circle()
								.foregroundStyle(self.chartData.getColor(data, color: color1))
								.frame(width: 5)
						}*/
						.accessibilityHidden(true)
						
						PointMark(
							x: .value("Index", data.index),
							y: .value(item2.rawValue, data.value2 / self.chartData.maxValue2)
						)
						.foregroundStyle(self.chartData.getColor(data, color: color2))
						/*
						.symbol {
							Circle()
								.foregroundStyle(self.chartData.getColor(data, color: color2))
								.frame(width: 5)
						}*/
						.accessibilityHidden(true)
						
						
						if let selectedIndex, let (v1, v2) = self.chartData.getValues(at: selectedIndex) {
							
							PointMark(
								x: .value("Index", selectedIndex),
								y: .value(item1.rawValue, v1 / self.chartData.maxValue1)
							)
							.foregroundStyle(Color.orange)
							.accessibilityHidden(true)
							
							PointMark(
								x: .value("Index", selectedIndex),
								y: .value(item1.rawValue, v2 / self.chartData.maxValue2)
							)
							.foregroundStyle(Color.orange)
							.accessibilityHidden(true)
							
						}
						
					}
				}
				.chartForegroundStyleScale([
					item1.rawValue: color1.gradient,
					item2.rawValue: color2.gradient,
				])
				.chartPlotStyle { plotContent in
					plotContent
						.background(.chartBackground.gradient.opacity(0.5))
				}
				.chartScrollableAxes(.horizontal)
				.chartXVisibleDomain(length: self.chartData.data.isNotEmpty ? min(self.chartData.data.count, 50) : 50)
				.chartXScale(domain: self.chartData.minIndex...self.chartData.maxIndex)
				//.chartYScale(domain: self.chartData.minYValue...self.chartData.maxYValue)
				.chartYAxis {
					
					let values = self.chartData.data.map { $0.value1 }
					if values.count > 0 {
						let min = self.chartData.minValue1
						let max = self.chartData.maxValue1
						let defaultStride = Array(stride(from: 0.0, to: 1.0, by: 1.0/Double(strideBy)))
						
						let stride = Array(stride(from: min,
												  through: max,
												  by: (max - min)/Float(strideBy)))
						AxisMarks(position: .leading, values: defaultStride) { axis in
							let value = stride[axis.index]
							AxisValueLabel("\(String(format: "%.1f", value))", centered: false)
								.foregroundStyle(color1)
						}
						
					}
					
					let values2 = self.chartData.data.map { $0.value2 }
					if values2.count > 0 {
						let min = self.chartData.minValue2
						let max = self.chartData.maxValue2
						
						if min < max {
							
							let defaultStride = Array(stride(from: 0.0, to: 1.0, by: 1.0/Double(strideBy)))
							
							let stride = Array(stride(from: min,
													  through: max,
													  by: (max - min)/Float(strideBy)))
							AxisMarks(position: .trailing, values: defaultStride) { axis in
								let value = stride[axis.index]
								AxisValueLabel("\(String(format: "%.1f", value))", centered: false)
									.foregroundStyle(color2)
							}
						}
					}
				}
				.chartXSelection(value: $selectedIndex)
			}
			.clipped()
			.task {
				await self.chartData.update(self.item1, self.item2, session: self.session)
			}
			.overlay {
				if showTip {
					VStack {
						Spacer()
						
						ShowTipView(tip: InfoTip(tipText: getTipText(item1, item2)))
							.transition(.asymmetric(insertion: .move(edge: .leading), removal: .move(edge: .trailing)))
						
					}
				}
			}
			
			if let selectedIndex, let image = self.chartData.getImage(at: selectedIndex) {
				
				VStack {
					Spacer()
					
					HStack {
						VStack(alignment: .leading) {
							
							Text(image.FileName).truncationMode(.middle)
							
							HStack {
								Text(image.filter).foregroundColor(session.getFilterColor(image.filter) ?? .primary)
								Text("Pierside \(image.pierSide)")
								Text("Altitude \(image.Altitude)")
							}
							HStack {
								Text("\(item1.rawValue): \(getFormattedValue(image, item: item1))")
							}
							HStack {
								Text("\(item2.rawValue): \(getFormattedValue(image, item: item2))")
							}
							
						}
						.padding(.horizontal, 32)
						.padding(.vertical)
						.background(.ultraThinMaterial).cornerRadius(8)
						.padding(.bottom, 8)
					
						Spacer()
					}
				}
				.transition(.opacity)
				
			}
			
		}
		
    }
}

struct MergedDataChartDetails: View {
	
	@State var session: SessionData
	let item1: GraphItem
	let item2: GraphItem
	let color1: Color
	let color2: Color
	
	@State private var chartData: MergedChartData = MergedChartData()
	@State private var appeared: Bool = false
	@State private var selectedIndex: Int?
	let strideBy = 6
	
	@ViewBuilder
	func table(_ image: SessionImage) -> some View {
		ScrollView(.horizontal) {
			
			LazyHGrid(rows: [GridItem(.adaptive(minimum: 20, maximum: 40), spacing: 0)], spacing: 0) {
				ForEach(session.tableDataColumns) { column in
					
					if column.id == TableImageColumnId.marking {
						
						Button {
							image.toggleMarked()
							session.onContentUpdated()
							Task {
								await self.chartData.update(item1, item2, session: session)
							}
							
						} label: {
							
							HStack {
								Text("Marked as BAD")
									.frame(width: 100, alignment: .trailing)
									.foregroundColor(image.bad ? .badColor : .green)
									.help("Checked if the image is marked as 'BAD'")
								
								Text(": ")
								Image(systemName: image.bad ? "checkmark.square" : "square")
									.imageScale(.large)
									.foregroundColor(image.bad ? .badColor : .green)
									.frame(width: 250, alignment: .leading)
							}
						}
						.buttonStyle(PlainButtonStyle())
						
					} else {
						
						NameValueCell(name: column.name, value: image.columnText(for: column.id),
									  valueColor: image.bad ? .badColor : .primary)
					}
				}
			}
			.padding()
			.background(Color.controlBackgroundColor.opacity(0.1))
			.contextMenu {
				if self.session.quickFitsAvailable {
					Button {
						self.session.openInQuickFits( image )
					} label: {
						Text("Open in QuickFits")
					}
				}
				
				if self.session.fitsPreviewAvailable {
					Button {
						self.session.openInFitsPreview( image )
					} label: {
						Text("Open in FITS Preview")
					}
				}
				
				if self.session.pixInsightAvailable {
					Button {
						self.session.openInPixInsight( image )
					} label: {
						Text("Open in PixInsight")
					}
				}
				
				if self.session.astapAvailable {
					Button {
						self.session.openInAstap( image )
					} label: {
						Text("Open in Astap")
					}
				}
			}
		}
		.frame(maxHeight: 160)
	}
	
	
	
	var body: some View {
		
		VStack {
			HStack {
				Text(item1.name) + Text(" ") + Text(item1.unitText)
				Text(" / ")
				Text(item2.name) + Text(" ") + Text(item2.unitText)
				
				Spacer()
			}
			
			Chart {
				ForEach(self.chartData.data, id: \.index) {  data in
					LineMark(
						x: .value("Index", data.index),
						y: .value(item1.rawValue, data.value1 / self.chartData.maxValue1)
					)
					.foregroundStyle(by: .value("Index", item1.rawValue))
					.accessibilityHidden(true)
					
					LineMark(
						x: .value("Index", data.index),
						y: .value(item2.rawValue, data.value2 / self.chartData.maxValue2)
					)
					.foregroundStyle(by: .value("Index", item2.rawValue))
					.accessibilityHidden(true)
					
					PointMark(
						x: .value("Index", data.index),
						y: .value(item1.rawValue, data.value1 / self.chartData.maxValue1)
					)
					.foregroundStyle(self.chartData.getColor(data, color: color1))
					.accessibilityHidden(true)
					
					PointMark(
						x: .value("Index", data.index),
						y: .value(item2.rawValue, data.value2 / self.chartData.maxValue2)
					)
					.foregroundStyle(self.chartData.getColor(data, color: color2))
					.accessibilityHidden(true)
					
					
					if let selectedIndex, let (v1, v2) = self.chartData.getValues(at: selectedIndex) {
						
						PointMark(
							x: .value("Index", selectedIndex),
							y: .value(item1.rawValue, v1 / self.chartData.maxValue1)
						)
						.foregroundStyle(Color.orange)
						.accessibilityHidden(true)
						
						PointMark(
							x: .value("Index", selectedIndex),
							y: .value(item1.rawValue, v2 / self.chartData.maxValue2)
						)
						.foregroundStyle(Color.orange)
						.accessibilityHidden(true)
						
					}
					
				}
			}
			.chartForegroundStyleScale([
				item1.rawValue: color1,
				item2.rawValue: color2,
			])
			.chartPlotStyle { plotContent in
				plotContent
					.background(.chartBackground.gradient.opacity(0.5))
			}
			.chartScrollableAxes(.horizontal)
			.chartXVisibleDomain(length: self.chartData.data.isNotEmpty ? min(self.chartData.data.count, 50) : 50)
			.chartXScale(domain: self.chartData.minIndex...self.chartData.maxIndex)
			.chartYAxis {
				let defaultStride = Array(stride(from: 0.0, to: 1.0, by: 1.0/Double(strideBy)))
				let values = self.chartData.data.map { $0.value1 }
				if values.count > 0 {
					let min = self.chartData.minValue1
					let max = self.chartData.maxValue1
					let stride = Array(stride(from: min,
											  through: max,
											  by: (max - min)/Float(strideBy)))
					AxisMarks(position: .leading, values: defaultStride) { axis in
						let value = stride[axis.index]
						AxisValueLabel("\(String(format: "%.1f", value))", centered: false)
							.foregroundStyle(color1)
					}
					
				}
				
				let values2 = self.chartData.data.map { $0.value2 }
				if values2.count > 0 {
					let min = self.chartData.minValue2
					let max = self.chartData.maxValue2
					let stride = Array(stride(from: min,
											  through: max,
											  by: (max - min)/Float(strideBy)))
					AxisMarks(position: .trailing, values: defaultStride) { axis in
						let value = stride[axis.index]
						AxisValueLabel("\(String(format: "%.1f", value))", centered: false)
							.foregroundStyle(color2)
					}
					
				}
			}
			.chartGesture({ chartProxy in
				DragGesture(minimumDistance: 0.0, coordinateSpace: .local)
					.onChanged { drag in
						guard let _ = chartProxy.plotFrame else {
							selectedIndex = nil
							return
						}
						if let index: Int = chartProxy.value(atX: drag.location.x) {
							selectedIndex = index
						} else {
							selectedIndex = nil
						}
						
					}
				
			})
			
			if let index = selectedIndex, let image = self.chartData.getImage(at: index) {
				table(image)
				
			} else {
				Rectangle().fill(Color.controlBackgroundColor.opacity(0.1)).frame(maxHeight: 160)
			}
			Spacer(minLength: 16)
			
		}
		.padding()
		.task {
			await self.chartData.update(self.item1, self.item2, session: self.session)
		}
		
	}
}
