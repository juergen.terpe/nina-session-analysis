//
//  CorrectionsNorthSouthGraph.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 14.04.24.
//

import SwiftUI
import Charts

@Observable
class NorthSouthGraphModel {
	var north: Float = 0.0
	var south: Float = 0.0
	var avg: Float = 0.0
	var maxValue: Float = 100.0
	
	
	func update(session: GuidingData) async {
		var north: Float = 0.0
		var south: Float = 0.0
		var avg: Float = 0.0
		
		for frame in session.getGuideGraphDataWithoutDithering() {
			if frame.pulseDEC > 0.0 {
				north += frame.pulseDEC
			} else if frame.pulseDEC < 0.0 {
				south += frame.pulseDEC
			}
			
			avg += frame.pulseDEC
		}
		
		let n = north
		let s = south
		let a = avg
		
		let maxV = max(Double(abs(south)), Double(north)) * 1.2
		
		await MainActor.run {
			self.north = n
			self.south = s
			self.avg = a
			self.maxValue = Float(maxV)
		}
	}
}


struct CorrectionsNorthSouthGraph: View {
	let session: GuidingData
	@State private var model: NorthSouthGraphModel = NorthSouthGraphModel()
	
	
    var body: some View {
		VStack {
			Text("North/South Distribution (ms)").font(.headline)
			
			Chart {
				RectangleMark(xStart: .value("North", model.north), xEnd: .value("North", 0.0), y: .value("Y",0.5), height: .fixed(20))
					.foregroundStyle(Color.orange.gradient)
				
				RectangleMark(xStart: .value("South", model.south), xEnd: .value("South", 0.0), y: .value("Y", 0.5), height: .fixed(20))
					.foregroundStyle(Color.yellow.gradient)

				RectangleMark(xStart: .value("Avg", model.avg), xEnd: .value("Avg", 0.0), y: .value("Y", 0.5), height: .fixed(20))
					.foregroundStyle(Color.red)
			}
			.chartXScale(domain: -model.maxValue...model.maxValue)
			.chartYScale(domain: 0...2)
			.chartYAxis(.hidden)
			.frame(height: 50)
			.transition(.blurReplace)
		}
		.task {
			await model.update(session: self.session)
		}
		.onChange(of: self.session.id) { _, _ in
			Task {
				await model.update(session: self.session)
			}
		}
    }
}

