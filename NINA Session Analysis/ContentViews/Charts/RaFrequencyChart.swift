//
//  RaFrequencyChart.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 30.06.24.
//
import SwiftUI
import Charts

fileprivate struct ChartDataEntry: Identifiable {
	let id = UUID()
	let x: Float
	let y: Float
}




@Observable
fileprivate class RaFrequncyModel {
	var data = [ChartDataEntry]()
	var maxFrequency: Float = 0.0
	var maxAmplitude: Float = 1.0
	
	func update(frames: [GuidingFrame], pixelScale: Double) async {
		
		guard frames.count > 0 else {
			
			await MainActor.run {
				self.data = []
				self.maxFrequency = 0.0
			}
			return
		}
			
		
		let analysis = GuidingDriftAnalysis()
		let data = await analysis.fastFourierTransform(on: frames, pixelScale: pixelScale)

		if let maxFrequency = data.last?.period {
			
			let maxAmplitude = data.map { $0.magnitude }.max()
			
			let dataEntries = data.map { (period, magnitude) -> ChartDataEntry in
				return ChartDataEntry(x: period, y: magnitude)
			}
			
			await MainActor.run {
				self.data = dataEntries
				self.maxFrequency = Float(maxFrequency)
				self.maxAmplitude = Float(maxAmplitude ?? 1.0) * 1.2
			}
		} else {
			
			await MainActor.run {
				self.data = []
				self.maxFrequency = 0.0
				self.maxAmplitude = 1.0
			}
		}
		
		
	}
	
}


struct RaFrequencyChart: View {
	let guidingFrames: [GuidingFrame]
	let pixelScale: Double
	@State private var model = RaFrequncyModel()
	@State private var selectedXValue: Float?
	@State private var selectedValue: ChartDataEntry?
	
	func findSelection(_ value: Float) {
		
		var minDelta: Float = Float.infinity
		for entry in self.model.data {
			let delta = abs(entry.x - value)
			if delta < minDelta {
				self.selectedValue = entry
				minDelta = delta
			}
		}
	}
	
	
	var body: some View {
		
		VStack {
		
			HStack {
				Text("Frequency chart")
			}
			.padding()
			
			
			if self.model.data.count > 0 {
				Chart {
					
					ForEach(self.model.data) { item in
						
						LineMark(x: .value("Frequency", item.x),
								 y: .value("Magnitude", item.y))
							.interpolationMethod(.catmullRom)
						
					}
					
					if let selected = self.selectedValue {
						PointMark(x: .value("Frequency", selected.x),
								  y: .value("Magnitude", selected.y))
							.foregroundStyle(Color.orange)
							.annotation(position: .top, overflowResolution: AnnotationOverflowResolution(x: .disabled, y: .disabled)) {
								VStack {
									Text("Period \(selected.x, specifier: "%.1f")s")
									Text("Magnitude \(selected.y, specifier: "%.4f")\"")
								}
								.padding(8)
								.background(.thinMaterial)
							}
							.zIndex(100)
					}
				}
				.chartLegend(.visible)
				.chartXScale(domain: 0...self.model.maxFrequency)
				.chartYScale(domain: 0...self.model.maxAmplitude)
				.chartScrollableAxes(.horizontal)
				.chartXSelection(value: $selectedXValue)
				//.chartXVisibleDomain(length: 300.0)
				.chartYAxis {
					AxisMarks(position: .leading)
					AxisMarks(position: .trailing)
				}
			}
		}
		.padding(.top, 32)
		.task {
			await self.model.update(frames: self.guidingFrames, pixelScale: pixelScale)
		}
		.onChange(of: self.selectedXValue) { _, newValue in
			if let newValue {
				findSelection(newValue)
			}
		}
		
	}
}
