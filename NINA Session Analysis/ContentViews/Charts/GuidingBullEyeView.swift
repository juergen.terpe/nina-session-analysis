//
//  GuidingBullEyeView.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 17.04.24.
//

import SwiftUI

struct BullsEyeShape: Shape {
	var scale: Float
	var data: [(ra: Float, dec: Float)]
	
	func path(in rect: CGRect) -> Path {
		let middleX = rect.width / 2.0
		let middleY = rect.height / 2.0
		let sc = min(rect.width, rect.height) / CGFloat(scale)
		
		var path = Path()
		
		for p in data {
			let x = middleX + CGFloat(p.ra) * sc
			let y = middleY + CGFloat(p.dec) * sc
			
			path.addEllipse(in: CGRect.from(center: CGPoint(x: x, y: y), radius: 1.5))
		}
		
		return path
	}
}

@Observable
class BullsEyeModel {
	var hits: [(ra: Float, dec: Float)] = []
	
	func update(from guiding: GuidingData) async {
		var result = [(ra: Float, dec: Float)]()
		
		for frame in guiding.frames {
			if !guiding.ditherData.contains(where: { $0.begin <= frame.time && $0.end >= frame.time }) {
				result.append((ra: frame.raRawDistance, dec: frame.decRawDistance))
			}
		}
		
		let hits = result
		await MainActor.run {
			self.hits = hits
		}
	}
	
	func update(from session: SessionData, for image: SessionImage) async {
		var result = [(ra: Float, dec: Float)]()

		let data = await session.getGuideGraphData(for: image)
		for frame in data {
			result.append((ra: frame.ra, dec: frame.dec))
		}
		
		let hits = result
		await MainActor.run {
			self.hits = hits
		}
	}
	
}


struct GuidingBullEyeView: View {
	var session: SessionData
	var guidingSession: GuidingData? = nil
	var selectedImage: SessionImage? = nil
	@State var model: BullsEyeModel = BullsEyeModel()
	
	@AppStorage("GuideGraphScale") var scale = 4
	@Environment(\.colorScheme) var colorScheme
	
    var body: some View {
		GeometryReader { geometry in
			ZStack {
				Circle()
					.fill(Color.blue.opacity(0.05))
				
				PolarGrid(rCount: 1, thetaCount: 5)
					.strokeColor(Color.primary.opacity(0.5), lineWidth: 0.5)
					.clipCircle()
				
				BullsEyeShape(scale: Float(scale), data: model.hits)
					.fill(Color.blue)
			}
			.drawingGroup(opaque: true)
			.clipCircle()
		}
		.task {
			if let guiding = self.guidingSession {
				await model.update(from: guiding)
			} else if let image = self.selectedImage {
				await self.model.update(from: session, for: image)
			}
		}
		.onChange(of: self.selectedImage) { _, _ in
			Task {
				if let image = self.selectedImage {
					await self.model.update(from: session, for: image)
				}
			}
		}
    }
}

