//
//  DataChart.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 30.03.24.
//

import SwiftUI
import Charts
import SigmaSwiftStatistics

@Observable
class DataChartData {
	
	struct ChartData: Identifiable {
		var id: UUID
		let index: Int
		let value: Float
		let bad: Bool
		let filter: String
		let target: String
		let session: String
	}
	
	private var sessionId: UUID? = nil
	var session: SessionData? = nil
	var filteredImages: [SessionImage] = []
	var item: GraphItem?
	var data: [ChartData] = []
	var filters: [String] = []
	var targets: [String] = []
	var sessions: [String] = []
	var minIndex: Int = 0
	var maxIndex: Int = 0
	var minValue: Float = 0.0
	var maxValue: Float = 100.0
	var ignoreOutliers: Bool = false
	
	var rulerValue: Float? = nil
	
	let colors = [Color.cyan, Color.indigo, Color.green, Color.purple, Color.mint, Color.pink, Color.brown]
	
	var formattedRulerValue: String {
		if let rulerValue = self.rulerValue, let item = self.item {
			switch item {
				case GraphItem.MEAN:
					return String(format: "%.2f ADU", rulerValue)
				case GraphItem.MEDIAN:
					return String(format: "%.0f ADU", rulerValue)
				case GraphItem.HFR:
					return String(format: "%.1fpx", rulerValue)
				case GraphItem.FWHM:
					return String(format: "%.1fpx", rulerValue)
				case GraphItem.ECCENTRICITY:
					return String(format: "%.1fpx", rulerValue)
					
				case GraphItem.RMS, GraphItem.RMSRA, GraphItem.RMSDEC:
					return String(format: "%.1f\"", rulerValue)
					
				case GraphItem.NUMSTARS:
					return String(format: "%.0f Stars", rulerValue)
				default:
					return ""
			}
		}
		return ""
	}
	
	private func hasZeroMinValue(_ item: GraphItem) -> Bool {
		switch item {
			case GraphItem.ADUMAX: return true
			case GraphItem.ADUMIN: return true
			case GraphItem.AIRMASS: return true
			case GraphItem.CLOUDCVR: return true
			case GraphItem.FOCPOS: return true
			case GraphItem.FWHM: return true
			case GraphItem.HFR: return true
			case GraphItem.MEDIAN: return true
			case GraphItem.MEAN: return true
			case GraphItem.DITHERDECPX: return true
			case GraphItem.DITHERPX: return true
			case GraphItem.DITHERRAPX: return true
			case GraphItem.HUMIDITY: return true
			case GraphItem.NUMSTARS: return true
			case GraphItem.ECCENTRICITY: return true
			case GraphItem.MOONANGL: return true
			case GraphItem.RMS: return true
			case GraphItem.RMSRA: return true
			case GraphItem.RMSDEC: return true
			case GraphItem.SKYBRIGHTNESS: return true
			case GraphItem.SkyQuality: return true
			case GraphItem.PulsesRA: return true
			case GraphItem.PulsesDEC: return true
			case GraphItem.MaxPeakRA: return true
			case GraphItem.MaxPeakDEC: return true
			case GraphItem.MaxPeak: return true
			case GraphItem.PRESSURE: return true
			case GraphItem.SUNANGLE: return true
			case GraphItem.SUNALT: return true
			case GraphItem.WINDSPD: return true
			case GraphItem.WINDGUST: return true
			default: return false
		}
	}
	
	private func getMinimumMaxValue(_ item: GraphItem, _ min: Float, _ max: Float) -> Float {
		switch item {
			case GraphItem.ADUMAX: return 65535.0
			case GraphItem.ADUMIN: return 1000.0
			case GraphItem.AIRMASS: return 2.0
			case GraphItem.CLOUDCVR: return 25.0
			case GraphItem.FOCPOS: return minValue + 10000.0
			case GraphItem.FWHM: return 4.0
			case GraphItem.HFR: return 3.0
			case GraphItem.MEDIAN: return 1000.0
			case GraphItem.MEAN: return 1000.0
			case GraphItem.DITHERDECPX: return 2.0
			case GraphItem.DITHERPX: return 2.0
			case GraphItem.DITHERRAPX: return 2.0
			case GraphItem.HUMIDITY: return 30.0
			case GraphItem.NUMSTARS: return 1000.0
			case GraphItem.ECCENTRICITY: return 0.6
			case GraphItem.MOONANGL: return 30.0
			case GraphItem.RMS: return 1.0
			case GraphItem.RMSRA: return 1.0
			case GraphItem.RMSDEC: return 1.0
			case GraphItem.SKYBRIGHTNESS: return 21.5
			case GraphItem.SkyQuality: return 30.0
			case GraphItem.PulsesRA: return 1000
			case GraphItem.PulsesDEC: return 1000
			case GraphItem.MaxPeakRA: return 2.0
			case GraphItem.MaxPeakDEC: return 2.0
			case GraphItem.MaxPeak: return 2.0
			case GraphItem.PRESSURE: return 1000.0
			case GraphItem.SUNANGLE: return 20.0
			case GraphItem.SUNALT: return 20.0
			case GraphItem.WINDSPD: return 20.0
			case GraphItem.WINDGUST: return 20.0
			case GraphItem.AMBTEMP: return 30.0
			case GraphItem.DELTAT: return 30.0
			case GraphItem.DEWPOINT: return 30.0
			case GraphItem.MOONALT: return 90.0
			case GraphItem.RELRMSRADEC: return 1.0
			default: return 1.0
		}
	}
	
	
	func update(_ item: GraphItem, session: SessionData, area: Bool, usePercentile: Bool = true) async {
		
		guard session.id != self.sessionId || self.ignoreOutliers != usePercentile else {
			return
		}
		
		await MainActor.run {
			self.sessionId = session.id
			self.ignoreOutliers = usePercentile
		}
		
		var result = [ChartData]()
		
		let filteredImages = session.filteredImages.filter({ $0.SessionDate != nil })
		
		var filters = Set<String>()
		var targets = Set<String>()
		var sessions = Set<String>()
		
		result.reserveCapacity(filteredImages.count)
		
		var minIndex: Int?
		var maxIndex: Int?
		var minVal: Float?
		var maxVal: Float?
		
		for image in filteredImages {
			if let value = image.getValue(item) {
				
				minIndex = minIndex != nil ? min(image.index, minIndex!) : image.index
				maxIndex = maxIndex != nil ? max(image.index, maxIndex!) : image.index
				minVal = minVal != nil ? min(value, minVal!) : value
				maxVal = maxVal != nil ? max(value, maxVal!) : value
				
				result.append(ChartData(id: image.id, index: image.index, value: value, bad: image.bad,
										filter: image.filter, target: image.target, session: image.SessionDate!.text))
				filters.insert(image.filter)
				targets.insert(image.target)
				sessions.insert(image.SessionDate!.text)
			}
		}
		
		let rulerValue = getRulerValue(item: item, session: session)
		
		let minIdx = minIndex ?? 0
		let maxIdx = maxIndex ?? 0
		
		let values = result.map({ Double($0.value) })
		
		let minV = minVal ?? 0.0
		let maxV = maxVal ?? 100.0
		
		
		var minValue = min(minV, rulerValue ?? minV)
		var maxValue = max(maxV, rulerValue ?? maxV)
		let filtersSorted = Array(filters).sorted()
		let targetsSorted = Array(targets).sorted()
		let sessionsSorted = Array(sessions).sorted()
		let resultingChartData = result
		
		let minValueOffset = hasZeroMinValue(item) ? 0.0 : abs(minValue < 1000.0 ? minValue * 0.1 : 10.0)
		let maxValueOffset = maxValue < 1000.0 ? maxValue * 0.1 : 10.0
		
		minValue = minValue - minValueOffset
		maxValue = maxValue + maxValueOffset
		
		if abs(maxValue - minValue) < 0.1 {
			maxValue = getMinimumMaxValue(item, minValue, maxValue)
		} else {
			switch item {
				case .DITHERDECPX, .DITHERPX, .DITHERRAPX, .FWHM, .HFR, .MaxPeak, .MaxPeakDEC,
					 .MaxPeakRA, .PulsesDEC, .PulsesRA, .RELRMSRADEC, .RMS, .RMSDEC, .RMSRA, .WINDSPD, .WINDGUST:
					if usePercentile {
						let p2 = Float(Sigma.percentile(values, percentile: 0.99) ?? Double(maxValue))
						maxValue = min(p2, maxValue)
					}
				default:
					break
			}
		}
		
		let minValueSet = minValue
		let maxValueSet = maxValue
		
		await MainActor.run {
			self.filteredImages = filteredImages
			self.filters = filtersSorted
			self.targets = targetsSorted
			self.sessions = sessionsSorted
			self.data = resultingChartData
			self.session = session
			self.item = item
			self.maxIndex = maxIdx
			self.minIndex = minIdx
			self.minValue = minValueSet
			self.maxValue = maxValueSet
			self.rulerValue = rulerValue
		}
	}
	
	private func getRulerValue(item: GraphItem, session: SessionData) -> Float? {
		switch item {
			case GraphItem.MEAN:
				
				if let minMean = self.filteredImages.filter({ $0.mean != nil }).map({ $0.mean! }).min() {
					return minMean + minMean * session.analyzationSettings.getMeanTolerance(session.selectedFilter)
				}
				return nil
			case GraphItem.MEDIAN:
				if let minMedian = self.filteredImages.filter({ $0.median != nil }).map({ Float($0.median!) }).min() {
					return minMedian + minMedian * session.analyzationSettings.getMeanTolerance(session.selectedFilter)
				}
				return nil
			case GraphItem.HFR:
				if let minHFR = self.filteredImages.filter({ $0.hfr != nil }).map({ $0.hfr! }).min() {
					return minHFR + minHFR * session.analyzationSettings.getHFRTolerance(session.selectedFilter)
				}
				return nil
			case GraphItem.FWHM:
				if let minFWHM = self.filteredImages.filter({ $0.fwhm != nil }).map({ $0.fwhm! }).min() {
					return minFWHM + minFWHM * session.analyzationSettings.getHFRTolerance(session.selectedFilter)
				}
				return nil
			case GraphItem.ECCENTRICITY:
				return session.analyzationSettings.starEccentricityTolerance
				
			case GraphItem.RMS, GraphItem.RMSRA, GraphItem.RMSDEC:
				
				
				let pxScale = session.getPixelScale()
				let rmsMaxValue = pxScale != nil
						? pxScale! * session.analyzationSettings.rmsTolerance / 100.0 
						: nil
				
				return rmsMaxValue
			
			case GraphItem.NUMSTARS:
				if let maxStars = self.filteredImages.filter({ $0.numStars != nil }).map({ Float($0.numStars!) }).max() {
					return maxStars - maxStars * session.analyzationSettings.getStarCountTolerance(session.selectedFilter)
				}
				return nil
			default:
				return nil
				
		}
	}
	
	func markAboveRuler(bad: Bool) {
		if let session = self.session,
		   let item = self.item,
		   let rulerValue = self.getRulerValue(item: item, session: session) {
			switch item {
				case GraphItem.MEAN:
					for image in self.filteredImages.filter({ $0.mean != nil && $0.mean! > rulerValue}) {
						image.bad = bad
						if bad {
							image.addBadMarking()
						} else {
							image.removeBadMarking()
						}
					}
				case GraphItem.MEDIAN:
					for image in self.filteredImages.filter({ $0.median != nil && $0.median! > Int(rulerValue)}) {
						image.bad = bad
						if bad {
							image.addBadMarking()
						} else {
							image.removeBadMarking()
						}
					}
				case GraphItem.HFR:
					for image in self.filteredImages.filter({ $0.hfr != nil && $0.hfr! > rulerValue}) {
						image.bad = bad
						if bad {
							image.addBadMarking()
						} else {
							image.removeBadMarking()
						}
					}
				case GraphItem.FWHM:
					for image in self.filteredImages.filter({ $0.fwhm != nil && $0.fwhm! > rulerValue}) {
						image.bad = bad
						if bad {
							image.addBadMarking()
						} else {
							image.removeBadMarking()
						}
					}
				case GraphItem.ECCENTRICITY:
					for image in self.filteredImages.filter({ $0.eccentricity != nil && $0.eccentricity! > rulerValue}) {
						image.bad = bad
						if bad {
							image.addBadMarking()
						} else {
							image.removeBadMarking()
						}
					}
				case GraphItem.RMS:
					for image in self.filteredImages.filter({ $0.rms != nil && $0.rms! > rulerValue}) {
						image.bad = bad
						if bad {
							image.addBadMarking()
						} else {
							image.removeBadMarking()
						}
					}
				case GraphItem.RMSRA:
					for image in self.filteredImages.filter({ $0.rmsRA != nil && $0.rmsRA! > rulerValue}) {
						image.bad = bad
						if bad {
							image.addBadMarking()
						} else {
							image.removeBadMarking()
						}
					}
				case GraphItem.RMSDEC:
					for image in self.filteredImages.filter({ $0.rmsDEC != nil && $0.rmsDEC! > rulerValue}) {
						image.bad = bad
						if bad {
							image.addBadMarking()
						} else {
							image.removeBadMarking()
						}
					}
				case GraphItem.NUMSTARS:
					for image in self.filteredImages.filter({ $0.numStars != nil && $0.numStars! < Int(rulerValue)}) {
						image.bad = bad
						if bad {
							image.addBadMarking()
						} else {
							image.removeBadMarking()
						}
					}
				default:
					break
			}
		}
	}
	
	func getImage(at index: Int) -> SessionImage? {
		if let images = self.session?.filteredImages {
			return images.first(where: { $0.index == index })
		}
		return nil
	}
	
	func getValue(at index: Int) -> Float? {
		if self.minIndex <= index && self.maxIndex >= index, let dp = self.data.first(where: { $0.index == index}) {
			return dp.value
		}
		return nil
	}
	
	func hasValue(for index: Int) -> Bool {
		if getValue(at: index) != nil {
			return true
		}
		return false
	}
	
	fileprivate func getColor(_ data: ChartData) -> Color {
		if data.bad {
			return Color.badColor
		} else {
			if self.filters.count > 1 {
				if let fc = self.session?.filterColors.first(where: { $0.filterName == data.filter }) {
					return fc.color
				}
			}
			else if self.targets.count > 1 {
				if let index = self.targets.firstIndex(where: { $0 == data.target }) {
					return self.colors[index % self.colors.count]
				}
			}
			else if self.sessions.count > 1 {
				if let index = self.sessions.firstIndex(where: { $0 == data.session }) {
					return self.colors[index % self.colors.count]
				}
			}
			
			return Color.blue
		}
		
	}
}


struct DataChart: View {
	@State var session: SessionData
	let item: GraphItem
	let area: Bool
	@State private var chartData: DataChartData = DataChartData()
	//@State private var appeared: Bool = false
	@State private var selection: Int?
	@State private var selectedIndex: Int?
	@State private var showTip = false
	let timerMarkButton = Timer.publish(every: 0.5, on: .main, in: .common).autoconnect()
	@State private var markButtonCount = 0
	@State private var showMarkButton = false
	
	private func getTipText(_ item: GraphItem) -> LocalizedStringKey {
		switch (item) {
			case .MEAN:
				return LocalizedStringKey("CHART_MEAN_HELP")
			case .MEDIAN:
				return LocalizedStringKey("CHART_MEDIAN_HELP")
			case .RMS:
				return LocalizedStringKey("CHART_RMS_HELP")
			case .ECCENTRICITY:
				return LocalizedStringKey("CHART_ECCENTRICITY_HELP")
			case .FWHM:
				return LocalizedStringKey("CHART_FWHM_HELP")
			case .HFR:
				return LocalizedStringKey("CHART_HFR_HELP")
			case .AIRMASS:
				return LocalizedStringKey("CHART_AIRMASS_HELP")
			case .NUMSTARS:
				return LocalizedStringKey("CHART_NUMSTARS_HELP")
			case .FOCPOS:
				return LocalizedStringKey("CHART_FOCPOS_HELP")
			case .CLOUDCVR:
				return LocalizedStringKey("CHART_CLOUDCVR_HELP")
			case .DEWPOINT:
				return LocalizedStringKey("CHART_DEWPOINT_HELP")
			case .HUMIDITY:
				return LocalizedStringKey("CHART_HUMIDITY_HELP")
			case .PRESSURE:
				return LocalizedStringKey("CHART_PRESSURE_HELP")
			case .AMBTEMP:
				return LocalizedStringKey("CHART_AMBTEMP_HELP")
			case .WINDSPD:
				return LocalizedStringKey("CHART_WINDSPD_HELP")
			case .WINDGUST:
				return LocalizedStringKey("CHART_WINDGUST_HELP")
			case .SKYBRIGHTNESS:
				return LocalizedStringKey("CHART_SKYBRIGHTNESS_HELP")
			case .SKYTEMPERATURE:
				return LocalizedStringKey("CHART_SKYTEMP_HELP")
			case .SkyQuality:
				return LocalizedStringKey("CHART_SKYQUALITY_HELP")
			case .SUNANGLE:
				return LocalizedStringKey("CHART_SUNANGLE_HELP")
			case .MOONANGL:
				return LocalizedStringKey("CHART_MOONANGLE_HELP")
			case .RMSRA:
				return LocalizedStringKey("CHART_RMSRA_HELP")
			case .RMSDEC:
				return LocalizedStringKey("CHART_RNSDEC_HELP")
			case .RELRMSRADEC:
				return LocalizedStringKey("CHART_RELRMSRADEC_HELP")
			case .ADUMIN:
				return LocalizedStringKey("CHART_ADUMIN_HELP")
			case .ADUMAX:
				return LocalizedStringKey("CHART_ADUMAX_HELP")
			case .MOONALT:
				return LocalizedStringKey("CHART_MOONALT_HELP")
			case .SUNALT:
				return LocalizedStringKey("CHART_SUNALT_HELP")
			case .DITHERRAPX:
				return LocalizedStringKey("CHART_DRIFTRAPX_HELP")
			case .DITHERDECPX:
				return LocalizedStringKey("CHART_DRIFTDECPX_HELP")
			case .DITHERPX:
				return LocalizedStringKey("CHART_DRIFTPX_HELP")
			case .DELTAT:
				return LocalizedStringKey("CHART_DELTAT_HELP")
			default:
				return LocalizedStringKey("No Information available!")
		}
		
	}
	
	@ViewBuilder
	func getHeader() -> some View {
		HStack {
			Text(item.name).kerning(1.5) + Text(" ") + Text(item.unitText)
			Spacer()
			Image(systemName: "questionmark.circle")
				.resizable()
				.frame(width: 20, height: 20)
				.padding(.bottom, 4)
				.foregroundColor(Color.primary)
				.zIndex(1)
				.contentShape(Circle())
				.onTapGesture {
					withAnimation(.easeInOut(duration: 0.2)) {
						showTip.toggle()
					}
				}
		}
	}
	
	@ViewBuilder
	func showAnnotations() -> some View {
		VStack {
			Spacer()
			
			if let selectedIndex {
				DataChartAnnotationView(session: session, index: selectedIndex, item: item)
					.transition(.opacity)
			}
		}
	}
		
	var body: some View {
		
		ZStack {
			VStack {
				getHeader()
				Chart {
					
					ForEach(self.chartData.data, id: \.index) {  data in
						LineMark(
							x: .value("Index", data.index),
							y: .value(item.rawValue, data.value)
						)
						.foregroundStyle(Color.blue)
						.accessibilityHidden(true)
						
						if self.area {
							AreaMark(
								x: .value("Index", data.index),
								y: .value(item.rawValue, data.value)
							)
							.foregroundStyle(Gradient([Color.blue.opacity(0.5), Color.blue.opacity(0.0)]))
							.accessibilityHidden(true)
						}
						
						
						PointMark(
							x: .value("Index", data.index),
							y: .value(item.rawValue, data.value)
						)
						.foregroundStyle(self.chartData.getColor(data))
						/*
						.symbol {
							Circle()
								.foregroundStyle(self.chartData.getColor(data))
								.frame(width: 5)
						}*/
						.accessibilityHidden(true)
						
					}
					.alignsMarkStylesWithPlotArea()
					
					if let rulerValue = self.chartData.rulerValue {
						RuleMark(y: .value("Threshold", rulerValue))
							.foregroundStyle(.purple)
					}
					
					
					if let selectedIndex, self.chartData.hasValue(for: selectedIndex) {
						PointMark(
							x: .value("Index", selectedIndex),
							y: .value(item.rawValue, self.chartData.getValue(at: selectedIndex) ?? 0.0)
						)
						.foregroundStyle(Color.orange)
						.accessibilityHidden(true)
					}
				}
				.chartLegend(.visible)
				.chartPlotStyle { plotContent in
					plotContent
						.background(.chartBackground.gradient.opacity(0.5))
				}
				//.chartScrollableAxes(.horizontal)
				//.chartXVisibleDomain(length: self.chartData.data.isNotEmpty ? min(self.chartData.data.count, 60) : 60)
				.chartXScale(domain: self.chartData.minIndex...self.chartData.maxIndex)
				.chartYAxis {
					AxisMarks(position: .leading)
					AxisMarks(position: .trailing)
				}
				.chartXSelection(value: $selection)
				.chartYScale(domain: self.chartData.minValue...self.chartData.maxValue)
				
				.onChange(of: selection) { _, index in
					if index != nil && index != self.selectedIndex {
						self.selectedIndex = index
						markButtonCount = 1
						showMarkButton = false
					} else if index == nil {
						markButtonCount = 0
						self.selectedIndex = nil
						if showMarkButton {
							withAnimation(.easeInOut(duration: 0.2)) {
								showMarkButton = false
							}
						}
					}
				}
				.onReceive(self.timerMarkButton) { _ in
					if markButtonCount == 3 {
						markButtonCount = 0
						withAnimation(.easeInOut(duration: 0.2)) {
							showMarkButton = true
							
						}
					}
					
					if markButtonCount > 0 {
						markButtonCount += 1
					}
				}
				
			}
			
			showAnnotations()
			
		}
		.overlay {
			if showTip {
				VStack {
					Spacer()
					
					ShowTipView(tip: InfoTip(tipText: getTipText(item)))
						.transition(.asymmetric(insertion: .move(edge: .leading), removal: .move(edge: .trailing)))
					
				}
			}
		}
		//.animation(Animation.easeInOut, value: self.appeared)
		.task {
			await self.chartData.update(self.item, session: self.session, area: self.area)
		}
		/*
		.onAppear {
			withAnimation {
				self.appeared.toggle()
			}
		}*/
		.onChange(of: self.session.selectedSessionDate) { _, _ in
			
			Task {
				await self.chartData.update(self.item, session: self.session, area: self.area)
				
				/*
				await MainActor.run {
					withAnimation {
						self.appeared.toggle()
					}
				}*/
			}
		}
		.onChange(of: self.session.selectedExposure) { _, _ in
			Task {
				await self.chartData.update(self.item, session: self.session, area: self.area)
				/*
				await MainActor.run {
					withAnimation {
						self.appeared.toggle()
					}
				}*/
			}
		}
		.onChange(of: self.session.selectedFilter) { _, _ in
			Task {
				await self.chartData.update(self.item, session: self.session, area: self.area)
				/*
				await MainActor.run {
					withAnimation {
						self.appeared.toggle()
					}
				}*/
			}
		}
		.onChange(of: self.session.selectedTarget) { _, _ in
			Task {
				await self.chartData.update(self.item, session: self.session, area: self.area)
				/*
				await MainActor.run {
					withAnimation {
						self.appeared.toggle()
					}
				}*/
			}
		}
		
	}
}



struct DataChartWithSelection: View {
	@State var session: SessionData
	@Binding var item: GraphItem
	@Binding var image: SessionImage?
	let area: Bool
	var imageTitle: Bool = true
	var showDots: Bool = true
	@State private var chartData: DataChartData = DataChartData()
	@State private var appeared: Bool = false
	
	var body: some View {
		
		
		VStack {
			HStack(alignment: .bottom) {
				Text(item.name).kerning(1.5) + Text(" ") + Text(item.unitText)
				Spacer()
			}
			
			Chart {
				ForEach(self.chartData.data, id: \.index) { data in
					LineMark(
						x: .value("Index", data.index),
						y: .value(item.rawValue, data.value)
					)
					.foregroundStyle(Color.blue)
					.accessibilityHidden(true)
					
					if self.area {
						AreaMark(
							x: .value("Index", data.index),
							y: .value(item.rawValue, data.value)
						)
						.foregroundStyle(Gradient([Color.blue.opacity(0.5), Color.blue.opacity(0.0)]))
						.accessibilityHidden(true)
					}
					
					if self.showDots {
						PointMark(
							x: .value("Index", data.index),
							y: .value(item.rawValue, data.value)
						)
						.foregroundStyle(self.chartData.getColor(data))
						.accessibilityHidden(true)
					}
				}
				.alignsMarkStylesWithPlotArea()
				
				if let rulerValue = self.chartData.rulerValue {
					RuleMark(y: .value("Threshold", rulerValue))
						.foregroundStyle(.purple)
				}
				
				if let selectedIndex = self.image?.index, self.chartData.hasValue(for: selectedIndex) {
					PointMark(
						x: .value("Index", selectedIndex),
						y: .value(item.rawValue, self.chartData.getValue(at: selectedIndex) ?? 0.0)
					)
					.foregroundStyle(Color.orange)
					.accessibilityHidden(true)
				}
			}
			.chartLegend(.visible)
			.chartXScale(domain: self.chartData.minIndex...self.chartData.maxIndex)
			.chartYAxis {
				AxisMarks(position: .leading)
				AxisMarks(position: .trailing)
			}
			.chartYScale(domain: self.chartData.minValue...self.chartData.maxValue)
			.chartPlotStyle { plotContent in
				plotContent
					.background(.chartBackground.gradient.opacity(0.5))
			}
			
			if self.imageTitle {
				Text(image?.FileName ?? "").padding(.vertical, 4)
			}
		}
		.transition(.opacity)
		.animation(Animation.easeInOut, value: self.appeared)
		.task {
			await self.chartData.update(self.item, session: self.session, area: self.area)
		}
		.onAppear {
			
			withAnimation {
				self.appeared.toggle()
			}
			
		}
	}
}

struct DataChartAnnotationView: View {
	let session: SessionData
	let index: Int
	let item: GraphItem
	
	var image: SessionImage? {
		return session.filteredImages.first(where: { $0.index == index})
	}
	
	private func getFormattedValue(_ image: SessionImage) -> String {
		if let val = image.getValue(item) {
			switch item {
				case .ADUMAX, .ADUMIN, .MEDIAN, .NUMSTARS:
					return String(format: "%.0f", val)
				case .DEWPOINT, .AMBTEMP, .CLOUDCVR, .FOCPOS, .DITHERDECPX, .DITHERPX, .DITHERRAPX,
					 .HUMIDITY, .MEAN, .MOONALT, .MOONANGL, .SUNALT, .SUNANGLE, .PRESSURE:
					return String(format: "%.1f", val)
				default:
					return String(format: "%.3f", val)
			}
		}
		return ""
	}
	
	var body: some View {
		VStack(alignment: .leading) {
			if let image = image {
				HStack {
					Text(image.FileName).truncationMode(.middle)
					Spacer()
				}
				
				HStack {
					Text(image.filter).foregroundColor(session.getFilterColor(image.filter) ?? .primary)
					Text("Pierside \(image.pierSide)")
					Text("Altitude \(image.Altitude)")
					Spacer()
				}
				HStack {
					Text("\(item.rawValue): \(getFormattedValue(image))")
					Spacer()
				}
			}
		}
		.shadow(color: Color.blue, radius: 2.5)
		.padding(.horizontal, 32)
		.padding(.vertical)
		.padding(.bottom, 8)
				
	}
}


struct DataChartDetailView: View {
	@State var session: SessionData
	var sessionImages: [SessionImage]
	@Binding var item: GraphItem
	@State var timelines: [DateOfSession : CapturingTimelineData]
	let area: Bool
	
	let areaGradient = Gradient([Color.blue.opacity(0.5), Color.blue.opacity(0.0)])
	
	@State private var chartData: DataChartData = DataChartData()
	@State private var selectedIndex: Int?
	@State private var selectedChartScale: CGFloat = 80.0
	@State private var limitYAxis: Bool = false
	@State private var detailsTab: Int = 1
	@Environment(\.openWindow) private var openWindow
	@Environment(\.dismissWindow) private var dismissWindow
	
	@ViewBuilder
	func details(_ image: SessionImage) -> some View {
		
		VStack {
			
			HStack {
			
				Spacer()
				
				Picker("", selection: $detailsTab) {
					Text("Image Details").tag(1)
					Text("Guiding").tag(2)
					Text("Timeline").tag(3)
				}
				.pickerStyle(.segmented)
				.frame(maxWidth: 400)
				
				Spacer()
			}
			
			if detailsTab == 1 {
				table(image)
			} else if detailsTab == 2 {
				GuideGraphView(session: session, selectedimage: image)
			} else if let dateOfSession = image.SessionDate, let timeline = timelines[dateOfSession],
					  let index = timeline.findIndex(of: image)  {
				
				CapturingTimelineView(data: timeline, date: dateOfSession, selectedImageIndex: index, readonly: true)
					.id(image.id)
			}
		}
		.frame(maxHeight: 260)
		
	}
	
	
	@ViewBuilder
	func table(_ image: SessionImage) -> some View {
		ScrollView(.horizontal) {
			
			LazyHGrid(rows: [GridItem(.adaptive(minimum: 20, maximum: 40), spacing: 0)], spacing: 0) {
				ForEach(session.tableDataColumns) { column in
					if column.id == TableImageColumnId.marking {
						
						Button {
							image.toggleMarked()
							session.onContentUpdated()
							Task {
								await self.chartData.update(self.item, session: self.session, area: self.area, usePercentile: self.limitYAxis)
							}
							
						} label: {
							
							HStack {
								Text("Marked as BAD")
									.frame(width: 100, alignment: .trailing)
									.foregroundColor(image.bad ? .badColor : .green)
									.help("Checked if the image is marked as 'BAD'")
								
								Text(": ")
								Image(systemName: image.bad ? "checkmark.square" : "square")
									.imageScale(.large)
									.foregroundColor(image.bad ? .badColor : .green)
									.frame(width: 250, alignment: .leading)
							}
						}
						.buttonStyle(PlainButtonStyle())
						
					} else {
						
						NameValueCell(name: column.name, value: image.columnText(for: column.id),
									  valueColor: image.bad ? .badColor : .primary)
					}
				}
			}
			.padding()
			.background(Color.controlBackgroundColor.opacity(0.1))
			.contextMenu {
				
				Button {
					dismissWindow(id: "InternalViewer")
					openWindow(id: "InternalViewer", value: image.id)
				} label: {
					Text("Show in Viewer")
				}
				
				if self.session.quickFitsAvailable {
					Button {
						self.session.openInQuickFits( image )
					} label: {
						Text("Open in QuickFits")
					}
				}
				
				if self.session.fitsPreviewAvailable {
					Button {
						self.session.openInFitsPreview( image )
					} label: {
						Text("Open in FITS Preview")
					}
				}
				
				if self.session.pixInsightAvailable {
					Button {
						self.session.openInPixInsight( image )
					} label: {
						Text("Open in PixInsight")
					}
				}
				
				if self.session.astapAvailable {
					Button {
						self.session.openInAstap( image )
					} label: {
						Text("Open in Astap")
					}
				}
			}
		}
		
	}
	
	var body: some View {
		
		
		VStack {
			
			VStack(spacing: 0) {
				
				HStack {
					(Text(item.name).kerning(1.5) + Text(" ") + Text(item.unitText))
						.font(.largeTitle)
						.padding(.trailing)
					
					Toggle(isOn: $limitYAxis) {
						Text("Ignore outliers")
					}
					
					Spacer()
					
					HStack(spacing: 0) {
						Text("Chart density: ")
						Slider(value: $selectedChartScale, in: 50...250).frame(width: 200)
					}
					.padding()
				}
				.padding()
				.frame(height: 40)
				.onChange(of: self.limitYAxis) { _, _ in
					Task {
						await self.chartData.update(self.item, session: self.session, area: self.area, usePercentile: self.limitYAxis)
					}
				}
				
				
				Chart {
					ForEach(self.chartData.data, id: \.index) { data in
						LineMark(
							x: .value("Index", data.index),
							y: .value(item.rawValue, data.value)
						)
						.foregroundStyle(Color.blue)
						
						PointMark(
							x: .value("Index", data.index),
							y: .value(item.rawValue, data.value)
						)
						.foregroundStyle(self.chartData.getColor(data))
						
						if self.area {
							AreaMark(
								x: .value("Index", data.index),
								y: .value(item.rawValue, data.value)
							)
							.foregroundStyle(areaGradient)
						}
						
					}
					.alignsMarkStylesWithPlotArea()
					.accessibilityHidden(true)
					
					if let rulerValue = self.chartData.rulerValue {
						RuleMark(y: .value("Threshold", rulerValue))
							.foregroundStyle(.purple)
					}
					
					if let selectedIndex = self.selectedIndex, self.chartData.hasValue(for: selectedIndex) {
						PointMark(
							x: .value("Index", selectedIndex),
							y: .value(item.rawValue, self.chartData.getValue(at: selectedIndex) ?? 0.0)
						)
						.foregroundStyle(Color.orange)
						.accessibilityHidden(true)
						
					}
				}
				.chartXScale(domain: self.chartData.minIndex...self.chartData.maxIndex)
				.chartScrollableAxes(.horizontal)
				.chartXVisibleDomain(length: min(Int(selectedChartScale), sessionImages.count))
				.chartYAxis {
					AxisMarks(position: .leading)
					AxisMarks(position: .trailing)
				}
				.chartYScale(domain: self.chartData.minValue...self.chartData.maxValue)
				.chartLegend(.visible)
				.chartPlotStyle { plotContent in
					plotContent
						.background(.chartBackground.gradient.opacity(0.5))
				}
				.chartGesture({ chartProxy in
					DragGesture(minimumDistance: 0.0, coordinateSpace: .local)
						.onChanged { drag in
							guard let _ = chartProxy.plotFrame else {
								selectedIndex = nil
								return
							}
							
							if let index: Int = chartProxy.value(atX: drag.location.x) {
								selectedIndex = index
							} else {
								selectedIndex = nil
							}
							
						}
					
				})
				.padding()
				.accessibilityHidden(true)
				
			}.padding()
			
			if let _ = self.chartData.rulerValue {
				HStack {
					
					Text("Threshold: \(self.chartData.formattedRulerValue)")
					
					Button {
						self.chartData.markAboveRuler(bad: true)
						session.onContentUpdated()
						Task {
							await self.chartData.update(self.item, session: self.session, area: self.area, usePercentile: self.limitYAxis)
						}
						
					} label: {
						
						Text("Mark images")
							.padding(.horizontal)
							.padding(.vertical, 4)
							.background(Color.accentColor)
							.cornerRadius(8)
					}
					.buttonStyle(PlainButtonStyle())
					
					Button {
						self.chartData.markAboveRuler(bad: false)
						session.onContentUpdated()
						Task {
							await self.chartData.update(self.item, session: self.session, area: self.area, usePercentile: self.limitYAxis)
						}
						
					} label: {
						
						Text("Unmark images")
							.padding(.horizontal)
							.padding(.vertical, 4)
							.background(Color.accentColor)
							.cornerRadius(8)
					}
					.buttonStyle(PlainButtonStyle())
					
					Spacer()
				}.padding(.horizontal)
			}
			
			if let index = selectedIndex, let image = self.chartData.getImage(at: index) {
				details(image)
				
			} else {
				Rectangle().fill(Color.controlBackgroundColor.opacity(0.1)).frame(maxHeight: 160)
			}
			Spacer(minLength: 16)
			
		}
		
		.task {
			await self.chartData.update(self.item, session: self.session, area: self.area, usePercentile: self.limitYAxis)
		}
	}
}
