//
//  GuideGraphView.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 31.03.24.
//

import SwiftUI
import Charts
import PureSwiftUI

struct GuideDitherShape: Shape {
	let dither: DitherGraphData
	let maxTime: Float
	
	func path(in rect: CGRect) -> Path {
		let sx = rect.width/CGFloat(maxTime)
		
		return Path { path in
			
			let x1 = rect.minX + CGFloat(dither.time1) * sx
			let x2 = rect.minX + CGFloat(dither.time2) * sx
			
			path.addRect(CGRect(origin: CGPoint(x: x1, y: rect.minY), size: CGSize(width: x2-x1, height: rect.height)))
			
		}
	}
}

struct LostStarLine: Shape {
	let lost: LostStarEvents
	let maxTime: Float
	let begin: JulianDate
	
	func path(in rect: CGRect) -> Path {
		let sx = rect.width/CGFloat(maxTime)
		
		
		return Path { path in
			let x = rect.minX + CGFloat((lost.jd.jd - begin.jd) * 86400.0) * sx
			path.move(to: CGPoint(x: x, y: rect.minY))
			path.addLine(to: CGPoint(x: x, y: rect.maxY))
		}
	}
}


struct GuideGraphGrid: Shape {
	let maxTime: Float
	let timeScale: Float?
	
	var numScales: Int {
		
		if let timeScale = self.timeScale {
			return Int(timeScale)
		}
		
		if maxTime < 10.0 {
			return 10
		} else {
			var count = 5
			var scales = Int(maxTime/Float(count))
			
			while scales > 15 {
				count += 5
				scales = Int(maxTime/Float(count))
			}
			return scales
 		}
	}
	
	func path(in rect: CGRect) -> Path {
		
		return Path { path in
			
			for y in stride(from: rect.minY, through: rect.maxY, by: rect.height/4) {
				path.move(to: CGPoint(x: rect.minX, y: y))
				path.addLine(to: CGPoint(x: rect.maxX, y: y))
			}
			
			for x in stride(from: rect.minX, to: rect.maxX, by: floor(rect.width / CGFloat(numScales))) {
				path.move(to: CGPoint(x: x, y: rect.minY))
				path.addLine(to: CGPoint(x: x, y: rect.maxY))
			}
		}
	}
}

struct GuideGraphGridHorizontal: Shape {
	
	func path(in rect: CGRect) -> Path {
		
		return Path { path in
			
			for y in stride(from: rect.minY, through: rect.maxY, by: rect.height/4) {
				path.move(to: CGPoint(x: rect.minX, y: y))
				path.addLine(to: CGPoint(x: rect.maxX, y: y))
			}
		}
	}
}

struct VLine: Shape {
	
	func path(in rect: CGRect) -> Path {
		
		return Path { path in
			
			path.move(to: CGPoint(x: rect.midX, y: rect.minY))
			path.addLine(to: CGPoint(x: rect.midX, y: rect.maxY))
		}
	}
}

struct HLine: Shape {
	
	func path(in rect: CGRect) -> Path {
		
		return Path { path in
			
			path.move(to: CGPoint(x: rect.minX, y: rect.midY))
			path.addLine(to: CGPoint(x: rect.maxX, y: rect.midY))
		}
	}
}


struct GuideGraphCorrection: Shape {
	let scaleY: Float
	var values: [GuideGraphData]
	var lineType: GuideGraphLineType
	var maxTime: Float
	
	func getPoints(in rect: CGRect) -> [CGPoint] {
		let sx = rect.width/CGFloat(maxTime)
		let sy = 0.5*rect.height/CGFloat(scaleY)
		
		var points = [CGPoint]()
		for value in values.filter({ !$0.drop }) {
			let val = lineType == .ra ? value.pulseRA : value.pulseDEC
			let x = rect.minX + CGFloat(value.time) * sx + (lineType == .ra ? -2.5 : 2.5)
			let y = rect.midY + sy * CGFloat(val)
			
			points.append(CGPoint(x: x, y: y))
		}
		return points
	}
	
	func path(in rect: CGRect) -> Path {
		
		return Path { path in
			for pt in getPoints(in: rect) {
				path.move(to: CGPoint(x: pt.x, y: rect.midY))
				path.addLine(to: pt)
			}
		}
	}
}





enum GuideGraphLineType {
	case ra
	case dec
	case snr
}

struct GuideGraphLine: Shape {
	let scaleY: Float
	var values: [GuideGraphData]
	var lineType: GuideGraphLineType
	var maxTime: Float
	
	func getPoints(in rect: CGRect) -> [CGPoint] {
		let sx = rect.width/CGFloat(maxTime)
		let sy = (0.5*rect.height)/CGFloat(lineType == .snr ? 1.2 : 0.5 * scaleY)
		
		var points = [CGPoint]()
		for value in values.filter({ !$0.drop }) {
			let val = value.getValue(lineType)
			let x = rect.minX + CGFloat(value.time) * sx
			let y = rect.midY + sy * CGFloat(val)
			
			points.append(CGPoint(x: x, y: y))
		}
		return points
	}
	
	
	func path(in rect: CGRect) -> Path {
		
		return Path { path in
			for (index,pt) in getPoints(in: rect).enumerated() {
				if index == 0 {
					path.move(to: pt)
				} else {
					path.addLine(to: pt)
				}
			}
		}
	}
}

struct GuidingIssueShape: Shape {
	var values: [GuidingIssueGraphData]
	var maxTime: Float
	
	func getRects(in rect: CGRect) -> [CGRect] {
		let sx = rect.width/CGFloat(maxTime)
		
		var result = [CGRect]()
		
		for value in values {
			let x1 = rect.minX + CGFloat(value.time1) * sx
			let x2 = rect.minX + CGFloat(value.time2) * sx
			result.append(CGRect(x: x1, y: 0, width: (x2-x1), height: rect.height))
		}
		return result
	}
	
	
	func path(in rect: CGRect) -> Path {
		
		return Path { path in
			for rect in getRects(in: rect) {
				path.addRect(rect)
			}
		}
	}
	
	
}


@Observable
fileprivate class GuideGraphDataModel {
	var data: [GuideGraphData] = []
	var issues: [GuidingIssueGraphData] = []
	
	var id: UUID = UUID()
	
	fileprivate func update(for image: SessionImage?, session: SessionData) async {
		if let image = image {
			async let dataTask = session.getGuideGraphData(for: image)
			async let issuesTask = self.getIssues(for: image)
			
			let result = await [dataTask, issuesTask] as [Any]
			let data = result.compactMap { $0 as? [GuideGraphData] }.first
			let issues = result.compactMap { $0 as? [GuidingIssueGraphData] }.first
			
			await MainActor.run {
				self.data = data ?? []
				self.issues = issues ?? []
				self.id = UUID()
			}
		} else {
			await MainActor.run {
				self.data = []
				self.issues = []
				self.id = UUID()
			}
		}
	}
	
	private func getIssues(for image: SessionImage) async -> [GuidingIssueGraphData] {
		var results = [GuidingIssueGraphData]()
		
		if let issues = image.guidingIssues {
			guard let jd1 = image.begin else {
				return results
			}
			
			if let ra = issues.ra {
				if let insufficient = ra.insufficentMovements {
					results.append(contentsOf: GuidingFrame.extract(jd: jd1, data: insufficient, type: .insufficientRA))
				}
				
				if let overshoot = ra.overshootMovements {
					results.append(contentsOf: GuidingFrame.extract(jd: jd1, data: overshoot, type: .overshootRA))
				}
			}
		
			if let dec = issues.dec {
				if let insufficient = dec.insufficentMovements {
					results.append(contentsOf: GuidingFrame.extract(jd: jd1, data: insufficient, type: .insufficientDEC))
				}
				
				if let overshoot = dec.overshootMovements {
					results.append(contentsOf: GuidingFrame.extract(jd: jd1, data: overshoot, type: .overshootDEC))
				}
			}
		}
		
		return results
	}
	
	
	
}


struct GuideGraphView: View {
	var session: SessionData
	var selectedimage: SessionImage? = nil
	@AppStorage("GuideGraphScale") var scale = 4
	@AppStorage("GuideGraphBullsEye") var bullsEye: Bool = false
	@Environment(\.colorScheme) var colorScheme
	@State private var model = GuideGraphDataModel()
	
	func getMaxTime() -> Float {
		if let image = selectedimage, let exposure = image.exposure {
			return exposure
		}
		return 100
	}
		
    var body: some View {
		
		VStack {
			
			HStack {
				
				Toggle(isOn: $bullsEye) {
					Text("Hit distribution")
				}
				.toggleStyle(.checkbox)
				
				Picker("Scale", selection: $scale) {
					Text("16\"").tag(16)
					Text("8\"").tag(8)
					Text("4\"").tag(4)
					Text("2\"").tag(2)
					Text("1\"").tag(1)
				}
				.frame(width: 120)
				.padding(.horizontal)
				
				HStack {
					HLine()
						.strokeColor(Color.blue, lineWidth: 3.0)
						.frame(width: 20, height: 20)
					
					Text("α")
						.foregroundColor(Color.blue)
				}
				.padding(.horizontal)
				
				HStack {
					HLine()
						.strokeColor(Color.red, lineWidth: 3.0)
						.frame(width: 20, height: 20)

					Text("δ")
						.foregroundColor(Color.red)
				}
				.padding(.horizontal)
				
				HStack {
					HLine()
						.strokeColor(Color.primary.opacity(0.8), lineWidth: 1.0)
						.frame(width: 20, height: 20)

					Text("SNR")
						.foregroundColor(Color.primary.opacity(0.8))
				}
				.padding(.horizontal)
				
				Spacer()
			}
			.padding(.horizontal)
			.padding(.vertical, 5)
			.background(Color.darkBlue.gradient.opacity(0.3))
			
			HStack(spacing: 0) {
				
				if bullsEye, let image = selectedimage {
					GuidingBullEyeView(session: session, selectedImage: image)
						.aspectRatio(1.0, contentMode: .fit)
						.padding()
				}
				
				ZStack(alignment: .topLeading) {
					Group {
						
						let maxTime = getMaxTime()
						
						GuideGraphGrid(maxTime: getMaxTime(), timeScale: nil)
							.strokeColor(Color.gray, lineWidth: 0.5)
						
						GuideGraphCorrection(scaleY: Float(scale), values: model.data, lineType: .ra, maxTime: maxTime)
							.strokeColor(Color.blue.opacity(0.5), lineWidth: 4.0)
						
						GuideGraphCorrection(scaleY: Float(scale), values: model.data, lineType: .dec, maxTime: maxTime)
							.strokeColor(Color.red.opacity(0.5), lineWidth: 4.0)
						
						GuidingIssueShape(values: model.issues, maxTime: maxTime)
							.fillColor(Color.pink.opacity(0.2))
						
						GuideGraphLine(scaleY: Float(scale), values: model.data, lineType: .ra, maxTime: maxTime)
							.strokeColor(Color.blue, lineWidth: 2.0)
						
						GuideGraphLine(scaleY: Float(scale), values: model.data, lineType: .dec, maxTime: maxTime)
							.strokeColor(Color.red, lineWidth: 2.0)
						
						
						GuideGraphLine(scaleY: Float(scale), values: model.data, lineType: .snr, maxTime: maxTime)
							.strokeColor(Color.primary.opacity(0.8), lineWidth: 1.0)
						
						
						
					}
					.clipped()
					.background(Color.blue.opacity(0.01))
					.padding()
					.id(model.id)
					
				}
				.clipped()
				
				VStack(alignment: .trailing) {
					Text("+\(Float(scale), specifier: "%.1f")\"")
					Spacer()
					Text("+\(Float(scale)/2, specifier: "%.1f")\"")
					Spacer()
					
					Text("0.0\"")
					Spacer()
					Text("-\(Float(scale)/2, specifier: "%.1f")\"")
					Spacer()
					Text("-\(Float(scale), specifier: "%.1f")\"")
				}
				.fontSize(12, weight: .light)
				.foregroundColor(Color.primary)
				.frame(maxWidth: 40)
				.padding(.vertical)
				.padding(.trailing, 4)
				
			}
			.padding(.horizontal)
			.background(colorScheme == .dark ? Color.black : Color.controlBackgroundDarkColor)
			.drawingGroup(opaque: true)
			
		}
		.task {
			await model.update(for: self.selectedimage, session: self.session)
		}
		.onChange(of: self.selectedimage) { _, _ in
			Task {
				await model.update(for: self.selectedimage, session: self.session)
			}
		}
    }
}

struct ImageFrameShape: Shape {
	let beginImage: JulianDate
	let endImage: JulianDate
	let beginGuiding: JulianDate
	let endGuiding: JulianDate
	
	func path(in rect: CGRect) -> Path {
		let width = rect.width
		let duration = endGuiding.jd - beginGuiding.jd
		let sx = width / duration
		let x1 = (beginImage.jd - beginGuiding.jd) * sx
		let x2 = (endImage.jd - beginGuiding.jd) * sx
	
		return Path { path in
			path.addRect(CGRect(x: x1, y: rect.minY, width: x2-x1, height: 20))
		}
	}
}

struct ImageFrameLabel: View {
	let image: SessionImage
	let beginGuiding: JulianDate
	let endGuiding: JulianDate
	let width: CGFloat
	
	func getPosition() -> CGPoint {
		let duration = endGuiding.jd - beginGuiding.jd
		let sx = width / duration
		let x1 = (image.begin!.jd - beginGuiding.jd) * sx
		let x2 = (image.end!.jd - beginGuiding.jd) * sx
		
		return CGPoint(x: (x1 + x2)/2.0, y: 10)
	}
	
	var body: some View {
		Text("\(image.startExposure!.formatted(date: .omitted, time: .shortened)): \(image.FileName)")
			.font(.callout)
			.foregroundColor(image.bad ? .badColor.lighter() : Color.primary)
			.truncationMode(.middle)
			.position(getPosition())
			.frame(maxWidth: width)
	}
}


struct GuidingImageShapes: View {
	@State var session: SessionData
	var guidingSession: GuidingData
	let width: CGFloat
	@Environment(\.openWindow) private var openWindow
	@Environment(\.dismissWindow) private var dismissWindow
	
	var images: [SessionImage] {
		let result = session.getImages(for: guidingSession.id)
							.filter({ $0.begin != nil && $0.end != nil })
		return result
	}
	
	var body: some View {
		
		ForEach(self.images) { image in
			ImageFrameShape(beginImage: image.begin!, endImage: image.end!, beginGuiding: guidingSession.guidingStart, endGuiding: guidingSession.guidingEnd)
				.foregroundColor(image.bad ? .badColor : Color.green.opacity(0.5))
				.overlay {
					ImageFrameLabel(image: image, beginGuiding: guidingSession.guidingStart, endGuiding: guidingSession.guidingEnd, width: width)
						.clipped()
				}
				.contextMenu {
					
					
					Button {
						dismissWindow(id: "InternalViewer")
						openWindow(id: "InternalViewer", value: image.id)
					} label: {
						Text("Show in Viewer")
					}
					
					if self.session.quickFitsAvailable {
						Button {
							self.session.openInQuickFits( image )
						} label: {
							Text("Open in QuickFits")
						}
					}
					
					if self.session.fitsPreviewAvailable {
						Button {
							self.session.openInFitsPreview( image )
						} label: {
							Text("Open in FITS Preview")
						}
					}
					
					if self.session.pixInsightAvailable {
						Button {
							self.session.openInPixInsight( image )
						} label: {
							Text("Open in PixInsight")
						}
					}
					
					if self.session.astapAvailable {
						Button {
							self.session.openInAstap( image )
						} label: {
							Text("Open in Astap")
						}
					}
				}
				
		}
		.drawingGroup(opaque: true)
	}
	
}

struct GuidingTimeString {
	let text: String
	let time: Date
	let jdd: JulianDate
}


@Observable
class GuidingSessionModel {
	var data: [GuideGraphData] = []
	var issues: [GuidingIssueGraphData] = []
	var dither: [DitherGraphData] = []
	var lostStars: [LostStarEvents] = []
	
	func update(session: SessionData, guidingSession: GuidingData) async {
		let data = await self.getData(guidingSession)
		let issues = await self.getIssues(session, guidingSession)
		let dither = self.getDitherGraphData(guidingSession)
		let lostStars = self.getLostStarGraphData(session, guidingSession)
		
		await MainActor.run {
			self.data = data
			self.issues = issues
			self.dither = dither
			self.lostStars = lostStars
		}
	}
	
	private func getDitherGraphData(_ guidingSession: GuidingData) -> [DitherGraphData] {
		return guidingSession.getDitherGraphData()
	}
	
	private func getLostStarGraphData(_ session: SessionData, _ guidingSession: GuidingData) -> [LostStarEvents] {
		return session.guiding.lostStarEvents(guidingSession.id)
	}
	
	
	private func getData(_ guidingSession: GuidingData) async -> [GuideGraphData] {
		return guidingSession.getGuideGraphData()
	}
	
	private func getIssues(_ session: SessionData, _ guidingSession: GuidingData) async -> [GuidingIssueGraphData] {
		var results = [GuidingIssueGraphData]()
		
		let images = session.getImages(for: guidingSession.id)
		let jd1 = guidingSession.guidingStart
		
		for image in images {
			
			if let issues = image.guidingIssues {
				
				if let ra = issues.ra {
					if let insufficient = ra.insufficentMovements {
						results.append(contentsOf: GuidingFrame.extract(jd: jd1, data: insufficient, type: .insufficientRA))
					}
					
					if let overshoot = ra.overshootMovements {
						results.append(contentsOf: GuidingFrame.extract(jd: jd1, data: overshoot, type: .overshootRA))
					}
				}
			
				if let dec = issues.dec {
					if let insufficient = dec.insufficentMovements {
						results.append(contentsOf: GuidingFrame.extract(jd: jd1, data: insufficient, type: .insufficientDEC))
					}
					
					if let overshoot = dec.overshootMovements {
						results.append(contentsOf: GuidingFrame.extract(jd: jd1, data: overshoot, type: .overshootDEC))
					}
				}
			}
		}
		
		return results
	}
}


struct GuideGraphSessionView: View {
	@State var session: SessionData
	var guidingSession: GuidingData
	@Binding var scaleX: Double
	@Binding var scaleY: Int
	@Binding var raEnabled: Bool
	@Binding var decEnabled: Bool
	@Binding var snrEnabled: Bool
	@Binding var correctionsEnabled: Bool
	@Binding var ditheringEnabled: Bool
	@Binding var lostStarsEnabled: Bool
	@Binding var imagesEnabled: Bool
	@Binding var issuesEnabled: Bool
	
	
	@State private var model = GuidingSessionModel()
	
	@Environment(\.colorScheme) var colorScheme
		
	var begin: Date {
		return self.guidingSession.guidingStart.toDateTime()
	}
	
	func getMaxTime() -> Float {
		return Float(self.guidingSession.guidingEnd.jd - self.guidingSession.guidingStart.jd)*86400.0
	}
	
	func getTimeStrings() -> [GuidingTimeString] {
		var result = [GuidingTimeString]()
		
		let jd0 = self.guidingSession.guidingStart
		let maxTime = (guidingSession.guidingEnd.jd - guidingSession.guidingStart.jd)*86400.0
		let timeZone = session.currentTimeZone
		for t in stride(from: 0.0, to: maxTime, by: 15.0) {
			let jdd = jd0.addSeconds(t)
			let time = jdd.toDateTime()
			result.append(
				GuidingTimeString(text: jdd.toDateTime(timeZone).formatted(date: .omitted, time: .standard),
								  time: time, jdd: jdd))
		}
		return result
	}
	
	func getWidth(_ minWidth: CGFloat) -> CGFloat {
		return max(CGFloat(getMaxTime()*Float(scaleX)), minWidth)
	}
	
	func getScrollPosition(_ time: Date, width: CGFloat) -> Int {
		let sx = width / CGFloat(getMaxTime())
		let jd0 = JulianDate.fromDate(date: self.begin)
		let jd1 = JulianDate.fromDate(date: time)
		let time = (jd1.jd - jd0.jd) * 86400.0
			
		return Int(time * sx)
	}
	
	var guidingLines: some View {
		Group {
			
			let guideData = model.data
			let issues = model.issues
			let maxTime = getMaxTime()
			let scale = Float(scaleY)
			
			if issuesEnabled {
				GuidingIssueShape(values: issues, maxTime: maxTime)
					.fillColor(Color.pink.opacity(0.2))
			}
			
			if correctionsEnabled {
				
				if raEnabled {
					GuideGraphCorrection(scaleY: scale, values: guideData, lineType: .ra, maxTime: maxTime)
						.strokeColor(Color.blue.opacity(0.5), lineWidth: 4.0)
				}
				
				if decEnabled {
					GuideGraphCorrection(scaleY: scale, values: guideData, lineType: .dec, maxTime: maxTime)
						.strokeColor(Color.red.opacity(0.5), lineWidth: 4.0)
				}
			}
			
			if raEnabled {
				GuideGraphLine(scaleY: scale, values: guideData, lineType: .ra, maxTime: maxTime)
					.strokeColor(Color.blue, lineWidth: 2.0)
			}
			
			if decEnabled {
				GuideGraphLine(scaleY: scale, values: guideData, lineType: .dec, maxTime: maxTime)
					.strokeColor(Color.red, lineWidth: 2.0)
			}
			
			if snrEnabled {
				GuideGraphLine(scaleY: scale, values: guideData, lineType: .snr, maxTime: maxTime)
					.strokeColor(Color.primary.opacity(0.8), lineWidth: 1.0)
			}
			
			
		}
	}
	
	var body: some View {
		
		GeometryReader { g in
			HStack(spacing: 0) {
				ScrollView(.horizontal) {
					ZStack(alignment: .topLeading) {
						
						if ditheringEnabled {
							
							ForEach(model.dither, id: \.self.id) { dither in
								GuideDitherShape(dither: dither, maxTime: getMaxTime())
									.fill(Color.gray.opacity(0.4))
									.id(dither.jdd)
							}
						}
						
						if lostStarsEnabled {
							ForEach(model.lostStars, id: \.self.id) { lostStar in
								LostStarLine(lost: lostStar, maxTime: getMaxTime(), begin: self.guidingSession.guidingStart)
									.strokeColor(Color.purple.opacity(0.6), lineWidth: 4.5)
									.id(lostStar.jd)
							}
						}
						
						GuideGraphGridHorizontal()
							.strokeColor(Color.gray, lineWidth: 0.5)
						
						VStack(alignment: .leading) {
							Spacer()
							
							HStack {
								ForEach(getTimeStrings(), id: \.self.time) { t in
									ZStack {
										VLine().strokeColor(Color.gray.opacity(0.6), lineWidth: 1.0)
										
										VStack {
											Spacer()
											Text(t.text)
												.font(.footnote).fontWeight(.bold)
												.foregroundColor(.primary)
												.shadowColor(Color.controlBackgroundColor, 4)
												.shadowColor(Color.controlBackgroundColor, 3)
												.shadowColor(Color.controlBackgroundColor, 1)
												.padding(.bottom, 8)
										}
										.id(t.jdd)
									}
									Spacer()
								}
								
								
							}
						}
						
						guidingLines
						
						if imagesEnabled {
							GuidingImageShapes(session: session, guidingSession: self.guidingSession, width: getWidth(g.size.width))
						}
						
						
					}
					.frame(width: getWidth(g.size.width))
					.clipped()
					.padding(.vertical, 2)
					.drawingGroup(opaque: true)
				}
				
				VStack(alignment: .trailing) {
					Text("+\(Float(scaleY), specifier: "%.1f")\"")
					Spacer()
					Text("+\(Float(scaleY)/2, specifier: "%.1f")\"")
					Spacer()
					
					Text("0.0\"")
					Spacer()
					Text("-\(Float(scaleY)/2, specifier: "%.1f")\"")
					Spacer()
					Text("-\(Float(scaleY), specifier: "%.1f")\"")
				}
				.font(.footnote)
				.fontWeight(.medium)
				.foregroundColor(Color.primary)
				.frame(width: 36)
				.padding(.vertical, 2)
				.padding(.horizontal, 8)
			}
			.background(Color.controlBackgroundDarkColor)
			.background(Color.blue.opacity(0.05))
		}
		.task {
			await model.update(session: self.session, guidingSession: self.guidingSession)
		}
		.onChange(of: self.session.id) { _, _ in
			Task {
				await model.update(session: self.session, guidingSession: self.guidingSession)
			}
		}
		.onChange(of: self.guidingSession.id) { _, _ in
			Task {
				await model.update(session: self.session, guidingSession: self.guidingSession)
			}
		}
	
	}
}
