//
//  LostTimePieChart.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 14.07.24.
//
import SwiftUI
import Charts

struct LostTimePieChart: View {
	@State var statistics: StatisticsDataSource
	@State private var selectedPart: Double? = nil
	@State private var selectedValue: PercentageValue? = nil
	
	
	func findSelected(_ value: Double)  {
		var sum = 0.0
		for item in self.statistics.percentageValues {
			sum += item.value
			if sum >= Double(value) {
				self.selectedValue = item
				break
			}
		}
		
	}
	
	var body: some View {
		VStack {
			Chart(statistics.percentageValues) { time in
				
				SectorMark(
					angle: .value(
						time.name,
						time.value
					),
					innerRadius: .ratio(0.6),
					outerRadius: self.selectedValue?.name == time.name ? .ratio(1.2) : .ratio(1.0),
					angularInset: 2
				)
				.foregroundStyle(
					by: .value(
						Text(time.name),
						time.name
					)
				)
				.cornerRadius(8)
				
			}
			.chartLegend(.visible)
			.chartAngleSelection(value: $selectedPart)
			.chartBackground(content: { _ in
				if let selectedValue {
					VStack {
						Text(selectedValue.name)
						Text("\(selectedValue.value, specifier: "%.1f")%")
					}
				}
			})
			.padding()
			.cornerRadius(8)
			
			
			
		}
		.onChange(of: self.selectedPart) { _, newValue in
			if let newValue {
				withAnimation(.easeInOut(duration: 0.2)) {
					findSelected(newValue)
				}
				
			}
		}
		
	}
}
