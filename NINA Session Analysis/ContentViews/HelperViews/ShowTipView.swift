//
//  ShowTipView.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 22.05.24.
//

import SwiftUI
import TipKit

struct ShowTipView: View {
	let tip: any Tip
	
    var body: some View {
		HStack {
			VStack(alignment: .leading) {
				tip.title.bold().foregroundColor(Color.lightBlue)
				
				if tip.message != nil {
					Divider()
					tip.message
				}
			}
			.padding()
			.background(.ultraThickMaterial)
			.cornerRadius(8)
			
			Spacer(minLength: 40)
		}
		
    }
}


