//
//  SessionImageStatisticsView.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 14.04.24.
//

import SwiftUI
import Charts

struct ImageCount: Identifiable {
	let id = UUID()
	
	let sessionDate: String
	let target: String
	let filter: String
	let exposure: String
	let count: Int
	let totalExposure: Float
}

@Observable
final class SesionImageStatisticsModel {
	var images: [ImageCount] = []
	var filters: [String] = []
	var exposures: [String] = []
	var targets: [String] = []
	var dates: [String] = []
	var colors: [Color] = []
	
	func update(session: SessionData) async {
		let images = getImages(session: session)
		let filters = images.compactMap({ $0.filter}).distinct()
		let exposures = images.compactMap({ $0.exposure }).distinct()
		let targets = images.compactMap({ $0.target }).distinct()
		let dates = images.compactMap({ $0.sessionDate }).distinct()
		let colors = createDomainColors(session: session, filters: filters)
		
		await MainActor.run {
			self.images = images
			self.filters = filters
			self.exposures = exposures
			self.targets = targets
			self.dates = dates
			self.colors = colors
		}
	}
	
	private func createDomainColors(session: SessionData, filters: [String]) -> [Color] {
		var colors = [Color]()
		let filterColors = session.filterColors
		
		if filters.count > 0 && filterColors.count > 0 {
			for filter in filters {
				if let fc = filterColors.first(where: { $0.filterName == filter }) {
					colors.append(fc.color)
				} else {
					colors.append(Color.gray)
				}
			}
		}
		return colors
	}
	
	
	func reset() {
		self.images = []
		
	}
	
	private func getImages(session: SessionData) -> [ImageCount] {
		let filters = session.filters
		let exposures = session.exposures
		let dates = session.sessionDates
		let targets = session.targets.filter({ $0 != "" && $0 != "Any" })
		
		var result = [ImageCount]()
		
		let sessionImages = session.sessionImages
		if sessionImages.count > 0 {
			for date in dates {
				for target in targets {
					for filter in filters {
						for exposure in exposures {
							let images = sessionImages.filter({ ($0.target == target)
																&& $0.SessionDate == date
																&& $0.filter == filter
																&& $0.Exposure == exposure})
							let count = images.count
							if count > 0 {
								let totalExposure = images.compactMap({ $0.exposure }).reduce(0.0, +)
								result.append(ImageCount(sessionDate: date.text,
														 target: target,
														 filter: filter,
														 exposure: exposure,
														 count: count, totalExposure: totalExposure))
							}
						}
					}
				}
			}
		}
		return result
	}
	
}


struct SessionImageStatisticsView: View {
	@State var session: SessionData
	@State var model = SesionImageStatisticsModel()
	
	func getExposure(_ exp: Float) -> String {
		if exp < 60.0 {
			return String(format: "%.0fs", exp)
		} else if exp < 3600.0 {
			let mins = exp/60.0
			let min = Int(mins)
			let sec = frac(Double(mins))*60.0
			
			return String(format: "%dmin, %.0fs", min, sec)
		} else {
			let hours = exp/3600.0
			var h = Int(hours)
			let mins = frac(Double(hours))*60.0
			var min = Int(mins)
			var sec = Int(frac(Double(mins))*60.0)
			if sec >= 60 {
				sec -= 60
				min += 1
				if min >= 60 {
					min -= 60
					h += 1
				}
			}
			
			return String(format: "%dh %dmin, %ds", h, min, sec)
		}
	}
	
	func getStyle(_ image: ImageCount) -> PlottableValue<String> {
		
		if model.targets.count > 1 {
			if model.filters.count > 1 {
				if model.exposures.count > 1 {
					if model.dates.count > 1 {
						return PlottableValue.value("Filter, Exposure, Target, Date", "\(image.filter),\(image.exposure),\(image.target),\(image.sessionDate)")
					}
					
					return PlottableValue.value("Filter, Exposure, Target", "\(image.filter),\(image.exposure),\(image.target)")
				}
				return PlottableValue.value( "Filter, Target", "\(image.filter),\(image.target)")
			}
			
			return PlottableValue.value("Target", image.target)
		}
		
		if session.filters.count > 1 {
			if model.exposures.count > 1 {
				if model.dates.count > 1 {
					return PlottableValue.value("Filter, Exposure, Date", "\(image.filter),\(image.exposure),\(image.sessionDate)")
				}
				return PlottableValue.value( "Filter, Exposure", "\(image.filter),\(image.exposure)")
			}
			
			return PlottableValue.value("Filter", image.filter)
		}
		
		if session.sessionDates.count > 1 {
			if model.exposures.count > 1 {
				return PlottableValue.value( "Date, Exposure", "\(image.sessionDate),\(image.exposure)")
			}
			
			return PlottableValue.value("Date", image.sessionDate)
		}
		
		return PlottableValue.value("Exposure", image.exposure)
	}
	
	func getDomain() -> [String] {
		return model.filters
	}
	
	func getDomainColors() -> [AnyGradient] {
		var colors = [AnyGradient]()
		let filterColors = session.filterColors
		
		if model.filters.count > 0 && filterColors.count > 0 {
			for filter in model.filters {
				if let fc = filterColors.first(where: { $0.filterName == filter }) {
					colors.append(fc.color.gradient)
				} else {
					colors.append(Color.gray.gradient)
				}
			}
		} else {
			colors.append(Color.gray.gradient)
		}
		
		return colors
	}
	
		
	var body: some View {
		ScrollView(.vertical) {
			VStack {
				Spacer(minLength: 40)
				
				
				if model.images.count > 0 {
					if model.filters.count > 1 {
						
						
						Chart(model.images) { image in
							BarMark(
								x: .value("Filter", image.filter),
								y: .value("Exposures", image.count)
								
							)
							.foregroundStyle(by: getStyle(image))
							.position(by: getStyle(image))
							.annotation {
								VStack {
									Text("\(image.count) images")
									Text(image.filter)
									Text("\(getExposure(image.totalExposure))")
								}.font(.caption)
							}
						}
						.chartPlotStyle { plotContent in
							plotContent
								.background(.chartBackground.gradient.opacity(0.5))
						}
							
						.frame(height: 400)
						.padding()
						.padding(.bottom)
						
					} else {
						Chart(model.images) { image in
							BarMark(
								x: .value("Filter", image.filter),
								y: .value("Exposures", image.count)
								
							)
							.foregroundStyle(by: getStyle(image))
							.position(by: getStyle(image))
							.annotation {
								VStack {
									Text("\(image.count) images")
									Text(image.filter)
									Text("\(getExposure(image.totalExposure))")
								}.font(.caption)
							}
						}
						.chartPlotStyle { plotContent in
							plotContent
								.background(.chartBackground.gradient.opacity(0.5))
						}
						.frame(height: 400)
						.padding()
						.padding(.bottom)
						
					}
				}
				 
				
			}
		}
		.task {
			await self.model.update(session: session)
		}
		.onChange(of: session.id) { _, _ in
			
			if session.sessionImages.count > 0 {
				Task {
					await self.model.update(session: session)
				}
			} else {
				self.model.reset()
			}
		}
    }
}


