//
//  LostTimeBarChart.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 14.07.24.
//

import SwiftUI
import Charts

struct LostPauseTimeBarChart: View {
	@State var statistics: StatisticsDataSource
	@Binding var selectedPause: PauseTime?
	@State private var selectedTime: Date? = nil
	
	func findSelectedPause(_ value: Date) {
		self.selectedPause = self.statistics.capturePauseTimes.first(where: { abs($0.time.distance(to: value)) < 30.0 })
	}
	
	var body: some View {
		Chart {
			
			ForEach(self.statistics.capturePauseTimes) { pause in
				BarMark(
					x: .value("Time", pause.time),
					y: .value("Pause", pause.value),
					width: .fixed(20),
					stacking: .unstacked
					
				)
				.foregroundStyle(Color.cyan.gradient)
				.annotation(position: .top, overflowResolution: AnnotationOverflowResolution(x: .disabled, y: .disabled)) {
					Group {
						if pause.value <= 180.0 {
							Text("\(pause.value, specifier: "%.1f")s").font(.footnote).foregroundColor(Color.cyan)
						} else {
							Text("\(pause.value/60.0, specifier: "%.1f")min").font(.footnote).foregroundColor(Color.cyan)
						}
					}
				}
			}
			
			if let selected = self.selectedPause {
				BarMark(
					x: .value("Time", selected.time),
					y: .value("Pause", selected.value),
					width: .fixed(20),
					stacking: .unstacked
				)
				.foregroundStyle(Color.orange.gradient)
				.accessibilityHidden(true)
				.annotation(position: .top, overflowResolution: AnnotationOverflowResolution(x: .disabled, y: .disabled)) {
					VStack(alignment: .center) {
						Text("\(selected.time.formatted(date: .omitted, time: .shortened))")
						Text("\(ValueFormatter.formatTimeSpan(selected.value, timeStyle: .full))")
					}
					.font(.footnote)
					.padding(4)
					.background(.ultraThinMaterial).cornerRadius(8)
					.zIndex(100)
					
				}
				
			}
			
		}
		.chartLegend(.visible)
		.chartXScale(domain: statistics.minTime...statistics.maxTime)
		.chartXSelection(value: $selectedTime)
		.chartYAxis {
			AxisMarks(position: .leading)
			AxisMarks(position: .trailing)
		}
		.chartPlotStyle { plotContent in
			plotContent
				.background(.chartBackground.gradient.opacity(0.5))
		}
		.onChange(of: selectedTime, { _, newValue in
			if let newValue {
				withAnimation(.easeInOut(duration: 0.2)) {
					findSelectedPause(newValue)
				}
			}
		})
	}
}


struct DitherTimesBarChart: View {
	@State var statistics: StatisticsDataSource
	@Binding var selectedPause: StatisticsTimespan?
	@State private var selectedTimeRange: ClosedRange<Date>? = nil
	@State private var selectedTime: Date? = nil
	
	func findSelectedPause(_ value: Date) {
		self.selectedPause = self.statistics.ditherTimes.first(where: { $0.start <= value && $0.end >= value })
	}
	
	
	var maxDuration: Double {
		(statistics.ditherTimes.max(by: { $0.duration < $1.duration })?.duration ?? 30.0) * 1.1
	}
	
	var body: some View {
		
		Chart(statistics.ditherTimes) { dither in
			BarMark(
				x: .value("Time", dither.start, unit: .minute),
				y: .value("Duration", dither.duration),
				width: .fixed(20),
				stacking: .unstacked
				
			)
			.foregroundStyle(Color.green.gradient)
			.annotation(position: .top, overflowResolution: AnnotationOverflowResolution(x: .disabled, y: .disabled)) {
				
				Group {
					if dither.duration <= 180.0 {
						Text("\(dither.duration, specifier: "%.1f")s").font(.footnote).foregroundColor(Color.green)
					} else {
						Text("\(dither.duration/60.0, specifier: "%.1f")min").font(.footnote).foregroundColor(Color.green)
					}
				}
			}
			
			if let selected = self.selectedPause {
				BarMark(
					x: .value("Time", selected.start, unit: .minute),
					y: .value("Pause", selected.duration),
					width: .fixed(20),
					stacking: .unstacked
				)
				.foregroundStyle(Color.orange.gradient)
				.accessibilityHidden(true)
				.annotation(position: .top, overflowResolution: AnnotationOverflowResolution(x: .disabled, y: .disabled)) {
					VStack(alignment: .center) {
						Text("\(selected.start.formatted(date: .omitted, time: .shortened)) - \(selected.end.formatted(date: .omitted, time: .shortened))")
						Text("\(ValueFormatter.formatTimeSpan(selected.duration, timeStyle: .full))")
					}
					.font(.footnote)
					.padding(4)
					.background(.ultraThinMaterial).cornerRadius(8)
					.zIndex(100)
				}
			}
		}
		.chartLegend(.visible)
		.chartXScale(domain: statistics.minTime...statistics.maxTime)
		.chartYScale(domain: 0.0...maxDuration)
		.chartYAxis {
			AxisMarks(position: .leading)
			AxisMarks(position: .trailing)
		}
		.chartXSelection(value: $selectedTime)
		.chartPlotStyle { plotContent in
			plotContent
				.background(.chartBackground.gradient.opacity(0.5))
		}
		.onChange(of: selectedTime, { _, newValue in
			if let newValue {
				withAnimation(.easeInOut(duration: 0.2)) {
					findSelectedPause(newValue)
				}
			}
		})
		
	}
}

struct AFTimesBarChart: View {
	@State var statistics: StatisticsDataSource
	@Binding var selectedPause: StatisticsTimespan? 
	@State private var selectedTime: Date? = nil
	
	func findSelectedPause(_ value: Date) {
		self.selectedPause = self.statistics.autoFocusTimes.first(where: { $0.start <= value && $0.end >= value })
	}
	
	
	var body: some View {
		Chart(statistics.autoFocusTimes) { af in
			BarMark(
				x: .value("Time", af.start),
				y: .value("Duration", af.duration),
				width: .fixed(20),
				stacking: .unstacked
				
			)
			.foregroundStyle(Color.mint.gradient)
			.annotation(position: .top, overflowResolution: AnnotationOverflowResolution(x: .disabled, y: .disabled)) {
				Group {
					if af.duration <= 180.0 {
						Text("\(af.duration, specifier: "%.1f")s").font(.footnote).foregroundColor(Color.mint)
					} else {
						Text("\(af.duration/60.0, specifier: "%.1f")min").font(.footnote).foregroundColor(Color.mint)
					}
				}
			}
			
			if let selected = self.selectedPause {
				BarMark(
					x: .value("Time", selected.start),
					y: .value("Pause", selected.duration),
					width: .fixed(20),
					stacking: .unstacked
					
				)
				.foregroundStyle(Color.orange.gradient)
				.accessibilityHidden(true)
				.annotation(position: .top, overflowResolution: AnnotationOverflowResolution(x: .disabled, y: .disabled)) {
					VStack(alignment: .center) {
						Text("\(selected.start.formatted(date: .omitted, time: .shortened)) - \(selected.end.formatted(date: .omitted, time: .shortened))")
						Text("\(ValueFormatter.formatTimeSpan(selected.duration, timeStyle: .full))")
					}
					.font(.footnote)
					.padding(4)
					.background(.ultraThinMaterial).cornerRadius(8)
					.zIndex(100)
					
				}
			}
		}
		.chartLegend(.visible)
		.chartXScale(domain: statistics.minTime...statistics.maxTime)
		.chartYAxis {
			AxisMarks(position: .leading)
			AxisMarks(position: .trailing)
		}
		.chartXSelection(value: $selectedTime)
		.chartPlotStyle { plotContent in
			plotContent
				.background(.chartBackground.gradient.opacity(0.5))
		}
		.onChange(of: selectedTime, { _, newValue in
			if let newValue {
				withAnimation(.easeInOut(duration: 0.2)) {
					findSelectedPause(newValue)
				}
			}
		})
		
	}
}

