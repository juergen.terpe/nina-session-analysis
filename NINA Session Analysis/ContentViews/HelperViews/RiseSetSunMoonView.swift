//
//  RiseSetSunMoonView.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 10.04.24.
//

import SwiftUI
import CoreLocation


@Observable
class RiseSetSunMoonModel {
	var sunRise: JulianDate?
	var sunSet: JulianDate?
	var moonRise: JulianDate?
	var moonSet: JulianDate?
	var nauticalDawn: JulianDate?
	var nauticalDusk: JulianDate?
	var astronomicalDawn: JulianDate?
	var astronomicalDusk: JulianDate?
	
	func update(jd: JulianDate, location: CLLocation) async {
		let long = location.coordinate.longitude * Constants.Rad
		let lat = location.coordinate.latitude * Constants.Rad
		
		let riseSun = RiseSetTransitFinder.findRise(object: .sun, lambda: long, phi: lat, julianDate: jd)
		let riseMoon = RiseSetTransitFinder.findMoonRise(lambda: long, phi: lat, julianDate: jd)
		let setSun = RiseSetTransitFinder.findSet(object: .sun, lambda: long, phi: lat, julianDate: jd)
		let setMoon = RiseSetTransitFinder.findMoonSet(lambda: long, phi: lat, julianDate: jd)
		let nauticalDawn = RiseSetTransitFinder.findRise(object: .nautical, lambda: long, phi: lat, julianDate: jd)
		let nauticalDusk = RiseSetTransitFinder.findSet(object: .nautical, lambda: long, phi: lat, julianDate: jd)
		let astronomicalDawn = RiseSetTransitFinder.findRise(object: .astronomical, lambda: long, phi: lat, julianDate: jd)
		let astronomicalDusk = RiseSetTransitFinder.findSet(object: .astronomical, lambda: long, phi: lat, julianDate: jd)
		
		await MainActor.run {
			self.sunRise = riseSun
			self.sunSet = setSun
			self.moonRise = riseMoon
			self.moonSet = setMoon
			self.nauticalDawn = nauticalDawn
			self.nauticalDusk = nauticalDusk
			self.astronomicalDawn = astronomicalDawn
			self.astronomicalDusk = astronomicalDusk
		}
	}
}


struct RiseSetSunMoonView: View {
	let jd: JulianDate
	let location: CLLocation
	@State private var model = RiseSetSunMoonModel()
	
	func getSunRise() -> String {
		
		if let rise = model.sunRise {
			return rise.toDateTime().formatted(date: .omitted, time: .shortened)
		}
		return "N/A"
	}
	
	func getMoonRise() -> String {
		if let rise = model.moonRise {
			return rise.toDateTime().formatted(date: .omitted, time: .shortened)
		}
		return "N/A"
	}
	
	func getSunSet() -> String {
		if let set =  model.sunSet {
			return set.toDateTime().formatted(date: .omitted, time: .shortened)
		}
		return "N/A"
	}
	
	func getMoonSet() -> String {
		if let set = model.moonSet {
			return set.toDateTime().formatted(date: .omitted, time: .shortened)
		}
		return "N/A"
	}
	
	func getNauticalDawn() -> String {
		if let dawn = model.nauticalDawn {
			return dawn.toDateTime().formatted(date: .omitted, time: .shortened)
		}
		return "N/A"
	}
	
	func getNauticalDusk() -> String {
		if let dusk = model.nauticalDusk {
			return dusk.toDateTime().formatted(date: .omitted, time: .shortened)
		}
		return "N/A"
	}
	
	func getAstronomicalDawn() -> String {
		if let dawn = model.astronomicalDawn {
			return dawn.toDateTime().formatted(date: .omitted, time: .shortened)
		}
		return "N/A"
	}
	
	func getAstronomicalDusk() -> String {
		if let dusk = model.astronomicalDusk {
			return dusk.toDateTime().formatted(date: .omitted, time: .shortened)
		}
		return "N/A"
	}
	
	
    var body: some View {
		VStack(spacing: 0) {
			HStack {
				Text("").frame(width: 120, alignment: .leading)
				Text("Rise").frame(minWidth: 90, alignment: .trailing)
				Text("Set").frame(minWidth: 90, alignment: .trailing)
			}
			.padding(8)
			.padding(.horizontal)
			Divider()
			
			HStack {
				Image(systemName: "sun.max").foregroundColor(Color.yellow)
					.frame(width: 120, alignment: .leading)
				Text(getSunRise()).frame(minWidth: 90, alignment: .trailing)
				Text(getSunSet()).frame(minWidth: 90, alignment: .trailing)
			}
			.padding(8)
			.padding(.horizontal)
			Divider()
			
			HStack {
				Image(systemName: "moon").foregroundColor(Color.cyan)
					.frame(width: 120, alignment: .leading)
				Text(getMoonRise()).frame(minWidth: 90, alignment: .trailing)
				Text(getMoonSet()).frame(minWidth: 90, alignment: .trailing)
			}
			.padding(8)
			.padding(.horizontal)
			.padding(.bottom, 32)
			
			
			HStack {
				Text("").frame(width: 120, alignment: .leading)
				Text("Dusk").frame(minWidth: 90, alignment: .trailing)
				Text("Dawn").frame(minWidth: 90, alignment: .trailing)
			}
			.padding(8)
			.padding(.horizontal)
			Divider()
			
			HStack {
				Text("Nautical")
					.frame(width: 120, alignment: .leading)
				Text(getNauticalDusk()).frame(minWidth: 90, alignment: .trailing)
				Text(getNauticalDawn()).frame(minWidth: 90, alignment: .trailing)
			}
			.foregroundColor(Color.mint)
			.padding(8)
			.padding(.horizontal)
			Divider()
			
			HStack {
				Text("Astronomical")
					.frame(width: 120, alignment: .leading)
				Text(getAstronomicalDusk()).frame(minWidth: 90, alignment: .trailing)
				Text(getAstronomicalDawn()).frame(minWidth: 90, alignment: .trailing)
			}
			.foregroundColor(Color.green)
			.padding(8)
			.padding(.horizontal)
			
		}
		.task {
			await model.update(jd: self.jd, location: self.location)
		}
		
    }
}


struct RiseSetSunMoonView2: View {
	@Binding var model: RiseSetSunMoonModel
	
	func getSunRise() -> String {
		
		if let rise = model.sunRise {
			return rise.toDateTime().formatted(date: .omitted, time: .shortened)
		}
		return "N/A"
	}
	
	func getMoonRise() -> String {
		if let rise = model.moonRise {
			return rise.toDateTime().formatted(date: .omitted, time: .shortened)
		}
		return "N/A"
	}
	
	func getSunSet() -> String {
		if let set =  model.sunSet {
			return set.toDateTime().formatted(date: .omitted, time: .shortened)
		}
		return "N/A"
	}
	
	func getMoonSet() -> String {
		if let set = model.moonSet {
			return set.toDateTime().formatted(date: .omitted, time: .shortened)
		}
		return "N/A"
	}
	
	func getNauticalDawn() -> String {
		if let dawn = model.nauticalDawn {
			return dawn.toDateTime().formatted(date: .omitted, time: .shortened)
		}
		return "N/A"
	}
	
	func getNauticalDusk() -> String {
		if let dusk = model.nauticalDusk {
			return dusk.toDateTime().formatted(date: .omitted, time: .shortened)
		}
		return "N/A"
	}
	
	func getAstronomicalDawn() -> String {
		if let dawn = model.astronomicalDawn {
			return dawn.toDateTime().formatted(date: .omitted, time: .shortened)
		}
		return "N/A"
	}
	
	func getAstronomicalDusk() -> String {
		if let dusk = model.astronomicalDusk {
			return dusk.toDateTime().formatted(date: .omitted, time: .shortened)
		}
		return "N/A"
	}
	
	
	var body: some View {
		VStack(spacing: 0) {
			HStack {
				Text("").frame(width: 120, alignment: .leading)
				Text("Rise").frame(minWidth: 90, alignment: .trailing)
				Text("Set").frame(minWidth: 90, alignment: .trailing)
			}
			.padding(8)
			.padding(.horizontal)
			Divider()
			
			HStack {
				Image(systemName: "sun.max").foregroundColor(Color.yellow)
					.frame(width: 120, alignment: .leading)
				Text(getSunRise()).frame(minWidth: 90, alignment: .trailing)
				Text(getSunSet()).frame(minWidth: 90, alignment: .trailing)
			}
			.padding(8)
			.padding(.horizontal)
			Divider()
			
			HStack {
				Image(systemName: "moon").foregroundColor(Color.cyan)
					.frame(width: 120, alignment: .leading)
				Text(getMoonRise()).frame(minWidth: 90, alignment: .trailing)
				Text(getMoonSet()).frame(minWidth: 90, alignment: .trailing)
			}
			.padding(8)
			.padding(.horizontal)
			.padding(.bottom, 32)
			
			
			HStack {
				Text("").frame(width: 120, alignment: .leading)
				Text("Dusk").frame(minWidth: 90, alignment: .trailing)
				Text("Dawn").frame(minWidth: 90, alignment: .trailing)
			}
			.padding(8)
			.padding(.horizontal)
			Divider()
			
			HStack {
				Text("Nautical")
					.frame(width: 120, alignment: .leading)
				Text(getNauticalDusk()).frame(minWidth: 90, alignment: .trailing)
				Text(getNauticalDawn()).frame(minWidth: 90, alignment: .trailing)
			}
			.foregroundColor(Color.mint)
			.padding(8)
			.padding(.horizontal)
			Divider()
			
			HStack {
				Text("Astronomical")
					.frame(width: 120, alignment: .leading)
				Text(getAstronomicalDusk()).frame(minWidth: 90, alignment: .trailing)
				Text(getAstronomicalDawn()).frame(minWidth: 90, alignment: .trailing)
			}
			.foregroundColor(Color.green)
			.padding(8)
			.padding(.horizontal)
			
		}
	}
}
