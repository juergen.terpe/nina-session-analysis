//
//  FilterSettingsView.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 03.05.24.
//

import SwiftUI
import TipKit

struct FilterSettingsView: View {
	@State var session: SessionData
	@State private var appeared = false
	
	
    var body: some View {
		ScrollView(.vertical) {
			
			VStack {
				Divider()
				ForEach($session.filterColors, id: \.self.id) { $fc in
					HStack {
						Text("Filter: \(fc.filterName)")
							.padding(.vertical)
							.padding(.trailing)
						Spacer()
						ColorPicker("Color", selection: $fc.color, supportsOpacity: false)
							.padding(.leading)
							.help("Changing the color for this filter helps to see which filter was used when capturing each image!")
					}
					.padding(.horizontal, 8)
					.padding(.vertical, 4)
					
					HStack {
						Spacer()
						
						Toggle(isOn: $fc.overwrite) {
							Text("Overwrite Default Settings")
						}
						.toggleStyle(CheckboxToggleStyle())
					}
					.padding(.horizontal, 8)
					.padding(.vertical, 4)
					
					 
					if fc.overwrite {
						
						
						VStack(alignment: .leading, spacing: 4) {
							
							Text("Mean Tolerance: \(fc.meanTolerance, specifier: "%2.1f") %").foregroundColor(.lightBlue)
							Slider(value: $fc.meanTolerance, in: 0...100)
								.padding(.bottom)
								.onChange(of: fc.meanTolerance) { _, newValue in
									session.analyzationSettings.meanToleranceForFilter[fc.filterName] = Float(fc.meanTolerance)
									session.analyzeAvailable = false
									
									Task(priority: .background) {
										session.analyzeImages()
										session.analyzationSettings.store()
									}
								}
								.help("Change the tolerance for mean values")
								.padding(.bottom, 4)
							
							
							Text("HFR Tolerance: \(fc.hfrTolerance, specifier: "%2.1f") %").foregroundColor(.lightBlue)
							Slider(value: $fc.hfrTolerance, in: 0...100)
								.padding(.bottom)
								.onChange(of: fc.hfrTolerance) { _, newValue in
									session.analyzationSettings.hfrToleranceForFilter[fc.filterName] = Float(fc.hfrTolerance)
									session.analyzeAvailable = false
									
									Task(priority: .background) {
										session.analyzeImages()
										session.analyzationSettings.store()
									}
								}
								.help("Change the tolerance for HFR values")
								.padding(.bottom, 4)
							
							Text("#Stars Tolerance: \(fc.starCountTolerance, specifier: "%2.1f") %").foregroundColor(.lightBlue)
							Slider(value: $fc.starCountTolerance, in: 0...100)
								.padding(.bottom)
								.onChange(of: fc.starCountTolerance) { _, newValue in
									session.analyzationSettings.starCountToleranceForFilter[fc.filterName] = Float(fc.starCountTolerance)
									session.analyzeAvailable = false
									
									Task(priority: .background) {
										session.analyzeImages()
										session.analyzationSettings.store()
									}
								}
								.help("Change the tolerance for number of stars")
								.padding(.bottom, 4)
						}
						 
					}
					
					Divider()
				}
			}
		}
		.padding()
		.padding(.top)
		.popoverTipConditionally(session.filterColors.isNotEmpty && appeared, modifier: { view in
			view.popoverTip(SetFiltersTip(), arrowEdge: .trailing)
		})
		.task {
			try? await Task.sleep(nanoseconds: 3_500_000_000)
			await MainActor.run {
				self.appeared = true
			}
		}
    }
}

