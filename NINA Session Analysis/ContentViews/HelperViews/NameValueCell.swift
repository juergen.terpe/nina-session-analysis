//
//  NameValueCell.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 12.04.24.
//

import SwiftUI
import PureSwiftUI

struct NameValueCell: View {
	let name: LocalizedStringKey
	let value: String
	let valueColor: Color
	
    var body: some View {
		HStack {
			Text(name).fontWeight(.light)
				.foregroundColor(.secondary)
				.frame(width: 100, alignment: .trailing)
				.help(name)
			Text(": ")
				.fontWeight(.light)
				.foregroundColor(.secondary)
			Text(value).fontWeight(.regular)
				.truncationMode(.middle)
				.frame(width: 250, alignment: .leading)
				.foregroundColor(valueColor)
				.help(value)
			
		}
		.padding()
    }
}


