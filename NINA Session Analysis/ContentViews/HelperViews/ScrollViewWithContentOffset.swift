//
//  ScrollViewWithContentOffset.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 07.05.24.
//

import SwiftUI
import AppKit

fileprivate extension NSScrollView {
	var documentSize: NSSize {
		set { documentView?.setFrameSize(newValue) }
		get { documentView?.frame.size ?? NSSize.zero }
	}
	var documentOffset: NSPoint {
		set { documentView?.scroll(newValue) }
		get { documentVisibleRect.origin }
	}
}


struct ScrollViewWithContentOffset <Content: View>: NSViewRepresentable  {
	typealias NSViewType = NSScrollView
	@Binding var contentOffset: CGPoint
	let content: () -> Content
	
	public init(contentOffset: Binding<CGPoint>, @ViewBuilder _ content: @escaping () -> Content) {
		self._contentOffset = contentOffset
		self.content = content
	}
	
	
	func makeNSView(context: Context) -> NSScrollView {
		let view = NSScrollView()
		//view.delegate = context.coordinator
		
		let controller = NSHostingController(rootView: content())
		controller.view.translatesAutoresizingMaskIntoConstraints = false  // Disable autoresizing
				
		view.addSubview(controller.view)
				
		// Set constraints for the controller's view
		NSLayoutConstraint.activate([
			controller.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
			controller.view.trailingAnchor.constraint(equalTo: view.trailingAnchor),
			controller.view.topAnchor.constraint(equalTo: view.topAnchor),
			controller.view.bottomAnchor.constraint(equalTo: view.bottomAnchor),
			controller.view.widthAnchor.constraint(equalTo: view.widthAnchor)  // Ensures the width matches the scroll view
		])
		
		return view
	}
	
	func updateNSView(_ nsView: NSScrollView, context: Context) {
		nsView.documentOffset = NSPoint(x: self.contentOffset.x, y: self.contentOffset.y)
	}
	
	
	
    
}

