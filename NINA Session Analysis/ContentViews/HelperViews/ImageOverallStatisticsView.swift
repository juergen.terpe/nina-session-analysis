//
//  ImageOverallStatisticsView.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 28.08.24.
//
import SwiftUI

public struct OverallStatisticsFilter: Identifiable {
	public let id: UUID = UUID()
	public let filter: String
	public let totalExposureTime: Float
	public let totalExposureTimeGood: Float
	public let totalExposureTimeBad: Float
	public let totalFrames: Int
	public let totalGoodFrames: Int
	public let totalBadFrames: Int
	
	public var badFramePercentage: String {
		String(format: "%.2f%%", Float(totalBadFrames) / Float(totalFrames) * 100.0)
	}
	
	public var badFrameTimePercentage: String {
		String(format: "%.2f%%", Float(totalExposureTimeBad) / Float(totalExposureTime) * 100.0)
	}
	
	public var TotalExposureTime: String {
		if totalExposureTime < 60.0 {
			return String(format: "%.0fs", totalExposureTime)
		} else if totalExposureTime < 3600.0 {
			return String(format: "%.0f min", totalExposureTime / 60.0)
		} else {
			return ValueFormatter.formatTimeSpan(Double(totalExposureTime))
		}
	}
	
	public var TotalExposureTimeGood: String {
		if totalExposureTimeGood < 60.0 {
			return String(format: "%.0fs", totalExposureTimeGood)
		} else if totalExposureTime < 3600.0 {
			return String(format: "%.0f min", totalExposureTimeGood / 60.0)
		} else {
			return ValueFormatter.formatTimeSpan(Double(totalExposureTimeGood))
		}
	}
	
	public var TotalExposureTimeBad: String {
		if totalExposureTimeBad < 60.0 {
			return String(format: "%.0fs", totalExposureTimeBad)
		} else if totalExposureTime < 3600.0 {
			return String(format: "%.0f min", totalExposureTimeBad / 60.0)
		} else {
			return ValueFormatter.formatTimeSpan(Double(totalExposureTimeBad))
		}
	}
}

@Observable
public class OverallStatisticsModel {
	public var statistics: [OverallStatisticsFilter] = []
	public var total: OverallStatisticsFilter? = nil
	
	public var available: Bool {
		self.total != nil && !self.statistics.isEmpty
	}
	
	func update(for session: SessionData) async {
		let filters = session.filters.filter({ $0 != "Any" }).sorted(by: { $0 < $1 })

		var filterStatistics = [OverallStatisticsFilter]()
		
		for filter in filters {
			let images = session.getImages(for: session.selectedSessionDate, target: session.selectedTarget).filter({ $0.filter == filter })
			if images.count == 0 { continue }
			
			let bad = images.filter({ $0.bad })
			let good = images.filter({ !$0.bad })
			
			var badExposureTime: Float = 0.0
			for image in bad {
				badExposureTime += image.exposure ?? 0.0
			}
			
			var goodExposureTime: Float = 0.0
			for image in good {
				goodExposureTime += image.exposure ?? 0.0
			}
			
			let totalExposureTime = badExposureTime + goodExposureTime
						
			let stat = OverallStatisticsFilter(filter: filter,
											   totalExposureTime: totalExposureTime,
											   totalExposureTimeGood: goodExposureTime,
											   totalExposureTimeBad: badExposureTime,
											   totalFrames: images.count,
											   totalGoodFrames: good.count,
											   totalBadFrames: bad.count)
			filterStatistics.append(stat)
		}
		
		let totalFrames = filterStatistics.map { $0.totalFrames }.reduce(0, +)
		let totalGoodFrames = filterStatistics.map { $0.totalGoodFrames }.reduce(0, +)
		let totalBadFrames = filterStatistics.map { $0.totalBadFrames }.reduce(0, +)
		let totalExposureTime = filterStatistics.map { $0.totalExposureTime }.reduce(0.0, +)
		let totalExposureTimeGood = filterStatistics.map { $0.totalExposureTimeGood }.reduce(0.0, +)
		let totalExposureTimeBad = filterStatistics.map { $0.totalExposureTimeBad }.reduce(0.0, +)
		
		let totalStatistics = OverallStatisticsFilter(filter: "Any",
													  totalExposureTime: totalExposureTime,
													  totalExposureTimeGood: totalExposureTimeGood,
													  totalExposureTimeBad: totalExposureTimeBad,
													  totalFrames: totalFrames,
													  totalGoodFrames: totalGoodFrames,
													  totalBadFrames: totalBadFrames)
		
		let allFilters = filterStatistics
		
		await MainActor.run {
			self.statistics = allFilters
			self.total = totalStatistics
		}
	}
	
}

struct ImageOverallStatisticsView: View {
	@State var session: SessionData
	@State private var model = OverallStatisticsModel()
	
	var body: some View {
		VStack {
			if self.model.available {
				
				HStack {
					Grid(alignment: .leading, horizontalSpacing: 8, verticalSpacing: 4) {
						GridRow(alignment: .firstTextBaseline) {
							Text("Filter").fontWeight(.semibold).kerning(1.5)
							Text("#Lights").fontWeight(.semibold).kerning(1.5)
							Text("ExposureTime").fontWeight(.semibold).kerning(1.5)
							Text("#Lights (Good)").fontWeight(.semibold).kerning(1.5)
							Text("ExposureTime (Good)").fontWeight(.semibold).kerning(1.5)
							Text("#Lights (Bad)").fontWeight(.semibold).kerning(1.5)
							Text("ExposureTime (Bad)").fontWeight(.semibold).kerning(1.5)
						}
						GridRow {
							Divider()
								.background(Color.primary.opacity(0.65))
								.gridCellColumns(7)
						}
						
						if self.model.statistics.count > 1 {
							GridRow(alignment: .firstTextBaseline) {
								Text("ALL")
								Text("\(self.model.total!.totalFrames)").fontWeight(.semibold)
								Text(self.model.total!.TotalExposureTime).fontWeight(.semibold)
								Text("\(self.model.total!.totalGoodFrames)").fontWeight(.semibold).foregroundStyle(Color.green)
								Text(self.model.total!.TotalExposureTimeGood).fontWeight(.semibold).foregroundStyle(Color.green)
								Text("\(self.model.total!.totalBadFrames) (\(self.model.total!.badFramePercentage))").fontWeight(.semibold).foregroundStyle(Color.red)
								Text("\(self.model.total!.TotalExposureTimeBad) (\(self.model.total!.badFrameTimePercentage))").fontWeight(.semibold).foregroundStyle(Color.red)
							}.kerning(1.2)
							
							GridRow {
								Divider()
									.gridCellColumns(7)
							}
						}
						
						ForEach(self.model.statistics) { stat in
							GridRow(alignment: .firstTextBaseline) {
								Text(stat.filter).fontWeight(.semibold)
								Text("\(stat.totalFrames)").fontWeight(.semibold)
								Text(stat.TotalExposureTime).fontWeight(.semibold)
								Text("\(stat.totalGoodFrames)").fontWeight(.semibold).foregroundStyle(Color.green)
								Text(stat.TotalExposureTimeGood).fontWeight(.semibold).foregroundStyle(Color.green)
								Text("\(stat.totalBadFrames) (\(stat.badFramePercentage))").fontWeight(.semibold).foregroundStyle(Color.red)
								Text("\(stat.TotalExposureTimeBad) (\(stat.badFrameTimePercentage))").fontWeight(.semibold).foregroundStyle(Color.red)
							}.kerning(1.2)
							
							GridRow {
								Divider()
									.gridCellColumns(7)
							}
							
						}
						
					}
				}
				
			}
			
		}
		.task {
			await self.model.update(for: session)
		}
	}
	
}

