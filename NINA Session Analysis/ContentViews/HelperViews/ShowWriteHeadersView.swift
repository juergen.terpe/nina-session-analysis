//
//  ShowWriteHeadersView.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 16.04.24.
//

import SwiftUI

struct ShowWriteHeadersView: View {
	@State var session: SessionData
	@Binding var show: Bool
	@AppStorage("WriteHFR") var hfr: Bool = true
	@AppStorage("WriteEccentricity") var eccentricity: Bool = true
	@AppStorage("WriteMean") var mean: Bool = true
	@AppStorage("WriteMedian") var median: Bool = true
	@AppStorage("WriteADUMin") var aduMin: Bool = false
	@AppStorage("WriteADUMax") var aduMax: Bool = false
	@AppStorage("WriteNumStars") var numStars: Bool = true
	@AppStorage("WriteFWHM") var fwhm: Bool = true
	@AppStorage("WriteRMS") var rms: Bool = false
	@AppStorage("WriteRMSRA") var rmsRA: Bool = false
	@AppStorage("WriteRMSDEC") var rmsDEC: Bool = false
	@State private var overwriteExistingFiles: Bool = false
	@State private var writingFiles: Bool = false
	@State private var writeProgress = 0.0
	
    var body: some View {
		VStack {
			
			
			VStack(alignment: .leading, spacing: 0) {
				HStack {
					Toggle(isOn: $hfr) {
						Text("HFR")
					}
					.toggleStyle(.checkbox)
					.frame(width: 200, alignment: .leading)
					
					Toggle(isOn: $eccentricity) {
						Text("Eccentricity")
					}
					.toggleStyle(.checkbox)
					.frame(width: 200, alignment: .leading)
					
					Toggle(isOn: $mean) {
						Text("Mean")
					}
					.toggleStyle(.checkbox)
					.frame(width: 200, alignment: .leading)
					
					Toggle(isOn: $median) {
						Text("Median")
					}
					.toggleStyle(.checkbox)
					.frame(width: 200, alignment: .leading)
					
					Spacer()
					
				}.padding()
				
				
				HStack {
					Toggle(isOn: $numStars) {
						Text("Number of Stars")
					}
					.toggleStyle(.checkbox)
					.frame(width: 200, alignment: .leading)
					
					Toggle(isOn: $fwhm) {
						Text("FWHM")
					}
					.toggleStyle(.checkbox)
					.frame(width: 200, alignment: .leading)
					
					Toggle(isOn: $aduMin) {
						Text("min(ADU)")
					}
					.toggleStyle(.checkbox)
					.frame(width: 200, alignment: .leading)
					
					Toggle(isOn: $aduMax) {
						Text("max(ADU)")
					}
					.toggleStyle(.checkbox)
					.frame(width: 200, alignment: .leading)
					Spacer()
					
				}.padding()
				
				
				HStack {
					Toggle(isOn: $rms) {
						Text("RMS")
					}
					.toggleStyle(.checkbox)
					.frame(width: 200, alignment: .leading)
					
					Toggle(isOn: $rmsRA) {
						Text("RMS(α)")
					}
					.toggleStyle(.checkbox)
					.frame(width: 200, alignment: .leading)
					
					Toggle(isOn: $rmsDEC) {
						Text("RMS(δ)")
					}
					.toggleStyle(.checkbox)
					.frame(width: 200, alignment: .leading)
					
					Spacer()
					
				}.padding()
				
			}
			.frame(maxWidth: .infinity)
			
			.padding()
			
			if writingFiles {
				
				ProgressView(value: writeProgress) {
					Text("Writing changed FITS headers...")
						.font(.title2).padding()
					} currentValueLabel: {
					Text("Progress \((writeProgress * 100), specifier: "%.1f")%")
						.font(.headline).padding()
				}
				.padding()
					
			}
						
			Spacer()
			
			
			HStack {
				
				Toggle(isOn: $overwriteExistingFiles) {
					Text("Overwrite existing files")
						.font(.headline)
						.foregroundColor(overwriteExistingFiles ? .red : .green)
						.transition(.slide)
				}
				.toggleStyle(.switch)
				.padding()
				
				Button {
					
					withAnimation(.easeInOut) {
						self.writingFiles = true
					}
					
					let settings = FitsWriteSettings(hfr: self.hfr,
													 fwhm: self.fwhm,
													 eccentricity: self.eccentricity,
													 mean: self.mean,
													 median: self.median, 
													 aduMin: self.aduMin,
													 aduMax: self.aduMax,
													 numStars: self.numStars,
													 rms: self.rms,
													 rmsRA: self.rmsRA,
													 rmsDEC: self.rmsDEC,
													 overwrite: self.overwriteExistingFiles,
													 path: nil)
					Task {
						await session.write(images: session.filteredImages, settings: settings) { prog in
							
							DispatchQueue.main.async {
								self.writeProgress = Double(prog)
							}
						}
					
						withAnimation(.easeInOut) {
							self.show = false
							self.writingFiles = false
						}
					}
					
					
				} label: {
					Text("Write")
						.font(.headline)
						.foregroundColor(.primary)
						.padding(.horizontal, 64)
						.padding(.vertical, 8)
				}
				.background(Color.blue)
				.padding()
				
				Spacer()
			}
			.padding()
		}
		.frame(maxHeight: .infinity)
		.background(Color.controlBackgroundColor).cornerRadius(8)
		
    }
}


