//
//  StatisticsDataSource.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 14.07.24.
//
import SwiftUI

struct StatisticsNameValue: Identifiable {
	let id = UUID()
	let name: LocalizedStringKey
	let value1: String
	let value2: String
	let value3: String
}

struct StatisticsTimespan: Identifiable, Equatable  {
	let id = UUID()
	let start: Date
	let end: Date
	let duration: TimeInterval
}

struct PercentageValue: Identifiable {
	let id = UUID()
	let name: String
	let value: Double
}

struct PauseTime: Identifiable, Equatable {
	let id = UUID()
	let time: Date
	let value: Double
	
}

enum StatisticsStatus {
	case noLog
	case noSequence
	case noData
	case processing
}



@Observable
class StatisticsDataSource {
	
	var sequenceDuration: TimeInterval?
	var data: [StatisticsNameValue] = []
	var percentageValues: [PercentageValue] = []
	var capturePauseTimes: [PauseTime] = []
	var ditherTimes: [StatisticsTimespan] = []
	var autoFocusTimes: [StatisticsTimespan] = []
	var status: StatisticsStatus = .processing
	var timeSpan: Double?
	var minTime: Date = Date.now
	var maxTime: Date = Date.now.advanced(by: TimeInterval(6*3600))
	
	func update(for session: SessionData, selectedSession: DateOfSession) async {
		
		var updateStatus: StatisticsStatus = .noData
		
		let logs = session.ninaLogs.getLogData(for: selectedSession)
		if logs.count == 0 {
			updateStatus = StatisticsStatus.noLog
		} else {
			let sequenceLogs = filterSequence(logs)
			
			if sequenceLogs.count == 0 {
				updateStatus = StatisticsStatus.noSequence
			} else if sequenceLogs.count > 0 {
				let beginSequence = sequenceLogs.first!.time
				let endSequence = sequenceLogs.last!.time
				let duration = beginSequence.distance(to: endSequence)
				let capturing = getCapturingTimes(sequenceLogs)
				if capturing.isEmpty {
					updateStatus = StatisticsStatus.noData
				} else {
					
					let dithering = getDitherTimes(sequenceLogs)
					let autoFocusTimes = getAutofocusTimes(sequenceLogs)
					let flipTimes = getMeridianFlipTimes(sequenceLogs)
					
					let timeData = processCapturingData(capturing: capturing,
														dithering: dithering,
														autofocus: autoFocusTimes,
														meridianFlips: flipTimes,
														duration: duration,
														beginSequence: beginSequence, endSequence: endSequence)
					
					let percentageValues = processPercentageValues(capturing: capturing,
																   dithering: dithering,
																   autofocus: autoFocusTimes,
																   meridianFlips: flipTimes,
																   duration: duration,
																   beginSequence: beginSequence, endSequence: endSequence)
					
					let pauseTimes = getCapturePauseTimes(capturing: capturing)
					
					
					
					let stat = updateStatus
					await MainActor.run {
						self.sequenceDuration = duration
						self.data = timeData
						self.percentageValues = percentageValues
						self.capturePauseTimes = pauseTimes
						self.ditherTimes = dithering
						self.autoFocusTimes = autoFocusTimes
						self.timeSpan = duration
						self.minTime = beginSequence
						self.maxTime = endSequence
						self.status = stat
					}
					
					return
				}
			}
		}
		
		
		let stat = updateStatus
		await MainActor.run {
			self.sequenceDuration = nil
			self.data = []
			self.percentageValues = []
			self.capturePauseTimes = []
			self.ditherTimes = []
			self.autoFocusTimes = []
			self.status = stat
			self.timeSpan = nil
		}
	}
	
	private func filterSequence(_ logs: [NINALogLine]) -> [NINALogLine] {
		
		if let first = logs.first(where: { $0.text.contains("Advanced Sequence starting") }) ,
		   let last = logs.last(where: { $0.text.contains("Telescope has parked")
					|| $0.text.contains("Advanced Sequence finished")
				    || $0.text.contains("Disconnected mount")}) {
			let time1 = first.time
			let time2 = last.time
			
			return logs.filter { $0.time >= time1 && $0.time <= time2 }
		}
		return []
	}
	
	private func getCapturePauseTimes(capturing: [StatisticsTimespan]) -> [PauseTime] {
		var result = [PauseTime]()

		var lastEndTime: Date? = nil
		for capture in capturing {
			if let last = lastEndTime {
				let pause = last.distance(to: capture.start)
				if pause > 0.0 {
					result.append(PauseTime(time: last, value: pause))
				}
			}
			lastEndTime = capture.end
		}
		
		return result
	}
	
	
	private func getMeridianFlipTimes(_ logs: [NINALogLine]) -> [StatisticsTimespan] {
		// Meridian Flip - Stopping tracking to pass meridian
		let mfStarts = logs.filter { $0.text.contains("Meridian Flip - Stopping tracking to pass meridian") }
		var result = [StatisticsTimespan]()
		
		if mfStarts.count > 0 {
			let exposureStarts = logs.filter { $0.text.contains("Starting Category:") && $0.text.contains("Item: TakeExposure") }
			
			// forech flip find the next exposure after the flip ...
			for mfStart in mfStarts {
				
				if let firstExpAfterFlip = exposureStarts.first(where: { $0.time > mfStart.time }) {
					let diff = abs(mfStart.time.distance(to: firstExpAfterFlip.time))
					
					result.append(StatisticsTimespan(start: mfStart.time, end: firstExpAfterFlip.time, duration: diff))
				}
				
			}
			
		}
		
		return result
	}
	
	private func getDitherTimes(_ logs: [NINALogLine]) -> [StatisticsTimespan] {
		// Starting Category: Guider, Item: Dither
		// Finishing Category: Guider, Item: Dither
		let ditherStarts = logs.filter { $0.text.contains("Starting Category:") && $0.text.contains("Item: Dither") }
		let ditherEnds = logs.filter { $0.text.contains("Finishing Category:") && $0.text.contains("Item: Dither") }
		
		var result = [StatisticsTimespan]()
		for start in ditherStarts {
			if let end = ditherEnds.filter({ $0.time > start.time }).first {
				let diff = abs(start.time.distance(to: end.time))
				if diff < 60.0 {
					result.append(StatisticsTimespan(start: start.time, end: end.time, duration: diff))
				}
			}
		}
		
		return result
	}
	
	private func getAutofocusTimes(_ logs: [NINALogLine]) -> [StatisticsTimespan] {
		// Starting Category: Focuser, Item: RunAutofocus
		// Finishing Category: Focuser, Item: RunAutofocus
		let afStarts = logs.filter { $0.text.contains("Starting Category:") && $0.text.contains("Item: RunAutofocus") }
		let afEnds = logs.filter { ($0.text.contains("Finishing Category:") && $0.text.contains("Item: RunAutofocus"))
			|| ($0.text.contains("Category: Focuser, Item: RunAutofocus") && $0.level == .error) }
		
		var result = [StatisticsTimespan]()
		for start in afStarts {
			if let end = afEnds.filter({ $0.time > start.time }).first {
				let diff = abs(start.time.distance(to: end.time))
				result.append(StatisticsTimespan(start: start.time, end: end.time, duration: diff))
			}
		}
		
		return result
	}
	
	
	private func getCapturingTimes(_ logs: [NINALogLine]) -> [StatisticsTimespan] {
		// "Starting Category: Camera, Item: TakeExposure, ExposureTime 120, Gain 100, Offset 120, ImageType LIGHT, Binning 1x1"
		// "Finishing Category: Camera, Item: TakeExposure, ExposureTime 120, Gain 100, Offset 120, ImageType LIGHT, Binning 1x1"
		let exposureStarts = logs.filter { $0.text.contains("Starting Category") && $0.text.contains("Item: TakeExposure")  }
		let exposureEnds = logs.filter { $0.text.contains("Finishing Category") && $0.text.contains("Item: TakeExposure") }
		
		let regEx1 = /Starting Category: .+, Item: TakeExposure, ExposureTime (?<expT>.+?), Gain (?<gain>.+?), Offset (?<offset>.+?), ImageType (?<type>.+?), Binning (?<binning>.+?)/
		let regEx2 = /Finishing Category: .+, Item: TakeExposure, ExposureTime (?<expT>.+?), Gain (?<gain>.+?), Offset (?<offset>.+?), ImageType (?<type>.+?), Binning (?<binning>.+?)/
		
		
		var result = [StatisticsTimespan]()
		
		for exp1 in exposureStarts {
			if let matches1 = try? regEx1.wholeMatch(in: exp1.text) {
				
				if let exposureTime1 = Double(matches1.expT), matches1.type == "LIGHT" {
					
					let endTime = exp1.time.addingTimeInterval(exposureTime1 + exposureTime1/3.0)
					for exp2 in exposureEnds.filter({ $0.time > exp1.time && $0.time <= endTime }) {
						if let matches2 = try? regEx2.wholeMatch(in: exp2.text) {
							if matches1.expT == matches2.expT && matches1.gain == matches2.gain
								&& matches1.offset == matches2.offset && matches1.type == matches2.type {
								
								let diff = abs(exp1.time.distance(to: exp2.time))
								result.append(StatisticsTimespan(start: exp1.time, end: exp2.time, duration: diff))
							}
						}
					}
				}
			}
		}
		
		return result
	}
	
	private func processPercentageValues(capturing: [StatisticsTimespan],
										 dithering: [StatisticsTimespan],
									  autofocus: [StatisticsTimespan],
									  meridianFlips: [StatisticsTimespan],
									  duration: TimeInterval,
									  beginSequence: Date,
									  endSequence: Date) -> [PercentageValue] {
		
		var percentageValues = [PercentageValue]()
		guard capturing.count > 0 else {
			return percentageValues
		}
				
		let beginCapturing = capturing.first!.start
		let endCapturing = capturing.last!.end
		let totalCapturingDuration = beginCapturing.distance(to: endCapturing)
		let capturingDuration = capturing.map({ $0.duration }).reduce(0.0, +)
		
		let lostTime = abs(totalCapturingDuration - capturingDuration)
		var totalLostTime = lostTime
		
		percentageValues.append(PercentageValue(name: "Imaging", value: capturingDuration/totalCapturingDuration * 100.0))
		
		if dithering.count > 0 {
			let ditheringDuration = dithering.map({ $0.duration }).reduce(0.0, +)
			percentageValues.append(PercentageValue(name: "Dithering", value: ditheringDuration/totalCapturingDuration * 100.0))
			totalLostTime -= ditheringDuration
		}
		
		if autofocus.count > 0 {
			let afDuration = autofocus.map({ $0.duration }).reduce(0.0, +)
			percentageValues.append(PercentageValue(name: "Autofocus", value: afDuration/totalCapturingDuration * 100.0))
			totalLostTime -= afDuration
		}
		
		if meridianFlips.count > 0 {
			let meridianFlipDuration = meridianFlips.map({ $0.duration }).reduce(0.0, +)
			percentageValues.append(PercentageValue(name: "Meridian Flip", value: meridianFlipDuration/totalCapturingDuration * 100.0))
			totalLostTime -= meridianFlipDuration
		}
		
		if totalLostTime > 0.0 {
			percentageValues.append(PercentageValue(name: "Processing/Transfer", value: totalLostTime/totalCapturingDuration * 100.0))
		}
		
		return percentageValues
	}
	
	
	private func processCapturingData(capturing: [StatisticsTimespan],
									  dithering: [StatisticsTimespan],
									  autofocus: [StatisticsTimespan],
									  meridianFlips: [StatisticsTimespan],
									  duration: TimeInterval,
									  beginSequence: Date,
									  endSequence: Date) -> [StatisticsNameValue] {
		var result = [StatisticsNameValue]()
		
		guard !capturing.isEmpty else { return result }
		
		let beginCapturing = capturing.first!.start
		let endCapturing = capturing.last!.end
		let totalCapturingDuration = beginCapturing.distance(to: endCapturing)
		let capturingDuration = capturing.map({ $0.duration }).reduce(0.0, +)
		
		result.append(StatisticsNameValue(name: LocalizedStringKey("Total imaging duration"),
										  value1: "\(String(format: "%.1f", totalCapturingDuration/60.0)) min",
										  value2: "\(ValueFormatter.formatTimeSpan(totalCapturingDuration, timeStyle: .full))",
										  value3: "\(String(format: "%.1f", totalCapturingDuration/duration*100.0))%"))
		
		let startDelay = abs(beginSequence.distance(to: beginCapturing))
		
		result.append(StatisticsNameValue(name: LocalizedStringKey("Imaging start delay"),
										  value1: "\(String(format: "%.1f", startDelay/60.0)) min",
										  value2: "\(ValueFormatter.formatTimeSpan(startDelay, timeStyle: .full))",
										  value3: "\(String(format: "%.1f", startDelay/duration*100.0))%"))
		
		let endDelay = abs(endSequence.distance(to: endCapturing))
		
		result.append(StatisticsNameValue(name: LocalizedStringKey("Imaging end delay"),
										  value1: "\(String(format: "%.1f", endDelay/60.0)) min",
										  value2: "\(ValueFormatter.formatTimeSpan(endDelay, timeStyle: .full))",
										  value3: "\(String(format: "%.1f",endDelay/duration*100.0))%"))
		
		result.append(StatisticsNameValue(name: LocalizedStringKey("Capturing duration"),
										  value1: "\(String(format: "%.1f", capturingDuration/60.0)) min",
										  value2: "\(ValueFormatter.formatTimeSpan(capturingDuration, timeStyle: .full))",
										  value3: "\(String(format: "%.1f", capturingDuration/duration*100.0))%"))
		
		let lostTime = abs(totalCapturingDuration - capturingDuration)
		result.append(StatisticsNameValue(name: LocalizedStringKey("Lost time between exposures"),
										  value1: "\(String(format: "%.1f", lostTime/60.0)) min",
										  value2: "\(ValueFormatter.formatTimeSpan(lostTime, timeStyle: .full))",
										  value3: "\(Int(lostTime/totalCapturingDuration*100.0))%"))
		
		var totalLostTime = lostTime
		
		if dithering.count > 0 {
			let ditheringDuration = dithering.map({ $0.duration }).reduce(0.0, +)
			result.append(StatisticsNameValue(name: LocalizedStringKey("Dithering total duration"),
											  value1: "\(String(format: "%.1f", ditheringDuration/60.0)) min",
											  value2: "\(ValueFormatter.formatTimeSpan(ditheringDuration, timeStyle: .full))",
											  value3: "\(String(format: "%.1f",ditheringDuration/totalCapturingDuration*100.0))%, (\(String(format: "%.1f", ditheringDuration/lostTime*100.0))%)"))
			
			let minTime = dithering.min { $0.duration < $1.duration}!.duration
			let maxTime = dithering.max { $0.duration < $1.duration}!.duration
						
			result.append(StatisticsNameValue(name: LocalizedStringKey("Dithering count"),
											  value1: "\(dithering.count)x",
											  value2: "min \(String(format: "%.1f", minTime)) s",
											  value3: "max \(String(format: "%.1f", maxTime)) s"))
			
			totalLostTime -= ditheringDuration
		}
		
		if autofocus.count > 0 {
			let afDuration = autofocus.map({ $0.duration }).reduce(0.0, +)
			result.append(StatisticsNameValue(name: LocalizedStringKey("Autofocus total duration"),
											  value1: "\(String(format: "%.1f", afDuration/60.0)) min",
											  value2: "\(ValueFormatter.formatTimeSpan(afDuration, timeStyle: .full))",
											  value3: "\(String(format: "%.1f", afDuration/totalCapturingDuration*100.0))%, (\(String(format: "%.1f", afDuration/lostTime*100.0))%)"))
			
			let minTime = autofocus.min { $0.duration < $1.duration}!.duration
			let maxTime = autofocus.max { $0.duration < $1.duration}!.duration
						
			
			result.append(StatisticsNameValue(name: LocalizedStringKey("Autofocus count"),
											  value1: "\(autofocus.count)x",
											  value2: "min \(String(format: "%.1f", minTime)) s",
											  value3: "max \(String(format: "%.1f", maxTime)) s"))
			
			totalLostTime -= afDuration
		}
		
		if meridianFlips.count > 0 {
			
			let meridianFlipDuration = meridianFlips.map({ $0.duration }).reduce(0.0, +)
			result.append(StatisticsNameValue(name: LocalizedStringKey("Meridian flip total duration"),
											  value1: "\(String(format: "%.1f", meridianFlipDuration/60.0)) min",
											  value2: "\(ValueFormatter.formatTimeSpan(meridianFlipDuration, timeStyle: .full))",
											  value3: "\(String(format: "%.1f", meridianFlipDuration/totalCapturingDuration*100.0))%, (\(String(format: "%.1f", meridianFlipDuration/lostTime*100.0))%)"))
			
			totalLostTime -= meridianFlipDuration
		}
		
		if totalLostTime > 0.0 {
			result.append(StatisticsNameValue(name: LocalizedStringKey("Transfer and processing duration"),
											  value1: "\(String(format: "%.1f", totalLostTime/60.0)) min",
											  value2: "\(ValueFormatter.formatTimeSpan(totalLostTime, timeStyle: .full))",
											  value3: "\(String(format: "%.1f", totalLostTime/totalCapturingDuration*100.0))%, (\(String(format: "%.1f", totalLostTime/lostTime*100.0))%)"))
			
		}
		
		
		return result
	}
}
