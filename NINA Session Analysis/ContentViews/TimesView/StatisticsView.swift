//
//  StatisticsView.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 14.07.24.
//

import SwiftUI

struct TimeStatisticsView: View {
	@State var session: SessionData
	@State var selectedSessionDate: DateOfSession? = nil
	
	func getSessions() -> [DateOfSession] {
		var sessionDates = [DateOfSession]()
		
		for sessionDate in session.sessionDates.filter({ $0.text != "Any" }) {
			let logs = session.ninaLogs.getLogData(for: sessionDate)
			if logs.count > 0 {
				if let _ = logs.first(where: { $0.text.contains("Advanced Sequence starting") }) {
					sessionDates.append(sessionDate)
				}
			}
		}
		
		if sessionDates.isEmpty {
			let logDates = session.ninaLogs.dates.map({ DateOfSession(text: $0.formatted(date: .abbreviated, time: .omitted), date: $0)})
			for logDate in logDates {
				let logs = session.ninaLogs.getLogData(for: logDate)
				if logs.count > 0 {
					if let _ = logs.first(where: { $0.text.contains("Advanced Sequence starting") }) {
						sessionDates.append(logDate)
					}
				}
			}
		}
		
		return sessionDates
	}
	
	
	var body: some View {
		
		Group {
			
			let sessions = getSessions()
			
			if !sessions.isEmpty {
				TabView(selection: $selectedSessionDate) {
					ForEach(getSessions(), id: \.self.text) { sessionDate in
						StatisticsView(session: session, sessionDate: sessionDate)
							.tabItem {
								Text(sessionDate.text)
							}
							.tag(sessionDate)
					}
				}
			}
			else {
				
				VStack(alignment: .center) {
					
					Spacer()
					
					Text("No Sessions found.")
						.font(.title)
					
					Text("Check, if you have loaded the correct N.I.N.A. logfile.")
						.font(.subheadline)
					
					Text("Sometimes, the content of the N.I.N.A. logfile changes and this application needs to be adapted to the changed logfile content in order to read it correctly. Please inform the developer using the feedback form if you encounter any issues.")
						.font(.body)
						.foregroundColor(.secondary)
					
					Spacer()
				}
				.frame(maxWidth: .infinity)
				
				
			}
			
		}
	}
	
	
}



struct StatisticsView: View {
	@State var session: SessionData
	var sessionDate: DateOfSession
	@State var statisticsSource = StatisticsDataSource()
	@State private var selectedPause: PauseTime? = nil
	@State private var selectedTimeSpan: StatisticsTimespan? = nil
	@State private var selectedAFTimeSpan: StatisticsTimespan? = nil
	@State private var showLogData: Bool = false
	@State private var logStart: Date? = nil
	@State private var logDuration: Double? = nil
	//@State private var positionOfView = ScrollPosition(edge: .leading)
	
	let factor: Double = 5.0
	
	@ViewBuilder
	func showLog(_ startTime: Date, _ duration: Double) -> some View {
		let logs = self.session.ninaLogs.getLogData(startingAt: startTime, duration: duration)
		
		VStack {
			Spacer()
			
			VStack {
				
				VStack(spacing: 2) {
					Divider().background(Color.gray.opacity(0.4))
					Divider().background(Color.gray.opacity(0.4))
				}
				
				HStack(alignment: .top) {
					
					ScrollView(.vertical) {
						VStack(alignment: .leading, spacing: 4) {
							
							ForEach(Array(zip(logs.indices, logs)), id: \.0) { index, log in
								HStack(alignment: .firstTextBaseline) {
									Text(log.time.formatted(date: .omitted, time: .standard))
										.font(.body.monospacedDigit()).fontWeight(.light)
										.frame(width: 80, alignment: .leading)
									
									
									Text(log.level.rawValue)
										.font(.body.monospacedDigit())
										.frame(width: 60, alignment: .leading)
									
									Text(log.text)
										.font(.body.monospacedDigit())
										.frame(alignment: .leading)
									
									Spacer(minLength: 8)
								}
								.padding(.horizontal)
								.padding(.vertical, 2)
								.background {
									Rectangle()
										.fill(index % 2 == 0 ? Color.blue.opacity(0.08) : Color.clear)
								}
							}
						}
						.padding(.vertical)
					}
					
					
					Button {
						withAnimation {
							self.logStart = nil
							self.logDuration = nil
							self.showLogData = false
						}
						
					} label: {
						Image(systemName: "xmark.circle")
							.resizable()
							.imageScale(.large)
							.frame(width: 24, height: 24)
							.foregroundColor(.accentColor)
							.padding()
					}
					.buttonStyle(PlainButtonStyle())
				}
			}
			.frame(maxHeight: 520)
			.background(Color.controlBackgroundDarkColor)
			
		}
		.transition(AnyTransition.move(edge: .bottom))
	}
	
	@ViewBuilder
	func getHeader(_ duration: TimeInterval) -> some View {
		HStack(alignment: .top) {
			
			VStack(alignment: .leading) {
				
				HStack {
					Spacer()
					Text("Session duration \(Int(duration/60)) min or \(ValueFormatter.formatTimeSpan(duration, timeStyle: .full))")
						.font(.title3)
						.foregroundColor(.cyan)
					Spacer()
				}
				.padding(.vertical, 12)
				
				Grid(alignment: .leading, horizontalSpacing: 8, verticalSpacing: 2) {
					ForEach(statisticsSource.data) { item in
						
						GridRow {
							Text(item.name).kerning(1.5)
							Text(item.value1).fontWeight(.light)
							Text(item.value2).fontWeight(.light).foregroundStyle(Color.secondary)
							Text(item.value3).fontWeight(.light)
						}
						
						GridRow {
							Divider()
								.gridCellColumns(4)
						}
					}
				}
				.padding(12)
			}
			.padding(.horizontal)
			
			LostTimePieChart(statistics: self.statisticsSource)
				.aspectRatio(1.0, contentMode: .fit)
				.padding(.horizontal, 24)
				.padding(.top, 8)
				.frame(maxHeight: 320)
			
		}
		.background(Color.controlBackgroundColor)
		.transition(.move(edge: .top))
	}
	
	var body: some View {
		VStack(spacing: 0) {
			
			if let duration = self.statisticsSource.sequenceDuration {
				getHeader(duration)
				
				ScrollView(.horizontal) {
					
					VStack(alignment: .leading, spacing: 0) {
						
						if let timeSpan = self.statisticsSource.timeSpan {
							
							VStack(alignment: .leading, spacing: 0) {
								
								HStack {
									Text("Time between subframes")
										.font(.title2)
										.kerning(1.5)
										.padding(.leading, 32)
									
									VStack { Divider().backgroundColor(.secondary) }.padding(.trailing, 32)
								}
								.padding(.top, 8)
								//.offset(x: CGFloat(self.positionOfView.point?.x ?? 0))
								
								LostPauseTimeBarChart(statistics: self.statisticsSource, selectedPause: $selectedPause)
									.frame(width: timeSpan/factor)
									.frame(maxHeight: 260)
									.padding(.horizontal, 32)
									.padding(.vertical, 12)
									.onTapGesture {
										if let selectedPause {
											self.logStart = selectedPause.time
											self.logDuration = selectedPause.value
											
											showLogData = true
										}
									}
							}
							
							
							if self.statisticsSource.ditherTimes.count > 0 {
								
								VStack(alignment: .leading, spacing: 0)  {
									
									HStack {
										Text("Dither times in seconds")
											.font(.title2)
											.kerning(1.5)
											.padding(.leading, 32)
										VStack { Divider().backgroundColor(.secondary) }.padding(.trailing, 32)
									}
									.padding(.top, 24)
									//.offset(x: CGFloat(self.positionOfView.point?.x ?? 0))
									
									DitherTimesBarChart(statistics: self.statisticsSource, selectedPause: $selectedTimeSpan)
										.frame(width: timeSpan/factor)
										.frame(maxHeight: 260)
										.padding(.horizontal, 32)
										.padding(.vertical, 12)
										.onTapGesture {
											if let selectedTimeSpan {
												self.logStart = selectedTimeSpan.start
												self.logDuration = selectedTimeSpan.duration
												
												showLogData = true
											}
										}
								}
							}
							
							
							if self.statisticsSource.autoFocusTimes.count > 0 {
								
								VStack(alignment: .leading, spacing: 0)  {
									
									HStack {
										Text("Autofocus times in seconds")
											.font(.title2)
											.kerning(1.5)
											.padding(.leading, 32)
										VStack { Divider().backgroundColor(.secondary) }.padding(.trailing, 32)
									}
									.padding(.top, 24)
									//.offset(x: CGFloat(self.positionOfView.point?.x ?? 0))
									
									AFTimesBarChart(statistics: self.statisticsSource, selectedPause: $selectedAFTimeSpan)
										.frame(width: timeSpan/factor)
										.frame(maxHeight: 260)
										.padding(.horizontal, 32)
										.padding(.vertical, 12)
										.onTapGesture {
											if let selectedAFTimeSpan {
												self.logStart = selectedAFTimeSpan.start
												self.logDuration = selectedAFTimeSpan.duration
												
												showLogData = true
											}
										}
								}
							}
						}
						
						Spacer()
					}
				}
				.transition(.move(edge: .bottom))
				//.scrollPosition($positionOfView)
				//.animation(.default, value: positionOfView)
			}
			else if self.statisticsSource.status != .processing {
				VStack(alignment: .center) {
					
					Spacer()
					
					Text("No exposures found in the N.I.N.A. logfile.")
						.font(.title)
						.padding()
					
					Text("Check, if you have loaded the correct N.I.N.A. logfile and if you selected the correct timezone!")
						.font(.headline)
						.padding()
					
					Text("Sometimes, the content of the N.I.N.A. logfile changes and this application needs to be adapted to the changed logfile content in order to read it correctly. Please inform the developer using the feedback form if you encounter any issues.")
						.font(.body)
						.foregroundColor(.secondary)
						.padding()
					
					Spacer()
				}
				.frame(maxWidth: .infinity)
			}
			else {
				VStack(alignment: .center) {
					Spacer()
					Text("Please wait...")
						.font(.title)
						.padding()
					Spacer()
				}
			}
		}
		.task {
			await statisticsSource.update(for: session, selectedSession: sessionDate)
		}
		.overlay {
			
			if showLogData, let start = self.logStart, let duration = self.logDuration {
				
				showLog(start, duration)
				
			}
		}
		.animation(.easeInOut(duration: 0.2), value: self.statisticsSource.sequenceDuration)
		
	}
}
