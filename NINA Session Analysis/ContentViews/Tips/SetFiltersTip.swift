//
//  SetFiltersTip.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 12.05.24.
//

import SwiftUI
import TipKit

struct SetFiltersTip: Tip {
	
	@Parameter
	static var runCount: Int = UserDefaults.standard.value(forKey: "runCount") as? Int ?? 0
	
	var title: Text {
		Text("Change the color to be used for your different filters and the HFR/Mean threshold values!")
			.foregroundColor(Color.lightBlue)
	}
	
	var message: Text? {
		Text("The colors will be used within charts. The HFR/Mean threshold values will be used for calculations and might be different for different filters. These settings will overwrite the default HFR/Mean thresholds for these filters...")
	}
	
	var rules: [Rule] {
		
		#Rule(Self.$runCount) {
			$0 <= 5 
		}
	}
}

