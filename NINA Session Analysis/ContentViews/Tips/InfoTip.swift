//
//  InfoTip.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 22.05.24.
//

import SwiftUI
import TipKit

struct InfoTip: Tip, @unchecked Sendable {
	let tipText: LocalizedStringKey
	
	var title: Text {
		Text("Info").foregroundColor(Color.lightBlue)
	}
	 
	var message: Text? {
		Text(tipText)
	}
}
