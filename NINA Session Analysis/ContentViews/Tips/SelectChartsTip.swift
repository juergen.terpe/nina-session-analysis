//
//  SelectChartsTip.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 10.05.24.
//

import SwiftUI
import TipKit

struct SelectChartsTip: Tip {
	
	@Parameter
	static var runCount: Int = UserDefaults.standard.value(forKey: "runCount") as? Int ?? 0
	
	var title: Text {
		Text("Select the charts you want to use!")
			.foregroundStyle(Color.lightBlue)
	}
	 
	var message: Text? {
		Text("Select the charts of your interest. You can change this later. Your selection will be stored.")
	}
	
	var rules: [Rule] {
		
		#Rule(Self.$runCount) {
			$0 <= 10
		}
	}
	
}

struct SelectColumnsTip: Tip {
	
	@Parameter
	static var runCount: Int = UserDefaults.standard.value(forKey: "runCount") as? Int ?? 0
	
	var title: Text {
		Text("Select the columns you want to see!")
			.foregroundStyle(Color.lightBlue)
	}
	 
	var message: Text? {
		Text("Select the columns of your interest. You can change this later. Your selection will be stored.")
	}
	
	var rules: [Rule] {
		
		#Rule(Self.$runCount) {
			$0 <= 10
		}
	}
}


