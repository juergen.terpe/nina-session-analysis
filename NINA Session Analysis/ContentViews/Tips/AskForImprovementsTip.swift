//
//  AskForImprovementsTip.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 10.01.25.
//

import SwiftUI
import TipKit

struct AskForImprovementsTip: Tip {
	
	@Parameter
	static var runCount: Int = UserDefaults.standard.value(forKey: "runCount") as? Int ?? 0
	
	
	var title: Text {
		Text("Any Ideas for improvements?")
			.foregroundStyle(Color.lightBlue)
	}
	 
	var message: Text? {
		Text("Don't hesitate to contact me if you have any ideas or wishes for improvements!")
	}
	
	var rules: [Rule] {
		
		#Rule(Self.$runCount) {
			$0 > 10 && $0 % 10 == 0
		}
	}
}
