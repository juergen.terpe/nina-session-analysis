//
//  StartHereTip.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 10.05.24.
//

import SwiftUI
import TipKit

struct StartHereTip: Tip {
	
	@Parameter
	static var runCount: Int = UserDefaults.standard.value(forKey: "runCount") as? Int ?? 0
	
	
	var title: Text {
		Text("Start here: Select the lights folder")
			.foregroundStyle(Color.lightBlue)
	}
	 
	var message: Text? {
		Text("Select a folder containing all lights. Best practice is to have also the image metadata, the PHD2 log file and the NINA log file inside this folder.")
	}
	
	var rules: [Rule] {
		
		#Rule(Self.$runCount) {
			$0 <= 5
		}
	}
}

