//
//  ClickAndDragToSelectStarProfileTip.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 29.01.25.
//
import SwiftUI
import TipKit

struct ClickAndDragToSelectStarProfileTip: Tip {
	
	@Parameter
	static var runCount: Int = UserDefaults.standard.value(forKey: "runCount3D") as? Int ?? 0
	
	
	var title: Text {
		Text("Select Star Profile from Image")
			.foregroundStyle(Color.lightBlue)
	}
	 
	var message: Text? {
		Text("Click and drag to select the star profile from within the image!")
	}
	
	var rules: [Rule] {
		
		#Rule(Self.$runCount) {
			$0 < 5
		}
	}
}
