//
//  SessionInfoView.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 10.04.24.
//

import SwiftUI
import PureSwiftUI

struct SessionInfo: Identifiable {
	let id: Int
	let name: LocalizedStringKey
	let value: String
	var indent: Int = 0
	var divider: Bool = false
	var hint: Bool = false 
}


struct SessionInfoView: View {
	@State var session: SessionData
	
	var body: some View {
		ScrollView(.vertical) {
			LazyVStack(spacing: 4) {
				ForEach(session.getSessionInfo()) { info in
					HStack {
						if info.divider {
							Color.clear.frame(height: 5)
						} else {
							if info.indent > 0 {
								(Text("  ▻ ") + Text(info.name)).frame(width: 200, alignment: .trailing)
									.fontWeight(.light)
									.padding(.horizontal).padding(.vertical, 4)
									.foregroundColor(.primary)
							} else {
								
								Text(info.name).frame(width: 200, alignment: .trailing)
									.fontWeight(.light)
									.padding(.horizontal).padding(.vertical, 4)
									.foregroundColor(.primary)
							}
							Text(info.value).frame(minWidth: 200, maxWidth: .infinity, alignment: .leading)
								.fontWeight(.semibold)
								.padding(.horizontal).padding(.vertical, 4)
								.foregroundColor(info.hint ? .red :.primary)
						}
					}
					.padding(.horizontal, 8)
					.backgroundIf(!info.divider, info.id % 2 == 0 ? Color.gray.opacity(0.06) : Color.clear)
				}
			}
		}
		.id(session.id)
    }
}

