//
//  TableView.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 29.03.24.
//

import SwiftUI

struct ColumnsSelectorView: View {
	@Binding var session: SessionData
	
	var body: some View {
		LazyHGrid(rows: [GridItem(.adaptive(minimum: 20, maximum: 40))], spacing: 10) {
			ForEach(session.tableColumns.indices, id: \.self) { index in
				Toggle(isOn: $session.tableColumns[index].enabled) {
					Text(session.tableColumns[index].name)
						.help(session.tableColumns[index].helpText)
				}
				.toggleStyle(.checkbox)
				.frame(maxWidth: 200, alignment: .leading)
			}
		}
		.compositingGroup()
		
	}
}

struct TableView: View {
	@State var session: SessionData
	@Binding var showCheckList: Bool 
	@Binding var showSettings: Bool 
	@State private var selection: SessionImage.ID?
	@State private var sortOrder = [KeyPathComparator(\SessionImage.Index)]
	@Environment(\.openWindow) private var openWindow
	@Environment(\.dismissWindow) private var dismissWindow
	
	@ViewBuilder
	func columnContent(for id: TableImageColumnId, in image: SessionImage) -> some View {
		Group {
			if id == .marking {
				Button {
					image.toggleMarked()
					session.onContentUpdated()
				} label: {
					
					HStack {
						Spacer()
						Image(systemName: image.bad ? "checkmark.square" : "square")
							.imageScale(.large)
							.foregroundColor(image.bad ? .badColor : .green)
						Spacer()
					}
				}
				.buttonStyle(PlainButtonStyle())
				
			} else {
				Text(image.columnText(for: id))
					.foregroundColor(image.bad ? .badColor : image.columnColor(for: id))
			}
		}
	}
	
	@ViewBuilder
	func columnContextMenu( _ image: SessionImage) -> some View {
		if image.isFits {
			
			Button {
				dismissWindow(id: "InternalViewer")
				openWindow(id: "InternalViewer", value: image.id)
			} label: {
				Text("Show in Viewer")
			}
			
			if self.session.quickFitsAvailable {
				Button {
					self.session.openInQuickFits( image )
				} label: {
					Text("Open in QuickFits")
				}
			}
			
			if self.session.fitsPreviewAvailable {
				Button {
					self.session.openInFitsPreview( image )
				} label: {
					Text("Open in FITS Preview")
				}
			}
		}
		
		if self.session.pixInsightAvailable {
			Button {
				self.session.openInPixInsight( image )
			} label: {
				Text("Open in PixInsight")
			}
		}
		
		if self.session.astapAvailable {
			Button {
				self.session.openInAstap( image )
			} label: {
				Text("Open in Astap")
			}
		}
	}
	
	
	var body: some View {
		
		VStack {
			
			Table(session.filteredImages, selection: $selection, sortOrder: $session.sortingOrder) {
				
				TableColumnForEach(session.tableDataColumns) { column in
					
					/*
					TableColumn(column.name, value: column.element)
						.contentShape(Rectangle())
						.contextMenu {
							columnContextMenu(image)
						}*/
					
					TableColumn(column.name) { image in
						
						columnContent(for: column.id, in: image)
							.frame(maxWidth: .infinity)
							.contentShape(Rectangle())
							.contextMenu {
								columnContextMenu(image)
							}
					}
				}
			}
			.onChange(of: session.sortingOrder) { _, _ in
				session.onContentUpdated()
			}
			
			
			GuideGraphView(session: session, selectedimage: session.getImageById(selection))
				.frame(height: 260)
			
		}
		.onAppear(perform: {
			selection = session.filteredImages.first?.id
		})
		.onChange(of: session.selectedSessionDate, { _, _ in
			selection = session.filteredImages.first?.id
		})
		.onChange(of: session.selectedFilter, { _, _ in
			selection = session.filteredImages.first?.id
		})
		.onChange(of: session.selectedTarget, { _, _ in
			selection = session.filteredImages.first?.id
		})
		.onChange(of: session.selectedExposure, { _, _ in
			selection = session.filteredImages.first?.id
		})
		.onChange(of: session.selectedPierSide, { _, _ in
			selection = session.filteredImages.first?.id
		})
		.overlay {
			HStack(alignment: .firstTextBaseline) {
				Spacer()
				if self.showCheckList {
					ColumnsSelectorView(session: $session)
						.padding()
						.background(.ultraThinMaterial, in: RoundedRectangle(cornerRadius: 8, style: .continuous))
						.transition(.move(edge: .trailing))
					
				} else if showSettings {
					AnalysationSettingsView(session: session)
						.transition(.move(edge: .trailing))
					
				}
			}
		}
	}
}

