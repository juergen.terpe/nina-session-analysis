//
//  DitheringView.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 30.03.24.
//

import SwiftUI

struct DitheringView: View {
	@State var session: SessionData
	@State var imageIndex: Int = 0
	@State var playing: Bool = false
	@State var targetRotation: Bool = false
	@AppStorage("DitheringRect_ShowAll") var showAll: Bool = false
	
	let timer = Timer.publish(every: 0.25, on: .main, in: .common).autoconnect()
	
	private func getImage() -> SessionImage? {
		let filteredImages = session.filteredImages
		if imageIndex >= 0 && imageIndex < filteredImages.count {
			return filteredImages[imageIndex]
		}
		let index = imageIndex % filteredImages.count
		if index >= 0 && index < filteredImages.count {
			return filteredImages[index]
		}
				
		return nil
	}
	
	
    var body: some View {
		
		Group {
			if let region = session.getSurveyRegion(), let image = getImage() {
				
				VStack {
					
					GeometryReader(content: { geometry in
						ZStack {
							Color.black
							DitheringRectangle(region: region, size: geometry.size, 
											   image: image,
											   images: session.filteredImages,
											   showTargetRotation: $targetRotation,
											   showAll: $showAll)
							
							
							
							HStack {
								VStack {
									VStack(alignment: .leading) {
										Text(image.FileName).foregroundColor(image.bad ? .badColor : .white)
										Text("Drift α(px): \(image.DriftRAPixel)").foregroundColor(image.bad ? .badColor : .white)
										Text("Drift δ(px): \(image.DriftDECPixel)").foregroundColor(image.bad ? .badColor : .white)
										Text("Pierside:    \(image.pierSide)").foregroundColor(image.bad ? .badColor : .white)
									}
									.font(.headline)
									.padding()
									.background(.ultraThinMaterial)
								
									Spacer()
								}
									
								Spacer()
							}
						}
						.drawingGroup(opaque: true)
					})
					
					HStack {
						
						Button(action: {
							playing.toggle()
						}, label: {
							Image(systemName: playing ? "pause.fill" : "play.fill")
								.resizable()
								.frame(width: 20, height: 20)
						})
						.buttonStyle(BorderlessButtonStyle())
						.padding()
						
						Slider(value: .convert(from: $imageIndex), in: 0...Float(session.filteredImages.count-1))
							.padding()
						
						VStack(alignment: .leading) {
							
							Toggle(isOn: $showAll) {
								Text("Show all frames")
							}
							.toggleStyle(.checkbox)
							
							Toggle(isOn: $targetRotation) {
								Text("Target rotation")
							}
							.toggleStyle(.checkbox)
						}
						.padding()
					}
					
				}
			}
		}
		.padding(.horizontal)
		.onReceive(timer, perform: { _ in
			if playing {
				let nextIndex = (imageIndex + 1) % session.filteredImages.count
				self.imageIndex = nextIndex
			}
		})
	}
}

