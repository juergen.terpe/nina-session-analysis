//
//  StatisticsTableView.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 01.04.24.
//

import SwiftUI
import PureSwiftUI

struct StatisticsTableView: View {
	var session: SessionData
	let valueWidth: CGFloat = 80
	
	func getStatistics() -> SessionStatistics? {
		return session.getSessionStatistics()
	}
	
	var body: some View {
		Group {
			
			if let statistics = getStatistics() {
				
				VStack(alignment: .leading, spacing: 0) {
					if let hfr = statistics.hfr {
						HStack(spacing: 0) {
							Text("HFR ").frame(maxWidth: .infinity, alignment: .trailing).borderColor(Color.gray, width: 0.5).background(Color.blue.opacity(0.1))
							Text("\(hfr.min, specifier: "%.2f") ").frame(width: valueWidth, alignment: .trailing).borderColor(Color.gray, width: 0.5)
							Text("\(hfr.mean, specifier: "%.2f") ").frame(width: valueWidth, alignment: .trailing).borderColor(Color.gray, width: 0.5)
							Text("\(hfr.max, specifier: "%.2f") ").frame(width: valueWidth, alignment: .trailing).borderColor(Color.gray, width: 0.5)
						}
						.background(Color.blue.opacity(0.1))
						
					}
					if let fwhm = statistics.fwhm {
						HStack(spacing: 0) {
							Text("FWHM ").frame(maxWidth: .infinity, alignment: .trailing).borderColor(Color.gray, width: 0.5).background(Color.blue.opacity(0.1))
							Text("\(fwhm.min, specifier: "%.2f") ").frame(width: valueWidth, alignment: .trailing).borderColor(Color.gray, width: 0.5)
							Text("\(fwhm.mean, specifier: "%.2f") ").frame(width: valueWidth, alignment: .trailing).borderColor(Color.gray, width: 0.5)
							Text("\(fwhm.max, specifier: "%.2f") ").frame(width: valueWidth, alignment: .trailing).borderColor(Color.gray, width: 0.5)
						}
						
					}
					if let nStars = statistics.numStars {
						HStack(spacing: 0) {
							Text("#Stars ").frame(maxWidth: .infinity, alignment: .trailing).borderColor(Color.gray, width: 0.5).background(Color.blue.opacity(0.1))
							Text("\(Int(nStars.min)) ").frame(width: valueWidth, alignment: .trailing).borderColor(Color.gray, width: 0.5)
							Text("\(Int(nStars.mean)) ").frame(width: valueWidth, alignment: .trailing).borderColor(Color.gray, width: 0.5)
							Text("\(Int(nStars.max)) ").frame(width: valueWidth, alignment: .trailing).borderColor(Color.gray, width: 0.5)
						}
						.background(Color.blue.opacity(0.1))
					}
					if let ecc = statistics.eccentricty {
						HStack(spacing: 0) {
							Text("Eccentricity ").frame(maxWidth: .infinity, alignment: .trailing).borderColor(Color.gray, width: 0.5).background(Color.blue.opacity(0.1))
							Text("\(ecc.min, specifier: "%.2f") ").frame(width: valueWidth, alignment: .trailing).borderColor(Color.gray, width: 0.5)
							Text("\(ecc.mean, specifier: "%.2f") ").frame(width: valueWidth, alignment: .trailing).borderColor(Color.gray, width: 0.5)
							Text("\(ecc.max, specifier: "%.2f") ").frame(width: valueWidth, alignment: .trailing).borderColor(Color.gray, width: 0.5)
						}
						
					}
					if let mean = statistics.mean {
						HStack(spacing: 0) {
							Text("Mean ").frame(maxWidth: .infinity, alignment: .trailing).borderColor(Color.gray, width: 0.5).background(Color.blue.opacity(0.1))
							Text("\(Int(mean.min)) ").frame(width: valueWidth, alignment: .trailing).borderColor(Color.gray, width: 0.5)
							Text("\(Int(mean.mean)) ").frame(width: valueWidth, alignment: .trailing).borderColor(Color.gray, width: 0.5)
							Text("\(Int(mean.max)) ").frame(width: valueWidth, alignment: .trailing).borderColor(Color.gray, width: 0.5)
						}
						.background(Color.blue.opacity(0.1))
					}
					if let median = statistics.median {
						HStack(spacing: 0) {
							Text("Median ").frame(maxWidth: .infinity, alignment: .trailing).borderColor(Color.gray, width: 0.5).background(Color.blue.opacity(0.1))
							Text("\(Int(median.min)) ").frame(width: valueWidth, alignment: .trailing).borderColor(Color.gray, width: 0.5)
							Text("\(Int(median.mean)) ").frame(width: valueWidth, alignment: .trailing).borderColor(Color.gray, width: 0.5)
							Text("\(Int(median.max)) ").frame(width: valueWidth, alignment: .trailing).borderColor(Color.gray, width: 0.5)
						}
						
					}
					if let rms = statistics.rms {
						HStack(spacing: 0) {
							Text("RMS ").frame(maxWidth: .infinity, alignment: .trailing).borderColor(Color.gray, width: 0.5).background(Color.blue.opacity(0.1))
							Text("\(rms.min, specifier: "%.2f")\" ").frame(width: valueWidth, alignment: .trailing).borderColor(Color.gray, width: 0.5)
							Text("\(rms.mean, specifier: "%.2f")\" ").frame(width: valueWidth, alignment: .trailing).borderColor(Color.gray, width: 0.5)
							Text("\(rms.max, specifier: "%.2f")\" ").frame(width: valueWidth, alignment: .trailing).borderColor(Color.gray, width: 0.5)
						}
						.background(Color.blue.opacity(0.1))
					}
					if let rmsRA = statistics.rmsRA {
						HStack(spacing: 0) {
							Text("RMS(RA) ").frame(maxWidth: .infinity, alignment: .trailing).borderColor(Color.gray, width: 0.5).background(Color.blue.opacity(0.1))
							Text("\(rmsRA.min, specifier: "%.2f")\" ").frame(width: valueWidth, alignment: .trailing).borderColor(Color.gray, width: 0.5)
							Text("\(rmsRA.mean, specifier: "%.2f")\" ").frame(width: valueWidth, alignment: .trailing).borderColor(Color.gray, width: 0.5)
							Text("\(rmsRA.max, specifier: "%.2f")\" ").frame(width: valueWidth, alignment: .trailing).borderColor(Color.gray, width: 0.5)
						}
						
					}
					if let rmsDEC = statistics.rmsDEC {
						HStack(spacing: 0) {
							Text("RMS(DEC) ").frame(maxWidth: .infinity, alignment: .trailing).borderColor(Color.gray, width: 0.5).background(Color.blue.opacity(0.1))
							Text("\(rmsDEC.min, specifier: "%.2f")\" ").frame(width: valueWidth, alignment: .trailing).borderColor(Color.gray, width: 0.5)
							Text("\(rmsDEC.mean, specifier: "%.2f")\" ").frame(width: valueWidth, alignment: .trailing).borderColor(Color.gray, width: 0.5)
							Text("\(rmsDEC.max, specifier: "%.2f")\" ").frame(width: valueWidth, alignment: .trailing).borderColor(Color.gray, width: 0.5)
						}
						.background(Color.blue.opacity(0.1))
					}
				}
				.font(.body)
				
			}

			
		}
    }
}


