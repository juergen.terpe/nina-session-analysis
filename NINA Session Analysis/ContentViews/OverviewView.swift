//
//  OverviewView.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 30.03.24.
//

import SwiftUI
import CoreLocation
import PureSwiftUI

class OverviewChart: Identifiable, Equatable {
	
	
	var id: String {
		if let item2 = self.item2 {
			return "\(item.rawValue)-\(item2.rawValue)"
		}
		
		return item.rawValue
	}
	var item: GraphItem
	var item2: GraphItem?
	var area: Bool
	var enabled: Bool {
		didSet {
			self.store()
		}
	}
	
	init(item: GraphItem, area: Bool, enabled: Bool) {
		self.item = item
		self.area = area
		self.enabled = enabled
		
		let defaults = UserDefaults.standard
		self.enabled = defaults.bool(forKey: "OverviewCharts_"+item.rawValue)
	}
	
	init(item1: GraphItem, item2: GraphItem, enabled: Bool) {
		self.item = item1
		self.item2 = item2
		self.area = false
		self.enabled = enabled
		
		let defaults = UserDefaults.standard
		self.enabled = defaults.bool(forKey: "OverviewCharts_"+item1.rawValue + "_" + item2.rawValue)
	}
	
	
	func store() {
		let defaults = UserDefaults.standard
		if let item2 = self.item2 {
			defaults.set(self.enabled, forKey: "OverviewCharts_"+item.rawValue + "_" + item2.rawValue)
		} else {
			defaults.set(self.enabled, forKey: "OverviewCharts_"+item.rawValue)
		}
	}
	
	static func == (lhs: OverviewChart, rhs: OverviewChart) -> Bool {
		return lhs.id == rhs.id
	}
}

@Observable class OverViewCharts {
	var charts: [OverviewChart] = [
		OverviewChart(item1: .NUMSTARS, item2: .AIRMASS, enabled: false),
		OverviewChart(item1: .NUMSTARS, item2: .MEAN, enabled: true),
		OverviewChart(item1: .ECCENTRICITY, item2: .RELRMSRADEC, enabled: true),
		OverviewChart(item1: .ECCENTRICITY, item2: .MaxPeak, enabled: true),
		OverviewChart(item1: .HFR, item2: .AIRMASS, enabled: true),
		OverviewChart(item1: .HFR, item2: .RMS, enabled: true),
		OverviewChart(item1: .HFR, item2: .WINDSPD, enabled: false),
		OverviewChart(item1: .HFR, item2: .WINDGUST, enabled: false),
		OverviewChart(item1: .MEAN, item2: .AIRMASS, enabled: true),
		OverviewChart(item1: .MEAN, item2: .SKYBRIGHTNESS, enabled: false),
		OverviewChart(item1: .MEAN, item2: .SkyQuality, enabled: false),
		OverviewChart(item1: .RMS, item2: .AIRMASS, enabled: false),
		OverviewChart(item1: .DELTAT, item2: .MEAN, enabled: false),
		OverviewChart(item: .NUMSTARS, area: true, enabled: true),
		OverviewChart(item: .HFR, area: true, enabled: true),
		OverviewChart(item: .FWHM, area: true, enabled: true),
		OverviewChart(item: .ECCENTRICITY, area: true, enabled: true),
		OverviewChart(item: .RMS, area: true, enabled: true),
		OverviewChart(item: .RMSRA, area: true, enabled: false),
		OverviewChart(item: .RMSDEC, area: true, enabled: false),
		OverviewChart(item: .RELRMSRADEC, area: false, enabled: false),
		OverviewChart(item: .FOCPOS, area: false, enabled: false),
		OverviewChart(item: .DITHERPX, area: false, enabled: true),
		OverviewChart(item: .DITHERRAPX, area: false, enabled: false),
		OverviewChart(item: .DITHERDECPX, area: false, enabled: false),
		OverviewChart(item: .AIRMASS, area: true, enabled: true),
		OverviewChart(item: .MEAN, area: true, enabled: true),
		OverviewChart(item: .MEDIAN, area: true, enabled: false),
		OverviewChart(item: .MOONALT, area: false, enabled: true),
		OverviewChart(item: .SUNALT, area: false, enabled: false),
		OverviewChart(item: .ADUMIN, area: true, enabled: false),
		OverviewChart(item: .ADUMAX, area: true, enabled: false),
		OverviewChart(item: .CLOUDCVR, area: true, enabled: false),
		OverviewChart(item: .DEWPOINT, area: false, enabled: false),
		OverviewChart(item: .AMBTEMP, area: false, enabled: false),
		OverviewChart(item: .HUMIDITY, area: true, enabled: false),
		OverviewChart(item: .WINDSPD, area: true, enabled: false),
		OverviewChart(item: .WINDGUST, area: true, enabled: false),
		OverviewChart(item: .SKYBRIGHTNESS, area: true, enabled: false),
		OverviewChart(item: .SKYTEMPERATURE, area: false, enabled: false),
		OverviewChart(item: .SkyQuality, area: true, enabled: true)
		
	]
	
	var visibleCharts: [OverviewChart] {
		self.charts.filter({$0.enabled})
	}
	
	func store() {
		for chart in self.charts {
			chart.store()
		}
	}
}

enum OverViewTabs: Int {
	case summary
	case analysis
	case sky
}

extension SessionData {
	
	func getBinnedData(for item: GraphItem, count: Int) -> BinnedData {
		return BinnedData(session: self, item: item, count: count)
	}
}

@Observable
fileprivate class BinnedDataChartModel {
	let DistributionValueCount: Int = 15
	var data: [GraphItem : BinnedData] = [:]
	private var id: UUID? = nil
	
	private let graphItems = [GraphItem.HFR, .RMS, .FWHM, .ECCENTRICITY, .MEAN, .MEDIAN, .AIRMASS, .NUMSTARS, .SkyQuality, .SKYTEMPERATURE]
	
	fileprivate func update(session: SessionData) async {
		if id != nil && session.id == id && data.isNotEmpty {
			return
		}
		
		var tempData = [GraphItem: BinnedData]()
		
		await withTaskGroup(of: (GraphItem, BinnedData).self) { group in
			for item in graphItems {
				group.addTask {
					let binnedData = session.getBinnedData(for: item, count: self.DistributionValueCount)
					return (item, binnedData)
				}
			}
			
			for await (item, binnedData) in group {
				tempData[item] = binnedData
			}
		}
		
		let transfData = tempData
		
		await MainActor.run {
			self.data = transfData
			self.id = session.id
		}
	}
}

@Observable
fileprivate class OverViewChartsModels {
	private var charts: [GraphItem : DataChartData] = [:]
	
	func update(session: SessionData, for chart: OverviewChart) async {
		
		if let chartData = self.charts[chart.item] {
			await chartData.update(chart.item, session: session, area: chart.area)
		} else {
			let chartData = DataChartData()
			await MainActor.run {
				charts[chart.item] = chartData
			}
			await chartData.update(chart.item, session: session, area: chart.area)
		}
	}
	
	func exists(for item: GraphItem) -> Bool {
		return self.charts[item] != nil
	}
	
	func getChartData(for item: GraphItem) -> DataChartData? {
		if let chartData = self.charts[item], chartData.data.isNotEmpty {
			return chartData
		}
		return nil
	}
	
}


struct OverviewView: View {
	@State var session: SessionData
	@Binding var showCheckList: Bool 
	@Binding var showSettings: Bool
	@Binding var toolsActive: Bool
	let GraphHeight: CGFloat = 280
	let DGraphHeight: CGFloat = 360
	let DGraphWidth: CGFloat = 480
	
	@State private var allcharts = OverViewCharts()
	//@State private var dataChartsModels = OverViewChartsModels()
	@State private var binnedDataModel = BinnedDataChartModel()
	@State private var selectedDistributionChart: GraphItem?
	@State private var selectedDistributionChartData: BinnedData?
	@State private var selectedChart: OverviewChart?
	@State private var selectedAutoFocusChart: AutoFocus?
	@State private var phaseSessionDateIndex: Int?
	@Namespace var overviewNamespace
	@State private var timeLines: [DateOfSession : CapturingTimelineData] = [:]
			
	func getChartSelector() -> some View {
		LazyHGrid(rows: [GridItem(.adaptive(minimum: 20, maximum: 40))], spacing: 10) {
			ForEach(allcharts.charts.indices, id: \.self) { index in
				Toggle(isOn: $allcharts.charts[index].enabled) {
					if allcharts.charts[index].item2 != nil {
						Text("\(allcharts.charts[index].item.rawValue) : \(allcharts.charts[index].item2!.rawValue)")
					} else {
						Text(allcharts.charts[index].item.rawValue)
					}
				}
				.toggleStyle(.checkbox)
				.frame(maxWidth: 260, alignment: .leading)
			}
		}
	}
	
	var overlayBody: some View {
		VStack {
			
			if let selectedChart = self.selectedChart {
				VStack(spacing: 0) {
					HStack {
						Spacer()
						Button {
							withAnimation {
								self.selectedChart = nil
								self.selectedAutoFocusChart = nil
								self.selectedDistributionChart = nil
								self.selectedDistributionChartData = nil
							}
							
						} label: {
							Image(systemName: "xmark.circle")
								.resizable()
								.imageScale(.medium)
								.frame(width: 24, height: 24)
								.foregroundColor(.secondary)
								.padding()
						}
						.buttonStyle(PlainButtonStyle())
						
					}
					
					if let item2 = selectedChart.item2 {
						MergedDataChartDetails(session: session, item1: selectedChart.item, item2: item2,
										color1: Color.blue, color2: Color.green)
						
					} else {
						
						DataChartDetailView(session: self.session,
											sessionImages: self.session.filteredImages,
											item: .constant(selectedChart.item),
											timelines: timeLines,
											area: selectedChart.area)
					}
				}
				.background(Color.controlBackgroundColor)
			} else if let selectedChartData = self.selectedDistributionChartData {
				VStack {
					HStack {
						Text(selectedChartData.item.rawValue)
							.kerning(1.5)
							.font(.title)
							.padding(.horizontal)
						Spacer()
						Button {
							withAnimation {
								self.selectedDistributionChart = nil
								self.selectedDistributionChartData = nil
								self.selectedAutoFocusChart = nil
							}
							
						} label: {
							Image(systemName: "xmark.circle")
								.resizable()
								.imageScale(.medium)
								.frame(width: 24, height: 24)
								.foregroundColor(.secondary)
								.padding()
						}
						.buttonStyle(PlainButtonStyle())
						
					}
					
					DistributionChartDetailView(binnedData: selectedChartData)
					
				}
				.background(Color.controlBackgroundColor)
			} else if let selectedChartData = self.selectedAutoFocusChart {
				VStack {
					HStack {
						
						Text("Autofocus").kerning(1.5).font(.title).padding(.horizontal)
						
						Spacer()
						Button {
							withAnimation {
								self.selectedDistributionChart = nil
								self.selectedDistributionChartData = nil
								self.selectedAutoFocusChart = nil
							}
							
						} label: {
							Image(systemName: "xmark.circle")
								.resizable()
								.imageScale(.medium)
								.frame(width: 24, height: 24)
								.foregroundColor(.secondary)
								.padding()
						}
						.buttonStyle(PlainButtonStyle())
						
					}
					
					AutoFocusChart(autoFocus: selectedChartData)
						.padding()
						.background(Color.chartBackground).cornerRadius(8)
						.padding(.horizontal)
						.padding(.bottom)
					
					
				}
				.background(Color.controlBackgroundColor)
				
			}
			
		}
	}
	
	@ViewBuilder
	func sessionMoonPhaseAndRiseSetTimes() -> some View {
		Group {
			if let jd = session.getSelectedSessionJulianDate() {
				VStack {
					
					Text(jd.toDate().formatted(date: .abbreviated, time: .omitted))
						.font(.title2).fontWeight(.semibold)
						.foregroundColor(Color.cyan)
					
					
					MoonPhaseView(jd: jd)
						.frame(maxWidth: 200, maxHeight: 200)
						.padding()
					
					 
					Spacer()
					
					
					if let loc = session.getLocation(for: jd) {
						RiseSetSunMoonView(jd: jd, location: loc)
							.padding()
					}
				}
				.padding()
				.frame(width: 400)
				.background(Color.chartBackground.gradient.opacity(0.5)).cornerRadius(8)
				.padding()
			} else {
				VStack {
					
					HStack {
						Button {
							var index = self.phaseSessionDateIndex! - 1
							if index < 0 {
								index += self.session.getSessionDates().count
							}
							self.phaseSessionDateIndex = index
							
						} label: {
							Image(systemName: "chevron.left.circle")
								.resizable()
								.frame(width: 24, height: 24)
								.imageScale(.medium)
								.foregroundColor(.cyan)
						}
						.buttonStyle(PlainButtonStyle())
						.disabled(self.phaseSessionDateIndex == nil)
						Spacer()
						
						if let index = self.phaseSessionDateIndex {
							let jd = self.session.getSessionDates()[index]
							Text(jd.toDate().formatted(date: .abbreviated, time: .omitted))
								.font(.title2).fontWeight(.semibold)
								.foregroundColor(Color.cyan)
						}
						
						Spacer()
						Button {
							var index = self.phaseSessionDateIndex! + 1
							if index >= self.session.getSessionDates().count {
								index -= self.session.getSessionDates().count
							}
							self.phaseSessionDateIndex = index
						} label: {
							Image(systemName: "chevron.right.circle")
								.resizable()
								.frame(width: 24, height: 24)
								.foregroundColor(.cyan)
						}
						.buttonStyle(PlainButtonStyle())
						.disabled(self.phaseSessionDateIndex == nil)
					}
					
					if let index = self.phaseSessionDateIndex {
						let jd = self.session.getSessionDates()[index]
						
						MoonPhaseView(jd: jd)
							.frame(maxWidth: 200, maxHeight: 200)
							.padding()
							.id(session.selectedSessionDate)
						
						Spacer()
						if let loc = session.getLocation(for: jd) {
							RiseSetSunMoonView(jd: jd, location: loc)
								.padding()
								.id(session.selectedSessionDate)
							
						}
					}
				}
				.padding()
				.frame(width: 400)
				.background(Color.chartBackground.gradient.opacity(0.5)).cornerRadius(8)
				.padding()
				.onAppear {
					self.phaseSessionDateIndex = self.session.getSessionDates().count > 0 ? 0 : nil
 				}
			}
			
			
		}
	}
	
	var header: some View {
		Group {
			LazyVStack(alignment: .leading) {
				HStack {
					
					VStack {
						StatisticsTableView(session: session)
							.padding()
							.background(.thinMaterial)
							.cornerRadius(8)
							.padding()
						
						sessionMoonPhaseAndRiseSetTimes()
					}
					.frame(width: 400)
					.animation(Animation.easeInOut, value: session.id)
					
					
					
					ScrollView(.horizontal) {
						
						LazyVStack {
							
							HStack {
								
								if self.selectedDistributionChart != .HFR, let binnedData = self.binnedDataModel.data[.HFR], binnedData.data.isNotEmpty {
									DistributionChart(binnedData: binnedData, showNormalDist: false)
										.compositingGroup()
										.padding()
										.background(.thinMaterial)
										//.background(Color.chartBackground)
										.cornerRadius(8)
										.frame(width: DGraphWidth, height: DGraphHeight)
										.padding()
										.matchedGeometryEffect(id: GraphItem.HFR, in: overviewNamespace)
										.onTapGesture {
											withAnimation {
												self.selectedDistributionChartData = binnedData
												self.selectedDistributionChart = .HFR
												self.toolsActive = false
											}
										}
									
								}
								
								if self.selectedDistributionChart != .ECCENTRICITY, let binnedData = self.binnedDataModel.data[.ECCENTRICITY], binnedData.data.isNotEmpty {
									DistributionChart(binnedData: binnedData, showNormalDist: false)
										.compositingGroup()
										.padding()
										.background(.thinMaterial)
										//.background(Color.chartBackground)
										.cornerRadius(8)
										.frame(width: DGraphWidth, height: DGraphHeight)
										.padding()
										.matchedGeometryEffect(id: GraphItem.ECCENTRICITY, in: overviewNamespace)
										.onTapGesture {
											withAnimation {
												self.selectedDistributionChartData = binnedData
												self.selectedDistributionChart = GraphItem.ECCENTRICITY
												self.toolsActive = false
											}
										}
								}
								
								
								if self.selectedDistributionChart != .RMS, let binnedData = self.binnedDataModel.data[.RMS], binnedData.data.isNotEmpty {
									DistributionChart(binnedData: binnedData, showNormalDist: false)
										.compositingGroup()
										.padding()
										.background(.thinMaterial)
										//.background(Color.chartBackground)
										.cornerRadius(8)
										.frame(width: DGraphWidth, height: DGraphHeight)
										.padding()
										.matchedGeometryEffect(id: GraphItem.RMS, in: overviewNamespace)
										.onTapGesture {
											withAnimation {
												self.selectedDistributionChartData = binnedData
												self.selectedDistributionChart = GraphItem.RMS
												self.toolsActive = false
											}
										}
								}
								
								if self.selectedDistributionChart != .FWHM, let binnedData = self.binnedDataModel.data[.FWHM], binnedData.data.isNotEmpty {
									DistributionChart(binnedData: binnedData, showNormalDist: false)
										.compositingGroup()
										.padding()
										.background(.thinMaterial)
										//.background(Color.chartBackground)
										.cornerRadius(8)
										.frame(width: DGraphWidth, height: DGraphHeight)
										.padding()
										.matchedGeometryEffect(id: GraphItem.FWHM, in: overviewNamespace)
										.onTapGesture {
											withAnimation {
												self.selectedDistributionChartData = binnedData
												self.selectedDistributionChart = GraphItem.FWHM
												self.toolsActive = false
											}
										}
									
								}
								
								Spacer(minLength: 0)
							}
							
							HStack {
								
								if self.selectedDistributionChart != .MEDIAN, let binnedData = self.binnedDataModel.data[.MEDIAN], binnedData.data.isNotEmpty {
									DistributionChart(binnedData: binnedData, showNormalDist: false)
										.compositingGroup()
										.padding()
										.background(.thinMaterial)
										//.background(Color.chartBackground)
										.cornerRadius(8)
										.frame(width: DGraphWidth, height: DGraphHeight)
										.padding()
										.matchedGeometryEffect(id: GraphItem.MEDIAN, in: overviewNamespace)
										.onTapGesture {
											withAnimation {
												self.selectedDistributionChartData = binnedData
												self.selectedDistributionChart = GraphItem.MEDIAN
												self.toolsActive = false
											}
										}
								}
								
								if self.selectedDistributionChart != .AIRMASS, let binnedData = self.binnedDataModel.data[.AIRMASS], binnedData.data.isNotEmpty {
									DistributionChart(binnedData: binnedData, showNormalDist: false)
										.compositingGroup()
										.padding()
										.background(.thinMaterial)
										//.background(Color.chartBackground)
										.cornerRadius(8)
										.frame(width: DGraphWidth, height: DGraphHeight)
										.padding()
										.matchedGeometryEffect(id: GraphItem.AIRMASS, in: overviewNamespace)
										.onTapGesture {
											withAnimation {
												self.selectedDistributionChartData = binnedData
												self.selectedDistributionChart = GraphItem.AIRMASS
												self.toolsActive = false
											}
										}
									
								}
								
								if self.selectedDistributionChart != .NUMSTARS, let binnedData = self.binnedDataModel.data[.NUMSTARS], binnedData.data.isNotEmpty  {
									DistributionChart(binnedData: binnedData, showNormalDist: false)
										.compositingGroup()
										.padding()
										.background(.thinMaterial)
										//.background(Color.chartBackground)
										.cornerRadius(8)
										.frame(width: DGraphWidth, height: DGraphHeight)
										.padding()
										.matchedGeometryEffect(id: GraphItem.NUMSTARS, in: overviewNamespace)
										.onTapGesture {
											withAnimation {
												self.selectedDistributionChartData = binnedData
												self.selectedDistributionChart = GraphItem.NUMSTARS
												self.toolsActive = false
											}
										}
									
								}
								
								if self.selectedDistributionChart != .MEAN, let binnedData = self.binnedDataModel.data[.MEAN], binnedData.data.isNotEmpty {
									DistributionChart(binnedData: binnedData, showNormalDist: false)
										.compositingGroup()
										.padding()
										.background(.thinMaterial)
										//.background(Color.chartBackground)
										.cornerRadius(8)
										.frame(width: DGraphWidth, height: DGraphHeight)
										.padding()
										.matchedGeometryEffect(id: GraphItem.MEAN, in: overviewNamespace)
										.onTapGesture {
											withAnimation {
												self.selectedDistributionChartData = binnedData
												self.selectedDistributionChart = GraphItem.MEAN
												self.toolsActive = false
											}
										}
									
								}
								
								if self.selectedDistributionChart != .SkyQuality, let binnedData = self.binnedDataModel.data[.SkyQuality], binnedData.data.isNotEmpty {
									DistributionChart(binnedData: binnedData, showNormalDist: false)
										.compositingGroup()
										.padding()
										.background(.thinMaterial)
										//.background(Color.chartBackground)
										.cornerRadius(8)
										.frame(width: DGraphWidth, height: DGraphHeight)
										.padding()
										.matchedGeometryEffect(id: GraphItem.SkyQuality, in: overviewNamespace)
										.onTapGesture {
											withAnimation {
												self.selectedDistributionChartData = binnedData
												self.selectedDistributionChart = GraphItem.SkyQuality
												self.toolsActive = false
											}
										}
								}
								
								Spacer(minLength: 0)
							}
							
						}
						.onChange(of: session.id, debounceTime: .seconds(0.5), perform: { _ in
							Task {
								await self.binnedDataModel.update(session: self.session)
							}
						})
						.task {
							await self.binnedDataModel.update(session: self.session)
						}
					}
					
					
				}
			}
		}
		.padding()
	}
	
	private func getTimeLineData(for date: DateOfSession) -> CapturingTimelineData? {
		if let cached = self.timeLines[date] {
			return cached
		}
		return nil
	}
	
	@ViewBuilder
	private func getSingleNight(_ timelineDate: CapturingTimelineData, date: DateOfSession) -> some View {
		Group {
			HStack {
				Text("Timeline").kerning(1.5).font(.title).foregroundColor(.primary).padding()
				VStack { Divider().backgroundColor(.secondary) }
			}
			.padding()
			//.background(Color.darkBlue).cornerRadius(8)
			
			CapturingTimelineView(data: timelineDate, date: date)
				.frame(height: 360)
				.padding()
				.padding()
		}
		
	}
	
	@ViewBuilder
	private func getAutoFocusImages(for af: AutoFocus) -> some View {
		HStack(spacing: 8) {
			
			if (af.previousImages.isNotEmpty && af.nextImages.isNotEmpty) {
				
				Grid(alignment: .trailing, horizontalSpacing: 8) {
					GridRow {
						Text("Filter").foregroundColor(.teal)
						Text("Mean").foregroundColor(.teal)
						Text("HFR").foregroundColor(.teal)
					}
					
					ForEach(af.previousImages) { image in
						GridRow {
							Text(image.filter).foregroundColor(.secondary)
							Text("\(image.mean, specifier: "%.1f")").monospaced().foregroundColor(.secondary)
							Text("\(image.hfr, specifier: "%.2f")").monospaced().foregroundColor(.secondary)
						}
					}
				}
				
				Spacer()
				
				Grid(alignment: .trailing, horizontalSpacing: 8) {
					GridRow {
						Text("Filter").foregroundColor(.teal)
						Text("Mean").foregroundColor(.teal)
						Text("HFR").foregroundColor(.teal)
					}
					
					ForEach(af.nextImages) { image in
						GridRow {
							Text(image.filter).foregroundColor(.secondary)
							Text("\(image.mean, specifier: "%.1f")").monospaced().foregroundColor(.secondary)
							Text("\(image.hfr, specifier: "%.2f")").monospaced().foregroundColor(.secondary)
						}
					}
				}
			}
			else {
				Grid(alignment: .trailing, horizontalSpacing: 8) {
					ForEach(1..<7) { _ in
						GridRow {
							Text("")
							Text("")
							Text("")
						}
					}
					
				}
			}
		}
	}
	
	
	@ViewBuilder
	private func getMultipleNights() -> some View {
		ForEach(session.sessionDates.filter{ $0.text != "Any" }, id: \.self.text) { date in
			if let timelineDate = getTimeLineData(for: date) {
				
				HStack {
					Text("Timeline \(date.text)").kerning(1.5).font(.title).foregroundColor(.primary).padding()
					VStack { Divider().backgroundColor(.secondary) }
				}
				.padding()
				//.background(Color.darkBlue).cornerRadius(8)
				
				CapturingTimelineView(data: timelineDate, date: date)
					.frame(height: 360)
					.padding()
					.padding()
				
				
				if session.autoFocuserRuns.filter({ $0.isSession(date)}).isNotEmpty {
				   HStack {
					   Text("AutoFocus Runs \(date.text)").kerning(1.5).font(.title).foregroundColor(.primary).padding()
					   VStack { Divider().backgroundColor(.secondary) }
				   }
				   .padding()
				   
				   ScrollView(.horizontal) {
					   LazyHStack(spacing: 32) {
						   ForEach(session.autoFocuserRuns.filter({ $0.isSession(date) })) { af in
							   VStack {
								   AutoFocusChart(autoFocus: af)
									   .frame(width: 500, height: 360)
									   .compositingGroup()
									   .padding()
									   .background(.thinMaterial)
									   //.background(Color.chartBackground)
									   .cornerRadius(8)
									   
									   .padding(.bottom)
									   .contentMargins(8, for: .scrollContent)
									   .matchedGeometryEffect(id: af.id, in: overviewNamespace)
									   .onTapGesture {
										   withAnimation {
											   self.selectedAutoFocusChart = af
											   self.toolsActive = false
										   }
									   }
								
								   getAutoFocusImages(for: af)
									   .padding(.bottom)
							   }
							   .padding(.horizontal)
						   }
						  .scrollTargetLayout()
					   }
				   }
				   .scrollTargetBehavior(.viewAligned)
				   .padding(.horizontal)
				   
			   }
				
				
			}
		}
		
	}
	
	private func containsAny(_ images: [SessionImage], _ predicate: (SessionImage) -> Float?) -> Bool {
		for image in images {
			if let _ = predicate(image) {
				return true
			}
		}
		return false
	}
	
	@ViewBuilder
	private func sessionCharts(for filteredImages: [SessionImage]) -> some View {
		LazyVGrid(columns: [GridItem(.adaptive(minimum: 600, maximum: 1200))]) {
			ForEach(allcharts.visibleCharts) { chart in
				
				if selectedChart?.id != chart.id {
					
					if let item2 = chart.item2 {
						if containsAny(filteredImages, { $0.getValue(chart.item) }) && containsAny(filteredImages, { $0.getValue(item2) }) {
						   
							MergedDataChart(session: session, item1: chart.item, item2: item2,
											color1: Color.blue, color2: Color.green)
							.frame(height: GraphHeight)
							.compositingGroup()
							.padding()
							.background(.thinMaterial)
							//.background(Color.chartBackground)
							.cornerRadius(8)
							.padding()
							.matchedGeometryEffect(id: chart.id, in: overviewNamespace)
							.transition(.slide)
							.onTapGesture {
								withAnimation {
									self.selectedChart = chart
									self.toolsActive = false
								}
							}
						}
						
					}
					else if containsAny(filteredImages, {  $0.getValue(chart.item) }) {
						DataChart(session: session, item: chart.item, area: chart.area)
							.frame(height: GraphHeight)
							.compositingGroup()
							.padding()
							.background(.thinMaterial)
							//.background(Color.chartBackground)
							.cornerRadius(8)
							.padding()
							.matchedGeometryEffect(id: chart.id, in: overviewNamespace)
							.transition(.slide)
							.onTapGesture {
								withAnimation {
									self.selectedChart = chart
									self.toolsActive = false
								}
							}
					}
				}
				else {
					Color.clear
						.frame(height: GraphHeight)
						.padding()
				}
			}
		}
		.padding()
		.animation(Animation.easeInOut, value: allcharts.visibleCharts.count)
		.animation(Animation.easeInOut, value: session.id)
		.id(session.id)
		.onChange(of: self.selectedChart) { _, _ in
			if selectedChart != nil {
				self.showSettings = false
				self.showCheckList = false
				self.toolsActive = false
			} else {
				self.toolsActive = self.selectedAutoFocusChart == nil && self.selectedChart == nil && self.selectedDistributionChart == nil
			}
		}
		
	}
	
	
	var body: some View {
		VStack(spacing: 0) {
			
			ScrollView(.vertical) {
				
				Spacer(minLength: 120)
				
				self.header
				let filteredImages = session.filteredImages
				
				LazyVStack {
					
					if session.guiding.count > 0, filteredImages.allSatisfy({ $0.hasGuidePulseValues() }) {
						
						HStack {
							Text("Guiding Summary").kerning(1.5).font(.title).foregroundColor(.primary).padding()
							VStack { Divider().backgroundColor(.secondary) }
						}
						.padding()
						
						HStack {
							GuidePulsesGraphFor(sessionImages: filteredImages, item: .PulsesRA, height: 1.6*GraphHeight)
								.compositingGroup()
								.padding()
								.background(.thinMaterial)
								.cornerRadius(8)
								.padding()
								
							GuidePulsesGraphFor(sessionImages: filteredImages, item: .PulsesDEC, height: 1.6*GraphHeight)
								.compositingGroup()
								.padding()
								.background(.thinMaterial)
								.cornerRadius(8)
								.padding()
								
						}
						.padding()
						.id(session.selectedSessionDate)
					} else if !filteredImages.allSatisfy({ $0.hasGuidePulseValues() }) {
						
						VStack {
							
							HStack {
								Text("Guiding Summary").kerning(1.5).font(.title).foregroundColor(.primary).padding()
								VStack { Divider().backgroundColor(.secondary) }
							}
							.padding()
							
							HStack {
								Text("Not all images have valid guiding data, unable to generate a guiding summary")
									.foregroundStyle(Color.secondary)
									.padding(.horizontal)
							
								Spacer()
							}
							.padding(.horizontal)
						}
						.padding(.bottom)
						.id(session.selectedSessionDate)
					}
					
					Group {
						if let sessionDate = session.selectedSession, let timelineDate = getTimeLineData(for: sessionDate) {
							getSingleNight(timelineDate, date: sessionDate)
							
							if session.autoFocuserRuns.isNotEmpty, let selectedSession = session.selectedSession {
								HStack {
									Text("AutoFocus Runs").kerning(1.5).font(.title).foregroundColor(.primary).padding()
									VStack { Divider().backgroundColor(.secondary) }
								}
								.padding()
								
								ScrollView(.horizontal) {
									LazyHStack(spacing: 32) {
										ForEach(session.autoFocuserRuns.filter({ $0.isSession(selectedSession) })) { af in
											VStack {
												AutoFocusChart(autoFocus: af)
													.frame(width: 500, height: 360)
													.compositingGroup()
													.padding()
													.background(.thinMaterial)
													.cornerRadius(8)
													.padding(.bottom)
													.contentMargins(8, for: .scrollContent)
													.matchedGeometryEffect(id: af.id, in: overviewNamespace)
													.onTapGesture {
														withAnimation {
															self.selectedAutoFocusChart = af
															self.toolsActive = false
														}
													}
												
												getAutoFocusImages(for: af)
													.padding(.bottom)
											}
											.padding(.horizontal)
										}
										.scrollTargetLayout()
									}
								}
								.scrollTargetBehavior(.viewAligned)
								.padding(.horizontal)
								
							}
							
							
						} else {
							getMultipleNights()
							
						}
					}
					.id(session.selectedSessionDate)
					
					if session.filteredImages.isNotEmpty, allcharts.visibleCharts.isNotEmpty {
						let filteredImages = session.filteredImages
						
						HStack {
							Text("Image Charts").font(.title).foregroundColor(.primary).padding()
							VStack { Divider().backgroundColor(.secondary) }
						}
						.padding()
						//.background(Color.darkBlue).cornerRadius(8)
						
						sessionCharts(for: filteredImages)
							
					}
					
				}
			}
			
			.overlay {
				VStack {
					ImageOverallStatisticsView(session: session)
						.padding(.horizontal, 32)
						.padding(.top, 32)
						.padding(.bottom, 16)
						.background(.thinMaterial)
						.id(session.id)
					
					Spacer()
				}
			}
		}
		.overlay {
			HStack(alignment: .firstTextBaseline) {
				Spacer()
				if self.showCheckList {
					getChartSelector()
						.padding()
						.background(.ultraThinMaterial, in: RoundedRectangle(cornerRadius: 8, style: .continuous))
						.transition(.move(edge: .trailing))
					
				} else if showSettings {
					AnalysationSettingsView(session: session)
						.transition(.move(edge: .trailing))
					
				}
				
			}
		}
		.overlay {
			
			if let selectedChart = self.selectedChart {
				overlayBody
					.matchedGeometryEffect(id: selectedChart.id, in: overviewNamespace, properties: .position)
			} else if let selectedDistributionChart = self.selectedDistributionChart {
				overlayBody
					.matchedGeometryEffect(id: selectedDistributionChart, in: overviewNamespace, properties: .position)
			} else if let selectedAFChart = self.selectedAutoFocusChart {
				overlayBody
					.matchedGeometryEffect(id: selectedAFChart.id, in: overviewNamespace, properties: .position)
			}
			
			
		}
		.onAppear {
			for sessionDate in session.sessionDates {
				if let timeline = CapturingTimelineData(date: JulianDate.fromDate(date: sessionDate.date), session: session) {
					self.timeLines[sessionDate] = timeline
				}
			}
		}
		.onChange(of: self.selectedDistributionChart) { _, _ in
			if self.selectedDistributionChart != nil {
				self.showSettings = false
				self.showCheckList = false
				self.toolsActive = false
			} else {
				self.toolsActive = self.selectedAutoFocusChart == nil && self.selectedChart == nil && self.selectedDistributionChart == nil
			}
		}
		.onChange(of: self.selectedAutoFocusChart) { _, _ in
			if self.selectedAutoFocusChart != nil {
				self.showSettings = false
				self.showCheckList = false
				self.toolsActive = false
			} else {
				self.toolsActive = self.selectedAutoFocusChart == nil && self.selectedChart == nil && self.selectedDistributionChart == nil
			}
		}
    }
}


