//
//  NINALogViewer.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 19.04.24.
//

import SwiftUI
import PureSwiftUI
import DebouncedOnChange

enum LogSearchMode: Int {
	case filter = 0
	case mark = 1
}

struct LogRange: Identifiable {
	let id = UUID()
	let logId: Set<UUID>
}

@Observable
class LogData {
	var lines: [(Range<Array<NINALogLine>.Index>.Element, NINALogLine)] = []
	var imagingRanges: [LogRange] = []
	var afRanges: [LogRange] = []
	
	var markIds: [UUID] = []
	private var index: Int = 0
	private var hiddenContent: Set<String> = []
	
	func update(session: SessionData, date: Date, text searchText: String, searchMode: LogSearchMode, category: String, logLevel: NINALogType) async {
		
		print("Date: \(date.formatted(date: .abbreviated, time: .omitted))")
		
		let lines = await getLogData(session, date, searchText, searchMode, logLevel, category)
		/*
		let imagingRanges = await getImagingRanges(lines)
		let afRanges = await getAFRanges(lines)
		
		print("TakeExposures \(imagingRanges.count)")
		print("AF Runs \(afRanges.count)")
		*/
		await MainActor.run {
			//self.imagingRanges = imagingRanges
			//self.afRanges = afRanges
			self.lines = lines
		}
	}
	
	
	var hiddenCount: Int {
		return self.hiddenContent.count
	}
	
	func hide(text: String) {
		self.hiddenContent.insert(text)
	}
	
	func clearHidden() {
		self.hiddenContent.removeAll()
	}
	
	private func getAFRanges(_ lines: [(Range<Array<NINALogLine>.Index>.Element, NINALogLine)]) async -> [LogRange] {
		// Starting Category: Focuser, Item: RunAutofocus
		let afStarts = lines.filter { $0.1.text.contains("Starting Category") && $0.1.text.contains("Item: RunAutofocus")  }
			.compactMap({ $0.0})
		
		let afEnds = lines.filter { ($0.1.text.contains("Finishing Category") && $0.1.text.contains("Item: RunAutofocus")) ||
			$0.1.text.contains("Advanced Sequence finished")}
			.compactMap({ $0.0 })
		
		return getRanges(afStarts, afEnds)
	}
		
	private func getImagingRanges(_ lines: [(Range<Array<NINALogLine>.Index>.Element, NINALogLine)]) async -> [LogRange] {
		
		let exposureStarts = lines.filter { $0.1.text.contains("Starting Category") && $0.1.text.contains("Item: TakeExposure")  }
			.compactMap({ $0.0})
		
		let exposureEnds = lines.filter { ($0.1.text.contains("Finishing Category") && $0.1.text.contains("Item: TakeExposure"))
			|| $0.1.text.contains("Interrupting running exposure item")
			|| $0.1.text.contains("Advanced Sequence finished")}
			.compactMap({ $0.0 })
		
		return getRanges(exposureStarts, exposureEnds)
	}
	
	private func getRanges(_ startIndices: [Int], _ endIndices: [Int]) -> [LogRange] {
		var starts = Queue(startIndices)
		var ends = Queue(endIndices)
		
		var range = [LogRange]()
		
		while !starts.isEmpty && !ends.isEmpty {
			if let startIndex = starts.deque() {
				if let endIndex = ends.deque() {
					var lastIndex = endIndex
					while lastIndex <= startIndex, ends.count > 0 {
						lastIndex = ends.deque()!
					}
					
					var logIds = Set<UUID>()
					
					for log in lines.filter({ (index, _) in index >= startIndex && index <= lastIndex}) {
						logIds.insert(log.1.id)
					}
					if !logIds.isEmpty {
						range.append(LogRange(logId: logIds))
					}
					
				}
			} else {
				break
			}
		}
		
		return range
	}
	
	private func getLogData(_ session: SessionData,
							_ date: Date,
							_ searchText: String,
							_ searchMode: LogSearchMode,
							_ logLevel: NINALogType,
							_ category: String) async -> [(Range<Array<NINALogLine>.Index>.Element, NINALogLine)] {
		
		let begin = date.midDay
		let end = date.midDay.advanced(by: TimeInterval(43200))
		
		let lines = session.ninaLogs.getLogData(between: begin, and: end, level: logLevel)
			.filter({ !self.hiddenContent.contains( $0.text) })
		if lines.count == 0 {
			return []
		}
		
		if !searchText.isEmpty {
			let st = searchText.lowercased()
			let markIds = lines.filter({ $0.text.lowercased().contains(st)}).map({ $0.id }).distinct()
			
			set( markIds )
			
			if searchMode == .filter {
				
				let filteredlines = lines.filter({ $0.text.lowercased().contains(st)})
				return Array(zip(filteredlines.indices, filteredlines))
			}
			
			return Array(zip(lines.indices, lines))
		}
		else if category != "None" {
			
			clearMarks()
			
			let catId = category
			if let cat = Array(session.ninaLogs.categories).first(where: { $0.id == catId }) {
				let c1 = "starting category: " + cat.category.lowercased()
				let c2 = "finishing category: " + cat.category.lowercased()
				let c3 = cat.item.lowercased()
				
				var result = [NINALogLine]()
				var inserting = false
				for line in lines {
					let lower = line.text.lowercased()
					if lower.contains(c1) && lower.contains(c3) {
						result.append(line)
						inserting = true
					} else if inserting {
						result.append(line)
						if lower.contains(c2) {
							inserting = false
							result.append(NINALogLine(id: UUID(),
													  time: line.time, level: .empty, source: "", member: "", line: 0, text: "", timeText: line.timeText, attachedLines: []))
						}
					}
				}
				
				return Array(zip(result.indices, result))
			}
		}
		
		return Array(zip(lines.indices, lines))
	}
	

	
	
	
	private func set(_ marks: [UUID]) {
		
		if marks.count == self.markIds.count {
			let hash = Set<UUID>(self.markIds)
			if marks.allSatisfy({ hash.contains($0)}) {
				return
			}
		}
		
		self.markIds = marks
		self.index = 0
		if self.markIds.count > 0 {
			self.currentMark = self.markIds[self.index]
		} else {
			self.currentMark = nil
		}
	}
	
	private func clearMarks() {
		self.markIds = []
		self.index = 0
	}
	
	var markCount: Int {
		self.markIds.count
	}
	
	var currentMark: UUID?
	
	func gotoNextMark() {
		if self.markIds.isNotEmpty {
			self.index = (self.index + 1) % self.markIds.count
			self.currentMark = self.markIds[self.index]
		}
	}
	
	func gotoPreviousMark() {
		if self.markIds.isNotEmpty {
			self.index = (self.index - 1)
			if self.index < 0 { self.index += self.markIds.count }
			self.currentMark = self.markIds[self.index]
		}
	}
	
	func gotoMark(_ mark: UUID) {
		if self.markIds.isNotEmpty {
			self.index = self.markIds.firstIndex(of: mark) ?? 0
			self.currentMark = self.markIds[self.index]
		}
	}
}


struct NINALogViewer: View {
	@State var session: SessionData
	@State private var logDate: Date? = nil
	@State private var searchText: String = ""
	@AppStorage("LogSearchMode") private var searchMode: LogSearchMode = .filter
	@State private var category: String = "None"
	@State private var time: UUID? = nil
	@State private var expandGoto: UUID? = nil
	@State private var logLines: LogData = LogData()
	@AppStorage("NINALogLevel") var logLevel: NINALogType = NINALogType.info
	
	
	
	func getSessionDates() -> [Date] {
		return session.ninaLogs.dates.sorted(by: >)
	}
	
	var header: some View {
		HStack(alignment: .top) {
			
			Picker("Date", selection: $logDate) {
				ForEach(getSessionDates(), id: \.self) { date in
					Text(date.formatted(date: .abbreviated, time: .omitted)).truncationMode(.middle).tag(date)
				}
			}
			.frame(minWidth: 160, maxWidth: 180)
			.padding()
			
			Picker("Level", selection: $logLevel) {
				ForEach(NINALogType.allCases.filter({ $0 != .empty }), id: \.self) { level in
					Text(level.rawValue).tag(level)
				}
			}
			.frame(minWidth: 160, maxWidth: 180)
			.padding()
			
			Picker("Category", selection: $category) {
				
				Text("None").tag("None")
				ForEach(Array(session.ninaLogs.categories)) { category in
					Text("\(category.category): \(category.item)").tag(category.id)
				}
			}
			.frame(minWidth: 250, maxWidth: 300)
			.padding()
			
			
			if self.logLines.hiddenCount > 0 {
				Button {
					self.logLines.clearHidden()
					
					Task {
						if let date = self.logDate {
							await self.logLines.update(session: self.session, date: date, text: self.searchText,
													   searchMode: self.searchMode,
													   category: self.category,
													   logLevel: self.logLevel)
						}
					}
				} label: {
					HStack {
						Image(sfSymbol: "exclamationmark.circle").imageScale(.large)
						Text("\(self.logLines.hiddenCount) hidden content lines")
						
						
					}
					.foregroundColor(.red)
				}
				.padding()
				.buttonStyle(PlainButtonStyle())
				.contentShape(.rect)
			}

			Spacer()
			
			HStack(alignment: .firstTextBaseline) {
				
				Picker("Mode", selection: $searchMode) {
					Text("Filter").tag(LogSearchMode.filter)
					Text("Mark").tag(LogSearchMode.mark)
				}
				.frame(minWidth: 120, maxWidth: 160)
				.padding(.trailing, 8)
				
			
				VStack(spacing: 4) {
					
					TextField(text: $searchText, prompt: Text("Enter search text here")) {
						Label("Search", sfSymbol: .magnifyingglass)
					}
					.frame(maxWidth: 300)
					.onChange(of: searchText, debounceTime: .seconds(1)) { newValue in
						Task {
							if let date = self.logDate {
								await self.logLines.update(session: self.session, date: date, text: self.searchText,
														   searchMode: self.searchMode,
														   category: self.category,
														   logLevel: self.logLevel)
							}
						}
					}
					
					HStack {
						
						Button {
							self.logLines.gotoPreviousMark()
						} label: {
							Image(systemName: "chevron.backward.circle")
								.resizable()
								.frame(width: 20, height: 20)
								.foregroundColor(.accentColor)
								.padding()
						}
						.buttonStyle(PlainButtonStyle())
						.padding(.trailing)
						.disabledIf(self.logLines.markCount == 0)
						
						Button {
							self.logLines.gotoNextMark()
						} label: {
							Image(systemName: "chevron.forward.circle")
								.resizable()
								.frame(width: 20, height: 20)
								.foregroundColor(.accentColor)
								.padding()
						}
						.buttonStyle(PlainButtonStyle())
						.disabledIf(self.logLines.markCount == 0)
						
						if self.logLines.markCount > 0 {
							Text("\(self.logLines.markCount) occurances found")
								.font(.callout).fontWeight(.light)
								.foregroundColor(.secondary)
						}
					}
				}
			}
		}
		.padding(.horizontal)
		.padding(.top)
		.padding(.bottom, 4)
		.background(Color.controlBackgroundDarkColor)
	}
	
	
	func getLineColor(_ level: NINALogType) -> Color {
		switch level {
			case .debug: return Color.secondary
			case .info:  return Color.primary
			case .warn:  return Color.yellow
			case .error: return Color.error
			case .empty: return Color.clear
		}
	}
	
	
	func getTextLine(text: String) -> some View {
		Group {
			if self.searchText.isNotEmpty, let range = text.lowercased().range(of: self.searchText.lowercased()) {
				Text(String(text[..<range.lowerBound])) +
				Text(String(text[range.lowerBound..<range.upperBound])).foregroundColor(Color.green) +
				Text(String(range.upperBound<text.endIndex ? text[range.upperBound...] : ""))
			} else {
				Text(text)
			}
		}
		.font(.body.monospacedDigit()).fontWeight(.light)
		.lineSpacing(2.0)
	}
	
	func getTimeTexts() -> [(time: String, id: UUID)] {
		
		if searchMode == .mark && searchText.isNotEmpty {
			let st = searchText.lowercased()
			let filteredlines = self.logLines.lines.filter({ $0.1.text.lowercased().contains(st)})
			return filteredlines.map({ (time: $0.1.timeText, id: $0.1.id) })
		} else {
			
			var hash = Set<String>()
			return self.logLines.lines.map({ (time: $0.1.timeText, id: $0.1.id) })
				.filter({ hash.insert($0.time).inserted })
		}
		
	}
	
	func getLineBackgroundColor(index: Int, log: NINALogLine) -> Color {
		if self.searchMode == .mark && self.searchText.isNotEmpty && log.id == self.logLines.currentMark {
			return Color.mint.opacity(0.3)
		}
		return index % 2 == 0 ? Color.blue.opacity(0.1) : Color.clear
	}
	
	func getIcon(log: NINALogLine) -> some View {
		Group {
			if log.level == .warn {
				Image(systemName: "info.circle")
					.foregroundStyle(Color.orange)
			} else if log.level == .error {
				Image(systemName: "exclamationmark.circle")
					.foregroundStyle(Color.red)
			} else if self.logLines.imagingRanges.contains(where: { $0.logId.contains(log.id)}) {
				Image(systemName: "camera.aperture")
					.foregroundColor(Color.green)
			} else if self.logLines.afRanges.contains(where: { $0.logId.contains(log.id)}) {
				Image(systemName: "camera.metering.partial")
					.foregroundColor(Color.teal)
			}
			else if log.level == .info || log.level == .debug || log.level == .empty {
				Image(systemName: "info.circle")
					.foregroundStyle(Color.secondary)
			} else {
				Image(systemName: "circle")
					.foregroundStyle(Color.primary.opacity(0.0))
			}
		}
	}
	
    var body: some View {
		VStack(alignment: .leading, spacing: 0) {
			header
			
			HStack(spacing: 4) {
				GeometryReader { g in
					ScrollViewReader { scr in
						ScrollView(.vertical, showsIndicators: true) {
							LazyVStack(alignment: .leading) {
								ForEach(getTimeTexts(), id: \.self.id) { tt in
									HStack {
										Text(tt.time)
											.font(.body.monospacedDigit()).kerning(1.1)
											.foregroundColor(Color.primary.opacity(0.8))
											.frame(maxWidth: .infinity, alignment: .center)
											.padding(.leading, 8)
									}
									.padding(.vertical, 12)
									.background(tt.id == self.time ? Color.accentColor.opacity(0.5) : .clear)
									.id(tt.id)
									.contentShape(Rectangle())
									.onTapGesture {
										self.time = tt.id
									}
								}
							}
						}
						.onChange(of: self.logLines.currentMark) { _, mark in
							
							if let mark = mark {
								withAnimation(.easeInOut(duration: 1)) {
									scr.scrollTo(mark, anchor: .center)
								}
							}
						}
						
					}
					
				}.frame(width: 80)
				
				ScrollViewReader { scr in
					
					ScrollView(.vertical) {
						
						LazyVStack(alignment: .leading, spacing: 0) {
							ForEach(self.logLines.lines, id: \.self.0) { index, log in
								HStack(alignment: .top) {
									//getIcon(log: log)
									
									Text(log.time.formatted(date: .omitted, time: .standard))
										.font(.body.monospacedDigit()).fontWeight(.light)
										.frame(width: 80, alignment: .leading)
									
									Group {
										Text(log.level.rawValue)
											.font(.body.monospacedDigit())
											.fontWeight(.thin)
											.frame(width: 60, alignment: .leading)
											
										
										if log.attachedLines.isNotEmpty {
											VStack(alignment: .leading, spacing: 4) {
												getTextLine(text: log.text)
												
												ForEach(log.attachedLines, id: \.self) { txt in
													Text(txt)
														.font(.body.monospacedDigit())
														.fontWeight(.light)
												}
											}
										} else {
											getTextLine(text: log.text)
										}
									}
									.foregroundColor(getLineColor(log.level))
									.contextMenu {
										Button {
											self.logLines.hide(text: log.text)
											Task {
												if let date = self.logDate {
													await self.logLines.update(session: self.session, date: date, text: self.searchText,
																			   searchMode: self.searchMode,
																			   category: self.category,
																			   logLevel: self.logLevel)
												}
											}
											
										} label: {
											Text("Hide this content")
										}

										if searchMode == .filter && searchText.isNotEmpty {
											Button {
												self.searchMode = .mark
												self.expandGoto = log.id
											} label: {
												Text("Expand/Goto")
											}
										}
										
									}
									
								}
								.padding(.vertical, 4)
								.padding(.horizontal, 8)
								.frame(maxWidth: .infinity, alignment: .leading)
								.background(getLineBackgroundColor(index: index, log: log))
								.id(log.id)
							}
						}
					}
					.onChange(of: self.time) { _, _ in
						withAnimation(.easeInOut(duration: 1)) {
							scr.scrollTo(self.time, anchor: .center)
						}
					}
					.onChange(of: self.logLines.currentMark) { _, _ in
						if let mark = self.logLines.currentMark {
							
							withAnimation(.easeInOut(duration: 1)) {
								scr.scrollTo(mark, anchor: .center)
							}
						}
					}
					.onChange(of: self.session.selectedSessionDate, { _, _ in
						
						Task {
							if let date = self.logDate {
								await self.logLines.update(session: self.session, date: date, text: self.searchText,
														   searchMode: self.searchMode,
														   category: self.category,
														   logLevel: self.logLevel)
							}
							
							if let mark = self.logLines.currentMark {
																
								withAnimation(.easeInOut(duration: 1)) {
									scr.scrollTo(mark, anchor: .center)
								}
							}
						}
					})
					.onChange(of: self.searchMode, { _, _ in
						Task {
							if let date = self.logDate {
								await self.logLines.update(session: self.session, date: date, text: self.searchText,
														   searchMode: self.searchMode,
														   category: self.category,
														   logLevel: self.logLevel)
							}
							
							if let expandGoto = self.expandGoto {
								self.logLines.gotoMark(expandGoto)
								self.expandGoto = nil
							}
						}
						
					})
					.onChange(of: self.category, { _, _ in
						Task {
							if let date = self.logDate {
								await self.logLines.update(session: self.session, date: date, text: self.searchText,
														   searchMode: self.searchMode,
														   category: self.category,
														   logLevel: self.logLevel)
							}
						}
					})
					.onChange(of: self.logLevel, { _, _ in
						Task {
							if let date = self.logDate {
								await self.logLines.update(session: self.session, date: date, text: self.searchText,
														   searchMode: self.searchMode,
														   category: self.category,
														   logLevel: self.logLevel)
							}
						}
						
					})
					.onChange(of: self.logDate) { _, _ in
						Task {
							if let date = self.logDate {
								await self.logLines.update(session: self.session, date: date, text: self.searchText,
														   searchMode: self.searchMode,
														   category: self.category,
														   logLevel: self.logLevel)
							}
						}
					}
					.onAppear {
						
						if let date = getSessionDates().first {
							self.logDate = date
							
							Task {
								if let date = self.logDate {
									await self.logLines.update(session: self.session, date: date, text: self.searchText,
															   searchMode: self.searchMode,
															   category: self.category,
															   logLevel: self.logLevel)
								}
								
								if let mark = self.logLines.currentMark {
									
									withAnimation(.easeInOut(duration: 1)) {
										scr.scrollTo(mark, anchor: .center)
									}
								}
							}
						}
					}
				}
				
			}
			
			
		}
    }
}

