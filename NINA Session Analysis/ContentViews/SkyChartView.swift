//
//  SkyChartView.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 31.03.24.
//

import SwiftUI
import CoreLocation

public extension Binding {

	static func convert<TInt, TFloat>(from intBinding: Binding<TInt>) -> Binding<TFloat>
	where TInt:   BinaryInteger,
		  TFloat: BinaryFloatingPoint{

		Binding<TFloat> (
			get: { TFloat(intBinding.wrappedValue) },
			set: { intBinding.wrappedValue = TInt($0) }
		)
	}

	static func convert<TFloat, TInt>(from floatBinding: Binding<TFloat>) -> Binding<TInt>
	where TFloat: BinaryFloatingPoint,
		  TInt:   BinaryInteger {

		Binding<TInt> (
			get: { TInt(floatBinding.wrappedValue) },
			set: { floatBinding.wrappedValue = TFloat($0) }
		)
	}
}

struct CameraShape: Shape {
	let projection: SkySphereViewProjection
	let position: Vector3
	func path(in rect: CGRect) -> Path {
		if let pt = projection.project(pos: position) {
			return Path(ellipseIn: CGRect.from(center: pt, size: CGSize.square(10)))
			
		}
		return Path()
	}
}



struct SkyChartView: View, SkySphereViewContent {
	@State var session: SessionData
	@StateObject var skyViewSource = SkySphereViewDataSource()
	@State private var imageIndex: Int = 0
	@State private var selectedImage: SessionImage? = nil
	@State var playing: Bool = false
	@State var showLargeSky: Bool = false
	@State private var model = RiseSetSunMoonModel()
	@State private var moonphaseModel = MoonphaseViewModel()
	@Namespace var skyChartNamespace
	
	@AppStorage("SkyChartViewTab") var selectedTabChart: String = "HFR/Eccentricity"
	let timer = Timer.publish(every: 0.5, on: .main, in: .common).autoconnect()
	

	func drawContent(_ projection: SkySphereViewProjection) -> AnyView {
		if let image = self.selectedImage, let ra = image.ra, let dec = image.dec {
			
			let pos = Vector3.from(ra: Double(ra) * Constants.Rad, dec: Double(dec) * Constants.Rad)
			
			return CameraShape(projection: projection, position: pos)
					.stroke(Color.yellow)
					.typeErased()
		}
		return EmptyView().typeErased()
	}
	
	func getSelectedImage() -> SessionImage? {
		let images = self.session.filteredImages
		if imageIndex >= 0 && imageIndex < images.count {
			return images[imageIndex]
		}
		
		return nil
	}
	
	private func getSunAltitudeColor(_ alt: Float) -> Color {
		if alt < -18.0 {
			return Color.green
		} else if alt < -12.0 {
			return Color.yellow
		} else {
			return Color.red
		}
	}
	
	private func getMoonAltitudeColor(_ alt: Float) -> Color {
		if alt < 0.0 {
			return Color.green
		} else if alt < 10.0 {
			return Color.yellow
		} else {
			return Color.red
		}
	}
	
	@ViewBuilder
	func altAzViews(_ image: SessionImage) -> some View {
		Grid(alignment: .leading, horizontalSpacing: 16, verticalSpacing: 4) {
			if image.moonAz != nil {
				GridRow {
					Text("Moon Azimuth:")
						.gridColumnAlignment(.leading)
					Text("\(image.MoonAzimuth)")
						.gridColumnAlignment(.trailing)
				}
				Divider()
			}
			if image.moonAlt != nil {
				GridRow {
					Text("Moon Altitude:")
						.foregroundColor(getMoonAltitudeColor(image.moonAlt!))
						.gridColumnAlignment(.leading)
					Text("\(image.MoonAltitude)")
						.foregroundColor(getMoonAltitudeColor(image.moonAlt!))
						.gridColumnAlignment(.trailing)
				}
				Divider()
			}
			
			if image.moonAngle != nil {
				GridRow {
					Text("Angular Separation to Moon:")
						.foregroundColor(image.moonAngle! < 30.0 ? .red : .green)
						.gridColumnAlignment(.leading)
					Text("\(image.MoonAngle)")
						.foregroundColor(image.moonAngle! < 30.0 ? .red : .green)
						.gridColumnAlignment(.trailing)
				}
				Divider()
			}
			
			if image.sunAz != nil {
				GridRow {
					Text("Sun Azimuth:")
						.gridColumnAlignment(.leading)
					Text("\(image.SunAzimuth)")
						.gridColumnAlignment(.trailing)
				}
				Divider()
			}
			
			if image.sunAlt != nil {
				GridRow {
					Text("Sun Altitude:")
						.foregroundColor(getSunAltitudeColor(image.sunAlt!))
						.gridColumnAlignment(.leading)
					Text("\(image.SunAltitude)")
						.foregroundColor(getSunAltitudeColor(image.sunAlt!))
						.gridColumnAlignment(.trailing)
				}
			}
		}
		.padding()
		.frame(maxWidth: .infinity)
		
	}
	
	@ViewBuilder
	var dataCharts: some View {
		VStack {
			HStack(alignment: .top) {
				DataChartWithSelection(session: session, item: .constant(.HFR), image: $selectedImage,
									   area: true, imageTitle: false, showDots: false)
				.padding(.horizontal, 8)
				.padding(.top)
				.id(session.id)
				
				Color.clear
				Color.clear
				
				DataChartWithSelection(session: session, item: .constant(.ECCENTRICITY), image: $selectedImage,
									   area: true, imageTitle: false, showDots: false)
				.padding(.horizontal, 8)
				.padding(.top)
				.id(session.id)
			}
			
			
			Color.clear
			
			HStack(alignment: .top) {
				DataChartWithSelection(session: session, item: .constant(.MEAN), image: $selectedImage,
									   area: true, imageTitle: false, showDots: false)
				.padding(.horizontal, 8)
				.padding(.bottom)
				.id(session.id)
				
				Color.clear
				Color.clear
				
				DataChartWithSelection(session: session, item: .constant(.NUMSTARS), image: $selectedImage,
									   area: true, imageTitle: false, showDots: false)
				.padding(.horizontal, 8)
				.padding(.bottom)
				.id(session.id)
			}
			
		}
	}
	
	@ViewBuilder
	func moonSunInfo(_ image: SessionImage) -> some View {
		VStack {
			MoonPhaseView2(model: self.$moonphaseModel)
				.aspectRatio(1.0, contentMode: .fit)
				.frame(maxHeight: 300)
				.padding(8)
			
			
			RiseSetSunMoonView2(model: self.$model)
				.padding()
			
			
			Spacer()
			altAzViews(image)
			
		}
		.frame(maxWidth: 340)
		.padding(.horizontal, 8)
		.padding(.vertical, 16)
		.background(Color.blue.opacity(0.025))
		.cornerRadius(8)
		.padding(.trailing, 4)
		.id(image.id)
	}
	
	
	var body: some View {
		
		VStack {
			
			ScrollView(.vertical) {
				HStack {
					
					if !self.showLargeSky {
						
						ZStack {
							
							SkySphereView(source: skyViewSource, content: self)
								.background(Color.black)
								.frame(minWidth: 600)
								.matchedGeometryEffect(id: "SkyChart", in: skyChartNamespace)
								.padding()
								.onTapGesture {
									self.showLargeSky = true
								}
							
							dataCharts
							
							VStack {
								
								HStack {
									Spacer()
									if let image = selectedImage {
										Text(image.FileName)
											.font(.callout)
											.lineLimit(1)
											.truncationMode(.middle)
											.foregroundColor(image.bad ? .red : .primary)
											.padding(.horizontal)
											.padding(.vertical, 2)
											.background(.ultraThinMaterial).cornerRadius(8)
									} else {
										Text("")
									}
									Spacer()
								}
								
								Spacer()
								
							}
							
						}
					} else {
						Color.clear
					}
					
					
					if let image = selectedImage {
						moonSunInfo(image)
					}
					
				}
			}
			
			HStack {
				
				Button(action: {
					playing.toggle()
				}, label: {
					Image(systemName: playing ? "pause.fill" : "play.fill")
						.resizable()
						.frame(width: 20, height: 20)
				})
				.buttonStyle(BorderlessButtonStyle())
				.padding()
				
				Slider(value: .convert(from: $imageIndex), in: 0...Float(session.filteredImages.count-1))
					.padding()
			}
			.background(Color.black.opacity(0.1))
			
			GuideGraphView(session: session, selectedimage: selectedImage)
				.frame(height: 260)
				
		}
		.background(Color.controlBackgroundDarkColor)
		.onAppear(perform: {
			self.selectedImage = getSelectedImage()
			if let image = self.selectedImage, let startExposure = image.startExposure {
				
				let jdd = JulianDate.fromDate(date: startExposure)
				if let lat = image.obsLat, let long = image.obsLong {
					let loc = CLLocation(latitude: lat, longitude: long)
					self.skyViewSource.setup(location: loc, jdd: jdd, maxMagnitude: 6.5)
					
					Task {
						await moonphaseModel.update(jd: jdd)
						await model.update(jd: jdd, location: loc)
					}
				}
			}
			
		})
		.onChange(of: self.session.selectedSessionDate) {
			self.imageIndex = 0
		}
		.onChange(of: self.session.selectedFilter) {
			self.imageIndex = 0
		}
		.onChange(of: self.session.selectedPierSide) {
			self.imageIndex = 0
		}
		.onChange(of: self.session.selectedTarget) {
			self.imageIndex = 0
		}
		.onChange(of: self.session.selectedExposure) {
			self.imageIndex = 0
		}
		.onChange(of: self.imageIndex) { _, _ in
			self.selectedImage = self.getSelectedImage()
			
			if let image = self.selectedImage, let startExposure = image.startExposure {
				
				let jdd = JulianDate.fromDate(date: startExposure)
				self.skyViewSource.set(julianDate: jdd)
				
				if let lat = image.obsLat, let long = image.obsLong {
					let loc = CLLocation(latitude: lat, longitude: long)
					Task {
						await moonphaseModel.update(jd: jdd)
						await model.update(jd: jdd, location: loc)
					}
				}
			}
		}
		.onReceive(timer, perform: { _ in
			if playing {
				let nextIndex = (self.imageIndex + 1) % session.filteredImages.count
				self.imageIndex = nextIndex
				self.selectedImage = getSelectedImage()
				if let image = self.selectedImage, let startExposure = image.startExposure {
					let jdd = JulianDate.fromDate(date: startExposure)
					self.skyViewSource.set(julianDate: jdd)
					
					
					if let lat = image.obsLat, let long = image.obsLong {
						let loc = CLLocation(latitude: lat, longitude: long)
						Task {
							await moonphaseModel.update(jd: jdd)
							await model.update(jd: jdd, location: loc)
						}
					}
				}
			}
		})
		.overlay {
			if self.showLargeSky {
				ZStack {
					
					SkySphereView(source: skyViewSource, content: self)
						.background(Color.black)
						.padding()
						.matchedGeometryEffect(id: "SkyChart", in: skyChartNamespace, properties: .position)
					
					VStack {
						HStack {
							Spacer()
							Button {
								withAnimation {
									self.showLargeSky = false
								}
								
							} label: {
								Image(systemName: "xmark.circle")
									.resizable()
									.imageScale(.large)
									.frame(width: 32, height: 32)
									.foregroundColor(.primary)
									.padding()
							}
							.buttonStyle(PlainButtonStyle())
						}
						Spacer()
					}
						
				}
				.background {
					Color.controlBackgroundDarkColor
				}
				
			}
			
		}
		
	}
}

