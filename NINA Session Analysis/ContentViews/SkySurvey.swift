//
//  SkySurvey.swift
//  SkySurvey
//
//  Created by Jürgen Terpe on 11.09.21.
//

import Foundation
import CoreGraphics
import CoreImage


public enum SkySurvaySource: String, Equatable {
    case nasa = "https://skyview.gsfc.nasa.gov/current/cgi/runquery.pl?Survey=dss2r&Position={RA},{DEC}&Size={FOV}&Pixels={WH}&Return=JPG"
    case eso = "https://archive.eso.org/dss/dss/image?ra={RA}&dec={DEC}&x={W}&y={H}&mime-type=download-gif&Sky-Survey=DSS2&equinox=J2000&statsmode=VO"
    case hips2Fits = "https://alasky.u-strasbg.fr/hips-image-services/hips2fits?projection=STG&hips=CDS%2FP%2FDSS2%2Fcolor&width={W}&height={H}&fov={FOV}&ra={RA}&dec={DEC}&format=jpg"
    case skyAtlas = "https://skyserver.sdss.org/dr14/SkyserverWS/ImgCutout/getjpeg?ra={RA}&dec={DEC}&width={W}&height={H}&scale={FOV}"
    case stsci = "https://archive.stsci.edu/cgi-bin/dss_search?format=GIF&r={RA}&d={DEC}&e=J2000&w={W}&h={H}&v=1"
    
    public func get(ra: Double, dec: Double, fov: Double, size: CGSize) -> URL? {
        
        
        switch self {
        case .nasa:
            let urlString = self.rawValue.replacingOccurrences(of: "{RA}", with: String(format: "%.6f", ra))
                .replacingOccurrences(of: "{DEC}", with: String(format: "%.6f", dec))
                .replacingOccurrences(of: "{FOV}", with: String(format: "%.5f", fov))
                .replacingOccurrences(of: "{WH}", with: String(format: "%d", max(Int(size.width), Int(size.height))))
            
            return URL(string: urlString)
        case .eso:
            let urlString = self.rawValue.replacingOccurrences(of: "{RA}", with: String(format: "%.6f", ra))
                .replacingOccurrences(of: "{DEC}", with: String(format: "%.6f", dec))
                .replacingOccurrences(of: "{FOV}", with: String(format: "%.5f", fov))
                .replacingOccurrences(of: "{W}", with: String(format: "%d", Int(size.width)))
                .replacingOccurrences(of: "{H}", with: String(format: "%d", Int(size.height)))
                
            return URL(string: urlString)
        case .hips2Fits:
            let urlString = self.rawValue.replacingOccurrences(of: "{RA}", with: String(format: "%.6f", ra))
                .replacingOccurrences(of: "{DEC}", with: String(format: "%.6f", dec))
                .replacingOccurrences(of: "{FOV}", with: String(format: "%.5f", fov))
                .replacingOccurrences(of: "{W}", with: String(format: "%d", Int(size.width)))
                .replacingOccurrences(of: "{H}", with: String(format: "%d", Int(size.height)))
            
            return URL(string: urlString)
            
        case .skyAtlas:
            let urlString = self.rawValue.replacingOccurrences(of: "{RA}", with: String(format: "%.6f", ra))
                .replacingOccurrences(of: "{DEC}", with: String(format: "%.6f", dec))
                .replacingOccurrences(of: "{FOV}", with: String(format: "%.5f", fov))
                .replacingOccurrences(of: "{W}", with: String(format: "%d", Int(size.width)))
                .replacingOccurrences(of: "{H}", with: String(format: "%d", Int(size.height)))
                .replacingOccurrences(of: "{FOV}", with: String(format: "%.5f", fov))
               
            return URL(string: urlString)
        case .stsci:
            let urlString = self.rawValue.replacingOccurrences(of: "{RA}", with: String(format: "%.6f", ra))
                .replacingOccurrences(of: "{DEC}", with: String(format: "%.6f", dec))
                .replacingOccurrences(of: "{FOV}", with: String(format: "%.5f", fov))
                .replacingOccurrences(of: "{W}", with: String(format: "%d", Int(size.width)))
                .replacingOccurrences(of: "{H}", with: String(format: "%d", Int(size.height)))
               
            return URL(string: urlString)
            
        }
        
    }
    
}


public class SkySurveyImageDownloader: ObservableObject {
    
    @Published var surveyImage: CGImage? = nil
    @Published var source: SkySurvaySource = .eso
    
    public func clear() {
        self.surveyImage = nil
    }
    
    public func request(survey: SkySurvaySource, ra: Double, dec: Double, fov: Double, size: CGSize) {
        
        if surveyImage != nil && source == survey {
            return
        }
        
		let imageSize = CGSize(width: 0.5 * size.width, height: 0.5 * size.height)
		
        if let url = survey.get(ra: ra, dec: dec, fov: fov, size: imageSize) {
            
            Task {
				
				if let cachedImage = await getFromCache(ra: ra, dec: dec, fow: fov, size: imageSize) {
					await MainActor.run {
						self.surveyImage = cachedImage
						self.source = survey
					}
				} 
				else if let image = try? await self.download(url: url, with: imageSize) {
					await writeToCache(ra: ra, dec: dec, fov: fov, size: imageSize, image: image)
					
					await MainActor.run {
						self.surveyImage = image
						self.source = survey
					}
                }
            }
        }
    }
	
	private func writeToCache(ra: Double, dec: Double, fov: Double, size: CGSize, image: CGImage) async {
		let cachesDirectory = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
		let raDeg = Int(ra * 150000.0)
		let decDeg = Int(dec * 10000.0)
		let w = Int(size.width)
		let h = Int(size.height)
		let filename = "SkyRegion_\(raDeg)_\(decDeg)_\(w)_\(h).png"
		let url = cachesDirectory.appendingPathComponent(filename)
		
		do {
			let cicontext = CIContext()
			let ciimage = CIImage(cgImage: image)
			try cicontext.writePNGRepresentation(of: ciimage, to: url, format: .RGBA8, colorSpace: ciimage.colorSpace!)
			print("write image to cache... \(url.path())")
		}
		catch(let error) {
			print(error.localizedDescription)
		}
	
	}
	
	private func getFromCache(ra: Double, dec: Double, fow: Double, size: CGSize) async -> CGImage? {
		let cachesDirectory = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
		let raDeg = Int(ra * 150000.0)
		let decDeg = Int(dec * 10000.0)
		let w = Int(size.width)
		let h = Int(size.height)
		let filename = "SkyRegion_\(raDeg)_\(decDeg)_\(w)_\(h).png"
		let path = cachesDirectory.appendingPathComponent(filename)
		
		print("read image from cache... \(path.path())")
		if FileManager.default.fileExists(atPath: path.path()) {
			if let dataProvider = CGDataProvider(filename: path.path()) {
				return CGImage(pngDataProviderSource: dataProvider, decode: nil, shouldInterpolate: false, intent: CGColorRenderingIntent.defaultIntent)
			}
		}
		
		return nil
	}
	
    
    private func download(url: URL, with size: CGSize) async throws -> CGImage? {
        let (localURL, _) = try await URLSession.shared.download(from: url)
        guard let data = try? Data(contentsOf: localURL, options: .mappedIfSafe) else { return nil }
        if let source = CGImageSourceCreateWithData(data as CFData, nil) {
			if let cgImage = CGImageSourceCreateImageAtIndex(source, 0, nil) {
				return cgImage
			}
        }
        return nil
    }
    
}
