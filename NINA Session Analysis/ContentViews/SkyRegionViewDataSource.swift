//
//  SkyRegionViewDataSource.swift
//  SkyToday
//
//  Created by Jürgen Terpe on 19.12.21.
//

import Foundation

import Foundation
import SwiftUI
import CoreLocation

struct ConstellationStarLine {
	var from: Vector3
	var to: Vector3
}

struct StarMag: Identifiable {
	let id: Int32
	let pos: Vector3
	let mag: Float
	let name: String?
}



final public class SkyRegionViewDataSource: ObservableObject {
    private var jdd: JulianDate
    private var updating: Bool = false
    private var needsUpdate: Bool = true
    private var location: CLLocation

    @Published var centerPosition: Vector3
    @Published var viewRadius: Double = 1.0
    @Published var scale: Double = 1.0
    @Published var starCount: Int = 0
    @Published var stars: [StarMag] = []
    @Published var lines: [ConstellationStarLine] = []
    @Published var center: Vector3 = Vector3.zero()
    @Published var angle: Double = 0.0
  
    
    public init() {
		self.centerPosition = Vector3.zAxis()
		self.viewRadius = 30.0
        self.jdd = JulianDate.now()
		self.location = CLLocation(latitude: 50, longitude: 12.1)
    }

    public func set(jd: JulianDate) {
        self.jdd = jd
        self.needsUpdate = true
    }
    
    public func set(center: Vector3) {
        self.centerPosition = center
    }
    
    public func set(radius: Double) {
        self.viewRadius = radius
    }    
    
    public var julianDate: JulianDate {
        return self.jdd
    }
    
    public func update() {
        
        if (self.stars.count == 0 || self.lines.count == 0 || self.needsUpdate) && !updating {
            
            updating = true
            
            DispatchQueue.global(qos: .userInitiated).async {
                let (foundLines, foundStars, center, angle) = self.getStarsAndLines()
                             
                DispatchQueue.main.async {
                    
                    self.center = center
                    self.angle = angle
                    self.updating = false
                    self.needsUpdate = false
                    self.starCount = foundStars.count
                    self.stars = foundStars
                    self.lines = foundLines
                  
                }
            }
        }
    }
    
    private func getStarsAndLines() -> (lines: [ConstellationStarLine], stars: [StarMag], center: Vector3, angle: Double) {
        
        var clines = [ConstellationStarLine]()
        var stars = [StarMag]()
        
        let centerStars = Vector3(polar: self.centerPosition.toPolar())
        let radius = self.viewRadius * 1.2 * Constants.Rad
        let t = self.jdd.t
        
        for star in StarDatabase.shared.getVisibleStars(maxMagnitude: 6.0) {
            if let mag = star.getMagnitude() {
                let pos = star.getPosition(t)

                let name = star.getName()
                stars.append(StarMag(id: star.id,
                                                  pos: pos,
                                                  mag: mag,
                                                  name: name))
                
            }
        }
        
        let lines = StarDatabase.shared.getConstellationLines()
        for key in lines.keys {
            if let const = lines[key] {
                for line in const.line {
                     if let starData1 = StarDatabase.shared.getStarData(by: line.from),
                       let starData2 = StarDatabase.shared.getStarData(by: line.to) {
                        
                        let pos1 = starData1.getPosition(t)
                        let pos2 = starData2.getPosition(t)
                        
                        clines.append(ConstellationStarLine(from: pos1, to: pos2))
                    }
                }
            }
        }
        
        return (lines: clines, stars: stars, center: centerStars, angle: radius)
    }
}


