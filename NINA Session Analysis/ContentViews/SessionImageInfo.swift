//
//  SessionImageInfo.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 01.04.24.
//

import SwiftUI
import PureSwiftUI

struct NameValue: Identifiable {
	
	var id: String {
		name
	}
	
	var name: String
	var value: String
}

struct SessionImageInfo: View {
	var image: SessionImage
	
	var nameValues: [NameValue] {
		return [
			NameValue(name: "Index", value: image.Index),
			NameValue(name: "FileName", value: image.FileName),
			NameValue(name: "Filter", value: image.filter),
			NameValue(name: "Exposure", value: image.ExposureTime),
			NameValue(name: "PierSide", value: image.pierSide),
			NameValue(name: "Azimuth", value: image.Azimuth),
			NameValue(name: "Altitude", value: image.Altitude),
			NameValue(name: "Airmass", value: image.Airmass),
			NameValue(name: "HFR", value: image.HFR),
			NameValue(name: "FWHM", value: image.FWHM),
			NameValue(name: "Eccentricity", value: image.Eccentricity),
			NameValue(name: "Mean", value: image.Mean),
			NameValue(name: "#Stars", value: image.NumStars),
			NameValue(name: "RMS", value: image.RMS),
			NameValue(name: "Sensor-Temperature", value: image.CameraTemperature),
			NameValue(name: "Dither(px)", value: image.DriftPixel),
			NameValue(name: "Moon Altitude", value: image.MoonAltitude),
			NameValue(name: "Sun Altitude", value: image.SunAltitude),
			NameValue(name: "Cloud Coverage", value: image.CloudCover)
		]
	}
	
    var body: some View {
		
		ScrollView(.vertical) {
			
			VStack(alignment: .leading, spacing: 0) {
				ForEach(nameValues) { nv in
					HStack(spacing: 0) {
						Text("\(nv.name) ")
							.frame(width: 160, alignment: .trailing)
							.borderColor(Color.gray.opacity(0.15), width: 0.5)
						Text(" \(nv.value) ")
							.lineLimit(1)
							.truncationMode(.middle)
							.frame(width: 500, alignment: .trailing).borderColor(Color.gray.opacity(0.15), width: 0.5)
					}
				}
			}
		}
		
    }
}


