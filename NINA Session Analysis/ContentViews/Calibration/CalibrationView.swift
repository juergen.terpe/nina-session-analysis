//
//  CalibrationView.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 03.04.24.
//

import SwiftUI
import PureSwiftUI


struct CalibrationView: View {
	var calibration: CalibrationData
	@Environment(\.colorScheme) var colorScheme
	
    var body: some View {
		GeometryReader { geometry in
			
			ZStack(alignment: .center) {
				
				PolarGrid(rCount: 4, thetaCount: 5)
					.stroke(Color.gray.opacity(0.5), lineWidth: 0.5)
					.frame(geometry.size)
					.backgroundColor(colorScheme == .dark ? Color.black : Color.controlBackgroundColor)
					.clipCircle()
				
				PolarAxis()
					.stroke(Color.gray, lineWidth: 0.5)
					.frame(geometry.size)
					.clipCircle()
				
				if let angle = calibration.westAngle {
					CalibrationAngleWest(angle: angle)
						.strokeColor(Color.blue, lineWidth: 2.0)
				}
				
				
				if let angle2 = calibration.northAngle {
					CalibrationAngleNorth(angle: angle2)
						.strokeColor(Color.red, lineWidth: 2.0)
				}
				
				if let distance = calibration.calibrationDistance {
					CalibrationFrames(distance: distance, frames: calibration.westFrames)
						.strokeColor(Color.blue, lineWidth: 0.5)
					
					CalibrationFrames(distance: distance, frames: calibration.northFrames)
						.strokeColor(Color.red, lineWidth: 0.5)
				}
				
				VStack {
					HStack(alignment: .firstTextBaseline) {
						VStack(alignment: .leading) {
							
							if let angle = calibration.northAngle, let rate = calibration.northRate {
								Text("North Angle: \(angle, specifier: "%.1f")°")
								Text("North Rate: \(rate, specifier:  "%.1f")px/s")
								Text("#Frames: \(calibration.northFrames.count)")
							} else {
								Text("Failed: no North Angle")
							}
						}
						.foregroundColor(Color.red)
					
						Spacer()
						
						if let na = calibration.northAngle, let wa = calibration.westAngle {
							let nv = CGVector(dx: cos(Double(na) * Constants.Rad), dy: sin(Double(na) * Constants.Rad))
							let wv = CGVector(dx: cos(Double(wa) * Constants.Rad), dy: sin(Double(wa) * Constants.Rad))
							let angle = abs(Double(CGVector.angleBetween(wv, nv))) * Constants.Deg
							
							Text("Orthogonal: \(angle, specifier: "%.1f")°")
								.font(.headline)
								.foregroundColor(angle > 75.0 ? Color.primary : Color.red)
						}
					}
					
					
					Spacer()
					
					HStack {
						Spacer()
						
						VStack(alignment: .trailing) {
							if let angle = calibration.westAngle, let rate = calibration.westRate {
								Text("#Frames: \(calibration.westFrames.count)")
								Text("West Rate: \(rate, specifier:  "%.1f")px/s")
								Text("West Angle: \(angle, specifier: "%.1f")°")
							} else {
								Text("Failed: no West Angle")
							}
						}
					}
					.foregroundColor(Color.blue)
					
				}
				.drawingGroup(opaque: true)
			}
		}
		
		
    }
}

