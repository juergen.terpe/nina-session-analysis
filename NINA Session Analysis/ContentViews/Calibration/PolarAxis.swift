//
//  PolarAxis.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 03.04.24.
//

import SwiftUI

public struct PolarAxis: Shape {
	
	public func path(in rect: CGRect) -> Path {
		let w = rect.width
		let h = rect.height
		let maxRadius =  w > h ? w/2.0 : h/2.0
		
		return Path { path in
			
			path.move(to: CGPoint(x: rect.midX, y: rect.midY - maxRadius))
			path.addLine(to: CGPoint(x: rect.midX, y: rect.midY + maxRadius))
			path.move(to: CGPoint(x: rect.midX - maxRadius, y: rect.midY))
			path.addLine(to: CGPoint(x: rect.midX + maxRadius, y: rect.midY))
		}
	}
}

