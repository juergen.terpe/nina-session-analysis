//
//  CalibrationAngles.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 03.04.24.
//

import SwiftUI

struct CalibrationAngleWest: Shape {
	public var angle: Float
	
	public func path(in rect: CGRect) -> Path {
		let w = rect.width
		let h = rect.height
		let maxRadius =  w > h ? w/2.0 : h/2.0
		
		let center = rect.getCenter()
				
		return Path { path in
			
			path.move(to: center)
			let x = center.x + maxRadius * CGFloat(cos(Double(360.0 - angle) * Constants.Rad))
			let y = center.y + maxRadius * CGFloat(sin(Double(360.0 - angle) * Constants.Rad))
			path.addLine(to: CGPoint(x: x, y: y))
		}
	}
}

struct CalibrationAngleNorth: Shape {
	public var angle: Float
	
	public func path(in rect: CGRect) -> Path {
		let w = rect.width
		let h = rect.height
		let maxRadius =  w > h ? w/2.0 : h/2.0
		
		let center = rect.getCenter()
				
		return Path { path in
			
			path.move(to: center)
			let x = center.x + maxRadius * CGFloat(cos(Double(angle + 180.0) * Constants.Rad))
			let y = center.y - maxRadius * CGFloat(sin(Double(angle + 180.0) * Constants.Rad))
			path.addLine(to: CGPoint(x: x, y: y))
		}
	}
}





