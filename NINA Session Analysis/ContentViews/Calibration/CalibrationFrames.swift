//
//  CalibrationFrames.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 03.04.24.
//

import SwiftUI

struct CalibrationFrames: Shape {
	var distance: Float
	var frames: [CalibrationFrame]
	
	func getPoints(rect: CGRect) -> [CGPoint] {
		let factor = min(rect.width, rect.height)/2.0 / CGFloat(distance)
		var points = [CGPoint]()
		
		for frame in frames {
			points.append(CGPoint(x: rect.midX + CGFloat(frame.dx) * factor, y: rect.midY - CGFloat(frame.dy) * factor))
		}
		return points
	}
	
	func path(in rect: CGRect) -> Path {
		
		let points = getPoints(rect: rect)
		
		return Path { path in
			
			for pt in points {
				path.addEllipse(in: CGRect.from(center: pt, radius: 5.0))
			}
			
			if let last = points.last {
				path.move(rect.center)
				path.addLine(to: last)
			}
			
		}
	}
	
 
}


