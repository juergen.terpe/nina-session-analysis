//
//  CalibrationDatraView.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 25.02.25.
//
import SwiftUI

struct CalibrationInfo: Identifiable {
	let id = UUID()
	
	let name: LocalizedStringKey
	let value: String
}

@Observable
class CalibrationDatraViewModel {
	var infos: [CalibrationInfo] = []
	
	func update(calibration: CalibrationData) async {
		let infos = getValues(calibration: calibration)
		await MainActor.run {
			self.infos = infos
		}
	}
	
	private func getValues(calibration: CalibrationData) ->  [CalibrationInfo] {
		var result =  [CalibrationInfo]()
		
		result.append(CalibrationInfo(name: LocalizedStringKey("Time"),
									  value: calibration.calibrationDate.formatted(date: .abbreviated, time: .shortened)))
		
		if let dist = calibration.calibrationDistance {
			result.append(CalibrationInfo(name: LocalizedStringKey("Distance"), value: "\(dist) px"))
		}
		
		if let stepSize = calibration.stepSize {
			result.append(CalibrationInfo(name: LocalizedStringKey("Stepsize"), value: "\(stepSize)"))
		}
		
		if let az = calibration.azimuth, let alt = calibration.altitude {
			result.append(CalibrationInfo(name: LocalizedStringKey("Azimuth"), value: String(format: "%.1f°", az)))
			result.append(CalibrationInfo(name: LocalizedStringKey("Altitude"), value: String(format: "%.1f°", alt)))
		}
		
		if let hfd = calibration.hfd {
			result.append(CalibrationInfo(name: LocalizedStringKey("HFD"), value: "\(hfd)"))
		}
		
		if let rate = calibration.westRate {
			result.append(CalibrationInfo(name: LocalizedStringKey("West rate"), value: "\(rate) px/s"))
		}
		
		if let angle = calibration.westAngle {
			result.append(CalibrationInfo(name: LocalizedStringKey("West angle"), value: "\(angle)°"))
		}
		
		if let rate = calibration.northRate {
			result.append(CalibrationInfo(name: LocalizedStringKey("North rate"), value: "\(rate) px/s"))
		}
		
		if let angle = calibration.northAngle {
			result.append(CalibrationInfo(name: LocalizedStringKey("North angle"), value: "\(angle)°"))
		}
		
		result.append(CalibrationInfo(name: LocalizedStringKey("Steps North"), value: "\(calibration.northFrames.count)"))
		result.append(CalibrationInfo(name: LocalizedStringKey("Steps West"), value: "\(calibration.westFrames.count)"))
		
		return result
	}
}




struct CalibrationDataView: View {
	let calibration: CalibrationData
	@State private var model = CalibrationDatraViewModel()
	
	var body: some View {
		Grid(alignment: .trailing, horizontalSpacing: 8, verticalSpacing: 4) {
			
			ForEach(model.infos) { txt in
				GridRow(alignment: .firstTextBaseline) {
					Text(txt.name)
						.fontWeight(.semibold).kerning(1.5)
						.foregroundColor(Color.teal.opacity(0.8))
						.gridColumnAlignment(.leading)
					Text(txt.value)
				}
				.padding(.horizontal).padding(.vertical, 4)
				
				GridRow {
					Divider()
						.gridCellColumns(2)
				}
				
			}
		}
		.background(Color.primary.opacity(0.05))
		.cornerRadius(8)
		.task {
			await model.update(calibration: self.calibration)
		}
		.onChange(of: self.calibration.id) { _, _ in
			Task {
				await model.update(calibration: self.calibration)
			}
		}
	}
}
