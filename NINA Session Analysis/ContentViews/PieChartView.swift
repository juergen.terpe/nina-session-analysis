//
//  PieChartView.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 07.04.24.
//

import SwiftUI
import Charts

struct PieChartImage: Identifiable {
	let id = UUID()
	let title: String
	let count: Int
	let index: Int
}

extension SessionData {
	
	func getPieIssueImages(issue: SessionIssues) -> [PieChartImage] {
		
		let allImages = self.filteredImages
		
		let issueCount = allImages.filter({ $0.issues.contains(issue) }).count
		
		
		return [
			PieChartImage(title: issue.issueText, count: issueCount, index: 0),
			PieChartImage(title: "All", count: allImages.count - issueCount, index: 1),
		]
	}
	
	func getComparedImages() -> [PieChartImage] {
		
		let issueImages = self.sessionImages.filter { $0.issues.contains(.hfrIncreased) ||  $0.issues.contains(.highMeanValue)
												   || $0.issues.contains(.elongatedStars) || $0.issues.contains(.ditherSettleIssue) }.count
		let noIssueImages = self.sessionImages.count - issueImages
		
		return [
			PieChartImage(title: "Issue", count: issueImages, index: 0),
			PieChartImage(title: "No Issues", count: noIssueImages, index: 1)
		]
	}
	
}



struct PieChartView: View {
	var images: [PieChartImage]
	var total: Int
	
    var body: some View {
		
		ZStack {
			Chart(images) { img in
				SectorMark(
					angle: .value(
						Text(verbatim: img.title),
						img.count),
					innerRadius: .ratio(0.6),
					angularInset: 8
				)
				.foregroundStyle(
					by: .value(
						Text(verbatim: img.title),
						img.title
					)
				)
				.annotation(position: .overlay, alignment: .center, overflowResolution: AnnotationOverflowResolution(x: .disabled, y: .disabled)) {
					VStack {
						Text(img.title)
							.font(.body)
						Text("\(img.count, format: .number.precision(.fractionLength(0)))")
							.font(.body)
					}
				}
				
			}
			
			
			VStack {
				Spacer()
				HStack {
					Spacer()
					Text("\( String(format: "%.1f", Double(images[0].count)/Double(total) * 100)) %")
						.font(.title3)
					Spacer()
				}
				Spacer()
			}
			
		}
    }
}
