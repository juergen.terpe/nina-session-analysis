//
//  GrapItems.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 30.03.24.
//

import Foundation
import SwiftUI

public enum GraphItem: String, CaseIterable, Equatable {
	case AIRMASS = "Airmass"
	case FOCPOS = "Focuser Position"
	case CLOUDCVR = "Cloud Coverage"
	case DEWPOINT = "Dew Point"
	case HUMIDITY = "Humidity"
	case PRESSURE = "Pressure"
	case AMBTEMP = "Ambient Temperature"
	case WINDDIR = "Wind Direction"
	case WINDSPD = "Wind Speed"
	case WINDGUST = "Wind Gust"
    case SKYBRIGHTNESS = "Sky Brightness"
	case SKYTEMPERATURE = "Sky Temperature"
	case SkyQuality = "Sky Quality"
	case SUNANGLE = "Sun Angle"
	case MOONANGL = "Moon Angle"
	case HFR = "HFR"
	case FWHM = "FWHM"
	case ECCENTRICITY = "Eccentricity"
	case RMS = "RMS"
	case MEAN = "Mean"
	case MEDIAN = "Median"
	case RMSRA = "RMS(α)"
	case RMSDEC = "RMS(δ)"
    case RELRMSRADEC = "RMS Excentricity"
	case ADUMIN = "min(ADU)"
	case ADUMAX = "max(ADU)"
	case NUMSTARS = "#Stars"
	case MOONALT = "Moon Altitude"
	case SUNALT = "Sun Altitude"
	case DITHERRAPX = "Drift RA(px)"
	case DITHERDECPX = "Drift DEC(px)"
	case DITHERPX = "Drift (px)"
	case PulsesRA = "Pulses α"
	case PulsesDEC = "Pulses δ"
	case MaxPeakRA = "max Peak α"
	case MaxPeakDEC = "max Peak δ"
	case MaxPeak = "max Peak α,δ"
	case DELTAT = "ΔT"
	
	public var name: LocalizedStringKey {
		switch self {
			case .AIRMASS: 	return LocalizedStringKey("Airmass")
			case .FOCPOS:	return LocalizedStringKey("Focuser Position")
			case .CLOUDCVR: return LocalizedStringKey("Cloud Coverage")
			case .DEWPOINT: return LocalizedStringKey("Dewpoint")
			case .HUMIDITY: return LocalizedStringKey("Humidity")
			case .PRESSURE: return LocalizedStringKey("Air Pressure")
			case .AMBTEMP:  return LocalizedStringKey("Ambient Temperature")
			case .WINDDIR:  return LocalizedStringKey("Wind Direction")
			case .WINDSPD:  return LocalizedStringKey("Wind Speed")
			case .SUNANGLE: return LocalizedStringKey("Sun Angle")
			case .MOONANGL: return LocalizedStringKey("Moon Angle")
			case .HFR: 		return LocalizedStringKey("HFR")
			case .FWHM:		return LocalizedStringKey("FWHM")
			case .ECCENTRICITY: return LocalizedStringKey("Eccentricity")
			case .RMS:		return LocalizedStringKey("RMS")
			case .MEAN:		return LocalizedStringKey("Mean")
			case .MEDIAN:	return LocalizedStringKey("Median")
			case .RMSRA:	return LocalizedStringKey("RMS(α)")
			case .RMSDEC:	return LocalizedStringKey("RMS(δ)")
			case .RELRMSRADEC: return LocalizedStringKey("RMS Excentricity")
			case .ADUMIN: 	return LocalizedStringKey("min(ADU)")
			case .ADUMAX:	return LocalizedStringKey("max(ADU)")
			case .NUMSTARS:	return LocalizedStringKey("Number of Stars")
			case .MOONALT:	return LocalizedStringKey("Moon Altitude")
			case .SUNALT:	return LocalizedStringKey("Sun Altiude")
			case .DITHERRAPX:	return LocalizedStringKey("Drift α(px)")
			case .DITHERDECPX:	return LocalizedStringKey("Drift DEC(px)")
			case .DITHERPX:		return LocalizedStringKey("Drift (px)")
			case .PulsesRA:		return LocalizedStringKey("Pulses α")
			case .PulsesDEC:	return LocalizedStringKey("Pulses δ")
			case .WINDGUST:		return LocalizedStringKey("Wind Gusts")
			case .SKYBRIGHTNESS: return LocalizedStringKey("Sky Brightness")
			case .SKYTEMPERATURE: return LocalizedStringKey("Sky Temperature")
			case .SkyQuality: 	  return LocalizedStringKey("Sky Quality")
			case .MaxPeakRA:  return LocalizedStringKey("max Peak α")
			case .MaxPeakDEC: return LocalizedStringKey("max Peak δ")
			case .MaxPeak: return LocalizedStringKey("max Peak α,δ")
			case .DELTAT: return LocalizedStringKey("ΔT")
		}
	}
	
	public var unitText: LocalizedStringKey {
		switch self {
			case .CLOUDCVR, .HUMIDITY: return LocalizedStringKey("in %")
			case .DEWPOINT, .AMBTEMP, .SKYTEMPERATURE, .DELTAT: return LocalizedStringKey("in °C")
			case .HFR, .FWHM: return LocalizedStringKey("in px")
			case .MEDIAN, .MEAN, .ADUMIN, .ADUMAX: return LocalizedStringKey("in ADU")
			case .PRESSURE: return LocalizedStringKey("in hPa")
			case .RMS, .RMSRA, .RMSDEC, .MaxPeakRA, .MaxPeakDEC, .MaxPeak: return LocalizedStringKey("in \"")
			case .MOONALT, .SUNALT, .MOONANGL, .SUNANGLE: return LocalizedStringKey("in °")
			case .WINDSPD, .WINDGUST: return LocalizedStringKey("in km/h")
			case .PulsesRA, .PulsesDEC: return LocalizedStringKey("in ms")
			default: return ""
		}
	}
}

public extension SessionImage {
	
	func getValue(_ item: GraphItem) -> Float? {
		switch item {
			case .AIRMASS:
				return self.airmass
			case .ADUMAX:
				return self.aduMax != nil ? Float(self.aduMax!) : nil
			case .ADUMIN:
				return self.aduMin != nil ? Float(self.aduMin!) : nil
			case .AMBTEMP:
				return self.ambientTemp
			case .CLOUDCVR:
				return self.cloudCover
			case .DEWPOINT:
				return self.dewPoint
			case .HUMIDITY:
				return self.humidity
			case .ECCENTRICITY:
				return self.eccentricity
			case .WINDDIR:
				return self.windDirection
			case .WINDSPD:
				return self.windSpeed
			case .SUNANGLE:
				return self.sunAngle
			case .MOONANGL:
				return self.moonAngle
			case .HFR:
				return self.hfr
			case .FWHM:
				return self.fwhm
			case .RMS:
				return self.rms
			case .MEAN:
				return self.mean
			case .MEDIAN:
				return self.median != nil ? Float(self.median!) : nil
			case .RMSRA:
				return self.rmsRA
			case .RMSDEC:
				return self.rmsDEC
			case .RELRMSRADEC:
				return self.relationRMSRADEC
			case .FOCPOS:
				return self.focusPosition
			case .PRESSURE:
				return self.pressure
			case .NUMSTARS:
				return self.numStars != nil ? Float(self.numStars!) : nil
			case .MOONALT:
				return self.moonAlt != nil ? Float(self.moonAlt!) : nil
			case .SUNALT:
				return self.sunAlt != nil ? Float(self.sunAlt!) : nil
			case .DITHERRAPX:
				return self.ditherRAPixel
			case .DITHERDECPX:
				return self.ditherDECPixel
			case .DITHERPX:
				return self.ditherPixel
			case .WINDGUST:
				return self.windGust
			case .SKYTEMPERATURE:
				return self.skyTemperature
			case .DELTAT:
				if let amb = self.ambientTemp, let skyT = self.skyTemperature {
					return amb - skyT
				}
				return nil 
			case .SKYBRIGHTNESS:
				return self.skyBrightness
			case .SkyQuality:
				return self.skyQuality
			case .MaxPeakRA:
				if let minV = self.minPeakRA, let maxV = self.maxPeakRA {
					return max(abs(minV), abs(maxV))
				}
				return nil
			case .MaxPeakDEC:
				if let minV = self.minPeakDEC, let maxV = self.maxPeakDEC {
					return max(abs(minV), abs(maxV))
				}
				return nil
			case .MaxPeak:
				if let minV1 = self.minPeakDEC, let maxV1 = self.maxPeakDEC, let minV2 = self.minPeakDEC, let maxV2 = self.maxPeakDEC {
					return max(max(abs(minV1), abs(maxV1)), max(abs(minV2), abs(maxV2)))
				}
				return nil
			default:
				return nil
		}
		
	}
	
	func getValues(_ item: GraphItem) -> (Float, Float)? {
		switch item {
			case .PulsesRA:
				if let e = self.eastPulseDurations, let w = self.westPulseDurations {
					return (e, w)
				}
				return nil
			case .PulsesDEC:
				if let n = self.northPulseDurations, let s = self.southPulseDurations {
					return (n, s)
				}
				return nil
			default: return nil 
		}
	}
	
	func hasGuidePulseValues() -> Bool {
		return self.eastPulseDurations != nil
			&& self.westPulseDurations != nil
			&& self.northPulseDurations != nil
			&& self.southPulseDurations != nil
	}
	
}
