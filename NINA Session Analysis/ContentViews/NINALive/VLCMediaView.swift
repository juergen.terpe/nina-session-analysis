//
//  VLCMediaView.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 26.05.24.
//


 import Foundation
 import SwiftUI
 import VLCKit
 
 
class VLCPlayerWrapper: NSObject, ObservableObject {
	var mediaPlayer: VLCMediaPlayer?
	
	@Published var isPlaying: Bool = false
	@Published var isBuffering: Bool = false
	
	override init() {
		super.init()
		mediaPlayer = VLCMediaPlayer(options: ["--network-caching=5000"]) // change your media player related options
		mediaPlayer?.delegate = self
	}
	
	///Method to begin playing the specified URL
	func play(url: URL) {
		let media = VLCMedia(url: url)
		mediaPlayer?.media = media
		mediaPlayer?.play()
	}
	
	///Method to stop playing the currently playing video
	func stop() {
		mediaPlayer?.stop()
		isPlaying = false
	}
	
	func pause() {
		if isPlaying {
			mediaPlayer?.pause()
			isPlaying = false
		} else {
			mediaPlayer?.play()
			isPlaying = true
		}
	}
	
}
 
extension VLCPlayerWrapper: VLCMediaPlayerDelegate {
	///Implementation for VLCMediaPlayerDelegate to handle media player state change
	func mediaPlayerStateChanged(_ aNotification: NSNotification) {
		if let player = mediaPlayer {
			if player.state == .stopped {
				isPlaying = false
				isBuffering = false
			} else if player.state == .playing {
				isPlaying = true
				isBuffering = false
			} else if player.state == .opening {
				isBuffering = true
			} else if player.state == .error {
				stop()
			} else if player.state == .buffering {
			} else if player.state == .paused {
				isPlaying = false
			}
		}
	}
}
 
 ///A wrapper which allows the MobileVLCKit to be used with SwiftUI
struct VLCMediaPlayerView: NSViewRepresentable {
	typealias NSViewType = NSView
	
	@ObservedObject var playerWrapper: VLCPlayerWrapper
	
	///Method to create the UIKit view that is to be represented in SwiftUI
	func makeNSView(context: Context) -> NSView {
		let playerView = NSView()
		return playerView
	}
	
	///Method to update the UIKit view that is being used in SwiftUI
	func updateNSView(_ view: NSView, context: Context) {
		if let player = playerWrapper.mediaPlayer {
			player.drawable = view
		}
	}
}

