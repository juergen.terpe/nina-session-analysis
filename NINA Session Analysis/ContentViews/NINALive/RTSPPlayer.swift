//
//  RTSPPlayer.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 26.05.24.
//

import SwiftUI

/*
struct RTSPPlayer: View {
	@AppStorage("RTSPURL") var rtspURL: URL = URL(string: "")!
	
	@StateObject private var playerWrapper: VLCPlayerWrapper = VLCPlayerWrapper()

	
	var body: some View {
			ZStack(alignment: .bottom) {
				VLCMediaPlayerView(playerWrapper: playerWrapper)
				.overlay(alignment: .center, content: {
					if self.playerWrapper.isBuffering {
						VStack {
							ProgressView() {
								Text("Bufferning...")
								.foregroundColor(.white)
								 .font(.headline)
							}
								.tint(.white)
								.padding()
						}
						.background(.black)
						.cornerRadius(20)
					}
				})
				Spacer()
				HStack {
					Button(action: {
						playerWrapper.play(url: self.rtspURL)
					}, label: {
						Text("Play")
					})

					Button(action: {
					   playerWrapper.stop()
					}, label: {
					   Text("Stop")
					})

					Button(action: {
					   playerWrapper.pause()
					}, label: {
					   Text("Pause")
					})
				}
			}
		}
}

#Preview {
    RTSPPlayer()
}
*/
