//
//  DataSummaryView.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 26.09.24.
//
import SwiftUI

@Observable
class DataSummaryViewModel {
	var summary: [DataSummary] = []
	
	func update(from session: SessionData) async {
		
		let summary = await session.coverage()
		
		await MainActor.run {
			self.summary = summary
		}
		
	}
	
}

struct DataSummaryView: View {
	@State var session: SessionData
	@State private var model = DataSummaryViewModel()
	var geometry : GeometryProxy
	
	var body: some View {
		
		Table(model.summary) {
			
			TableColumn("Date/Time") { summary in
				
				if let date = summary.date {
					Text(date.formatted(date: .abbreviated, time: .shortened))
						.foregroundColor(summary.bad ? .red : .primary)
				} else {
					Text("???")
						.foregroundColor(Color.yellow)
				}
				
			}.width(200)
			
			TableColumn("Exposure") { summary in
				Text(summary.exposure)
					.foregroundColor(summary.bad ? .red : .primary)
			}.width(80)
			
			TableColumn("N.I.N.A. Log") { summary in
				Image(systemName: summary.ninaLog ? "checkmark" : "xmark")
					.imageScale(.medium)
					.foregroundColor(summary.ninaLog ? .green : .red)
			}.width(120)
			
			TableColumn("Guide Log") { summary in
				Image(systemName: summary.guideLog ? "checkmark" : "xmark")
					.imageScale(.medium)
					.foregroundColor(summary.guideLog ? .green : .red)
			}.width(80)
			
			TableColumn("Metadata") { summary in
				Image(systemName: summary.metaData.icon)
					.imageScale(.medium)
					.foregroundColor(summary.metaData.color)
			}.width(80)
			
			TableColumn("Image") { summary in
				Text(summary.image)
					.foregroundColor(summary.bad ? .red : .primary)
			}.width(800)
			
			TableColumn("Filter") { summary in
				Text(summary.filter)
					.foregroundColor(summary.bad ? .red : .primary)
			}.width(200)
		}
		.frame(height: geometry.height)
		
		.task {
			await self.model.update(from: session)
		}
	}
}
