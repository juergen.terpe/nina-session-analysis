//
//  InfoScreenView.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 23.09.24.
//
import SwiftUI


struct InfoScreenView: View {
	@State private var mailContent: String = ""
	@State private var tipStore: TipsStore = TipsStore()
	
	var body: some View {
		
		
		ScrollView(.vertical) {
			LazyVStack {
				
				HStack {
					Text("N.I.N.A. Session Analysis").font(.largeTitle).kerning(1.5).bold().foregroundColor(Color.teal)
					Spacer()
				}
				.padding(.vertical)
				Spacer().frame(height: 32)
				
				Divider()
				
				if !tipStore.hasAlreadyPurchased {
					
					VStack(alignment: .leading) {
						
						HStack {
							Text("Do you like this app?...").font(.title3)
								.kerning(1.5).bold().foregroundColor(Color.teal)
							Spacer()
						}
						.padding(.bottom, 8)
						
						Text("InvitationText").font(.body).bold()
							.frame(maxWidth: .infinity)
						
						InAppPurchaseTipsView(tipStore: $tipStore)
							.padding(.horizontal)
							.padding(.vertical, 32)
							.frame(maxWidth: .infinity)
					}
					.transition(.slide)
					.animation(.easeInOut, value: tipStore.purchased)
				}
				
				Spacer().frame(height: 32)
				
				Divider()
				
				HStack {
					Text("Is there anything you would like to see added?")
						.font(.title3).kerning(1.5).bold().foregroundColor(Color.teal)
					Spacer()
				}
				
				HStack {
					Text("Report a bug or feature request...").font(.body).bold()
					Spacer()
				}
				.padding(.vertical)
				
				VStack {
					
					TextEditor(text: $mailContent)
						.font(.body)
						.lineLimit(400)
						.border(.secondary.opacity(0.2), width: 0.5)
						.frame(height: 400)
					
					HStack {
						Button {
							SendEmail.send(subject: "NINA Session Analysis", message: mailContent)
							
						} label: {
							
							Label {
								Text("Send")
									.frame(alignment: .leading)
									.foregroundColor(.primary)
							} icon: {
								Image(systemName: "mail")
									.foregroundColor(Color.green)
							}
							.padding(.horizontal, 8)
							.padding(.vertical, 8)
							.frame(width: 300)
							.background(Color.blue.opacity(0.2))
						}
						.buttonStyle(PlainButtonStyle())
						.contentShape(Rectangle())
						.buttonRepeatBehavior(.disabled)
						.cornerRadius(8)
						
						Spacer()
					}
					
					Spacer(minLength: 100)
				}
				
			}
			.padding()
		}
		.background(.thickMaterial)
		.cornerRadius(8)
	}
}
