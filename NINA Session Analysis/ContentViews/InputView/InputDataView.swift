//
//  InputData.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 29.03.24.
//

import SwiftUI
import PureSwiftUI
import TipKit
import DebouncedOnChange
import SwiftRater

extension LogEntry {
	func getColor() -> Color {
		switch self.type {
			case .info: return Color.primary.opacity(0.85)
			case .note: return Color.green
			case .warn: return Color.orange
			case .error: return Color.red
		}
	}
}

enum InputDataViewMode {
	case wellcomeScreen
	case logScreen
	case summaryScreen
}


struct InputDataViewButtonStyle: ButtonStyle {
	@Environment(\.isEnabled) var isEnabled
	@State private var hovered = false
	
	func makeBody(configuration: Configuration) -> some View {
		configuration.label
			.background(configuration.isPressed ? Color.accentColor.opacity(0.8).gradient
						: self.hovered ? Color.accentColor.opacity(0.4).gradient : Color.primary.opacity(0.2).gradient)
			.cornerRadius(8)
			.saturation(isEnabled ? 1 : 0)
			.onHover { isHovered in
				self.hovered = isHovered
			}
	}
}



struct InputDataView: View {
	@State var session: SessionData
	@Binding var showSettings: Bool 
	@State private var showWriteFITSHeaders = false
	@State private var showFeedback = false
	@State private var inputDataViewMode: InputDataViewMode = .wellcomeScreen
	
	@Binding var menuDisabled: Bool
	
	@ViewBuilder
	var inputButtons: some View {
		ScrollView(.vertical) {
			
			VStack(alignment: .leading, spacing: 32) {
				
				
				Button {
					self.inputDataViewMode = .logScreen
					
					let openPanel = NSOpenPanel()
					openPanel.title = "Select a folder to the lights of the session"
					openPanel.message = "All fits inside this folder will be inspected"
					openPanel.canChooseDirectories = true
					openPanel.canChooseFiles = false
					openPanel.allowsMultipleSelection = false
					openPanel.canCreateDirectories = false
					if openPanel.runModal() == .OK && openPanel.url != nil {
						self.menuDisabled = true
						session.updateSessionLights(openPanel.url!)
						
						self.menuDisabled = false
					}
					else {
						self.inputDataViewMode = session.sessionImages.count == 0 ? .wellcomeScreen : .summaryScreen
					}
					
				} label: {
					
					Label {
						Text("Select Lights folder")
							.frame(alignment: .leading)
							.foregroundColor(.primary)
					} icon: {
						Image(systemName: "folder.fill")
							.foregroundStyle(session.sessionImages.count == 0 ? Color.accentColor.gradient : Color.green.gradient)
					}
					.padding(.horizontal, 8)
					.padding(.vertical, 8)
					.frame(maxWidth: .infinity)
					//.background(Color.primary.opacity(0.2))
				}
				.buttonStyle(InputDataViewButtonStyle())
				.contentShape(Rectangle())
				.cornerRadius(8)
				.buttonRepeatBehavior(.disabled)
				.keyboardShortcut(KeyEquivalent("L"), modifiers: .command)
				.help("Select a folder containing lights. This can be the top folder of any multi-night project and might also contain darks and other files. (Command + L)")
				.popoverTip(StartHereTip(), arrowEdge: .trailing)
				
				Button {
					self.inputDataViewMode = .logScreen
					
					let openPanel = NSOpenPanel()
					openPanel.title = "Select the metadata files for this session"
					openPanel.message = "Select the metadata files for this session"
					openPanel.canChooseDirectories = false
					openPanel.canChooseFiles = true
					openPanel.allowsMultipleSelection = true
					openPanel.canCreateDirectories = false
					if openPanel.runModal() == .OK && openPanel.urls.isNotEmpty {
						Task {
							var metaDataAvailable = false
							for url in openPanel.urls {
								if await session.updateImageMetadata(url) {
									metaDataAvailable = true
								}
							}
							
							let metaData = metaDataAvailable
							await MainActor.run {
								session.id = UUID()
								session.metaDataAvailable = metaData
								
								self.inputDataViewMode = .summaryScreen
							}
						}
					}
				} label: {
					
					Label {
						Text("Select Image Metadata")
							.frame(alignment: .leading)
							.foregroundColor(.primary)
					} icon: {
						Image(systemName: "folder.fill")
							.foregroundStyle(session.metaDataAvailable ? Color.green.gradient : Color.accentColor.gradient)
					}
					.padding(.horizontal, 8)
					.padding(.vertical, 8)
					.frame(maxWidth: .infinity)
					//.background(Color.primary.opacity(0.2))
				}
				.buttonStyle(InputDataViewButtonStyle())
				.contentShape(Rectangle())
				.cornerRadius(8)
				.buttonRepeatBehavior(.disabled)
				.disabled(self.session.sessionImages.count == 0)
				.keyboardShortcut(KeyEquivalent("M"), modifiers: .command)
				.help("Select multiple Image Metadata files. This is only necessary, if these files are located outside the folder you selected for the lights! Avoid reading this information twice, because it is just wasted time! (Command + M)")
				
				
				Button {
					self.inputDataViewMode = .logScreen
					
					let openPanel = NSOpenPanel()
					openPanel.title = "Select the Guide log files for this session"
					openPanel.message = "Select the Guide log files for this session"
					openPanel.canChooseDirectories = false
					openPanel.canChooseFiles = true
					openPanel.allowsMultipleSelection = true
					openPanel.canCreateDirectories = false
					if openPanel.runModal() == .OK && openPanel.urls.isNotEmpty {
						let guideLogUrls = openPanel.urls
						
						Task {
							await withTaskGroup(of: Void.self) { taskGroup in
								// Iterate over each file and add task to TaskGroup
								for file in guideLogUrls {
									taskGroup.addTask {
										await MainActor.run {
											self.session.logging.append(LogEntry(text: LocalizedStringKey("reading \(file.absoluteString)")))
										}
										
										await self.session.readGuideLog(file)
									}
								}
								
								// Wait for all tasks to complete
								for await _ in taskGroup {}
							}
							
							await MainActor.run {
								session.assignImagesToGuideLogs()
								session.id = UUID()
								
								self.inputDataViewMode = .summaryScreen
							}
							
						}
					
					}
					
				} label: {
					
					Label {
						Text("Select Guide Log")
							.frame(alignment: .leading)
							.foregroundColor(.primary)
					} icon: {
						Image(systemName: "folder.fill")
							.foregroundStyle(session.guideLogAvailable ? Color.green.gradient : Color.accentColor.gradient)
					}
					.padding(.horizontal, 8)
					.padding(.vertical, 8)
					.frame(maxWidth: .infinity)
					//.background(Color.primary.opacity(0.2))
				}
				.buttonStyle(InputDataViewButtonStyle())
				.contentShape(Rectangle())
				.cornerRadius(8)
				.buttonRepeatBehavior(.disabled)
				.disabled(self.session.sessionImages.count == 0)
				.keyboardShortcut(KeyEquivalent("G"), modifiers: .command)
				.help("Select multiple PHD2 Guidelog files. This is only necessary, if these files are located outside the folder you selected for the lights! Avoid reading this information twice, because it is just wasted time! (Command + G)")
				
				Button {
					self.inputDataViewMode = .logScreen
					
					let openPanel = NSOpenPanel()
					openPanel.title = "Select the folder containing the NINA log files for this session"
					openPanel.message = "Select the folder containing the NINA log file for this session"
					openPanel.canChooseDirectories = true
					openPanel.canChooseFiles = false
					openPanel.allowsMultipleSelection = false
					openPanel.canCreateDirectories = false
					if openPanel.runModal() == .OK && openPanel.url != nil {
						Task {
							await session.findNINALogs(openPanel.url!, update: true)
							
							await MainActor.run {
								session.id = UUID()
								
								self.inputDataViewMode = .summaryScreen
							}
						}
					}
				} label: {
					
					Label {
						Text("Select NINA Log")
							.frame(alignment: .leading)
							.foregroundColor(.primary)
					} icon: {
						Image(systemName: "folder.fill")
							.foregroundStyle(session.ninaLogs.count > 0 ? Color.green.gradient : Color.accentColor.gradient)
					}
					.padding(.horizontal, 8)
					.padding(.vertical, 8)
					.frame(maxWidth: .infinity)
					//.background(Color.primary.opacity(0.2))
				}
				.buttonStyle(InputDataViewButtonStyle())
				.contentShape(Rectangle())
				.cornerRadius(8)
				.buttonRepeatBehavior(.disabled)
				.keyboardShortcut(KeyEquivalent("N"), modifiers: .command)
				.help("Select multiple NINA log files. This is only necessary, if these files are located outside the folder you selected for the lights! Avoid reading this information twice, because it is just wasted time! (Command + N)")
				
				 
				Button {
					self.inputDataViewMode = .wellcomeScreen
					session.id = UUID()
					
					DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
						session.resetAll()
					}
					
				} label: {
					
					Label {
						Text("Clear")
							.frame(alignment: .leading)
							.foregroundColor(.primary)
					} icon: {
						Image(systemName: "trash")
							.foregroundStyle(Color.red.gradient)
					}
					.padding(.horizontal, 8)
					.padding(.vertical, 8)
					.frame(maxWidth: .infinity)
					//.background(Color.primary.opacity(0.2))
				}
				.buttonStyle(InputDataViewButtonStyle())
				.contentShape(Rectangle())
				.buttonRepeatBehavior(.disabled)
				.cornerRadius(8)
				.disabledIf(session.readingData)
				.id(session.id)
				
				Picker("Timezone", selection: $session.selectedTimeZone) {
					ForEach(TimeZone.knownTimeZoneIdentifiers, id: \.self) { tz in
						Text(tz).truncationMode(.middle).tag(tz)
					}
				}
				.frame(minWidth: 200, maxWidth: 380)
				.padding(.top)
				.padding(.bottom)
				.help("Select the timezone on the machine you are running this N.I.N.A. session. In most cases this will be identical to your local timezone, which is already preselected. But when you are running a remote observatory in another timezone this setting must be changed, so the logs of PHD2 and N.I.N.A. are matching with the image metadata!")
				
			}
			.padding(32)
			.frame(maxWidth: 400)
			.background(Color.controlBackgroundDarkColor)
			.cornerRadius(8)
			.padding(.horizontal)
			.compositingGroup()
		}
	}
	
	@ViewBuilder
	var bottomViews: some View {
		HStack(spacing: 4) {
			
			VStack {
				HStack {
					Text("Filter").padding(.horizontal).padding(.vertical, 4).frame(maxWidth: .infinity, alignment: .leading)
				}
				.frame(maxWidth: .infinity, alignment: .leading)
				.background(Color.controlBackgroundDarkColor.gradient.opacity(0.5))
				
				FilterSettingsView(session: session)
			}
			.background(Color.controlBackgroundColor)
			.frame(maxWidth: 380)
			
			VStack {
				HStack {
					Text("Info").padding(.horizontal).padding(.vertical, 4).frame(maxWidth: .infinity, alignment: .leading)
				}
				.frame(maxWidth: .infinity, alignment: .leading)
				.background(Color.controlBackgroundDarkColor.gradient.opacity(0.5))
			
				HStack{
					
					SessionImageStatisticsView(session: session)
						.frame(minWidth: 300, maxWidth: .infinity)
					
					SessionInfoView(session: session)
						.frame(minWidth: 400, maxWidth: 600)
				}
			}
			.background(Color.controlBackgroundColor)
			
		}
		.compositingGroup()
		
	}
	
	var logView: some View {
		ScrollViewReader { scrollView in
			ScrollView {
				LazyVStack {
					ForEach(session.logging, id: \.self.id) { message in
						VStack {
							Text(message.text)
								.font(.body)
								.monospaced()
								.foregroundColor(message.getColor())
								.padding(.horizontal)
								.frame(maxWidth: .infinity, alignment: .leading)
						}
					}
					
					
				}
				.id("LogScrollView")
				
			}
			.onChange(of: session.logging) { _, _ in
				withAnimation {
					scrollView.scrollTo("LogScrollView", anchor: .bottom)
				}
			}
			.disabled(session.readingData)
			//.frame(maxHeight: .infinity)
			.background(Color.controlBackgroundDarkColor)
			.cornerRadius(8)
			.compositingGroup()
			.padding(.bottom, 8)
		}
		
	}
	
	@ViewBuilder
	var feedbackView: some View {
		HStack {
			
			Spacer()
			
			Button {
				SwiftRater.rateApp()
			} label: {
				Text("Rate in App Store")
					.fontWeight(.medium)
					.padding(.horizontal, 32)
					.padding(.vertical, 8)
					.background(Color.gray.opacity(0.05).cornerRadius(16))
			}
			.foregroundColor(.accentColor)
			.buttonStyle(PlainButtonStyle())
			.buttonRepeatBehavior(.disabled)
			.disabledIf(session.readingData)
			.padding(.top, 8)
			.padding(.trailing)
			
			Button {
				
				withAnimation(.easeIn(duration: 0.2)) {
					self.showFeedback.toggle()
				}
				
			} label: {
				Label {
					Text("Feedback")
						.fontWeight(.medium)
						.frame(alignment: .leading)
					
				} icon: {
					Image(systemName: "arrowshape.left")
						.rotationEffect(self.showFeedback ? .degrees(180) : .degrees(0))
				}
				.foregroundColor(.accentColor)
				.padding(.horizontal, 32)
				.padding(.vertical, 8)
				.background(Color.gray.opacity(0.05).cornerRadius(16))
				.contentShape(Rectangle())
				.popoverTip(AskForImprovementsTip(), arrowEdge: .leading)
			}
			.buttonStyle(PlainButtonStyle())
			.buttonRepeatBehavior(.disabled)
			.cornerRadius(8)
			.disabledIf(session.readingData)
			.padding(.top, 8)
			.padding(.trailing)
			
		}
	}
	
	
    var body: some View {
		VStack {
			
			feedbackView
			
			GeometryReader { g in
				
				HStack(alignment: .top) {
					
					// Buttons
					inputButtons
					
					VStack(alignment: .leading) {
						
						Group {
							
							if self.inputDataViewMode == .wellcomeScreen {
								InputDataViewWellcomeScreen()
									.transition(.move(edge: .top))
							} else if self.showWriteFITSHeaders {
								ShowWriteHeadersView(session: session, show: $showWriteFITSHeaders)
									.transition(.move(edge: .top))
							} else if self.inputDataViewMode == .summaryScreen {
								DataSummaryView(session: session, geometry: g)
									.transition(.move(edge: .trailing))
							} else {
								
								logView
									.transition(.move(edge: .top))
							}
						}
						.overlay {
							if self.showFeedback {
								InfoScreenView()
									.transition(.move(edge: .trailing))
							}
							
						}
					}
					.padding(.horizontal)
					.padding(.bottom)
				}
			}
			
			if self.inputDataViewMode == .summaryScreen {
				bottomViews
					.transition(.move(edge: .bottom))
			}
		}
		.overlay {
			
			VStack {
				HStack {
					Spacer()
					if showSettings {
						AnalysationSettingsView(session: session)
							.transition(.move(edge: .top))
					}
				}
				Spacer()
			}
		}
		.onAppear(perform: {
			if session.sessionImages.isEmpty {
				self.inputDataViewMode = .wellcomeScreen
			} else {
				self.inputDataViewMode = .summaryScreen
			}
		})
		.onChange(of: session.readingData,
				  debounceTime: session.sessionImages.isEmpty ? .seconds(3.0) : .milliseconds(200.0) ) { _ in
			
			if session.readingData {
				self.inputDataViewMode = .logScreen
			} else if session.sessionImages.isEmpty {
				self.inputDataViewMode = .wellcomeScreen
			} else {
				self.inputDataViewMode = .summaryScreen
				
				DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3)) {
					SwiftRater.check()
				}
			}
		}
    }
}

