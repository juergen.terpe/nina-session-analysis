//
//  InAppPurchaseTipsView.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 23.09.24.
//
import SwiftUI
import StoreKit





struct InAppPurchaseTipsView: View {
	private let IconSize: CGFloat = 50
	@Binding var tipStore: TipsStore 
	

	func getIcon(_ id: String) -> some View {
		Group {
			if id == tipIds[0] {
				Text("🍏").font(.system(size: IconSize))
			} else if id == tipIds[1] {
				Text("☕️").font(.system(size: IconSize))
			} else if id == tipIds[2] {
				Text("🍺").font(.system(size: IconSize))
			} else if id == tipIds[3] {
				Text("🍾").font(.system(size: IconSize))
			}
		}
	}
	
	
	var body: some View {
		LazyVGrid(columns: [GridItem(.adaptive(minimum: 420, maximum: 800))]) {
			
			ForEach(tipStore.tips) { tip in
				
				ProductView(tip) {
					getIcon(tip.id)
				}
				.productViewStyle(.compact)
				.padding()
				
			}
			
		}
		.task {
			await tipStore.fetch()
		}
		
	}
}
