//
//  InputDataViewWelcomeScreen.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 15.05.24.
//

import SwiftUI

struct InputDataViewWellcomeScreen: View {
    var body: some View {
		
		ScrollView(.vertical) {
			
			
			
			VStack(alignment: .leading) {
				Text("Session Analysis").font(.title).kerning(1.5).bold().foregroundColor(Color.teal)
				Divider()
				Text("In order to analyze your imaging session you will need the following: ").padding(.bottom, 16)
				Text("1. The Lights").font(.title2).foregroundColor(Color.cyan).padding(.bottom)
				Text("Select the folder containing all your Lights. The folder can contain lights from one or multiple nights. Best practice is to store also the Image Metadata, the PHD2 Guide Logfiles and the N.I.N.A. Logfiles inside this folder. In this case all you need to do is to select this folder and the application will read everything it needs.")
					.padding(.bottom, 16)
				
				Text("2. The Image Metadata").font(.title2).kerning(1.5).foregroundColor(Color.cyan).padding(.bottom, 8)
				Text("The Image Metadata files are the files created by the N.I.N.A. Plugin \"Session Metadata\". You will need to install this plugin and configure it to create these additional files in csv or json format. I would recommend csv format, because you can also use these files with other programs such as Excel or Numbers. These files contain valuable information such as the HFR values or star eccentricities for each image you captured, which are missing in the FITS file. Store these files together with your lights. ")
					.padding(.bottom, 16)
				
				Text("3. The PHD2 Guide Logfile").font(.title2).kerning(1.5).foregroundColor(Color.cyan).padding(.bottom, 8)
				Text("PHD2 creates two logfiles, one containing \"DEBUG\" and one containing the word \"GuideLog\". Please store this file together with your lights, if you want to analyze guiding. This file is not very large and can give you a lot of information for later troubleshooting, if required. The PHD2 Guide Logfile is not required, but having it makes using this application really useful. ")
					.padding(.bottom, 16)
				
				Text("4. The N.I.N.A. Logfile").font(.title2).kerning(1.5).foregroundColor(Color.cyan).padding(.bottom, 8)
				Text("The NINA logfile is not required, but you can only have a look into the logfile, if you have it. In case of any problems during your session this is an essential must have to find and fix issues. For this reason I would recommend storing this file together with your lights. ")
					.padding(.bottom, 32)
				
				Text("Timezone").font(.title2).kerning(1.5).foregroundColor(Color.cyan).padding(.bottom, 8)
				Text("If you captured your lights from another timezone (the timezone configured in your PC running NINA) you must select the timezone to ensure the PHD2 Guidelog and the NINA Logfile both have times that match the times of your captured images. If you see any mismatch you should check the timezone and change it before you load all your data!")

			}
			.padding()
			
		}
		.background(Color.controlBackgroundDarkColor)
		.cornerRadius(8.0)
    }
}

#Preview {
	InputDataViewWellcomeScreen()
}
