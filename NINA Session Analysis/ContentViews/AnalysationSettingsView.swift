//
//  AnalysationSettingsView.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 05.04.24.
//

import SwiftUI

struct AnalysationSettingsView: View {
	@State var session: SessionData
	
	var body: some View {
		
		ScrollView {
			
			VStack(alignment: .leading) {
				Text("Mean Tolerance: \(session.analyzationSettings.meanTolerance, specifier: "%2.1f") %").foregroundColor(.lightBlue)
				Slider(value: $session.analyzationSettings.meanTolerance, in: 0...100)
					.padding(.bottom)
					.onChange(of: session.analyzationSettings.meanTolerance) { _, newValue in
						session.analyzeAvailable = false
						
						Task(priority: .background) {
							session.analyzeImages()
							session.analyzationSettings.store()
						}
					}
					.help("Change the tolerance for mean values")
				
				Text("HFR Tolerance: \(session.analyzationSettings.hfrTolerance, specifier: "%2.1f") %").foregroundColor(.lightBlue)
				Slider(value: $session.analyzationSettings.hfrTolerance, in: 0...100)
					.padding(.bottom)
					.onChange(of: session.analyzationSettings.hfrTolerance) { _, newValue in
						session.analyzeAvailable = false
						
						Task(priority: .background) {
							session.analyzeImages()
							session.analyzationSettings.store()
						}
					}
					.help("Change the tolerance for HFR values")
				
				Text("#Stars Tolerance: \(session.analyzationSettings.starCountTolerance, specifier: "%2.1f") %").foregroundColor(.lightBlue)
				Slider(value: $session.analyzationSettings.starCountTolerance, in: 0...100)
					.padding(.bottom)
					.onChange(of: session.analyzationSettings.starCountTolerance) { _, newValue in
						session.analyzeAvailable = false
						
						Task(priority: .background) {
							session.analyzeImages()
							session.analyzationSettings.store()
						}
					}
					.help("Change the tolerance for number of stars")
				
				Text("Star Eccentricity Tolerance: \(session.analyzationSettings.starEccentricityTolerance, specifier: "%2.1f")").foregroundColor(.lightBlue)
				Slider(value: $session.analyzationSettings.starEccentricityTolerance, in: 0.2...0.7)
					.padding(.bottom)
					.onChange(of: session.analyzationSettings.starEccentricityTolerance) { _, newValue in
						session.analyzeAvailable = false
						
						Task(priority: .background) {
							session.analyzeImages()
							session.analyzationSettings.store()
						}
					}
					.help("Change the tolerance for star eccentricities")
				
				Text("RMS Tolerance: \(session.analyzationSettings.rmsTolerance, specifier: "%2.1f")% of Pixel resolution").foregroundColor(.lightBlue)
				Slider(value: $session.analyzationSettings.rmsTolerance, in: 0...120)
					.padding(.bottom)
					.onChange(of: session.analyzationSettings.rmsTolerance) { _, newValue in
						session.analyzeAvailable = false
						
						Task(priority: .background) {
							session.analyzeImages()
							session.analyzationSettings.store()
						}
					}
					.help("Change the tolerance for Guiding RMS")
				
				Text("Humidity Tolerance: \(session.analyzationSettings.humidityTolerance, specifier: "%2.1f")%").foregroundColor(.lightBlue)
				Slider(value: $session.analyzationSettings.humidityTolerance, in: 20...100)
					.padding(.bottom)
					.onChange(of: session.analyzationSettings.humidityTolerance) { _, newValue in
						session.analyzeAvailable = false
						
						Task(priority: .background) {
							session.analyzeImages()
							session.analyzationSettings.store()
						}
					}
					.help("Change the tolerance for humidity")
				
				Text("Cloud Coverage Tolerance: \(session.analyzationSettings.cloudCoverageTolerance, specifier: "%2.1f")%").foregroundColor(.lightBlue)
				Slider(value: $session.analyzationSettings.cloudCoverageTolerance, in: 0...100)
					.padding(.bottom)
					.onChange(of: session.analyzationSettings.cloudCoverageTolerance) { _, newValue in
						session.analyzeAvailable = false
						
						Task(priority: .background) {
							session.analyzeImages()
							session.analyzationSettings.store()
						}
						
					}
					.help("Change the tolerance for cloud coverage")
				
				Text("Wind Speed Tolerance: \(session.analyzationSettings.windSpeedTolerance, specifier: "%2.1f")").foregroundColor(.lightBlue)
				Slider(value: $session.analyzationSettings.windSpeedTolerance, in: 0...60)
					.padding(.bottom)
					.onChange(of: session.analyzationSettings.windSpeedTolerance) { _, newValue in
						session.analyzeAvailable = false
						
						Task(priority: .background) {
							session.analyzeImages()
							session.analyzationSettings.store()
						}
					}
					.help("Change the tolerance for wind speeds")
				
			}
			.padding()
			.frame(maxWidth: 600)
			.background(.thinMaterial)
			
		}
		.compositingGroup()
    }
}


