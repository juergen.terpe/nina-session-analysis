//
//  GuidingView.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 03.04.24.
//

import SwiftUI
import PureSwiftUI
import CoreLocation

extension SessionData {
	
	func getBinnedData(for item: GraphItem, guidingId: GuidingData.ID, count: Int = 30) async -> BinnedData {
		
		let binnedData = BinnedData(session: self,  item: item, guidingId: guidingId, count: count)
		await binnedData.update()
		
		return binnedData
	}
}

fileprivate func ??<T>(binding: Binding<T?>, fallback: T) -> Binding<T> {
	return Binding(
		get: {
			binding.wrappedValue ?? fallback
		}, 
		set: {
			binding.wrappedValue = $0
		})
}

enum GuidingTabs: Int, Hashable {
	case info = 1
	case ra = 2
	case dec = 3
	case calibration = 4
}


struct GuidingView: View, SkySphereViewContent {
	@State var session: SessionData
	@State private var calibrationDate: UUID?
	@State var selectedGuidingSessionId: GuidingData.ID
	@State private var selectedTime: JulianDate?
	@State private var selectedLostStar: LostStarEvents.ID?
	@State private var selectedDithering: DitheringEvents.ID?
	@State private var hfrBinnedData: BinnedData?
	@State private var eccBinnedData: BinnedData?
	@State private var rmsBinnedData: BinnedData?
	
	@StateObject var skyViewSource = SkySphereViewDataSource()
	
	
	@AppStorage("GuidingRaEnabled") private var raEnabled: Bool = true
	@AppStorage("GuidingDecEnabled") private var decEnabled: Bool = true
	@AppStorage("GuidingCorrectionsEnabled") private var correctionsEnabled: Bool = true
	@AppStorage("GuidingSNREnabled") private var snrEnabled: Bool = true
	@AppStorage("GuidingImagesEnabled") private var imagesEnabled: Bool = true
	@AppStorage("GuidingIssuesEnabled") private var issuesEnabled: Bool = true
	@AppStorage("GuidingDitheringEnabled") private var ditheringEnabled: Bool = true
	@AppStorage("LostStarsEnabled") private var lostStarsEnabled: Bool = true
	@AppStorage("GuidingMinDuration") private var minGuidingDuration: Int = 10
	
	@AppStorage("GuidingView2Tab") var selectedTabChart: GuidingTabs = GuidingTabs.info
	@AppStorage("GuideGraphSessionScaleY") var scaleY = 4
	@AppStorage("GuideGraphSessionScaleX") var scaleX = 5.0
	@AppStorage("GuidingViewsOnlyImages") var imagesOnly: Bool = false
	
	func drawContent(_ projection: SkySphereViewProjection) -> AnyView {
		if let calib = session.guiding.Calibrations.first(where: { $0.id == self.calibrationDate}),
		   let ra = calib.ra, let dec = calib.dec {
			
			let pos = Vector3.from(ra: Double(ra * 15.0) * Constants.Rad, dec: Double(dec) * Constants.Rad)
			
			return CameraShape(projection: projection, position: pos)
					.stroke(Color.yellow)
					.typeErased()
		}
		return EmptyView().typeErased()
	}
	
	
	var selectedCalibration: CalibrationData? {
		return session.guiding.Calibrations.first(where: { $0.id == calibrationDate })
	}
	
	var selectedGuiding: GuidingData? {
		return session.guiding.guidingSessions.first(where: { $0.id == selectedGuidingSessionId })
	}
	
	var hasGuidingImages: Bool {
		return session.getImages(for: selectedGuidingSessionId).count > 5
	}
	
	var calibrationTab: some View {
		HStack(alignment: .top) {
			
			VStack(alignment: .leading) {
				
				Text("Calibrations")
					.kerning(1.5)
					.foregroundStyle(Color.teal)
					.padding(.horizontal)
				
				List(selection: $calibrationDate) {
					ForEach(session.guiding.Calibrations) { cal in
						Text(cal.text)
							.foregroundStyle(Color.lightBlue)
							.tag(cal.id)
							.padding(.vertical, 4)
					}
				}
				.listRowSeparator(.hidden)
			}
			.frame(maxWidth: 180)
			.padding(.horizontal)
			.padding(.vertical)
			
			VStack {
				
				if let cal = session.guiding.Calibrations.first(where: { $0.id == calibrationDate }) {
					ScrollView(.horizontal) {
						HStack(alignment: .top) {
							
							VStack {
								
								CalibrationView(calibration: cal)
									.aspectRatio(1.0, contentMode: .fit)
									.frame(minWidth: 600, maxWidth: 800)
									.padding(.bottom, 4)
							
								Grid(alignment: .trailing, horizontalSpacing: 8, verticalSpacing: 4) {
									
									if let md = cal.meridianDistance {
										
										GridRow(alignment: .firstTextBaseline) {
											Text("Meridian distance")
												.kerning(1.5).foregroundStyle(Color.teal)
												.padding(.horizontal)
												.gridColumnAlignment(.leading)
											Text("\(md, specifier: "%.2f")°")
												.padding(.horizontal)
										}
										.padding(.vertical, 8)
										
										GridRow {
											Divider()
												.gridCellColumns(2)
										}
									}
									
									if let eqd = cal.dec {
										GridRow(alignment: .firstTextBaseline) {
											Text("Equatorial distance")
												.kerning(1.5).foregroundStyle(Color.teal)
												.padding(.horizontal)
												.gridColumnAlignment(.leading)
											Text("\(eqd, specifier: "%.2f")°")
												.padding(.horizontal)
										}
										.padding(.vertical, 8)
									}
								}
								.background(Color.blue.opacity(0.05))
								.cornerRadius(8)
								.padding(.vertical, 64)
								
								Spacer()
							}
							.padding(.trailing, 64)
							
							VStack {
								
								CalibrationDataView(calibration: cal)
									.padding(.horizontal)
									.padding(.vertical)
								
								SkySphereView(source: skyViewSource, content: self)
									.background(Color.black)
									.padding()
							}
							.frame(minWidth: 720)
						}
					}
						
				} else {
					Spacer()
				}
			}
			.frame(minHeight: 600)
			.padding(.horizontal)
			.padding(.vertical)
			
			Spacer()
		}
		.padding(.vertical)
	}
	
	var infoTab: some View {
		
		HStack {
			
			selectGuidingSession
			
			ScrollView(.horizontal) {
				
				VStack {
					
					HStack(alignment: .top) {
						
						if let guiding = self.selectedGuiding {
							
							VStack {
								
								GuidingDriftInfoView(session: session, guidingSession: guiding)
									.padding(.horizontal)
									.padding(.bottom, 64)
								
								GuidingDataInfoView(session: session, guidingSession: guiding)
									.padding(.horizontal)
								
								Spacer()
							}
							.frame(minWidth: 600, maxWidth: 800)
							
							VStack {
								GuidingParametersView(session: session, guidingSession: guiding)
									.padding(.horizontal)
							}
							.frame(minWidth: 600, maxWidth: 800)
						}
						
						
						if self.hasGuidingImages {
							distributionCharts
								.frame(minWidth: 480, maxWidth: 800)
						}
						
					}
					.padding(.top, 8)
					
					Spacer()
				}
				.padding(.vertical)
			}
			.frame(minHeight: 620, maxHeight: .infinity)
		}
	}
	
	var raGuidingTab: some View {
		
		HStack {
			selectGuidingSession
			
			VStack {
				
				if let guiding = self.selectedGuiding {
					HStack {
						CorrectionsDistributionsGraph(session: guiding, item: .east, color: .blue)
							.padding()
						CorrectionsDistributionsGraph(session: guiding, item: .west, color: .blue)
							.padding()
					}
					
					CorrectionsEastWestGraph(session: guiding)
						.padding()
				}
			}
			.padding(.vertical)
			.frame(minHeight: 620, maxHeight: .infinity)
		}
	}
	
	var decGuidingTab: some View {
		
		HStack {
			selectGuidingSession
			
			VStack {
				
				if let guiding = selectedGuiding {
					HStack {
						CorrectionsDistributionsGraph(session: guiding, item: .north, color: .red)
							.padding()
						CorrectionsDistributionsGraph(session: guiding, item: .south, color: .red)
							.padding()
					}
					
					CorrectionsNorthSouthGraph(session: guiding)
						.padding()
				}
			}
			.padding(.vertical)
			.frame(minHeight: 620, maxHeight: .infinity)
		}
	}
	
	var distributionCharts: some View {
		ScrollView(.vertical) {
			VStack(spacing: 16) {
				
				if let hfrBinnedData = self.hfrBinnedData, hfrBinnedData.data.isNotEmpty, hfrBinnedData.guidingId == selectedGuidingSessionId {
					DistributionChart(binnedData: hfrBinnedData,
									  showNormalDist: true)
						.padding()
						.frame(height: 260)
				}
				
				if let eccBinnedData = self.eccBinnedData, eccBinnedData.data.isNotEmpty, eccBinnedData.guidingId == selectedGuidingSessionId {
					DistributionChart(binnedData: eccBinnedData,
									  showNormalDist: true)
						.padding()
						.frame(height: 260)
				}
				
				if let rmsBinnedData = self.rmsBinnedData, rmsBinnedData.data.isNotEmpty, rmsBinnedData.guidingId == selectedGuidingSessionId {
					DistributionChart(binnedData: rmsBinnedData,
									  showNormalDist: true)
						.padding()
						.frame(height: 260)
				}
				
			}
			.padding(.horizontal, 8)
			.id(selectedGuidingSessionId)
		}
	}
	
	var minGuidingDurations: [(text: String, value: Int)] {
		return [
			("-", 0),
			("5", 5),
			("10", 10),
			("15", 15),
			("20", 20),
			("30", 30)
		]
	}
	
	var guidingSessions: [GuidingData]  {
		let sessions = session.guiding.getGuidingSessions(onlyImages: imagesOnly, minDuration: minGuidingDuration)
		if sessions.isEmpty {
			let sessions2 = session.guiding.getGuidingSessions(onlyImages: imagesOnly, minDuration: 0)
			if sessions2.isEmpty {
				let sessions3 = session.guiding.getGuidingSessions(onlyImages: false, minDuration: minGuidingDuration)
				if sessions3.isEmpty {
					let sessions4 = session.guiding.getGuidingSessions(onlyImages: false, minDuration: 0)
					minGuidingDuration = 0
					imagesOnly = false
					return sessions4
				}
					
				imagesOnly = false
				return sessions3
			}
			minGuidingDuration = 0
			return sessions2
		}
		return sessions
	}
	
	var selectGuidingSession: some View {
		VStack(alignment: .leading) {
			
			Group {
				Toggle(isOn: $imagesOnly) {
					Text("Only sessions with captured images")
				}
				.toggleStyle(.checkbox)
				.padding(.bottom, 8)
				
				HStack {
					Picker("Minimum Duration", selection: $minGuidingDuration) {
						ForEach(minGuidingDurations, id: \.self.text) { duration in
							Text(duration.text)
								.tag(duration.value)
						}
					}
					.frame(width: 180)
					Text("Minutes")
				}
				.padding(.bottom)
			}
			.padding(.leading)
			
			List(selection: $selectedGuidingSessionId) {
				ForEach(guidingSessions) { gs in
					VStack(alignment: .leading) {
						Text(gs.getSessionTime(timeZone: session.currentTimeZone))
							.foregroundStyle(Color.lightBlue)
						
						if gs.images.count > 0 {
							Text("\(gs.images.count) Lights captured")
								.font(.caption).fontWeight(.light).foregroundStyle(Color.secondary)
						}
						
						ForEach(gs.getSessionInfo(), id: \.self) { text in
							Text(text).font(.footnote).fontWeight(.light).foregroundStyle(Color.secondary)
						}
					}
					.tag(gs.id)
					.padding(.horizontal, 8)
					.padding(.bottom, 4)
				}
			}
			.listRowSeparator(.hidden)
			.padding(.horizontal, 8)
			.id(imagesOnly)
			
		}
		.frame(width: 350)
		.padding(.vertical, 32)
	}
	
	var guideGraphToolbar: some View {
		HStack {
			Picker("Scale", selection: $scaleY) {
				Text("16\"").tag(16)
				Text("8\"").tag(8)
				Text("4\"").tag(4)
				Text("2\"").tag(2)
				Text("1\"").tag(1)
			}
			.frame(width: 150)
			.padding(.horizontal)
			.padding(.vertical, 8)
			
			Slider(value: $scaleX, in: 5.0...20.0)
				.frame(width: 600)
			
			Toggle(isOn: $raEnabled) {
				HStack {
					HLine()
						.strokeColor(Color.blue, lineWidth: 3.0)
						.frame(width: 20, height: 20)
					
					Text("α")
						.foregroundColor(Color.blue)
				}
			}
			.padding(.horizontal)
			
			Toggle(isOn: $decEnabled) {
				HStack {
					HLine()
						.strokeColor(Color.red, lineWidth: 3.0)
						.frame(width: 20, height: 20)
					
					Text("δ")
						.foregroundColor(Color.red)
				}
			}
			.padding(.horizontal)
			
			Toggle(isOn: $snrEnabled) {
				HStack {
					HLine()
						.strokeColor(Color.primary.opacity(0.8), lineWidth: 1.0)
						.frame(width: 20, height: 20)
					
					Text("SNR")
						.foregroundColor(Color.primary.opacity(0.8))
				}
			}
			.padding(.horizontal)
			
			Toggle(isOn: $correctionsEnabled) {
				HStack {
					Text("Corrections")
						.foregroundColor(Color.primary.opacity(0.8))
				}
			}
			.padding(.horizontal)
			
			Toggle(isOn: $imagesEnabled) {
				HStack {
					Text("Images")
						.foregroundColor(Color.primary.opacity(0.8))
				}
			}
			.padding(.horizontal)
			
			Toggle(isOn: $ditheringEnabled) {
				HStack {
					Text("Dithering")
						.foregroundColor(Color.primary.opacity(0.8))
				}
			}
			.padding(.horizontal)
			
			Toggle(isOn: $issuesEnabled) {
				HStack {
					Text("Under- and Overcorrections")
						.foregroundColor(Color.primary.opacity(0.8))
				}
			}
			.padding(.horizontal)
			
			Toggle(isOn: $lostStarsEnabled) {
				HStack {
					Text("Lost Stars")
						.foregroundColor(Color.primary.opacity(0.8))
				}
			}
			.padding(.horizontal)
			
			Spacer()
		}
	}
	
	
	var body: some View {
		VStack(spacing: 0) {
			
			HStack(alignment: .top) {
				
				VStack(alignment: .leading) {
					
					TabView(selection: $selectedTabChart) {
						
						infoTab
							.tabItem {
								Text("Info")
							}.tag(GuidingTabs.info)
						
						raGuidingTab
							.tabItem {
								Text("α Guiding")
							}.tag(GuidingTabs.ra)
						
						decGuidingTab
							.tabItem {
								Text("δ Guiding")
							}.tag(GuidingTabs.dec)
						
						
						if session.guiding.Calibrations.isNotEmpty {
							calibrationTab
								.tabItem {
									Text("Calibration")
								}.tag(GuidingTabs.calibration)
							
						}
					}
				}
				.padding(.horizontal)
				
			}
			
			
			if selectedTabChart != GuidingTabs.calibration {
				
				guideGraphToolbar
					.background(Color.darkBlue.gradient.opacity(0.3))
				
				if let guiding = self.selectedGuiding {
					GuideGraphSessionView(session: session, guidingSession: guiding, scaleX: $scaleX, scaleY: $scaleY,
										  raEnabled: $raEnabled, decEnabled: $decEnabled,
										  snrEnabled: $snrEnabled, correctionsEnabled: $correctionsEnabled,
										  ditheringEnabled: $ditheringEnabled, lostStarsEnabled: $lostStarsEnabled,
										  imagesEnabled: $imagesEnabled, issuesEnabled: $issuesEnabled)
						.frame(height: 300)
						.transition(.blurReplace)
						.id(guiding.id)
				}
			}
		}
		.background {
			Color.controlBackgroundDarkColor
		}
		.onAppear {
			self.calibrationDate = session.guiding.Calibrations.last?.id
			self.hfrBinnedData = BinnedData(session: session, item: .HFR)
			self.eccBinnedData = BinnedData(session: session, item: .ECCENTRICITY)
			self.rmsBinnedData = BinnedData(session: session, item: .RMS)
			
			if let calibration = session.guiding.Calibrations.first(where: { $0.id == self.calibrationDate }),
			   let image = session.sessionImages.first {
				
				let jdd = JulianDate.fromDate(date: calibration.calibrationDate)
				if let lat = image.obsLat, let long = image.obsLong {
					let loc = CLLocation(latitude: lat, longitude: long)
					self.skyViewSource.setup(location: loc, jdd: jdd, maxMagnitude: 5.0)
				}
			}
				
			Task {
				await self.hfrBinnedData?.update(for: selectedGuidingSessionId)
				await self.eccBinnedData?.update(for: selectedGuidingSessionId)
				await self.rmsBinnedData?.update(for: selectedGuidingSessionId)
			}
		}
		.onChange(of: self.calibrationDate) { _, id in
			
			if id == nil {
				self.calibrationDate = session.guiding.Calibrations.last?.id
			}
			
			if let calibration = session.guiding.Calibrations.first(where: { $0.id == self.calibrationDate }),
			   let image = session.sessionImages.first {
				
				let jdd = JulianDate.fromDate(date: calibration.calibrationDate)
				if let lat = image.obsLat, let long = image.obsLong {
					let loc = CLLocation(latitude: lat, longitude: long)
					self.skyViewSource.setup(location: loc, jdd: jdd, maxMagnitude: 5.0)
				}
			}
		}
		.onChange(of: self.minGuidingDuration) { _, _ in
			let sessions = self.guidingSessions
			if !sessions.contains(where: { $0.id == selectedGuidingSessionId }) {
				selectedGuidingSessionId = sessions.sorted(by: { $0.duration > $1.duration}).first!.id
			}
		}
		.onChange(of: self.imagesOnly) { _, _ in
			let sessions = self.guidingSessions
			if !sessions.contains(where: { $0.id == selectedGuidingSessionId }) {
				selectedGuidingSessionId = sessions.sorted(by: { $0.duration > $1.duration}).first!.id
			}
		}
		.onChange(of: selectedGuidingSessionId, { _, id in
			Task {
				await self.hfrBinnedData?.update(for: selectedGuidingSessionId)
				await self.eccBinnedData?.update(for: selectedGuidingSessionId)
				await self.rmsBinnedData?.update(for: selectedGuidingSessionId)
			}
		})
		//.animation(Animation.easeInOut, value: selectedGuidingSessionId)
    }
}

