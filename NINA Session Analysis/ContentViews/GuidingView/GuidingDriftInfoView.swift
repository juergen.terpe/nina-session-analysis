//
//  GuidingDriftInfoView.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 08.05.24.
//

import SwiftUI

struct GuidingDriftInfoView: View {
	let session: SessionData
	let guidingSession: GuidingData
	let nameWidth: CGFloat = 320
	let nameWidthMin: CGFloat = 200
	let valueWidth: CGFloat = 100
	let valueWidth2: CGFloat = 200
	let valueWidthMin: CGFloat = 90
	
	var body: some View {
		VStack {
			if let info = guidingSession.driftAnalysis, let pxScale = session.getPixelScale() {
				
				Grid(alignment: .trailing, horizontalSpacing: 8, verticalSpacing: 4) {
					
					GridRow(alignment: .firstTextBaseline) {
						Text("")
						Text("\"").foregroundColor(Color.teal)
						Text(guidingSession.camera.isEmpty ? "Pixel (Guiding camera)" : guidingSession.camera)
							.fontWeight(.semibold).kerning(1.5)
							.foregroundColor(Color.teal)
							
						Text(session.filteredImages.first?.Camera ?? "Pixel (Camera)")
							.fontWeight(.semibold).kerning(1.5)
							.foregroundColor(Color.teal)
					}
					.padding(.horizontal).padding(.vertical, 8)
					
					GridRow {
						Divider()
							.gridCellColumns(4)
					}
					
					GridRow(alignment: .firstTextBaseline) {
						Text("RMS α").fontWeight(.semibold)
						Text(info.rmsRAArcSeconds)
						Text(info.rmsRAPixel)
						Text(info.rmsRAPixelCamera(pixelScale: pxScale))
					}
					.padding(.horizontal).padding(.vertical, 8)
					
					GridRow {
						Divider()
							.gridCellColumns(4)
					}
					
					GridRow(alignment: .firstTextBaseline) {
						Text("RMS δ").fontWeight(.semibold)
						Text(info.rmsDECArcSeconds)
						Text(info.rmsDECPixel)
						Text(info.rmsDECPixelCamera(pixelScale: pxScale))
					}
					.padding(.horizontal).padding(.vertical, 8)
					
					GridRow {
						Divider()
							.gridCellColumns(4)
					}
					
					GridRow(alignment: .firstTextBaseline) {
						Text("Peak α").fontWeight(.semibold)
						Text(info.peakRAArcSeconds)
						Text(info.peakRAPixel)
						Text(info.peakRAPixelCamera(pixelScale: pxScale))
					}
					.padding(.horizontal).padding(.vertical, 8)
					
					GridRow {
						Divider()
							.gridCellColumns(4)
					}
					
					GridRow(alignment: .firstTextBaseline) {
						Text("Peak δ").fontWeight(.semibold)
						Text(info.peakDECArcSeconds)
						Text(info.peakDECPixel)
						Text(info.peakDECPixelCamera(pixelScale: pxScale))
					}
					.padding(.horizontal).padding(.vertical, 8)
					
					GridRow {
						Divider()
							.gridCellColumns(4)
					}
					
					GridRow(alignment: .firstTextBaseline) {
						Text("Elongation").fontWeight(.semibold)
						Text(info.rmsRAArcSeconds)
						Text(info.rmsRAPixel)
						Text(info.rmsRAPixelCamera(pixelScale: pxScale))
					}
					.padding(.horizontal).padding(.vertical, 8)
					
					GridRow {
						Divider()
							.gridCellColumns(4)
					}
					
					GridRow(alignment: .firstTextBaseline) {
						Text("Drift α").fontWeight(.semibold)
						Text(info.raDriftArcSeconds)
						Text(info.raDriftPixel)
						Text(info.raDriftPixelCamera(pixelScale: pxScale))
					}
					.padding(.horizontal).padding(.vertical, 8)
					
					GridRow {
						Divider()
							.gridCellColumns(4)
					}
					
					GridRow(alignment: .firstTextBaseline) {
						Text("Drift δ").fontWeight(.semibold)
						Text(info.decDriftArcSeconds)
						Text(info.decDriftPixel)
						Text(info.decDriftPixelCamera(pixelScale: pxScale))
					}
					.padding(.horizontal).padding(.vertical, 8)
					
					GridRow {
						Divider()
							.gridCellColumns(4)
					}
					
					GridRow(alignment: .firstTextBaseline) {
						Text("Polar Alignment").fontWeight(.semibold)
						Text(info.errorPolarAlignment)
						Text("")
						Text("")
					}
					.padding(.horizontal).padding(.vertical, 8)
				}
				.background(Color.primary.opacity(0.05))
				.cornerRadius(8)
				
			}
		}
    }
}


