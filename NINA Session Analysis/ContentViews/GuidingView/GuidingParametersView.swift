//
//  GuidingParametersView.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 25.02.25.
//
import SwiftUI

struct GuidingParameterInfo: Identifiable {
	let id: UUID = UUID()
	let name: LocalizedStringKey
	let value: String
	var separator = false
}

@Observable
class GuidingParameterModel {
	var parameters: [GuidingParameterInfo] = []
	
	func update(for session: SessionData, guidingSession: GuidingData) async {
		let parameters = getValues(for: session, guidingSession)
		
		await MainActor.run {
			self.parameters = parameters
		}
	}
	
	private func getValues(for session: SessionData, _ guidingSession: GuidingData) ->  [GuidingParameterInfo] {
		var result =  [GuidingParameterInfo]()
		
		if let profile = guidingSession.profile  {
			result.append(GuidingParameterInfo(name: "Profile", value: profile))
		}
		
		result.append(GuidingParameterInfo(name: "Start",
										   value: "\(guidingSession.guidingStart.toDateTime(session.currentTimeZone).formatted(date: .abbreviated, time: .shortened))"))
		result.append(GuidingParameterInfo(name: "End",
										   value: "\(guidingSession.guidingEnd.toDateTime(session.currentTimeZone).formatted(date: .abbreviated, time: .shortened))"))
		
		if let exp = guidingSession.exposure {
			result.append(GuidingParameterInfo(name: "Guiding Exposure", value: "\(exp) ms"))
		}
		
		if let binning = guidingSession.binning {
			result.append(GuidingParameterInfo(name: "Binning", value: "\(binning)x"))
		}
		
		if let noise = guidingSession.noiseReduction {
			result.append(GuidingParameterInfo(name: "Noise reduction", value: noise))
		}
		
		if let searchRegionSize = guidingSession.searchRegionSize {
			result.append(GuidingParameterInfo(name: "Search region size", value: "\(searchRegionSize) px"))
		}
		
		if let starMassTolerance = guidingSession.starMassTolerance {
			result.append(GuidingParameterInfo(name: "Star mass tolerance", value: starMassTolerance))
		}
		
		if let multiStarMode = guidingSession.multiStarMode	{
			result.append(GuidingParameterInfo(name: "Star mode", value: multiStarMode))
		}
		
		if let axesDither = guidingSession.ditherAxes {
			result.append(GuidingParameterInfo(name: "Dither axes", value: axesDither))
		}
		
		if let ditherScale = guidingSession.ditherScale {
			result.append(GuidingParameterInfo(name: "Dither scale", value: "\(ditherScale)x"))
		}
		
		
		if let ra = guidingSession.raStart, let dec = guidingSession.decStart {
			result.append(GuidingParameterInfo(name: "α Start", value: "\(ValueFormatter.formatRectascension(Angle.degrees(Double(ra)*15.0)))"))
			result.append(GuidingParameterInfo(name: "δ Start", value: "\(ValueFormatter.formatDeclination(Angle.degrees(Double(dec))))"))
		}
		
		if let az = guidingSession.azStart, let alt = guidingSession.altStart {
			result.append(GuidingParameterInfo(name: "Azimuth Start", value: "\(ValueFormatter.formatAzimuth(Angle.degrees(Double(az))))"))
			result.append(GuidingParameterInfo(name: "Altitude Start", value: "\(ValueFormatter.formatAltitude(Angle.degrees(Double(alt))))"))
		}
		
		if let alg = guidingSession.raGuideAlgorithm {
			result.append(GuidingParameterInfo(name: "", value: "", separator: true))
			
			result.append(GuidingParameterInfo(name: "α Guiding", value: alg))
			
			if guidingSession.raRate > 0.0  {
				result.append(GuidingParameterInfo(name: "α Tracking Rate", value: "\(guidingSession.raRate)\"/s"))
			}
			
			if let rate = guidingSession.normRateRA  {
				result.append(GuidingParameterInfo(name: "α Norm Rate", value: "\(rate)\"/s"))
			}
			
			if let pred = guidingSession.raPredictionGain {
				result.append(GuidingParameterInfo(name: "α Predictive Gain", value: "\(pred)"))
			}
			
			if let aggr = guidingSession.raGuideControlGain {
				result.append(GuidingParameterInfo(name: "Control Gain", value: "\(aggr)"))
			}
			
			if let hyst = guidingSession.raGuidingHysteresis {
				result.append(GuidingParameterInfo(name: "α Guiding Hysteresis", value: "\(hyst)"))
			}
			
			if let aggr = guidingSession.raGuidingAggressiveness {
				result.append(GuidingParameterInfo(name: "α Guiding Aggressiveness", value: "\(aggr)"))
			}
			
			if let move = guidingSession.raMinMove {
				result.append(GuidingParameterInfo(name: "α Min Move", value: "\(move) px"))
			}
			
			if let dur = guidingSession.raMaxPulseDuration {
				result.append(GuidingParameterInfo(name: "α Maximum Pulse", value: "\(dur) ms"))
			}
		}
		
		if let alg = guidingSession.decGuideAlgorithm {
			result.append(GuidingParameterInfo(name: "", value: "", separator: true))
			
			result.append(GuidingParameterInfo(name: "δ Guiding", value: alg))
			
			if guidingSession.decRate > 0.0 {
				result.append(GuidingParameterInfo(name: "δ Tracking Rate", value: "\(guidingSession.decRate)\"/s"))
			}
			
			if let rate = guidingSession.normRateDEC {
				result.append(GuidingParameterInfo(name: "δ Norm Rate", value: "\(rate)\"/s"))
			}
			
			if let aggr = guidingSession.decAggressiveness {
				result.append(GuidingParameterInfo(name: "δ Aggressiveness", value: "\(aggr)"))
			}
			
			if let move = guidingSession.decMinMove {
				result.append(GuidingParameterInfo(name: "δ Min Move", value: "\(move) px"))
			}
			
			if let dur = guidingSession.decMaxPulseDuration {
				result.append(GuidingParameterInfo(name: "δ Maximum Pulse", value: "\(dur) ms"))
			}
			
			if let mode = guidingSession.decGuideMode {
				result.append(GuidingParameterInfo(name: "δ Guiding Mode", value: mode))
			}
		}
		
		if let backlashPulse = guidingSession.backlashPulse, let backlashCompensation = guidingSession.backlashCompensation {
			result.append(GuidingParameterInfo(name: "", value: "", separator: true))
			
			result.append(GuidingParameterInfo(name: "Backlash Compensation", value: backlashCompensation ? "enabled" : "disabled"))
			result.append(GuidingParameterInfo(name: "Backlash Pulse", value: "\(backlashPulse) ms"))
		}
		
		result.append(GuidingParameterInfo(name: "", value: "", separator: true))
		
		return result
	}
}


struct GuidingParametersView: View {
	var session: SessionData
	var guidingSession: GuidingData
	@State private var model = GuidingParameterModel()
	
	
	var body: some View {
		ScrollView(.vertical) {
			Grid(alignment: .trailing, horizontalSpacing: 8, verticalSpacing: 4) {
				
				ForEach(model.parameters) { txt in
						GridRow(alignment: .firstTextBaseline) {
							Text(txt.name)
								.fontWeight(.semibold).kerning(1.5)
								.foregroundColor(Color.teal.opacity(0.8))
								.gridColumnAlignment(.leading)
							Text(txt.value)
						}
						.padding(.horizontal).padding(.vertical, 4)
						
						if !txt.separator {
							GridRow {
								Divider()
									.gridCellColumns(2)
							}
						}
				}
			}
			.background(Color.primary.opacity(0.05))
			.cornerRadius(8)
		}
		.task {
			await model.update(for: session, guidingSession: guidingSession)
		}
		.onChange(of: session.id) { _, _ in
			Task {
				await model.update(for: session, guidingSession: guidingSession)
			}
		}
		.onChange(of: guidingSession.id) { _, _ in
			Task {
				await model.update(for: session, guidingSession: guidingSession)
			}
		}
	}
}
