//
//  GuidingDataInfoView.swift
//  NINA-Session-Analysis
//
//  Created by Jürgen Terpe on 18.05.24.
//

import SwiftUI

fileprivate struct DitherStatistics: Identifiable {
	let id: UUID = UUID()
	
	let maxDx: Float
	let maxDy: Float
	let avgDx: Float
	let avgDy: Float
	let minDitherTime: Double
	let maxDitherTime: Double
	let avgDitherTime: Double
	let pixelScale: Float
	
	func maxDxArcSeconds() -> String {
		let dx = maxDx / pixelScale
		return String(format: "%.2f\"", dx)
	}
	
	func avgDxArcSeconds() -> String {
		let dx = avgDx / pixelScale
		return String(format: "%.2f\"", dx)
	}
	
	func maxDyArcSeconds() -> String {
		let dx = maxDy / pixelScale
		return String(format: "%.2f\"", dx)
	}
	
	func avgDyArcSeconds() -> String {
		let dx = avgDy / pixelScale
		return String(format: "%.2f\"", dx)
	}
	
	func maxDxCamera(_ pxScale: Float) -> String {
		let dx = maxDx / pixelScale * pxScale
		return String(format: "%.1fpx", dx)
	}
	
	func maxDyCamera(_ pxScale: Float) -> String {
		let dy = maxDy / pixelScale * pxScale
		return String(format: "%.1fpx", dy)
	}
	
	func avgDxCamera(_ pxScale: Float) -> String {
		let dx = avgDx / pixelScale * pxScale
		return String(format: "%.1fpx", dx)
	}
	
	func avgDyCamera(_ pxScale: Float) -> String {
		let dy = avgDy / pixelScale * pxScale
		return String(format: "%.1fpx", dy)
	}
}


struct GuidingDataInfoView: View {
	let session: SessionData
	let guidingSession: GuidingData
	
	let nameWidth: CGFloat = 320
	let nameWidthMin: CGFloat = 80
	let valueWidth: CGFloat = 100
	let valueWidth2: CGFloat = 200
	let valueWidthMin: CGFloat = 60
	
	
	
	fileprivate func ditherData() -> DitherStatistics? {
		if !self.guidingSession.ditherData.isEmpty {
			var maxDx: Float = 0.0
			var sumDx: Float = 0.0
			var maxDy: Float = 0.0
			var sumDy: Float = 0.0
			var minTime: Double?
			var maxTime: Double?
			var sumTime: Double = 0.0
			
			for dither in self.guidingSession.ditherData {
				maxDx = max(dither.dx!, abs(maxDx))
				maxDy = max(dither.dy!, abs(maxDy))
				sumDx += abs(dither.dx!)
				sumDy += abs(dither.dy!)
				let dt = (dither.end - dither.begin) * 86400.0
				if minTime == nil {
					minTime = dt
				} else {
					minTime = min(minTime!, dt)
				}
				
				if maxTime == nil {
					maxTime = dt
				} else {
					maxTime = max(maxTime!, dt)
				}
				sumTime += dt
			}
			
			let avgTime = sumTime / Double(self.guidingSession.ditherData.count)
			let avgDx = sumDx / Float(self.guidingSession.ditherData.count)
			let avgDy = sumDy / Float(self.guidingSession.ditherData.count)
			let pxScale = self.guidingSession.pixelScale ?? 1.0
			
			return DitherStatistics(maxDx: maxDx,
									maxDy: maxDy, 
									avgDx: avgDx,
									avgDy: avgDy,
									minDitherTime: minTime!, maxDitherTime: maxTime!, avgDitherTime: avgTime, pixelScale: pxScale)
			
		}
		return nil
	}
	
    var body: some View {
		HStack {
			
			if let dither = self.ditherData(), let pxScale = session.getPixelScale() {
				Grid(alignment: .trailing) {
					
					GridRow(alignment: .firstTextBaseline) {
						Text("Dither").fontWeight(.semibold).kerning(1.5).foregroundColor(Color.teal)
						Text("Range").fontWeight(.semibold).kerning(1.5).foregroundColor(Color.teal)
						
						Text("⌀")
							.fontWeight(.semibold).kerning(1.5).foregroundColor(Color.teal)
							
						
						Text("\(session.sessionImages.map({ $0.camera ?? "" }).first ?? "")")
							.lineLimit(1)
							.truncationMode(.middle)
							.fontWeight(.semibold).kerning(1.5)
							.foregroundColor(Color.teal)
							
					}
					.padding(.horizontal).padding(.vertical, 8)
					
					GridRow {
						Divider()
							.gridCellColumns(4)
					}
					
					
					GridRow(alignment: .firstTextBaseline) {
						
						Text("dx").fontWeight(.semibold).kerning(1.5)
						Text("\(dither.maxDx, specifier: "%.1f")px \(dither.maxDxArcSeconds())")
							
						Text("\(dither.avgDx, specifier: "%.1f")px \(dither.avgDxArcSeconds())")
							
						Text("max \(dither.maxDxCamera(pxScale)) ⌀ \(dither.avgDxCamera(pxScale))")
					}
					.padding(.horizontal).padding(.vertical, 8)
					
					GridRow {
						Divider()
							.gridCellColumns(4)
					}
					
					GridRow(alignment: .firstTextBaseline) {
						
						Text("dy").fontWeight(.semibold).kerning(1.5)
						Text("\(dither.maxDy, specifier: "%.1f")px \(dither.maxDyArcSeconds())")
							
						Text("\(dither.avgDy, specifier: "%.1f")px \(dither.avgDyArcSeconds())")
							
						
						Text("max \(dither.maxDyCamera(pxScale)) ⌀ \(dither.avgDyCamera(pxScale))")
							
					}
					.padding(.horizontal).padding(.vertical, 8)
					
					GridRow {
						Divider()
							.gridCellColumns(4)
					}
					
					GridRow(alignment: .firstTextBaseline) {
						
						Text("Duration").fontWeight(.semibold).kerning(1.5)
						Text("\(dither.minDitherTime, specifier: "%.1f")-\(dither.maxDitherTime, specifier: "%.1f")s")
							
						Text("\(dither.avgDitherTime, specifier: "%.1f")s")
							
						Text("")
							
					}
					.padding(.horizontal).padding(.vertical, 8)
					
				}
				.background(Color.primary.opacity(0.05))
				.cornerRadius(8)
				
			}
		
		}
    }
}

