//
//  ToolsPanel.swift
//  NINA Session Analysis
//
//  Created by Jürgen Terpe on 02.04.24.
//

import SwiftUI
import TipKit

struct ToolsPanel: View {
	var menuItem: MenuItem
	@Binding var showCheckList: Bool
	@Binding var showSettings: Bool
	@Binding var toolsActive: Bool
	@Bindable var session: SessionData
	
	func getSessionDates() -> [DateOfSession] {
		if menuItem == .ninaLog {
			return session.sessionDates.filter({ $0.text != "Any"})
		} else {
			return session.sessionDates
		}
	}
	
    var body: some View {
		HStack {
			
			if menuItem != .hints && menuItem != .ninaLog {
				
				Picker("Date", selection: $session.selectedSessionDate) {
					ForEach(getSessionDates(), id: \.self) { sessionDate in
						Text(sessionDate.text).truncationMode(.middle).tag(sessionDate.text)
					}
				}
				.frame(minWidth: 160, maxWidth: 180)
				.padding()
				.disabled(!toolsActive)
			}
			
			if menuItem != .ninaLog && menuItem != .hints && menuItem != .timingStatistics {
				
				Picker("Target", selection: $session.selectedTarget) {
					ForEach(session.targets, id: \.self) { target in
						Text(target).truncationMode(.middle).tag(target)
					}
				}
				.frame(minWidth: 160, maxWidth: 300)
				.padding()
				.disabled(!toolsActive)
				
				Picker("Filter", selection: $session.selectedFilter) {
					ForEach(session.filters, id: \.self) { filter in
						Text(filter).truncationMode(.middle).tag(filter)
					}
				}
				.frame(minWidth: 100, maxWidth: 200)
				.padding()
				.disabled(!toolsActive)
				
				Picker("Pierside", selection: $session.selectedPierSide) {
					ForEach(session.pierSides, id: \.self) { side in
						Text(side).truncationMode(.middle).tag(side)
					}
				}
				.frame(minWidth: 80, maxWidth: 140)
				.padding()
				.disabled(!toolsActive)
				
				Picker("Exposure", selection: $session.selectedExposure) {
					ForEach(session.exposures, id: \.self) { exp in
						Text(exp).truncationMode(.middle).tag(exp)
					}
				}
				.frame(minWidth: 80, maxWidth: 140)
				.padding()
				.disabled(!toolsActive)
			}
			
			Spacer()
			
			if menuItem == .table || menuItem == .hints || menuItem == .data || menuItem == .overview {
				
				Button {
					withAnimation(.easeInOut) {
						self.showSettings.toggle()
						if self.showCheckList && self.showSettings {
							self.showCheckList = false
						}
						
					}
				} label: {
					Image(systemName: "gear")
						.foregroundStyle(self.showSettings ? Color.yellow : Color.primary)
				}
				.padding(.trailing)
				.disabled(!toolsActive)
			}
			
			if menuItem == .overview {
				
				Button {
					withAnimation(.easeInOut) {
						self.showCheckList.toggle()
						if self.showCheckList && self.showSettings {
							self.showSettings = false
						}
					}
				} label: {
					Image(systemName: "checklist")
						.foregroundStyle(self.showCheckList ? Color.yellow : Color.primary)

				}
				.disabled(self.menuItem != .overview && self.menuItem != .table)
				.popoverTip(SelectChartsTip(), arrowEdge: .leading)
				.disabled(!toolsActive)
				
			} else if menuItem == .table {
				Button {
					withAnimation(.easeInOut) {
						self.showCheckList.toggle()
						if self.showCheckList && self.showSettings {
							self.showSettings = false
						}
					}
				} label: {
					Image(systemName: "checklist")
				}
				.disabled(self.menuItem != .overview && self.menuItem != .table)
				.popoverTip(SelectColumnsTip(), arrowEdge: .leading)
				.disabled(!toolsActive)
			}
			
		}
		.padding()
		.frame(height: 40)
		.background(.ultraThickMaterial)
		.onChange(of: menuItem) { _, _ in
			if menuItem == .ninaLog && session.selectedSessionDate == "Any" && session.sessionDates.count > 1 {
				session.selectedSessionDate = getSessionDates().first!.text
			}
		}
    }
}
